"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.expressAuthentication = void 0;
const crypto = __importStar(require("crypto"));
const adminService_1 = require("../services/adminService");
const utils = __importStar(require("../utilities/utils"));
const Enums = __importStar(require("../utilities/enums"));
const customError_1 = __importDefault(require("./customError"));
const userRepository_1 = require("../repositories/userRepository");
const userRoleRepository_1 = require("../repositories/userRoleRepository");
const addressRepository_1 = require("../repositories/addressRepository");
const userDocumentRepository_1 = require("../repositories/userDocumentRepository");
const userApiKeyRepository_1 = require("../repositories/userApiKeyRepository");
const fireblocksNetworkLinkCustomError_1 = __importDefault(require("./fireblocksNetworkLinkCustomError"));
function expressAuthentication(request, securityName, scopes) {
    return __awaiter(this, void 0, void 0, function* () {
        if (securityName === "api_key") {
            if (!request.headers["authorization"])
                return new Promise((resolve, reject) => {
                    reject(new customError_1.default(Enums.responseCode.Unauthorized, Enums.responseMessage.Unauthorized));
                });
            /*
            if (!request.headers["access-code"])
              return new Promise((resolve, reject) => {
                reject(new CustomError(Enums.responseCode.Unauthorized, Enums.responseMessage.Unauthorized));
              });
              */
            const accessToken = request.headers["authorization"].replace("Bearer ", "");
            if (!accessToken) {
                return new Promise((resolve, reject) => {
                    reject(new customError_1.default(Enums.responseCode.Unauthorized, Enums.responseMessage.Unauthorized));
                });
            }
            try {
                const userAccess = yield utils.jwtVerifyToken(accessToken);
                const obj = JSON.stringify(userAccess);
                const resLogin = JSON.parse(obj);
                let tempObject = null;
                const userRepository = new userRepository_1.UserRepository();
                const userRoleRepository = new userRoleRepository_1.UserRoleRepository();
                const userAddressRepository = new addressRepository_1.AddressRepository();
                const userDocumentRepository = new userDocumentRepository_1.UserDocumentRepository();
                const _adminService = new adminService_1.AdminService(userRepository, userRoleRepository, userAddressRepository, userDocumentRepository, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject, tempObject);
                const userInfo = yield _adminService.getActiveUserById(parseInt(resLogin.user_id));
                return Promise.resolve({
                    user_id: userInfo.user_id,
                    code: userInfo.code,
                    user_name: userInfo.user_name,
                    first_name: userInfo.first_name,
                    middle_name: userInfo.middle_name,
                    last_name: userInfo.last_name,
                    email: userInfo.email,
                    tel: userInfo.tel,
                });
            }
            catch (error) {
                let errorMessage;
                if (error instanceof Error) {
                    errorMessage = error.message;
                }
                return new Promise((resolve, reject) => {
                    reject(new Error(errorMessage));
                });
            }
        }
        if (securityName === "network_link") {
            const userApiKeyRepository = new userApiKeyRepository_1.UserApiKeyRepository();
            const userRepository = new userRepository_1.UserRepository();
            if (!request.headers["x-fbapi-key"] ||
                !request.headers["x-fbapi-signature"] ||
                !request.headers["x-fbapi-timestamp"] ||
                !request.headers["x-fbapi-nonce"]) {
                return new Promise((resolve, reject) => {
                    reject(new fireblocksNetworkLinkCustomError_1.default(400, Enums.responseCodeFireblock.MISSING_REQUEST, Enums.responseMessageFireblock.MISSING_REQUEST));
                });
            }
            const fbKey = request.headers["x-fbapi-key"];
            const fbSignature = request.headers["x-fbapi-signature"];
            const fbTimestamp = request.headers["x-fbapi-timestamp"];
            const fbNonce = request.headers["x-fbapi-nonce"];
            const url = request.url.replace("/fireblocks", "");
            const reqBody = JSON.stringify(request.body) !== "{}" ? JSON.stringify(request.body) : "";
            const prehashString = fbTimestamp.toString() +
                fbNonce.toString() +
                request.method +
                url +
                reqBody;
            const preHashStringUTF8 = Buffer.from(prehashString, "utf-8").toString("hex");
            try {
                const userApiKey = yield userApiKeyRepository.getInfoByKey(fbKey.toString());
                if (userApiKey.length <= 0) {
                    throw new fireblocksNetworkLinkCustomError_1.default(400, Enums.responseCodeFireblock.ACCOUNT_NOT_FOUND, Enums.responseMessageFireblock.ACCOUNT_NOT_FOUND);
                }
                const hmac = crypto.createHmac("sha256", userApiKey[0].user_secret);
                hmac.update(preHashStringUTF8, "utf-8");
                const signature = hmac.digest("hex");
                if (signature === fbSignature) {
                    const users = yield userRepository.getActiveInfoById(userApiKey[0].user_id);
                    if (users.length <= 0) {
                        throw new fireblocksNetworkLinkCustomError_1.default(400, Enums.responseCodeFireblock.ACCOUNT_NOT_FOUND, Enums.responseMessageFireblock.ACCOUNT_NOT_FOUND);
                    }
                    const userInfo = users[0];
                    return Promise.resolve({
                        user_id: userInfo.user_id,
                        first_name: userInfo.first_name,
                        middle_name: userInfo.middle_name,
                        last_name: userInfo.last_name,
                        date_of_birth: userInfo.date_of_birth,
                        email: userInfo.email,
                        tel: userInfo.tel,
                    });
                }
                else {
                    throw new fireblocksNetworkLinkCustomError_1.default(400, Enums.responseCodeFireblock.SIGNATURE_INVALID, Enums.responseMessageFireblock.SIGNATURE_INVALID);
                }
            }
            catch (error) {
                throw error;
            }
        }
        return new Promise((resolve, reject) => {
            reject(new customError_1.default(Enums.responseCode.Unauthorized, Enums.responseMessage.Unauthorized));
        });
    });
}
exports.expressAuthentication = expressAuthentication;
//# sourceMappingURL=authentication.js.map