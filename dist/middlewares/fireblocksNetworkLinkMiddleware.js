"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fireblocksNetworkLinkAuthorization = void 0;
const fireblocksNetworkLinkAuthorization = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.headers["x-fbapi-key"])
        return res.sendStatus(401);
    if (!req.headers["x-fbapi-signature"])
        return res.sendStatus(401);
    if (!req.headers["x-fbapi-timestamp"])
        return res.sendStatus(401);
    if (!req.headers["x-fbapi-nonce"])
        return res.sendStatus(401);
    const fbKey = req.headers["x-fbapi-key"];
    const fbSignature = req.headers["x-fbapi-signature"];
    const fbTimestamp = req.headers["x-fbapi-timestamp"];
    const fbNonce = req.headers["x-fbapi-nonce"];
    console.log(fbKey);
    console.log(fbSignature);
    console.log(fbTimestamp);
    console.log(fbNonce);
    try {
        return next();
    }
    catch (err) {
        return res.status(500).send({
            code: 500,
            message: {
                error: (err === null || err === void 0 ? void 0 : err.message) || err,
                stack: process.env.NODE_ENV === "development" ? err === null || err === void 0 ? void 0 : err.stack : {},
            },
        });
    }
});
exports.fireblocksNetworkLinkAuthorization = fireblocksNetworkLinkAuthorization;
//# sourceMappingURL=fireblocksNetworkLinkMiddleware.js.map