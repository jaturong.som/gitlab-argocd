"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.morgan = void 0;
const morgan_1 = __importDefault(require("morgan"));
exports.morgan = morgan_1.default;
const httpLogService_1 = require("../services/httpLogService");
// import { FireblocksWebhookService } from "../services/fireblocksWebhookService";
const httpLogRepository_1 = require("../repositories/httpLogRepository");
// import { TransactionRepository } from "../repositories/transactionRepository";
// import { FireblocksTransactionRepository } from "../repositories/fireblocksTransactionRepository";
// import * as enums from "../utilities/enums";
// import { TransactionService } from "../services/transactionService";
// import { ChainalysisService } from "../services/chainalysisService";
// import { FireblocksService } from "../services/fireblocksService";
morgan_1.default.token("user", function getRequester(req) {
    return JSON.stringify(req.user);
});
morgan_1.default.token("input", function getInput(req) {
    let input = {};
    if (req.method === "GET") {
        input = req.query;
    }
    else {
        input = req.body;
    }
    // mask any input that should be secret
    input = Object.assign({}, input);
    if (input.password) {
        input.password = "***";
    }
    if (input.old_password) {
        input.old_password = "***";
    }
    if (input.new_password) {
        input.new_password = "***";
    }
    if (input.refresh_token) {
        input.refresh_token = "***";
    }
    return JSON.stringify(input);
});
morgan_1.default.token("response-body", (req, res) => {
    // console.log("Request:", req);
    // console.log("Response:", res);
    // console.log("requester:", req.user);
    // console.log("_remoteAddress:", req.socket.remoteAddress);
    // console.log("method:", req.method);
    // console.log("url:", req.url);
    // console.log("httpVersion:", req.httpVersion);
    // console.log("Input:", req.body);
    // console.log("response-body:", res.responseBody);
    var _a, _b;
    var jsonString = "";
    try {
        jsonString = JSON.stringify(req.headers);
    }
    catch (error) {
        console.log(error);
    }
    var vJSONValid = true;
    var body;
    try {
        body = Object.assign({}, JSON.parse(res.responseBody));
        vJSONValid = true;
    }
    catch (error) {
        vJSONValid = false;
    }
    if (vJSONValid) {
        const reqBody = req.body;
        // mask any input that should be secret
        if (reqBody === null || reqBody === void 0 ? void 0 : reqBody.password) {
            reqBody.password = "***";
        }
        if (reqBody === null || reqBody === void 0 ? void 0 : reqBody.old_password) {
            reqBody.old_password = "***";
        }
        if (reqBody === null || reqBody === void 0 ? void 0 : reqBody.new_password) {
            reqBody.new_password = "***";
        }
        if (reqBody === null || reqBody === void 0 ? void 0 : reqBody.refresh_token) {
            reqBody.refresh_token = "***";
        }
        if ((_a = body === null || body === void 0 ? void 0 : body.result) === null || _a === void 0 ? void 0 : _a.token) {
            body.result.token = "***";
        }
        if ((_b = body === null || body === void 0 ? void 0 : body.result) === null || _b === void 0 ? void 0 : _b.refresh_token) {
            body.result.refresh_token = "***";
        }
        try {
            var httpLogService = new httpLogService_1.HttpLogService(new httpLogRepository_1.HttpLogRepository());
            httpLogService.createHttpLog(req.user, req.socket.remoteAddress === undefined ? "" : req.socket.remoteAddress, req.method, req.url, jsonString, req.body, body);
        }
        catch (error) {
            console.log(error);
        }
    }
    return JSON.stringify(body);
});
//# sourceMappingURL=morgan.js.map