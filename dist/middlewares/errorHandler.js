"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const tsoa_1 = require("tsoa");
const customError_1 = __importDefault(require("./customError"));
const fireblocksNetworkLinkCustomError_1 = __importDefault(require("./fireblocksNetworkLinkCustomError"));
const errorHandler = (err, req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    if (err instanceof tsoa_1.ValidateError) {
        console.warn(`Caught Validation Error for ${req.path}:`, err.fields);
        return res.status(422).json({
            message: "Validation Failed",
            details: err === null || err === void 0 ? void 0 : err.fields,
        });
    }
    if (err instanceof customError_1.default) {
        console.warn(`Custom Error:`, err === null || err === void 0 ? void 0 : err.message);
        return res.status(400).json({
            code: err === null || err === void 0 ? void 0 : err.code,
            message: err === null || err === void 0 ? void 0 : err.message,
        });
    }
    if (err instanceof fireblocksNetworkLinkCustomError_1.default) {
        console.warn(`FireblocksNetworkLinkCustomError Error:`, err === null || err === void 0 ? void 0 : err.message);
        return res.status(err === null || err === void 0 ? void 0 : err.httpStatas).json({
            errorCode: err === null || err === void 0 ? void 0 : err.errorCode,
            error: err === null || err === void 0 ? void 0 : err.message,
        });
    }
    if (err instanceof Error) {
        console.error(`Error:`, err === null || err === void 0 ? void 0 : err.message);
        return res.status(500).json({
            code: 500,
            message: err === null || err === void 0 ? void 0 : err.message,
            stack: process.env.NODE_ENV === "development" ? err.stack : {},
        });
    }
    return next();
});
module.exports = errorHandler;
//# sourceMappingURL=errorHandler.js.map