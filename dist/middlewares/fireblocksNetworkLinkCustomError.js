"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class FireblocksNetworkLinkCustomError extends Error {
    constructor(httpStatas, code, message) {
        super(message);
        this.errorCode = code;
        this.error = message;
        this.httpStatas = httpStatas;
    }
}
exports.default = FireblocksNetworkLinkCustomError;
//# sourceMappingURL=fireblocksNetworkLinkCustomError.js.map