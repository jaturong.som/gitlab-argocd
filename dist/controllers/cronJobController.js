"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CronJobController = void 0;
const tsoa_1 = require("tsoa");
const CronJobModels = __importStar(require("../models/cronJobModels"));
const Enums = __importStar(require("../utilities/enums"));
const cronJobService_1 = require("../jobSchedulers/cronJobService");
const chainalysisService_1 = require("../services/chainalysisService");
const tsyringe_1 = require("tsyringe");
let CronJobController = exports.CronJobController = class CronJobController extends tsoa_1.Controller {
    constructor(cronJobService, chainalysisService) {
        super();
        this._cronJobService = cronJobService;
        this._chainalysisService = chainalysisService;
    }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._cronJobService.startJob();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    stop() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._cronJobService.stopJob();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    manualUpdateTransactionStatus(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._chainalysisService.manualUpdateTransactionStatus(req.external_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
};
__decorate([
    (0, tsoa_1.Post)("/start"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CronJobController.prototype, "start", null);
__decorate([
    (0, tsoa_1.Post)("/stop"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CronJobController.prototype, "stop", null);
__decorate([
    (0, tsoa_1.Post)("/chainalysis/manual"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CronJobController.prototype, "manualUpdateTransactionStatus", null);
exports.CronJobController = CronJobController = __decorate([
    (0, tsoa_1.Tags)("Job Scheduler"),
    (0, tsoa_1.Security)("api_key"),
    (0, tsoa_1.Route)("/api/v1/job"),
    (0, tsyringe_1.injectable)(),
    __metadata("design:paramtypes", [cronJobService_1.CronJobService,
        chainalysisService_1.ChainalysisService])
], CronJobController);
//# sourceMappingURL=cronJobController.js.map