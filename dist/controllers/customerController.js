"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerController = void 0;
const tsoa_1 = require("tsoa");
const Enums = __importStar(require("../utilities/enums"));
const CustomerModels = __importStar(require("../models/customerModels"));
const customerService_1 = require("../services/customerService");
const tsyringe_1 = require("tsyringe");
let CustomerController = exports.CustomerController = class CustomerController extends tsoa_1.Controller {
    constructor(customerService) {
        super();
        this._customerService = customerService;
    }
    getUserApiKeys(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getUserApiKeys(reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserApiKeyById(id, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getUserApiKeyById(id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createUserApiKey(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.createUserApiKey(reqIncomingMessage.user.user_id, req.name, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    disableUserApiKey(id, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.enableOrDisableUserApiKey(reqIncomingMessage.user.user_id, id, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    disableAllUserApiKeyByUserId(reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.disableAllUserApiKeyByUserId(reqIncomingMessage.user.user_id, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    requestAccessAsset(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.requestAccessAsset(reqIncomingMessage.user.user_id, req.asset_code);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserFiatAccounts(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getUserFiatAccounts(reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    // @Get("/bank-account/list")
    // public async getBankAccounts(
    //   @Request() reqIncomingMessage: any
    // ): Promise<BaseModels.ResponseResult> {
    //   try {
    //     const result = await this._customerService.getBankAccounts(
    //       reqIncomingMessage.user.user_id
    //     );
    //     return {
    //       code: Enums.responseCode.Success,
    //       message: Enums.responseMessage.Success,
    //       result: result,
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    //deprecate
    getBankAccountCoutries(reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getBankAccountCoutries();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getOwnerBankAccounts(reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getBankAccountsByBankType(reqIncomingMessage.user.user_id, Enums.bankAccountType.OWNER);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getOtherBankAccounts(reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getBankAccountsByBankType(reqIncomingMessage.user.user_id, Enums.bankAccountType.OTHER);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getBankAccountById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getBankAccountById(id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    // @Get("/bank-account/default/{id}")
    // public async getDefaultBankAccountByUser(
    //   @Path() id: number
    // ): Promise<BaseModels.ResponseResult> {
    //   try {
    //     const result = await this._customerService.getDefaultBankAccountByUser(
    //       id
    //     );
    //     return {
    //       code: Enums.responseCode.Success,
    //       message: Enums.responseMessage.Success,
    //       result: result,
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    createBankAccount(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const resultCreated = yield this._customerService.createBankAccount(req.bank_type, req.receiver_first_name, req.receiver_middle_name, req.receiver_last_name, req.receiver_address, req.receiver_date_of_birth, req.receiver_gender, req.receiver_contact_number, req.receiver_state, req.receiver_area_town, req.receiver_city, req.receiver_zip_code, req.receiver_country, req.receiver_nationality, req.receiver_id_type, req.receiver_id_type_remarks, req.receiver_id_number, req.receiver_id_issue_date, req.receiver_id_expire_date, req.receiver_email, req.receiver_account_type, req.receiver_occupation, req.receiver_occupation_remarks, req.receiver_district, req.beneficiary_type, req.location_id, req.bank_name, req.bank_branch_name, req.bank_branch_code, req.bank_account_number, req.swift_code, req.iban, reqIncomingMessage.user.user_id, reqIncomingMessage.user.user_id);
                const result = yield this._customerService.getBankAccountById(resultCreated);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    // @Post("/bank-account/owner/create")
    // public async createOwnerBankAccount(
    //   @Body() req: CustomerModels.RequestCreateOwnerBankAccount,
    //   @Request() reqIncomingMessage: any
    // ): Promise<BaseModels.ResponseResult> {
    //   try {
    //     const result = await this._customerService.createOwnerBankAccount(
    //       reqIncomingMessage.user.user_id,
    //       req.bank_id,
    //       req.bank_code,
    //       req.bank_name,
    //       req.account_no,
    //       req.account_name,
    //       req.swift_code!,
    //       req.iban!,
    //       reqIncomingMessage.user.user_id
    //     );
    //     return {
    //       code: Enums.responseCode.Success,
    //       message: Enums.responseMessage.Success,
    //       result: result,
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // @Post("/bank-account/other/create")
    // public async createOtherBankAccount(
    //   @Body() req: CustomerModels.RequestCreateOtherBankAccount,
    //   @Request() reqIncomingMessage: any
    // ): Promise<BaseModels.ResponseResult> {
    //   try {
    //     const result = await this._customerService.createOtherBankAccount(
    //       reqIncomingMessage.user.user_id,
    //       req.bank_code,
    //       req.bank_name,
    //       req.account_no,
    //       req.account_name,
    //       req.swift_code!,
    //       req.iban!,
    //       req.form_name,
    //       req.branch_code!,
    //       req.first_name,
    //       req.last_name,
    //       req.nationality!,
    //       req.date_of_birth!,
    //       req.mobile_number!,
    //       reqIncomingMessage.user.user_id,
    //       {
    //         line1: req.address !== undefined ? req.address.line1 : undefined,
    //         line2: req.address !== undefined ? req.address.line2 : undefined,
    //         city: req.address !== undefined ? req.address.city : undefined,
    //         state: req.address !== undefined ? req.address.state : undefined,
    //         countryCode:
    //           req.address !== undefined ? req.address.country_code : undefined,
    //         postcode:
    //           req.address !== undefined ? req.address.postcode : undefined,
    //       }
    //     );
    //     return {
    //       code: Enums.responseCode.Success,
    //       message: Enums.responseMessage.Success,
    //       result: result,
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // @Put("/bank-account/owner/{id}")
    // public async updateOwnerBankAccount(
    //   @Path() id: number,
    //   @Body() req: CustomerModels.RequestUpdateOwnerBankAccount,
    //   @Request() reqIncomingMessage: any
    // ): Promise<BaseModels.ResponseResult> {
    //   try {
    //     const result = await this._customerService.updateOwnerBankAccount(
    //       id,
    //       reqIncomingMessage.user.user_id,
    //       req.bank_code,
    //       req.bank_name,
    //       req.account_no,
    //       req.account_name,
    //       req.swift_code,
    //       req.iban,
    //       reqIncomingMessage.user.user_id
    //     );
    //     return {
    //       code: Enums.responseCode.Success,
    //       message: Enums.responseMessage.Success,
    //       result: result,
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // @Put("/bank-account/other/{id}")
    // public async updateOtherBankAccount(
    //   @Path() id: number,
    //   @Body() req: CustomerModels.RequestUpdateOtherBankAccount,
    //   @Request() reqIncomingMessage: any
    // ): Promise<BaseModels.ResponseResult> {
    //   try {
    //     const result = await this._customerService.updateOtherBankAccount(
    //       id,
    //       reqIncomingMessage.user.user_id,
    //       req.bank_code,
    //       req.bank_name,
    //       req.account_no,
    //       req.account_name,
    //       req.swift_code!,
    //       req.iban!,
    //       req.branch_code!,
    //       req.first_name,
    //       req.last_name,
    //       req.nationality!,
    //       req.date_of_birth!,
    //       req.mobile_number!,
    //       reqIncomingMessage.user.user_id,
    //       {
    //         addressId:
    //           req.address !== undefined ? req.address.address_id : undefined,
    //         line1: req.address !== undefined ? req.address.line1 : undefined,
    //         line2: req.address !== undefined ? req.address.line2 : undefined,
    //         city: req.address !== undefined ? req.address.city : undefined,
    //         state: req.address !== undefined ? req.address.state : undefined,
    //         countryCode:
    //           req.address !== undefined ? req.address.country_code : undefined,
    //         postcode:
    //           req.address !== undefined ? req.address.postcode : undefined,
    //       }
    //     );
    //     return {
    //       code: Enums.responseCode.Success,
    //       message: Enums.responseMessage.Success,
    //       result: result,
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // @Put("/bank-account/{id}/default")
    // public async updateDefaultBankAccount(
    //   @Path() id: number,
    //   @Body() req: CustomerModels.RequestUpdateDefaultBankAccount,
    //   @Request() reqIncomingMessage: any
    // ): Promise<BaseModels.ResponseResult> {
    //   try {
    //     const result = await this._customerService.updateDefaultBankAccount(
    //       id,
    //       reqIncomingMessage.user.user_id,
    //       reqIncomingMessage.user.user_id
    //     );
    //     return {
    //       code: Enums.responseCode.Success,
    //       message: Enums.responseMessage.Success,
    //       result: result,
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    deleteBankAccount(id, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.deleteBankAccount(id, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCryptoWalletListForUser(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getCryptoWalletList(reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTotalAssetForUser(id, asset_group) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getTotalAssetForUser(id, asset_group);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createTransaction(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.createTransaction(reqIncomingMessage.user.user_id, req.type, req.asset_code, req.total_assets, req.total_amount, req.input_type, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWithdraws(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getWithdraws(reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWithdrawById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getWithdrawById(id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createWithdrawTransaction(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.createWithdraw(req.sender_source_of_fund, req.sender_source_of_fund_remarks, req.purpose_of_remittance, req.purpose_of_remittance_remarks, req.sender_beneficiary_relationship, req.sender_beneficiary_relationship_remarks, req.transfer_amount, req.bank_account_id, reqIncomingMessage.user.user_id, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createTransferTransaction(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.createTransfer(req.sender_source_of_fund, req.sender_source_of_fund_remarks, req.purpose_of_remittance, req.purpose_of_remittance_remarks, req.sender_beneficiary_relationship, req.sender_beneficiary_relationship_remarks, req.transfer_amount, req.bank_account_id, reqIncomingMessage.user.user_id, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactions(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getTransactions(reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserProfile(reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getUserProfile(reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    // @Put("/profile")
    // public async updateUserProfile(
    //   @Body() req: CustomerModels.RequestUpdateCustomerProfile,
    //   @Request() reqIncomingMessage: any
    // ): Promise<BaseModels.ResponseResult> {
    //   try {
    //     const result = await this._customerService.updateUserProfile(
    //       req,
    //       reqIncomingMessage.user.user_id
    //     );
    //     return {
    //       code: Enums.responseCode.Success,
    //       message: Enums.responseMessage.Success,
    //       result: result,
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // @Put("/address")
    // public async updateUserAddress(
    //   @Body() req: CustomerModels.RequestUpdateCustomerAddress,
    //   @Request() reqIncomingMessage: any
    // ): Promise<BaseModels.ResponseResult> {
    //   try {
    //     const result = await this._customerService.updateUserAddress(
    //       req,
    //       reqIncomingMessage.user.user_id
    //     );
    //     return {
    //       code: Enums.responseCode.Success,
    //       message: Enums.responseMessage.Success,
    //       result: result,
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    getNetworksByUserId(id, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getNetworksByUserId(id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetsByUserIdNetworkId(id, network_id, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getAssetsByUserIdNetworkId(id, network_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
};
__decorate([
    (0, tsoa_1.Get)("/api-key/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getUserApiKeys", null);
__decorate([
    (0, tsoa_1.Get)("/api-key/{id}"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getUserApiKeyById", null);
__decorate([
    (0, tsoa_1.Post)("/api-key/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "createUserApiKey", null);
__decorate([
    (0, tsoa_1.Put)("/api-key/disable/{id}"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "disableUserApiKey", null);
__decorate([
    (0, tsoa_1.Put)("/api-key/all/disable"),
    __param(0, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "disableAllUserApiKeyByUserId", null);
__decorate([
    (0, tsoa_1.Post)("/account/asset/request"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "requestAccessAsset", null);
__decorate([
    (0, tsoa_1.Get)("/fiat-account/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getUserFiatAccounts", null);
__decorate([
    (0, tsoa_1.Get)("/bank-account/country/list"),
    __param(0, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getBankAccountCoutries", null);
__decorate([
    (0, tsoa_1.Get)("/bank-account/owner/list"),
    __param(0, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getOwnerBankAccounts", null);
__decorate([
    (0, tsoa_1.Get)("/bank-account/other/list"),
    __param(0, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getOtherBankAccounts", null);
__decorate([
    (0, tsoa_1.Get)("/bank-account/{id}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getBankAccountById", null);
__decorate([
    (0, tsoa_1.Post)("/bank-account/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "createBankAccount", null);
__decorate([
    (0, tsoa_1.Delete)("/bank-account/{id}"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "deleteBankAccount", null);
__decorate([
    (0, tsoa_1.Get)("/crypto-wallet/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getCryptoWalletListForUser", null);
__decorate([
    (0, tsoa_1.Get)("/crypto-wallet/{id}/total/{asset_group}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getTotalAssetForUser", null);
__decorate([
    (0, tsoa_1.Post)("/transaction/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "createTransaction", null);
__decorate([
    (0, tsoa_1.Get)("/withdraw/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getWithdraws", null);
__decorate([
    (0, tsoa_1.Get)("/withdraw/{id}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getWithdrawById", null);
__decorate([
    (0, tsoa_1.Post)("/withdraw/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "createWithdrawTransaction", null);
__decorate([
    (0, tsoa_1.Post)("/transfer/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "createTransferTransaction", null);
__decorate([
    (0, tsoa_1.Get)("/transaction/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getTransactions", null);
__decorate([
    (0, tsoa_1.Get)("/profile"),
    __param(0, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getUserProfile", null);
__decorate([
    (0, tsoa_1.Get)("/{id}/network/list"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getNetworksByUserId", null);
__decorate([
    (0, tsoa_1.Get)("/{id}/network/{network_id}/asset/list"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Path)()),
    __param(2, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, Object]),
    __metadata("design:returntype", Promise)
], CustomerController.prototype, "getAssetsByUserIdNetworkId", null);
exports.CustomerController = CustomerController = __decorate([
    (0, tsoa_1.Tags)("Customers"),
    (0, tsoa_1.Security)("api_key"),
    (0, tsoa_1.Route)("/api/v1/customer"),
    (0, tsyringe_1.injectable)(),
    __metadata("design:paramtypes", [customerService_1.CustomerService])
], CustomerController);
//# sourceMappingURL=customerController.js.map