"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminController = void 0;
const tsoa_1 = require("tsoa");
const Enums = __importStar(require("../utilities/enums"));
const AdminModels = __importStar(require("../models/adminModels"));
const adminService_1 = require("../services/adminService");
const tsyringe_1 = require("tsyringe");
const customerService_1 = require("../services/customerService");
let AdminController = exports.AdminController = class AdminController extends tsoa_1.Controller {
    constructor(adminService, customerService) {
        super();
        this._adminService = adminService;
        this._customerService = customerService;
    }
    getUsers(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getUsers();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserActiveList(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getUserAcriveList();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserById(user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getUserById(user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createUser(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.createUser(req.password, req.first_name, req.middle_name, req.last_name, req.email, req.tel, req.status, req.role_id, reqIncomingMessage.user.user_id, req.gender, req.nationality, req.occupation, req.date_of_birth, {
                    line1: req.address.line1,
                    line2: req.address !== undefined ? req.address.line2 : undefined,
                    city: req.address.city,
                    state: req.address.state,
                    countryCode: req.address.country_code,
                    postcode: req.address.postcode,
                    areaTown: req.address !== undefined ? req.address.area_town : undefined,
                }, req.document_type, req.document_text, req.country_code, req.occupation_remark, req.document_type_remark, req.document_issue_date, req.document_expired_date);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateUser(user_id, req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.updateUser(user_id, req.first_name, req.middle_name, req.last_name, req.tel, req.status, req.role_id, reqIncomingMessage.user.user_id, req.gender, req.nationality, req.occupation, req.date_of_birth, {
                    line1: req.address.line1,
                    line2: req.address !== undefined ? req.address.line2 : undefined,
                    city: req.address.city,
                    state: req.address.state,
                    countryCode: req.address.country_code,
                    postcode: req.address.postcode,
                    areaTown: req.address !== undefined ? req.address.area_town : undefined,
                }, req.document_type, req.document_text, req.country_code, req.occupation_remark, req.document_type_remark, req.document_issue_date, req.document_expired_date);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    deleteUser(user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.deleteUser(user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getRoles(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getRoles();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getActiveRoles(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getActiveRoles();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getRoleById(role_id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getRoleById(role_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createRole(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.createRole(req.name, req.description, req.status, req.menus, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateRole(role_id, req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.updateRole(role_id, req.name, req.description, req.status, req.menus, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    deleteRole(role_id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.deleteRole(role_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getMenuByUserId(reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getMenuByUserId(reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    changePassword(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.changePassword(reqIncomingMessage.user.user_id, req.old_password, req.new_password, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserAssetInVaultAccountByUserId(user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getUserAssetInVaultAccountByUserId(user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    resetPassword(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.resetPassword(req.user_id, req.new_password, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateRoleMenuByRoleId(role_id, req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.updateRoleMenuByRoleId(role_id, req.group_menu_code, req.menu_code, req.permission, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getMasterMenu(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getMasterMenu();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCryptoWalletList(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getCryptoWalletList();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    // @Get("/company/crypto-wallet/total/{asset_group}")
    // public async getTotalAssetPieChart(
    //   @Path() asset_group: string,
    //   @Queries() req: AdminModels.RequestGetTotalAssetPieChart
    // ): Promise<BaseModels.ResponseResult> {
    //   try {
    //     const result = await this._adminService.getTotalAssetPieChart(
    //       asset_group
    //     );
    //     return {
    //       code: Enums.responseCode.Success,
    //       message: Enums.responseMessage.Success,
    //       result: result,
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // @Get("/company/fiat/total")
    // public async getTotaFiatPieChart(
    //   @Queries() req: AdminModels.RequestGetTotalFiatPieChart
    // ): Promise<BaseModels.ResponseResult> {
    //   try {
    //     const result = await this._adminService.getTotalFiatPieChart();
    //     return {
    //       code: Enums.responseCode.Success,
    //       message: Enums.responseMessage.Success,
    //       result: result,
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    createTransaction(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.createTransaction(req.user_id, req.type, req.asset_code, req.total_assets, req.total_amount, req.input_type, req.is_manual_mode, req.wallet_network, req.wallet_address, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getFiatAccounts() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getFiatAccounts();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getFiatAccountByUser(user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getFiatAccountByUser(user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateFavoriteFiatAccounts(fiat_id, req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.updateFavoriteFiatAccount(fiat_id, req.is_favorite, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createTopupTransaction(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var vDocuments = [];
                const allowedTypes = [
                    "image/jpeg",
                    "image/jpg",
                    "image/png",
                    "image/gif",
                    "application/pdf",
                ];
                const maxSize = 10 * 1024 * 1024; // 10MB
                if (reqIncomingMessage.files !== null) {
                    if (reqIncomingMessage.files.file_path.name === undefined) {
                        var totalSize = 0;
                        for (let i = 0; i < reqIncomingMessage.files.file_path.length; i++) {
                            const element = reqIncomingMessage.files.file_path[i];
                            totalSize += element.size;
                            if (!allowedTypes.includes(element.mimetype)) {
                                return {
                                    code: Enums.responseCode.UploadFileInvalidFileType,
                                    message: Enums.responseMessage.UploadFileInvalidFileType +
                                        ": " +
                                        element.name,
                                };
                            }
                            if (totalSize > maxSize) {
                                return {
                                    code: Enums.responseCode.UploadFileFileTooLarge,
                                    message: Enums.responseMessage.UploadFileFileTooLarge,
                                };
                            }
                            vDocuments.push({
                                file_name: element.name,
                                file_data: element.data,
                            });
                        }
                    }
                    else {
                        if (!allowedTypes.includes(reqIncomingMessage.files.file_path.mimetype)) {
                            return {
                                code: Enums.responseCode.UploadFileInvalidFileType,
                                message: Enums.responseMessage.UploadFileInvalidFileType +
                                    ": " +
                                    reqIncomingMessage.files.file_path.name,
                            };
                        }
                        if (reqIncomingMessage.files.file_path.size > maxSize) {
                            return {
                                code: Enums.responseCode.UploadFileFileTooLarge,
                                message: Enums.responseMessage.UploadFileFileTooLarge,
                            };
                        }
                        vDocuments.push({
                            file_name: reqIncomingMessage.files.file_path.name,
                            file_data: reqIncomingMessage.files.file_path.data,
                        });
                    }
                }
                const result = yield this._adminService.createTopup(req.fiat_id, req.amount, vDocuments, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTopupInfo(wallet_credit_id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getTopupInfo(wallet_credit_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    downloadWalletCreditDocument(wallet_credit_document_id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.downloadWalletCreditDocument(wallet_credit_document_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    previewWalletCreditDocument(wallet_credit_document_id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.previewWalletCreditDocument(wallet_credit_document_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getOwnerBankAccounts(user_id, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getBankAccountsByBankType(user_id, Enums.bankAccountType.OWNER);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getOtherBankAccounts(user_id, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.getBankAccountsByBankType(user_id, Enums.bankAccountType.OTHER);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createWithdrawTransaction(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.createWithdraw(req.sender_source_of_fund, req.sender_source_of_fund_remarks, req.purpose_of_remittance, req.purpose_of_remittance_remarks, req.sender_beneficiary_relationship, req.sender_beneficiary_relationship_remarks, req.transfer_amount, req.bank_account_id, req.user_id, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createTransferTransaction(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._adminService.createTransfer(req.sender_source_of_fund, req.sender_source_of_fund_remarks, req.purpose_of_remittance, req.purpose_of_remittance_remarks, req.sender_beneficiary_relationship, req.sender_beneficiary_relationship_remarks, req.transfer_amount, req.bank_account_id, req.user_id, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createBankAccount(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const resultCreated = yield this._customerService.createBankAccount(req.bank_type, req.receiver_first_name, req.receiver_middle_name, req.receiver_last_name, req.receiver_address, req.receiver_date_of_birth, req.receiver_gender, req.receiver_contact_number, req.receiver_state, req.receiver_area_town, req.receiver_city, req.receiver_zip_code, req.receiver_country, req.receiver_nationality, req.receiver_id_type, req.receiver_id_type_remarks, req.receiver_id_number, req.receiver_id_issue_date, req.receiver_id_expire_date, req.receiver_email, req.receiver_account_type, req.receiver_occupation, req.receiver_occupation_remarks, req.receiver_district, req.beneficiary_type, req.location_id, req.bank_name, req.bank_branch_name, req.bank_branch_code, req.bank_account_number, req.swift_code, req.iban, req.user_id, reqIncomingMessage.user.user_id);
                const result = yield this._customerService.getBankAccountById(resultCreated);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
};
__decorate([
    (0, tsoa_1.Get)("/user/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getUsers", null);
__decorate([
    (0, tsoa_1.Get)("/user/active/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getUserActiveList", null);
__decorate([
    (0, tsoa_1.Get)("/user/{user_id}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getUserById", null);
__decorate([
    (0, tsoa_1.Post)("/user/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "createUser", null);
__decorate([
    (0, tsoa_1.Put)("/user/{user_id}"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Body)()),
    __param(2, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "updateUser", null);
__decorate([
    (0, tsoa_1.Delete)("/user/{user_id}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "deleteUser", null);
__decorate([
    (0, tsoa_1.Get)("/role/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getRoles", null);
__decorate([
    (0, tsoa_1.Get)("/active-role/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getActiveRoles", null);
__decorate([
    (0, tsoa_1.Get)("/role/{role_id}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getRoleById", null);
__decorate([
    (0, tsoa_1.Post)("/role/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "createRole", null);
__decorate([
    (0, tsoa_1.Put)("/role/{role_id}"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Body)()),
    __param(2, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "updateRole", null);
__decorate([
    (0, tsoa_1.Delete)("/role/{role_id}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "deleteRole", null);
__decorate([
    (0, tsoa_1.Get)("/user/menu/list"),
    __param(0, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getMenuByUserId", null);
__decorate([
    (0, tsoa_1.Post)("/user/change-password"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "changePassword", null);
__decorate([
    (0, tsoa_1.Get)("/user/{user_id}/asset/list"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getUserAssetInVaultAccountByUserId", null);
__decorate([
    (0, tsoa_1.Post)("/user/reset-password"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "resetPassword", null);
__decorate([
    (0, tsoa_1.Put)("/role/{role_id}/menu"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Body)()),
    __param(2, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "updateRoleMenuByRoleId", null);
__decorate([
    (0, tsoa_1.Get)("/master-menu/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getMasterMenu", null);
__decorate([
    (0, tsoa_1.Get)("/company/crypto-wallet/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getCryptoWalletList", null);
__decorate([
    (0, tsoa_1.Post)("/transaction/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "createTransaction", null);
__decorate([
    (0, tsoa_1.Get)("/fiat-account/list"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getFiatAccounts", null);
__decorate([
    (0, tsoa_1.Get)("/fiat-account/user/{user_id}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getFiatAccountByUser", null);
__decorate([
    (0, tsoa_1.Put)("/fiat-account/{fiat_id}/favorite"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Body)()),
    __param(2, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "updateFavoriteFiatAccounts", null);
__decorate([
    (0, tsoa_1.Post)("/fiat-account/topup"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "createTopupTransaction", null);
__decorate([
    (0, tsoa_1.Get)("/fiat-account/topup/{wallet_credit_id}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getTopupInfo", null);
__decorate([
    (0, tsoa_1.Get)("/fiat-account/wallet-credit-document/{wallet_credit_document_id}/download"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "downloadWalletCreditDocument", null);
__decorate([
    (0, tsoa_1.Get)("/fiat-account/wallet-credit-document/{wallet_credit_document_id}/preview"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "previewWalletCreditDocument", null);
__decorate([
    (0, tsoa_1.Get)("/bank-account/user/{user_id}/owner/list"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getOwnerBankAccounts", null);
__decorate([
    (0, tsoa_1.Get)("/bank-account/user/{user_id}/other/list"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "getOtherBankAccounts", null);
__decorate([
    (0, tsoa_1.Post)("/withdraw/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "createWithdrawTransaction", null);
__decorate([
    (0, tsoa_1.Post)("/transfer/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "createTransferTransaction", null);
__decorate([
    (0, tsoa_1.Post)("/bank-account/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AdminController.prototype, "createBankAccount", null);
exports.AdminController = AdminController = __decorate([
    (0, tsoa_1.Tags)("Administration"),
    (0, tsoa_1.Security)("api_key"),
    (0, tsoa_1.Route)("/api/v1/admin"),
    (0, tsyringe_1.injectable)(),
    __metadata("design:paramtypes", [adminService_1.AdminService, customerService_1.CustomerService])
], AdminController);
//# sourceMappingURL=adminController.js.map