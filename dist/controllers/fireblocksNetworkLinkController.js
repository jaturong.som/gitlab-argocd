"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FireblocksNetworkLinkController = void 0;
const tsoa_1 = require("tsoa");
const fireblocksNetworkLinkService_1 = require("../services/fireblocksNetworkLinkService");
const FireblocksNetworkLinkModels = __importStar(require("../models/fireblocksNetworkLinkModels"));
const tsyringe_1 = require("tsyringe");
let FireblocksNetworkLinkController = exports.FireblocksNetworkLinkController = class FireblocksNetworkLinkController extends tsoa_1.Controller {
    constructor(fireblocksNetworkLinkService) {
        super();
        this._fireblocksNetworkLinkService = fireblocksNetworkLinkService;
    }
    getAllAccountTypes(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkService.getAllAccountTypes(reqIncomingMessage.user.user_id);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getDepositWalletAddress(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkService.getDepositWalletAddress(reqIncomingMessage.user.user_id, req.accountType, req.coinSymbol, req.network);
            }
            catch (error) {
                throw error;
            }
        });
    }
    createDepositWalletAddress(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkService.createDepositWalletAddress(reqIncomingMessage.user.user_id, req.accountType, req.coinSymbol, req.network);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWithDrawalFee(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkService.getWithDrawalFee(req.transferAmount, req.coinSymbol, req.network);
            }
            catch (error) {
                throw error;
            }
        });
    }
    createWithDraw(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkService.createWithDraw(reqIncomingMessage.user.user_id, req.amount, req.maxFee, req.coinSymbol, req.network, req.accountType, req.toAddress, req.tag, req.isGross);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionById(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkService.getTransactionById(req.transactionID);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionByHash(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkService.getTransactionByHash(req.txHash, req.network);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionHistory(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkService.getTransactionHistory(req.fromDate, req.toDate, req.pageSize, req.pageCursor, req.isSubTransfer, req.coinSymbol, req.network, reqIncomingMessage.user.user_id);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getSupportedAssets(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkService.getSupportedAssets();
            }
            catch (error) {
                throw error;
            }
        });
    }
    createSubMainTransfer(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkService.createSubMainTransfer();
            }
            catch (error) {
                throw error;
            }
        });
    }
    createSubAccountsTransfer(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkService.createSubAccountsTransfer();
            }
            catch (error) {
                throw error;
            }
        });
    }
    createInternalTransfer(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkService.createInternalTransfer();
            }
            catch (error) {
                throw error;
            }
        });
    }
};
__decorate([
    (0, tsoa_1.Get)("/accounts"),
    __param(0, (0, tsoa_1.Queries)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], FireblocksNetworkLinkController.prototype, "getAllAccountTypes", null);
__decorate([
    (0, tsoa_1.Get)("/depositAddress"),
    __param(0, (0, tsoa_1.Queries)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], FireblocksNetworkLinkController.prototype, "getDepositWalletAddress", null);
__decorate([
    (0, tsoa_1.Post)("/depositAddress"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], FireblocksNetworkLinkController.prototype, "createDepositWalletAddress", null);
__decorate([
    (0, tsoa_1.Get)("/withdrawalFee"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FireblocksNetworkLinkController.prototype, "getWithDrawalFee", null);
__decorate([
    (0, tsoa_1.Post)("/withdraw"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], FireblocksNetworkLinkController.prototype, "createWithDraw", null);
__decorate([
    (0, tsoa_1.Get)("/transactionByID"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FireblocksNetworkLinkController.prototype, "getTransactionById", null);
__decorate([
    (0, tsoa_1.Get)("/transactionByHash"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FireblocksNetworkLinkController.prototype, "getTransactionByHash", null);
__decorate([
    (0, tsoa_1.Get)("/transactionHistory"),
    __param(0, (0, tsoa_1.Queries)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], FireblocksNetworkLinkController.prototype, "getTransactionHistory", null);
__decorate([
    (0, tsoa_1.Get)("/supportedAssets"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FireblocksNetworkLinkController.prototype, "getSupportedAssets", null);
__decorate([
    (0, tsoa_1.Post)("/subMainTransfer"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FireblocksNetworkLinkController.prototype, "createSubMainTransfer", null);
__decorate([
    (0, tsoa_1.Post)("/subaccountsTransfer"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FireblocksNetworkLinkController.prototype, "createSubAccountsTransfer", null);
__decorate([
    (0, tsoa_1.Post)("/internalTransfer"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FireblocksNetworkLinkController.prototype, "createInternalTransfer", null);
exports.FireblocksNetworkLinkController = FireblocksNetworkLinkController = __decorate([
    (0, tsoa_1.Tags)("Fireblocks Network Link"),
    (0, tsoa_1.Security)("network_link"),
    (0, tsoa_1.Route)("/fireblocks/v1"),
    (0, tsyringe_1.injectable)(),
    __metadata("design:paramtypes", [fireblocksNetworkLinkService_1.FireblocksNetworkLinkService])
], FireblocksNetworkLinkController);
//# sourceMappingURL=fireblocksNetworkLinkController.js.map