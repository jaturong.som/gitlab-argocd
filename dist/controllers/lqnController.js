"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LQNController = void 0;
const tsoa_1 = require("tsoa");
const Enums = __importStar(require("../utilities/enums"));
const LqnModels = __importStar(require("../models/lqnModels"));
const tsyringe_1 = require("tsyringe");
const lqnService_1 = require("../services/lqnService");
const lqnWithdrawService_1 = require("../services/lqnWithdrawService");
let LQNController = exports.LQNController = class LQNController extends tsoa_1.Controller {
    constructor(lqnService, lqnWithdrawService) {
        super();
        this._lqnService = lqnService;
        this._lqnWithdrawService = lqnWithdrawService;
    }
    getPurposeTransfer(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnService.getCatalogue("POR", "", "", "");
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getRelationToReceiver(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnService.getCatalogue("REL", "", "", "");
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getSourceOfFund(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnService.getCatalogue("SOF", "", "", "");
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getOccupation(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnService.getCatalogue("OCC", "", "", "");
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getDocumentType(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnService.getCatalogue("DOC", "", "", "");
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getNationality(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnWithdrawService.getNationalityList();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCountry(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnWithdrawService.getCoutryList();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAgentList(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnWithdrawService.getBankList("B", req.country_code);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCatalogue(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnService.getCatalogue(req.catalogue_type, req.additional_field_1, "", "");
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getExchangeRate(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnService.getExchangeRate(req.transfer_amount, req.calc_by, req.payout_currency, req.payment_mode, req.bank_id, req.payout_country);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    sendTransaction(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnService.sendTransaction(req.sender_source_of_fund, req.sender_source_of_fund_remarks, req.purpose_of_remittance, req.purpose_of_remittance_remarks, req.sender_beneficiary_relationship, req.sender_beneficiary_relationship_remarks, req.calc_by, req.transfer_amount, req.remit_currency, req.customer_id, req.receiver_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    commitTransaction(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnService.commitTransaction(req.transaction_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    queryTransaction(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnService.queryTransaction(req.pin_number);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    callback(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._lqnWithdrawService.callback(req.agentTxnId, req.pinNumber, req.status, req.message, req.notification_ts);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
};
__decorate([
    (0, tsoa_1.Get)("/dropdown/purpose-of-transfer"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "getPurposeTransfer", null);
__decorate([
    (0, tsoa_1.Get)("/dropdown/relationship-to-receiver"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "getRelationToReceiver", null);
__decorate([
    (0, tsoa_1.Get)("/dropdown/source-of-fund"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "getSourceOfFund", null);
__decorate([
    (0, tsoa_1.Get)("/dropdown/occupation"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "getOccupation", null);
__decorate([
    (0, tsoa_1.Get)("/dropdown/document-type"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "getDocumentType", null);
__decorate([
    (0, tsoa_1.Get)("/dropdown/nationality"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "getNationality", null);
__decorate([
    (0, tsoa_1.Get)("/dropdown/country"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "getCountry", null);
__decorate([
    (0, tsoa_1.Get)("/dropdown/bank"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "getAgentList", null);
__decorate([
    (0, tsoa_1.Get)("/catalogue"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "getCatalogue", null);
__decorate([
    (0, tsoa_1.Get)("/exchangerate"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "getExchangeRate", null);
__decorate([
    (0, tsoa_1.Post)("/sendTransaction"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "sendTransaction", null);
__decorate([
    (0, tsoa_1.Post)("/commitTransaction"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "commitTransaction", null);
__decorate([
    (0, tsoa_1.Get)("/queryTransaction"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "queryTransaction", null);
__decorate([
    (0, tsoa_1.Post)("/callback"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LQNController.prototype, "callback", null);
exports.LQNController = LQNController = __decorate([
    (0, tsoa_1.Tags)("LQN"),
    (0, tsoa_1.Route)("/api/v1/lqn"),
    (0, tsyringe_1.injectable)(),
    __metadata("design:paramtypes", [lqnService_1.LqnService, lqnWithdrawService_1.LqnWithdrawService])
], LQNController);
//# sourceMappingURL=lqnController.js.map