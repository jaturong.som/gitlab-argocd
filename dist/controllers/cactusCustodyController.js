"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CactusCustodyController = void 0;
const tsoa_1 = require("tsoa");
const CactusCustodyModels = __importStar(require("../models/cactusCustodyModels"));
const cactusCustodyService_1 = require("../services/cactusCustodyService");
const tsyringe_1 = require("tsyringe");
let CactusCustodyController = exports.CactusCustodyController = class CactusCustodyController extends tsoa_1.Controller {
    constructor(cactusCustodyService) {
        super();
        this._cactusCustodyService = cactusCustodyService;
    }
    getWallets() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getWalletList();
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWalletInfo(wallet_code) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getWalletInfo(process.env.BID_CUSTOMER, wallet_code);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCoinInfo() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getCoinInfo();
            }
            catch (error) {
                throw error;
            }
        });
    }
    getChainInfo() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getChainInfo();
            }
            catch (error) {
                throw error;
            }
        });
    }
    createWallet(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.createWallet(process.env.BID_CUSTOMER, req.wallet_type, req.number);
            }
            catch (error) {
                throw error;
            }
        });
    }
    applyNewAddress(wallet_code, req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.applyNewAddress(process.env.BID_CUSTOMER, wallet_code, req.address_num, req.address_type, req.coin_name);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAddressInfo(wallet_code, address) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getAddressInfo(process.env.BID_CUSTOMER, wallet_code, address);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAddressList(wallet_code) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getAddressList(process.env.BID_CUSTOMER, wallet_code);
            }
            catch (error) {
                throw error;
            }
        });
    }
    editAddress(wallet_code, address, req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.editAddress(process.env.BID_CUSTOMER, wallet_code, address, req.description);
            }
            catch (error) {
                throw error;
            }
        });
    }
    verifyAddressFormat(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.verifyAddressFormat(req.addresses, req.coin_name);
            }
            catch (error) {
                throw error;
            }
        });
    }
    estimateGasFee(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.estimateGasFee(process.env.BID_CUSTOMER, req.from_address, req.from_wallet_code, req.description, req.amount, req.dest_address, req.memo_type, req.memo, req.remark);
            }
            catch (error) {
                throw error;
            }
        });
    }
    createWithdrawalOrder(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.createWithdrawalOrder(process.env.BID_CUSTOMER, req.from_address, req.from_wallet_code, req.coin_name, req.description, req.amount, req.dest_address, req.memo_type, req.memo, req.remark);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWithdrawalRateRange() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getWithdrawalRateRange();
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWithdrawalRate() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getWithdrawalRate();
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWalletTransactionSummary(wallet_code) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getWalletTransactionSummary(process.env.BID_CUSTOMER, wallet_code);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionDetails(wallet_code) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getTransactionDetails(process.env.BID_CUSTOMER, wallet_code);
            }
            catch (error) {
                throw error;
            }
        });
    }
    editTransactionDetailRemark(wallet_code, id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.editTransactionDetailRemark(process.env.BID_CUSTOMER, wallet_code, id, req.remark);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionDetailsForCustomCurrency(wallet_code) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getTransactionDetailsForCustomCurrency(process.env.BID_CUSTOMER, wallet_code);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTotalAssetHistoryNotionalValue() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getTotalAssetHistoryNotionalValue();
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCurrentAssetNotionalValue() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getCurrentAssetNotionalValue();
            }
            catch (error) {
                throw error;
            }
        });
    }
    getOrdersFilterByCriteria() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getOrdersFilterByCriteria(process.env.BID_CUSTOMER);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getOrderDetails(order_no) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.getOrderDetails(process.env.BID_CUSTOMER, order_no);
            }
            catch (error) {
                throw error;
            }
        });
    }
    rbf(order_no, req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.rbf(process.env.BID_CUSTOMER, order_no, req.level);
            }
            catch (error) {
                throw error;
            }
        });
    }
    cancelOrder(order_no, req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._cactusCustodyService.cancelOrder(process.env.BID_CUSTOMER, order_no, req.level);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
__decorate([
    (0, tsoa_1.Get)("/wallet/list"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getWallets", null);
__decorate([
    (0, tsoa_1.Get)("/wallet/{wallet_code}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getWalletInfo", null);
__decorate([
    (0, tsoa_1.Get)("/coin-infos"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getCoinInfo", null);
__decorate([
    (0, tsoa_1.Get)("/chain-infos"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getChainInfo", null);
__decorate([
    (0, tsoa_1.Post)("/wallet/create"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "createWallet", null);
__decorate([
    (0, tsoa_1.Post)("/wallet/{wallet_code}/addresses/apply"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "applyNewAddress", null);
__decorate([
    (0, tsoa_1.Get)("/wallet/{wallet_code}/addresses/{address}"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getAddressInfo", null);
__decorate([
    (0, tsoa_1.Get)("/wallet/{wallet_code}/addresses"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getAddressList", null);
__decorate([
    (0, tsoa_1.Put)("/wallet/{wallet_code}/addresses/{address}"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Path)()),
    __param(2, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Object]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "editAddress", null);
__decorate([
    (0, tsoa_1.Post)("/addresses/type/check"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "verifyAddressFormat", null);
__decorate([
    (0, tsoa_1.Post)("/estimate-miner-fee"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "estimateGasFee", null);
__decorate([
    (0, tsoa_1.Post)("/order/create"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "createWithdrawalOrder", null);
__decorate([
    (0, tsoa_1.Get)("/customize-fee-rate/range"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getWithdrawalRateRange", null);
__decorate([
    (0, tsoa_1.Get)("/recommend-fee-rate/list"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getWithdrawalRate", null);
__decorate([
    (0, tsoa_1.Get)("/wallets/{wallet_code}/tx-summaries"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getWalletTransactionSummary", null);
__decorate([
    (0, tsoa_1.Get)("/wallets/{wallet_code}/tx-details"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getTransactionDetails", null);
__decorate([
    (0, tsoa_1.Post)("/wallets/{wallet_code}/details/{id}"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Path)()),
    __param(2, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Number, Object]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "editTransactionDetailRemark", null);
__decorate([
    (0, tsoa_1.Get)("/wallets/{wallet_code}/customize-flow"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getTransactionDetailsForCustomCurrency", null);
__decorate([
    (0, tsoa_1.Get)("/history-asset"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getTotalAssetHistoryNotionalValue", null);
__decorate([
    (0, tsoa_1.Get)("/asset"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getCurrentAssetNotionalValue", null);
__decorate([
    (0, tsoa_1.Get)("/orders"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getOrdersFilterByCriteria", null);
__decorate([
    (0, tsoa_1.Get)("/orders/{order_no}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "getOrderDetails", null);
__decorate([
    (0, tsoa_1.Post)("/orders/{order_no}/accelerate"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "rbf", null);
__decorate([
    (0, tsoa_1.Post)("/orders/{order_no}/cancel"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], CactusCustodyController.prototype, "cancelOrder", null);
exports.CactusCustodyController = CactusCustodyController = __decorate([
    (0, tsoa_1.Tags)("Cactus Custody"),
    (0, tsoa_1.Security)("api_key"),
    (0, tsoa_1.Route)("/api/v1/cactus"),
    (0, tsyringe_1.injectable)(),
    __metadata("design:paramtypes", [cactusCustodyService_1.CactusCustodyService])
], CactusCustodyController);
//# sourceMappingURL=cactusCustodyController.js.map