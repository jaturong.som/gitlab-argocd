"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigController = void 0;
const tsoa_1 = require("tsoa");
const Enums = __importStar(require("../utilities/enums"));
const ConfigModels = __importStar(require("../models/configModels"));
const configService_1 = require("../services/configService");
const tsyringe_1 = require("tsyringe");
let ConfigController = exports.ConfigController = class ConfigController extends tsoa_1.Controller {
    constructor(configService) {
        super();
        this._configService = configService;
    }
    getAssets(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.getAssetsInVaultAccount();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createAsset(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.createAssetInVaultAccount(req.asset_code, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetRates(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.getAssetRates();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetRateById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.getAssetRateById(id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetRateByAssetCode(asset_code) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.getAssetRateByAssetCode(asset_code);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createAssetRate(req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.createAssetRate(req.asset_code, req.buying, req.selling, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateAssetRate(id, req, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.updateAssetRate(id, req.buying, req.selling, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    deleteAssetRate(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.deleteAssetRate(id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getNetworks(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.getNetworks();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getFBAssets(network_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.getAssets(network_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getPurposeOfTransfer() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.getPurposeOfTransfer();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getRelationshipToReceiver() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.getRelationshipToReceiver();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getDropdownListByType(type) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._configService.getDropdownListByType(type);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
};
__decorate([
    (0, tsoa_1.Get)("/asset/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "getAssets", null);
__decorate([
    (0, tsoa_1.Post)("/asset/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "createAsset", null);
__decorate([
    (0, tsoa_1.Get)("/asset-rate/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "getAssetRates", null);
__decorate([
    (0, tsoa_1.Get)("/asset-rate/{id}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "getAssetRateById", null);
__decorate([
    (0, tsoa_1.Get)("/asset-rate-by-code/{asset_code}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "getAssetRateByAssetCode", null);
__decorate([
    (0, tsoa_1.Post)("/asset-rate/create"),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "createAssetRate", null);
__decorate([
    (0, tsoa_1.Put)("/asset-rate/{id}"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Body)()),
    __param(2, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object, Object]),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "updateAssetRate", null);
__decorate([
    (0, tsoa_1.Delete)("/asset-rate/{id}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "deleteAssetRate", null);
__decorate([
    (0, tsoa_1.Get)("/network/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "getNetworks", null);
__decorate([
    (0, tsoa_1.Get)("/fb-asset/list/{network_id}"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "getFBAssets", null);
__decorate([
    (0, tsoa_1.Get)("/dropdown/purpose-of-transfer"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "getPurposeOfTransfer", null);
__decorate([
    (0, tsoa_1.Get)("/dropdown/relationship-to-receiver"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "getRelationshipToReceiver", null);
__decorate([
    (0, tsoa_1.Get)("/dropdown/{type}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ConfigController.prototype, "getDropdownListByType", null);
exports.ConfigController = ConfigController = __decorate([
    (0, tsoa_1.Tags)("Configurations"),
    (0, tsoa_1.Security)("api_key"),
    (0, tsoa_1.Route)("/api/v1/config"),
    (0, tsyringe_1.injectable)(),
    __metadata("design:paramtypes", [configService_1.ConfigService])
], ConfigController);
//# sourceMappingURL=configController.js.map