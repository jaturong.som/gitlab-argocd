"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionController = void 0;
const tsoa_1 = require("tsoa");
const Enums = __importStar(require("../utilities/enums"));
const TransactionModels = __importStar(require("../models/transactionModels"));
const transactionService_1 = require("../services/transactionService");
const tsyringe_1 = require("tsyringe");
let TransactionController = exports.TransactionController = class TransactionController extends tsoa_1.Controller {
    constructor(transactionService) {
        super();
        this._transactionService = transactionService;
    }
    getAdminTransactions(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._transactionService.getAdminTransactions();
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    // @Get("/admin/logs")
    // public async getAdminTransactionLogs(
    //   @Queries() req: TransactionModels.RequestGetAdminTransactionLogs
    // ): Promise<BaseModels.ResponseResult> {
    //   try {
    //     const result = await this._transactionService.getAdminTransactionLogs(
    //       req.page_size,
    //       req.page_cursor,
    //       req.from_date!,
    //       req.to_date!,
    //       req.transaction_type!,
    //       req.status!,
    //       req.asset!
    //     );
    //     return {
    //       code: Enums.responseCode.Success,
    //       message: Enums.responseMessage.Success,
    //       result: result,
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    getFireblocksNotificationList(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._transactionService.getFireblocksNotificationList(req.from_date, req.to_date, req.status, req.search_text);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    submitBuySellTransaction(id, reqIncomingMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._transactionService.submitBuySellTransaction(id, reqIncomingMessage.user.user_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    manualUpdateTransactionStatus(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._transactionService.manualUpdateTransactionStatus(req.fireblock_transaction_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    manualFreezeTransaction(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._transactionService.manualFreezeTransaction(req.fb_transaction_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    manualUnfreezeTransaction(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._transactionService.manualUnfreezeTransaction(req.fb_transaction_id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWithdrawById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._transactionService.getWithdrawById(id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getConvertById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._transactionService.getConvertById(id);
                return {
                    code: Enums.responseCode.Success,
                    message: Enums.responseMessage.Success,
                    result: result,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
};
__decorate([
    (0, tsoa_1.Get)("/admin/list"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "getAdminTransactions", null);
__decorate([
    (0, tsoa_1.Get)("/admin/fireblocks/notifications"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "getFireblocksNotificationList", null);
__decorate([
    (0, tsoa_1.Put)("/admin/transaction/{id}/submit"),
    __param(0, (0, tsoa_1.Path)()),
    __param(1, (0, tsoa_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "submitBuySellTransaction", null);
__decorate([
    (0, tsoa_1.Post)("/manual-update-transaction-status"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "manualUpdateTransactionStatus", null);
__decorate([
    (0, tsoa_1.Post)("/manual/freeze"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "manualFreezeTransaction", null);
__decorate([
    (0, tsoa_1.Post)("/manual/unfreeze"),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "manualUnfreezeTransaction", null);
__decorate([
    (0, tsoa_1.Get)("/admin/withdraw/{id}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "getWithdrawById", null);
__decorate([
    (0, tsoa_1.Get)("/admin/convert/{id}"),
    __param(0, (0, tsoa_1.Path)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "getConvertById", null);
exports.TransactionController = TransactionController = __decorate([
    (0, tsoa_1.Tags)("Transactions"),
    (0, tsoa_1.Security)("api_key"),
    (0, tsoa_1.Route)("/api/v1/transaction"),
    (0, tsyringe_1.injectable)(),
    __metadata("design:paramtypes", [transactionService_1.TransactionService])
], TransactionController);
//# sourceMappingURL=transactionController.js.map