"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CronJobService = void 0;
const cron_1 = require("cron");
const assetRepository_1 = require("../repositories/assetRepository");
const jobSchedulerLogRepository_1 = require("../repositories/jobSchedulerLogRepository");
const chainalysisTransactionRepository_1 = require("../repositories/chainalysisTransactionRepository");
const fetchLogRepository_1 = require("../repositories/fetchLogRepository");
const vaultService_1 = require("../services/vaultService");
const fireblocksService_1 = require("../services/fireblocksService");
const httpService_1 = require("../services/httpService");
const chainalysisService_1 = require("../services/chainalysisService");
const enums = __importStar(require("../utilities/enums"));
const tsyringe_1 = require("tsyringe");
let CronJobService = exports.CronJobService = class CronJobService {
    constructor() {
        this._assetRepository = new assetRepository_1.AssetRepository();
        this._jobSchedulerLogRepository = new jobSchedulerLogRepository_1.JobSchedulerLogRepository();
        this._chainalysisTransactionRepository =
            new chainalysisTransactionRepository_1.ChainalysisTransactionRepository();
        this._fetchLogRepository = new fetchLogRepository_1.FetchLogRepository();
        this._vaultService = new vaultService_1.VaultService();
        this._fireblocksService = new fireblocksService_1.FireblocksService(this._vaultService);
        this._httpService = new httpService_1.HttpService(this._fetchLogRepository);
        this._chainalysisService = new chainalysisService_1.ChainalysisService(this._chainalysisTransactionRepository, this._assetRepository, this._vaultService, this._fireblocksService, this._httpService);
        this._cronJob1 = new cron_1.CronJob(process.env.CRON_JOB_UPDATE_CHAINALYSIS_TRANSACTION, () => __awaiter(this, void 0, void 0, function* () {
            const jobSchedulerLogs = yield this._jobSchedulerLogRepository.logStartJob(enums.jobMethod.UPDATE_CHAINALYSIS_TRANSACTIONS);
            try {
                const timeElapsed = Date.now();
                const today = new Date(timeElapsed);
                console.log(today.toISOString(), ": Running update chainalysis transactions");
                yield this.updateChainalysisTransactions();
                const endTimeElapsed = Date.now();
                const endToday = new Date(endTimeElapsed);
                console.log(endToday.toISOString(), ": Finish running update chainalysis transactions");
                yield this._jobSchedulerLogRepository.logEndJob(jobSchedulerLogs[0].job_scheduler_log_id, "Finish running " + enums.jobMethod.UPDATE_CHAINALYSIS_TRANSACTIONS);
            }
            catch (e) {
                console.error(e);
                if (e instanceof Error) {
                    yield this._jobSchedulerLogRepository.logEndJob(jobSchedulerLogs[0].job_scheduler_log_id, e === null || e === void 0 ? void 0 : e.message);
                }
            }
        }), null, false);
    }
    startJob() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this._cronJob1.running) {
                this._cronJob1.start();
            }
        });
    }
    stopJob() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this._cronJob1.running) {
                this._cronJob1.stop();
            }
        });
    }
    updateChainalysisTransactions() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisTransactions = yield this._chainalysisTransactionRepository.getPendingList();
                for (let index = 0; index < chainalysisTransactions.length; index++) {
                    const element = chainalysisTransactions[index];
                    yield this._chainalysisService.updateTransactionStatus(element.external_id);
                }
            }
            catch (error) {
                console.log(error);
            }
        });
    }
};
exports.CronJobService = CronJobService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], CronJobService);
//# sourceMappingURL=cronJobService.js.map