"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.saveToDatabase = exports.sendMail = exports.passwordValidation = exports.verifyCustomerSignature = exports.generateCustomerSignature = exports.generateCustomerApiKey = exports.generateKey = exports.jwtVerifyRefreshToken = exports.jwtVerifyToken = exports.jwtRefreshToken = exports.jwtGenerateToken = exports.comparePassword = exports.hashPassword = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const generate_api_key_1 = __importDefault(require("generate-api-key"));
const jwt = __importStar(require("jsonwebtoken"));
const fs_1 = require("fs");
const customError_1 = __importDefault(require("../middlewares/customError"));
const enums = __importStar(require("../utilities/enums"));
const nodemailer_1 = __importDefault(require("nodemailer"));
const vaultService_1 = require("../services/vaultService");
const hashPassword = (password) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise(function (fulfill, reject) {
        bcrypt_1.default.hash(password, 10, function (err, hash) {
            return __awaiter(this, void 0, void 0, function* () {
                if (err) {
                    return reject(err);
                }
                else {
                    return fulfill(hash);
                }
            });
        });
    });
});
exports.hashPassword = hashPassword;
const comparePassword = (password, hashPassword) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise(function (fulfill, reject) {
        bcrypt_1.default.compare(password, hashPassword, function (err, result) {
            return __awaiter(this, void 0, void 0, function* () {
                if (err) {
                    return reject(err);
                }
                else {
                    return fulfill(result);
                }
            });
        });
    });
});
exports.comparePassword = comparePassword;
const jwtGenerateToken = (user_id, code) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let signOptions = {
            expiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRES_IN,
            algorithm: "HS256",
        };
        return jwt.sign({ user_id: user_id, code: code }, process.env.JWT_ACCESS_TOKEN_SECRET, signOptions);
    }
    catch (error) {
        throw error;
    }
});
exports.jwtGenerateToken = jwtGenerateToken;
const jwtRefreshToken = (user_id, code) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let signOptions = {
            expiresIn: process.env.JWT_REFRESH_TOKEN_EXPIRES_IN,
            algorithm: "HS256",
        };
        return jwt.sign({ user_id: user_id, code: code }, process.env.JWT_REFRESH_TOKEN_SECRET, signOptions);
    }
    catch (error) {
        throw error;
    }
});
exports.jwtRefreshToken = jwtRefreshToken;
const jwtVerifyToken = (token) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let verifyOptions = {
            algorithms: ["HS256"],
        };
        return jwt.verify(token, process.env.JWT_ACCESS_TOKEN_SECRET, verifyOptions);
    }
    catch (error) {
        throw error;
    }
});
exports.jwtVerifyToken = jwtVerifyToken;
const jwtVerifyRefreshToken = (token) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let verifyOptions = {
            algorithms: ["HS256"],
        };
        return jwt.verify(token, process.env.JWT_REFRESH_TOKEN_SECRET, verifyOptions);
    }
    catch (error) {
        throw error;
    }
});
exports.jwtVerifyRefreshToken = jwtVerifyRefreshToken;
const generateKey = (name) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const code = (0, generate_api_key_1.default)({
            method: "uuidv5",
            name: name,
            namespace: (0, generate_api_key_1.default)({ method: "uuidv4" }).toString(),
        });
        return code.toString();
    }
    catch (error) {
        throw error;
    }
});
exports.generateKey = generateKey;
const generateCustomerApiKey = (user_id, name) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const code = (0, generate_api_key_1.default)({
            method: "uuidv5",
            name: user_id + "-" + name,
            namespace: (0, generate_api_key_1.default)({ method: "uuidv4" }).toString(),
        });
        return code.toString();
    }
    catch (error) {
        throw error;
    }
});
exports.generateCustomerApiKey = generateCustomerApiKey;
const generateCustomerSignature = (user_id, user_api_key_name) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let signOptions = {
            algorithm: "HS256",
        };
        return jwt.sign({ user_id: user_id, user_api_key_name: user_api_key_name }, process.env.CUSTOMER_SECRET, signOptions);
    }
    catch (error) {
        throw error;
    }
});
exports.generateCustomerSignature = generateCustomerSignature;
const verifyCustomerSignature = (token) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let verifyOptions = {
            algorithms: ["HS256"],
        };
        return jwt.verify(token, process.env.CUSTOMER_SECRET, verifyOptions);
    }
    catch (error) {
        throw error;
    }
});
exports.verifyCustomerSignature = verifyCustomerSignature;
const passwordValidation = (password) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const charactorLongCase = new RegExp(/^.{8,16}$/);
        const oneUpperCase = new RegExp(/(?=.*[A-Z])/);
        const oneLowerCase = new RegExp(/(?=.*[a-z])/);
        const oneDigitCase = new RegExp(/(?=.*[0-9])/);
        if (!charactorLongCase.test(password)) {
            throw new customError_1.default(enums.responseCode.CharactorLongCase, enums.responseMessage.CharactorLongCase);
        }
        if (!oneUpperCase.test(password)) {
            throw new customError_1.default(enums.responseCode.OneUpperCase, enums.responseMessage.OneUpperCase);
        }
        if (!oneLowerCase.test(password)) {
            throw new customError_1.default(enums.responseCode.OneLowerCase, enums.responseMessage.OneLowerCase);
        }
        if (!oneDigitCase.test(password)) {
            throw new customError_1.default(enums.responseCode.OneDigitCase, enums.responseMessage.OneDigitCase);
        }
    }
    catch (error) {
        throw error;
    }
});
exports.passwordValidation = passwordValidation;
const sendMail = (from, to, subject, html) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const _vaultService = new vaultService_1.VaultService();
        const smtpServer = yield _vaultService.getSMTPServer();
        let transporter = nodemailer_1.default.createTransport({
            host: process.env.MAIL_HOST,
            port: process.env.MAIL_PORT,
            secure: false,
            auth: {
                user: smtpServer.data.smtpUser,
                pass: smtpServer.data.smtpPass,
            },
        });
        let info = yield transporter.sendMail({
            from: from,
            to: to,
            subject: subject,
            html: html,
        });
        return info;
    }
    catch (error) {
        throw error;
    }
});
exports.sendMail = sendMail;
const saveToDatabase = (DB) => {
    (0, fs_1.writeFileSync)("./src/repositories/db.json", JSON.stringify(DB, null, 2), {
        encoding: "utf-8",
    });
};
exports.saveToDatabase = saveToDatabase;
//# sourceMappingURL=utils.js.map