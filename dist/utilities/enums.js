"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.responseMessageLQN = exports.responseCodeLQN = exports.status = exports.paymentMode = exports.userType = exports.lqnUserType = exports.responseMessageFireblock = exports.responseCodeFireblock = exports.chainalysisAlert = exports.jobMethod = exports.logsType = exports.source = exports.addressType = exports.bankAccountType = exports.dropdownlistType = exports.specialAssetCode = exports.fireblocksTransactionStatus = exports.stepJobProcess = exports.lqnTransactionStatusText = exports.lqnTransactionStatus = exports.transactionStatusName = exports.transactionStatus = exports.inputType = exports.transactionType = exports.fireblocksStatus = exports.mode = exports.accountType = exports.fireblocksTransactionType = exports.responseMessage = exports.responseCode = void 0;
var responseCode;
(function (responseCode) {
    responseCode[responseCode["Success"] = 0] = "Success";
    responseCode[responseCode["Duplicate"] = 4001] = "Duplicate";
    responseCode[responseCode["UserNotFound"] = 4002] = "UserNotFound";
    responseCode[responseCode["CompanyNotFound"] = 4003] = "CompanyNotFound";
    responseCode[responseCode["VaultAccountNotFound"] = 4004] = "VaultAccountNotFound";
    responseCode[responseCode["FiatAccountNotFound"] = 4005] = "FiatAccountNotFound";
    responseCode[responseCode["AssetNotFound"] = 4006] = "AssetNotFound";
    responseCode[responseCode["AssetRateNotFound"] = 4007] = "AssetRateNotFound";
    responseCode[responseCode["BankAccountNotFound"] = 4008] = "BankAccountNotFound";
    responseCode[responseCode["InsufficientFunds"] = 4009] = "InsufficientFunds";
    responseCode[responseCode["InsufficientAssets"] = 4010] = "InsufficientAssets";
    responseCode[responseCode["AccountNameMismatch"] = 4011] = "AccountNameMismatch";
    responseCode[responseCode["ProtectOwnerAccount"] = 4012] = "ProtectOwnerAccount";
    responseCode[responseCode["GasConfigurationNotFound"] = 4013] = "GasConfigurationNotFound";
    responseCode[responseCode["InsufficientFundsForCompany"] = 4014] = "InsufficientFundsForCompany";
    responseCode[responseCode["UsernameOrPasswordIncorrectCase"] = 4020] = "UsernameOrPasswordIncorrectCase";
    responseCode[responseCode["PasswordMismatch"] = 4021] = "PasswordMismatch";
    responseCode[responseCode["CharactorLongCase"] = 4022] = "CharactorLongCase";
    responseCode[responseCode["OneUpperCase"] = 4023] = "OneUpperCase";
    responseCode[responseCode["OneLowerCase"] = 4024] = "OneLowerCase";
    responseCode[responseCode["OneDigitCase"] = 4025] = "OneDigitCase";
    responseCode[responseCode["InvalidStatus"] = 4026] = "InvalidStatus";
    responseCode[responseCode["SenderNotfound"] = 4027] = "SenderNotfound";
    responseCode[responseCode["BankAccountTypeInvalid"] = 4028] = "BankAccountTypeInvalid";
    responseCode[responseCode["InvalidRequest"] = 4029] = "InvalidRequest";
    responseCode[responseCode["InvalidUserType"] = 4030] = "InvalidUserType";
    responseCode[responseCode["ReceiverNotfound"] = 4031] = "ReceiverNotfound";
    responseCode[responseCode["UploadFileInvalidFileType"] = 4032] = "UploadFileInvalidFileType";
    responseCode[responseCode["UploadFileFileTooLarge"] = 4033] = "UploadFileFileTooLarge";
    responseCode[responseCode["Unauthorized"] = 403] = "Unauthorized";
    responseCode[responseCode["NotFound"] = 404] = "NotFound";
    responseCode[responseCode["Error"] = 500] = "Error";
})(responseCode || (exports.responseCode = responseCode = {}));
var responseMessage;
(function (responseMessage) {
    responseMessage["Success"] = "Success";
    responseMessage["Duplicate"] = "Data has already exists";
    responseMessage["UserNotFound"] = "User not found";
    responseMessage["CompanyNotFound"] = "Company not found";
    responseMessage["VaultAccountNotFound"] = "Vault account not found";
    responseMessage["FiatAccountNotFound"] = "Fiat account not found";
    responseMessage["AssetNotFound"] = "Asset not found";
    responseMessage["AssetRateNotFound"] = "Asset Rate not found";
    responseMessage["BankAccountNotFound"] = "Bank account not found";
    responseMessage["InsufficientFunds"] = "Insufficient funds";
    responseMessage["InsufficientAssets"] = "Insufficient balance";
    responseMessage["AccountNameMismatch"] = "Account name mismatch";
    responseMessage["ProtectOwnerAccount"] = "You are not able to change status of your owner account";
    responseMessage["GasConfigurationNotFound"] = "Gas configuration not found";
    responseMessage["InsufficientFundsForCompany"] = "Insufficient funds of company";
    responseMessage["UsernameOrPasswordIncorrectCase"] = "Username or password is incorrect. Please try again.";
    responseMessage["PasswordMismatch"] = "Password mismatch";
    responseMessage["CharactorLongCase"] = "Password must be 8-16 Characters Long.";
    responseMessage["OneUpperCase"] = "Password must have at least one Uppercase Character.";
    responseMessage["OneLowerCase"] = "Password must have at least one Lowercase Character.";
    responseMessage["OneDigitCase"] = "Password must contain at least one Digit.";
    responseMessage["InvalidStatus"] = "Status is invalid.";
    responseMessage["SenderNotfound"] = "Sender account not found";
    responseMessage["BankAccountTypeInvalid"] = "Bank account type is invalid";
    responseMessage["InvalidRequest"] = "Request is invalid";
    responseMessage["InvalidUserType"] = "User type is invalid";
    responseMessage["ReceiverNotfound"] = "Receiver not found";
    responseMessage["UploadFileInvalidFileType"] = "Invalid file type";
    responseMessage["UploadFileFileTooLarge"] = "File too large";
    responseMessage["Unauthorized"] = "Unauthorized";
    responseMessage["NotFound"] = "Data not found";
    responseMessage["Error"] = "Internal error";
})(responseMessage || (exports.responseMessage = responseMessage = {}));
var fireblocksTransactionType;
(function (fireblocksTransactionType) {
    fireblocksTransactionType["ADD_BASE_ASSET"] = "ADD_BASE_ASSET";
    fireblocksTransactionType["AUTO_FUELING_GAS_FEES"] = "AUTO_FUELING_GAS_FEES";
    fireblocksTransactionType["AUTO_FUELING_GAS_FEES_FOR_NEW_ASSET"] = "AUTO_FUELING_GAS_FEES_FOR_NEW_ASSET";
    fireblocksTransactionType["TRANSFER_BUYING"] = "TRANSFER_BUYING";
    fireblocksTransactionType["TRANSFER_SELLING"] = "TRANSFER_SELLING";
})(fireblocksTransactionType || (exports.fireblocksTransactionType = fireblocksTransactionType = {}));
var accountType;
(function (accountType) {
    accountType["COMPANY"] = "C";
    accountType["USER"] = "U";
})(accountType || (exports.accountType = accountType = {}));
var mode;
(function (mode) {
    mode["INSERT"] = "INSERT";
    mode["UPDATE"] = "UPDATE";
})(mode || (exports.mode = mode = {}));
var fireblocksStatus;
(function (fireblocksStatus) {
    fireblocksStatus["SUBMITTED"] = "SUBMITTED";
})(fireblocksStatus || (exports.fireblocksStatus = fireblocksStatus = {}));
var transactionType;
(function (transactionType) {
    transactionType["BUYING"] = "B";
    transactionType["SELLING"] = "S";
    transactionType["WITHDRAW"] = "W";
    transactionType["TRANSFER"] = "TF";
    transactionType["CONVERT"] = "CV";
    transactionType["TOPUP"] = "TU";
})(transactionType || (exports.transactionType = transactionType = {}));
var inputType;
(function (inputType) {
    inputType["ASSET"] = "ASSET";
    inputType["AMOUNT"] = "AMOUNT";
})(inputType || (exports.inputType = inputType = {}));
var transactionStatus;
(function (transactionStatus) {
    transactionStatus["ACTIVE"] = "A";
    transactionStatus["SUCCESS"] = "S";
    transactionStatus["FAILED"] = "F";
    transactionStatus["PENDING"] = "P";
    transactionStatus["REJECTED"] = "R";
    transactionStatus["CANCELLED"] = "C";
    transactionStatus["ONHOLD"] = "H";
})(transactionStatus || (exports.transactionStatus = transactionStatus = {}));
var transactionStatusName;
(function (transactionStatusName) {
    transactionStatusName["ACTIVE"] = "ACTIVE";
    transactionStatusName["SUCCESS"] = "SUCCESS";
    transactionStatusName["FAILED"] = "FAILED";
    transactionStatusName["PENDING"] = "PENDING";
    transactionStatusName["REJECTED"] = "REJECTED";
    transactionStatusName["CANCELLED"] = "CANCELLED";
    transactionStatusName["ONHOLD"] = "ONHOLD";
})(transactionStatusName || (exports.transactionStatusName = transactionStatusName = {}));
var lqnTransactionStatus;
(function (lqnTransactionStatus) {
    lqnTransactionStatus["UN_COMMIT_HOLD"] = "UN-COMMIT-HOLD";
    lqnTransactionStatus["UN_COMMIT_COMPLIANCE"] = "UN-COMMIT-COMPLIANCE";
    lqnTransactionStatus["HOLD"] = "HOLD";
    lqnTransactionStatus["COMPLIANCE"] = "COMPLIANCE";
    lqnTransactionStatus["SANCTION"] = "SANCTION";
    lqnTransactionStatus["UN_PAID"] = "UN-PAID";
    lqnTransactionStatus["POST"] = "POST";
    lqnTransactionStatus["PAID"] = "PAID";
    lqnTransactionStatus["CANCEL"] = "CANCEL";
    lqnTransactionStatus["CANCEL_HOLD"] = "CANCELHOLD";
    lqnTransactionStatus["API_PROCESSING"] = "API PROCESSING";
    lqnTransactionStatus["BLOCK"] = "BLOCK";
})(lqnTransactionStatus || (exports.lqnTransactionStatus = lqnTransactionStatus = {}));
var lqnTransactionStatusText;
(function (lqnTransactionStatusText) {
    lqnTransactionStatusText["UN_COMMIT_HOLD"] = "Hold (Un-Commit-Hold";
    lqnTransactionStatusText["UN_COMMIT_COMPLIANCE"] = "Hold (Un-Commit-Compliance)";
    lqnTransactionStatusText["HOLD"] = "Hold";
    lqnTransactionStatusText["COMPLIANCE"] = "Hold (Compliance)";
    lqnTransactionStatusText["SANCTION"] = "Hold (Sanction)";
    lqnTransactionStatusText["UN_PAID"] = "Pending (Un-Paid)";
    lqnTransactionStatusText["API_PROCESSING"] = "Pending (API Processing)";
    lqnTransactionStatusText["POST"] = "Pending (Post)";
    lqnTransactionStatusText["PAID"] = "Success (Paid)";
    lqnTransactionStatusText["CANCEL"] = "Cancel";
    lqnTransactionStatusText["CANCEL_HOLD"] = "Cancel (Hold)";
    lqnTransactionStatusText["BLOCK"] = "Cancel (Block)";
})(lqnTransactionStatusText || (exports.lqnTransactionStatusText = lqnTransactionStatusText = {}));
var stepJobProcess;
(function (stepJobProcess) {
    stepJobProcess["SUBMITTED"] = "SUBMITTED";
    stepJobProcess["AUTO_FUELING_GAS_FEES"] = "AUTO_FUELING_GAS_FEES";
    stepJobProcess["FAILED"] = "FAILED";
    stepJobProcess["CANCELLED"] = "CANCELLED";
    stepJobProcess["COMPLETED"] = "COMPLETED";
})(stepJobProcess || (exports.stepJobProcess = stepJobProcess = {}));
var fireblocksTransactionStatus;
(function (fireblocksTransactionStatus) {
    fireblocksTransactionStatus["SUBMITTED"] = "SUBMITTED";
    fireblocksTransactionStatus["QUEUED"] = "QUEUED";
    fireblocksTransactionStatus["PENDING_SIGNATURE"] = "PENDING_SIGNATURE";
    fireblocksTransactionStatus["BROADCASTING"] = "BROADCASTING";
    fireblocksTransactionStatus["COMPLETED"] = "COMPLETED";
    fireblocksTransactionStatus["FAILED"] = "FAILED";
    fireblocksTransactionStatus["CANCELLED"] = "CANCELLED";
    fireblocksTransactionStatus["REJECTED"] = "REJECTED";
})(fireblocksTransactionStatus || (exports.fireblocksTransactionStatus = fireblocksTransactionStatus = {}));
var specialAssetCode;
(function (specialAssetCode) {
    specialAssetCode["ALGO"] = "ALGO";
    specialAssetCode["ALGO_USDC_UV4I"] = "ALGO_USDC_UV4I";
    specialAssetCode["SOL"] = "SOL";
    specialAssetCode["SOL_USDC_PTHX"] = "SOL_USDC_PTHX";
    specialAssetCode["XLM"] = "XLM";
    specialAssetCode["XLM_USDC_5F3T"] = "XLM_USDC_5F3T";
})(specialAssetCode || (exports.specialAssetCode = specialAssetCode = {}));
var dropdownlistType;
(function (dropdownlistType) {
    dropdownlistType["PURPOSE_OF_TRANSFER"] = "PURPOSE_OF_TRANSFER";
    dropdownlistType["RELATIONSHIP_TO_RECEIVER"] = "RELATIONSHIP_TO_RECEIVER";
})(dropdownlistType || (exports.dropdownlistType = dropdownlistType = {}));
var bankAccountType;
(function (bankAccountType) {
    bankAccountType["OWNER"] = "M";
    bankAccountType["OTHER"] = "O";
})(bankAccountType || (exports.bankAccountType = bankAccountType = {}));
var addressType;
(function (addressType) {
    addressType["USER_PROFILE"] = "USER_PROFILE";
    addressType["BANK_ACCOUNT"] = "BANK_ACCOUNT";
    addressType["LQN_USER_PROFILE"] = "LQN_USER_PROFILE";
})(addressType || (exports.addressType = addressType = {}));
var source;
(function (source) {
    source["FIREBLOCKS"] = "FIREBLOCKS";
    source["PORTAL"] = "PORTAL";
})(source || (exports.source = source = {}));
var logsType;
(function (logsType) {
    logsType["INFO"] = "INFO";
    logsType["ERROR"] = "ERROR";
    logsType["DEBUG"] = "DEBUG";
})(logsType || (exports.logsType = logsType = {}));
var jobMethod;
(function (jobMethod) {
    jobMethod["CREATE_SPECIAL_ASSETS"] = "CREATE_SPECIAL_ASSETS";
    jobMethod["PROCESS_PENDING_TRANSACTIONS"] = "PROCESS_PENDING_TRANSACTIONS";
    jobMethod["SYNC_FIREBLOCKS_TRANSACTIONS"] = "SYNC_FIREBLOCKS_TRANSACTIONS";
    jobMethod["UPDATE_CHAINALYSIS_TRANSACTIONS"] = "UPDATE_CHAINALYSIS_TRANSACTIONS";
})(jobMethod || (exports.jobMethod = jobMethod = {}));
var chainalysisAlert;
(function (chainalysisAlert) {
    chainalysisAlert["HIGH"] = "HIGH";
    chainalysisAlert["MEDIUM"] = "MEDIUM";
    chainalysisAlert["LOW"] = "LOW";
})(chainalysisAlert || (exports.chainalysisAlert = chainalysisAlert = {}));
var responseCodeFireblock;
(function (responseCodeFireblock) {
    responseCodeFireblock[responseCodeFireblock["MISSING_REQUEST"] = 400000] = "MISSING_REQUEST";
    responseCodeFireblock[responseCodeFireblock["NONCE_INVALID"] = 400001] = "NONCE_INVALID";
    responseCodeFireblock[responseCodeFireblock["TIMESTAMP_INVALID"] = 400002] = "TIMESTAMP_INVALID";
    responseCodeFireblock[responseCodeFireblock["SIGNATURE_INVALID"] = 400003] = "SIGNATURE_INVALID";
    responseCodeFireblock[responseCodeFireblock["INSUFFICIENT_PERMISSION"] = 400004] = "INSUFFICIENT_PERMISSION";
    responseCodeFireblock[responseCodeFireblock["INSUFFICIENT_FUND"] = 400005] = "INSUFFICIENT_FUND";
    responseCodeFireblock[responseCodeFireblock["INSUFFICIENT_FEE"] = 400006] = "INSUFFICIENT_FEE";
    responseCodeFireblock[responseCodeFireblock["UNSUPPORTED_ACCOUNT"] = 400007] = "UNSUPPORTED_ACCOUNT";
    responseCodeFireblock[responseCodeFireblock["UNSUPPORTED_OPERATION"] = 400008] = "UNSUPPORTED_OPERATION";
    responseCodeFireblock[responseCodeFireblock["ASSET_NOT_SUPPORTED"] = 400009] = "ASSET_NOT_SUPPORTED";
    responseCodeFireblock[responseCodeFireblock["PARAMETER_INVALID"] = 400010] = "PARAMETER_INVALID";
    responseCodeFireblock[responseCodeFireblock["BAD_ADDRESS_FORMAT"] = 400011] = "BAD_ADDRESS_FORMAT";
    responseCodeFireblock[responseCodeFireblock["BALANCE_AMOUNT_TOO_SMALL"] = 400012] = "BALANCE_AMOUNT_TOO_SMALL";
    responseCodeFireblock[responseCodeFireblock["THIRD_PARTY_NEED_MANUAL_DEPOSIT_ADDRESS"] = 400013] = "THIRD_PARTY_NEED_MANUAL_DEPOSIT_ADDRESS";
    responseCodeFireblock[responseCodeFireblock["THIRD_PARTY_REJECTED_OPERATION"] = 400014] = "THIRD_PARTY_REJECTED_OPERATION";
    responseCodeFireblock[responseCodeFireblock["WITHDRAW_CANCELLED"] = 400015] = "WITHDRAW_CANCELLED";
    responseCodeFireblock[responseCodeFireblock["ADDRESS_NOT_WHITELIST"] = 400016] = "ADDRESS_NOT_WHITELIST";
    responseCodeFireblock[responseCodeFireblock["IP_NOT_WHITELIST"] = 400017] = "IP_NOT_WHITELIST";
    responseCodeFireblock[responseCodeFireblock["ACCOUNT_NOT_FOUND"] = 400018] = "ACCOUNT_NOT_FOUND";
    responseCodeFireblock[responseCodeFireblock["WITHDRAW_LIMITED"] = 400019] = "WITHDRAW_LIMITED";
    responseCodeFireblock[responseCodeFireblock["THIRD_PARTY_DENIEDREQUEST"] = 400020] = "THIRD_PARTY_DENIEDREQUEST";
    responseCodeFireblock[responseCodeFireblock["NotFound"] = 404] = "NotFound";
})(responseCodeFireblock || (exports.responseCodeFireblock = responseCodeFireblock = {}));
var responseMessageFireblock;
(function (responseMessageFireblock) {
    responseMessageFireblock["MISSING_REQUEST"] = "Missing request header params";
    responseMessageFireblock["NONCE_INVALID"] = "Nonce sent was invalid";
    responseMessageFireblock["TIMESTAMP_INVALID"] = "Timestamp sent was invalid";
    responseMessageFireblock["SIGNATURE_INVALID"] = "Signature sent was invalid";
    responseMessageFireblock["INSUFFICIENT_PERMISSION"] = "Insufficient permissions for this API key";
    responseMessageFireblock["INSUFFICIENT_FUND"] = "Insufficient funds to carry out this operation";
    responseMessageFireblock["INSUFFICIENT_FEE"] = "Insufficient fee to carry out this operation";
    responseMessageFireblock["UNSUPPORTED_ACCOUNT"] = "Unsupported account type for this 3rd party";
    responseMessageFireblock["UNSUPPORTED_OPERATION"] = "Unsupported operation for this 3rd party";
    responseMessageFireblock["ASSET_NOT_SUPPORTED"] = "Asset not supported on this 3rd party";
    responseMessageFireblock["PARAMETER_INVALID"] = "One of the parameters sent in the body or query is invalid";
    responseMessageFireblock["BAD_ADDRESS_FORMAT"] = "Bad address format sent";
    responseMessageFireblock["BALANCE_AMOUNT_TOO_SMALL"] = "Balance amount is too small";
    responseMessageFireblock["THIRD_PARTY_NEED_MANUAL_DEPOSIT_ADDRESS"] = "This 3rd party needs manual deposit address generation";
    responseMessageFireblock["THIRD_PARTY_REJECTED_OPERATION"] = "The 3rd party rejected this operation";
    responseMessageFireblock["WITHDRAW_CANCELLED"] = "Withdraw was cancelled or failed on the 3rd party";
    responseMessageFireblock["ADDRESS_NOT_WHITELIST"] = "Address wasn't whitelisted";
    responseMessageFireblock["IP_NOT_WHITELIST"] = "IP wasn't whitelisted";
    responseMessageFireblock["ACCOUNT_NOT_FOUND"] = "Account not found";
    responseMessageFireblock["WITHDRAW_LIMITED"] = "Withdrawals are limited by the 3rd party. Please try again in a bit.";
    responseMessageFireblock["THIRD_PARTY_DENIEDREQUEST"] = "3rd party has denied the request - a settlement is required!";
    responseMessageFireblock["NotFound"] = "Account ID or Account Type not found or supported.";
})(responseMessageFireblock || (exports.responseMessageFireblock = responseMessageFireblock = {}));
var lqnUserType;
(function (lqnUserType) {
    lqnUserType["SENDER"] = "SENDER";
    lqnUserType["RECEIVER"] = "RECEIVER";
})(lqnUserType || (exports.lqnUserType = lqnUserType = {}));
// export enum lqnStatus {
//   ACTIVE = "ACTIVE",
//   INACTIVE = "INACTIVE",
// }
var userType;
(function (userType) {
    userType["INDIVIDUAL"] = "I";
    userType["BUSINESS"] = "B";
})(userType || (exports.userType = userType = {}));
var paymentMode;
(function (paymentMode) {
    paymentMode["BANK_TRANSFER"] = "B";
})(paymentMode || (exports.paymentMode = paymentMode = {}));
var status;
(function (status) {
    status["ACTIVE"] = "A";
    status["INACTIVE"] = "I";
})(status || (exports.status = status = {}));
var responseCodeLQN;
(function (responseCodeLQN) {
    responseCodeLQN[responseCodeLQN["INVALID_FIRST_NAME"] = 4001] = "INVALID_FIRST_NAME";
    responseCodeLQN[responseCodeLQN["INVALID_LAST_NAME"] = 4002] = "INVALID_LAST_NAME";
    responseCodeLQN[responseCodeLQN["INVALID_ADDRESS"] = 4003] = "INVALID_ADDRESS";
    responseCodeLQN[responseCodeLQN["INVALID_DATE_OF_BIRTH"] = 4004] = "INVALID_DATE_OF_BIRTH";
    responseCodeLQN[responseCodeLQN["INVALID_GENDER"] = 4005] = "INVALID_GENDER";
    responseCodeLQN[responseCodeLQN["INVALID_CONTACT_NUMBER"] = 4006] = "INVALID_CONTACT_NUMBER";
    responseCodeLQN[responseCodeLQN["INVALID_POSTCODE"] = 4007] = "INVALID_POSTCODE";
    responseCodeLQN[responseCodeLQN["INVALID_ID_TYPE"] = 4008] = "INVALID_ID_TYPE";
    responseCodeLQN[responseCodeLQN["INVALID_ID_NUMBER"] = 4009] = "INVALID_ID_NUMBER";
    responseCodeLQN[responseCodeLQN["INVALID_ID_ISSUE_DATE"] = 4010] = "INVALID_ID_ISSUE_DATE";
    responseCodeLQN[responseCodeLQN["INVALID_ID_EXPIRE_DATE"] = 4011] = "INVALID_ID_EXPIRE_DATE";
    responseCodeLQN[responseCodeLQN["INVALID_OCCUPATION"] = 4012] = "INVALID_OCCUPATION";
    responseCodeLQN[responseCodeLQN["INVALID_BENEFICIARY"] = 4013] = "INVALID_BENEFICIARY";
})(responseCodeLQN || (exports.responseCodeLQN = responseCodeLQN = {}));
var responseMessageLQN;
(function (responseMessageLQN) {
    responseMessageLQN["INVALID_FIRST_NAME"] = "First name is required";
    responseMessageLQN["INVALID_LAST_NAME"] = "Last name is required";
    responseMessageLQN["INVALID_ADDRESS"] = "Address is required";
    responseMessageLQN["INVALID_DATE_OF_BIRTH"] = "Date of birth is required";
    responseMessageLQN["INVALID_GENDER"] = "Gender is required";
    responseMessageLQN["INVALID_CONTACT_NUMBER"] = "Contact number is required";
    responseMessageLQN["INVALID_POSTCODE"] = "Postcode is required";
    responseMessageLQN["INVALID_ID_TYPE"] = "Id type is required";
    responseMessageLQN["INVALID_ID_NUMBER"] = "Id number is required";
    responseMessageLQN["INVALID_ID_ISSUE_DATE"] = "Issue date is required";
    responseMessageLQN["INVALID_ID_EXPIRE_DATE"] = "Expire date is required";
    responseMessageLQN["INVALID_OCCUPATION"] = "Occupation is required";
    responseMessageLQN["INVALID_BENEFICIARY"] = "Beneficiary is required";
})(responseMessageLQN || (exports.responseMessageLQN = responseMessageLQN = {}));
//# sourceMappingURL=enums.js.map