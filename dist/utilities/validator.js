"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validate = exports.updateRoleMenuByRoleIdValidationRules = exports.updateRoleValidationRules = exports.createRoleValidationRules = exports.updateUserValidationRules = exports.createUserValidationRules = exports.registerValidationRules = exports.loginValidationRules = void 0;
const express_validator_1 = require("express-validator");
const loginValidationRules = () => {
    return [
        (0, express_validator_1.body)("user_name").exists({ checkFalsy: true }),
        (0, express_validator_1.body)("password").exists({ checkFalsy: true }),
    ];
};
exports.loginValidationRules = loginValidationRules;
const registerValidationRules = () => {
    return [
        (0, express_validator_1.body)("email").isEmail(),
        (0, express_validator_1.body)("password").exists({ checkFalsy: true }),
    ];
};
exports.registerValidationRules = registerValidationRules;
const createUserValidationRules = () => {
    return [
        (0, express_validator_1.body)("type").exists({ checkFalsy: true }),
        (0, express_validator_1.body)("email").isEmail(),
        (0, express_validator_1.body)("password").exists({ checkFalsy: true }),
    ];
};
exports.createUserValidationRules = createUserValidationRules;
const updateUserValidationRules = () => {
    return [(0, express_validator_1.body)("email").isEmail(), (0, express_validator_1.body)("status").exists({ checkFalsy: true })];
};
exports.updateUserValidationRules = updateUserValidationRules;
const createRoleValidationRules = () => {
    return [(0, express_validator_1.body)("name").exists({ checkFalsy: true })];
};
exports.createRoleValidationRules = createRoleValidationRules;
const updateRoleValidationRules = () => {
    return [
        (0, express_validator_1.body)("name").exists({ checkFalsy: true }),
        (0, express_validator_1.body)("status").exists({ checkFalsy: true }),
    ];
};
exports.updateRoleValidationRules = updateRoleValidationRules;
const updateRoleMenuByRoleIdValidationRules = () => {
    return [
        (0, express_validator_1.body)("group_menu_id").exists({ checkFalsy: true }),
        (0, express_validator_1.body)("permission").exists({ checkFalsy: true }),
    ];
};
exports.updateRoleMenuByRoleIdValidationRules = updateRoleMenuByRoleIdValidationRules;
const validate = (req, res, next) => {
    const errors = (0, express_validator_1.validationResult)(req);
    if (errors.isEmpty()) {
        return next();
    }
    return res.status(400).json({
        code: 400,
        message: "Invalid",
        result: errors.array(),
    });
};
exports.validate = validate;
//# sourceMappingURL=validator.js.map