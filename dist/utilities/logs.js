"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.logDebug = exports.logError = exports.logInfo = exports.writeLogsDB = void 0;
const enums = __importStar(require("../utilities/enums"));
const logs = __importStar(require("../utilities/logs"));
const logsRepository_1 = require("../repositories/logsRepository");
// @singleton()
// export class LogsUtil {
//   readonly _logsRepository: LogsRepository;
//   constructor(
//     logsRepository: LogsRepository,
//   ) {
//     this._logsRepository = logsRepository;
//   }
//   async writeLogsDB(
//     logsType: enums.logsType,
//     logsName: string,
//     logsMessage: string
//     ) {
//     try {
//       this._logsRepository.create(logsType,logsName,logsMessage);
//     } catch (error) {
//       throw error;
//     }
//   }
// }
const writeLogsDB = (logsType, logsName, logsMessage) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let logRepo = new logsRepository_1.LogsRepository();
        logRepo.create(logsType, logsName, logsMessage);
    }
    catch (error) {
        console.log(error);
        // throw error;
    }
});
exports.writeLogsDB = writeLogsDB;
const logInfo = (logsName, logsMessage) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logs.writeLogsDB(enums.logsType.INFO, logsName, logsMessage);
        console.log(logsMessage);
        return;
    }
    catch (error) {
        console.log(error);
        // throw error;
    }
});
exports.logInfo = logInfo;
const logError = (logsName, logsMessage) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logs.writeLogsDB(enums.logsType.ERROR, logsName, logsMessage);
        console.log(logsMessage);
        return;
    }
    catch (error) {
        console.log(error);
        // throw error;
    }
});
exports.logError = logError;
const logDebug = (logsName, logsMessage) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logs.writeLogsDB(enums.logsType.DEBUG, logsName, logsMessage);
        console.log(logsMessage);
        return;
    }
    catch (error) {
        console.log(error);
        // throw error;
    }
});
exports.logDebug = logDebug;
//# sourceMappingURL=logs.js.map