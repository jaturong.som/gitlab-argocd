"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssetRateRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let AssetRateRepository = exports.AssetRateRepository = class AssetRateRepository {
    constructor() { }
    getList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select asset_rates.asset_rate_id
          , networks.network_id
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , asset_rates.buying
          , asset_rates.selling
          , CONCAT(users.first_name,' ', users.last_name) as updated_by
          , asset_rates.updated_date
        from asset_rates
          inner join assets on assets.code = asset_rates.asset_code
          inner join networks on networks.network_id = assets.network_id
          left join users on users.user_id = NULLIF(asset_rates.updated_by, '')::int
        order by networks.code, assets.code`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListByNetworkId(networkId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select asset_rates.asset_rate_id
          , networks.network_id
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , asset_rates.buying
          , asset_rates.selling
          , CONCAT(users.first_name,' ', users.last_name) as updated_by
          , asset_rates.updated_date
        from asset_rates
          inner join assets on assets.code = asset_rates.asset_code
          inner join networks on networks.network_id = assets.network_id
          left join users on users.user_id = NULLIF(asset_rates.updated_by, '')::int
        where assets.network_id = $1
        order by networks.code, assets.code`;
                return yield connection_1.db.query(sql, [networkId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoById(assetRateId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select asset_rates.asset_rate_id
          , networks.network_id
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , asset_rates.buying
          , asset_rates.selling
        from asset_rates
          inner join assets on assets.code = asset_rates.asset_code
          inner join networks on networks.network_id = assets.network_id
        where asset_rates.asset_rate_id = $1`;
                return yield connection_1.db.query(sql, [assetRateId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByAssetCode(assetCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select asset_rates.asset_rate_id
          , networks.network_id
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , asset_rates.buying
          , asset_rates.selling
        from asset_rates
          inner join assets on assets.code = asset_rates.asset_code
          inner join networks on networks.network_id = assets.network_id
        where assets.code = $1`;
                return yield connection_1.db.query(sql, [assetCode]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(assetId, assetCode, buying, selling, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO asset_rates(asset_id, asset_code, buying, selling, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, CURRENT_TIMESTAMP, $5, CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    assetId,
                    assetCode,
                    buying,
                    selling,
                    logOnId,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    update(assetRateId, buying, selling, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE asset_rates
        SET buying = $2
            , selling = $3
            , updated_by = $4
            , updated_date = CURRENT_TIMESTAMP
        WHERE asset_rate_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [assetRateId, buying, selling, logOnId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    delete(assetRateId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        DELETE FROM asset_rates
        WHERE asset_rate_id = $1`;
                return yield connection_1.db.query(sql, [assetRateId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.AssetRateRepository = AssetRateRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], AssetRateRepository);
//# sourceMappingURL=assetRateRepository.js.map