"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let RoleRepository = exports.RoleRepository = class RoleRepository {
    constructor() { }
    getList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM roles
        ORDER BY role_id`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getActiveList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM roles
        WHERE status = 'A'
        ORDER BY role_id`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoById(roleId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM roles
        WHERE role_id = $1`;
                return yield connection_1.db.query(sql, [roleId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByName(name) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM roles
        WHERE "name" = $1`;
                return yield connection_1.db.query(sql, [name]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    isDuplicateByName(mode, name, roleId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let sql = "";
                if (mode === "INSERT") {
                    sql = `
          SELECT *
          FROM roles
          WHERE lower("name") = lower($1)`;
                }
                else {
                    sql = `
          SELECT *
          FROM roles
          WHERE role_id != $2
          AND lower("name") = lower($1)`;
                }
                return yield connection_1.db.query(sql, [name, roleId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(name, description, status, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO roles("name", description, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, CURRENT_TIMESTAMP, $4, CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [name, description, status, logOnId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    update(roleId, name, description, status, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE roles
        SET "name" = $2
          , description = $3
          , status = $4
          , updated_by = $5
          , updated_date = CURRENT_TIMESTAMP
        WHERE role_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [roleId, name, description, status, logOnId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    delete(roleId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        DELETE FROM roles
        WHERE role_id = $1`;
                return yield connection_1.db.query(sql, [roleId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.RoleRepository = RoleRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], RoleRepository);
//# sourceMappingURL=roleRepository.js.map