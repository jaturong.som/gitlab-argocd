"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssetRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let AssetRepository = exports.AssetRepository = class AssetRepository {
    constructor() { }
    getListByNetworkId(networkId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , assets.network_id
          , assets.contract_address
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
        FROM assets
            inner join networks on networks.network_id = assets.network_id
                    and networks.status = 'A'
        WHERE assets.status = 'A'
        AND assets.network_id = $1
        ORDER BY assets.asset_id`;
                return yield connection_1.db.query(sql, [networkId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByCode(code) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , assets.network_id
          , assets.contract_address
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
        FROM assets
            inner join networks on networks.network_id = assets.network_id
                    and networks.status = 'A'
        WHERE assets.status = 'A'
        AND assets.code = $1`;
                return yield connection_1.db.query(sql, [code]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByCodeIgnoreStatus(code) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , assets.network_id
          , assets.status as asset_status
          , assets.contract_address
          , assets.native_asset_symbol
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
        FROM assets
            inner join networks on networks.network_id = assets.network_id
        WHERE assets.code = $1`;
                return yield connection_1.db.query(sql, [code]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByAssetGroup(assetGroup) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM assets
        WHERE assets.status = 'A'
        AND assets.asset_group = $1`;
                return yield connection_1.db.query(sql, [assetGroup]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetIdByAssetCodeNetworkName(assetCode, networkName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
          select a.asset_id
          from assets a, networks n
          where a.network_id = n.network_id
          and a.code = $1
          and n.name = $2
          and a.status = 'A'`;
                return yield connection_1.db.query(sql, [assetCode, networkName]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select a.code, a.name as asset_name, n.name, a.contract_address, n.code as network_code
        from assets a, networks n
        where a.network_id = n.network_id
        and a.status ='A'`;
                return yield connection_1.db.query(sql, []);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.AssetRepository = AssetRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], AssetRepository);
//# sourceMappingURL=assetRepository.js.map