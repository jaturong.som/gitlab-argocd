"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssetInVaultAccountRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let AssetInVaultAccountRepository = exports.AssetInVaultAccountRepository = class AssetInVaultAccountRepository {
    constructor() { }
    getList(vaultAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select asset_in_vault_accounts.asset_in_vault_account_id
          , networks.network_id
          , networks.code as network_code
          , networks.name as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets.name as asset_name
          , assets.image as asset_image
          , (asset_in_vault_accounts.total - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as total
          , (asset_in_vault_accounts.available - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as available
          , asset_in_vault_accounts.pending
          , asset_in_vault_accounts.lockedamount	
          , asset_in_vault_accounts.asset_address
          , COALESCE((asset_in_vault_accounts.total * COALESCE(asset_rates.selling,0)),0)::numeric as total_buying_amount
          , COALESCE((asset_in_vault_accounts.total * COALESCE(asset_rates.buying,0)),0)::numeric as total_selling_amount
          , CONCAT(users.first_name,' ', users.last_name) as updated_by
          , asset_in_vault_accounts.updated_date
        from vault_accounts
          inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
          inner join assets on assets.asset_id = asset_in_vault_accounts.asset_id and assets.status = 'A'
          inner join networks on networks.network_id = assets.network_id and networks.status = 'A'
          left join asset_rates on asset_rates.asset_id = asset_in_vault_accounts.asset_id
          left join users on users.user_id = NULLIF(asset_in_vault_accounts.updated_by, '')::int
        where asset_in_vault_accounts.vault_account_id = $1
        order by asset_in_vault_accounts.asset_in_vault_account_id`;
                return yield connection_1.db.query(sql, [vaultAccountId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select asset_in_vault_accounts.asset_in_vault_account_id
          , networks.network_id
          , networks.code as network_code
          , networks.name as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets.name as asset_name
          , assets.image as asset_image
          , (asset_in_vault_accounts.total - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as total
          , (asset_in_vault_accounts.available - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as available
          , asset_in_vault_accounts.pending
          , asset_in_vault_accounts.lockedamount	
          , asset_in_vault_accounts.asset_address
          , COALESCE((asset_in_vault_accounts.total * COALESCE(asset_rates.selling,0)),0)::numeric as total_buying_amount
          , COALESCE((asset_in_vault_accounts.total * COALESCE(asset_rates.buying,0)),0)::numeric as total_selling_amount
          , CONCAT(users.first_name,' ', users.last_name) as updated_by
          , asset_in_vault_accounts.updated_date
          , COALESCE(asset_rates.buying,0) as buying
          , COALESCE(asset_rates.selling,0) as selling
        from vault_accounts
          inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
          inner join assets on assets.asset_id = asset_in_vault_accounts.asset_id and assets.status = 'A'
          inner join networks on networks.network_id = assets.network_id and networks.status = 'A'
          left join asset_rates on asset_rates.asset_id = asset_in_vault_accounts.asset_id
          left join users on users.user_id = NULLIF(asset_in_vault_accounts.updated_by, '')::int
        where vault_accounts.status = 'A'
        and asset_in_vault_accounts.status = 'A'
        and vault_accounts.user_id = $1
        order by asset_in_vault_accounts.asset_in_vault_account_id`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCryptoWalletList(vaultAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select user_vault.asset_in_vault_account_id
          , networks.network_id
          , networks.code as network_code
          , networks.name as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets.name as asset_name
          , assets.image as asset_image
          , COALESCE(user_vault.total,0)::numeric as total
          , COALESCE(user_vault.available,0)::numeric as available
          , COALESCE(user_vault.pending,0)::numeric as pending
          , COALESCE(user_vault.lockedamount,0)::numeric as lockedamount
          , user_vault.asset_address
          , COALESCE((user_vault.total * COALESCE(asset_rates.selling,0)),0)::numeric as total_buying_amount
          , COALESCE((user_vault.total * COALESCE(asset_rates.buying,0)),0)::numeric as total_selling_amount
          , CASE WHEN user_vault.asset_in_vault_account_id is null THEN 'H'
                  WHEN user_vault.status = 'I' THEN 'H'
                  WHEN user_vault.status = 'H' THEN 'P'
                  else user_vault.status
            end as status
          , CASE WHEN user_vault.asset_in_vault_account_id is null THEN 99
                  WHEN user_vault.status = 'A' THEN 1
                  WHEN user_vault.status = 'H' THEN 2
                  WHEN user_vault.status = 'P' THEN 3
                  WHEN user_vault.status = 'I' THEN 4
                  else 98
            end as order_status
        from assets
          inner join networks on networks.network_id = assets.network_id and networks.status = 'A'
          left join (
            select asset_in_vault_accounts.asset_in_vault_account_id
              , asset_in_vault_accounts.asset_id
              , asset_in_vault_accounts.asset_address
              , (asset_in_vault_accounts.total - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as total
              , (asset_in_vault_accounts.available - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as available
              , asset_in_vault_accounts.pending
              , asset_in_vault_accounts.lockedamount
              , asset_in_vault_accounts.status
            from vault_accounts
              inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
            where vault_accounts.vault_account_id = $1
          ) user_vault on user_vault.asset_id = assets.asset_id
          left join asset_rates on asset_rates.asset_id = assets.asset_id
        where assets.status = 'A'
        order by order_status, networks.code, assets.code`;
                return yield connection_1.db.query(sql, [vaultAccountId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoById(assetInVaultAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select vault_accounts.vault_account_id
          , vault_accounts.fireblock_vault_account_id
          , asset_in_vault_accounts.asset_in_vault_account_id
          , (asset_in_vault_accounts.total - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as total
          , (asset_in_vault_accounts.available - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as available
          , asset_in_vault_accounts.pending
          , asset_in_vault_accounts.lockedamount
          , asset_in_vault_accounts.asset_address
          , asset_in_vault_accounts.status
          , networks.network_id
          , networks.code as network_code
          , networks.name as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets.name as asset_name
          , assets.image as asset_image
        from vault_accounts 
        	inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
          inner join assets on assets.asset_id = asset_in_vault_accounts.asset_id
          inner join networks on networks.network_id = assets.network_id
        where asset_in_vault_accounts.asset_in_vault_account_id = $1`;
                return yield connection_1.db.query(sql, [assetInVaultAccountId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByVaultAccountIdAssetCode(vaultAccountId, assetCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select asset_in_vault_accounts.asset_in_vault_account_id
          , (asset_in_vault_accounts.total - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as total
          , (asset_in_vault_accounts.available - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as available
          , asset_in_vault_accounts.pending
          , asset_in_vault_accounts.lockedamount
          , asset_in_vault_accounts.asset_address
          , asset_in_vault_accounts.status
          , networks.network_id
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
        from vault_accounts
            inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
            inner join assets on assets.asset_id = asset_in_vault_accounts.asset_id and assets.status = 'A'
            inner join networks on networks.network_id = assets.network_id and networks.status = 'A'
        where vault_accounts.vault_account_id = $1
        and assets.code = $2`;
                return yield connection_1.db.query(sql, [vaultAccountId, assetCode]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByVaultAccountIdAssetCodeIgnoreStatus(vaultAccountId, assetCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select asset_in_vault_accounts.asset_in_vault_account_id
          , (asset_in_vault_accounts.total - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as total
          , (asset_in_vault_accounts.available - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as available
          , asset_in_vault_accounts.pending
          , asset_in_vault_accounts.lockedamount
          , asset_in_vault_accounts.asset_address
          , asset_in_vault_accounts.status
          , networks.network_id
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
        from vault_accounts
          inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
          inner join assets on assets.asset_id = asset_in_vault_accounts.asset_id
          inner join networks on networks.network_id = assets.network_id
        where asset_in_vault_accounts.vault_account_id = $1
        and assets.code = $2`;
                return yield connection_1.db.query(sql, [vaultAccountId, assetCode]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListPending() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select vault_accounts.vault_account_id
          , vault_accounts.user_id
          , asset_in_vault_accounts.asset_in_vault_account_id
          , asset_in_vault_accounts.vault_account_id
          , asset_in_vault_accounts.asset_id
          , asset_in_vault_accounts.asset_address
          , (asset_in_vault_accounts.total - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as total
          , (asset_in_vault_accounts.available - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as available
          , asset_in_vault_accounts.pending
          , asset_in_vault_accounts.lockedamount
          , asset_in_vault_accounts.status
          , asset_in_vault_accounts.step_process
          , assets.code as asset_code
        from vault_accounts
            inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
                                              and asset_in_vault_accounts.status = 'P'
            inner join assets on assets.asset_id = asset_in_vault_accounts.asset_id
                              and assets.status = 'A'`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(vaultAccountId, assetId, assetCode, assetAddress, total, available, pending, lockedamount, status, stepProcess, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO asset_in_vault_accounts(vault_account_id, asset_id, asset_code, asset_address, total, available, pending, lockedamount, status, step_process, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, CURRENT_TIMESTAMP, $11, CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    vaultAccountId,
                    assetId,
                    assetCode,
                    assetAddress,
                    total,
                    available,
                    pending,
                    lockedamount,
                    status,
                    stepProcess,
                    logOnId,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateBalance(vaultAccountId, assetCode, total, available, pending, lockedamount) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE asset_in_vault_accounts
        SET total = $3
          , available = $4
          , pending = $5
          , lockedamount = $6
          , updated_date = CURRENT_TIMESTAMP
        WHERE vault_account_id = $1
        AND asset_code = $2
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    vaultAccountId,
                    assetCode,
                    total,
                    available,
                    pending,
                    lockedamount,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateStatus(assetInVaultAccountId, status) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE asset_in_vault_accounts
        SET status = $2
          , updated_date = CURRENT_TIMESTAMP
        WHERE asset_in_vault_account_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [assetInVaultAccountId, status]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateStatusWithAddress(assetInVaultAccountId, status, assetAddress) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE asset_in_vault_accounts
        SET status = $2
          , asset_address = $3
          , updated_date = CURRENT_TIMESTAMP
        WHERE asset_in_vault_account_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [assetInVaultAccountId, status, assetAddress]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateAddress(assetInVaultAccountId, assetAddress) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE asset_in_vault_accounts
        SET asset_address = $2
        WHERE asset_in_vault_account_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [assetInVaultAccountId, assetAddress]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateStepProcess(assetInVaultAccountId, stepProcess) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE asset_in_vault_accounts
        SET step_process = $2
        WHERE asset_in_vault_account_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [assetInVaultAccountId, stepProcess]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    syncBalanceFromFireblocks(assetInVaultAccountId, total, available, pending, lockedamount) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE asset_in_vault_accounts
        SET total = $2
          , available = $3
          , pending = $4
          , lockedamount = $5
        WHERE asset_in_vault_account_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    assetInVaultAccountId,
                    total,
                    available,
                    pending,
                    lockedamount,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getSummaryFiatBalanceForCompany(assetGroup) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select COALESCE(sum(summary_vault.total),0)::numeric as total
        from (
          select (asset_in_vault_accounts.total * COALESCE(asset_rates.buying,0)) as total
          from vault_accounts
            inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
                              and asset_in_vault_accounts.status = 'A'
            inner join assets on assets.asset_id = asset_in_vault_accounts.asset_id
                    and assets.asset_group = $1
                    and assets.status = 'A'
            inner join asset_rates on asset_rates.asset_id = asset_in_vault_accounts.asset_id
          where vault_accounts.status = 'A'
        ) as summary_vault;`;
                return yield connection_1.db.query(sql, [assetGroup]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getSummaryAssetForCustomerByAssetGroup(assetGroup) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select COALESCE(sum(user_vault.total),0)::numeric as total
        from (
          select (asset_in_vault_accounts.total - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H'))) as total
          from vault_accounts
            inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
                              and asset_in_vault_accounts.status = 'A'
            inner join assets on assets.asset_id = asset_in_vault_accounts.asset_id
                    and assets.asset_group = $1
                    and assets.status = 'A'
          where vault_accounts.status = 'A'
          and vault_accounts.type = 'U'
        ) as user_vault;`;
                return yield connection_1.db.query(sql, [assetGroup]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getSummaryAssetForCompanyByAssetGroup(companyId, assetGroup) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select COALESCE(sum(company_vault.total),0)::numeric as total
        from (
          select asset_in_vault_accounts.total as total
          from vault_accounts
            inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
                              and asset_in_vault_accounts.status = 'A'
            inner join assets on assets.asset_id = asset_in_vault_accounts.asset_id
                    and assets.asset_group = $2
                    and assets.status = 'A'
          where vault_accounts.status = 'A'
          and vault_accounts.company_id = $1
        ) as company_vault;`;
                return yield connection_1.db.query(sql, [companyId, assetGroup]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getSummaryAssetForUserByAssetGroup(userId, assetGroup) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select COALESCE(sum(user_vault.total),0)::numeric as total
        from (
          select (asset_in_vault_accounts.total - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H'))) as total
          from vault_accounts
            inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
                              and asset_in_vault_accounts.status = 'A'
            inner join assets on assets.asset_id = asset_in_vault_accounts.asset_id
                    and assets.asset_group = $2
                    and assets.status = 'A'
          where vault_accounts.status = 'A'
          and vault_accounts.user_id = $1
        ) as user_vault;`;
                return yield connection_1.db.query(sql, [userId, assetGroup]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAddressByUserIdAssetNetwork(userId, assetCode, networkName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select asset_in_vault_accounts.*
      from vault_accounts
        inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
                          and asset_in_vault_accounts.status = 'A'
        inner join assets on assets.asset_id = asset_in_vault_accounts.asset_id
                      and assets.status = 'A'
                      and assets.name = $2
        inner join networks on networks.network_id = assets.network_id
                  and networks.status = 'A'
                  and networks."name" = $3
      where vault_accounts.status = 'A'
      and vault_accounts.user_id = $1;`;
                return yield connection_1.db.query(sql, [userId, assetCode, networkName]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListByUserIdNetworkId(userId, networkId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select asset_in_vault_accounts.asset_in_vault_account_id
          , networks.network_id
          , networks.code as network_code
          , networks.name as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets.name as asset_name
          , assets.image as asset_image
          , (asset_in_vault_accounts.total - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as total
          , (asset_in_vault_accounts.available - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = vault_accounts.user_id and transactions.asset_code = asset_in_vault_accounts.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as available
          , asset_in_vault_accounts.pending
          , asset_in_vault_accounts.lockedamount	
          , asset_in_vault_accounts.asset_address
          , COALESCE((asset_in_vault_accounts.total * COALESCE(asset_rates.selling,0)),0)::numeric as total_buying_amount
          , COALESCE((asset_in_vault_accounts.total * COALESCE(asset_rates.buying,0)),0)::numeric as total_selling_amount
          , CONCAT(users.first_name,' ', users.last_name) as updated_by
          , asset_in_vault_accounts.updated_date
          , COALESCE(asset_rates.buying,0) as buying
          , COALESCE(asset_rates.selling,0) as selling
        from vault_accounts
          inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
          inner join assets on assets.asset_id = asset_in_vault_accounts.asset_id and assets.status = 'A'
          inner join networks on networks.network_id = assets.network_id and networks.status = 'A'
          left join asset_rates on asset_rates.asset_id = asset_in_vault_accounts.asset_id
          left join users on users.user_id = NULLIF(asset_in_vault_accounts.updated_by, '')::int
        where vault_accounts.status = 'A'
        and asset_in_vault_accounts.status = 'A'
        and vault_accounts.user_id = $1
        and networks.network_id = $2
        order by asset_in_vault_accounts.asset_in_vault_account_id`;
                return yield connection_1.db.query(sql, [userId, networkId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.AssetInVaultAccountRepository = AssetInVaultAccountRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], AssetInVaultAccountRepository);
//# sourceMappingURL=assetInVaultAccountRepository.js.map