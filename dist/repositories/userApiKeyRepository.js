"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserApiKeyRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let UserApiKeyRepository = exports.UserApiKeyRepository = class UserApiKeyRepository {
    constructor() { }
    getList(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM user_api_keys
        WHERE user_id = $1
        ORDER BY user_api_key_id`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoById(userApiKeyId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM user_api_keys
        WHERE user_api_key_id = $1`;
                return yield connection_1.db.query(sql, [userApiKeyId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByName(userId, name) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM user_api_keys
        WHERE user_id = $1
        AND name = $2`;
                return yield connection_1.db.query(sql, [userId, name]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByKey(userKey) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM user_api_keys
        where status = 'A'
        and user_key = $1`;
                return yield connection_1.db.query(sql, [userKey]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    isDuplicateByName(userId, name) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM user_api_keys
        WHERE user_id = $1
        AND lower("name") = lower($2)`;
                return yield connection_1.db.query(sql, [userId, name]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(userId, name, userKey, userSecret, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO user_api_keys(user_id, "name", user_key, user_secret, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, 'A', $5, CURRENT_TIMESTAMP, $5, CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [userId, name, userKey, userSecret, logOnId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    enableOrDisable(userId, userApiKeyId, status, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE user_api_keys
        SET status = $3
            , updated_by = $4
            , updated_date = CURRENT_TIMESTAMP
        WHERE user_id = $1
        AND user_api_key_id = $2
        RETURNING *`;
                return yield connection_1.db.query(sql, [userId, userApiKeyId, status, logOnId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    disableAllByUserId(userId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE user_api_keys
        SET status = 'I'
            , updated_by = $2
            , updated_date = CURRENT_TIMESTAMP
        WHERE user_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [userId, logOnId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.UserApiKeyRepository = UserApiKeyRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], UserApiKeyRepository);
//# sourceMappingURL=userApiKeyRepository.js.map