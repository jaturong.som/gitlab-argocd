"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WalletCreditRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let WalletCreditRepository = exports.WalletCreditRepository = class WalletCreditRepository {
    constructor() { }
    getList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select wallet_credits.wallet_credit_id
        , wallet_credits.wallet_credit_date
        , wallet_credits.wallet_credit_no
        , wallet_credits.user_id
        , wallet_credits."type"
        , wallet_credits.amount
        , wallet_credits.currency_code
        , wallet_credits.status
        , concat(users.first_name, ' ', users.last_name) as customer_name
      from wallet_credits
        inner join users on users.user_id = wallet_credits.user_id
      order by wallet_credits.wallet_credit_date desc`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select wallet_credits.wallet_credit_id
        , wallet_credits.wallet_credit_date
        , wallet_credits.wallet_credit_no
        , wallet_credits.user_id
        , wallet_credits."type"
        , wallet_credits.amount
        , wallet_credits.currency_code
        , wallet_credits.status
        , concat(users.first_name, ' ', users.last_name) as customer_name
      from wallet_credits
        inner join users on users.user_id = wallet_credits.user_id
      where wallet_credits.user_id = $1`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoById(walletCreditId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select wallet_credits.*
          , CONCAT(users.first_name,' ', users.last_name) as operator_by
        from wallet_credits
          left join users on users.user_id = NULLIF(wallet_credits.created_by, '')::int
        where wallet_credits.status = 'S'
        and wallet_credits.wallet_credit_id = $1`;
                return yield connection_1.db.query(sql, [walletCreditId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(walletCreditNo, userId, type, amount, currencyCode, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO wallet_credits(wallet_credit_date, wallet_credit_no, user_id, "type", amount, currency_code, status, created_by, created_date, updated_by, updated_date)
        VALUES(CURRENT_TIMESTAMP, $1, $2, $3, $4, $5, 'S', $6, CURRENT_TIMESTAMP, $6, CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    walletCreditNo,
                    userId,
                    type,
                    amount,
                    currencyCode,
                    logOnId,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.WalletCreditRepository = WalletCreditRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], WalletCreditRepository);
//# sourceMappingURL=walletCreditRepository.js.map