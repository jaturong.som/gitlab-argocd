"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogsRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let LogsRepository = exports.LogsRepository = class LogsRepository {
    constructor() { }
    create(
    // requestId: string,
    logsType, logsName, logsMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO logs(logs_type, logs_name, logs_message, created_date)
        VALUES($1, $2, $3,CURRENT_TIMESTAMP)
        RETURNING *`;
                // const sql = `
                // INSERT INTO users(request_id, logs_type, logs_name, logs_message, created_date)
                // VALUES($1, $2, $3, $4,CURRENT_TIMESTAMP)
                // RETURNING *`;
                return yield connection_1.db.query(sql, [
                    // requestId,
                    logsType,
                    logsName,
                    logsMessage,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.LogsRepository = LogsRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], LogsRepository);
//# sourceMappingURL=logsRepository.js.map