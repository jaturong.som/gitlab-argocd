"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CactusWalletRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let CactusWalletRepository = exports.CactusWalletRepository = class CactusWalletRepository {
    constructor() { }
    getInfo(type, companyId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let sql = "";
                if (type === "C") {
                    sql = `
            select *
            from cactus_wallets
            where company_id = $1`;
                }
                else {
                    sql = `
            select *
            from cactus_wallets
            where user_id = $2`;
                }
                return yield connection_1.db.query(sql, [companyId, userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(type, bid, businessName, walletCode, walletType, storageType, logOnId, companyId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO cactus_wallets("type", company_id, user_id, bid, business_name, wallet_code, wallet_type, storage_type, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, $6, $7 ,$8 ,'A', $9, CURRENT_TIMESTAMP, $9, CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    type,
                    companyId,
                    userId,
                    bid,
                    businessName,
                    walletCode,
                    walletType,
                    storageType,
                    logOnId,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.CactusWalletRepository = CactusWalletRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], CactusWalletRepository);
//# sourceMappingURL=cactusWalletRepository.js.map