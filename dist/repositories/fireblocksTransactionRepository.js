"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FireblocksTransactionRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let FireblocksTransactionRepository = exports.FireblocksTransactionRepository = class FireblocksTransactionRepository {
    constructor() { }
    getList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select *
        from fireblock_transactions`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListByStatus(status) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select *
        from fireblock_transactions
        where status = $1`;
                return yield connection_1.db.query(sql, [status]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByStatusRefId(status, refId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select *
        from fireblock_transactions
        where status = $1
        and ref_id = $2`;
                return yield connection_1.db.query(sql, [status, refId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoById(fireblockTransactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select *
        from fireblock_transactions
        where fireblock_transaction_id = $1`;
                return yield connection_1.db.query(sql, [fireblockTransactionId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByfbId(fbTransactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select *
        from fireblock_transactions
        where fb_transaction_id = $1`;
                return yield connection_1.db.query(sql, [fbTransactionId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByTypeRefId(type, ref_id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select *
        from fireblock_transactions
        where "type" = $1
        and ref_id = $2`;
                return yield connection_1.db.query(sql, [type, ref_id]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(fbTransactionId, type, assetId, sourceId, sourceType, amount, fee, refId, status, destinationId, destinationAddress, destinationType) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO fireblock_transactions(fb_transaction_id, "type", asset_id, source_id, source_type, destination_id, destination_address, destination_type, amount, fee, remark, ref_id, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, '', $11, $12, '0', CURRENT_TIMESTAMP, '0', CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    fbTransactionId,
                    type,
                    assetId,
                    sourceId,
                    sourceType,
                    destinationId,
                    destinationAddress,
                    destinationType,
                    amount,
                    fee,
                    refId,
                    status,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateStatusFromFireblocks(fireblockTransactionId, status, remark, fbTransactionHash) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE fireblock_transactions
        SET status = $2
        , remark = $3
        , fb_transaction_hash = $4
        , updated_date = CURRENT_TIMESTAMP
        WHERE fireblock_transaction_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    fireblockTransactionId,
                    status,
                    remark,
                    fbTransactionHash,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.FireblocksTransactionRepository = FireblocksTransactionRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], FireblocksTransactionRepository);
//# sourceMappingURL=fireblocksTransactionRepository.js.map