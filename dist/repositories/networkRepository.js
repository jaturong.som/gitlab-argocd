"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NetworkRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let NetworkRepository = exports.NetworkRepository = class NetworkRepository {
    constructor() { }
    getList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      SELECT networks.*
          , CONCAT(users.first_name,' ', users.last_name) as fullname_created_by
      FROM networks
        left join users on users.user_id = NULLIF(networks.created_by, '')::int
      WHERE networks.status = 'A'
      ORDER BY networks.network_id`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListForAssetRates() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      SELECT networks.*
          , CONCAT(users.first_name,' ', users.last_name) as fullname_created_by
      FROM networks
        left join users on users.user_id = NULLIF(networks.created_by, '')::int
      WHERE networks.status = 'A'
      and networks.network_id in (
            select network_id
            from assets
              inner join asset_rates on asset_rates.asset_code = assets.code
            where assets.status = 'A')
      ORDER BY networks.network_id`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select networks.*
        from vault_accounts
          inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
                            and asset_in_vault_accounts.status = 'A'
          inner join assets on assets.code = asset_in_vault_accounts.asset_code
                    and assets.status = 'A'
          inner join networks on networks.network_id = assets.network_id
                    and networks.status = 'A'
        where vault_accounts.status = 'A'
        and vault_accounts.user_id = $1`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.NetworkRepository = NetworkRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], NetworkRepository);
//# sourceMappingURL=networkRepository.js.map