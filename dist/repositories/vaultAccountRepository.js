"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VaultAccountRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let VaultAccountRepository = exports.VaultAccountRepository = class VaultAccountRepository {
    constructor() { }
    getInfo(type, companyId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let sql = "";
                if (type === "C") {
                    sql = `
          select companies.company_id as id
            , companies."name" as "name"
            , vault_accounts.vault_account_id
            , vault_accounts.vault_account_name
            , vault_accounts.fireblock_vault_account_id
            , vault_accounts.status
          from vault_accounts
            inner join companies on companies.company_id = vault_accounts.company_id
                                and companies.status = 'A'
          where vault_accounts.status = 'A'
          and companies.company_id = $1`;
                }
                else {
                    sql = `
          select users.user_id as id
            , CONCAT(users.first_name, ' ', users.last_name) as "name"
            , vault_accounts.vault_account_id
            , vault_accounts.vault_account_name
            , vault_accounts.fireblock_vault_account_id
            , vault_accounts.status
          from vault_accounts
            inner join users on users.user_id = vault_accounts.user_id
                            and users.status = 'A'
          where vault_accounts.status = 'A'
          and users.user_id = $2`;
                }
                return yield connection_1.db.query(sql, [companyId, userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByFireblocksVaultAccountId(fireblocksVaultAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select users.user_id
          , CONCAT(users.first_name, ' ', users.last_name) as "user_full_name"
          , vault_accounts.vault_account_id
          , vault_accounts.vault_account_name
          , vault_accounts.fireblock_vault_account_id
          , vault_accounts.status
        from vault_accounts
          inner join users on users.user_id = vault_accounts.user_id
        and users.status = 'A'
        where vault_accounts.status = 'A'
        and vault_accounts.fireblock_vault_account_id = $1`;
                return yield connection_1.db.query(sql, [fireblocksVaultAccountId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    // async getInfoById(type: string, vaultAccountId: number) {
    //   try {
    //     let sql = "";
    //     if (type === "C") {
    //       sql = `
    //         select companies.company_id as id
    //           , companies."name" as "name"
    //           , vault_accounts.vault_account_id
    //           , vault_accounts.vault_account_name
    //           , vault_accounts.fireblock_vault_account_id
    //           , vault_accounts.status
    //         from vault_accounts
    //           inner join companies on companies.company_id = vault_accounts.company_id
    //                               and companies.status = 'A'
    //         where vault_accounts.status = 'A'
    //         and vault_accounts.vault_account_id = $1`;
    //     } else {
    //       sql = `
    //         select users.user_id as id
    //           , CONCAT(users.first_name, ' ', users.last_name) as "name"
    //           , vault_accounts.vault_account_id
    //           , vault_accounts.vault_account_name
    //           , vault_accounts.fireblock_vault_account_id
    //           , vault_accounts.status
    //         from vault_accounts
    //           inner join users on users.user_id = vault_accounts.user_id
    //                           and users.status = 'A'
    //         where vault_accounts.status = 'A'
    //         and vault_accounts.vault_account_id = $1`;
    //     }
    //     return await db.query(sql, [vaultAccountId]);
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    create(type, vaultAccountName, fireblockVaultAccountId, logOnId, companyId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO vault_accounts("type", company_id, user_id, vault_account_name, fireblock_vault_account_id, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, 'A', $6, CURRENT_TIMESTAMP, $6, CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    type,
                    companyId,
                    userId,
                    vaultAccountName,
                    fireblockVaultAccountId,
                    logOnId,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.VaultAccountRepository = VaultAccountRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], VaultAccountRepository);
//# sourceMappingURL=vaultAccountRepository.js.map