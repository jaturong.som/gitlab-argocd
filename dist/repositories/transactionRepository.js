"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let TransactionRepository = exports.TransactionRepository = class TransactionRepository {
    constructor() { }
    getList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select transactions.transaction_id
          , transactions.transaction_date
          , transactions.transaction_no
          , transactions.user_id
          , concat(users.first_name, ' ', users.last_name) as customer_name
          , transactions."type"
          , transactions.source_vault_account_id
          , transactions.destination_vault_account_id
          , networks.network_id
          , networks.code as network_code
          , networks.name as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets.name as asset_name
          , assets.image as asset_image
          , transactions.buying
          , transactions.selling
          , transactions.total_amount
          , transactions.total_assets
          , transactions.currency_code
          , transactions.status
          , transactions.fb_transaction_hash
          , transactions.fb_transaction_status
        from transactions
          inner join users on users.user_id = transactions.user_id
          inner join assets on assets.asset_id = transactions.asset_id
          inner join networks on networks.network_id = assets.network_id
        order by transactions.transaction_date desc`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select transactions.transaction_id
          , transactions.transaction_date
          , transactions.transaction_no
          , transactions.user_id
          , concat(users.first_name, ' ', users.last_name) as customer_name
          , transactions."type"
          , transactions.source_vault_account_id
          , transactions.destination_vault_account_id
          , networks.network_id
          , networks.code as network_code
          , networks.name as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets.name as asset_name
          , assets.image as asset_image
          , transactions.buying
          , transactions.selling
          , transactions.total_amount
          , transactions.total_assets
          , transactions.currency_code
          , transactions.status
          , transactions.fb_transaction_hash
          , transactions.fb_transaction_status
        from transactions
          inner join users on users.user_id = transactions.user_id
          inner join assets on assets.asset_id = transactions.asset_id
          inner join networks on networks.network_id = assets.network_id
          where transactions.user_id = $1
        order by transactions.transaction_date desc`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListByStatus(status) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select transactions.*
        from transactions
        where transactions.status = $1`;
                return yield connection_1.db.query(sql, [status]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoById(transactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select transactions.transaction_id
          , transactions.transaction_date
          , transactions.transaction_no
          , transactions.user_id
          , concat(users.first_name, ' ', users.last_name) as customer_name
          , transactions."type"
          , transactions.source_vault_account_id
          , transactions.destination_vault_account_id
          , networks.network_id
          , networks.code as network_code
          , networks.name as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets.name as asset_name
          , assets.image as asset_image
          , transactions.buying
          , transactions.selling
          , transactions.total_amount
          , transactions.total_assets
          , transactions.currency_code
          , transactions.status
          , transactions.fb_transaction_hash
          , transactions.fb_transaction_status
          , vault_user.fireblock_vault_account_id as user_fireblock_vault_account_id
          , vault_com.fireblock_vault_account_id as company_fireblock_vault_account_id
        from transactions
          inner join users on users.user_id = transactions.user_id
          inner join assets on assets.asset_id = transactions.asset_id
          inner join networks on networks.network_id = assets.network_id
          inner join vault_accounts vault_user on vault_user.vault_account_id = transactions.source_vault_account_id
          inner join vault_accounts vault_com on vault_com.vault_account_id = transactions.destination_vault_account_id
        where transactions.transaction_id = $1`;
                return yield connection_1.db.query(sql, [transactionId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(userId, transactionNo, type, sourceVaultAccountId, assetId, assetCode, buying, selling, totalAmount, totalAssets, status, logOnId, source, is_manual_mode, wallet_network, wallet_address, destinationVaultAccountId, destinationAddress) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO transactions(transaction_date, user_id, transaction_no, "type", source_vault_account_id, destination_vault_account_id, asset_id, asset_code, buying, selling, total_amount, total_assets, currency_code, completion_date, status, created_by, created_date, updated_by, updated_date, source, destination_address, is_manual_mode, wallet_network, wallet_address)
        VALUES(CURRENT_TIMESTAMP, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, 'USD', NULL, $12, $13, CURRENT_TIMESTAMP, $13, CURRENT_TIMESTAMP, $14, $15, $16, $17, $18)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    userId,
                    transactionNo,
                    type,
                    sourceVaultAccountId,
                    destinationVaultAccountId,
                    assetId,
                    assetCode,
                    buying,
                    selling,
                    totalAmount,
                    totalAssets,
                    status,
                    logOnId,
                    source,
                    destinationAddress,
                    is_manual_mode,
                    wallet_network,
                    wallet_address,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateTransactionWithFBIdStatus(transactionId, status, fbTransactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE transactions
        SET status = $2
          , fb_transaction_id = $3
          , updated_date = CURRENT_TIMESTAMP
        WHERE transaction_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [transactionId, status, fbTransactionId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateTransactionStatus(transactionId, status) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE transactions
        SET status = $2
          , updated_date = CURRENT_TIMESTAMP
        WHERE transaction_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [transactionId, status]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateTransactionStatusFromFireblocks(transactionId, status, fbTransactionHash, fbTransactionStatus) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE transactions
        SET status = $2
          , fb_transaction_hash = $3
          , fb_transaction_status = $4
          , completion_date = CURRENT_TIMESTAMP
        WHERE transaction_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    transactionId,
                    status,
                    fbTransactionHash,
                    fbTransactionStatus,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionByTransactionID(transactionID) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select t.fb_transaction_id,t.fb_transaction_status,t.fb_transaction_hash,t.total_amount,a.code, n.name, t.type,t.transaction_date
      from transactions t, assets a ,networks n
      where t.asset_id = a.asset_id
        and a.network_id = n.network_id
        and    fb_transaction_id = $1`;
                return yield connection_1.db.query(sql, [transactionID]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionByTransactionHash(transactionHash, networkName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select t.fb_transaction_id,t.fb_transaction_status,t.fb_transaction_hash,t.total_amount,a.code, n.name, t.type,t.transaction_date
      from transactions t, assets a ,networks n
      where t.asset_id = a.asset_id
        and a.network_id = n.network_id
        and t.fb_transaction_hash = $1
        and n.name = $2`;
                return yield connection_1.db.query(sql, [transactionHash, networkName]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionListPageSize(pageSize, pageCursor, fromDate, toDate, assetCode, networkName, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select t.fb_transaction_id,t.fb_transaction_status,t.fb_transaction_hash,t.total_amount,a.code, n.name, t.type,t.transaction_date
      from transactions t, assets a ,networks n
      where t.asset_id = a.asset_id
      and a.network_id = n.network_id
      and a.code = $5
      and n.name = $6
      and user_id = $7
      and transaction_date between $3 and $4
      order by transaction_date desc
      limit $1
      offset (($2-1) * $1)`;
                return yield connection_1.db.query(sql, [
                    pageSize,
                    pageCursor,
                    fromDate,
                    toDate,
                    assetCode,
                    networkName,
                    userId,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionLogs(pageSize, pageCursor, fromDate, toDate, transactionType, status, assetName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let sql = `
      select source_vault.vault_account_name as "source_account_name", destination_vault.vault_account_name as "destination_account_name", fireblock_transactions.*
      from fireblock_transactions
         left join vault_accounts source_vault on source_vault.fireblock_vault_account_id = fireblock_transactions.source_id
         left join vault_accounts destination_vault on destination_vault.fireblock_vault_account_id = fireblock_transactions.destination_id`;
                let filter = ``;
                if (fromDate != "" &&
                    fromDate != undefined &&
                    toDate != "" &&
                    toDate != undefined) {
                    filter = `fireblock_transactions.created_date between  $3 and $4`;
                }
                if (transactionType != "" && transactionType != undefined) {
                    if (filter != ``) {
                        filter = filter + ` and `;
                    }
                    filter = filter + ` fireblock_transactions.type = $5`;
                }
                if (status != "" && status != undefined) {
                    if (filter != ``) {
                        filter = filter + ` and `;
                    }
                    filter = filter + ` fireblock_transactions.status = $6`;
                }
                if (assetName != "" && assetName != undefined) {
                    if (filter != ``) {
                        filter = filter + ` and `;
                    }
                    filter = filter + ` fireblock_transactions.asset_id = $7`;
                }
                if (filter != ``) {
                    filter =
                        ` where ` +
                            filter +
                            ` order by fireblock_transactions.created_date desc
        limit $1
        offset (($2-1) * $1)`;
                    sql = sql + filter;
                }
                else {
                    sql = sql + ` order by created_date desc limit $1 offset (($2-1) * $1)`;
                }
                return yield connection_1.db.query(sql, [
                    pageSize,
                    pageCursor,
                    fromDate,
                    toDate,
                    transactionType,
                    status,
                    assetName,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionLogsTotal(fromDate, toDate, transactionType, status, assetName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let sql = `
      select count(*)
      from fireblock_transactions
         left join vault_accounts source_vault on source_vault.fireblock_vault_account_id = fireblock_transactions.source_id
         left join vault_accounts destination_vault on destination_vault.fireblock_vault_account_id = fireblock_transactions.destination_id`;
                let fliter = ``;
                if (fromDate != "" &&
                    fromDate != undefined &&
                    toDate != "" &&
                    toDate != undefined) {
                    fliter = `fireblock_transactions.created_date between  $1 and $2`;
                }
                if (transactionType != "" && transactionType != undefined) {
                    if (fliter != ``) {
                        fliter = fliter + ` and `;
                    }
                    fliter = fliter + ` fireblock_transactions.type = $3`;
                }
                if (status != "" && status != undefined) {
                    if (fliter != ``) {
                        fliter = fliter + ` and `;
                    }
                    fliter = fliter + ` fireblock_transactions.status = $4`;
                }
                if (assetName != "" && assetName != undefined) {
                    if (fliter != ``) {
                        fliter = fliter + ` and `;
                    }
                    fliter = fliter + ` fireblock_transactions.asset_id = $5`;
                }
                if (fliter != ``) {
                    sql = sql + ` where ` + fliter;
                }
                return yield connection_1.db.query(sql, [
                    fromDate,
                    toDate,
                    transactionType,
                    status,
                    assetName,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.TransactionRepository = TransactionRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], TransactionRepository);
//# sourceMappingURL=transactionRepository.js.map