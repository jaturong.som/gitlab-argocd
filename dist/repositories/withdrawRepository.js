"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WithdrawRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let WithdrawRepository = exports.WithdrawRepository = class WithdrawRepository {
    constructor() { }
    //admin
    getList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select withdraws.withdraw_id
          , withdraws.withdraw_date
          , withdraws.withdraw_no
          , withdraws.user_id
          , concat(users.first_name, ' ', users.last_name) as customer_name
          , withdraws.type
          , case when withdraws.type = 'CV' then withdraws.amount::varchar else lqn_transactions.transfer_amount end as transfer_amount
          , withdraws.currency_code
          , withdraws.purpose_of_transfer
          , withdraws.purpose_of_transfer_other
          , withdraws.relationship_to_receiver
          , withdraws.relationship_to_receiver_other
          , withdraws.status
          , withdraws.completion_date
          , bank_accounts.bank_account_id
          , bank_accounts.bank_id
          , bank_accounts.bank_id as bank_code
          , bank_accounts.bank_name
          , bank_accounts.account_no
          , bank_accounts.account_name
          , bank_accounts.swift_code
          , bank_accounts.iban
          , withdraws.reason
          , lqn_transactions.status as lqn_status
          , case when withdraws.type = 'CV' then withdraws.amount::varchar else lqn_transactions.collect_amount end as collect_amount
        from withdraws
          left join bank_accounts on bank_accounts.bank_account_id = withdraws.bank_account_id
          inner join users on users.user_id = withdraws.user_id
          left join lqn_transactions on lqn_transactions.lqn_transaction_id = withdraws.lqn_transaction_id
        order by withdraws.withdraw_date desc`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select withdraws.withdraw_id
          , withdraws.withdraw_date
          , withdraws.withdraw_no
          , withdraws.user_id
          , concat(users.first_name, ' ', users.last_name) as customer_name
          , withdraws.type
          , withdraws.amount
          , withdraws.currency_code
          , withdraws.purpose_of_transfer
          , withdraws.purpose_of_transfer_other
          , withdraws.relationship_to_receiver
          , withdraws.relationship_to_receiver_other
          , withdraws.status
          , withdraws.completion_date
          , bank_accounts.bank_account_id
          , bank_accounts.bank_id
          , bank_accounts.bank_id as bank_code
          , bank_accounts.bank_name
          , bank_accounts.account_no
          , bank_accounts.account_name
          , bank_accounts.swift_code
          , bank_accounts.iban
          , case when withdraws.type = 'CV' then withdraws.amount::varchar else lqn_transactions.transfer_amount end as transfer_amount
          , lqn_transactions.status as lqn_status
          , case when withdraws.type = 'CV' then withdraws.amount::varchar else lqn_transactions.collect_amount end as collect_amount
        from withdraws
          inner join users on users.user_id = withdraws.user_id
          left join bank_accounts on bank_accounts.bank_account_id = withdraws.bank_account_id
          left join lqn_transactions on lqn_transactions.lqn_transaction_id = withdraws.lqn_transaction_id
        where withdraws.user_id = $1
        order by withdraws.withdraw_date desc`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoById(withdrawId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select withdraws.withdraw_id,
          withdraws.withdraw_date,
          withdraws.withdraw_no,
          withdraws."type",
          withdraws.completion_date,
          withdraws.status,
          sender.first_name AS sender_first_name,
          sender.middle_name AS sender_middle_name,
          sender.last_name AS sender_last_name,
          sender.mobile_number AS sender_contact_number,
          sender.email AS sender_email,
          sender_address.line1 AS sender_address,
          sender_address.state AS sender_state,
          sender_address.area_town AS sender_area_town,
          sender_address.city AS sender_city,
          sender_address.postcode AS sender_zip_code,
          sender_address.country_code AS sender_country_code,
          sender_country.name as sender_country_name,
          receiver.first_name AS receiver_first_name,
          receiver.middle_name AS receiver_middle_name,
          receiver.last_name AS receiver_last_name,
          receiver.date_of_birth AS receiver_date_of_birth,
          receiver.gender AS receiver_gender,
          receiver.mobile_number AS receiver_contact_number,
          receiver.email AS receiver_email,
          receiver.nationality AS receiver_nationality,
          receiver.occupation AS receiver_occupation,
          receiver.occupation_remark AS receiver_occupation_remarks,
          receiver.id_type AS receiver_id_type,
          receiver.id_type_remark AS receiver_id_type_remarks,
          receiver.id_number AS receiver_id_number,
          receiver.id_issue_date AS receiver_id_issue_date,
          receiver.id_expired_date AS receiver_id_expire_date,
          receiver.native_first_name AS receiver_native_first_name,
          receiver.native_middle_name AS receiver_native_middle_name,
          receiver.native_last_name AS receiver_native_last_name,
          '' AS receiver_native_address,
          '' AS receiver_account_type,
          '' AS receiver_district,
          receiver.user_type AS beneficiary_type,
          receiver.currency AS currency_code,
          receiver_address.line1 AS receiver_address,
          receiver_address.state AS receiver_state,
          receiver_address.area_town AS receiver_area_town,
          receiver_address.city AS receiver_city,
          receiver_address.postcode AS receiver_zip_code,
          receiver_address.country_code AS receiver_country_code,
          receiver_country.name as receiver_country_name,
          bank_accounts.bank_account_id,
          bank_accounts.bank_type,
          bank_accounts.bank_id AS location_id,
          bank_accounts.bank_name AS bank_name,
          bank_accounts.branch_name AS bank_branch_name,
          bank_accounts.branch_code AS bank_branch_code,
          bank_accounts.account_no AS bank_account_number,
          bank_accounts.account_name AS bank_account_name,
          bank_accounts.swift_code AS swift_code,
          bank_accounts.iban AS iban,
          lqn_transactions.lqn_transaction_id,
          lqn_transactions.lqn_transaction_date,
          lqn_transactions.lqn_transaction_type,
          lqn_transactions.customer_id,
          lqn_transactions.receiver_id,
          lqn_transactions.agent_session_id,
          lqn_transactions.confirmation_id,
          lqn_transactions.agent_txn_id,
          lqn_transactions.collect_amount,
          lqn_transactions.collect_currency,
          lqn_transactions.service_charge,
          lqn_transactions.gst_charge,
          lqn_transactions.transfer_amount,
          lqn_transactions.exchange_rate,
          lqn_transactions.payout_amount,
          lqn_transactions.payout_currency,
          lqn_transactions.fee_discount,
          lqn_transactions.additional_premium_rate,
          lqn_transactions.settlement_rate,
          lqn_transactions.send_commission,
          lqn_transactions.settlement_amount,
          lqn_transactions.pin_number,
          lqn_transactions.status as lqn_transaction_status,
          lqn_transactions.message,
          lqn_transactions.additionalmessage
        from withdraws
          inner join lqn_transactions on lqn_transactions.lqn_transaction_id = withdraws.lqn_transaction_id
          inner join lqn_user_profile as sender on sender.lqn_ref_id = lqn_transactions.customer_id
          inner join lqn_user_profile as receiver on receiver.lqn_ref_id = lqn_transactions.receiver_id
          inner join bank_accounts on bank_accounts.bank_account_id = receiver.bank_account_id
          left join addresses as sender_address on sender_address.ref_id = sender.user_id
                and sender_address.address_type = 'USER_PROFILE'
          left join addresses as receiver_address on receiver_address.ref_id = receiver.lqn_user_profile_id
                and receiver_address.address_type = 'LQN_USER_PROFILE'
          left join countries as sender_country on sender_country.code = sender_address.country_code
          left join countries as receiver_country on receiver_country.code = receiver_address.country_code
        where withdraws.withdraw_id = $1`;
                return yield connection_1.db.query(sql, [withdrawId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getConvertInfoById(withdrawId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select withdraws.withdraw_id
          , withdraws.withdraw_date
          , withdraws.withdraw_no
          , withdraws.user_id
          , CONCAT(users.first_name,' ', users.last_name) as operator_by
          , withdraws."type"
          , transactions.total_amount
          , transactions.total_assets
          , transactions.currency_code 
          , withdraws.status
        from withdraws
          inner join transactions on transactions.transaction_id = withdraws.transaction_id
          left join users on users.user_id = NULLIF(withdraws.created_by, '')::int
        where withdraws.withdraw_id = $1`;
                return yield connection_1.db.query(sql, [withdrawId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(withdrawNo, userId, type, bankAccountId, amount, serviceCharge, status, logOnId, transactionId, lqnTransactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO withdraws(withdraw_date, withdraw_no, user_id, "type", bank_account_id, amount, service_charge, transaction_id, lqn_transaction_id, status, created_by, created_date, updated_by, updated_date)
        VALUES(CURRENT_TIMESTAMP, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, CURRENT_TIMESTAMP, $10, CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    withdrawNo,
                    userId,
                    type,
                    bankAccountId,
                    amount,
                    serviceCharge,
                    transactionId,
                    lqnTransactionId,
                    status,
                    logOnId,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTotalPendingAmount() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select COALESCE(sum(amount),0) as total
      from withdraws
      where status = 'P';`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    update(lqnTransactionId, status) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE withdraws
        SET status = $2
        , completion_date = CURRENT_TIMESTAMP
        , updated_date = CURRENT_TIMESTAMP
        WHERE lqn_transaction_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [lqnTransactionId, status]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.WithdrawRepository = WithdrawRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], WithdrawRepository);
//# sourceMappingURL=withdrawRepository.js.map