"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupMenuRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let GroupMenuRepository = exports.GroupMenuRepository = class GroupMenuRepository {
    constructor() { }
    getList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM group_menus
        WHERE status = 'A'
        ORDER BY "order"`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListByRoleId(roleId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM group_menus
        WHERE status = 'A'
        AND group_menu_code in (
          SELECT group_menu_code
          FROM role_menus
          WHERE role_id = $1
          GROUP BY group_menu_code)
        ORDER BY "order"`;
                return yield connection_1.db.query(sql, [roleId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.GroupMenuRepository = GroupMenuRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], GroupMenuRepository);
//# sourceMappingURL=groupMenuRepository.js.map