"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserTokenRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let UserTokenRepository = exports.UserTokenRepository = class UserTokenRepository {
    constructor() { }
    getInfo(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM user_tokens
        WHERE user_id = $1`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(userId, token, refreshToken) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO user_tokens(user_id, "token", refresh_token, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, '0', CURRENT_TIMESTAMP, '0', CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [userId, token, refreshToken]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    update(userId, token, refreshToken) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE user_tokens
        SET "token" = $2
          , refresh_token = $3
          , updated_by = '0'
          , updated_date = CURRENT_TIMESTAMP
        WHERE user_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [userId, token, refreshToken]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    delete(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        DELETE FROM user_tokens
        WHERE user_id = $1`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.UserTokenRepository = UserTokenRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], UserTokenRepository);
//# sourceMappingURL=userTokenRepository.js.map