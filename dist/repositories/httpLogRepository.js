"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpLogRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let HttpLogRepository = exports.HttpLogRepository = class HttpLogRepository {
    constructor() { }
    getFireblocksNotificationList(fromDate, toDate, status, searchText) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let sql = `
        SELECT *
        FROM http_logs
        WHERE url = '/api/v1/webhook/notification'
        and (request_date::date between $1 and $2)`;
                let filter = ``;
                if (status != "" && status != undefined) {
                    filter = filter + ` and body like '%"status":"$3:value"%'`;
                }
                if (searchText != "" && searchText != undefined) {
                    searchText = searchText.toLowerCase();
                    filter = filter + ` and lower(body) like '%$4:value%'`;
                }
                if (filter != ``) {
                    filter = filter + ` order by request_date desc`;
                    sql = sql + filter;
                }
                else {
                    sql = sql + ` order by request_date desc`;
                }
                return yield connection_1.db.query(sql, [fromDate, toDate, status, searchText]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(requester, remoteAddress, method, url, header, body, response) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO http_logs(request_date, requester, remote_address, "method", url, header, body, response)
        VALUES(CURRENT_TIMESTAMP, $1, $2, $3, $4, $5, $6, $7)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    requester,
                    remoteAddress,
                    method,
                    url,
                    header,
                    body,
                    response,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.HttpLogRepository = HttpLogRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], HttpLogRepository);
//# sourceMappingURL=httpLogRepository.js.map