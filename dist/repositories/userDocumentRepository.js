"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserDocumentRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let UserDocumentRepository = exports.UserDocumentRepository = class UserDocumentRepository {
    constructor() { }
    getList(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM user_documents
        WHERE user_id = $1
        ORDER BY user_document_id`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByType(userId, documentType) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM user_documents
        WHERE user_id = $1
        and document_type = $2`;
                return yield connection_1.db.query(sql, [userId, documentType]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(userId, documentType, documentText, logOnId, documentTypeRemark, documentIssueDate, documentExpiredDate) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO user_documents(user_id, document_type, document_text, status, created_by, created_date, updated_by, updated_date,document_type_remark, document_issue_date, document_expired_date)
        VALUES($1, $2, $3, 'A', $4, CURRENT_TIMESTAMP, $4, CURRENT_TIMESTAMP, $5, $6, $7)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    userId,
                    documentType,
                    documentText,
                    logOnId,
                    documentTypeRemark,
                    documentIssueDate,
                    documentExpiredDate,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    // async update(userDocumentId: number, documentText: string, logOnId: string) {
    //   try {
    //     const sql = `
    //       UPDATE user_documents
    //       SET document_text = $2
    //           , updated_by = $3
    //           , updated_date = CURRENT_TIMESTAMP
    //       WHERE user_document_id = $1
    //       RETURNING *`;
    //     return await db.query(sql, [userDocumentId, documentText, logOnId]);
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    updateByDocType(userDocumentId, documentType, documentText, logOnId, documentTypeRemark, documentIssueDate, documentExpiredDate) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE user_documents
        SET document_type = $2
            , document_text = $3
            , updated_by = $4
            , updated_date = CURRENT_TIMESTAMP
            , document_type_remark = $5
            , document_issue_date = $6
            , document_expired_date = $7
        WHERE user_document_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    userDocumentId,
                    documentType,
                    documentText,
                    logOnId,
                    documentTypeRemark,
                    documentIssueDate,
                    documentExpiredDate,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    delete(userDocumentId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        DELETE FROM user_documents WHERE user_document_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [userDocumentId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.UserDocumentRepository = UserDocumentRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], UserDocumentRepository);
//# sourceMappingURL=userDocumentRepository.js.map