"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LqnTransactionRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let LqnTransactionRepository = exports.LqnTransactionRepository = class LqnTransactionRepository {
    constructor() { }
    create(lqnTransactionType, customerId, receiverId, agentSessionId, confirmationId, agentTxnId, collectAmount, collectCurrency, serviceCharge, gstCharge, transferAmount, exchangeRate, payoutAmount, payoutCurrency, feeDiscount, additionalPremiumRate, settlementRate, sendCommission, settlementAmount, pinNumber, status, message, additionalMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO lqn_transactions(lqn_transaction_date, lqn_transaction_type, customer_id, receiver_id, agent_session_id, confirmation_id, agent_txn_id, collect_amount, collect_currency, service_charge, gst_charge, transfer_amount, exchange_rate, payout_amount, payout_currency, fee_discount, additional_premium_rate, settlement_rate, send_commission, settlement_amount, pin_number, status, message, additionalmessage)
        VALUES(CURRENT_TIMESTAMP, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    lqnTransactionType,
                    customerId,
                    receiverId,
                    agentSessionId,
                    confirmationId,
                    agentTxnId,
                    collectAmount,
                    collectCurrency,
                    serviceCharge,
                    gstCharge,
                    transferAmount,
                    exchangeRate,
                    payoutAmount,
                    payoutCurrency,
                    feeDiscount,
                    additionalPremiumRate,
                    settlementRate,
                    sendCommission,
                    settlementAmount,
                    pinNumber,
                    status,
                    message,
                    additionalMessage,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateConfirmTransaction(confirmationId, pinNumber, status, message) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE lqn_transactions
        SET pin_number = $2
          , status = $3
          , message = $4
        WHERE confirmation_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [confirmationId, pinNumber, status, message]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateCallbackTransaction(pinNumber, status, message) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE lqn_transactions
        SET status = $2
          , message = $3
        WHERE pin_number = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [pinNumber, status, message]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.LqnTransactionRepository = LqnTransactionRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], LqnTransactionRepository);
//# sourceMappingURL=lqnTransactionRepository.js.map