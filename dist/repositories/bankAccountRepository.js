"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BankAccountRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let BankAccountRepository = exports.BankAccountRepository = class BankAccountRepository {
    constructor() { }
    // async getList(userId: number) {
    //   try {
    //     const sql = `
    //     SELECT *
    //     FROM bank_accounts
    //     WHERE status = 'A'
    //     AND user_id = $1
    //     ORDER BY bank_account_id`;
    //     return await db.query(sql, [userId]);
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    getListByBankType(userId, bankType) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `

        select bank_accounts.bank_account_id,
          bank_accounts.bank_type,
          bank_accounts.bank_id AS location_id,
          bank_accounts.bank_name AS bank_name,
          bank_accounts.branch_name AS bank_branch_name,
          bank_accounts.branch_code AS bank_branch_code,
          bank_accounts.account_no AS bank_account_number,
          bank_accounts.account_name AS bank_account_name,
          bank_accounts.swift_code AS swift_code,
          bank_accounts.iban AS iban,
          sender.first_name AS sender_first_name,
          sender.middle_name AS sender_middle_name,
          sender.last_name AS sender_last_name,
          sender.mobile_number AS sender_contact_number,
          sender.email AS sender_email,
          sender_address.line1 AS sender_address,
          sender_address.state AS sender_state,
          sender_address.area_town AS sender_area_town,
          sender_address.city AS sender_city,
          sender_address.postcode AS sender_zip_code,
          sender_address.country_code AS sender_country_code,
          sender_country.name as sender_country_name,
          receiver.first_name AS receiver_first_name,
          receiver.middle_name AS receiver_middle_name,
          receiver.last_name AS receiver_last_name,
          receiver.date_of_birth AS receiver_date_of_birth,
          receiver.gender AS receiver_gender,
          receiver.mobile_number AS receiver_contact_number,
          receiver.email AS receiver_email,
          receiver.nationality AS receiver_nationality,
          nationality_country.name as receiver_nationality_name,
          receiver.occupation AS receiver_occupation,
          receiver.occupation_remark AS receiver_occupation_remarks,
          receiver.id_type AS receiver_id_type,
          receiver.id_type_remark AS receiver_id_type_remarks,
          receiver.id_number AS receiver_id_number,
          receiver.id_issue_date AS receiver_id_issue_date,
          receiver.id_expired_date AS receiver_id_expire_date,
          receiver.native_first_name AS receiver_native_first_name,
          receiver.native_middle_name AS receiver_native_middle_name,
          receiver.native_last_name AS receiver_native_last_name,
          '' AS receiver_native_address,
          '' AS receiver_account_type,
          '' AS receiver_district,
          receiver.user_type AS beneficiary_type,
          ddl_beneficiary_type.text as beneficiary_type_text,
          receiver.currency AS currency_code,
          receiver_address.line1 AS receiver_address,
          receiver_address.state AS receiver_state,
          receiver_address.area_town AS receiver_area_town,
          receiver_address.city AS receiver_city,
          receiver_address.postcode AS receiver_zip_code,
          receiver_address.country_code AS receiver_country_code,
          receiver_country.name as receiver_country_name
        from bank_accounts
          inner join lqn_user_profile as receiver on receiver.bank_account_id = bank_accounts.bank_account_id
          inner join lqn_user_profile as sender on sender.lqn_ref_id = receiver.sender_ref_id
          left join addresses as sender_address on sender_address.ref_id = sender.user_id
          and sender_address.address_type = 'USER_PROFILE'
          left join addresses as receiver_address on receiver_address.ref_id = receiver.lqn_user_profile_id
          and receiver_address.address_type = 'LQN_USER_PROFILE'
          left join countries as sender_country on sender_country.code = sender_address.country_code
          left join countries as receiver_country on receiver_country.code = receiver_address.country_code
          left join countries as nationality_country on nationality_country.code = receiver.nationality
          left join dropdownlists as ddl_beneficiary_type on ddl_beneficiary_type.type = 'USER_TYPE'
                and ddl_beneficiary_type.status = 'A'
                and ddl_beneficiary_type.value = receiver.user_type
        WHERE bank_accounts.status = 'A'
        AND bank_accounts.user_id = $1
        AND bank_accounts.bank_type = $2
        ORDER BY bank_accounts.bank_account_id`;
                return yield connection_1.db.query(sql, [userId, bankType]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoById(bankAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select bank_accounts.bank_account_id,
          bank_accounts.bank_type,
          bank_accounts.bank_id AS location_id,
          bank_accounts.bank_name AS bank_name,
          bank_accounts.branch_name AS bank_branch_name,
          bank_accounts.branch_code AS bank_branch_code,
          bank_accounts.account_no AS bank_account_number,
          bank_accounts.account_name AS bank_account_name,
          bank_accounts.swift_code AS swift_code,
          bank_accounts.iban AS iban,
          sender.first_name AS sender_first_name,
          sender.middle_name AS sender_middle_name,
          sender.last_name AS sender_last_name,
          sender.mobile_number AS sender_contact_number,
          sender.email AS sender_email,
          sender_address.line1 AS sender_address,
          sender_address.state AS sender_state,
          sender_address.area_town AS sender_area_town,
          sender_address.city AS sender_city,
          sender_address.postcode AS sender_zip_code,
          sender_address.country_code AS sender_country_code,
          sender_country.name as sender_country_name,
          receiver.first_name AS receiver_first_name,
          receiver.middle_name AS receiver_middle_name,
          receiver.last_name AS receiver_last_name,
          receiver.date_of_birth AS receiver_date_of_birth,
          receiver.gender AS receiver_gender,
          receiver.mobile_number AS receiver_contact_number,
          receiver.email AS receiver_email,
          receiver.nationality AS receiver_nationality,
          nationality_country.name as receiver_nationality_name,
          receiver.occupation AS receiver_occupation,
          receiver.occupation_remark AS receiver_occupation_remarks,
          receiver.id_type AS receiver_id_type,
          receiver.id_type_remark AS receiver_id_type_remarks,
          receiver.id_number AS receiver_id_number,
          receiver.id_issue_date AS receiver_id_issue_date,
          receiver.id_expired_date AS receiver_id_expire_date,
          receiver.native_first_name AS receiver_native_first_name,
          receiver.native_middle_name AS receiver_native_middle_name,
          receiver.native_last_name AS receiver_native_last_name,
          '' AS receiver_native_address,
          '' AS receiver_account_type,
          '' AS receiver_district,
          receiver.user_type AS beneficiary_type,
          ddl_beneficiary_type.text as beneficiary_type_text,
          receiver.currency AS currency_code,
          receiver_address.line1 AS receiver_address,
          receiver_address.state AS receiver_state,
          receiver_address.area_town AS receiver_area_town,
          receiver_address.city AS receiver_city,
          receiver_address.postcode AS receiver_zip_code,
          receiver_address.country_code AS receiver_country_code,
          receiver_country.name as receiver_country_name
        from bank_accounts
          inner join lqn_user_profile as receiver on receiver.bank_account_id = bank_accounts.bank_account_id
          inner join lqn_user_profile as sender on sender.lqn_ref_id = receiver.sender_ref_id
          left join addresses as sender_address on sender_address.ref_id = sender.user_id
            and sender_address.address_type = 'USER_PROFILE'
          left join addresses as receiver_address on receiver_address.ref_id = receiver.lqn_user_profile_id
            and receiver_address.address_type = 'LQN_USER_PROFILE'
          left join countries as sender_country on sender_country.code = sender_address.country_code
          left join countries as receiver_country on receiver_country.code = receiver_address.country_code
          left join countries as nationality_country on nationality_country.code = receiver.nationality
          left join dropdownlists as ddl_beneficiary_type on ddl_beneficiary_type.type = 'USER_TYPE'
                    and ddl_beneficiary_type.status = 'A'
                    and ddl_beneficiary_type.value = receiver.user_type
        where bank_accounts.bank_account_id = $1`;
                return yield connection_1.db.query(sql, [bankAccountId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    // async getDefaultBankAccount(userId: number) {
    //   try {
    //     const sql = `
    //       SELECT *
    //       FROM bank_accounts
    //       WHERE status = 'A'
    //       AND is_default = 'Y'
    //       AND user_id = $1`;
    //     return await db.query(sql, [userId]);
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    isDuplicateByBankIdAccountNo(mode, userId, bankType, bankId, accountNo, bankAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let sql = "";
                if (mode === "INSERT") {
                    if (bankType === "M") {
                        sql = `
            SELECT *
            FROM bank_accounts
            WHERE status != 'D'
            AND status != 'I'
            AND user_id = $1
            AND bank_type = $2
            AND lower(bank_id) = lower($3)
            AND lower(account_no) = lower($4)`;
                    }
                    else {
                        sql = `
            SELECT *
            FROM bank_accounts
            WHERE status != 'D'
            AND status != 'I'
            AND user_id = $1
            AND bank_type = $2
            AND lower(account_no) = lower($4)`;
                    }
                }
                else {
                    if (bankType === "M") {
                        sql = `
            SELECT *
            FROM bank_accounts
            WHERE status != 'D'
            AND status != 'I'
            AND bank_account_id != $5
            AND user_id = $1
            AND bank_type = $2
            AND lower(bank_id) = lower($3)
            AND lower(account_no) = lower($4)`;
                    }
                    else {
                        sql = `
            SELECT *
            FROM bank_accounts
            WHERE status != 'D'
            AND status != 'I'
            AND bank_account_id != $5
            AND user_id = $1
            AND bank_type = $2
            AND lower(account_no) = lower($4)`;
                    }
                }
                return yield connection_1.db.query(sql, [
                    userId,
                    bankType,
                    bankId,
                    accountNo,
                    bankAccountId,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(userId, bankType, bankId, bankName, accountNo, accountName, swiftCode, iban, branchName, branchCode, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO bank_accounts(user_id, bank_type, bank_id, bank_name, account_no, account_name, swift_code, iban, is_default, status, created_by, created_date, updated_by, updated_date, branch_code, branch_name)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, 'N', 'A', $9, CURRENT_TIMESTAMP, $9, CURRENT_TIMESTAMP, $10 , $11)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    userId,
                    bankType,
                    bankId,
                    bankName,
                    accountNo,
                    accountName,
                    swiftCode,
                    iban,
                    logOnId,
                    branchCode,
                    branchName,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    // async createOwner(
    //   userId: number,
    //   bankType: string,
    //   bankId: string,
    //   bankName: string,
    //   accountNo: string,
    //   accountName: string,
    //   swiftCode: string,
    //   iban: string,
    //   logOnId: string
    // ) {
    //   try {
    //     const sql = `
    //       INSERT INTO bank_accounts(user_id, bank_type, bank_id, bank_name, account_no, account_name, swift_code, iban, is_default, status, created_by, created_date, updated_by, updated_date)
    //       VALUES($1, $2, $3, $4, $5, $6, $7, $8, 'N', 'A', $9, CURRENT_TIMESTAMP, $9, CURRENT_TIMESTAMP)
    //       RETURNING *`;
    //     return await db.query(sql, [
    //       userId,
    //       bankType,
    //       bankId,
    //       bankName,
    //       accountNo,
    //       accountName,
    //       swiftCode,
    //       iban,
    //       logOnId,
    //     ]);
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // async createOther(
    //   userId: number,
    //   bankType: string,
    //   bankCode: string,
    //   bankName: string,
    //   accountNo: string,
    //   accountName: string,
    //   swiftCode: string,
    //   iban: string,
    //   logOnId: string,
    //   firstName: string,
    //   lastName: string,
    //   nationality: string,
    //   mobileNumber: string,
    //   branchCode: string,
    //   formName: string,
    //   dateOfBirth?: string
    // ) {
    //   try {
    //     const sql = `
    //       INSERT INTO bank_accounts(user_id, bank_type, bank_code, bank_name, account_no, account_name,
    //         swift_code, iban, status, created_by, created_date, updated_by, updated_date,
    //         first_name, last_name, nationality, date_of_birth, mobile_number, branch_code, form_name)
    //       VALUES($1, $2, $3, $4, $5, $6, $7, $8, 'A', $9, CURRENT_TIMESTAMP, $9, CURRENT_TIMESTAMP,
    //       $10, $11, $12, $13, $14, $15, $16)
    //       RETURNING *`;
    //     return await db.query(sql, [
    //       userId,
    //       bankType,
    //       bankCode,
    //       bankName,
    //       accountNo,
    //       accountName,
    //       swiftCode,
    //       iban,
    //       logOnId,
    //       firstName,
    //       lastName,
    //       nationality,
    //       dateOfBirth,
    //       mobileNumber,
    //       branchCode,
    //       formName,
    //     ]);
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // async updateOwner(
    //   bankAccountId: number,
    //   bankCode: string,
    //   bankName: string,
    //   accountNo: string,
    //   accountName: string,
    //   swiftCode: string,
    //   iban: string,
    //   logOnId: string
    // ) {
    //   try {
    //     const sql = `
    //       UPDATE bank_accounts
    //       SET bank_code = $2
    //         , bank_name = $3
    //         , account_no = $4
    //         , account_name = $5
    //         , swift_code = $6
    //         , iban = $7
    //         , updated_by = $8
    //         , updated_date = CURRENT_TIMESTAMP
    //       WHERE bank_account_id = $1
    //       RETURNING *`;
    //     return await db.query(sql, [
    //       bankAccountId,
    //       bankCode,
    //       bankName,
    //       accountNo,
    //       accountName,
    //       swiftCode,
    //       iban,
    //       logOnId,
    //     ]);
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // async updateOther(
    //   bankAccountId: number,
    //   bankCode: string,
    //   bankName: string,
    //   accountNo: string,
    //   accountName: string,
    //   swiftCode: string,
    //   iban: string,
    //   logOnId: string,
    //   firstName: string,
    //   lastName: string,
    //   nationality: string,
    //   mobileNumber: string,
    //   branchCode: string,
    //   dateOfBirth?: string
    // ) {
    //   try {
    //     const sql = `
    //       UPDATE bank_accounts
    //       SET bank_code = $2
    //         , bank_name = $3
    //         , account_no = $4
    //         , account_name = $5
    //         , swift_code = $6
    //         , iban = $7
    //         , updated_by = $8
    //         , updated_date = CURRENT_TIMESTAMP
    //         , first_name = $9
    //         , last_name = $10
    //         , nationality = $11
    //         , date_of_birth = $12
    //         , mobile_number = $13
    //         , branch_code = $14
    //       WHERE bank_account_id = $1
    //       RETURNING *`;
    //     return await db.query(sql, [
    //       bankAccountId,
    //       bankCode,
    //       bankName,
    //       accountNo,
    //       accountName,
    //       swiftCode,
    //       iban,
    //       logOnId,
    //       firstName,
    //       lastName,
    //       nationality,
    //       dateOfBirth,
    //       mobileNumber,
    //       branchCode,
    //     ]);
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // async setDefaultBankAccount(bankAccountId: number, logOnId: string) {
    //   try {
    //     const sql = `
    //       UPDATE bank_accounts
    //       SET is_default = 'Y'
    //         , updated_by = $2
    //         , updated_date = CURRENT_TIMESTAMP
    //       WHERE bank_account_id = $1
    //       RETURNING *`;
    //     return await db.query(sql, [bankAccountId, logOnId]);
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // async disableAllBankAccount(userId: number, logOnId: string) {
    //   try {
    //     const sql = `
    //       UPDATE bank_accounts
    //       SET is_default = 'N'
    //         , updated_by = $2
    //         , updated_date = CURRENT_TIMESTAMP
    //       WHERE user_id = $1
    //       RETURNING *`;
    //     return await db.query(sql, [userId, logOnId]);
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    delete(bankAccountId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE bank_accounts
        SET status = 'D'
          , updated_by = $2
          , updated_date = CURRENT_TIMESTAMP
        WHERE bank_account_id = $1`;
                return yield connection_1.db.query(sql, [bankAccountId, logOnId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getActiveAccountListByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select *
      from bank_accounts
      where user_id = $1
      and status = 'A'`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    inactive(bankAccountId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE bank_accounts
        SET status = 'I'
          , updated_by = $2
          , updated_date = CURRENT_TIMESTAMP
        WHERE bank_account_id = $1`;
                return yield connection_1.db.query(sql, [bankAccountId, logOnId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.BankAccountRepository = BankAccountRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], BankAccountRepository);
//# sourceMappingURL=bankAccountRepository.js.map