"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleMenuRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let RoleMenuRepository = exports.RoleMenuRepository = class RoleMenuRepository {
    constructor() { }
    getInfoByRoleIdGroupMenuCode(roleId, groupMenuCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM role_menus
        WHERE role_id = $1
        AND group_menu_code = $2
        AND menu_code IS NULL`;
                return yield connection_1.db.query(sql, [roleId, groupMenuCode]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByRoleIdGroupMenuCodeMenuCode(roleId, groupMenuCode, menuCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM role_menus
        WHERE role_id = $1
        AND group_menu_code = $2
        AND menu_code = $3`;
                return yield connection_1.db.query(sql, [roleId, groupMenuCode, menuCode]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(roleId, groupMenuCode, menuCode, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO role_menus(role_id, group_menu_code, menu_code, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, CURRENT_TIMESTAMP, $4, CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [roleId, groupMenuCode, menuCode, logOnId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    deleteByRoleId(roleId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        DELETE FROM role_menus
        WHERE role_id = $1`;
                return yield connection_1.db.query(sql, [roleId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    deleteByRoleIdGroupMenuCode(roleId, groupMenuCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        DELETE FROM role_menus
        WHERE role_id = $1
        AND group_menu_code = $2
        AND menu_code IS NULL`;
                return yield connection_1.db.query(sql, [roleId, groupMenuCode]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    deleteByRoleIdGroupMenuCodeMenuCode(roleId, groupMenuCode, menuCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        DELETE FROM role_menus
        WHERE role_id = $1
        AND group_menu_code = $2
        AND menu_code = $3`;
                return yield connection_1.db.query(sql, [roleId, groupMenuCode, menuCode]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.RoleMenuRepository = RoleMenuRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], RoleMenuRepository);
//# sourceMappingURL=roleMenuRepository.js.map