"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LqnUserRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let LqnUserRepository = exports.LqnUserRepository = class LqnUserRepository {
    constructor() { }
    create(lqnType, firstName, middleName, lastname, gender, mobileNumber, nationality, idType, idTpyeRemark, idNumber, idIssueCountry, idIssueDate, idExpiredDate, dateOfBirth, occupation, occupation_remark, email, nativefirstName, nativeMiddleName, nativeLastName, lqnRefId, logon, senderRefId, currency, status, userId, userType, bankAccountId, countryCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO lqn_user_profile(lqn_type,first_name,middle_name,last_name,gender,mobile_number,nationality,id_type,id_type_remark,id_number,id_issue_country,id_issue_date,id_expired_date,date_of_birth,
          occupation,occupation_remark,email,native_first_name,native_middle_name,native_last_name,lqn_ref_id,created_by,created_date,updated_by,updated_date,sender_ref_id,currency,status,user_id,user_type,bank_account_id, country_code)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, CURRENT_TIMESTAMP, $22, CURRENT_TIMESTAMP, $23, $24, $25, $26, $27, $28, $29)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    lqnType,
                    firstName,
                    middleName,
                    lastname,
                    gender,
                    mobileNumber,
                    nationality,
                    idType,
                    idTpyeRemark,
                    idNumber,
                    idIssueCountry,
                    idIssueDate,
                    idExpiredDate,
                    dateOfBirth,
                    occupation,
                    occupation_remark,
                    email,
                    nativefirstName,
                    nativeMiddleName,
                    nativeLastName,
                    lqnRefId,
                    logon,
                    senderRefId,
                    currency,
                    status,
                    userId,
                    userType,
                    bankAccountId,
                    countryCode
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    update(lqnUserId, gender, nationality, mobileNumber, idType, idTpyeRemark, idNumber, idIssueCountry, idIssueDate, idExpiredDate, dateOfBirth, occupation, occupation_remark, email, nativefirstName, nativeMiddleName, nativeLastName, lqnRefId, logon, currency, countryCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      UPDATE lqn_user_profile
      SET gender = $2,
          nationality =$3,
          mobile_number = $4,
          id_type =  $5,
          id_type_remark = $6,
          id_number =$7,
          id_issue_country = $8,
          id_issue_date = $9,
          id_expired_date = $10,
          date_of_birth = $11,
          occupation = $12,
          occupation_remark = $13,
          email = $14,
          native_first_name = $15,
          native_middle_name = $16,
          native_last_name = $17,
          lqn_ref_id = $18,
          updated_by = $19 ,
          updated_date = CURRENT_TIMESTAMP,
          currency = $20,
          country_code = $21
      WHERE lqn_user_profile_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    lqnUserId,
                    gender,
                    nationality,
                    mobileNumber,
                    idType,
                    idTpyeRemark,
                    idNumber,
                    idIssueCountry,
                    idIssueDate,
                    idExpiredDate,
                    dateOfBirth,
                    occupation,
                    occupation_remark,
                    email,
                    nativefirstName,
                    nativeMiddleName,
                    nativeLastName,
                    lqnRefId,
                    logon,
                    currency,
                    countryCode,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateStatus(lqnUserId, status) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE lqn_user_profile
        SET status =$2
        WHERE lqn_user_profile_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [lqnUserId, status]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getLqnUser(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select *
      from lqn_user_profile
      where user_id = $1;`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getLqnUserByType(userId, lqnType) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select *
      from lqn_user_profile
      where user_id = $1
        and status = 'A'
        and lqn_type = $2;`;
                return yield connection_1.db.query(sql, [userId, lqnType]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getLqnUserByLqnRefIdLqnType(lqnRefId, lqnType) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select *
      from lqn_user_profile
      where lqn_ref_id = $1
        and lqn_type = $2;`;
                return yield connection_1.db.query(sql, [lqnRefId, lqnType]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getLqnUserByBankAccountId(bankAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select *
        from lqn_user_profile
	      where bank_account_id = $1`;
                return yield connection_1.db.query(sql, [bankAccountId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateUserIdById(lqnUserProfileId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE lqn_user_profile
        SET user_id = $2
        WHERE lqn_user_profile_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [lqnUserProfileId, userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getRecieverBySenderRefId(senderRefId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select *
      from lqn_user_profile
      where  sender_ref_id = $1
      and lqn_type = 'RECEIVER'`;
                return yield connection_1.db.query(sql, [senderRefId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.LqnUserRepository = LqnUserRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], LqnUserRepository);
//# sourceMappingURL=lqnUserRepository.js.map