"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WalletCreditDocumentRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let WalletCreditDocumentRepository = exports.WalletCreditDocumentRepository = class WalletCreditDocumentRepository {
    constructor() { }
    getList(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select wallet_credit_documents.*
        from wallet_credits
          inner join wallet_credit_documents on wallet_credit_documents.wallet_credit_id = wallet_credits.wallet_credit_id
                                            and wallet_credit_documents.status = 'A'
        where wallet_credits.status = 'A'
        and wallet_credits.user_id = $1
        ORDER BY wallet_credit_documents.wallet_credit_document_id`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListByWalletCreditId(walletCreditId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select *
        from wallet_credit_documents
        where status = 'A'
        and wallet_credit_id = $1
        ORDER BY wallet_credit_documents.wallet_credit_document_id`;
                return yield connection_1.db.query(sql, [walletCreditId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoById(walletCreditDocumentId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select *
        from wallet_credit_documents
        where status = 'A'
        and wallet_credit_document_id = $1`;
                return yield connection_1.db.query(sql, [walletCreditDocumentId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(walletCreditId, fileName, url, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO wallet_credit_documents(wallet_credit_id, file_name, url, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, 'A', $4, CURRENT_TIMESTAMP, $4, CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [walletCreditId, fileName, url, logOnId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.WalletCreditDocumentRepository = WalletCreditDocumentRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], WalletCreditDocumentRepository);
//# sourceMappingURL=walletCreditDocumentRepository.js.map