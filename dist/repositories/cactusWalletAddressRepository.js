"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CactusWalletAddressRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let CactusWalletAddressRepository = exports.CactusWalletAddressRepository = class CactusWalletAddressRepository {
    constructor() { }
    getList(cactusWalletId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
          select *
          from cactus_wallet_addresses
          where cactus_wallet_id = $1`;
                return yield connection_1.db.query(sql, [cactusWalletId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(cactusWalletId, networkCode, assetCode, assetAddress, addressType, addressStorage, total, available, pending, lockedamount, status, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO cactus_wallet_addresses(cactus_wallet_id, network_code, asset_code, asset_address, address_type, address_storage, total, available, pending, lockedamount, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, CURRENT_TIMESTAMP, $12, CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    cactusWalletId,
                    networkCode,
                    assetCode,
                    assetAddress,
                    addressType,
                    addressStorage,
                    total,
                    available,
                    pending,
                    lockedamount,
                    status,
                    logOnId,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getListByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select cactus_wallet_addresses.cactus_wallet_address_id
          , networks.network_id
          , networks.code as network_code
          , networks.name as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets.name as asset_name
          , assets.image as asset_image
          , (cactus_wallet_addresses.total - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = cactus_wallets.user_id and transactions.asset_code = cactus_wallet_addresses.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as total
          , (cactus_wallet_addresses.available - (select COALESCE(sum(transactions.total_assets),0) from transactions where transactions.user_id = cactus_wallets.user_id and transactions.asset_code = cactus_wallet_addresses.asset_code and transactions.type = 'S' and transactions.status IN ('P','H')))::numeric as available
          , cactus_wallet_addresses.pending
          , cactus_wallet_addresses.lockedamount	
          , cactus_wallet_addresses.asset_address
          , COALESCE((cactus_wallet_addresses.total * COALESCE(asset_rates.selling,0)),0)::numeric as total_buying_amount
          , COALESCE((cactus_wallet_addresses.total * COALESCE(asset_rates.buying,0)),0)::numeric as total_selling_amount
          , CONCAT(users.first_name,' ', users.last_name) as updated_by
          , cactus_wallet_addresses.updated_date
          , COALESCE(asset_rates.buying,0) as buying
          , COALESCE(asset_rates.selling,0) as selling
        from cactus_wallets
          inner join cactus_wallet_addresses on cactus_wallet_addresses.cactus_wallet_id = cactus_wallets.cactus_wallet_id
          inner join assets on assets.code = cactus_wallet_addresses.asset_code and assets.status = 'A'
          inner join networks on networks.network_id = assets.network_id and networks.status = 'A'
          left join asset_rates on asset_rates.asset_code = cactus_wallet_addresses.asset_code
          left join users on users.user_id = NULLIF(cactus_wallet_addresses.updated_by, '')::int
        where cactus_wallets.status = 'A'
        and cactus_wallet_addresses.status = 'A'
        and cactus_wallets.user_id = $1
        order by cactus_wallet_addresses.cactus_wallet_address_id`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.CactusWalletAddressRepository = CactusWalletAddressRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], CactusWalletAddressRepository);
//# sourceMappingURL=cactusWalletAddressRepository.js.map