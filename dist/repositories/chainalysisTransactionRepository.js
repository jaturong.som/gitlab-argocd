"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChainalysisTransactionRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let ChainalysisTransactionRepository = exports.ChainalysisTransactionRepository = class ChainalysisTransactionRepository {
    constructor() { }
    getPendingList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      SELECT *
      FROM chainalysis_transactions
      WHERE updated_at IS NULL`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByFBTransactionId(fbTransactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      SELECT *
      FROM chainalysis_transactions
      WHERE fb_transaction_id = $1`;
                return yield connection_1.db.query(sql, [fbTransactionId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByExternalId(externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      SELECT *
      FROM chainalysis_transactions
      WHERE external_id = $1`;
                return yield connection_1.db.query(sql, [externalId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(fbTransactionId, updatedAt, asset, network, transferReference, tx, idx, usdAmount, assetAmount, timestamp, outputAddress, externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO chainalysis_transactions(fb_transaction_id, updated_at, asset, network, transfer_reference, tx, idx, usd_amount, asset_amount, "timestamp", output_address, external_id, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, 'A','0',CURRENT_TIMESTAMP,'0',CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    fbTransactionId,
                    updatedAt,
                    asset,
                    network,
                    transferReference,
                    tx,
                    idx,
                    usdAmount,
                    assetAmount,
                    timestamp,
                    outputAddress,
                    externalId,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    update(externalId, updatedAt, asset, network, transferReference, tx, idx, usdAmount, assetAmount, timestamp, outputAddress) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE chainalysis_transactions
        SET updated_at = $2
          , asset = $3
          , network = $4
          , transfer_reference = $5
          , tx = $6
          , idx = $7
          , usd_amount = $8
          , asset_amount = $9
          , "timestamp" = $10
          , output_address = $11
          , updated_date = CURRENT_TIMESTAMP
        WHERE external_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    externalId,
                    updatedAt,
                    asset,
                    network,
                    transferReference,
                    tx,
                    idx,
                    usdAmount,
                    assetAmount,
                    timestamp,
                    outputAddress,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.ChainalysisTransactionRepository = ChainalysisTransactionRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], ChainalysisTransactionRepository);
//# sourceMappingURL=chainalysisTransactionRepository.js.map