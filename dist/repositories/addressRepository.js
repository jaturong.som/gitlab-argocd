"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddressRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let AddressRepository = exports.AddressRepository = class AddressRepository {
    constructor() { }
    getInfoById(addressType, addressId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select addresses.*, countries."name" as country_name
        from addresses
          left join countries on countries.code = addresses.country_code
                and countries.status = 'A'
        where addresses.status = 'A'
        and addresses.address_type = $1
        and addresses.address_id = $2`;
                return yield connection_1.db.query(sql, [addressType, addressId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByRefId(addressType, refId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        select addresses.*, countries."name" as country_name
        from addresses
          left join countries on countries.code = addresses.country_code
                  and countries.status = 'A'
        where addresses.status = 'A'
        and addresses.address_type = $1
        and addresses.ref_id = $2`;
                return yield connection_1.db.query(sql, [addressType, refId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(addressType, refId, line1, line2, city, state, countryCode, postcode, areaTown, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO addresses(address_type, ref_id, line1, line2, city, state, country_code, postcode, area_town, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, 'A', $10, CURRENT_TIMESTAMP, $10, CURRENT_TIMESTAMP)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    addressType,
                    refId,
                    line1,
                    line2,
                    city,
                    state,
                    countryCode,
                    postcode,
                    areaTown,
                    logOnId,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    update(addressId, line1, line2, city, state, countryCode, postcode, areaTown, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE addresses
        SET line1 = $2
            , line2 = $3
            , city = $4
            , state = $5
            , country_code = $6
            , postcode = $7
            , area_town = $8
            , updated_by = $9
            , updated_date = CURRENT_TIMESTAMP
        WHERE address_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    addressId,
                    line1,
                    line2,
                    city,
                    state,
                    countryCode,
                    postcode,
                    areaTown,
                    logOnId,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    delete(addressId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE addresses
        SET status = 'D'
            , updated_by = $2
            , updated_date = CURRENT_TIMESTAMP
        WHERE address_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [addressId, logOnId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.AddressRepository = AddressRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], AddressRepository);
//# sourceMappingURL=addressRepository.js.map