"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const connection_1 = require("./connection");
const tsyringe_1 = require("tsyringe");
let UserRepository = exports.UserRepository = class UserRepository {
    constructor() { }
    getList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT users.*, (select role_id from user_roles where user_roles.user_id = users.user_id limit 1) as role_id
        FROM users
        ORDER BY users.user_id`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getActiveList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
      select user_id ,  CONCAT(first_name , ' ', last_name ) as "full_name"
      from users
      where status = 'A'
      order by first_name`;
                return yield connection_1.db.query(sql);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoById(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT users.*
          , (select role_id from user_roles where user_roles.user_id = users.user_id limit 1) as role_id
          , countries."name" as country_name
        FROM users
        left join countries on countries.code = users.country_code
                  and countries.status = 'A'
        WHERE user_id = $1`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByUserName(userName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM users
        WHERE user_name = $1`;
                return yield connection_1.db.query(sql, [userName]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getInfoByEmail(email) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM users
        WHERE email = $1`;
                return yield connection_1.db.query(sql, [email]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getLoginInfoByUserName(userName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM users
        WHERE lower(user_name) = lower($1)`;
                return yield connection_1.db.query(sql, [userName]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getActiveInfoById(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT users.*, (select role_id from user_roles where user_roles.user_id = users.user_id limit 1) as role_id
        FROM users
        WHERE status = 'A'
        AND user_id = $1`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getActiveInfoByUserName(userName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM users
        WHERE status = 'A'
        AND user_name = $1`;
                return yield connection_1.db.query(sql, [userName]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getActiveLoginInfoByUserName(userName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        SELECT *
        FROM users
        WHERE status = 'A'
        AND lower(user_name) = lower($1)`;
                return yield connection_1.db.query(sql, [userName]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    create(code, type, password, firstName, middleName, lastName, email, tel, status, logOnId, gender, nationality, occupation, occupationRemark, dateOfBirth, countryCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        INSERT INTO users(code, "type", user_name, "password", first_name, middle_name, last_name, email, tel, status, created_by, created_date, updated_by, updated_date, date_of_birth, country_code,
          gender, nationality_code, occupation, occupation_remark)
        VALUES($1, $2, $7, $3, $4, $5, $6, $7, $8, $9, $10, CURRENT_TIMESTAMP, $10, CURRENT_TIMESTAMP, $11, $12, $13, $14, $15 ,$16)
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    code,
                    type,
                    password,
                    firstName,
                    middleName,
                    lastName,
                    email,
                    tel,
                    status,
                    logOnId,
                    dateOfBirth,
                    countryCode,
                    gender,
                    nationality,
                    occupation,
                    occupationRemark
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    update(userId, firstName, middleName, lastName, tel, status, logOnId, gender, nationality, occupation, occupationRemark, dateOfBirth, countryCode, email) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE users
        SET first_name = $2
          , middle_name = $3
          , last_name = $4
          , tel = $5
          , status = $6
          , updated_by = $7
          , updated_date = CURRENT_TIMESTAMP
          , date_of_birth = $8
          , country_code = $9
          , email = $10
          , gender = $11
          , nationality_code = $12
          , occupation = $13
          , occupation_remark = $14
        WHERE user_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [
                    userId,
                    firstName,
                    middleName,
                    lastName,
                    tel,
                    status,
                    logOnId,
                    dateOfBirth,
                    countryCode,
                    email,
                    gender,
                    nationality,
                    occupation,
                    occupationRemark,
                ]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    delete(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        DELETE FROM users
        WHERE user_id = $1`;
                return yield connection_1.db.query(sql, [userId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    changePassword(userId, password, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const sql = `
        UPDATE users
        SET password = $2
          , updated_by = $3
          , updated_date = CURRENT_TIMESTAMP
        WHERE user_id = $1
        RETURNING *`;
                return yield connection_1.db.query(sql, [userId, password, logOnId]);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.UserRepository = UserRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], UserRepository);
//# sourceMappingURL=userRepository.js.map