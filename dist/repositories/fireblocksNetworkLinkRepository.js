"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FireblocksNetworkLinkRepository = void 0;
const ethers_1 = __importDefault(require("ethers"));
const crypto_1 = __importDefault(require("crypto"));
const db_json_1 = __importDefault(require("./db.json"));
const utils_1 = require("../utilities/utils");
const tsyringe_1 = require("tsyringe");
let FireblocksNetworkLinkRepository = exports.FireblocksNetworkLinkRepository = class FireblocksNetworkLinkRepository {
    constructor() { }
    getAllAccountTypes(customerId) {
        return __awaiter(this, void 0, void 0, function* () {
            const customer = db_json_1.default.customers.find((c) => c.id === customerId);
            if (!customer) {
                return;
            }
            const accounts = [];
            const objAccounts = customer.accounts;
            for (let i = 0; i < objAccounts.length; i++) {
                const assets = [];
                var objAssets = objAccounts[i].assets;
                for (let j = 0; j < objAssets.length; j++) {
                    assets.push({
                        coinSymbol: objAssets[j].coinSymbol,
                        totalAmount: objAssets[j].totalAmount,
                        pendingAmount: objAssets[j].pendingAmount,
                        availableAmount: objAssets[j].availableAmount,
                        creditAmount: objAssets[j].creditAmount,
                    });
                }
                accounts.push({
                    displayName: objAccounts[i].accountName,
                    type: objAccounts[i].accountType,
                    balances: assets,
                });
            }
            return accounts;
        });
    }
    getDepositWalletAddress(customerId, accountType, coinSymbol, network) {
        return __awaiter(this, void 0, void 0, function* () {
            const customer = db_json_1.default.customers.find((c) => c.id === customerId);
            if (!customer) {
                return;
            }
            const account = customer.accounts.find((a) => a.accountType === accountType);
            if (!account) {
                return;
            }
            const asset = account.assets.find((a) => a.coinSymbol === coinSymbol && a.network === network);
            if (!asset) {
                return;
            }
            const depositAddress = {
                depositAddress: asset.depositAddress,
                depositAddressTag: asset.depositAddressTag,
            };
            return depositAddress;
        });
    }
    createDepositWalletAddress(customerId, accountType, coinSymbol, network) {
        return __awaiter(this, void 0, void 0, function* () {
            const indexCustomer = db_json_1.default.customers.findIndex((c) => c.id === customerId);
            if (indexCustomer === -1) {
                return;
            }
            const indexAccount = db_json_1.default.customers[indexCustomer].accounts.findIndex((a) => a.accountType === accountType);
            if (indexAccount === -1) {
                return;
            }
            const indexAsset = db_json_1.default.customers[indexCustomer].accounts[indexAccount].assets.findIndex((a) => a.coinSymbol === coinSymbol && a.network === network);
            if (indexAsset === -1) {
                var id = crypto_1.default.randomBytes(32).toString("hex");
                var privateKey = "0x" + id;
                var wallet = new ethers_1.default.Wallet(privateKey);
                const newAsset = {
                    coinSymbol: coinSymbol,
                    totalAmount: 0,
                    pendingAmount: 0,
                    availableAmount: 0,
                    creditAmount: 0,
                    network: network,
                    depositAddress: wallet.address,
                    depositAddressTag: Date.now(),
                };
                db_json_1.default.customers[indexCustomer].accounts[indexAccount].assets.push(newAsset);
                (0, utils_1.saveToDatabase)(db_json_1.default);
                return {
                    depositAddress: wallet.address,
                    depositAddressTag: Date.now(),
                };
            }
            return;
        });
    }
    getWithDrawalFee(transferAmount, coinSymbol, network) {
        return __awaiter(this, void 0, void 0, function* () {
            return { feeAmount: transferAmount / 65 };
        });
    }
    createWithDraw(amount, maxFee, coinSymbol, network, accountType, toAddress, tag, isGross) {
        return __awaiter(this, void 0, void 0, function* () {
            var newTransactionID = crypto_1.default.randomBytes(16).toString("hex");
            var newTxHash = crypto_1.default.randomBytes(32).toString("hex");
            const newTransaction = {
                transactionID: newTransactionID,
                status: "COMPLETED",
                txHash: newTxHash,
                amount: amount,
                serviceFee: maxFee,
                coinSymbol: coinSymbol,
                network: network,
                direction: "CRYPTO_WITHDRAWAL",
                timestamp: Date.now(),
                accountType: accountType,
                toAddress: toAddress,
                tag: tag,
                isGross: isGross,
                maxFee: maxFee,
            };
            db_json_1.default.transactions.push(newTransaction);
            (0, utils_1.saveToDatabase)(db_json_1.default);
            return { transactionID: newTransactionID };
        });
    }
    getTransactionById(transactionID) {
        return __awaiter(this, void 0, void 0, function* () {
            const transaction = db_json_1.default.transactions.find((c) => c.transactionID === transactionID);
            if (!transaction) {
                return;
            }
            return {
                transactionID: transaction.transactionID,
                status: transaction.status,
                txHash: transaction.txHash,
                amount: transaction.amount,
                serviceFee: transaction.serviceFee,
                coinSymbol: transaction.coinSymbol,
                network: transaction.network,
                direction: transaction.direction,
                timestamp: transaction.timestamp,
            };
        });
    }
    getTransactionByHash(txHash, network) {
        return __awaiter(this, void 0, void 0, function* () {
            const transaction = db_json_1.default.transactions.find((c) => c.txHash === txHash && c.network === network);
            if (!transaction) {
                return;
            }
            return {
                transactionID: transaction.transactionID,
                status: transaction.status,
                txHash: transaction.txHash,
                amount: transaction.amount,
                serviceFee: transaction.serviceFee,
                coinSymbol: transaction.coinSymbol,
                network: transaction.network,
                direction: transaction.direction,
                timestamp: transaction.timestamp,
            };
        });
    }
    getTransactionHistory(fromDate, toDate, pageSize, pageCursor, isSubTransfer, coinSymbol, network) {
        return __awaiter(this, void 0, void 0, function* () {
            const transactions = db_json_1.default.transactions.filter((c) => c.coinSymbol === coinSymbol && c.network === network);
            if (!transactions) {
                return;
            }
            const results = [];
            for (let i = 0; i < transactions.length; i++) {
                results.push({
                    transactionID: transactions[i].transactionID,
                    status: transactions[i].status,
                    txHash: transactions[i].txHash,
                    amount: transactions[i].amount,
                    serviceFee: transactions[i].serviceFee,
                    coinSymbol: transactions[i].coinSymbol,
                    network: transactions[i].network,
                    direction: transactions[i].direction,
                    timestamp: transactions[i].timestamp,
                });
            }
            return {
                nextPageCursor: null,
                transactions: results,
            };
        });
    }
    getSupportedAssets() {
        return __awaiter(this, void 0, void 0, function* () {
            return db_json_1.default.assets;
        });
    }
    createSubMainTransfer() {
        return __awaiter(this, void 0, void 0, function* () {
            return {};
        });
    }
    createSubAccountsTransfer() {
        return __awaiter(this, void 0, void 0, function* () {
            return {};
        });
    }
    createInternalTransfer() {
        return __awaiter(this, void 0, void 0, function* () {
            return {};
        });
    }
};
exports.FireblocksNetworkLinkRepository = FireblocksNetworkLinkRepository = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], FireblocksNetworkLinkRepository);
//# sourceMappingURL=fireblocksNetworkLinkRepository.js.map