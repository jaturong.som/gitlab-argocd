"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegisterRoutes = void 0;
/* tslint:disable */
/* eslint-disable */
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const runtime_1 = require("@tsoa/runtime");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const adminController_1 = require("./../../controllers/adminController");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const authController_1 = require("./../../controllers/authController");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const cactusCustodyController_1 = require("./../../controllers/cactusCustodyController");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const cactusCustodyWebhookController_1 = require("./../../controllers/cactusCustodyWebhookController");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const configController_1 = require("./../../controllers/configController");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const cronJobController_1 = require("./../../controllers/cronJobController");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const customerController_1 = require("./../../controllers/customerController");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const fireblocksNetworkLinkController_1 = require("./../../controllers/fireblocksNetworkLinkController");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const fireblocksWebhookController_1 = require("./../../controllers/fireblocksWebhookController");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const forgotPasswordController_1 = require("./../../controllers/forgotPasswordController");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const lqnController_1 = require("./../../controllers/lqnController");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const masterController_1 = require("./../../controllers/masterController");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const transactionController_1 = require("./../../controllers/transactionController");
const authentication_1 = require("./../../middlewares/authentication");
// @ts-ignore - no great way to install types from subpackage
const promiseAny = require('promise.any');
const tsyringeTsoaIocContainer_1 = require("./../../ioc/tsyringeTsoaIocContainer");
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
const models = {
    "BaseModels.ResponseResult": {
        "dataType": "refObject",
        "properties": {
            "code": { "dataType": "double", "required": true },
            "message": { "dataType": "string", "required": true },
            "result": { "dataType": "any" },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestGetUsers": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestCreateUser": {
        "dataType": "refObject",
        "properties": {
            "password": { "dataType": "string", "required": true },
            "first_name": { "dataType": "string", "required": true },
            "middle_name": { "dataType": "string" },
            "last_name": { "dataType": "string", "required": true },
            "date_of_birth": { "dataType": "string", "required": true },
            "email": { "dataType": "string", "required": true },
            "tel": { "dataType": "string", "required": true },
            "country_code": { "dataType": "string", "required": true },
            "status": { "dataType": "string", "required": true },
            "role_id": { "dataType": "double", "required": true },
            "document_type": { "dataType": "string", "required": true },
            "document_type_remark": { "dataType": "string" },
            "document_text": { "dataType": "string", "required": true },
            "address": { "dataType": "nestedObjectLiteral", "nestedProperties": { "area_town": { "dataType": "string" }, "postcode": { "dataType": "string", "required": true }, "country_code": { "dataType": "string", "required": true }, "state": { "dataType": "string", "required": true }, "city": { "dataType": "string", "required": true }, "line2": { "dataType": "string" }, "line1": { "dataType": "string", "required": true } }, "required": true },
            "gender": { "dataType": "string", "required": true },
            "nationality": { "dataType": "string", "required": true },
            "occupation": { "dataType": "string", "required": true },
            "occupation_remark": { "dataType": "string" },
            "document_issue_date": { "dataType": "string" },
            "document_expired_date": { "dataType": "string" },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestUpdateUser": {
        "dataType": "refObject",
        "properties": {
            "first_name": { "dataType": "string", "required": true },
            "middle_name": { "dataType": "string" },
            "last_name": { "dataType": "string", "required": true },
            "date_of_birth": { "dataType": "string", "required": true },
            "tel": { "dataType": "string", "required": true },
            "country_code": { "dataType": "string", "required": true },
            "status": { "dataType": "string", "required": true },
            "role_id": { "dataType": "double", "required": true },
            "document_type": { "dataType": "string", "required": true },
            "document_type_remark": { "dataType": "string" },
            "document_text": { "dataType": "string", "required": true },
            "address": { "dataType": "nestedObjectLiteral", "nestedProperties": { "area_town": { "dataType": "string" }, "postcode": { "dataType": "string", "required": true }, "country_code": { "dataType": "string", "required": true }, "state": { "dataType": "string", "required": true }, "city": { "dataType": "string", "required": true }, "line2": { "dataType": "string" }, "line1": { "dataType": "string", "required": true } }, "required": true },
            "gender": { "dataType": "string", "required": true },
            "nationality": { "dataType": "string", "required": true },
            "occupation": { "dataType": "string", "required": true },
            "occupation_remark": { "dataType": "string" },
            "document_issue_date": { "dataType": "string" },
            "document_expired_date": { "dataType": "string" },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestGetRoles": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestGetActiveRoles": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "RoleMenuPermission": {
        "dataType": "refObject",
        "properties": {
            "group_menu_code": { "dataType": "string", "required": true },
            "menu_code": { "dataType": "string" },
            "permission": { "dataType": "boolean", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestCreateRole": {
        "dataType": "refObject",
        "properties": {
            "name": { "dataType": "string", "required": true },
            "description": { "dataType": "string" },
            "status": { "dataType": "string", "required": true },
            "menus": { "dataType": "array", "array": { "dataType": "refObject", "ref": "RoleMenuPermission" }, "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestUpdateRole": {
        "dataType": "refObject",
        "properties": {
            "name": { "dataType": "string", "required": true },
            "description": { "dataType": "string" },
            "status": { "dataType": "string", "required": true },
            "menus": { "dataType": "array", "array": { "dataType": "refObject", "ref": "RoleMenuPermission" }, "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestChangePassword": {
        "dataType": "refObject",
        "properties": {
            "old_password": { "dataType": "string", "required": true },
            "new_password": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestResetPassword": {
        "dataType": "refObject",
        "properties": {
            "user_id": { "dataType": "double", "required": true },
            "new_password": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestUpdateRoleMenuByRoleId": {
        "dataType": "refObject",
        "properties": {
            "group_menu_code": { "dataType": "string", "required": true },
            "menu_code": { "dataType": "string" },
            "permission": { "dataType": "boolean", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestGetMasterMenu": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestGetCryptoWalletList": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestCreateTransaction": {
        "dataType": "refObject",
        "properties": {
            "user_id": { "dataType": "double", "required": true },
            "type": { "dataType": "string", "required": true },
            "asset_code": { "dataType": "string", "required": true },
            "total_assets": { "dataType": "double", "required": true },
            "total_amount": { "dataType": "double", "required": true },
            "input_type": { "dataType": "string", "required": true },
            "is_manual_mode": { "dataType": "boolean", "required": true },
            "wallet_network": { "dataType": "string", "required": true },
            "wallet_address": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestUpdateFavoriteFiatAccounts": {
        "dataType": "refObject",
        "properties": {
            "is_favorite": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestCreateTopup": {
        "dataType": "refObject",
        "properties": {
            "fiat_id": { "dataType": "double", "required": true },
            "amount": { "dataType": "double", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestCreateWithdraw": {
        "dataType": "refObject",
        "properties": {
            "user_id": { "dataType": "double", "required": true },
            "sender_source_of_fund": { "dataType": "string", "required": true },
            "sender_source_of_fund_remarks": { "dataType": "string" },
            "purpose_of_remittance": { "dataType": "string", "required": true },
            "purpose_of_remittance_remarks": { "dataType": "string" },
            "sender_beneficiary_relationship": { "dataType": "string", "required": true },
            "sender_beneficiary_relationship_remarks": { "dataType": "string" },
            "transfer_amount": { "dataType": "double", "required": true },
            "bank_account_id": { "dataType": "double", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestCreateTransfer": {
        "dataType": "refObject",
        "properties": {
            "user_id": { "dataType": "double", "required": true },
            "sender_source_of_fund": { "dataType": "string", "required": true },
            "sender_source_of_fund_remarks": { "dataType": "string" },
            "purpose_of_remittance": { "dataType": "string", "required": true },
            "purpose_of_remittance_remarks": { "dataType": "string" },
            "sender_beneficiary_relationship": { "dataType": "string", "required": true },
            "sender_beneficiary_relationship_remarks": { "dataType": "string" },
            "transfer_amount": { "dataType": "double", "required": true },
            "bank_account_id": { "dataType": "double", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AdminModels.RequestCreateBankAccount": {
        "dataType": "refObject",
        "properties": {
            "user_id": { "dataType": "double", "required": true },
            "bank_type": { "dataType": "string", "required": true },
            "receiver_first_name": { "dataType": "string" },
            "receiver_middle_name": { "dataType": "string" },
            "receiver_last_name": { "dataType": "string" },
            "receiver_date_of_birth": { "dataType": "string" },
            "receiver_gender": { "dataType": "string" },
            "receiver_contact_number": { "dataType": "string" },
            "receiver_email": { "dataType": "string" },
            "receiver_nationality": { "dataType": "string" },
            "receiver_occupation": { "dataType": "string" },
            "receiver_occupation_remarks": { "dataType": "string" },
            "receiver_id_type": { "dataType": "string" },
            "receiver_id_type_remarks": { "dataType": "string" },
            "receiver_id_number": { "dataType": "string" },
            "receiver_id_issue_date": { "dataType": "string" },
            "receiver_id_expire_date": { "dataType": "string" },
            "receiver_address": { "dataType": "string" },
            "receiver_state": { "dataType": "string" },
            "receiver_area_town": { "dataType": "string" },
            "receiver_city": { "dataType": "string" },
            "receiver_zip_code": { "dataType": "string" },
            "receiver_country": { "dataType": "string", "required": true },
            "receiver_native_first_name": { "dataType": "string" },
            "receiver_native_middle_name": { "dataType": "string" },
            "receiver_native_last_name": { "dataType": "string" },
            "receiver_native_address": { "dataType": "string" },
            "receiver_account_type": { "dataType": "string" },
            "receiver_district": { "dataType": "string" },
            "beneficiary_type": { "dataType": "string" },
            "location_id": { "dataType": "string", "required": true },
            "bank_name": { "dataType": "string", "required": true },
            "bank_branch_name": { "dataType": "string", "required": true },
            "bank_branch_code": { "dataType": "string", "required": true },
            "bank_account_number": { "dataType": "string", "required": true },
            "swift_code": { "dataType": "string" },
            "iban": { "dataType": "string" },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AuthModels.RequestLogin": {
        "dataType": "refObject",
        "properties": {
            "user_name": { "dataType": "string", "required": true },
            "password": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "AuthModels.RequestRefreshToken": {
        "dataType": "refObject",
        "properties": {
            "refresh_token": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CactusCustodyModels.RequestCreateWallet": {
        "dataType": "refObject",
        "properties": {
            "wallet_type": { "dataType": "string", "required": true },
            "number": { "dataType": "double", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CactusCustodyModels.RequestApplyNewAddress": {
        "dataType": "refObject",
        "properties": {
            "address_num": { "dataType": "double", "required": true },
            "address_type": { "dataType": "string", "required": true },
            "coin_name": { "dataType": "string" },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CactusCustodyModels.RequestEditAddress": {
        "dataType": "refObject",
        "properties": {
            "description": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CactusCustodyModels.RequestVerifyAddressFormat": {
        "dataType": "refObject",
        "properties": {
            "addresses": { "dataType": "array", "array": { "dataType": "string" }, "required": true },
            "coin_name": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CactusCustodyModels.RequestEstimateGasFee": {
        "dataType": "refObject",
        "properties": {
            "from_address": { "dataType": "string", "required": true },
            "from_wallet_code": { "dataType": "string", "required": true },
            "description": { "dataType": "string", "required": true },
            "amount": { "dataType": "double", "required": true },
            "dest_address": { "dataType": "string", "required": true },
            "memo_type": { "dataType": "string", "required": true },
            "memo": { "dataType": "string", "required": true },
            "remark": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CactusCustodyModels.RequestCreateWithdrawalOrder": {
        "dataType": "refObject",
        "properties": {
            "from_address": { "dataType": "string", "required": true },
            "from_wallet_code": { "dataType": "string", "required": true },
            "coin_name": { "dataType": "string", "required": true },
            "description": { "dataType": "string", "required": true },
            "amount": { "dataType": "double", "required": true },
            "dest_address": { "dataType": "string", "required": true },
            "memo_type": { "dataType": "string", "required": true },
            "memo": { "dataType": "string", "required": true },
            "remark": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CactusCustodyModels.RequestEditTransactionDetailRemark": {
        "dataType": "refObject",
        "properties": {
            "remark": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CactusCustodyModels.RequestRBF": {
        "dataType": "refObject",
        "properties": {
            "level": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CactusCustodyModels.RequestCancelOrder": {
        "dataType": "refObject",
        "properties": {
            "level": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "ConfigModels.RequestGetAssetsInVaultAccount": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "ConfigModels.RequestCreateAssetInVaultAccount": {
        "dataType": "refObject",
        "properties": {
            "asset_code": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "ConfigModels.RequestGetAssetRates": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "ConfigModels.RequestCreateAssetRate": {
        "dataType": "refObject",
        "properties": {
            "asset_code": { "dataType": "string", "required": true },
            "buying": { "dataType": "double", "required": true },
            "selling": { "dataType": "double", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "ConfigModels.RequestUpdateAssetRate": {
        "dataType": "refObject",
        "properties": {
            "buying": { "dataType": "double", "required": true },
            "selling": { "dataType": "double", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "ConfigModels.RequestGetNetworks": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "ConfigModels.RequestGetAssets": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CronJobModels.RequestManualUpdateTransactionStatus": {
        "dataType": "refObject",
        "properties": {
            "external_id": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CustomerModels.RequestGetUserApiKeys": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CustomerModels.RequestCreateUserApiKey": {
        "dataType": "refObject",
        "properties": {
            "name": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CustomerModels.RequestRequestAccessAsset": {
        "dataType": "refObject",
        "properties": {
            "asset_code": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CustomerModels.RequestGetUserFiatAccounts": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CustomerModels.RequestCreateBankAccount": {
        "dataType": "refObject",
        "properties": {
            "bank_type": { "dataType": "string", "required": true },
            "receiver_first_name": { "dataType": "string" },
            "receiver_middle_name": { "dataType": "string" },
            "receiver_last_name": { "dataType": "string" },
            "receiver_date_of_birth": { "dataType": "string" },
            "receiver_gender": { "dataType": "string" },
            "receiver_contact_number": { "dataType": "string" },
            "receiver_email": { "dataType": "string" },
            "receiver_nationality": { "dataType": "string" },
            "receiver_occupation": { "dataType": "string" },
            "receiver_occupation_remarks": { "dataType": "string" },
            "receiver_id_type": { "dataType": "string" },
            "receiver_id_type_remarks": { "dataType": "string" },
            "receiver_id_number": { "dataType": "string" },
            "receiver_id_issue_date": { "dataType": "string" },
            "receiver_id_expire_date": { "dataType": "string" },
            "receiver_address": { "dataType": "string" },
            "receiver_state": { "dataType": "string" },
            "receiver_area_town": { "dataType": "string" },
            "receiver_city": { "dataType": "string" },
            "receiver_zip_code": { "dataType": "string" },
            "receiver_country": { "dataType": "string", "required": true },
            "receiver_native_first_name": { "dataType": "string" },
            "receiver_native_middle_name": { "dataType": "string" },
            "receiver_native_last_name": { "dataType": "string" },
            "receiver_native_address": { "dataType": "string" },
            "receiver_account_type": { "dataType": "string" },
            "receiver_district": { "dataType": "string" },
            "beneficiary_type": { "dataType": "string" },
            "location_id": { "dataType": "string", "required": true },
            "bank_name": { "dataType": "string", "required": true },
            "bank_branch_name": { "dataType": "string", "required": true },
            "bank_branch_code": { "dataType": "string", "required": true },
            "bank_account_number": { "dataType": "string", "required": true },
            "swift_code": { "dataType": "string" },
            "iban": { "dataType": "string" },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CustomerModels.RequestGetCryptoWalletListForUser": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CustomerModels.RequestCreateTransaction": {
        "dataType": "refObject",
        "properties": {
            "type": { "dataType": "string", "required": true },
            "asset_code": { "dataType": "string", "required": true },
            "total_assets": { "dataType": "double", "required": true },
            "total_amount": { "dataType": "double", "required": true },
            "input_type": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CustomerModels.RequestGetWithdraws": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CustomerModels.RequestCreateWithdraw": {
        "dataType": "refObject",
        "properties": {
            "sender_source_of_fund": { "dataType": "string", "required": true },
            "sender_source_of_fund_remarks": { "dataType": "string" },
            "purpose_of_remittance": { "dataType": "string", "required": true },
            "purpose_of_remittance_remarks": { "dataType": "string" },
            "sender_beneficiary_relationship": { "dataType": "string", "required": true },
            "sender_beneficiary_relationship_remarks": { "dataType": "string" },
            "transfer_amount": { "dataType": "double", "required": true },
            "bank_account_id": { "dataType": "double", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CustomerModels.RequestCreateTransfer": {
        "dataType": "refObject",
        "properties": {
            "sender_source_of_fund": { "dataType": "string", "required": true },
            "sender_source_of_fund_remarks": { "dataType": "string" },
            "purpose_of_remittance": { "dataType": "string", "required": true },
            "purpose_of_remittance_remarks": { "dataType": "string" },
            "sender_beneficiary_relationship": { "dataType": "string", "required": true },
            "sender_beneficiary_relationship_remarks": { "dataType": "string" },
            "transfer_amount": { "dataType": "double", "required": true },
            "bank_account_id": { "dataType": "double", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CustomerModels.RequestGetTransactions": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "FireblocksNetworkLinkModels.RequestGetAllAccountTypes": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "FireblocksNetworkLinkModels.RequestGetDepositWalletAddress": {
        "dataType": "refObject",
        "properties": {
            "accountType": { "dataType": "string", "required": true },
            "coinSymbol": { "dataType": "string", "required": true },
            "network": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "FireblocksNetworkLinkModels.RequestCreateDepositWalletAddress": {
        "dataType": "refObject",
        "properties": {
            "accountType": { "dataType": "string", "required": true },
            "coinSymbol": { "dataType": "string", "required": true },
            "network": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "FireblocksNetworkLinkModels.RequestGetWithDrawalFee": {
        "dataType": "refObject",
        "properties": {
            "transferAmount": { "dataType": "double", "required": true },
            "coinSymbol": { "dataType": "string", "required": true },
            "network": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "FireblocksNetworkLinkModels.RequestCreateWithDraw": {
        "dataType": "refObject",
        "properties": {
            "amount": { "dataType": "double", "required": true },
            "maxFee": { "dataType": "string" },
            "coinSymbol": { "dataType": "string", "required": true },
            "network": { "dataType": "string", "required": true },
            "accountType": { "dataType": "string", "required": true },
            "toAddress": { "dataType": "string", "required": true },
            "tag": { "dataType": "string" },
            "isGross": { "dataType": "boolean", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "FireblocksNetworkLinkModels.RequestGetTransactionById": {
        "dataType": "refObject",
        "properties": {
            "transactionID": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "FireblocksNetworkLinkModels.RequestGetTransactionByHash": {
        "dataType": "refObject",
        "properties": {
            "txHash": { "dataType": "string", "required": true },
            "network": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "FireblocksNetworkLinkModels.RequestGetTransactionHistory": {
        "dataType": "refObject",
        "properties": {
            "fromDate": { "dataType": "string", "required": true },
            "toDate": { "dataType": "string", "required": true },
            "pageSize": { "dataType": "string", "required": true },
            "pageCursor": { "dataType": "string", "required": true },
            "isSubTransfer": { "dataType": "string", "required": true },
            "coinSymbol": { "dataType": "string", "required": true },
            "network": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "FireblocksNetworkLinkModels.RequestGetSupportedAssets": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "FireblocksNetworkLinkModels.RequestCreateSubMainTransfer": {
        "dataType": "refObject",
        "properties": {
            "subAccountID": { "dataType": "string", "required": true },
            "direction": { "dataType": "string", "required": true },
            "coinSymbol": { "dataType": "string", "required": true },
            "amount": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "FireblocksNetworkLinkModels.RequestCreateSubAccountsTransfer": {
        "dataType": "refObject",
        "properties": {
            "srcSubAccountID": { "dataType": "string", "required": true },
            "dstSubAccountID": { "dataType": "string", "required": true },
            "coinSymbol": { "dataType": "string", "required": true },
            "amount": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "FireblocksNetworkLinkModels.RequestCreateInternalTransfer": {
        "dataType": "refObject",
        "properties": {
            "fromAccountType": { "dataType": "string", "required": true },
            "toAccountType": { "dataType": "string", "required": true },
            "coinSymbol": { "dataType": "string", "required": true },
            "amount": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "CustomerModels.RequestForgotPassword": {
        "dataType": "refObject",
        "properties": {
            "email": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.RequestGetPurposeTransfer": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.RequestGetRelationToReceiver": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.RequestGetSourceOfFund": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.RequestGetOccupation": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.RequestGetDocumentType": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.RequestGetNationalityList": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.RequestGetCountryList": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.RequestGetBankList": {
        "dataType": "refObject",
        "properties": {
            "country_code": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.RequestGetCatalogue": {
        "dataType": "refObject",
        "properties": {
            "catalogue_type": { "dataType": "string", "required": true },
            "additional_field_1": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.RequestGetExchangeRate": {
        "dataType": "refObject",
        "properties": {
            "transfer_amount": { "dataType": "double", "required": true },
            "calc_by": { "dataType": "string", "required": true },
            "payout_currency": { "dataType": "string", "required": true },
            "payment_mode": { "dataType": "string", "required": true },
            "bank_id": { "dataType": "string", "required": true },
            "payout_country": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.SendTransaction": {
        "dataType": "refObject",
        "properties": {
            "sender_source_of_fund": { "dataType": "string", "required": true },
            "sender_source_of_fund_remarks": { "dataType": "string", "required": true },
            "sender_beneficiary_relationship": { "dataType": "string", "required": true },
            "sender_beneficiary_relationship_remarks": { "dataType": "string", "required": true },
            "purpose_of_remittance": { "dataType": "string", "required": true },
            "purpose_of_remittance_remarks": { "dataType": "string", "required": true },
            "calc_by": { "dataType": "string", "required": true },
            "transfer_amount": { "dataType": "double", "required": true },
            "remit_currency": { "dataType": "string", "required": true },
            "customer_id": { "dataType": "string", "required": true },
            "receiver_id": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.CommitTransaction": {
        "dataType": "refObject",
        "properties": {
            "transaction_id": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.QueryTransaction": {
        "dataType": "refObject",
        "properties": {
            "pin_number": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "LqnModels.RequestCallback": {
        "dataType": "refObject",
        "properties": {
            "agentTxnId": { "dataType": "string", "required": true },
            "pinNumber": { "dataType": "string", "required": true },
            "status": { "dataType": "string", "required": true },
            "message": { "dataType": "string", "required": true },
            "notification_ts": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "TransactionModels.RequestGetAdminTransactions": {
        "dataType": "refObject",
        "properties": {},
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "TransactionModels.RequestGetFireblocksNotificationList": {
        "dataType": "refObject",
        "properties": {
            "from_date": { "dataType": "string", "required": true },
            "to_date": { "dataType": "string", "required": true },
            "status": { "dataType": "string" },
            "search_text": { "dataType": "string" },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "TransactionModels.RequestManualUpdateTransactionStatus": {
        "dataType": "refObject",
        "properties": {
            "fireblock_transaction_id": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "TransactionModels.RequestManualFreezeTransaction": {
        "dataType": "refObject",
        "properties": {
            "fb_transaction_id": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    "TransactionModels.RequestManualUnfreezeTransaction": {
        "dataType": "refObject",
        "properties": {
            "fb_transaction_id": { "dataType": "string", "required": true },
        },
        "additionalProperties": false,
    },
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
};
const validationService = new runtime_1.ValidationService(models);
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
function RegisterRoutes(app) {
    // ###########################################################################################################
    //  NOTE: If you do not see routes for all of your controllers in this file, then you might not have informed tsoa of where to look
    //      Please look into the "controllerPathGlobs" config option described in the readme: https://github.com/lukeautry/tsoa
    // ###########################################################################################################
    app.get('/api/v1/admin/user/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getUsers)), function AdminController_getUsers(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "AdminModels.RequestGetUsers" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getUsers.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/user/active/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getUserActiveList)), function AdminController_getUserActiveList(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "AdminModels.RequestGetUsers" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getUserActiveList.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/user/:user_id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getUserById)), function AdminController_getUserById(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                user_id: { "in": "path", "name": "user_id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getUserById.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/admin/user/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.createUser)), function AdminController_createUser(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestCreateUser" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createUser.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.put('/api/v1/admin/user/:user_id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.updateUser)), function AdminController_updateUser(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                user_id: { "in": "path", "name": "user_id", "required": true, "dataType": "double" },
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestUpdateUser" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.updateUser.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.delete('/api/v1/admin/user/:user_id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.deleteUser)), function AdminController_deleteUser(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                user_id: { "in": "path", "name": "user_id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.deleteUser.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/role/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getRoles)), function AdminController_getRoles(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "AdminModels.RequestGetRoles" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getRoles.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/active-role/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getActiveRoles)), function AdminController_getActiveRoles(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "AdminModels.RequestGetActiveRoles" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getActiveRoles.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/role/:role_id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getRoleById)), function AdminController_getRoleById(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                role_id: { "in": "path", "name": "role_id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getRoleById.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/admin/role/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.createRole)), function AdminController_createRole(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestCreateRole" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createRole.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.put('/api/v1/admin/role/:role_id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.updateRole)), function AdminController_updateRole(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                role_id: { "in": "path", "name": "role_id", "required": true, "dataType": "double" },
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestUpdateRole" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.updateRole.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.delete('/api/v1/admin/role/:role_id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.deleteRole)), function AdminController_deleteRole(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                role_id: { "in": "path", "name": "role_id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.deleteRole.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/user/menu/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getMenuByUserId)), function AdminController_getMenuByUserId(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getMenuByUserId.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/admin/user/change-password', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.changePassword)), function AdminController_changePassword(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestChangePassword" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.changePassword.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/user/:user_id/asset/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getUserAssetInVaultAccountByUserId)), function AdminController_getUserAssetInVaultAccountByUserId(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                user_id: { "in": "path", "name": "user_id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getUserAssetInVaultAccountByUserId.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/admin/user/reset-password', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.resetPassword)), function AdminController_resetPassword(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestResetPassword" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.resetPassword.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.put('/api/v1/admin/role/:role_id/menu', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.updateRoleMenuByRoleId)), function AdminController_updateRoleMenuByRoleId(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                role_id: { "in": "path", "name": "role_id", "required": true, "dataType": "double" },
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestUpdateRoleMenuByRoleId" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.updateRoleMenuByRoleId.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/master-menu/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getMasterMenu)), function AdminController_getMasterMenu(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "AdminModels.RequestGetMasterMenu" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getMasterMenu.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/company/crypto-wallet/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getCryptoWalletList)), function AdminController_getCryptoWalletList(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "AdminModels.RequestGetCryptoWalletList" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getCryptoWalletList.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/admin/transaction/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.createTransaction)), function AdminController_createTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestCreateTransaction" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/fiat-account/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getFiatAccounts)), function AdminController_getFiatAccounts(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getFiatAccounts.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/fiat-account/user/:user_id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getFiatAccountByUser)), function AdminController_getFiatAccountByUser(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                user_id: { "in": "path", "name": "user_id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getFiatAccountByUser.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.put('/api/v1/admin/fiat-account/:fiat_id/favorite', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.updateFavoriteFiatAccounts)), function AdminController_updateFavoriteFiatAccounts(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                fiat_id: { "in": "path", "name": "fiat_id", "required": true, "dataType": "double" },
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestUpdateFavoriteFiatAccounts" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.updateFavoriteFiatAccounts.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/admin/fiat-account/topup', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.createTopupTransaction)), function AdminController_createTopupTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestCreateTopup" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createTopupTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/fiat-account/topup/:wallet_credit_id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getTopupInfo)), function AdminController_getTopupInfo(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                wallet_credit_id: { "in": "path", "name": "wallet_credit_id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getTopupInfo.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/fiat-account/wallet-credit-document/:wallet_credit_document_id/download', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.downloadWalletCreditDocument)), function AdminController_downloadWalletCreditDocument(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                wallet_credit_document_id: { "in": "path", "name": "wallet_credit_document_id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.downloadWalletCreditDocument.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/fiat-account/wallet-credit-document/:wallet_credit_document_id/preview', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.previewWalletCreditDocument)), function AdminController_previewWalletCreditDocument(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                wallet_credit_document_id: { "in": "path", "name": "wallet_credit_document_id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.previewWalletCreditDocument.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/bank-account/user/:user_id/owner/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getOwnerBankAccounts)), function AdminController_getOwnerBankAccounts(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                user_id: { "in": "path", "name": "user_id", "required": true, "dataType": "double" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getOwnerBankAccounts.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/admin/bank-account/user/:user_id/other/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.getOtherBankAccounts)), function AdminController_getOtherBankAccounts(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                user_id: { "in": "path", "name": "user_id", "required": true, "dataType": "double" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getOtherBankAccounts.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/admin/withdraw/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.createWithdrawTransaction)), function AdminController_createWithdrawTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestCreateWithdraw" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createWithdrawTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/admin/transfer/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.createTransferTransaction)), function AdminController_createTransferTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestCreateTransfer" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createTransferTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/admin/bank-account/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController)), ...((0, runtime_1.fetchMiddlewares)(adminController_1.AdminController.prototype.createBankAccount)), function AdminController_createBankAccount(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "AdminModels.RequestCreateBankAccount" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(adminController_1.AdminController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createBankAccount.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/auth/login', ...((0, runtime_1.fetchMiddlewares)(authController_1.AuthController)), ...((0, runtime_1.fetchMiddlewares)(authController_1.AuthController.prototype.login)), function AuthController_login(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "AuthModels.RequestLogin" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(authController_1.AuthController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.login.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/auth/refresh', ...((0, runtime_1.fetchMiddlewares)(authController_1.AuthController)), ...((0, runtime_1.fetchMiddlewares)(authController_1.AuthController.prototype.refresh)), function AuthController_refresh(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "AuthModels.RequestRefreshToken" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(authController_1.AuthController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.refresh.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/wallet/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getWallets)), function CactusCustodyController_getWallets(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getWallets.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/wallet/:wallet_code', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getWalletInfo)), function CactusCustodyController_getWalletInfo(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                wallet_code: { "in": "path", "name": "wallet_code", "required": true, "dataType": "string" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getWalletInfo.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/coin-infos', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getCoinInfo)), function CactusCustodyController_getCoinInfo(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getCoinInfo.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/chain-infos', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getChainInfo)), function CactusCustodyController_getChainInfo(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getChainInfo.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/cactus/wallet/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.createWallet)), function CactusCustodyController_createWallet(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "CactusCustodyModels.RequestCreateWallet" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createWallet.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/cactus/wallet/:wallet_code/addresses/apply', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.applyNewAddress)), function CactusCustodyController_applyNewAddress(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                wallet_code: { "in": "path", "name": "wallet_code", "required": true, "dataType": "string" },
                req: { "in": "body", "name": "req", "required": true, "ref": "CactusCustodyModels.RequestApplyNewAddress" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.applyNewAddress.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/wallet/:wallet_code/addresses/:address', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getAddressInfo)), function CactusCustodyController_getAddressInfo(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                wallet_code: { "in": "path", "name": "wallet_code", "required": true, "dataType": "string" },
                address: { "in": "path", "name": "address", "required": true, "dataType": "string" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getAddressInfo.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/wallet/:wallet_code/addresses', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getAddressList)), function CactusCustodyController_getAddressList(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                wallet_code: { "in": "path", "name": "wallet_code", "required": true, "dataType": "string" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getAddressList.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.put('/api/v1/cactus/wallet/:wallet_code/addresses/:address', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.editAddress)), function CactusCustodyController_editAddress(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                wallet_code: { "in": "path", "name": "wallet_code", "required": true, "dataType": "string" },
                address: { "in": "path", "name": "address", "required": true, "dataType": "string" },
                req: { "in": "body", "name": "req", "required": true, "ref": "CactusCustodyModels.RequestEditAddress" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.editAddress.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/cactus/addresses/type/check', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.verifyAddressFormat)), function CactusCustodyController_verifyAddressFormat(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "CactusCustodyModels.RequestVerifyAddressFormat" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.verifyAddressFormat.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/cactus/estimate-miner-fee', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.estimateGasFee)), function CactusCustodyController_estimateGasFee(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "CactusCustodyModels.RequestEstimateGasFee" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.estimateGasFee.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/cactus/order/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.createWithdrawalOrder)), function CactusCustodyController_createWithdrawalOrder(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "CactusCustodyModels.RequestCreateWithdrawalOrder" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createWithdrawalOrder.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/customize-fee-rate/range', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getWithdrawalRateRange)), function CactusCustodyController_getWithdrawalRateRange(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getWithdrawalRateRange.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/recommend-fee-rate/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getWithdrawalRate)), function CactusCustodyController_getWithdrawalRate(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getWithdrawalRate.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/wallets/:wallet_code/tx-summaries', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getWalletTransactionSummary)), function CactusCustodyController_getWalletTransactionSummary(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                wallet_code: { "in": "path", "name": "wallet_code", "required": true, "dataType": "string" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getWalletTransactionSummary.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/wallets/:wallet_code/tx-details', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getTransactionDetails)), function CactusCustodyController_getTransactionDetails(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                wallet_code: { "in": "path", "name": "wallet_code", "required": true, "dataType": "string" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getTransactionDetails.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/cactus/wallets/:wallet_code/details/:id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.editTransactionDetailRemark)), function CactusCustodyController_editTransactionDetailRemark(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                wallet_code: { "in": "path", "name": "wallet_code", "required": true, "dataType": "string" },
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
                req: { "in": "body", "name": "req", "required": true, "ref": "CactusCustodyModels.RequestEditTransactionDetailRemark" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.editTransactionDetailRemark.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/wallets/:wallet_code/customize-flow', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getTransactionDetailsForCustomCurrency)), function CactusCustodyController_getTransactionDetailsForCustomCurrency(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                wallet_code: { "in": "path", "name": "wallet_code", "required": true, "dataType": "string" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getTransactionDetailsForCustomCurrency.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/history-asset', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getTotalAssetHistoryNotionalValue)), function CactusCustodyController_getTotalAssetHistoryNotionalValue(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getTotalAssetHistoryNotionalValue.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/asset', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getCurrentAssetNotionalValue)), function CactusCustodyController_getCurrentAssetNotionalValue(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getCurrentAssetNotionalValue.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/orders', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getOrdersFilterByCriteria)), function CactusCustodyController_getOrdersFilterByCriteria(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getOrdersFilterByCriteria.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/cactus/orders/:order_no', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.getOrderDetails)), function CactusCustodyController_getOrderDetails(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                order_no: { "in": "path", "name": "order_no", "required": true, "dataType": "string" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getOrderDetails.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/cactus/orders/:order_no/accelerate', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.rbf)), function CactusCustodyController_rbf(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                order_no: { "in": "path", "name": "order_no", "required": true, "dataType": "string" },
                req: { "in": "body", "name": "req", "required": true, "ref": "CactusCustodyModels.RequestRBF" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.rbf.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/cactus/orders/:order_no/cancel', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyController_1.CactusCustodyController.prototype.cancelOrder)), function CactusCustodyController_cancelOrder(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                order_no: { "in": "path", "name": "order_no", "required": true, "dataType": "string" },
                req: { "in": "body", "name": "req", "required": true, "ref": "CactusCustodyModels.RequestCancelOrder" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyController_1.CactusCustodyController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.cancelOrder.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/cactus/webhook/notification', ...((0, runtime_1.fetchMiddlewares)(cactusCustodyWebhookController_1.CactusCustodyWebhookController)), ...((0, runtime_1.fetchMiddlewares)(cactusCustodyWebhookController_1.CactusCustodyWebhookController.prototype.notification)), function CactusCustodyWebhookController_notification(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cactusCustodyWebhookController_1.CactusCustodyWebhookController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.notification.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/config/asset/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.getAssets)), function ConfigController_getAssets(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "ConfigModels.RequestGetAssetsInVaultAccount" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getAssets.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/config/asset/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.createAsset)), function ConfigController_createAsset(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "ConfigModels.RequestCreateAssetInVaultAccount" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createAsset.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/config/asset-rate/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.getAssetRates)), function ConfigController_getAssetRates(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "ConfigModels.RequestGetAssetRates" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getAssetRates.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/config/asset-rate/:id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.getAssetRateById)), function ConfigController_getAssetRateById(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getAssetRateById.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/config/asset-rate-by-code/:asset_code', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.getAssetRateByAssetCode)), function ConfigController_getAssetRateByAssetCode(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                asset_code: { "in": "path", "name": "asset_code", "required": true, "dataType": "string" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getAssetRateByAssetCode.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/config/asset-rate/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.createAssetRate)), function ConfigController_createAssetRate(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "ConfigModels.RequestCreateAssetRate" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createAssetRate.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.put('/api/v1/config/asset-rate/:id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.updateAssetRate)), function ConfigController_updateAssetRate(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
                req: { "in": "body", "name": "req", "required": true, "ref": "ConfigModels.RequestUpdateAssetRate" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.updateAssetRate.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.delete('/api/v1/config/asset-rate/:id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.deleteAssetRate)), function ConfigController_deleteAssetRate(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.deleteAssetRate.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/config/network/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.getNetworks)), function ConfigController_getNetworks(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "ConfigModels.RequestGetNetworks" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getNetworks.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/config/fb-asset/list/:network_id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.getFBAssets)), function ConfigController_getFBAssets(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                network_id: { "in": "path", "name": "network_id", "required": true, "dataType": "double" },
                req: { "in": "queries", "name": "req", "required": true, "ref": "ConfigModels.RequestGetAssets" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getFBAssets.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/config/dropdown/purpose-of-transfer', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.getPurposeOfTransfer)), function ConfigController_getPurposeOfTransfer(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getPurposeOfTransfer.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/config/dropdown/relationship-to-receiver', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.getRelationshipToReceiver)), function ConfigController_getRelationshipToReceiver(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getRelationshipToReceiver.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/config/dropdown/:type', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController)), ...((0, runtime_1.fetchMiddlewares)(configController_1.ConfigController.prototype.getDropdownListByType)), function ConfigController_getDropdownListByType(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                type: { "in": "path", "name": "type", "required": true, "dataType": "string" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(configController_1.ConfigController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getDropdownListByType.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/job/start', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cronJobController_1.CronJobController)), ...((0, runtime_1.fetchMiddlewares)(cronJobController_1.CronJobController.prototype.start)), function CronJobController_start(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cronJobController_1.CronJobController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.start.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/job/stop', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cronJobController_1.CronJobController)), ...((0, runtime_1.fetchMiddlewares)(cronJobController_1.CronJobController.prototype.stop)), function CronJobController_stop(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cronJobController_1.CronJobController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.stop.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/job/chainalysis/manual', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(cronJobController_1.CronJobController)), ...((0, runtime_1.fetchMiddlewares)(cronJobController_1.CronJobController.prototype.manualUpdateTransactionStatus)), function CronJobController_manualUpdateTransactionStatus(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "CronJobModels.RequestManualUpdateTransactionStatus" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(cronJobController_1.CronJobController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.manualUpdateTransactionStatus.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/api-key/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getUserApiKeys)), function CustomerController_getUserApiKeys(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "CustomerModels.RequestGetUserApiKeys" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getUserApiKeys.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/api-key/:id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getUserApiKeyById)), function CustomerController_getUserApiKeyById(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getUserApiKeyById.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/customer/api-key/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.createUserApiKey)), function CustomerController_createUserApiKey(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "CustomerModels.RequestCreateUserApiKey" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createUserApiKey.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.put('/api/v1/customer/api-key/disable/:id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.disableUserApiKey)), function CustomerController_disableUserApiKey(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.disableUserApiKey.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.put('/api/v1/customer/api-key/all/disable', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.disableAllUserApiKeyByUserId)), function CustomerController_disableAllUserApiKeyByUserId(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.disableAllUserApiKeyByUserId.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/customer/account/asset/request', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.requestAccessAsset)), function CustomerController_requestAccessAsset(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "CustomerModels.RequestRequestAccessAsset" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.requestAccessAsset.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/fiat-account/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getUserFiatAccounts)), function CustomerController_getUserFiatAccounts(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "CustomerModels.RequestGetUserFiatAccounts" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getUserFiatAccounts.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/bank-account/country/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getBankAccountCoutries)), function CustomerController_getBankAccountCoutries(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getBankAccountCoutries.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/bank-account/owner/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getOwnerBankAccounts)), function CustomerController_getOwnerBankAccounts(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getOwnerBankAccounts.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/bank-account/other/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getOtherBankAccounts)), function CustomerController_getOtherBankAccounts(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getOtherBankAccounts.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/bank-account/:id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getBankAccountById)), function CustomerController_getBankAccountById(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getBankAccountById.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/customer/bank-account/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.createBankAccount)), function CustomerController_createBankAccount(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "CustomerModels.RequestCreateBankAccount" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createBankAccount.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.delete('/api/v1/customer/bank-account/:id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.deleteBankAccount)), function CustomerController_deleteBankAccount(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.deleteBankAccount.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/crypto-wallet/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getCryptoWalletListForUser)), function CustomerController_getCryptoWalletListForUser(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "CustomerModels.RequestGetCryptoWalletListForUser" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getCryptoWalletListForUser.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/crypto-wallet/:id/total/:asset_group', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getTotalAssetForUser)), function CustomerController_getTotalAssetForUser(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
                asset_group: { "in": "path", "name": "asset_group", "required": true, "dataType": "string" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getTotalAssetForUser.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/customer/transaction/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.createTransaction)), function CustomerController_createTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "CustomerModels.RequestCreateTransaction" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/withdraw/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getWithdraws)), function CustomerController_getWithdraws(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "CustomerModels.RequestGetWithdraws" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getWithdraws.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/withdraw/:id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getWithdrawById)), function CustomerController_getWithdrawById(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getWithdrawById.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/customer/withdraw/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.createWithdrawTransaction)), function CustomerController_createWithdrawTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "CustomerModels.RequestCreateWithdraw" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createWithdrawTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/customer/transfer/create', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.createTransferTransaction)), function CustomerController_createTransferTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "CustomerModels.RequestCreateTransfer" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createTransferTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/transaction/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getTransactions)), function CustomerController_getTransactions(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "CustomerModels.RequestGetTransactions" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getTransactions.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/profile', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getUserProfile)), function CustomerController_getUserProfile(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getUserProfile.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/:id/network/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getNetworksByUserId)), function CustomerController_getNetworksByUserId(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getNetworksByUserId.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/customer/:id/network/:network_id/asset/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController)), ...((0, runtime_1.fetchMiddlewares)(customerController_1.CustomerController.prototype.getAssetsByUserIdNetworkId)), function CustomerController_getAssetsByUserIdNetworkId(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
                network_id: { "in": "path", "name": "network_id", "required": true, "dataType": "double" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(customerController_1.CustomerController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getAssetsByUserIdNetworkId.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/fireblocks/v1/accounts', authenticateMiddleware([{ "network_link": [] }]), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController.prototype.getAllAccountTypes)), function FireblocksNetworkLinkController_getAllAccountTypes(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "FireblocksNetworkLinkModels.RequestGetAllAccountTypes" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getAllAccountTypes.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/fireblocks/v1/depositAddress', authenticateMiddleware([{ "network_link": [] }]), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController.prototype.getDepositWalletAddress)), function FireblocksNetworkLinkController_getDepositWalletAddress(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "FireblocksNetworkLinkModels.RequestGetDepositWalletAddress" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getDepositWalletAddress.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/fireblocks/v1/depositAddress', authenticateMiddleware([{ "network_link": [] }]), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController.prototype.createDepositWalletAddress)), function FireblocksNetworkLinkController_createDepositWalletAddress(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "FireblocksNetworkLinkModels.RequestCreateDepositWalletAddress" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createDepositWalletAddress.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/fireblocks/v1/withdrawalFee', authenticateMiddleware([{ "network_link": [] }]), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController.prototype.getWithDrawalFee)), function FireblocksNetworkLinkController_getWithDrawalFee(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "FireblocksNetworkLinkModels.RequestGetWithDrawalFee" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getWithDrawalFee.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/fireblocks/v1/withdraw', authenticateMiddleware([{ "network_link": [] }]), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController.prototype.createWithDraw)), function FireblocksNetworkLinkController_createWithDraw(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "FireblocksNetworkLinkModels.RequestCreateWithDraw" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createWithDraw.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/fireblocks/v1/transactionByID', authenticateMiddleware([{ "network_link": [] }]), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController.prototype.getTransactionById)), function FireblocksNetworkLinkController_getTransactionById(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "FireblocksNetworkLinkModels.RequestGetTransactionById" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getTransactionById.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/fireblocks/v1/transactionByHash', authenticateMiddleware([{ "network_link": [] }]), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController.prototype.getTransactionByHash)), function FireblocksNetworkLinkController_getTransactionByHash(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "FireblocksNetworkLinkModels.RequestGetTransactionByHash" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getTransactionByHash.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/fireblocks/v1/transactionHistory', authenticateMiddleware([{ "network_link": [] }]), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController.prototype.getTransactionHistory)), function FireblocksNetworkLinkController_getTransactionHistory(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "FireblocksNetworkLinkModels.RequestGetTransactionHistory" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getTransactionHistory.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/fireblocks/v1/supportedAssets', authenticateMiddleware([{ "network_link": [] }]), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController.prototype.getSupportedAssets)), function FireblocksNetworkLinkController_getSupportedAssets(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "FireblocksNetworkLinkModels.RequestGetSupportedAssets" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getSupportedAssets.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/fireblocks/v1/subMainTransfer', authenticateMiddleware([{ "network_link": [] }]), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController.prototype.createSubMainTransfer)), function FireblocksNetworkLinkController_createSubMainTransfer(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "FireblocksNetworkLinkModels.RequestCreateSubMainTransfer" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createSubMainTransfer.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/fireblocks/v1/subaccountsTransfer', authenticateMiddleware([{ "network_link": [] }]), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController.prototype.createSubAccountsTransfer)), function FireblocksNetworkLinkController_createSubAccountsTransfer(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "FireblocksNetworkLinkModels.RequestCreateSubAccountsTransfer" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createSubAccountsTransfer.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/fireblocks/v1/internalTransfer', authenticateMiddleware([{ "network_link": [] }]), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController.prototype.createInternalTransfer)), function FireblocksNetworkLinkController_createInternalTransfer(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "FireblocksNetworkLinkModels.RequestCreateInternalTransfer" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksNetworkLinkController_1.FireblocksNetworkLinkController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.createInternalTransfer.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/webhook/notification', ...((0, runtime_1.fetchMiddlewares)(fireblocksWebhookController_1.FireblocksWebhookController)), ...((0, runtime_1.fetchMiddlewares)(fireblocksWebhookController_1.FireblocksWebhookController.prototype.notification)), function FireblocksWebhookController_notification(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(fireblocksWebhookController_1.FireblocksWebhookController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.notification.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/forgotpassword', ...((0, runtime_1.fetchMiddlewares)(forgotPasswordController_1.ForgotPasswordController)), ...((0, runtime_1.fetchMiddlewares)(forgotPasswordController_1.ForgotPasswordController.prototype.submitForgotPassword)), function ForgotPasswordController_submitForgotPassword(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "CustomerModels.RequestForgotPassword" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(forgotPasswordController_1.ForgotPasswordController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.submitForgotPassword.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/lqn/dropdown/purpose-of-transfer', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.getPurposeTransfer)), function LQNController_getPurposeTransfer(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "LqnModels.RequestGetPurposeTransfer" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getPurposeTransfer.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/lqn/dropdown/relationship-to-receiver', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.getRelationToReceiver)), function LQNController_getRelationToReceiver(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "LqnModels.RequestGetRelationToReceiver" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getRelationToReceiver.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/lqn/dropdown/source-of-fund', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.getSourceOfFund)), function LQNController_getSourceOfFund(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "LqnModels.RequestGetSourceOfFund" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getSourceOfFund.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/lqn/dropdown/occupation', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.getOccupation)), function LQNController_getOccupation(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "LqnModels.RequestGetOccupation" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getOccupation.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/lqn/dropdown/document-type', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.getDocumentType)), function LQNController_getDocumentType(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "LqnModels.RequestGetDocumentType" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getDocumentType.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/lqn/dropdown/nationality', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.getNationality)), function LQNController_getNationality(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "LqnModels.RequestGetNationalityList" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getNationality.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/lqn/dropdown/country', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.getCountry)), function LQNController_getCountry(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "LqnModels.RequestGetCountryList" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getCountry.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/lqn/dropdown/bank', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.getAgentList)), function LQNController_getAgentList(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "LqnModels.RequestGetBankList" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getAgentList.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/lqn/catalogue', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.getCatalogue)), function LQNController_getCatalogue(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "LqnModels.RequestGetCatalogue" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getCatalogue.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/lqn/exchangerate', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.getExchangeRate)), function LQNController_getExchangeRate(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "LqnModels.RequestGetExchangeRate" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getExchangeRate.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/lqn/sendTransaction', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.sendTransaction)), function LQNController_sendTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "LqnModels.SendTransaction" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.sendTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/lqn/commitTransaction', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.commitTransaction)), function LQNController_commitTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "LqnModels.CommitTransaction" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.commitTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/lqn/queryTransaction', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.queryTransaction)), function LQNController_queryTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "LqnModels.QueryTransaction" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.queryTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/lqn/callback', ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController)), ...((0, runtime_1.fetchMiddlewares)(lqnController_1.LQNController.prototype.callback)), function LQNController_callback(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "LqnModels.RequestCallback" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(lqnController_1.LQNController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.callback.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/master/country/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(masterController_1.MasterController)), ...((0, runtime_1.fetchMiddlewares)(masterController_1.MasterController.prototype.getCountries)), function MasterController_getCountries(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(masterController_1.MasterController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getCountries.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/master/currency/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(masterController_1.MasterController)), ...((0, runtime_1.fetchMiddlewares)(masterController_1.MasterController.prototype.getCurrencies)), function MasterController_getCurrencies(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(masterController_1.MasterController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getCurrencies.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/master/nationality/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(masterController_1.MasterController)), ...((0, runtime_1.fetchMiddlewares)(masterController_1.MasterController.prototype.getNationality)), function MasterController_getNationality(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {};
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(masterController_1.MasterController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getNationality.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/transaction/admin/list', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController)), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController.prototype.getAdminTransactions)), function TransactionController_getAdminTransactions(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "TransactionModels.RequestGetAdminTransactions" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(transactionController_1.TransactionController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getAdminTransactions.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/transaction/admin/fireblocks/notifications', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController)), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController.prototype.getFireblocksNotificationList)), function TransactionController_getFireblocksNotificationList(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "queries", "name": "req", "required": true, "ref": "TransactionModels.RequestGetFireblocksNotificationList" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(transactionController_1.TransactionController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getFireblocksNotificationList.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.put('/api/v1/transaction/admin/transaction/:id/submit', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController)), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController.prototype.submitBuySellTransaction)), function TransactionController_submitBuySellTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
                reqIncomingMessage: { "in": "request", "name": "reqIncomingMessage", "required": true, "dataType": "object" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(transactionController_1.TransactionController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.submitBuySellTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/transaction/manual-update-transaction-status', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController)), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController.prototype.manualUpdateTransactionStatus)), function TransactionController_manualUpdateTransactionStatus(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "TransactionModels.RequestManualUpdateTransactionStatus" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(transactionController_1.TransactionController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.manualUpdateTransactionStatus.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/transaction/manual/freeze', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController)), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController.prototype.manualFreezeTransaction)), function TransactionController_manualFreezeTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "TransactionModels.RequestManualFreezeTransaction" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(transactionController_1.TransactionController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.manualFreezeTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.post('/api/v1/transaction/manual/unfreeze', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController)), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController.prototype.manualUnfreezeTransaction)), function TransactionController_manualUnfreezeTransaction(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "ref": "TransactionModels.RequestManualUnfreezeTransaction" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(transactionController_1.TransactionController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.manualUnfreezeTransaction.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/transaction/admin/withdraw/:id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController)), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController.prototype.getWithdrawById)), function TransactionController_getWithdrawById(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(transactionController_1.TransactionController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getWithdrawById.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    app.get('/api/v1/transaction/admin/convert/:id', authenticateMiddleware([{ "api_key": [] }]), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController)), ...((0, runtime_1.fetchMiddlewares)(transactionController_1.TransactionController.prototype.getConvertById)), function TransactionController_getConvertById(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const args = {
                id: { "in": "path", "name": "id", "required": true, "dataType": "double" },
            };
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            let validatedArgs = [];
            try {
                validatedArgs = getValidatedArgs(args, request, response);
                const container = typeof tsyringeTsoaIocContainer_1.iocContainer === 'function' ? tsyringeTsoaIocContainer_1.iocContainer(request) : tsyringeTsoaIocContainer_1.iocContainer;
                const controller = yield container.get(transactionController_1.TransactionController);
                if (typeof controller['setStatus'] === 'function') {
                    controller.setStatus(undefined);
                }
                const promise = controller.getConvertById.apply(controller, validatedArgs);
                promiseHandler(controller, promise, response, undefined, next);
            }
            catch (err) {
                return next(err);
            }
        });
    });
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    function authenticateMiddleware(security = []) {
        return function runAuthenticationMiddleware(request, _response, next) {
            return __awaiter(this, void 0, void 0, function* () {
                // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
                // keep track of failed auth attempts so we can hand back the most
                // recent one.  This behavior was previously existing so preserving it
                // here
                const failedAttempts = [];
                const pushAndRethrow = (error) => {
                    failedAttempts.push(error);
                    throw error;
                };
                const secMethodOrPromises = [];
                for (const secMethod of security) {
                    if (Object.keys(secMethod).length > 1) {
                        const secMethodAndPromises = [];
                        for (const name in secMethod) {
                            secMethodAndPromises.push((0, authentication_1.expressAuthentication)(request, name, secMethod[name])
                                .catch(pushAndRethrow));
                        }
                        // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
                        secMethodOrPromises.push(Promise.all(secMethodAndPromises)
                            .then(users => { return users[0]; }));
                    }
                    else {
                        for (const name in secMethod) {
                            secMethodOrPromises.push((0, authentication_1.expressAuthentication)(request, name, secMethod[name])
                                .catch(pushAndRethrow));
                        }
                    }
                }
                // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
                try {
                    request['user'] = yield promiseAny.call(Promise, secMethodOrPromises);
                    next();
                }
                catch (err) {
                    // Show most recent error as response
                    const error = failedAttempts.pop();
                    error.status = error.status || 401;
                    next(error);
                }
                // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            });
        };
    }
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    function isController(object) {
        return 'getHeaders' in object && 'getStatus' in object && 'setStatus' in object;
    }
    function promiseHandler(controllerObj, promise, response, successStatus, next) {
        return Promise.resolve(promise)
            .then((data) => {
            let statusCode = successStatus;
            let headers;
            if (isController(controllerObj)) {
                headers = controllerObj.getHeaders();
                statusCode = controllerObj.getStatus() || statusCode;
            }
            // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
            returnHandler(response, statusCode, data, headers);
        })
            .catch((error) => next(error));
    }
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    function returnHandler(response, statusCode, data, headers = {}) {
        if (response.headersSent) {
            return;
        }
        Object.keys(headers).forEach((name) => {
            response.set(name, headers[name]);
        });
        if (data && typeof data.pipe === 'function' && data.readable && typeof data._read === 'function') {
            response.status(statusCode || 200);
            data.pipe(response);
        }
        else if (data !== null && data !== undefined) {
            response.status(statusCode || 200).json(data);
        }
        else {
            response.status(statusCode || 204).end();
        }
    }
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    function responder(response) {
        return function (status, data, headers) {
            returnHandler(response, status, data, headers);
        };
    }
    ;
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
    function getValidatedArgs(args, request, response) {
        const fieldErrors = {};
        const values = Object.keys(args).map((key) => {
            const name = args[key].name;
            switch (args[key].in) {
                case 'request':
                    return request;
                case 'query':
                    return validationService.ValidateParam(args[key], request.query[name], name, fieldErrors, undefined, { "noImplicitAdditionalProperties": "throw-on-extras" });
                case 'queries':
                    return validationService.ValidateParam(args[key], request.query, name, fieldErrors, undefined, { "noImplicitAdditionalProperties": "throw-on-extras" });
                case 'path':
                    return validationService.ValidateParam(args[key], request.params[name], name, fieldErrors, undefined, { "noImplicitAdditionalProperties": "throw-on-extras" });
                case 'header':
                    return validationService.ValidateParam(args[key], request.header(name), name, fieldErrors, undefined, { "noImplicitAdditionalProperties": "throw-on-extras" });
                case 'body':
                    return validationService.ValidateParam(args[key], request.body, name, fieldErrors, undefined, { "noImplicitAdditionalProperties": "throw-on-extras" });
                case 'body-prop':
                    return validationService.ValidateParam(args[key], request.body[name], name, fieldErrors, 'body.', { "noImplicitAdditionalProperties": "throw-on-extras" });
                case 'formData':
                    if (args[key].dataType === 'file') {
                        return validationService.ValidateParam(args[key], request.file, name, fieldErrors, undefined, { "noImplicitAdditionalProperties": "throw-on-extras" });
                    }
                    else if (args[key].dataType === 'array' && args[key].array.dataType === 'file') {
                        return validationService.ValidateParam(args[key], request.files, name, fieldErrors, undefined, { "noImplicitAdditionalProperties": "throw-on-extras" });
                    }
                    else {
                        return validationService.ValidateParam(args[key], request.body[name], name, fieldErrors, undefined, { "noImplicitAdditionalProperties": "throw-on-extras" });
                    }
                case 'res':
                    return responder(response);
            }
        });
        if (Object.keys(fieldErrors).length > 0) {
            throw new runtime_1.ValidateError(fieldErrors, '');
        }
        return values;
    }
    // WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
}
exports.RegisterRoutes = RegisterRoutes;
// WARNING: This file was auto-generated with tsoa. Please do not modify it. Re-run tsoa to re-generate this file: https://github.com/lukeautry/tsoa
//# sourceMappingURL=routes.js.map