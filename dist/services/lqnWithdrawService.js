"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LqnWithdrawService = void 0;
const tsyringe_1 = require("tsyringe");
const customError_1 = __importDefault(require("../middlewares/customError"));
const countryRepository_1 = require("../repositories/countryRepository");
const lqnTransactionRepository_1 = require("../repositories/lqnTransactionRepository");
const lqnUserRepository_1 = require("../repositories/lqnUserRepository");
const nationalityRepository_1 = require("../repositories/nationalityRepository");
const withdrawRepository_1 = require("../repositories/withdrawRepository");
const enums = __importStar(require("../utilities/enums"));
const lqnService_1 = require("./lqnService");
let LqnWithdrawService = exports.LqnWithdrawService = class LqnWithdrawService {
    constructor(lqnUserRepository, lqnService, nationlityRepository, countryRepository, lqnTransactionRepository, withdrawRepository) {
        this._lqnUserRepository = lqnUserRepository;
        this._lqnService = lqnService;
        this._nationlityRepository = nationlityRepository;
        this._countryRepository = countryRepository;
        this._lqnTransactionRepository = lqnTransactionRepository;
        this._withdrawRepository = withdrawRepository;
    }
    CreateSender(senderFirstName, senderMiddleName, senderLastName, senderGender, senderMobile, senderNationality, senderIdType, senderIdNumber, senderIdIssueCountry, senderIdIssueDate, senderIdExpireDate, senderDateOfBirth, senderOccupation, senderEmail, senderAddress, senderCity, senderState, senderZipCode, senderCountry, logon, userId, currencyCode, senderOccupationRemarks, senderIdTypeRemarks) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var senderLqn = yield this._lqnService.createSender("I", senderFirstName, senderMiddleName, senderLastName, senderGender, senderAddress, senderCity, senderState, senderZipCode, senderCountry, senderMobile, senderNationality, senderIdType, senderIdNumber, senderIdIssueCountry, senderIdIssueDate, senderIdExpireDate, senderDateOfBirth, senderOccupation, "", "", senderEmail, "", "", "", "", senderOccupationRemarks, senderIdTypeRemarks, "", "", "", "", "");
                if (senderLqn.code != "0") {
                    throw new customError_1.default(senderLqn.code, senderLqn.message);
                }
                return yield this._lqnUserRepository.create(enums.lqnUserType.SENDER, senderFirstName, senderMiddleName, senderLastName, senderGender, senderMobile, senderNationality, senderIdType, senderIdTypeRemarks, senderIdNumber, senderIdIssueCountry, senderIdIssueDate, senderIdExpireDate, senderDateOfBirth, senderOccupation, senderOccupationRemarks, senderEmail, "", "", "", senderLqn.customerId, logon, "", currencyCode, enums.status.ACTIVE, userId, "I", 0, senderCountry);
            }
            catch (error) {
                throw error;
            }
        });
    }
    UpdateSender(lqnUserId, senderId, senderFirstName, senderMiddleName, senderLastName, senderGender, senderMobile, senderNationality, senderIdType, senderIdNumber, senderIdIssueCountry, senderIdIssueDate, senderIdExpireDate, senderDateOfBirth, senderOccupation, senderEmail, senderAddress, senderCity, senderState, senderZipCode, senderCountry, logon, isUpdate, userId, senderOccupationRemarks, senderIdTypeRemarks) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var countries = yield this._countryRepository.getInfoByCode(senderCountry);
                if (countries.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const countryInfo = countries[0];
                if (isUpdate) {
                    var senderLqn = yield this._lqnService.updateSender(senderId, senderGender, senderAddress, senderCity, senderState, senderZipCode, senderCountry, senderMobile, senderNationality, senderIdType, senderIdNumber, senderIdIssueCountry, senderIdIssueDate, senderIdExpireDate, senderDateOfBirth, senderOccupation, "", //senderSecondaryIdType
                    "", //senderSecondaryIdNumber
                    senderEmail, "", //senderNativeFirstname
                    "", //senderNativeMiddlename
                    "", //senderNativeLastname
                    "", //senderNativeAddress
                    senderOccupationRemarks, senderIdTypeRemarks, "", //authorizedPersonIdType
                    "", //authorizedPersonIdNumber
                    "", //authorizedPersonFirstName
                    "", //authorizedPersonLastName
                    "" //invoiceOrPayrollNumber
                    );
                    if (senderLqn.code != "0") {
                        throw new customError_1.default(senderLqn.code, senderLqn.message);
                    }
                    var sender = yield this._lqnUserRepository.update(lqnUserId, senderGender, senderNationality, senderMobile, senderIdType, senderIdTypeRemarks, senderIdNumber, senderIdIssueCountry, senderIdIssueDate, senderIdExpireDate, senderDateOfBirth, senderOccupation, senderOccupationRemarks, senderEmail, "", //nativefirstName
                    "", //nativeMiddleName
                    "", //nativeLastName
                    senderLqn.customerId, logon, countryInfo.currency_code, senderCountry);
                    return sender.lqn_user_profile_id;
                }
                else {
                    var senderLqn = yield this._lqnService.createSender("I", senderFirstName, senderMiddleName, senderLastName, senderGender, senderAddress, senderCity, senderState, senderZipCode, senderCountry, senderMobile, senderNationality, senderIdType, senderIdNumber, senderIdIssueCountry, senderIdIssueDate, senderIdExpireDate, senderDateOfBirth, senderOccupation, "", //senderSecondaryIdType
                    "", //senderSecondaryIdNumber
                    senderEmail, "", //senderNativeFirstname
                    "", //senderNativeMiddlename
                    "", //senderNativeLastname
                    "", //senderNativeAddress
                    senderOccupationRemarks, senderIdTypeRemarks, "", //authorizedPersonIdType
                    "", //authorizedPersonIdNumber
                    "", //authorizedPersonFirstName
                    "", //authorizedPersonLastName
                    "" //invoiceOrPayrollNumber
                    );
                    if (senderLqn.code != "0") {
                        throw new customError_1.default(senderLqn.code, senderLqn.message);
                    }
                    var sender = yield this._lqnUserRepository.create(enums.lqnUserType.SENDER, senderFirstName, senderMiddleName, senderLastName, senderGender, senderMobile, senderNationality, senderIdType, senderIdTypeRemarks, senderIdNumber, senderIdIssueCountry, senderIdIssueDate, senderIdExpireDate, senderDateOfBirth, senderOccupation, senderOccupationRemarks, senderEmail, "", //nativefirstName
                    "", //nativeMiddleName
                    "", //nativeLastName
                    senderLqn.customerId, logon, "", //senderRefId
                    countryInfo.currency_code, enums.status.ACTIVE, userId, "I", 0, senderCountry);
                    yield this._lqnUserRepository.updateStatus(lqnUserId, "I");
                    return sender.lqn_user_profile_id;
                }
            }
            catch (error) {
                throw error;
            }
        });
    }
    Createreceiver(receiverFirstName, receiverMiddleName, receiverLastName, receiverAddress, receiverDateOfBirth, receiverGender, receiverContactNumber, receiverState, receiverAreaTown, receiverCity, receiverZipCode, receiverCountry, receiverNationality, receiverIdType, receiverIdTypeRemarks, receiverIdNumber, receiverIdIssueDate, receiverIdExpireDate, receiverEmail, receiverAccountType, receiverOccupation, receiverOccupationRemarks, receiverDistrict, beneficiaryType, locationId, bankName, bankBranchName, bankBranchCode, bankAccountNumber, swiftCode, iban, paymentMode, payoutCurrency, customerId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var result = yield this._lqnService.createReceiver(receiverFirstName, receiverMiddleName, receiverLastName, receiverAddress, receiverDateOfBirth, receiverGender, receiverContactNumber, receiverState, receiverAreaTown, receiverCity, receiverZipCode, receiverCountry, receiverNationality, receiverIdType, receiverIdTypeRemarks, receiverIdNumber, receiverIdIssueDate, receiverIdExpireDate, receiverEmail, receiverAccountType, receiverOccupation, receiverOccupationRemarks, receiverDistrict, beneficiaryType, locationId, bankName, bankBranchName, bankBranchCode, bankAccountNumber, swiftCode, iban, paymentMode, payoutCurrency, customerId);
                if (result.code != "0") {
                    throw new customError_1.default(result.code, result.message);
                }
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getNationalityList() {
        return __awaiter(this, void 0, void 0, function* () {
            const lqnCountry = yield this._lqnService.getCatalogue("CTY", "", "", "");
            const nationalityList = yield this._nationlityRepository.getList();
            var vResult = [];
            for (let i = 0; i < lqnCountry.length; i++) {
                for (let j = 0; j < nationalityList.length; j++) {
                    var lqn = lqnCountry[i].value;
                    var nationality = nationalityList[j].code;
                    if (lqn == nationality) {
                        vResult.push(yield this.setDropdownList(nationalityList[j].code, nationalityList[j].name));
                    }
                }
            }
            return vResult;
        });
    }
    setDropdownList(value, text) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                value: value,
                text: text,
            };
        });
    }
    getCoutryList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const lqnCountry = yield this._lqnService.getCatalogue("CTY", "", "", "");
                const countryList = yield this._countryRepository.getList();
                var vResult = [];
                for (let i = 0; i < lqnCountry.length; i++) {
                    for (let j = 0; j < countryList.length; j++) {
                        var lqn = lqnCountry[i].value;
                        var country = countryList[j].code;
                        if (lqn == country) {
                            vResult.push(yield this.setDropdownList(countryList[j].code, countryList[j].name));
                        }
                    }
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getBankList(paymentMode, payoutCountry) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const bankList = yield this._lqnService.getAgentList(paymentMode, payoutCountry);
                var vResult = [];
                for (let i = 0; i < bankList.length; i++) {
                    vResult.push(yield this.setDropdownList(bankList[i].location_id, bankList[i].location_name));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    callback(agentTxnId, pinNumber, lqnStatus, message, notification_ts) {
        return __awaiter(this, void 0, void 0, function* () {
            var lqnTransaction = yield this._lqnTransactionRepository.updateCallbackTransaction(pinNumber, lqnStatus, message);
            var status = "";
            if (lqnStatus.toUpperCase() == enums.lqnTransactionStatus.PAID) {
                status = enums.transactionStatus.SUCCESS;
            }
            else if (lqnStatus.toUpperCase() == enums.lqnTransactionStatus.CANCEL ||
                lqnStatus.toUpperCase() == enums.lqnTransactionStatus.CANCEL_HOLD ||
                lqnStatus.toUpperCase() == enums.lqnTransactionStatus.BLOCK) {
                status = enums.transactionStatus.CANCELLED;
            }
            yield this._withdrawRepository.update(lqnTransaction[0].lqn_transaction_id, status);
            try {
                return "OK";
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.LqnWithdrawService = LqnWithdrawService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [lqnUserRepository_1.LqnUserRepository,
        lqnService_1.LqnService,
        nationalityRepository_1.NationalityRepository,
        countryRepository_1.CountryRepository,
        lqnTransactionRepository_1.LqnTransactionRepository,
        withdrawRepository_1.WithdrawRepository])
], LqnWithdrawService);
//# sourceMappingURL=lqnWithdrawService.js.map