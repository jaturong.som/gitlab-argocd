"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CactusWalletService = void 0;
const enums = __importStar(require("../utilities/enums"));
const customError_1 = __importDefault(require("../middlewares/customError"));
const tsyringe_1 = require("tsyringe");
const cactusWalletRepository_1 = require("../repositories/cactusWalletRepository");
const cactusWalletAddressRepository_1 = require("../repositories/cactusWalletAddressRepository");
const cactusCustodyService_1 = require("./cactusCustodyService");
const assetRepository_1 = require("../repositories/assetRepository");
let CactusWalletService = exports.CactusWalletService = class CactusWalletService {
    constructor(cactusWalletAddressRepository, cactusWalletRepository, cactusCustodyService, assetRepository) {
        this._cactusWalletAddressRepository = cactusWalletAddressRepository;
        this._cactusWalletRepository = cactusWalletRepository;
        this._cactusCustodyService = cactusCustodyService;
        this._assetRepository = assetRepository;
    }
    getWallets() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
            }
            catch (error) {
                throw error;
            }
        });
    }
    createWalletAccount(type, logOnId, companyId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._cactusWalletRepository.getInfo(type, companyId, userId);
                if (isExists.length > 0) {
                    throw new customError_1.default(enums.responseCode.Duplicate, enums.responseMessage.Duplicate);
                }
                // create new address
                var newAddressResponse = yield this._cactusCustodyService.applyNewAddress(process.env.BID_CUSTOMER, process.env.WALLET_CODE_CUSTOMER, 1, "NORMAL_ADDRESS");
                var walletAddress = newAddressResponse.data[0];
                // var walletAddress = "0x4e62098a78E7E367712987ff8A9f7413e43fd88f"
                //get address info
                var addressInfoResponse = yield this._cactusCustodyService.getAddressInfo(process.env.BID_CUSTOMER, process.env.WALLET_CODE_CUSTOMER, walletAddress);
                var addressInfo = addressInfoResponse.data;
                //get wallet info
                var walletInfoResponse = yield this._cactusCustodyService.getWalletInfo(process.env.BID_CUSTOMER, process.env.WALLET_CODE_CUSTOMER);
                var walletInfo = walletInfoResponse.data;
                //save 
                var cactusWallet = yield this._cactusWalletRepository.create(type, process.env.BID_COMPANY, walletInfo.business_name, process.env.WALLET_CODE_CUSTOMER, walletInfo.wallet_type, walletInfo.storage_type, logOnId, companyId, userId);
                //get wallet list
                var walletListResponse = yield this._cactusCustodyService.getWalletList(process.env.WALLET_CODE_CUSTOMER);
                var walletList = walletListResponse.data.list;
                //get asset company
                var assetCompanyList = yield this._assetRepository.getAssetList();
                for (let i = 0; i < assetCompanyList.length; i++) {
                    for (let j = 0; j < walletList.length; j++) {
                        if (assetCompanyList[i].code == walletList[j].coin_name) {
                            yield this._cactusWalletAddressRepository.create(cactusWallet[0].cactus_wallet_id, assetCompanyList[i].network_code, assetCompanyList[i].code, walletAddress, addressInfo.address_type, addressInfo.address_storage, 0, 0, 0, 0, 'A', logOnId);
                        }
                    }
                }
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.CactusWalletService = CactusWalletService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [cactusWalletAddressRepository_1.CactusWalletAddressRepository,
        cactusWalletRepository_1.CactusWalletRepository,
        cactusCustodyService_1.CactusCustodyService,
        assetRepository_1.AssetRepository])
], CactusWalletService);
//# sourceMappingURL=cactusWalletService.js.map