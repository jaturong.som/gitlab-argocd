"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LqnService = void 0;
const tsyringe_1 = require("tsyringe");
const crypto_js_1 = __importDefault(require("crypto-js"));
const httpService_1 = require("./httpService");
const vaultService_1 = require("./vaultService");
const customError_1 = __importDefault(require("../middlewares/customError"));
let LqnService = exports.LqnService = class LqnService {
    constructor(httpService, vaultService) {
        this._httpService = httpService;
        this._vaultService = vaultService;
    }
    getCatalogue(catalogueType, additionalField1, additionalField2, additionalField3) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const requestTimeStamp = Math.round(new Date().getTime() / 1000);
                var body = JSON.stringify({
                    agentSessionId: requestTimeStamp.toString(),
                    catalogueType: catalogueType,
                    additionalField1: additionalField1,
                    additionalField2: additionalField2,
                    additionalField3: additionalField3,
                });
                const lqnKey = yield this._vaultService.getLQN();
                var signature = yield this.generateAuthHeader(lqnKey.data.apiKey, lqnKey.data.secretKey, body, process.env.LQN_GET_CATALOGUE);
                const response = yield this._httpService.callFetch("POST", {
                    Authorization: signature,
                    "Content-type": "application/json",
                }, process.env.LQN_ENDPOINT + process.env.LQN_GET_CATALOGUE, body);
                if (response.code != "0") {
                    throw new customError_1.default(response.code, response.message);
                }
                var vResult = [];
                var catalogueList = response.result;
                for (let i = 0; i < catalogueList.length; i++) {
                    var catalogue = {
                        value: catalogueList[i].data,
                        text: catalogueList[i].value,
                    };
                    vResult.push(catalogue);
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAgentList(paymentMode, payoutCountry) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const requestTimeStamp = Math.round(new Date().getTime() / 1000);
                var body = JSON.stringify({
                    agentSessionId: requestTimeStamp.toString(),
                    paymentMode: paymentMode,
                    payoutCountry: payoutCountry,
                });
                const lqnKey = yield this._vaultService.getLQN();
                var signature = yield this.generateAuthHeader(lqnKey.data.apiKey, lqnKey.data.secretKey, body, process.env.LQN_GET_AGENT_LIST);
                const response = yield this._httpService.callFetch("POST", {
                    Authorization: signature,
                    "Content-type": "application/json",
                }, process.env.LQN_ENDPOINT + process.env.LQN_GET_AGENT_LIST, body);
                if (response.code != "0") {
                    throw new customError_1.default(response.code, response.message);
                }
                var vResult = [];
                var locationDetails = response.locationDetail;
                for (let i = 0; i < locationDetails.length; i++) {
                    var location = {
                        location_id: locationDetails[i].locationId,
                        location_name: locationDetails[i].locationName,
                        optional_field: locationDetails[i].optionalField,
                    };
                    vResult.push(location);
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getExchangeRate(transferAmount, calcBy, payoutCurrency, paymentMode, locationId, payoutCountry) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const requestTimeStamp = Math.round(new Date().getTime() / 1000);
                var body = JSON.stringify({
                    agentSessionId: requestTimeStamp.toString(),
                    transferAmount: transferAmount.toString(),
                    calcBy: calcBy,
                    payoutCurrency: payoutCurrency,
                    paymentMode: paymentMode,
                    locationId: locationId,
                    payoutCountry: payoutCountry,
                });
                const lqnKey = yield this._vaultService.getLQN();
                var signature = yield this.generateAuthHeader(lqnKey.data.apiKey, lqnKey.data.secretKey, body, process.env.LQN_GET_EXCHANGE_RATE);
                const response = yield this._httpService.callFetch("POST", {
                    Authorization: signature,
                    "Content-type": "application/json",
                }, process.env.LQN_ENDPOINT + process.env.LQN_GET_EXCHANGE_RATE, body);
                if (response.code != "0") {
                    throw new customError_1.default(response.code, response.message);
                }
                return {
                    code: response.code,
                    agent_session_id: response.agentSessionId,
                    message: response.message,
                    collect_amount: response.collectAmount,
                    collect_currency: response.collectCurrency,
                    service_charge: response.serviceCharge,
                    gst_charge: response.gstCharge,
                    transfer_amount: response.transferAmount,
                    exchange_rate: response.exchangeRate,
                    payout_amount: response.payoutAmount,
                    payout_currency: response.payoutCurrency,
                    fee_discount: response.feeDiscount,
                    additional_premium_rate: response.additionalPremiumRate,
                    settlement_rate: response.settlementRate,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    sendTransaction(senderSourceOfFund, senderSourceOfFundRemarks, purposeOfRemittance, purposeOfRemittanceRemarks, senderBeneficiaryRelationship, senderBeneficiaryRelationshipRemarks, calcBy, transferAmount, remitCurrency, customerId, receiverId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const requestTimeStamp = Math.round(new Date().getTime() / 1000);
                var body = JSON.stringify({
                    agentSessionId: requestTimeStamp.toString(),
                    AgentTxnId: requestTimeStamp.toString(),
                    senderSourceOfFund: senderSourceOfFund,
                    senderSourceOfFundRemarks: senderSourceOfFundRemarks,
                    purposeOfRemittance: purposeOfRemittance,
                    purposeOfRemittanceRemarks: purposeOfRemittanceRemarks,
                    senderBeneficiaryRelationship: senderBeneficiaryRelationship,
                    senderBeneficiaryRelationshipRemarks: senderBeneficiaryRelationshipRemarks,
                    calcBy: calcBy,
                    transferAmount: transferAmount.toString(),
                    remitCurrency: remitCurrency,
                    customerId: customerId,
                    receiverId: receiverId,
                });
                const lqnKey = yield this._vaultService.getLQN();
                var signature = yield this.generateAuthHeader(lqnKey.data.apiKey, lqnKey.data.secretKey, body, process.env.LQN_SEND_TRANSACTION);
                var response = yield this._httpService.callFetch("POST", {
                    Authorization: signature,
                    "Content-type": "application/json",
                }, process.env.LQN_ENDPOINT + process.env.LQN_SEND_TRANSACTION, body);
                if (response.code != "0") {
                    throw new customError_1.default(response.code, response.message);
                }
                return response;
            }
            catch (error) {
                throw error;
            }
        });
    }
    commitTransaction(confirmationId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const requestTimeStamp = Math.round(new Date().getTime() / 1000);
                var body = JSON.stringify({
                    agentSessionId: requestTimeStamp.toString(),
                    confirmationId: confirmationId,
                });
                const lqnKey = yield this._vaultService.getLQN();
                var signature = yield this.generateAuthHeader(lqnKey.data.apiKey, lqnKey.data.secretKey, body, process.env.LQN_COMMIT_TRANSACTION);
                var response = yield this._httpService.callFetch("POST", {
                    Authorization: signature,
                    "Content-type": "application/json",
                }, process.env.LQN_ENDPOINT + process.env.LQN_COMMIT_TRANSACTION, body);
                if (response.code != "0") {
                    throw new customError_1.default(response.code, response.message);
                }
                return response;
            }
            catch (error) {
                throw error;
            }
        });
    }
    queryTransaction(pinNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const requestTimeStamp = Math.round(new Date().getTime() / 1000);
                var body = JSON.stringify({
                    agentSessionId: requestTimeStamp.toString(),
                    pinNumber: pinNumber,
                });
                const lqnKey = yield this._vaultService.getLQN();
                var signature = yield this.generateAuthHeader(lqnKey.data.apiKey, lqnKey.data.secretKey, body, process.env.LQN_QUERY_TRANSACTION);
                var response = yield this._httpService.callFetch("POST", {
                    Authorization: signature,
                    "Content-type": "application/json",
                }, process.env.LQN_ENDPOINT + process.env.LQN_QUERY_TRANSACTION, body);
                if (response.code != "0") {
                    throw new customError_1.default(response.code, response.message);
                }
                return response;
            }
            catch (error) {
                throw error;
            }
        });
    }
    createSender(senderType, senderFirstName, senderMiddleName, senderLastName, senderGender, senderAddress, senderCity, senderState, senderZipCode, senderCountry, senderMobile, senderNationality, senderIdType, senderIdNumber, senderIdIssueCountry, senderIdIssueDate, senderIdExpireDate, senderDateOfBirth, senderOccupation, senderSecondaryIdType, senderSecondaryIdNumber, senderEmail, senderNativeFirstname, senderNativeMiddlename, senderNativeLastname, senderNativeAddress, senderOccupationRemarks, senderIdTypeRemarks, authorizedPersonIdType, authorizedPersonIdNumber, authorizedPersonFirstName, authorizedPersonLastName, invoiceOrPayrollNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const requestTimeStamp = Math.round(new Date().getTime() / 1000);
                var body = JSON.stringify({
                    agentSessionId: requestTimeStamp.toString(),
                    senderType: senderType,
                    senderFirstName: senderFirstName,
                    senderMiddleName: senderMiddleName,
                    senderLastName: senderLastName,
                    senderGender: senderGender,
                    senderAddress: senderAddress,
                    senderCity: senderCity,
                    senderState: senderState,
                    senderZipCode: senderZipCode,
                    senderCountry: senderCountry,
                    senderMobile: senderMobile,
                    senderNationality: senderNationality,
                    senderIdType: senderIdType,
                    senderIdNumber: senderIdNumber,
                    senderIdIssueCountry: senderIdIssueCountry,
                    senderIdIssueDate: senderIdIssueDate,
                    senderIdExpireDate: senderIdExpireDate,
                    senderDateOfBirth: senderDateOfBirth,
                    senderOccupation: senderOccupation,
                    senderSecondaryIdType: senderSecondaryIdType,
                    senderSecondaryIdNumber: senderSecondaryIdNumber,
                    senderEmail: senderEmail,
                    senderNativeFirstname: senderNativeFirstname,
                    senderNativeMiddlename: senderNativeMiddlename,
                    senderNativeLastname: senderNativeLastname,
                    senderNativeAddress: senderNativeAddress,
                    senderOccupationRemarks: senderOccupationRemarks,
                    senderIdTypeRemarks: senderIdTypeRemarks,
                    authorizedPersonIdType: authorizedPersonIdType,
                    authorizedPersonIdNumber: authorizedPersonIdNumber,
                    authorizedPersonFirstName: authorizedPersonFirstName,
                    authorizedPersonLastName: authorizedPersonLastName,
                    invoiceOrPayrollNumber: invoiceOrPayrollNumber,
                });
                const lqnKey = yield this._vaultService.getLQN();
                var signature = yield this.generateAuthHeader(lqnKey.data.apiKey, lqnKey.data.secretKey, body, process.env.LQN_CREATE_SENDER);
                var response = yield this._httpService.callFetch("POST", {
                    Authorization: signature,
                    "Content-type": "application/json",
                }, process.env.LQN_ENDPOINT + process.env.LQN_CREATE_SENDER, body);
                if (response.code != "0") {
                    throw new customError_1.default(response.code, response.message);
                }
                return response;
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateSender(senderId, senderGender, senderAddress, senderCity, senderState, senderZipCode, senderCountry, senderMobile, senderNationality, senderIdType, senderIdNumber, senderIdIssueCountry, senderIdIssueDate, senderIdExpireDate, senderDateOfBirth, senderOccupation, senderSecondaryIdType, senderSecondaryIdNumber, senderEmail, senderNativeFirstname, senderNativeMiddlename, senderNativeLastname, senderNativeAddress, senderOccupationRemarks, senderIdTypeRemarks, authorizedPersonIdType, authorizedPersonIdNumber, authorizedPersonFirstName, authorizedPersonLastName, invoiceOrPayrollNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const requestTimeStamp = Math.round(new Date().getTime() / 1000);
                var body = JSON.stringify({
                    agentSessionId: requestTimeStamp.toString(),
                    customerId: senderId,
                    senderGender: senderGender,
                    senderAddress: senderAddress,
                    senderCity: senderCity,
                    senderState: senderState,
                    senderZipCode: senderZipCode,
                    senderCountry: senderCountry,
                    senderMobile: senderMobile,
                    senderNationality: senderNationality,
                    senderIdType: senderIdType,
                    senderIdNumber: senderIdNumber,
                    senderIdIssueCountry: senderIdIssueCountry,
                    senderIdIssueDate: senderIdIssueDate,
                    senderIdExpireDate: senderIdExpireDate,
                    senderDateOfBirth: senderDateOfBirth,
                    senderOccupation: senderOccupation,
                    senderSecondaryIdType: senderSecondaryIdType,
                    senderSecondaryIdNumber: senderSecondaryIdNumber,
                    senderEmail: senderEmail,
                    senderNativeFirstname: senderNativeFirstname,
                    senderNativeMiddlename: senderNativeMiddlename,
                    senderNativeLastname: senderNativeLastname,
                    senderNativeAddress: senderNativeAddress,
                    senderOccupationRemarks: senderOccupationRemarks,
                    senderIdTypeRemarks: senderIdTypeRemarks,
                    authorizedPersonIdType: authorizedPersonIdType,
                    authorizedPersonIdNumber: authorizedPersonIdNumber,
                    authorizedPersonFirstName: authorizedPersonFirstName,
                    authorizedPersonLastName: authorizedPersonLastName,
                    invoiceOrPayrollNumber: invoiceOrPayrollNumber,
                });
                const lqnKey = yield this._vaultService.getLQN();
                var signature = yield this.generateAuthHeader(lqnKey.data.apiKey, lqnKey.data.secretKey, body, process.env.LQN_UPDATE_SENDER);
                var response = yield this._httpService.callFetch("POST", {
                    Authorization: signature,
                    "Content-type": "application/json",
                }, process.env.LQN_ENDPOINT + process.env.LQN_UPDATE_SENDER, body);
                if (response.code != "0") {
                    throw new customError_1.default(response.code, response.message);
                }
                return response;
            }
            catch (error) {
                throw error;
            }
        });
    }
    createReceiver(receiverFirstName, receiverMiddleName, receiverLastName, receiverAddress, receiverDateOfBirth, receiverGender, receiverContactNumber, receiverState, receiverAreaTown, receiverCity, receiverZipCode, receiverCountry, receiverNationality, receiverIdType, receiverIdTypeRemarks, receiverIdNumber, receiverIdIssueDate, receiverIdExpireDate, receiverEmail, receiverAccountType, receiverOccupation, receiverOccupationRemarks, receiverDistrict, beneficiaryType, locationId, bankName, bankBranchName, bankBranchCode, bankAccountNumber, swiftCode, iban, paymentMode, payoutCurrency, customerId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const requestTimeStamp = Math.round(new Date().getTime() / 1000);
                var body = JSON.stringify({
                    agentSessionId: requestTimeStamp.toString(),
                    receiverFirstName: receiverFirstName,
                    receiverMiddleName: receiverMiddleName,
                    receiverLastName: receiverLastName,
                    receiverAddress: receiverAddress,
                    receiverDateOfBirth: receiverDateOfBirth,
                    receiverGender: receiverGender,
                    receiverContactNumber: receiverContactNumber,
                    receiverState: receiverState,
                    receiverAreaTown: receiverAreaTown,
                    receiverCity: receiverCity,
                    receiverZipCode: receiverZipCode,
                    receiverCountry: receiverCountry,
                    receiverNationality: receiverNationality,
                    receiverIdType: receiverIdType,
                    receiverIdTypeRemarks: receiverIdTypeRemarks,
                    receiverIdNumber: receiverIdNumber,
                    receiverIdIssueDate: receiverIdIssueDate,
                    receiverIdExpireDate: receiverIdExpireDate,
                    receiverEmail: receiverEmail,
                    receiverNativeFirstName: receiverFirstName,
                    receiverNativeMiddleName: receiverMiddleName,
                    receiverNativeLastName: receiverLastName,
                    receiverAccountType: receiverAccountType,
                    receiverNativeAddress: receiverAddress,
                    receiverOccupation: receiverOccupation,
                    receiverOccupationRemarks: receiverOccupationRemarks,
                    receiverDistrict: receiverDistrict,
                    beneficiaryType: beneficiaryType,
                    locationId: locationId,
                    bankName: bankName,
                    bankBranchName: bankBranchName,
                    bankBranchCode: bankBranchCode,
                    bankAccountNumber: bankAccountNumber,
                    swiftCode: swiftCode,
                    iban: iban,
                    paymentMode: paymentMode,
                    payoutCurrency: payoutCurrency,
                    customerId: customerId,
                });
                const lqnKey = yield this._vaultService.getLQN();
                var signature = yield this.generateAuthHeader(lqnKey.data.apiKey, lqnKey.data.secretKey, body, process.env.LQN_CREATE_RECEIVER);
                var response = yield this._httpService.callFetch("POST", {
                    Authorization: signature,
                    "Content-type": "application/json",
                }, process.env.LQN_ENDPOINT + process.env.LQN_CREATE_RECEIVER, body);
                if (response.code != "0") {
                    throw new customError_1.default(response.code, response.message);
                }
                return response;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getFieldInfo(pLocationId, pPayoutCountry, pPayoutCurrency, pPaymentMode, pTransferAmount) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var body = JSON.stringify({
                    agentSessionId: Math.round(new Date().getTime() / 1000),
                    locationId: pLocationId,
                    payoutCountry: pPayoutCountry,
                    payoutCurrency: pPayoutCurrency,
                    paymentMode: pPaymentMode,
                    transferAmount: pTransferAmount,
                });
                const lqnKey = yield this._vaultService.getLQN();
                var signature = yield this.generateAuthHeader(lqnKey.data.apiKey, lqnKey.data.secretKey, body, "/GetFieldInfo");
                var response = yield this._httpService.callFetch("POST", {
                    Authorization: signature,
                    "Content-type": "application/json",
                }, process.env.LQN_ENDPOINT + "/GetFieldInfo", body);
                if (response.code != "0") {
                    throw new customError_1.default(response.code, response.message);
                }
                return response;
            }
            catch (error) {
                throw error;
            }
        });
    }
    checkBalance() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const requestTimeStamp = Math.round(new Date().getTime() / 1000);
                var body = JSON.stringify({
                    agentSessionId: requestTimeStamp.toString(),
                });
                const lqnKey = yield this._vaultService.getLQN();
                var signature = yield this.generateAuthHeader(lqnKey.data.apiKey, lqnKey.data.secretKey, body, "/CheckBalance");
                var response = yield this._httpService.callFetch("POST", {
                    Authorization: signature,
                    "Content-type": "application/json",
                }, process.env.LQN_ENDPOINT + "/CheckBalance", body);
                if (response.code != "0") {
                    throw new customError_1.default(response.code, response.message);
                }
                return response;
            }
            catch (error) {
                throw error;
            }
        });
    }
    generateAuthHeader(apiKey, secretKey, body, path) {
        return __awaiter(this, void 0, void 0, function* () {
            const requestTimeStamp = Math.round(new Date().getTime() / 1000);
            var nonce = requestTimeStamp;
            var appId = apiKey;
            var requestUrl = encodeURIComponent(process.env.LQN_ENDPOINT + path).toLowerCase();
            var requestContent = crypto_js_1.default.MD5(body);
            var requestContentBase64String = requestContent.toString(crypto_js_1.default.enc.Base64);
            var dataToSign = appId +
                "POST" +
                requestUrl +
                requestTimeStamp +
                nonce +
                requestContentBase64String;
            let apiSecretHash = Buffer.from(secretKey, "base64");
            let apiSecret = apiSecretHash.toString("ascii");
            var hash = crypto_js_1.default.HmacSHA256(dataToSign, apiSecret);
            var hmac = hash.toString(crypto_js_1.default.enc.Base64);
            var signature = "hmacauth " + appId + ":" + hmac + ":" + nonce + ":" + requestTimeStamp;
            return signature;
        });
    }
    test() {
        return __awaiter(this, void 0, void 0, function* () {
            // *** STEP 1: Choose country ***
            // CTY: Get Available Country
            // const catalogueInfoCTY = await this.getCatalogue("CTY", "", "", "");
            // console.log(catalogueInfoCTY);
            // [
            //   { value: 'CHN', text: 'CHN' },
            //   { value: 'SGP', text: 'SGP' },
            //   { value: 'THA', text: 'THA' }
            // ]
            // *** STEP 2: Choose payment mode by country ***
            // PTY: Get Available Payment Mode
            // const catalogueInfoPTY = await this.getCatalogue("PTY", "THA", "", "");
            // console.log(catalogueInfoPTY);
            // [ { value: 'B', text: 'Bank Transfer' } ]
            // *** STEP 3: Get agents by country and payment mode ***
            // const agentList = await this.getAgentList(
            //   "B", // Payment Mode
            //   "THA" // Country
            // );
            // console.log(agentList);
            // [
            //   {
            //     location_id: 'THABAN3710',
            //     location_name: 'BANK OF AYUDHYA PUBLIC COMPANY LTD.',
            //     optional_field: ''
            //   },
            //   {
            //     location_id: 'THACIM3708',
            //     location_name: 'CIMB THAI BANK PUBLIC COMPANY LIMITED (FORMERLY BANKTHAI PUBLIC COMPANY LIMITED)',
            //     optional_field: ''
            //   },
            //   {
            //     location_id: 'THACIT',
            //     location_name: 'CITIBANK N.A.',
            //     optional_field: ''
            //   },
            //   {
            //     location_id: 'THAKAS3699',
            //     location_name: 'KASIKORNBANK PUBLIC COMPANY LIMITED',
            //     optional_field: ''
            //   },
            //   {
            //     location_id: 'THAKRU3701',
            //     location_name: 'KRUNG THAI BANK PUBLIC COMPANY LTD.',
            //     optional_field: ''
            //   },
            //   {
            //     location_id: 'THASIA3704',
            //     location_name: 'SIAM COMMERCIAL BANK PUBLIC COMPANY LTD.',
            //     optional_field: ''
            //   },
            //   {
            //     location_id: 'THATES',
            //     location_name: 'TEST BANK AASHISH',
            //     optional_field: ''
            //   },
            //   {
            //     location_id: 'THATMB1',
            //     location_name: 'TMB BANK PUBLIC COMPANY LIMITED(TTB)',
            //     optional_field: ''
            //   }
            // ]
            // *** STEP 4: Get require fields ***
            // const fieldInfo = await this.getFieldInfo(
            //   "THASIA3704", // location_id
            //   "THA", // Country
            //   "THB", // Currency
            //   "B", // Payment Mode
            //   "100" // Amount
            // );
            // console.log(fieldInfo);
            // fieldList: [
            //   {
            //     fieldName: 'BankAccountNumber',
            //     fieldLabel: 'Receiver Account Number',
            //     required: true,
            //     dynamicField: false,
            //     minLength: 1,
            //     maxLength: 128
            //   },
            //   {
            //     fieldName: 'PaymentMode',
            //     fieldLabel: 'Payment Mode',
            //     required: true,
            //     dynamicField: false,
            //     minLength: 0,
            //     maxLength: 50
            //   },
            //   {
            //     fieldName: 'PurposeOfRemittance',
            //     fieldLabel: 'Purpose Of Remittance',
            //     required: true,
            //     dynamicField: false,
            //     minLength: 2,
            //     maxLength: 2
            //   },
            //   {
            //     fieldName: 'ReceiverCountry',
            //     fieldLabel: 'Receiver Country',
            //     required: true,
            //     dynamicField: false,
            //     minLength: 3,
            //     maxLength: 3
            //   }
            // ]
            // *** STEP 5 Create sender or get existing ***
            // const catalogueInfoSTA = await this.getCatalogue("STA", "THA", "", "");
            // console.log(catalogueInfoSTA);
            // [ { value: 'Bangkok', text: 'Bangkok' } ]
            // const catalogueInfoDOC = await this.getCatalogue("DOC", "THA", "", "");
            // console.log(catalogueInfoDOC);
            // [
            //   { value: '05', text: 'Company Registration' },
            //   { value: '03', text: 'Driver License' },
            //   { value: '02', text: 'ID card (government issued)' },
            //   { value: '99', text: 'Other' },
            //   { value: '01', text: 'Passport' },
            //   { value: '04', text: 'Residence' },
            //   { value: '06', text: 'Work Permit' }
            // ]
            // const catalogueInfoOCC = await this.getCatalogue("OCC", "THA", "", "");
            // console.log(catalogueInfoOCC);
            // [
            //   { value: '07', text: 'Agriculture forestry fisheries' },
            //   { value: '08', text: 'Construction, manufacturing, marine' },
            //   {
            //     value: '03',
            //     text: 'Government officials and Special Interest Organizations'
            //   },
            //   { value: '99', text: 'Other' },
            //   { value: '23', text: 'Professional, and related workers' },
            //   { value: '15', text: 'Retired' },
            //   { value: '05', text: 'Self-employed' },
            //   { value: '13', text: 'Student' },
            //   { value: '14', text: 'Unemployed' }
            // ]
            // const senderInfo = await this.createSender(
            //   "I", //I – Individual, B – Business
            //   "Sender first name 01",
            //   "",
            //   "Sender last name 01",
            //   "Male", // Male, Female
            //   "11/22",
            //   "Bangkok",
            //   "Bangkok", // CATALOGUETYPE:STA
            //   "12345",
            //   "THA", // CATALOGUETYPE:CTY
            //   "0899999999",
            //   "THA", // CATALOGUETYPE:CTY
            //   "01", // CATALOGUETYPE: DOC
            //   "1234567890",
            //   "THA", // CATALOGUETYPE:CTY
            //   "2017-04-17", // ID Issue Date format: yyyy-mm-dd
            //   "2027-08-24", // Expire Date format: yyyy-mm-dd
            //   "1979-08-06", // Date of birth Date format: yyyy-mm-dd
            //   "23", // CATALOGUETYPE: OCC
            //   "",
            //   "",
            //   "test01@test.com",
            //   "",
            //   "",
            //   "",
            //   "",
            //   "",
            //   "",
            //   "",
            //   "",
            //   "",
            //   "",
            //   ""
            // );
            // console.log(senderInfo);
            // {
            //   code: '0',
            //   message: 'Customer Created Successfully',
            //   agentSessionId: '1690349732',
            //   customerId: 'xv9LiPHgsl/LZOfC2CStcw=='
            // }
            // *** STEP 6 Create receiver ***
            // const receiverInfo = await this.createReceiver(
            //   // "I", //I – Individual, B – Business
            //   "Receiver first name 01",
            //   "",
            //   "Receiver last name 01",
            //   "11/22",
            //   "Shang Hai", // CATALOGUETYPE:STA
            //   "Shang Hai", // CATALOGUETYPE:ART
            //   "Shang Hai",
            //   "321",
            //   "1976-04-16", // Date of birth Date format: yyyy-mm-dd
            //   "CHN", // CATALOGUETYPE:CTY
            //   "CHN", // Nationality, CATALOGUETYPE:CTY
            //   "01", // CATALOGUETYPE: DOC
            //   "",
            //   "9876543210",
            //   "receiver01@test.com",
            //   "0999999999",
            //   "B",
            //   "bank name 01",
            //   "branch name 01",
            //   "branch code 01",
            //   "account number 01",
            //   "23", // CATALOGUETYPE: OCC
            //   "",
            //   "2018-05-18", // ID Issue Date format: yyyy-mm-dd
            //   "2028-09-25", // Expire Date format: yyyy-mm-dd
            //   "CHNAgr531", // LocationId, AgentList
            //   "CNY",
            //   "xv9LiPHgsl/LZOfC2CStcw==" //customerid
            //   // "Female" // Male, Female
            // );
            // console.log(receiverInfo);
            // {
            //   code: '0',
            //   message: 'Receiver Created Successfully',
            //   agentSessionId: '1690362154',
            //   receiverId: 'z6fT3IN8rgDKNtZ40NDjjA=='
            // }
            // *** STEP 7 Send a transaction ***
            // SOF: Sender Source of Fund
            // const catalogueInfoSOF = await this.getCatalogue("SOF", "", "", "");
            // console.log(catalogueInfoSOF);
            // [
            //   { value: '02', text: 'Business and investment' },
            //   {
            //     value: '05',
            //     text: 'Cryptocurrency or other Digital Payment Tokens'
            //   },
            //   { value: '06', text: 'Donation' },
            //   { value: '04', text: 'Friends and family' },
            //   { value: '99', text: 'Other' },
            //   {
            //     value: '01',
            //     text: 'Salary to include any work related compensation and pensions'
            //   }
            // ]
            // POR: Purpose of Remittance
            // const catalogueInfoPOR = await this.getCatalogue("POR", "", "", "");
            // console.log(catalogueInfoPOR);
            // [
            //   {
            //     value: '16',
            //     text: 'Business Expenses (including wages, salaries etc)'
            //   },
            //   {
            //     value: '12',
            //     text: 'Construction, purchase of land and mortgage repayments'
            //   },
            //   { value: '05', text: 'Donation' },
            //   { value: '03', text: 'Education' },
            //   {
            //     value: '02',
            //     text: 'Family Expenses (including medical expenses, vehicle purchases etc)'
            //   },
            //   { value: '99', text: 'Other' },
            //   { value: '06', text: 'Payment for products and services' },
            //   { value: '10', text: 'Self' }
            // ]
            // REL: Relationship between sender and beneficiary
            // const catalogueInfoREL = await this.getCatalogue("REL", "", "", "");
            // console.log(catalogueInfoREL);
            // [
            //   { value: '04', text: 'EMPLOYEE' },
            //   { value: '01', text: 'FAMILY' },
            //   { value: '02', text: 'FRIENDS' },
            //   { value: '99', text: 'Other' },
            //   { value: '03', text: 'SELF' },
            //   { value: '09', text: 'Seller/Service Provider' }
            // ]
            // const sendResult = await this.sendTransaction(
            //   "02", // Sender Source of Fund,
            //   "", // Sender Source of Fund if senderSourceOfFund inputted as Others
            //   "09", // Relationship between sender and beneficiary
            //   "", // Relationship between sender and beneficiary if senderBeneficiaryRelationship inputted as Others
            //   "06", // Purpose of Remittance
            //   "", // Purpose of Remittance if purposeOfRemittance inputted as Others
            //   "P", // calcBy option ‘C’ removed in sendTransaction API. Use only calcBy as ‘P’
            //   "100", // Amount
            //   "USD", // Currency (FIXED!?)
            //   "xv9LiPHgsl/LZOfC2CStcw==", // Sender (My new account in Thailand)
            //   "z6fT3IN8rgDKNtZ40NDjjA==" // Receiver (New account in China)
            // );
            // console.log(sendResult);
            // {
            //   code: '0',
            //   agentSessionId: '1690355597',
            //   message: 'Transaction Accepted',
            //   confirmationId: '152077',
            //   agentTxnId: '1690355597',
            //   collectAmount: '3.73',
            //   collectCurrency: 'USD',
            //   serviceCharge: '1.00',
            //   gstCharge: '',
            //   transferAmount: '2.73',
            //   exchangeRate: '36.5847',
            //   payoutAmount: '100.00',
            //   payoutCurrency: 'THB',
            //   feeDiscount: '0',
            //   additionalPremiumRate: '0',
            //   txnDate: '2023-07-26 15:08:29',
            //   settlementRate: '36.5847',
            //   sendCommission: '0.00',
            //   settlementAmount: '3.73'
            // }
            // *** STEP 8 Commit a transaction ***
            // const commitResult = await this.commitTransaction("152077");
            // console.log(commitResult);
            // {
            //   code: '0',
            //   agentSessionId: '1690356467',
            //   message: 'TXN Successfully Authorized',
            //   pinNumber: 'LN23434508',
            //   senderName: 'TEST FIRST NAME 01 TEST LAST NAME 01',
            //   collectAmount: '3.73',
            //   collectCurrency: 'USD',
            //   payoutAmount: '100.00',
            //   payoutCurrency: 'THB',
            //   status: 'HOLD',
            //   isPinOnHold: 'False'
            // }
            // *** STEP 9 check state of transactions ***
            // const queryResult = await this.queryTransaction("LN23434508");
            // console.log(queryResult);
            // {
            //   code: '0',
            //   agentSessionId: '1690356737',
            //   message: 'Transaction is Processing',
            //   pinNumber: 'LN23434508',
            //   senderName: 'TEST FIRST NAME 01 TEST LAST NAME 01',
            //   receiverName: 'SEA-99 SENDER',
            //   collectAmount: '3.73',
            //   collectCurrency: 'USD',
            //   serviceCharge: '1.00',
            //   gstCharge: '',
            //   transferAmount: '2.73',
            //   exchangeRate: '36.5847',
            //   payoutAmount: '100.00',
            //   payoutCurrency: 'THB',
            //   settlementAmount: '3.73',
            //   sendCommission: '0.00',
            //   settlementRate: '36.5847',
            //   status: 'API PROCESSING',
            //   statusDate: '',
            //   additionalMessage: 'Transaction is Processing',
            //   agentTxnId: '1690355597'
            // }
            // const balanceResult = await this.checkBalance();
            // console.log(balanceResult);
            // {
            //   code: '0',
            //   agentSessionId: '1690357312',
            //   currentBalance: '1674.0800',
            //   availableLimitBalance: '998325.9200',
            //   currency: 'USD'
            // }
            return "Pass";
        });
    }
};
exports.LqnService = LqnService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [httpService_1.HttpService, vaultService_1.VaultService])
], LqnService);
//# sourceMappingURL=lqnService.js.map