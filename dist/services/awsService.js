"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AWSService = void 0;
const client_s3_1 = require("@aws-sdk/client-s3");
const s3_request_presigner_1 = require("@aws-sdk/s3-request-presigner");
const mime_types_1 = __importDefault(require("mime-types"));
const vaultService_1 = require("./vaultService");
const tsyringe_1 = require("tsyringe");
let AWSService = exports.AWSService = class AWSService {
    constructor(vaultService) {
        this._vaultService = vaultService;
    }
    upload(bucketName, fileName, fileContent) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const awsKey = yield this._vaultService.getAWS();
                const s3Client = new client_s3_1.S3Client({
                    region: process.env.S3_REGION,
                    credentials: {
                        accessKeyId: awsKey.data.awsS3AccessKeyId,
                        secretAccessKey: awsKey.data.awsS3SecretAccessKey,
                    },
                });
                yield s3Client.send(new client_s3_1.PutObjectCommand({
                    Bucket: bucketName,
                    Key: fileName,
                    Body: fileContent,
                }));
                const encodeFileName = encodeURIComponent(fileName);
                return `https://${bucketName}.s3.amazonaws.com/${encodeFileName}`;
            }
            catch (error) {
                throw error;
            }
        });
    }
    download(bucketName, fileName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const awsKey = yield this._vaultService.getAWS();
                const s3Client = new client_s3_1.S3Client({
                    region: process.env.S3_REGION,
                    credentials: {
                        accessKeyId: awsKey.data.awsS3AccessKeyId,
                        secretAccessKey: awsKey.data.awsS3SecretAccessKey,
                    },
                });
                return yield (0, s3_request_presigner_1.getSignedUrl)(s3Client, new client_s3_1.GetObjectCommand({
                    Bucket: bucketName,
                    Key: fileName,
                }), { expiresIn: 60 });
            }
            catch (error) {
                throw error;
            }
        });
    }
    preview(bucketName, fileName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const awsKey = yield this._vaultService.getAWS();
                const s3Client = new client_s3_1.S3Client({
                    region: process.env.S3_REGION,
                    credentials: {
                        accessKeyId: awsKey.data.awsS3AccessKeyId,
                        secretAccessKey: awsKey.data.awsS3SecretAccessKey,
                    },
                });
                return yield (0, s3_request_presigner_1.getSignedUrl)(s3Client, new client_s3_1.GetObjectCommand({
                    Bucket: bucketName,
                    Key: fileName,
                    ResponseContentDisposition: "inline",
                    ResponseContentType: mime_types_1.default.lookup(fileName).toString(),
                }), { expiresIn: 60 });
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.AWSService = AWSService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [vaultService_1.VaultService])
], AWSService);
//# sourceMappingURL=awsService.js.map