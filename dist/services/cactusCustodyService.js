"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CactusCustodyService = void 0;
// import CryptoJS from "crypto-js";
const crypto = __importStar(require("crypto"));
const vaultService_1 = require("./vaultService");
const httpService_1 = require("./httpService");
const tsyringe_1 = require("tsyringe");
let CactusCustodyService = exports.CactusCustodyService = class CactusCustodyService {
    constructor(vaultService, httpService) {
        this._vaultService = vaultService;
        this._httpService = httpService;
    }
    getWalletList(walletCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlWalletList = "/custody/v1/api/wallets";
                var vQueryParam = [];
                if (walletCode !== undefined) {
                    vQueryParam.push({
                        main_wallet_code: walletCode,
                    });
                }
                var queryParam = vQueryParam;
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlWalletList, queryParam, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWalletInfo(businessLineId, walletCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlWalletInfo = "/custody/v1/api/projects/" + businessLineId + "/wallets/" + walletCode;
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlWalletInfo, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCoinInfo() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlCoinInfo = "/custody/v1/api/coin-infos";
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlCoinInfo, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getChainInfo() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlChainInfo = "/custody/v1/api/chain-infos";
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlChainInfo, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    createWallet(businessLineId, walletType, number) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlCreateWallet = "/custody/v1/api/projects/" + businessLineId + "/wallets/create";
                const body = JSON.stringify({
                    wallet_type: walletType,
                    number: number,
                });
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "POST", urlCreateWallet, undefined, body);
            }
            catch (error) {
                throw error;
            }
        });
    }
    applyNewAddress(businessLineId, walletCode, addressNum, addressType, coinName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlApplyNewAddress = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/wallets/" +
                    walletCode +
                    "/addresses/apply";
                const body = JSON.stringify({
                    address_num: addressNum,
                    address_type: addressType,
                    coin_name: coinName,
                });
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "POST", urlApplyNewAddress, undefined, body);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAddressInfo(businessLineId, walletCode, address) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlAddressInfo = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/wallets/" +
                    walletCode +
                    "/addresses/" +
                    address;
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlAddressInfo, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAddressList(businessLineId, walletCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlAddressList = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/wallets/" +
                    walletCode +
                    "/addresses";
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlAddressList, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    editAddress(businessLineId, walletCode, address, description) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlEditAddress = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/wallets/" +
                    walletCode +
                    "/addresses/" +
                    address;
                const body = JSON.stringify({
                    description: description,
                });
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "PUT", urlEditAddress, undefined, body);
            }
            catch (error) {
                throw error;
            }
        });
    }
    verifyAddressFormat(addresses, coinName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlVerifyAddressFormat = "/custody/v1/api/addresses/type/check";
                const body = JSON.stringify({
                    addresses: addresses,
                    coin_name: coinName,
                });
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "POST", urlVerifyAddressFormat, undefined, body);
            }
            catch (error) {
                throw error;
            }
        });
    }
    estimateGasFee(businessLineId, fromAddress, fromWalletCode, description, amount, destAddress, memoType, memo, remark) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlEstimateGasFee = "/custody/v1/api/projects/" + businessLineId + "/estimate-miner-fee";
                const body = JSON.stringify({
                    from_address: fromAddress,
                    from_wallet_code: fromWalletCode,
                    description: description,
                    dest_address_item_list: [
                        {
                            amount: amount,
                            dest_address: destAddress,
                            memo_type: memoType,
                            memo: memo,
                            is_all_withdrawal: false,
                            remark: remark,
                        },
                    ],
                });
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "POST", urlEstimateGasFee, undefined, body);
            }
            catch (error) {
                throw error;
            }
        });
    }
    createWithdrawalOrder(businessLineId, fromAddress, fromWalletCode, coinName, description, amount, destAddress, memoType, memo, remark) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlCreateWithdrawalOrder = "/custody/v1/api/projects/" + businessLineId + "/order/create";
                const body = JSON.stringify({
                    from_address: fromAddress,
                    from_wallet_code: fromWalletCode,
                    coin_name: coinName,
                    description: description,
                    dest_address_item_list: [
                        {
                            amount: amount,
                            dest_address: destAddress,
                            memo_type: memoType,
                            memo: memo,
                            is_all_withdrawal: false,
                            remark: remark,
                        },
                    ],
                });
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "POST", urlCreateWithdrawalOrder, undefined, body);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWithdrawalRateRange() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlWithdrawalRateRange = "/custody/v1/api/customize-fee-rate/range";
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlWithdrawalRateRange, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWithdrawalRate() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlWithdrawalRate = "/custody/v1/api/recommend-fee-rate/list";
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlWithdrawalRate, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWalletTransactionSummary(businessLineId, walletCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlWalletTransactionSummary = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/wallets/" +
                    walletCode +
                    "/tx-summaries";
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlWalletTransactionSummary, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionDetailsDeFi(businessLineId, walletCode, orderNo) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlTransactionDetails = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/wallets/" +
                    walletCode +
                    "/contract/orders/" +
                    orderNo;
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlTransactionDetails, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    editTransactionDetailRemark(businessLineId, walletCode, id, remark) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlEditTransactionDetailRemark = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/wallets/" +
                    walletCode +
                    "/details/" +
                    id;
                const body = JSON.stringify({
                    remark: remark,
                });
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "POST", urlEditTransactionDetailRemark, undefined, body);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionDetailsForCustomCurrency(businessLineId, walletCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlTransactionDetailsForCustomCurrency = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/wallets/" +
                    walletCode +
                    "/customize-flow";
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlTransactionDetailsForCustomCurrency, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTotalAssetHistoryNotionalValue() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlTotalAssetHistoryNotionalValue = "/custody/v1/api/history-asset";
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlTotalAssetHistoryNotionalValue, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCurrentAssetNotionalValue() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlCurrentAssetNotionalValue = "/custody/v1/api/asset";
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlCurrentAssetNotionalValue, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getOrdersFilterByCriteria(businessLineId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlOrdersFilterByCriteria = "/custody/v1/api/projects/" + businessLineId + "/orders";
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlOrdersFilterByCriteria, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getOrderDetails(businessLineId, orderNo) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlOrderDetails = "/custody/v1/api/projects/" + businessLineId + "/orders/" + orderNo;
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlOrderDetails, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    rbf(businessLineId, orderNo, level) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlRBF = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/orders/" +
                    orderNo +
                    "/accelerate";
                const body = JSON.stringify({
                    level: level,
                });
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "POST", urlRBF, undefined, body);
            }
            catch (error) {
                throw error;
            }
        });
    }
    cancelOrder(businessLineId, orderNo, level) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlCancelOrder = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/orders/" +
                    orderNo +
                    "/cancel";
                const body = JSON.stringify({
                    level: level,
                });
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "POST", urlCancelOrder, undefined, body);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getOrderTXForCallback(orderNo, level) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlOrderTXForCallback = "/[Client Url]";
                const body = JSON.stringify({
                // domain_id: domain_id,
                // b_id: b_id,
                // wallet_code: wallet_code,
                // order_id: order_id,
                // confirmations: confirmations,
                // event_status: event_status,
                // event_type: event_type,
                // detail_id: detail_id,
                });
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "POST", urlOrderTXForCallback, undefined, body);
            }
            catch (error) {
                throw error;
            }
        });
    }
    createContractOrder(businessLineId, orderNo, fromWalletCode, fromAddress, toAddress, amount, contractData, gasPriceLevel, gasPrice, gasLimit, description) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlContractOrder = "/custody/v1/api/projects/" + businessLineId + "/contract/call";
                const body = JSON.stringify({
                    order_no: orderNo,
                    from_wallet_code: fromWalletCode,
                    from_address: fromAddress,
                    to_address: toAddress,
                    amount: amount,
                    contract_data: contractData,
                    "gas_price_level ": gasPriceLevel,
                    gas_price: gasPrice,
                    gas_limit: gasLimit,
                    description: description,
                });
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "POST", urlContractOrder, undefined, body);
            }
            catch (error) {
                throw error;
            }
        });
    }
    signContractOrder(businessLineId, walletCode, address, signatureVersion, payloadMessage, chain, orderNo, description) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlSignContractOrder = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/wallets/" +
                    walletCode +
                    "/signatures";
                const body = JSON.stringify({
                    address: address,
                    signature_version: signatureVersion,
                    payload: { message: payloadMessage },
                    chain: chain,
                    order_no: orderNo,
                    description: description,
                });
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "POST", urlSignContractOrder, undefined, body);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getConnectedWalletTransactionHistory(businessLineId, walletCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlConnectedWalletTransactionHistory = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/wallets/" +
                    walletCode +
                    "/contract/orders";
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlConnectedWalletTransactionHistory, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionDetails(businessLineId, walletCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const cactusKey = yield this._vaultService.getCactusCustody();
                const urlTransactionDetails = "/custody/v1/api/projects/" +
                    businessLineId +
                    "/wallets/" +
                    walletCode +
                    "/contract/orders";
                return this.callExternalService(cactusKey.data.akid, cactusKey.data.apiKey, cactusKey.data.privateKey, "GET", urlTransactionDetails, undefined, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    callExternalService(akid, apiKey, privateKey, mothod, endpoint, queryParam, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const date = new Date().toUTCString();
            const apiNonce = Math.round(new Date().getTime() / 1000);
            var queryString = "";
            var queryStringToSign = "";
            if (queryParam !== undefined) {
                if ((queryParam === null || queryParam === void 0 ? void 0 : queryParam.length) != 0) {
                    queryString = "?" + (yield this.getUrlParams(queryParam));
                    queryStringToSign = "?" + (yield this.getUrlSignParams(queryParam));
                }
            }
            var bodyHash = undefined;
            if (body !== undefined) {
                bodyHash = yield this.getBodyHash(body);
            }
            var contentToSign = "";
            if (mothod !== "GET") {
                contentToSign =
                    mothod +
                        "\n" +
                        "application/json\n" +
                        bodyHash +
                        "\n" +
                        "application/json\n" +
                        date +
                        "\n" +
                        "x-api-key:" +
                        apiKey +
                        "\n" +
                        "x-api-nonce:" +
                        apiNonce +
                        "\n" +
                        endpoint;
            }
            else {
                contentToSign =
                    mothod +
                        "\n" +
                        "application/json\n" +
                        "\n" +
                        "application/json\n" +
                        date +
                        "\n" +
                        "x-api-key:" +
                        apiKey +
                        "\n" +
                        "x-api-nonce:" +
                        apiNonce +
                        "\n" +
                        endpoint +
                        queryStringToSign;
            }
            const signature = yield this.generateSignature(contentToSign, privateKey);
            const header = yield this.getHeaderMethod(akid, apiKey, apiNonce.toString(), date, signature, bodyHash);
            return yield this._httpService.callFetch(mothod, header, process.env.HOST_CACTUS_CUSTODY + endpoint + queryString, body);
        });
    }
    getUrlParams(queryParam) {
        return __awaiter(this, void 0, void 0, function* () {
            if (queryParam.length == 0) {
                return "";
            }
            var param = "";
            var sortedKey = queryParam.sort();
            sortedKey.forEach((element) => {
                Object.entries(element).forEach(([key, value]) => {
                    param += key + "=" + value + "&";
                });
            });
            param = param.slice(0, -1);
            return param;
        });
    }
    getUrlSignParams(queryParam) {
        return __awaiter(this, void 0, void 0, function* () {
            if (queryParam.length == 0) {
                return "";
            }
            var param = "{";
            var sortedKey = queryParam.sort();
            sortedKey.forEach((element) => {
                Object.entries(element).forEach(([key, value]) => {
                    param += key + "=[" + value + "], ";
                });
            });
            param = param.slice(0, -2);
            return param + "}";
        });
    }
    getHeaderMethod(akid, apiKey, apiNonce, date, signature, bodyHash) {
        return __awaiter(this, void 0, void 0, function* () {
            if (bodyHash !== undefined) {
                return {
                    "x-api-key": apiKey,
                    "x-api-nonce": apiNonce,
                    Accept: "application/json",
                    "Content-SHA256": bodyHash,
                    Date: date,
                    "Content-type": "application/json",
                    Authorization: "api " + akid + ":" + signature,
                };
            }
            else {
                return {
                    "x-api-key": apiKey,
                    "x-api-nonce": apiNonce,
                    Accept: "application/json",
                    Date: date,
                    "Content-type": "application/json",
                    Authorization: "api " + akid + ":" + signature,
                };
            }
        });
    }
    generateSignature(contentToSign, privateKey) {
        return __awaiter(this, void 0, void 0, function* () {
            const sign = crypto.createSign("sha256");
            sign.write(contentToSign);
            sign.end();
            var signature = sign.sign(privateKey, "base64");
            return signature;
        });
    }
    getBodyHash(contentToSign) {
        return __awaiter(this, void 0, void 0, function* () {
            const hash = crypto.createHash("sha256");
            hash.update(contentToSign);
            hash.end();
            return hash.digest("base64");
        });
    }
};
exports.CactusCustodyService = CactusCustodyService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [vaultService_1.VaultService, httpService_1.HttpService])
], CactusCustodyService);
//# sourceMappingURL=cactusCustodyService.js.map