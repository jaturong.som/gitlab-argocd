"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigService = void 0;
const companyRepository_1 = require("../repositories/companyRepository");
const assetRateRepository_1 = require("../repositories/assetRateRepository");
const assetRepository_1 = require("../repositories/assetRepository");
const networkRepository_1 = require("../repositories/networkRepository");
const dropdownlistRepository_1 = require("../repositories/dropdownlistRepository");
const vaultAccountService_1 = require("./vaultAccountService");
const assetInVaultAccountService_1 = require("./assetInVaultAccountService");
const masterService_1 = require("./masterService");
const enums = __importStar(require("../utilities/enums"));
const customError_1 = __importDefault(require("../middlewares/customError"));
const tsyringe_1 = require("tsyringe");
let ConfigService = exports.ConfigService = class ConfigService {
    constructor(companyRepository, assetRateRepository, assetRepository, networkRepository, dropdownlistRepository, vaultAccountService, assetInVaultAccountService, masterService) {
        this._companyRepository = companyRepository;
        this._assetRateRepository = assetRateRepository;
        this._assetRepository = assetRepository;
        this._networkRepository = networkRepository;
        this._dropdownlistRepository = dropdownlistRepository;
        this._vaultAccountService = vaultAccountService;
        this._assetInVaultAccountService = assetInVaultAccountService;
        this._masterService = masterService;
    }
    getAssetsInVaultAccount() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const companies = yield this._companyRepository.getList();
                if (companies.length <= 0) {
                    throw new customError_1.default(enums.responseCode.CompanyNotFound, enums.responseMessage.CompanyNotFound);
                }
                const vaultAccountInfo = yield this._vaultAccountService.getVaultAccount(enums.accountType.COMPANY, companies[0].company_id, undefined);
                return yield this._assetInVaultAccountService.getAssetsInVaultAccount(vaultAccountInfo.vault_account_id);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetInVaultAccountById(assetInVaultAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._assetInVaultAccountService.getAssetInVaultAccountById(assetInVaultAccountId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    createAssetInVaultAccount(assetCode, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const companies = yield this._companyRepository.getList();
                if (companies.length <= 0) {
                    throw new customError_1.default(enums.responseCode.CompanyNotFound, enums.responseMessage.CompanyNotFound);
                }
                return yield this._assetInVaultAccountService.createAssetInVaultAccount(enums.accountType.COMPANY, assetCode, logOnId, parseInt(companies[0].company_id), undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetRates() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const networks = yield this._networkRepository.getListForAssetRates();
                if (networks.length <= 0) {
                    return [];
                }
                var vResult = [];
                for (let i = 0; i < networks.length; i++) {
                    const networkInfo = networks[i];
                    const assetRates = yield this._assetRateRepository.getListByNetworkId(networkInfo.network_id);
                    var vResultAssetRates = [];
                    for (let i = 0; i < assetRates.length; i++) {
                        const assetRateInfo = assetRates[i];
                        vResultAssetRates.push({
                            asset_rate_id: assetRateInfo.asset_rate_id,
                            asset_id: assetRateInfo.asset_id,
                            asset_code: assetRateInfo.asset_code,
                            asset_name: assetRateInfo.asset_name,
                            asset_image: assetRateInfo.asset_image,
                            buying: assetRateInfo.buying,
                            selling: assetRateInfo.selling,
                            updated_by: assetRateInfo.updated_by,
                            updated_date: assetRateInfo.updated_date,
                        });
                    }
                    vResult.push({
                        network_id: networkInfo.network_id,
                        network_code: networkInfo.code,
                        network_name: networkInfo.name,
                        network_image: networkInfo.image,
                        created_by: networkInfo.fullname_created_by,
                        created_date: networkInfo.created_date,
                        assets: vResultAssetRates,
                    });
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetRateById(assetRateId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const assetRates = yield this._assetRateRepository.getInfoById(assetRateId);
                if (assetRates.length <= 0) {
                    throw new customError_1.default(enums.responseCode.AssetNotFound, enums.responseMessage.AssetNotFound);
                }
                return yield this.setAssetRateModel(assetRates[0]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetRateByAssetCode(assetCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const assetRates = yield this._assetRateRepository.getInfoByAssetCode(assetCode);
                if (assetRates.length <= 0) {
                    throw new customError_1.default(enums.responseCode.AssetNotFound, enums.responseMessage.AssetNotFound);
                }
                return yield this.setAssetRateModel(assetRates[0]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    setAssetRateModel(assetRateInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                asset_rate_id: assetRateInfo.asset_rate_id,
                asset_id: assetRateInfo.asset_id,
                asset_code: assetRateInfo.asset_code,
                asset_name: assetRateInfo.asset_name,
                asset_image: assetRateInfo.asset_image,
                network_id: assetRateInfo.network_id,
                network_code: assetRateInfo.network_code,
                network_name: assetRateInfo.network_name,
                network_image: assetRateInfo.network_image,
                buying: assetRateInfo.buying,
                selling: assetRateInfo.selling,
                updated_by: assetRateInfo.updated_by,
                updated_date: assetRateInfo.updated_date,
            };
        });
    }
    createAssetRate(assetCode, buying, selling, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const assets = yield this._assetRepository.getInfoByCode(assetCode);
                if (assets.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const assetRates = yield this._assetRateRepository.getInfoByAssetCode(assetCode);
                if (assetRates.length > 0) {
                    throw new customError_1.default(enums.responseCode.Duplicate, enums.responseMessage.Duplicate);
                }
                yield this._assetRateRepository.create(assets[0].asset_id, assets[0].asset_code, buying, selling, logOnId);
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateAssetRate(assetRateId, buying, selling, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._assetRateRepository.getInfoById(assetRateId);
                if (isExists.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                yield this._assetRateRepository.update(assetRateId, buying, selling, logOnId);
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    deleteAssetRate(assetRateId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._assetRateRepository.getInfoById(assetRateId);
                if (isExists.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                yield this._assetRateRepository.delete(assetRateId);
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getNetworks() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const networks = yield this._networkRepository.getList();
                var vResult = [];
                for (let i = 0; i < networks.length; i++) {
                    vResult.push(yield this.setNetworkInfo(networks[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setNetworkInfo(networkInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                network_id: networkInfo.network_id,
                code: networkInfo.code,
                name: networkInfo.name,
                image: networkInfo.image,
            };
        });
    }
    getAssets(networkId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const assets = yield this._assetRepository.getListByNetworkId(networkId);
                var vResult = [];
                for (let i = 0; i < assets.length; i++) {
                    vResult.push(yield this.setAssetInfo(assets[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setAssetInfo(assetInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                asset_id: assetInfo.asset_id,
                asset_code: assetInfo.asset_code,
                asset_name: assetInfo.asset_name,
                asset_image: assetInfo.asset_image,
                contract_address: assetInfo.contract_address,
                network_id: assetInfo.network_id,
                network_code: assetInfo.network_code,
                network_name: assetInfo.network_name,
                network_image: assetInfo.network_image,
            };
        });
    }
    getPurposeOfTransfer() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._masterService.getDropdownListByType(enums.dropdownlistType.PURPOSE_OF_TRANSFER);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getRelationshipToReceiver() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._masterService.getDropdownListByType(enums.dropdownlistType.RELATIONSHIP_TO_RECEIVER);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getDropdownListByType(type) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._masterService.getDropdownListByType(type);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.ConfigService = ConfigService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [companyRepository_1.CompanyRepository,
        assetRateRepository_1.AssetRateRepository,
        assetRepository_1.AssetRepository,
        networkRepository_1.NetworkRepository,
        dropdownlistRepository_1.DropdownlistRepository,
        vaultAccountService_1.VaultAccountService,
        assetInVaultAccountService_1.AssetInVaultAccountService,
        masterService_1.MasterService])
], ConfigService);
//# sourceMappingURL=configService.js.map