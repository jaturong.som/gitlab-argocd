"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MasterService = void 0;
const countryRepository_1 = require("../repositories/countryRepository");
const currencyRepository_1 = require("../repositories/currencyRepository");
const dropdownlistRepository_1 = require("../repositories/dropdownlistRepository");
const tsyringe_1 = require("tsyringe");
const nationalityRepository_1 = require("../repositories/nationalityRepository");
let MasterService = exports.MasterService = class MasterService {
    constructor(countryRepository, currencyRepository, dropdownlistRepository, nationalityRepository) {
        this._countryRepository = countryRepository;
        this._currencyRepository = currencyRepository;
        this._dropdownlistRepository = dropdownlistRepository;
        this._nationalityRepository = nationalityRepository;
    }
    getCountryList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._countryRepository.getList();
                var vResult = [];
                for (let i = 0; i < result.length; i++) {
                    vResult.push(yield this.setCountrylistInfo(result[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setCountrylistInfo(data) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                code: data.code,
                alpha_2_code: data.alpha_2_code,
                name: data.name,
                image: data.image,
                calling_code: data.calling_code,
            };
        });
    }
    getCurrencyList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._currencyRepository.getList();
                var vResult = [];
                for (let i = 0; i < result.length; i++) {
                    vResult.push(yield this.setCurrencylistInfo(result[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setCurrencylistInfo(data) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                code: data.code,
            };
        });
    }
    getDropdownListByType(type) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._dropdownlistRepository.getListByType(type);
                var vResult = [];
                for (let i = 0; i < result.length; i++) {
                    vResult.push(yield this.setDropdownlistInfo(result[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setDropdownlistInfo(data) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                value: data.value,
                text: data.text,
            };
        });
    }
    getNationalityList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._nationalityRepository.getList();
                var vResult = [];
                for (let i = 0; i < result.length; i++) {
                    vResult.push(yield this.setNationaliotylistInfo(result[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setNationaliotylistInfo(data) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                value: data.code,
                text: data.name,
            };
        });
    }
};
exports.MasterService = MasterService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [countryRepository_1.CountryRepository,
        currencyRepository_1.CurrencyRepository,
        dropdownlistRepository_1.DropdownlistRepository,
        nationalityRepository_1.NationalityRepository])
], MasterService);
//# sourceMappingURL=masterService.js.map