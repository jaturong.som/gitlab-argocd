"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChainalysisService = void 0;
const assetRepository_1 = require("../repositories/assetRepository");
const chainalysisTransactionRepository_1 = require("../repositories/chainalysisTransactionRepository");
const vaultService_1 = require("./vaultService");
const fireblocksService_1 = require("./fireblocksService");
const tsyringe_1 = require("tsyringe");
const enums = __importStar(require("../utilities/enums"));
const customError_1 = __importDefault(require("../middlewares/customError"));
const httpService_1 = require("./httpService");
let ChainalysisService = exports.ChainalysisService = class ChainalysisService {
    constructor(chainalysisTransactionRepository, assetRepository, vaultService, fireblocksService, httpService) {
        this._chainalysisTransactionRepository = chainalysisTransactionRepository;
        this._assetRepository = assetRepository;
        this._vaultService = vaultService;
        this._fireblocksService = fireblocksService;
        this._httpService = httpService;
    }
    manualUpdateTransactionStatus(externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.updateTransactionStatus(externalId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateTransactionStatus(externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisTransactions = yield this._chainalysisTransactionRepository.getInfoByExternalId(externalId);
                if (chainalysisTransactions.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const chainalysisTransactionInfo = chainalysisTransactions[0];
                const summary = yield this.transfers_get_a_summary(externalId);
                if (summary !== undefined) {
                    if (summary.updatedAt !== undefined) {
                        if (summary.updatedAt !== null) {
                            yield this._chainalysisTransactionRepository.update(externalId, summary.updatedAt, summary.asset, summary.network, summary.transferReference, summary.tx, summary.idx, summary.usdAmount, summary.assetAmount, summary.timestamp, summary.outputAddress);
                            const alertList = yield this.transfers_get_alerts(externalId);
                            if (alertList === undefined) {
                                yield this._fireblocksService.unfreezeTransactionById(chainalysisTransactionInfo.fb_transaction_id);
                            }
                            else {
                                if (alertList.alerts.length <= 0) {
                                    yield this._fireblocksService.unfreezeTransactionById(chainalysisTransactionInfo.fb_transaction_id);
                                }
                                else {
                                    var isPass = true;
                                    for (let index = 0; index < alertList.alerts.length; index++) {
                                        const alert = alertList.alerts[index];
                                        if (alert.alertLevel === enums.chainalysisAlert.HIGH ||
                                            alert.alertLevel === enums.chainalysisAlert.MEDIUM) {
                                            isPass = false;
                                            break;
                                        }
                                    }
                                    if (isPass) {
                                        yield this._fireblocksService.unfreezeTransactionById(chainalysisTransactionInfo.fb_transaction_id);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (error) {
                throw error;
            }
        });
    }
    monitoringTransaction(body) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let data = body.data;
                const chainalysisTransactions = yield this._chainalysisTransactionRepository.getInfoByFBTransactionId(data.id);
                if (chainalysisTransactions.length > 0) {
                    return;
                }
                if (data.source === undefined) {
                    return;
                }
                if (data.source.type === "EXTERNAL_WALLET" ||
                    data.source.type === "UNKNOWN") {
                    let vUserId = "";
                    if (data.destination != undefined) {
                        if (data.destination.id != "") {
                            vUserId = data.destination.id;
                        }
                        else {
                            vUserId = "Anonymous";
                        }
                    }
                    const assets = yield this._assetRepository.getInfoByCodeIgnoreStatus(data.assetId);
                    if (assets.length <= 0) {
                        return;
                    }
                    const assetInfo = assets[0];
                    const registerTrasaction = yield this.register_a_transfer(vUserId, assetInfo.network_name, assetInfo.native_asset_symbol, data.txHash, data.destinationAddress, "sent", data.amount, 1, "USD");
                    yield this._chainalysisTransactionRepository.create(data.id, registerTrasaction.updatedAt, registerTrasaction.asset, registerTrasaction.network, registerTrasaction.transferReference, registerTrasaction.tx, registerTrasaction.idx, registerTrasaction.usdAmount, registerTrasaction.assetAmount, registerTrasaction.timestamp, registerTrasaction.outputAddress, registerTrasaction.externalId);
                    yield this._fireblocksService.freezeTransactionById(data.id);
                }
            }
            catch (error) {
                throw error;
            }
        });
    }
    register_a_transfer(userId, pNetwork, pAsset, pTransactionHash, pOutputAddress, pDirection, pAssetAmount, pAssetPrice, pAssetDenomination) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisToken = yield this._vaultService.getChainalysisToken();
                const vBody = JSON.stringify({
                    network: pNetwork,
                    asset: pAsset,
                    transferReference: pTransactionHash + ":" + pOutputAddress,
                    direction: pDirection,
                    transferTimestamp: new Date().toISOString(),
                    assetAmount: pAssetAmount,
                    outputAddress: pOutputAddress,
                    assetPrice: pAssetPrice,
                    assetDenomination: pAssetDenomination,
                });
                return yield this._httpService.callFetch("POST", {
                    Token: chainalysisToken.data.apiKey,
                    "Content-type": "application/json",
                }, process.env.CHAINALYSIS_ENDPOINT +
                    "/v2/users/" +
                    userId +
                    "/transfers", vBody);
            }
            catch (error) {
                throw error;
            }
        });
    }
    register_a_withdrawal_attempt(userId, pNetwork, pAsset, pAddress, pAttemptIdentifier, pAssetAmount, pAssetPrice, pAssetDenomination) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisToken = yield this._vaultService.getChainalysisToken();
                const vBody = JSON.stringify({
                    network: pNetwork,
                    asset: pAsset,
                    address: pAddress,
                    attemptIdentifier: pAttemptIdentifier,
                    assetAmount: pAssetAmount,
                    assetPrice: pAssetPrice,
                    assetDenomination: pAssetDenomination,
                    attemptTimestamp: new Date().toISOString(),
                });
                return yield this._httpService.callFetch("POST", {
                    Token: chainalysisToken.data.apiKey,
                    "Content-type": "application/json",
                }, process.env.CHAINALYSIS_ENDPOINT +
                    "/v2/users/" +
                    userId +
                    "/withdrawal-attempts", vBody);
            }
            catch (error) {
                throw error;
            }
        });
    }
    transfers_get_a_summary(externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisToken = yield this._vaultService.getChainalysisToken();
                return yield this._httpService.callFetch("GET", {
                    Token: chainalysisToken.data.apiKey,
                    "Content-type": "application/json",
                }, process.env.CHAINALYSIS_ENDPOINT + "/v2/transfers/" + externalId, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    transfers_get_direct_exposure(externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisToken = yield this._vaultService.getChainalysisToken();
                return yield this._httpService.callFetch("GET", {
                    Token: chainalysisToken.data.apiKey,
                    "Content-type": "application/json",
                }, process.env.CHAINALYSIS_ENDPOINT +
                    "/v2/transfers/" +
                    externalId +
                    "/exposures", undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    transfers_get_alerts(externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisToken = yield this._vaultService.getChainalysisToken();
                return yield this._httpService.callFetch("GET", {
                    Token: chainalysisToken.data.apiKey,
                    "Content-type": "application/json",
                }, process.env.CHAINALYSIS_ENDPOINT +
                    "/v2/transfers/" +
                    externalId +
                    "/alerts", undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    transfers_get_network_identifications(externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisToken = yield this._vaultService.getChainalysisToken();
                return yield this._httpService.callFetch("GET", {
                    Token: chainalysisToken.data.apiKey,
                    "Content-type": "application/json",
                }, process.env.CHAINALYSIS_ENDPOINT +
                    "/v2/transfers/" +
                    externalId +
                    "/network-identifications", undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    withdrawal_attempts_get_a_summary(externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisToken = yield this._vaultService.getChainalysisToken();
                return yield this._httpService.callFetch("GET", {
                    Token: chainalysisToken.data.apiKey,
                    "Content-type": "application/json",
                }, process.env.CHAINALYSIS_ENDPOINT +
                    "/v2/withdrawal-attempts/" +
                    externalId, undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    withdrawal_attempts_get_direct_exposure(externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisToken = yield this._vaultService.getChainalysisToken();
                return yield this._httpService.callFetch("GET", {
                    Token: chainalysisToken.data.apiKey,
                    "Content-type": "application/json",
                }, process.env.CHAINALYSIS_ENDPOINT +
                    "/v2/withdrawal-attempts/" +
                    externalId +
                    "/exposures", undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    withdrawal_attempts_get_alerts(externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisToken = yield this._vaultService.getChainalysisToken();
                return yield this._httpService.callFetch("GET", {
                    Token: chainalysisToken.data.apiKey,
                    "Content-type": "application/json",
                }, process.env.CHAINALYSIS_ENDPOINT +
                    "/v2/withdrawal-attempts/" +
                    externalId +
                    "/alerts", undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    withdrawal_attempts_get_address_identifications(externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisToken = yield this._vaultService.getChainalysisToken();
                return yield this._httpService.callFetch("GET", {
                    Token: chainalysisToken.data.apiKey,
                    "Content-type": "application/json",
                }, process.env.CHAINALYSIS_ENDPOINT +
                    "/v2/withdrawal-attempts/" +
                    externalId +
                    "/high-risk-addresses", undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
    withdrawal_attempts_get_network_identifications(externalId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chainalysisToken = yield this._vaultService.getChainalysisToken();
                return yield this._httpService.callFetch("GET", {
                    Token: chainalysisToken.data.apiKey,
                    "Content-type": "application/json",
                }, process.env.CHAINALYSIS_ENDPOINT +
                    "/v2/withdrawal-attempts/" +
                    externalId +
                    "/network-identifications", undefined);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.ChainalysisService = ChainalysisService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [chainalysisTransactionRepository_1.ChainalysisTransactionRepository,
        assetRepository_1.AssetRepository,
        vaultService_1.VaultService,
        fireblocksService_1.FireblocksService,
        httpService_1.HttpService])
], ChainalysisService);
//# sourceMappingURL=chainalysisService.js.map