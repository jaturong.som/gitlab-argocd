"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CactusWalletAddressService = void 0;
const tsyringe_1 = require("tsyringe");
const cactusWalletAddressRepository_1 = require("../repositories/cactusWalletAddressRepository");
let CactusWalletAddressService = exports.CactusWalletAddressService = class CactusWalletAddressService {
    constructor(cactusWalletAddressRepository) {
        this._cactusWalletAddressRepository = cactusWalletAddressRepository;
    }
    getAssetsInVaultAccountByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const assetInVaults = yield this._cactusWalletAddressRepository.getListByUserId(userId);
                var vResult = [];
                for (let i = 0; i < assetInVaults.length; i++) {
                    vResult.push(yield this.setAssetInVaultAccountInfo(assetInVaults[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setAssetInVaultAccountInfo(assetInVaultInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                asset_in_vault_account_id: assetInVaultInfo.cactus_wallet_address_id,
                network_id: assetInVaultInfo.network_id,
                network_code: assetInVaultInfo.network_code,
                network_name: assetInVaultInfo.network_name,
                network_image: assetInVaultInfo.network_image,
                asset_id: assetInVaultInfo.asset_id,
                asset_code: assetInVaultInfo.asset_code,
                asset_name: assetInVaultInfo.asset_name,
                asset_image: assetInVaultInfo.asset_image,
                asset_address: assetInVaultInfo.asset_address,
                total: assetInVaultInfo.total,
                lockedamount: assetInVaultInfo.lockedamount,
                available: assetInVaultInfo.available,
                pending: assetInVaultInfo.pending,
                total_selling_amount: assetInVaultInfo.total_selling_amount,
                buying: assetInVaultInfo.buying,
                selling: assetInVaultInfo.selling,
                status: assetInVaultInfo.status,
                updated_by: assetInVaultInfo.updated_by,
                updated_date: assetInVaultInfo.updated_date,
            };
        });
    }
};
exports.CactusWalletAddressService = CactusWalletAddressService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [cactusWalletAddressRepository_1.CactusWalletAddressRepository])
], CactusWalletAddressService);
//# sourceMappingURL=cactusWalletAddressService.js.map