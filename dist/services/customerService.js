"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerService = void 0;
const companyRepository_1 = require("../repositories/companyRepository");
const userRepository_1 = require("../repositories/userRepository");
const userApiKeyRepository_1 = require("../repositories/userApiKeyRepository");
const bankAccountRepository_1 = require("../repositories/bankAccountRepository");
const vaultAccountRepository_1 = require("../repositories/vaultAccountRepository");
const fiatAccountRepository_1 = require("../repositories/fiatAccountRepository");
const assetInVaultAccountRepository_1 = require("../repositories/assetInVaultAccountRepository");
const withdrawRepository_1 = require("../repositories/withdrawRepository");
const assetRepository_1 = require("../repositories/assetRepository");
const networkRepository_1 = require("../repositories/networkRepository");
const assetRateRepository_1 = require("../repositories/assetRateRepository");
const transactionRepository_1 = require("../repositories/transactionRepository");
const fireblocksTransactionRepository_1 = require("../repositories/fireblocksTransactionRepository");
const vaultAccountService_1 = require("./vaultAccountService");
const assetInVaultAccountService_1 = require("./assetInVaultAccountService");
const transactionService_1 = require("./transactionService");
const fireblocksService_1 = require("./fireblocksService");
const enums = __importStar(require("../utilities/enums"));
const customError_1 = __importDefault(require("../middlewares/customError"));
const utils = __importStar(require("../utilities/utils"));
const tsyringe_1 = require("tsyringe");
const addressRepository_1 = require("../repositories/addressRepository");
const userDocumentRepository_1 = require("../repositories/userDocumentRepository");
// import {
//   RequestUpdateCustomerAddress,
//   RequestUpdateCustomerProfile,
// } from "../models/customerModels";
const lqnService_1 = require("./lqnService");
const countryRepository_1 = require("../repositories/countryRepository");
const lqnWithdrawService_1 = require("./lqnWithdrawService");
const lqnUserRepository_1 = require("../repositories/lqnUserRepository");
const lqnTransactionRepository_1 = require("../repositories/lqnTransactionRepository");
// import moment from "moment";
let CustomerService = exports.CustomerService = class CustomerService {
    constructor(companyRepository, userRepository, userApiKeyRepository, bankAccountRepository, vaultAccountRepository, fiatAccountRepository, assetInVaultAccountRepository, withdrawRepository, assetRepository, networkRepository, assetRateRepository, transactionRepository, fireblocksTransactionRepository, addressRepository, userDocumentRepository, vaultAccountService, assetInVaultAccountService, transactionService, fireblocksService, lqnService, countryRepository, lqnWithdrawService, lqnUserRepository, lqnTransactionRepository) {
        this._companyRepository = companyRepository;
        this._userRepository = userRepository;
        this._userApiKeyRepository = userApiKeyRepository;
        this._bankAccountRepository = bankAccountRepository;
        this._vaultAccountRepository = vaultAccountRepository;
        this._fiatAccountRepository = fiatAccountRepository;
        this._assetInVaultAccountRepository = assetInVaultAccountRepository;
        this._withdrawRepository = withdrawRepository;
        this._assetRepository = assetRepository;
        this._networkRepository = networkRepository;
        this._assetRateRepository = assetRateRepository;
        this._transactionRepository = transactionRepository;
        this._fireblocksTransactionRepository = fireblocksTransactionRepository;
        this._addressRepository = addressRepository;
        this._userDocumentRepository = userDocumentRepository;
        this._vaultAccountService = vaultAccountService;
        this._assetInVaultAccountService = assetInVaultAccountService;
        this._transactionService = transactionService;
        this._fireblocksService = fireblocksService;
        this._lqnService = lqnService;
        this._countryRepository = countryRepository;
        this._lqnWithdrawService = lqnWithdrawService;
        this._lqnUserRepository = lqnUserRepository;
        this._lqnTransactionRepository = lqnTransactionRepository;
    }
    getUserApiKeys(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userApiKeys = yield this._userApiKeyRepository.getList(userId);
                var vResult = [];
                for (let i = 0; i < userApiKeys.length; i++) {
                    vResult.push(yield this.setUserApiKeyInfo(userApiKeys[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserApiKeyById(userApiKeyId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userApiKeys = yield this._userApiKeyRepository.getInfoById(userApiKeyId);
                if (userApiKeys.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                return yield this.setUserApiKeyInfo(userApiKeys[0]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    setUserApiKeyInfo(userApiKeyInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                user_api_key_id: userApiKeyInfo.user_api_key_id,
                user_id: userApiKeyInfo.user_id,
                name: userApiKeyInfo.name,
                user_key: userApiKeyInfo.user_key,
                user_secret: userApiKeyInfo.user_secret,
                status: userApiKeyInfo.status,
                created_by: userApiKeyInfo.created_by,
                created_date: userApiKeyInfo.created_date,
            };
        });
    }
    createUserApiKey(userId, name, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._userApiKeyRepository.isDuplicateByName(userId, name);
                if (isExists.length > 0) {
                    throw new customError_1.default(enums.responseCode.Duplicate, enums.responseMessage.Duplicate);
                }
                yield this._userApiKeyRepository.create(userId, name, yield utils.generateCustomerApiKey(userId.toString(), name), yield utils.generateCustomerSignature(userId.toString(), name), logOnId);
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    enableOrDisableUserApiKey(userId, userApiKeyId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const apiKeys = yield this._userApiKeyRepository.getInfoById(userApiKeyId);
                if (apiKeys.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const apiKeyInfo = apiKeys[0];
                yield this._userApiKeyRepository.enableOrDisable(userId, userApiKeyId, apiKeyInfo.status === "A" ? "I" : "A", logOnId);
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    disableAllUserApiKeyByUserId(userId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._userApiKeyRepository.getList(userId);
                if (isExists.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                yield this._userApiKeyRepository.disableAllByUserId(userId, logOnId);
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserFiatAccounts(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._fiatAccountRepository.updateBalanceAllUsers();
                const userFiatAccounts = yield this._fiatAccountRepository.getInfoByUserId(userId);
                if (userFiatAccounts.length <= 0) {
                    throw new customError_1.default(enums.responseCode.FiatAccountNotFound, enums.responseMessage.FiatAccountNotFound);
                }
                return yield this.setFiatAccountInfo(userFiatAccounts[0]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    setFiatAccountInfo(fiatAccountInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                total: fiatAccountInfo.total,
                available: fiatAccountInfo.available,
                pending: fiatAccountInfo.pending,
                lockedamount: fiatAccountInfo.lockedamount,
            };
        });
    }
    // async getBankAccounts(userId: number) {
    //   try {
    //     const bankAccounts = await this._bankAccountRepository.getList(userId);
    //     var vResult = [];
    //     for (let i = 0; i < bankAccounts.length; i++) {
    //       var bankAddress = await this._addressRepository.getInfoByRefId(
    //         enums.addressType.BANK_ACCOUNT,
    //         bankAccounts[i].bank_account_id
    //       );
    //       vResult.push(
    //         await this.setBankAccountInfo(bankAccounts[i], bankAddress)
    //       );
    //     }
    //     return vResult;
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    //deprecate
    getBankAccountCoutries() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const lqnCountry = yield this._lqnService.getCatalogue("CTY", "", "", "");
                const countryList = yield this._countryRepository.getList();
                var vResult = [];
                for (let i = 0; i < lqnCountry.length; i++) {
                    for (let j = 0; j < countryList.length; j++) {
                        var lqn = lqnCountry[i].value;
                        var country = countryList[j].code;
                        if (lqn == country) {
                            vResult.push(yield this.setBankAccountCountry(i + 1, countryList[j]));
                        }
                    }
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setBankAccountCountry(id, bankAccountCountry) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                value: bankAccountCountry.name,
                text: bankAccountCountry.code,
            };
        });
    }
    getBankAccountsByBankType(userId, bankType) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const bankAccounts = yield this._bankAccountRepository.getListByBankType(userId, bankType);
                var vResult = [];
                for (let i = 0; i < bankAccounts.length; i++) {
                    vResult.push(yield this.setBankAccountInfo(bankAccounts[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getBankAccountById(bankAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const bankAccounts = yield this._bankAccountRepository.getInfoById(bankAccountId);
                if (bankAccounts.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                return yield this.setBankAccountInfo(bankAccounts[0]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    // async getDefaultBankAccountByUser(userId: number) {
    //   try {
    //     const bankAccounts =
    //       await this._bankAccountRepository.getDefaultBankAccount(userId);
    //     if (bankAccounts.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     var bankAddress = await this._addressRepository.getInfoByRefId(
    //       enums.addressType.BANK_ACCOUNT,
    //       bankAccounts[0].bank_account_id
    //     );
    //     return await this.setBankAccountInfo(bankAccounts[0], bankAddress);
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    setBankAccountInfo(bankAccountInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            var vTypeText = "";
            if (bankAccountInfo.bank_type === enums.bankAccountType.OWNER) {
                vTypeText = "My Book Bank";
            }
            else if (bankAccountInfo.bank_type === enums.bankAccountType.OTHER) {
                vTypeText = "Beneficiary Book Bank";
            }
            return {
                bank_account_id: bankAccountInfo.bank_account_id,
                bank_type: bankAccountInfo.bank_type,
                bank_type_text: vTypeText,
                location_id: bankAccountInfo.location_id,
                bank_name: bankAccountInfo.bank_name,
                bank_branch_name: bankAccountInfo.bank_branch_name,
                bank_branch_code: bankAccountInfo.bank_branch_code,
                bank_account_number: bankAccountInfo.bank_account_number,
                bank_account_name: bankAccountInfo.bank_account_name,
                swift_code: bankAccountInfo.swift_code,
                iban: bankAccountInfo.iban,
                sender_first_name: bankAccountInfo.sender_first_name,
                sender_middle_name: bankAccountInfo.sender_middle_name,
                sender_last_name: bankAccountInfo.sender_last_name,
                sender_contact_number: bankAccountInfo.sender_contact_number,
                sender_email: bankAccountInfo.sender_email,
                sender_address: bankAccountInfo.sender_address,
                sender_state: bankAccountInfo.sender_state,
                sender_area_town: bankAccountInfo.sender_area_town,
                sender_city: bankAccountInfo.sender_city,
                sender_zip_code: bankAccountInfo.sender_zip_code,
                sender_country_code: bankAccountInfo.sender_country_code,
                sender_country_name: bankAccountInfo.sender_country_name,
                receiver_first_name: bankAccountInfo.receiver_first_name,
                receiver_middle_name: bankAccountInfo.receiver_middle_name,
                receiver_last_name: bankAccountInfo.receiver_last_name,
                receiver_date_of_birth: bankAccountInfo.receiver_date_of_birth,
                receiver_gender: bankAccountInfo.receiver_gender,
                receiver_contact_number: bankAccountInfo.receiver_contact_number,
                receiver_email: bankAccountInfo.receiver_email,
                receiver_nationality: bankAccountInfo.receiver_nationality,
                receiver_nationality_name: bankAccountInfo.receiver_nationality_name,
                receiver_occupation: bankAccountInfo.receiver_occupation,
                receiver_occupation_remarks: bankAccountInfo.receiver_occupation_remarks,
                receiver_id_type: bankAccountInfo.receiver_id_type,
                receiver_id_type_remarks: bankAccountInfo.receiver_id_type_remarks,
                receiver_id_number: bankAccountInfo.receiver_id_number,
                receiver_id_issue_date: bankAccountInfo.receiver_id_issue_date,
                receiver_id_expire_date: bankAccountInfo.receiver_id_expire_date,
                receiver_native_first_name: bankAccountInfo.receiver_native_first_name,
                receiver_native_middle_name: bankAccountInfo.receiver_native_middle_name,
                receiver_native_last_name: bankAccountInfo.receiver_native_last_name,
                receiver_native_address: bankAccountInfo.receiver_native_address,
                receiver_account_type: bankAccountInfo.receiver_account_type,
                receiver_district: bankAccountInfo.receiver_district,
                beneficiary_type: bankAccountInfo.beneficiary_type,
                beneficiary_type_text: bankAccountInfo.beneficiary_type_text,
                currency_code: bankAccountInfo.currency_code,
                receiver_address: bankAccountInfo.receiver_address,
                receiver_state: bankAccountInfo.receiver_state,
                receiver_area_town: bankAccountInfo.receiver_area_town,
                receiver_city: bankAccountInfo.receiver_city,
                receiver_zip_code: bankAccountInfo.receiver_zip_code,
                receiver_country: bankAccountInfo.receiver_country_code,
                receiver_country_name: bankAccountInfo.receiver_country_name,
            };
        });
    }
    createBankAccount(bankType, receiverFirstName, receiverMiddleName, receiverLastName, receiverAddress, receiverDateOfBirth, receiverGender, receiverContactNumber, receiverState, receiverAreaTown, receiverCity, receiverZipCode, receiverCountry, receiverNationality, receiverIdType, receiverIdTypeRemarks, receiverIdNumber, receiverIdIssueDate, receiverIdExpireDate, receiverEmail, receiverAccountType, receiverOccupation, receiverOccupationRemarks, receiverDistrict, beneficiaryType, locationId, bankName, bankBranchName, bankBranchCode, bankAccountNumber, swiftCode, iban, userId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getActiveInfoById(userId);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                // const userInfo = users[0];
                var countries = yield this._countryRepository.getInfoByCode(receiverCountry);
                if (countries.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const countryInfo = countries[0];
                var senders = yield this._lqnUserRepository.getLqnUserByType(userId, enums.lqnUserType.SENDER);
                if (senders.length <= 0) {
                    throw new customError_1.default(enums.responseCode.SenderNotfound, enums.responseMessage.SenderNotfound);
                }
                const senderInfo = senders[0];
                const paymentMode = enums.paymentMode.BANK_TRANSFER;
                if (bankType == enums.bankAccountType.OWNER) {
                    if (senderInfo.country_code == receiverCountry) {
                        var addresses = yield this._addressRepository.getInfoByRefId(enums.addressType.USER_PROFILE, userId);
                        if (addresses.length <= 0) {
                            receiverAddress = "";
                            receiverState = "";
                            receiverAreaTown = "";
                            receiverCity = "";
                            receiverZipCode = "";
                            receiverCountry = "";
                        }
                        else {
                            const addressInfo = addresses[0];
                            receiverAddress = addressInfo.line1;
                            receiverState = addressInfo.state;
                            receiverAreaTown = addressInfo.area_town;
                            receiverCity = addressInfo.city;
                            receiverZipCode = addressInfo.postcode;
                            receiverCountry = addressInfo.country_code;
                        }
                    }
                    receiverFirstName = senderInfo.first_name;
                    receiverMiddleName = senderInfo.middle_name;
                    receiverLastName = senderInfo.last_name;
                    receiverDateOfBirth = senderInfo.date_of_birth;
                    receiverGender = senderInfo.gender;
                    receiverContactNumber = senderInfo.mobile_number;
                    receiverNationality = senderInfo.nationality;
                    receiverIdType = senderInfo.id_type;
                    receiverIdTypeRemarks = senderInfo.id_type_remark;
                    receiverIdNumber = senderInfo.id_number;
                    receiverIdIssueDate = senderInfo.id_issue_date;
                    receiverIdExpireDate = senderInfo.id_expired_date;
                    receiverEmail = senderInfo.email;
                    receiverAccountType = "";
                    receiverOccupation = senderInfo.occupation;
                    receiverOccupationRemarks = senderInfo.occupation_remark;
                    receiverDistrict = "";
                    beneficiaryType = senderInfo.user_type;
                }
                else {
                    yield this.validateBankAccount(bankType, receiverFirstName, receiverLastName, receiverAddress, receiverDateOfBirth, receiverGender, receiverContactNumber, receiverZipCode, receiverCountry, receiverIdType, receiverIdNumber, receiverIdIssueDate, receiverIdExpireDate, receiverOccupation, beneficiaryType);
                }
                const createReceiver = yield this._lqnService.createReceiver(receiverFirstName, receiverMiddleName, receiverLastName, receiverAddress, receiverDateOfBirth, receiverGender, receiverContactNumber, receiverState, receiverAreaTown, receiverCity, receiverZipCode, receiverCountry, receiverNationality, receiverIdType, receiverIdTypeRemarks, receiverIdNumber, receiverIdIssueDate, receiverIdExpireDate, receiverEmail, receiverAccountType, receiverOccupation, receiverOccupationRemarks, receiverDistrict, beneficiaryType, locationId, bankName, bankBranchName, bankBranchCode, bankAccountNumber, swiftCode, iban, paymentMode, countryInfo.currency_code, senderInfo.lqn_ref_id);
                if (createReceiver.code !== "0") {
                    throw new customError_1.default(createReceiver.code, createReceiver.message);
                }
                const accountName = receiverFirstName + " " + receiverLastName;
                const bankAccountCreated = yield this._bankAccountRepository.create(userId, bankType, locationId, bankName, bankAccountNumber, accountName, swiftCode, iban, bankBranchName, bankBranchCode, logOnId);
                const lqnUserProfiles = yield this._lqnUserRepository.create(enums.lqnUserType.RECEIVER, receiverFirstName, receiverMiddleName, receiverLastName, receiverGender, receiverContactNumber, receiverNationality, receiverIdType, receiverIdTypeRemarks, receiverIdNumber, receiverCountry, receiverIdIssueDate, receiverIdExpireDate, receiverDateOfBirth, receiverOccupation, receiverOccupationRemarks, receiverEmail, receiverFirstName, receiverMiddleName, receiverLastName, createReceiver.receiverId, logOnId, senderInfo.lqn_ref_id, countryInfo.currency_code, enums.status.ACTIVE, userId, beneficiaryType, bankAccountCreated[0].bank_account_id, receiverCountry);
                yield this._addressRepository.create(enums.addressType.LQN_USER_PROFILE, lqnUserProfiles[0].lqn_user_profile_id, receiverAddress, "", receiverCity, receiverState, receiverCountry, receiverZipCode, receiverAreaTown, logOnId);
                return bankAccountCreated[0].bank_account_id;
            }
            catch (error) {
                throw error;
            }
        });
    }
    validateBankAccount(bankType, receiverFirstName, 
    // receiverMiddleName: string,
    receiverLastName, receiverAddress, receiverDateOfBirth, receiverGender, receiverContactNumber, 
    // receiverState: string,
    // receiverAreaTown: string,
    // receiverCity: string,
    receiverZipCode, receiverCountry, 
    // receiverNationality: string,
    receiverIdType, 
    // receiverIdTypeRemarks: string,
    receiverIdNumber, receiverIdIssueDate, receiverIdExpireDate, 
    // receiverEmail: string,
    // receiverAccountType: string,
    receiverOccupation, 
    // receiverOccupationRemarks: string,
    // receiverDistrict: string,
    beneficiaryType
    // locationId: string,
    // bankName: string,
    // bankBranchName: string,
    // bankBranchCode: string,
    // bankAccountNumber: string,
    // swiftCode: string,
    // iban: string,
    // userId: number
    ) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (receiverFirstName === undefined || receiverFirstName === undefined) {
                    throw new customError_1.default(enums.responseCodeLQN.INVALID_FIRST_NAME, enums.responseMessageLQN.INVALID_FIRST_NAME);
                }
                else if (receiverLastName === undefined ||
                    receiverLastName === undefined) {
                    throw new customError_1.default(enums.responseCodeLQN.INVALID_LAST_NAME, enums.responseMessageLQN.INVALID_LAST_NAME);
                }
                else if (receiverAddress === undefined || receiverAddress === "") {
                    throw new customError_1.default(enums.responseCodeLQN.INVALID_ADDRESS, enums.responseMessageLQN.INVALID_ADDRESS);
                }
                else if (receiverZipCode === undefined || receiverZipCode === "") {
                    throw new customError_1.default(enums.responseCodeLQN.INVALID_POSTCODE, enums.responseMessageLQN.INVALID_POSTCODE);
                }
                else if (beneficiaryType === undefined || beneficiaryType === "") {
                    throw new customError_1.default(enums.responseCodeLQN.INVALID_BENEFICIARY, enums.responseMessageLQN.INVALID_BENEFICIARY);
                }
                if (receiverCountry === "SGP") {
                    if (receiverContactNumber === undefined ||
                        receiverContactNumber === "") {
                        throw new customError_1.default(enums.responseCodeLQN.INVALID_CONTACT_NUMBER, enums.responseMessageLQN.INVALID_CONTACT_NUMBER);
                    }
                }
                else if (receiverCountry === "CHN") {
                    if (receiverDateOfBirth === undefined || receiverDateOfBirth === "") {
                        throw new customError_1.default(enums.responseCodeLQN.INVALID_DATE_OF_BIRTH, enums.responseMessageLQN.INVALID_DATE_OF_BIRTH);
                    }
                    else if (receiverGender === undefined || receiverGender === "") {
                        throw new customError_1.default(enums.responseCodeLQN.INVALID_GENDER, enums.responseMessageLQN.INVALID_GENDER);
                    }
                    else if (receiverContactNumber === undefined ||
                        receiverContactNumber === "") {
                        throw new customError_1.default(enums.responseCodeLQN.INVALID_CONTACT_NUMBER, enums.responseMessageLQN.INVALID_CONTACT_NUMBER);
                    }
                    else if (receiverIdType === undefined || receiverIdType === "") {
                        throw new customError_1.default(enums.responseCodeLQN.INVALID_ID_TYPE, enums.responseMessageLQN.INVALID_ID_TYPE);
                    }
                    else if (receiverIdNumber === undefined || receiverIdNumber === "") {
                        throw new customError_1.default(enums.responseCodeLQN.INVALID_ID_NUMBER, enums.responseMessageLQN.INVALID_ID_NUMBER);
                    }
                    else if (receiverIdIssueDate === undefined ||
                        receiverIdIssueDate === "") {
                        throw new customError_1.default(enums.responseCodeLQN.INVALID_ID_ISSUE_DATE, enums.responseMessageLQN.INVALID_ID_ISSUE_DATE);
                    }
                    else if (receiverIdExpireDate === undefined ||
                        receiverIdExpireDate === "") {
                        throw new customError_1.default(enums.responseCodeLQN.INVALID_ID_EXPIRE_DATE, enums.responseMessageLQN.INVALID_ID_EXPIRE_DATE);
                    }
                    else if (receiverOccupation === undefined ||
                        receiverOccupation === "") {
                        throw new customError_1.default(enums.responseCodeLQN.INVALID_OCCUPATION, enums.responseMessageLQN.INVALID_OCCUPATION);
                    }
                }
            }
            catch (error) {
                throw error;
            }
        });
    }
    // async createOwnerBankAccount(
    //   userId: number,
    //   bankId: string,
    //   bankCode: string,
    //   bankName: string,
    //   accountNo: string,
    //   accountName: string,
    //   swiftCode: string,
    //   iban: string,
    //   logOnId: string
    // ) {
    //   try {
    //     const users = await this._userRepository.getActiveInfoById(userId);
    //     if (users.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.UserNotFound,
    //         enums.responseMessage.UserNotFound
    //       );
    //     }
    //     const userInfo = users[0];
    //     const isExists =
    //       await this._bankAccountRepository.isDuplicateByBankIdAccountNo(
    //         enums.mode.INSERT,
    //         userId,
    //         enums.bankAccountType.OWNER,
    //         bankCode,
    //         accountNo,
    //         undefined
    //       );
    //     if (isExists.length > 0) {
    //       throw new CustomError(
    //         enums.responseCode.Duplicate,
    //         enums.responseMessage.Duplicate
    //       );
    //     }
    //     const fullName = userInfo.first_name + " " + userInfo.last_name;
    //     if (accountName.toLowerCase() !== fullName.toLowerCase()) {
    //       throw new CustomError(
    //         enums.responseCode.AccountNameMismatch,
    //         enums.responseMessage.AccountNameMismatch
    //       );
    //     }
    //     var sender = await this._lqnUserRepository.getLqnUser(userId);
    //     if (sender.length == 0) {
    //       throw new CustomError(
    //         enums.responseCode.SenderNotfound,
    //         enums.responseMessage.SenderNotfound
    //       );
    //     }
    //     var currency = await this._countryCurrencyRepository.getByCountryCode(
    //       userInfo.country_code
    //     );
    //     var address = await this._addressRepository.getInfoByRefId(
    //       enums.addressType.USER_PROFILE,
    //       userId
    //     );
    //     var document = await this._userDocumentRepository.getList(userId);
    //     const dob = moment(userInfo.date_of_birth).format("yyyy-MM-DD");
    //     await this._lqnWithdrawService.Createreceiver(
    //       userInfo.first_name,
    //       userInfo.middle_name,
    //       userInfo.last_name,
    //       address[0].line1 + " " + address.line2,
    //       address[0].state,
    //       "",
    //       address[0].city,
    //       address[0].postcode,
    //       dob,
    //       userInfo.country_code,
    //       userInfo.nationality_code,
    //       document[0].document_type,
    //       document[0].document_type_remark,
    //       document[0].document_text,
    //       userInfo.email,
    //       userInfo.tel,
    //       "B",
    //       bankName,
    //       "",
    //       "",
    //       accountNo,
    //       userInfo.occupation,
    //       userInfo.occupation_remark,
    //       "",
    //       "",
    //       bankId,
    //       currency[0].currency_code,
    //       sender[0].lqn_ref_id,
    //       logOnId,
    //       userId
    //     );
    //     await this._bankAccountRepository.createOwner(
    //       userId,
    //       enums.bankAccountType.OWNER,
    //       bankCode,
    //       bankName,
    //       accountNo,
    //       accountName,
    //       swiftCode,
    //       iban,
    //       logOnId
    //     );
    //     return enums.responseMessage.Success;
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // async createOtherBankAccount(
    //   userId: number,
    //   bankCode: string,
    //   bankName: string,
    //   accountNo: string,
    //   accountName: string,
    //   swiftCode: string,
    //   iban: string,
    //   formName: string,
    //   branchCode: string,
    //   firstName: string,
    //   lastName: string,
    //   nationality: string,
    //   dateOfBirth: string,
    //   mobileNumber: string,
    //   logOnId: string,
    //   address?: {
    //     line1?: string;
    //     line2?: string;
    //     city?: string;
    //     state?: string;
    //     countryCode?: string;
    //     postcode?: string;
    //   }
    // ) {
    //   try {
    //     const users = await this._userRepository.getActiveInfoById(userId);
    //     if (users.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.UserNotFound,
    //         enums.responseMessage.UserNotFound
    //       );
    //     }
    //     const isExists =
    //       await this._bankAccountRepository.isDuplicateByBankIdAccountNo(
    //         enums.mode.INSERT,
    //         userId,
    //         enums.bankAccountType.OTHER,
    //         bankCode,
    //         accountNo,
    //         undefined
    //       );
    //     if (isExists.length > 0) {
    //       throw new CustomError(
    //         enums.responseCode.Duplicate,
    //         enums.responseMessage.Duplicate
    //       );
    //     }
    //     var vDateOfBirth = undefined;
    //     if (dateOfBirth !== undefined) {
    //       if (dateOfBirth !== null) {
    //         if (dateOfBirth !== "") {
    //           vDateOfBirth = dateOfBirth;
    //         }
    //       }
    //     }
    //     var bankAccount = await this._bankAccountRepository.createOther(
    //       userId,
    //       enums.bankAccountType.OTHER,
    //       bankCode,
    //       bankName,
    //       accountNo,
    //       accountName,
    //       swiftCode,
    //       iban,
    //       logOnId,
    //       firstName,
    //       lastName,
    //       nationality,
    //       mobileNumber,
    //       branchCode,
    //       formName,
    //       vDateOfBirth
    //     );
    //     await this._addressRepository.create(
    //       enums.addressType.BANK_ACCOUNT,
    //       bankAccount[0].bank_account_id,
    //       address!.line1!,
    //       address!.line2!,
    //       address!.city!,
    //       address!.state!,
    //       address!.countryCode!,
    //       address!.postcode!,
    //       logOnId
    //     );
    //     return enums.responseMessage.Success;
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // async updateOwnerBankAccount(
    //   bankAccountId: number,
    //   userId: number,
    //   bankCode: string,
    //   bankName: string,
    //   accountNo: string,
    //   accountName: string,
    //   swiftCode: string,
    //   iban: string,
    //   logOnId: string
    // ) {
    //   try {
    //     const users = await this._userRepository.getActiveInfoById(userId);
    //     if (users.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.UserNotFound,
    //         enums.responseMessage.UserNotFound
    //       );
    //     }
    //     const userInfo = users[0];
    //     const isExists = await this._bankAccountRepository.getInfoById(
    //       bankAccountId
    //     );
    //     if (isExists.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     const isDuplicate =
    //       await this._bankAccountRepository.isDuplicateByBankIdAccountNo(
    //         enums.mode.UPDATE,
    //         userId,
    //         enums.bankAccountType.OWNER,
    //         bankCode,
    //         accountNo,
    //         bankAccountId
    //       );
    //     if (isDuplicate.length > 0) {
    //       throw new CustomError(
    //         enums.responseCode.Duplicate,
    //         enums.responseMessage.Duplicate
    //       );
    //     }
    //     const fullName = userInfo.first_name + " " + userInfo.last_name;
    //     if (accountName.toLowerCase() !== fullName.toLowerCase()) {
    //       throw new CustomError(
    //         enums.responseCode.AccountNameMismatch,
    //         enums.responseMessage.AccountNameMismatch
    //       );
    //     }
    //     await this._bankAccountRepository.updateOwner(
    //       bankAccountId,
    //       bankCode,
    //       bankName,
    //       accountNo,
    //       accountName,
    //       swiftCode,
    //       iban,
    //       logOnId
    //     );
    //     return enums.responseMessage.Success;
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // async updateOtherBankAccount(
    //   bankAccountId: number,
    //   userId: number,
    //   bankCode: string,
    //   bankName: string,
    //   accountNo: string,
    //   accountName: string,
    //   swiftCode: string,
    //   iban: string,
    //   branchCode: string,
    //   firstName: string,
    //   lastName: string,
    //   nationality: string,
    //   dateOfBirth: string,
    //   mobileNumber: string,
    //   logOnId: string,
    //   address?: {
    //     addressId?: number;
    //     line1?: string;
    //     line2?: string;
    //     city?: string;
    //     state?: string;
    //     countryCode?: string;
    //     postcode?: string;
    //   }
    // ) {
    //   try {
    //     const users = await this._userRepository.getActiveInfoById(userId);
    //     if (users.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.UserNotFound,
    //         enums.responseMessage.UserNotFound
    //       );
    //     }
    //     const isExists = await this._bankAccountRepository.getInfoById(
    //       bankAccountId
    //     );
    //     if (isExists.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     const isDuplicate =
    //       await this._bankAccountRepository.isDuplicateByBankIdAccountNo(
    //         enums.mode.UPDATE,
    //         userId,
    //         enums.bankAccountType.OTHER,
    //         bankCode,
    //         accountNo,
    //         bankAccountId
    //       );
    //     if (isDuplicate.length > 0) {
    //       throw new CustomError(
    //         enums.responseCode.Duplicate,
    //         enums.responseMessage.Duplicate
    //       );
    //     }
    //     var vDateOfBirth = undefined;
    //     if (dateOfBirth !== undefined) {
    //       if (dateOfBirth !== null) {
    //         if (dateOfBirth !== "") {
    //           vDateOfBirth = dateOfBirth;
    //         }
    //       }
    //     }
    //     await this._bankAccountRepository.updateOther(
    //       bankAccountId,
    //       bankCode,
    //       bankName,
    //       accountNo,
    //       accountName,
    //       swiftCode,
    //       iban,
    //       logOnId,
    //       firstName,
    //       lastName,
    //       nationality,
    //       mobileNumber,
    //       branchCode,
    //       vDateOfBirth
    //     );
    //     await this._addressRepository.update(
    //       address!.addressId!,
    //       address!.line1!,
    //       address!.line2!,
    //       address!.city!,
    //       address!.state!,
    //       address!.countryCode!,
    //       address!.postcode!,
    //       logOnId
    //     );
    //     return enums.responseMessage.Success;
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    deleteBankAccount(bankAccountId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._bankAccountRepository.getInfoById(bankAccountId);
                if (isExists.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                yield this._bankAccountRepository.delete(bankAccountId, logOnId);
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    // async updateDefaultBankAccount(
    //   bankAccountId: number,
    //   userId: number,
    //   logOnId: string
    // ) {
    //   try {
    //     const isExists = await this._bankAccountRepository.getInfoById(
    //       bankAccountId
    //     );
    //     if (isExists.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     await this._bankAccountRepository.disableAllBankAccount(userId, logOnId);
    //     await this._bankAccountRepository.setDefaultBankAccount(
    //       bankAccountId,
    //       logOnId
    //     );
    //     return enums.responseMessage.Success;
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    getWithdraws(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._fiatAccountRepository.updateBalanceAllUsers();
                return yield this._transactionService.getWithdrawTransactionsByUserId(userId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWithdrawById(withdrawId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._transactionService.getWithdrawById(withdrawId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    // async createWithdraw(
    //   userId: number,
    //   bankAccountId: number,
    //   amount: number,
    //   logOnId: string
    // ) {
    //   try {
    //     await this._transactionService.updateBalance(
    //       enums.accountType.USER,
    //       undefined,
    //       userId
    //     );
    //     const userInfo = await this._userRepository.getActiveInfoById(userId);
    //     if (userInfo.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.UserNotFound,
    //         enums.responseMessage.UserNotFound
    //       );
    //     }
    //     const bankAccountInfo = await this._bankAccountRepository.getInfoById(
    //       bankAccountId
    //     );
    //     if (bankAccountInfo.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.BankAccountNotFound,
    //         enums.responseMessage.BankAccountNotFound
    //       );
    //     }
    //     var fiatAccounts = await this._fiatAccountRepository.getList(
    //       enums.accountType.USER,
    //       undefined,
    //       userId
    //     );
    //     if (fiatAccounts.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.FiatAccountNotFound,
    //         enums.responseMessage.FiatAccountNotFound
    //       );
    //     }
    //     const fiatAccountInfo = fiatAccounts[0];
    //     if (
    //       parseFloat(fiatAccountInfo.total_income) -
    //         parseFloat(fiatAccountInfo.total_expense) -
    //         parseFloat(fiatAccountInfo.total_pending) -
    //         amount <
    //       0
    //     ) {
    //       throw new CustomError(
    //         enums.responseCode.InsufficientFunds,
    //         enums.responseMessage.InsufficientFunds
    //       );
    //     }
    //     await this._withdrawRepository.create(
    //       userId,
    //       await utils.generateKey(userId.toString()),
    //       enums.transactionType.WITHDRAW,
    //       bankAccountId,
    //       amount,
    //       logOnId,
    //       undefined,
    //       undefined,
    //       undefined,
    //       undefined
    //     );
    //     await this._transactionService.updateBalance(
    //       enums.accountType.USER,
    //       undefined,
    //       userId
    //     );
    //     return enums.responseMessage.Success;
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    createWithdraw(senderSourceOfFund, senderSourceOfFundRemarks, purposeOfRemittance, purposeOfRemittanceRemarks, senderBeneficiaryRelationship, senderBeneficiaryRelationshipRemarks, transferAmount, //USD
    bankAccountId, userId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getActiveInfoById(userId);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                yield this._fiatAccountRepository.updateBalanceAllUsers();
                var fiatAccounts = yield this._fiatAccountRepository.getInfoByUserId(userId);
                if (fiatAccounts.length <= 0) {
                    throw new customError_1.default(enums.responseCode.FiatAccountNotFound, enums.responseMessage.FiatAccountNotFound);
                }
                const fiatAccountInfo = fiatAccounts[0];
                var bankAccounts = yield this._bankAccountRepository.getInfoById(bankAccountId);
                if (bankAccounts.length <= 0) {
                    throw new customError_1.default(enums.responseCode.ReceiverNotfound, enums.responseMessage.ReceiverNotfound);
                }
                const bankAccountsInfo = bankAccounts[0];
                var exchangeRate = yield this._lqnService.getExchangeRate(transferAmount, "C", bankAccountsInfo.currency_code, "B", bankAccountsInfo.location_id, bankAccountsInfo.receiver_country_code);
                var collectAmount = exchangeRate.collect_amount; // transferAmount + service_charge
                var payoutAmount = exchangeRate.payout_amount; //
                var serviceCharge = exchangeRate.service_charge;
                if (parseFloat(fiatAccountInfo.total) - collectAmount < 0) {
                    throw new customError_1.default(enums.responseCode.InsufficientFunds, enums.responseMessage.InsufficientFunds);
                }
                var receivers = yield this._lqnUserRepository.getLqnUserByBankAccountId(bankAccountId);
                if (receivers.length <= 0) {
                    throw new customError_1.default(enums.responseCode.ReceiverNotfound, enums.responseMessage.ReceiverNotfound);
                }
                const receiverInfo = receivers[0];
                const sendTransaction = yield this._lqnService.sendTransaction(senderSourceOfFund, senderSourceOfFundRemarks, purposeOfRemittance, purposeOfRemittanceRemarks, senderBeneficiaryRelationship, senderBeneficiaryRelationshipRemarks, "P", // calcBy option ‘C’ removed in sendTransaction API. Use only calcBy as ‘P’
                payoutAmount, "USD", // Currency (FIXED!?)
                receiverInfo.sender_ref_id, receiverInfo.lqn_ref_id);
                if (sendTransaction.code !== "0") {
                    throw new customError_1.default(sendTransaction.code, sendTransaction.message);
                }
                const lqnTransactionCreated = yield this._lqnTransactionRepository.create(enums.transactionType.WITHDRAW, receiverInfo.sender_ref_id, receiverInfo.lqn_ref_id, sendTransaction.agentSessionId, sendTransaction.confirmationId, sendTransaction.agentTxnId, sendTransaction.collectAmount, sendTransaction.collectCurrency, sendTransaction.serviceCharge, sendTransaction.gstCharge, sendTransaction.transferAmount, sendTransaction.exchangeRate, sendTransaction.payoutAmount, sendTransaction.payoutCurrency, sendTransaction.feeDiscount, sendTransaction.additionalPremiumRate, sendTransaction.settlementRate, sendTransaction.sendCommission, sendTransaction.settlementAmount, "", "", sendTransaction.message, "");
                const commitTransaction = yield this._lqnService.commitTransaction(sendTransaction.confirmationId);
                if (commitTransaction.code !== "0") {
                    throw new customError_1.default(commitTransaction.code, commitTransaction.message);
                }
                yield this._lqnTransactionRepository.updateConfirmTransaction(sendTransaction.confirmationId, commitTransaction.pinNumber, commitTransaction.status, "");
                var status = yield this.setLqnStatus(commitTransaction.status);
                yield this._withdrawRepository.create(yield utils.generateKey(userId.toString()), userId, enums.transactionType.WITHDRAW, bankAccountId, transferAmount, serviceCharge, status, logOnId, undefined, lqnTransactionCreated[0].lqn_transaction_id);
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setLqnStatus(status) {
        return __awaiter(this, void 0, void 0, function* () {
            status = status.toUpperCase();
            if (status == enums.lqnTransactionStatus.UN_COMMIT_HOLD ||
                status == enums.lqnTransactionStatus.UN_COMMIT_COMPLIANCE ||
                status == enums.lqnTransactionStatus.HOLD ||
                status == enums.lqnTransactionStatus.COMPLIANCE ||
                status == enums.lqnTransactionStatus.SANCTION) {
                return enums.transactionStatus.ONHOLD;
            }
            else if (status == enums.lqnTransactionStatus.UN_PAID ||
                status == enums.lqnTransactionStatus.POST ||
                status == enums.lqnTransactionStatus.API_PROCESSING) {
                return enums.transactionStatus.PENDING;
            }
            else if (status == enums.lqnTransactionStatus.PAID) {
                return enums.transactionStatus.SUCCESS;
            }
            else if (status == enums.lqnTransactionStatus.CANCEL ||
                status == enums.lqnTransactionStatus.CANCEL_HOLD ||
                status == enums.lqnTransactionStatus.BLOCK) {
                return enums.transactionStatus.CANCELLED;
            }
            else {
                return status;
            }
        });
    }
    createTransfer(senderSourceOfFund, senderSourceOfFundRemarks, purposeOfRemittance, purposeOfRemittanceRemarks, senderBeneficiaryRelationship, senderBeneficiaryRelationshipRemarks, transferAmount, bankAccountId, userId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getActiveInfoById(userId);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                yield this._fiatAccountRepository.updateBalanceAllUsers();
                var fiatAccounts = yield this._fiatAccountRepository.getInfoByUserId(userId);
                if (fiatAccounts.length <= 0) {
                    throw new customError_1.default(enums.responseCode.FiatAccountNotFound, enums.responseMessage.FiatAccountNotFound);
                }
                const fiatAccountInfo = fiatAccounts[0];
                var bankAccounts = yield this._bankAccountRepository.getInfoById(bankAccountId);
                if (bankAccounts.length <= 0) {
                    throw new customError_1.default(enums.responseCode.ReceiverNotfound, enums.responseMessage.ReceiverNotfound);
                }
                const bankAccountsInfo = bankAccounts[0];
                var exchangeRate = yield this._lqnService.getExchangeRate(transferAmount, "C", bankAccountsInfo.currency_code, "B", bankAccountsInfo.location_id, bankAccountsInfo.receiver_country_code);
                var collectAmount = exchangeRate.collect_amount; // transferAmount + service_charge
                var payoutAmount = exchangeRate.payout_amount; //
                var serviceCharge = exchangeRate.service_charge;
                if (parseFloat(fiatAccountInfo.total) - collectAmount < 0) {
                    throw new customError_1.default(enums.responseCode.InsufficientFunds, enums.responseMessage.InsufficientFunds);
                }
                var receivers = yield this._lqnUserRepository.getLqnUserByBankAccountId(bankAccountId);
                if (receivers.length <= 0) {
                    throw new customError_1.default(enums.responseCode.ReceiverNotfound, enums.responseMessage.ReceiverNotfound);
                }
                const receiverInfo = receivers[0];
                const sendTransaction = yield this._lqnService.sendTransaction(senderSourceOfFund, senderSourceOfFundRemarks, purposeOfRemittance, purposeOfRemittanceRemarks, senderBeneficiaryRelationship, senderBeneficiaryRelationshipRemarks, "P", // calcBy option ‘C’ removed in sendTransaction API. Use only calcBy as ‘P’
                payoutAmount, "USD", // Currency (FIXED!?)
                receiverInfo.sender_ref_id, receiverInfo.lqn_ref_id);
                if (sendTransaction.code !== "0") {
                    throw new customError_1.default(sendTransaction.code, sendTransaction.message);
                }
                const lqnTransactionCreated = yield this._lqnTransactionRepository.create(enums.transactionType.TRANSFER, receiverInfo.sender_ref_id, receiverInfo.lqn_ref_id, sendTransaction.agentSessionId, sendTransaction.confirmationId, sendTransaction.agentTxnId, sendTransaction.collectAmount, sendTransaction.collectCurrency, sendTransaction.serviceCharge, sendTransaction.gstCharge, sendTransaction.transferAmount, sendTransaction.exchangeRate, sendTransaction.payoutAmount, sendTransaction.payoutCurrency, sendTransaction.feeDiscount, sendTransaction.additionalPremiumRate, sendTransaction.settlementRate, sendTransaction.sendCommission, sendTransaction.settlementAmount, "", "", sendTransaction.message, "");
                const commitTransaction = yield this._lqnService.commitTransaction(sendTransaction.confirmationId);
                if (commitTransaction.code !== "0") {
                    throw new customError_1.default(commitTransaction.code, commitTransaction.message);
                }
                yield this._lqnTransactionRepository.updateConfirmTransaction(sendTransaction.confirmationId, commitTransaction.pinNumber, commitTransaction.status, "");
                var status = yield this.setLqnStatus(commitTransaction.status);
                yield this._withdrawRepository.create(yield utils.generateKey(userId.toString()), userId, enums.transactionType.TRANSFER, bankAccountId, transferAmount, serviceCharge, status, logOnId, undefined, lqnTransactionCreated[0].lqn_transaction_id);
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCryptoWalletList(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._assetInVaultAccountService.syncFireblocksAssetInVaults(enums.accountType.USER, undefined, userId);
                const vaultAccountInfo = yield this._vaultAccountService.getVaultAccount(enums.accountType.USER, undefined, userId);
                return yield this._assetInVaultAccountService.getCryptoWalletList(vaultAccountInfo.vault_account_id);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetInVaultAccountById(assetInVaultAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._assetInVaultAccountService.getAssetInVaultAccountById(assetInVaultAccountId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    createAssetInVaultAccount(userId, assetCode, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._assetInVaultAccountService.createAssetInVaultAccount(enums.accountType.USER, assetCode, logOnId, undefined, userId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionById(transactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._transactionService.getTransactionById(transactionId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    createTransaction(userId, type, assetCode, totalAssets, totalAmount, inputType, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._transactionService.createTransaction(userId, type, assetCode, totalAssets, totalAmount, inputType, false, "", "", logOnId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactions(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._transactionService.getUserTransactions(userId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTotalAssetForUser(userId, assetGroup) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var balance = 0;
                const totalAssetUser = yield this._assetInVaultAccountRepository.getSummaryAssetForUserByAssetGroup(userId, assetGroup);
                if (totalAssetUser.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                if (totalAssetUser[0].total != null) {
                    balance = totalAssetUser[0].total;
                }
                return {
                    asset_grou: assetGroup,
                    balance: balance,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserProfile(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getInfoById(userId);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                var userAddress = yield this._addressRepository.getInfoByRefId(enums.addressType.USER_PROFILE, users[0].user_id);
                var userDocuments = yield this._userDocumentRepository.getList(users[0].user_id);
                var result = yield this.setUserInfo(users[0], userAddress, userDocuments);
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    // async updateUserProfile(req: RequestUpdateCustomerProfile, userId: number) {
    //   try {
    //     var users = await this._userRepository.getInfoById(userId);
    //     if (users.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     users = await this._userRepository.update(
    //       userId,
    //       req.first_name ? req.first_name : users[0].first_name,
    //       req.middle_name ? req.middle_name : users[0].middle_name,
    //       req.last_name ? req.last_name : users[0].last_name,
    //       req.tel ? req.tel : users[0].tel,
    //       users[0].status,
    //       userId.toString(),
    //       req.gender ? req.gender : users[0].gender,
    //       req.nationality ? req.nationality : users[0].nationality,
    //       req.occupation ? req.occupation : users[0].occupation,
    //       req.occupation_remark
    //         ? req.occupation_remark
    //         : users[0].occupation_remark,
    //       req.date_of_birth ? req.date_of_birth : users[0].date_of_birth,
    //       req.nationality ? req.nationality : users[0].country_code,
    //       users[0].email
    //     );
    //     if (req.document_type !== undefined) {
    //       if (req.document_type !== "") {
    //         const isExists = await this._userDocumentRepository.getList(userId);
    //         if (isExists.length <= 0) {
    //           throw new CustomError(
    //             enums.responseCode.NotFound,
    //             enums.responseMessage.NotFound
    //           );
    //         }
    //         return await this._userDocumentRepository.updateByDocType(
    //           isExists[0].user_document_id,
    //           req.document_type,
    //           req.document_text,
    //           userId.toString(),
    //           req.document_type_remark!,
    //           req.document_issue_date!,
    //           req.document_expired_date!
    //         );
    //       }
    //     }
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // async updateUserAddress(req: RequestUpdateCustomerAddress, userId: number) {
    //   try {
    //     const userAddress = await this._addressRepository.getInfoByRefId(
    //       enums.addressType.USER_PROFILE,
    //       userId
    //     );
    //     return await this._addressRepository.update(
    //       userAddress[0].address_id,
    //       req.line1 ? req.line1 : userAddress[0].line1,
    //       req.line2 ? req.line2 : userAddress[0].line2,
    //       req.city ? req.city : userAddress[0].city,
    //       req.state ? req.state : userAddress[0].state,
    //       req.country_code ? req.country_code : userAddress[0].country_code,
    //       req.postcode ? req.postcode : userAddress[0].postcode,
    //       userId.toString()
    //     );
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    setUserInfo(userInfo, userAddress, userDocuments) {
        return __awaiter(this, void 0, void 0, function* () {
            var vAddress = {};
            if (userAddress.length > 0) {
                vAddress = {
                    address_id: userAddress[0].address_id,
                    line1: userAddress[0].line1,
                    line2: userAddress[0].line2,
                    city: userAddress[0].city,
                    state: userAddress[0].state,
                    country_code: userAddress[0].country_code,
                    country_name: userAddress[0].country_name,
                    postcode: userAddress[0].postcode,
                    area_town: userAddress[0].area_town,
                };
            }
            // var vUserDocuments = [];
            var vDocumentType = "";
            var vDocumentTypeName = "";
            var vDocumentText = "";
            var vDocumentTypeRemark = "";
            var vDocumentIssueDate = "";
            var vDocumentExpiredDate = "";
            if (userDocuments.length > 0) {
                // for (let index = 0; index < userDocuments.length; index++) {
                //   const element = userDocuments[index];
                //   vUserDocuments.push({
                //     document_type: element.document_type,
                //     document_text: element.document_text,
                //   });
                // }
                const lqnDocuments = yield this._lqnService.getCatalogue("DOC", userInfo.country_code, "", "");
                for (let index = 0; index < lqnDocuments.length; index++) {
                    const element = lqnDocuments[index];
                    if (element.value === userDocuments[0].document_type) {
                        vDocumentTypeName = element.text;
                        break;
                    }
                }
                vDocumentType = userDocuments[0].document_type;
                vDocumentText = userDocuments[0].document_text;
                vDocumentTypeRemark = userDocuments[0].document_type_remark;
                vDocumentIssueDate = userDocuments[0].document_issue_date;
                vDocumentExpiredDate = userDocuments[0].document_expired_date;
            }
            return {
                user_id: userInfo.user_id,
                first_name: userInfo.first_name,
                middle_name: userInfo.middle_name,
                last_name: userInfo.last_name,
                date_of_birth: userInfo.date_of_birth,
                email: userInfo.email,
                tel: userInfo.tel,
                country_code: userInfo.country_code,
                country_name: userInfo.country_name,
                status: userInfo.status,
                gender: userInfo.gender,
                nationality: userInfo.nationality_code,
                occupation: userInfo.occupation,
                occupation_remark: userInfo.occupation_remark,
                status_boolean: userInfo.status === "A" ? true : false,
                address: vAddress,
                // documents: vUserDocuments,
                document_type: vDocumentType,
                document_type_name: vDocumentTypeName,
                document_text: vDocumentText,
                document_type_remark: vDocumentTypeRemark,
                document_issue_date: vDocumentIssueDate,
                document_expired_date: vDocumentExpiredDate,
            };
        });
    }
    requestAccessAsset(userId, assetCode) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getInfoById(userId);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                const userInfo = users[0];
                const assets = yield this._assetRepository.getInfoByCode(assetCode);
                if (assets.length <= 0) {
                    throw new customError_1.default(enums.responseCode.AssetNotFound, enums.responseMessage.AssetNotFound);
                }
                const assetInfo = assets[0];
                return yield utils.sendMail(process.env.MAIL_FROM_ADD_ASSET, process.env.MAIL_TO_ADD_ASSET, `Request access ${assetInfo.asset_name} from ${userInfo.email}`, `
          To Customer support
          <br/><br/>
          <b>Customer</b><br/>
          Email: <b>${userInfo.email}</b><br/>
          First name: <b>${userInfo.first_name}</b><br/>
          Last name: <b>${userInfo.last_name}</b>
          <br/><br/>
          <b>Request to access</b><br/>
          Network Code: <b>${assetInfo.network_code}</b><br/>
          Network Name: <b>${assetInfo.network_name}</b><br/>
          Asset Code: <b>${assetInfo.asset_code}</b><br/>
          Asset Name: <b>${assetInfo.asset_name}</b><br/>
        `);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserProfileByEmail(email) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getInfoByEmail(email);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                return users[0];
            }
            catch (error) {
                throw error;
            }
        });
    }
    getNetworksByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const networks = yield this._networkRepository.getListByUserId(userId);
                var vResult = [];
                for (let i = 0; i < networks.length; i++) {
                    vResult.push({
                        network_id: networks[i].network_id,
                        code: networks[i].code,
                        name: networks[i].name,
                        image: networks[i].image,
                    });
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetsByUserIdNetworkId(userId, networkId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const assetsInVaultAccount = yield this._assetInVaultAccountRepository.getListByUserIdNetworkId(userId, networkId);
                var vResult = [];
                for (let i = 0; i < assetsInVaultAccount.length; i++) {
                    vResult.push({
                        asset_in_vault_account_id: assetsInVaultAccount[i].asset_in_vault_account_id,
                        network_id: assetsInVaultAccount[i].network_id,
                        network_code: assetsInVaultAccount[i].network_code,
                        network_name: assetsInVaultAccount[i].network_name,
                        network_image: assetsInVaultAccount[i].network_image,
                        asset_id: assetsInVaultAccount[i].asset_id,
                        asset_code: assetsInVaultAccount[i].asset_code,
                        asset_name: assetsInVaultAccount[i].asset_name,
                        asset_image: assetsInVaultAccount[i].asset_image,
                        asset_address: assetsInVaultAccount[i].asset_address,
                        total: assetsInVaultAccount[i].total,
                        lockedamount: assetsInVaultAccount[i].lockedamount,
                        available: assetsInVaultAccount[i].available,
                        pending: assetsInVaultAccount[i].pending,
                        total_selling_amount: assetsInVaultAccount[i].total_selling_amount,
                        buying: assetsInVaultAccount[i].buying,
                        selling: assetsInVaultAccount[i].selling,
                        status: assetsInVaultAccount[i].status,
                        updated_by: assetsInVaultAccount[i].updated_by,
                        updated_date: assetsInVaultAccount[i].updated_date,
                    });
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.CustomerService = CustomerService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [companyRepository_1.CompanyRepository,
        userRepository_1.UserRepository,
        userApiKeyRepository_1.UserApiKeyRepository,
        bankAccountRepository_1.BankAccountRepository,
        vaultAccountRepository_1.VaultAccountRepository,
        fiatAccountRepository_1.FiatAccountRepository,
        assetInVaultAccountRepository_1.AssetInVaultAccountRepository,
        withdrawRepository_1.WithdrawRepository,
        assetRepository_1.AssetRepository,
        networkRepository_1.NetworkRepository,
        assetRateRepository_1.AssetRateRepository,
        transactionRepository_1.TransactionRepository,
        fireblocksTransactionRepository_1.FireblocksTransactionRepository,
        addressRepository_1.AddressRepository,
        userDocumentRepository_1.UserDocumentRepository,
        vaultAccountService_1.VaultAccountService,
        assetInVaultAccountService_1.AssetInVaultAccountService,
        transactionService_1.TransactionService,
        fireblocksService_1.FireblocksService,
        lqnService_1.LqnService,
        countryRepository_1.CountryRepository,
        lqnWithdrawService_1.LqnWithdrawService,
        lqnUserRepository_1.LqnUserRepository,
        lqnTransactionRepository_1.LqnTransactionRepository])
], CustomerService);
//# sourceMappingURL=customerService.js.map