"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AssetInVaultAccountService = void 0;
const companyRepository_1 = require("../repositories/companyRepository");
const assetRepository_1 = require("../repositories/assetRepository");
const vaultAccountRepository_1 = require("../repositories/vaultAccountRepository");
const assetInVaultAccountRepository_1 = require("../repositories/assetInVaultAccountRepository");
const fireblocksService_1 = require("./fireblocksService");
const enums = __importStar(require("../utilities/enums"));
const customError_1 = __importDefault(require("../middlewares/customError"));
const tsyringe_1 = require("tsyringe");
let AssetInVaultAccountService = exports.AssetInVaultAccountService = class AssetInVaultAccountService {
    constructor(companyRepository, assetRepository, vaultAccountRepository, assetInVaultAccountRepository, fireblocksService) {
        this._companyRepository = companyRepository;
        this._assetRepository = assetRepository;
        this._vaultAccountRepository = vaultAccountRepository;
        this._assetInVaultAccountRepository = assetInVaultAccountRepository;
        this._fireblocksService = fireblocksService;
    }
    getAssetsInVaultAccount(vaultAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const assetInVaults = yield this._assetInVaultAccountRepository.getList(vaultAccountId);
                var vResult = [];
                for (let i = 0; i < assetInVaults.length; i++) {
                    vResult.push(yield this.setAssetInVaultAccountInfo(assetInVaults[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetInVaultAccountById(assetInVaultAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const assetInVault = yield this._assetInVaultAccountRepository.getInfoById(assetInVaultAccountId);
                if (assetInVault.length <= 0) {
                    throw new customError_1.default(enums.responseCode.AssetNotFound, enums.responseMessage.AssetNotFound);
                }
                return yield this.setAssetInVaultAccountInfo(assetInVault[0]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCryptoWalletList(vaultAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const assetInVaults = yield this._assetInVaultAccountRepository.getCryptoWalletList(vaultAccountId);
                var vResult = [];
                for (let i = 0; i < assetInVaults.length; i++) {
                    vResult.push(yield this.setAssetInVaultAccountInfo(assetInVaults[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAssetsInVaultAccountByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const assetInVaults = yield this._assetInVaultAccountRepository.getListByUserId(userId);
                var vResult = [];
                for (let i = 0; i < assetInVaults.length; i++) {
                    vResult.push(yield this.setAssetInVaultAccountInfo(assetInVaults[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setAssetInVaultAccountInfo(assetInVaultInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                asset_in_vault_account_id: assetInVaultInfo.asset_in_vault_account_id,
                network_id: assetInVaultInfo.network_id,
                network_code: assetInVaultInfo.network_code,
                network_name: assetInVaultInfo.network_name,
                network_image: assetInVaultInfo.network_image,
                asset_id: assetInVaultInfo.asset_id,
                asset_code: assetInVaultInfo.asset_code,
                asset_name: assetInVaultInfo.asset_name,
                asset_image: assetInVaultInfo.asset_image,
                asset_address: assetInVaultInfo.asset_address,
                total: assetInVaultInfo.total,
                lockedamount: assetInVaultInfo.lockedamount,
                available: assetInVaultInfo.available,
                pending: assetInVaultInfo.pending,
                total_selling_amount: assetInVaultInfo.total_selling_amount,
                buying: assetInVaultInfo.buying,
                selling: assetInVaultInfo.selling,
                status: assetInVaultInfo.status,
                updated_by: assetInVaultInfo.updated_by,
                updated_date: assetInVaultInfo.updated_date,
            };
        });
    }
    createAssetInVaultAccount(type, assetCode, logOnId, companyId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const assets = yield this._assetRepository.getInfoByCode(assetCode);
                if (assets.length <= 0) {
                    throw new customError_1.default(enums.responseCode.AssetNotFound, enums.responseMessage.AssetNotFound);
                }
                const assetInfo = assets[0];
                const vaultAccounts = yield this._vaultAccountRepository.getInfo(type, companyId, userId);
                if (vaultAccounts.length <= 0) {
                    throw new customError_1.default(enums.responseCode.VaultAccountNotFound, enums.responseMessage.VaultAccountNotFound);
                }
                const vaultAccountInfo = vaultAccounts[0];
                const assetInVaultInfo = yield this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(vaultAccountInfo.vault_account_id, assetCode);
                if (assetInVaultInfo.length > 0) {
                    if (assetInVaultInfo[0].status === enums.transactionStatus.ACTIVE ||
                        assetInVaultInfo[0].status === enums.transactionStatus.PENDING ||
                        assetInVaultInfo[0].status === enums.transactionStatus.ONHOLD) {
                        throw new customError_1.default(enums.responseCode.Duplicate, enums.responseMessage.Duplicate);
                    }
                }
                var vStatus = enums.transactionStatus.ACTIVE;
                var vStepProcess = enums.stepJobProcess.COMPLETED;
                if (assetInfo.asset_code === enums.specialAssetCode.ALGO_USDC_UV4I ||
                    assetInfo.asset_code === enums.specialAssetCode.SOL_USDC_PTHX ||
                    assetInfo.asset_code === enums.specialAssetCode.XLM_USDC_5F3T) {
                    vStatus = enums.transactionStatus.PENDING;
                    vStepProcess = enums.stepJobProcess.SUBMITTED;
                }
                var vAssetAddress = "";
                if (vStatus === enums.transactionStatus.ACTIVE) {
                    const fbResult = yield this._fireblocksService.createVaultAsset(vaultAccountInfo.fireblock_vault_account_id, assetInfo.asset_code);
                    vAssetAddress = fbResult.address;
                }
                if (assetInVaultInfo.length > 0) {
                    yield this._assetInVaultAccountRepository.updateStatus(assetInVaultInfo[0].asset_in_vault_account_id, vStatus);
                }
                else {
                    yield this._assetInVaultAccountRepository.create(vaultAccountInfo.vault_account_id, assetInfo.asset_id, assetInfo.asset_code, vAssetAddress, 0, 0, 0, 0, vStatus, vStepProcess, logOnId);
                }
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    syncFireblocksAssetAdded(FireblocksVaultAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const vaultAccounts = yield this._vaultAccountRepository.getInfoByFireblocksVaultAccountId(FireblocksVaultAccountId);
                if (vaultAccounts.length <= 0) {
                    return;
                }
                const vaultAccountInfo = vaultAccounts[0];
                yield this.syncFireblocksAssetInVaults(enums.accountType.USER, undefined, vaultAccountInfo.user_id);
            }
            catch (error) {
                throw error;
            }
        });
    }
    syncFireblocksAssetInVaults(type, companyId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const vaultAccounts = yield this._vaultAccountRepository.getInfo(type, companyId, userId);
                if (vaultAccounts.length <= 0) {
                    return;
                }
                const vaultAccountInfo = vaultAccounts[0];
                const fbVaultAccount = yield this._fireblocksService.getVaultAccountById(vaultAccountInfo.fireblock_vault_account_id);
                if (fbVaultAccount === undefined) {
                    return;
                }
                else {
                    if (fbVaultAccount.assets !== undefined) {
                        for (let index = 0; index < fbVaultAccount.assets.length; index++) {
                            const fbAsset = fbVaultAccount.assets[index];
                            const assetInVaults = yield this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(vaultAccountInfo.vault_account_id, fbAsset.id);
                            if (assetInVaults.length <= 0) {
                                const assets = yield this._assetRepository.getInfoByCodeIgnoreStatus(fbAsset.id);
                                if (assets.length <= 0) {
                                    continue;
                                }
                                const assetInfo = assets[0];
                                const fbAssetAddress = yield this.getFBAssetAddress(vaultAccountInfo.fireblock_vault_account_id, fbAsset.id);
                                yield this._assetInVaultAccountRepository.create(vaultAccountInfo.vault_account_id, assetInfo.asset_id, assetInfo.asset_code, fbAssetAddress, parseFloat(fbAsset.total), fbAsset.available === undefined
                                    ? 0
                                    : parseFloat(fbAsset.available), fbAsset.pending === undefined ? 0 : parseFloat(fbAsset.pending), fbAsset.lockedAmount === undefined
                                    ? 0
                                    : parseFloat(fbAsset.lockedAmount), assetInfo.asset_status, enums.stepJobProcess.COMPLETED, "0");
                            }
                            else {
                                const assetInVaultInfo = assetInVaults[0];
                                if (assetInVaultInfo.asset_address === "") {
                                    const fbAssetAddress = yield this.getFBAssetAddress(vaultAccountInfo.fireblock_vault_account_id, fbAsset.id);
                                    if (fbAssetAddress !== "") {
                                        yield this.updateAssetAddress(assetInVaultInfo.asset_in_vault_account_id, fbAssetAddress);
                                    }
                                }
                                yield this._assetInVaultAccountRepository.syncBalanceFromFireblocks(assetInVaultInfo.asset_in_vault_account_id, parseFloat(fbAsset.total), fbAsset.available === undefined
                                    ? 0
                                    : parseFloat(fbAsset.available), fbAsset.pending === undefined ? 0 : parseFloat(fbAsset.pending), fbAsset.lockedAmount === undefined
                                    ? 0
                                    : parseFloat(fbAsset.lockedAmount));
                            }
                        }
                    }
                }
            }
            catch (error) {
                throw error;
            }
        });
    }
    getFBAssetAddress(fbVaultAccountId, assetId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbAddress = yield this._fireblocksService.getDepositAddresses(fbVaultAccountId, assetId);
                if (fbAddress !== undefined) {
                    if (fbAddress.length > 0) {
                        if (fbAddress[0].assetId === assetId) {
                            return fbAddress[0].address;
                        }
                    }
                }
                return "";
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateAssetAddress(assetInVaultAccountId, fbAssetAddress) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._assetInVaultAccountRepository.updateAddress(assetInVaultAccountId, fbAssetAddress);
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateStepProcess(assetInVaultAccountId, stepProcess) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._assetInVaultAccountRepository.updateStepProcess(assetInVaultAccountId, stepProcess);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.AssetInVaultAccountService = AssetInVaultAccountService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [companyRepository_1.CompanyRepository,
        assetRepository_1.AssetRepository,
        vaultAccountRepository_1.VaultAccountRepository,
        assetInVaultAccountRepository_1.AssetInVaultAccountRepository,
        fireblocksService_1.FireblocksService])
], AssetInVaultAccountService);
//# sourceMappingURL=assetInVaultAccountService.js.map