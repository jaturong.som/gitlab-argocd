"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminService = void 0;
const userRepository_1 = require("../repositories/userRepository");
const userRoleRepository_1 = require("../repositories/userRoleRepository");
const addressRepository_1 = require("../repositories/addressRepository");
const userDocumentRepository_1 = require("../repositories/userDocumentRepository");
const userTokenRepository_1 = require("../repositories/userTokenRepository");
const roleRepository_1 = require("../repositories/roleRepository");
const roleMenuRepository_1 = require("../repositories/roleMenuRepository");
const menuRepository_1 = require("../repositories/menuRepository");
const groupMenuRepository_1 = require("../repositories/groupMenuRepository");
const companyRepository_1 = require("../repositories/companyRepository");
const fiatAccountRepository_1 = require("../repositories/fiatAccountRepository");
const assetRepository_1 = require("../repositories/assetRepository");
const withdrawRepository_1 = require("../repositories/withdrawRepository");
const summaryRepository_1 = require("../repositories/summaryRepository");
const enums = __importStar(require("../utilities/enums"));
const customError_1 = __importDefault(require("../middlewares/customError"));
const utils = __importStar(require("../utilities/utils"));
const tsyringe_1 = require("tsyringe");
const transactionService_1 = require("./transactionService");
const lqnWithdrawService_1 = require("./lqnWithdrawService");
const lqnUserRepository_1 = require("../repositories/lqnUserRepository");
const walletCreditRepository_1 = require("../repositories/walletCreditRepository");
const walletCreditDocumentRepository_1 = require("../repositories/walletCreditDocumentRepository");
const awsService_1 = require("./awsService");
const uuid_1 = require("uuid");
const bankAccountRepository_1 = require("../repositories/bankAccountRepository");
const countryRepository_1 = require("../repositories/countryRepository");
const customerService_1 = require("./customerService");
const cactusWalletService_1 = require("./cactusWalletService");
const cactusWalletAddressService_1 = require("./cactusWalletAddressService");
const assetInVaultAccountService_1 = require("./assetInVaultAccountService");
let AdminService = exports.AdminService = class AdminService {
    constructor(userRepository, userRoleRepository, addressRepository, userDocumentRepository, userTokenRepository, roleRepository, roleMenuRepository, menuRepository, groupMenuRepository, companyRepository, fiatAccountRepository, assetRepository, withdrawRepository, summaryRepository, cactusWalletService, cactusWalletAddressService, transactionService, lqnWithdrawService, lqnUserRepository, walletCreditRepository, walletCreditDocumentRepository, awsService, bankAccountRepository, countryRepository, customerService, assetInVaultAccountService) {
        this._userRepository = userRepository;
        this._userRoleRepository = userRoleRepository;
        this._addressRepository = addressRepository;
        this._userDocumentRepository = userDocumentRepository;
        this._userTokenRepository = userTokenRepository;
        this._roleRepository = roleRepository;
        this._roleMenuRepository = roleMenuRepository;
        this._menuRepository = menuRepository;
        this._groupMenuRepository = groupMenuRepository;
        this._companyRepository = companyRepository;
        this._fiatAccountRepository = fiatAccountRepository;
        this._assetRepository = assetRepository;
        this._withdrawRepository = withdrawRepository;
        this._summaryRepository = summaryRepository;
        this._cactusWalletService = cactusWalletService;
        this._cactusWalletAddressService = cactusWalletAddressService;
        this._transactionService = transactionService;
        this._lqnWithdrawService = lqnWithdrawService;
        this._lqnUserRepository = lqnUserRepository;
        this._walletCreditRepository = walletCreditRepository;
        this._walletCreditDocumentRepository = walletCreditDocumentRepository;
        this._awsService = awsService;
        this._bankAccountRepository = bankAccountRepository;
        this._countryRepository = countryRepository;
        this._customerService = customerService;
        this._assetInVaultAccountService = assetInVaultAccountService;
    }
    login(userName, password) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getActiveLoginInfoByUserName(userName);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UsernameOrPasswordIncorrectCase, enums.responseMessage.UsernameOrPasswordIncorrectCase);
                }
                const userInfo = users[0];
                if (!(yield utils.comparePassword(password, userInfo.password))) {
                    throw new customError_1.default(enums.responseCode.UsernameOrPasswordIncorrectCase, enums.responseMessage.UsernameOrPasswordIncorrectCase);
                }
                else {
                    var result = yield this.setUserLogin(userInfo);
                    return result;
                }
            }
            catch (error) {
                throw error;
            }
        });
    }
    refresh(refreshToken) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userAccess = yield utils.jwtVerifyRefreshToken(refreshToken);
                const obj = JSON.stringify(userAccess);
                const resRefresh = JSON.parse(obj);
                const users = yield this._userRepository.getActiveInfoById(parseInt(resRefresh.user_id));
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.Unauthorized, enums.responseMessage.Unauthorized);
                }
                const userInfo = users[0];
                var result = yield this.setUserLogin(userInfo);
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setUserLogin(userInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            const genToken = yield utils.jwtGenerateToken(userInfo.user_id, userInfo.code);
            const genRefreshToken = yield utils.jwtRefreshToken(userInfo.user_id, userInfo.code);
            const isExists = yield this._userTokenRepository.getInfo(userInfo.user_id);
            if (isExists.length <= 0) {
                yield this._userTokenRepository.create(userInfo.user_id, genToken, genRefreshToken);
            }
            else {
                yield this._userTokenRepository.update(userInfo.user_id, genToken, genRefreshToken);
            }
            return {
                user_id: userInfo.user_id,
                code: userInfo.code,
                user_name: userInfo.user_name,
                full_name: userInfo.first_name + " " + userInfo.last_name,
                token: genToken,
                refresh_token: genRefreshToken,
            };
        });
    }
    getUsers() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getList();
                var vResult = [];
                for (let i = 0; i < users.length; i++) {
                    var userAddress = yield this._addressRepository.getInfoByRefId(enums.addressType.USER_PROFILE, users[i].user_id);
                    var userDocuments = yield this._userDocumentRepository.getList(users[i].user_id);
                    vResult.push(yield this.setUserInfo(users[i], userAddress, userDocuments));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserAcriveList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getActiveList();
                return users;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserById(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getInfoById(userId);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                var userAddress = yield this._addressRepository.getInfoByRefId(enums.addressType.USER_PROFILE, users[0].user_id);
                var userDocuments = yield this._userDocumentRepository.getList(users[0].user_id);
                var result = yield this.setUserInfo(users[0], userAddress, userDocuments);
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getActiveUserById(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getActiveInfoById(userId);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                var userAddress = yield this._addressRepository.getInfoByRefId(enums.addressType.USER_PROFILE, users[0].user_id);
                var userDocuments = yield this._userDocumentRepository.getList(users[0].user_id);
                var result = yield this.setUserInfo(users[0], userAddress, userDocuments);
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setUserInfo(userInfo, userAddress, userDocuments) {
        return __awaiter(this, void 0, void 0, function* () {
            var vAddress = {};
            if (userAddress.length > 0) {
                vAddress = {
                    address_id: userAddress[0].address_id,
                    line1: userAddress[0].line1,
                    line2: userAddress[0].line2,
                    city: userAddress[0].city,
                    state: userAddress[0].state,
                    country_code: userAddress[0].country_code,
                    country_name: userAddress[0].country_name,
                    postcode: userAddress[0].postcode,
                    area_town: userAddress[0].area_town,
                };
            }
            // var vUserDocuments = [];
            var vDocumentType = "";
            var vDocumentText = "";
            var vDocumentTypeRemark = "";
            var vDocumentIssueDate = "";
            var vDocumentExpiredDate = "";
            if (userDocuments.length > 0) {
                // for (let index = 0; index < userDocuments.length; index++) {
                //   const element = userDocuments[index];
                //   vUserDocuments.push({
                //     document_type: element.document_type,
                //     document_text: element.document_text,
                //   });
                // }
                vDocumentType = userDocuments[0].document_type;
                vDocumentText = userDocuments[0].document_text;
                vDocumentTypeRemark = userDocuments[0].document_type_remark;
                vDocumentIssueDate = userDocuments[0].document_issue_date;
                vDocumentExpiredDate = userDocuments[0].document_expired_date;
            }
            return {
                user_id: userInfo.user_id,
                code: userInfo.code,
                type: userInfo.type,
                user_name: userInfo.user_name,
                password: userInfo.password,
                first_name: userInfo.first_name,
                middle_name: userInfo.middle_name,
                last_name: userInfo.last_name,
                date_of_birth: userInfo.date_of_birth,
                email: userInfo.email,
                tel: userInfo.tel,
                country_code: userInfo.country_code,
                user_id_ref: userInfo.user_id_ref,
                status: userInfo.status,
                status_boolean: userInfo.status === "A" ? true : false,
                role_id: userInfo.role_id,
                gender: userInfo.gender,
                nationality: userInfo.nationality_code,
                occupation: userInfo.occupation,
                occupation_remark: userInfo.occupation_remark,
                address: vAddress,
                // documents: vUserDocuments,
                document_type: vDocumentType,
                document_text: vDocumentText,
                document_type_remark: vDocumentTypeRemark,
                document_issue_date: vDocumentIssueDate,
                document_expired_date: vDocumentExpiredDate,
            };
        });
    }
    createUser(password, firstName, middleName, lastName, email, tel, status, roleId, logOnId, gender, nationality, occupation, dateOfBirth, address, documentType, documentText, countryCode, occupationRemark, documentTypeRemark, documentIssueDate, documentExpiredDate) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._userRepository.getLoginInfoByUserName(email);
                if (isExists.length > 0) {
                    throw new customError_1.default(enums.responseCode.Duplicate, enums.responseMessage.Duplicate);
                }
                var countries = yield this._countryRepository.getInfoByCode(address.countryCode);
                if (countries.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const countryInfo = countries[0];
                const createSender = yield this._lqnWithdrawService.CreateSender(firstName, middleName, lastName, gender, tel, nationality, documentType, documentText, "", documentIssueDate === undefined ? "" : documentIssueDate, documentExpiredDate === undefined ? "" : documentExpiredDate, dateOfBirth, occupation, email, address.line1 + " " + (address === null || address === void 0 ? void 0 : address.line2), address.city, address.state, address.postcode, address.countryCode, logOnId, 0, countryInfo.currency_code, occupationRemark, documentTypeRemark);
                yield utils.passwordValidation(password);
                const hashPassword = yield utils.hashPassword(password);
                const userCreated = yield this._userRepository.create(yield utils.generateKey(email), "G", hashPassword, firstName, middleName, lastName, email, tel, status, logOnId, gender, nationality, occupation, occupationRemark, dateOfBirth, countryCode);
                const userInfo = userCreated[0];
                yield this._lqnUserRepository.updateUserIdById(createSender[0].lqn_user_profile_id, userInfo.user_id);
                if (roleId !== 0) {
                    yield this._userRoleRepository.create(userInfo.user_id, roleId, logOnId);
                }
                if (address !== undefined) {
                    if (address.line1 !== undefined) {
                        yield this.CreateOrUpdateUserAddress(userInfo.user_id, address.line1, address.line2, address.city, address.state, address.countryCode, address.postcode, address.areaTown, logOnId);
                    }
                }
                if (documentType !== undefined) {
                    if (documentType !== "") {
                        yield this.CreateOrUpdateUserDocument(userInfo.user_id, documentType, documentText, logOnId, documentTypeRemark, documentIssueDate, documentExpiredDate);
                    }
                }
                yield this._cactusWalletService.createWalletAccount(enums.accountType.USER, logOnId, undefined, userInfo.user_id);
                yield this._fiatAccountRepository.create(enums.accountType.USER, "0", undefined, userInfo.user_id);
                var result = enums.responseMessage.Success;
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateUser(userId, firstName, middleName, lastName, tel, status, roleId, logOnId, gender, nationality, occupation, dateOfBirth, address, documentType, documentText, countryCode, occupationRemark, documentTypeRemark, documentIssueDate, documentExpiredDate) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getInfoById(userId);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                var senders = yield this._lqnUserRepository.getLqnUserByType(userId, enums.lqnUserType.SENDER);
                if (senders.length <= 0) {
                    var countries = yield this._countryRepository.getInfoByCode(address.countryCode);
                    if (countries.length <= 0) {
                        throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                    }
                    const countryInfo = countries[0];
                    const createSender = yield this._lqnWithdrawService.CreateSender(firstName, middleName, lastName, gender, tel, nationality, documentType, documentText, "", documentIssueDate === undefined ? "" : documentIssueDate, documentExpiredDate === undefined ? "" : documentExpiredDate, dateOfBirth, occupation, users[0].email, address.line1 + " " + (address === null || address === void 0 ? void 0 : address.line2), address.city, address.state, address.postcode, address.countryCode, logOnId, 0, countryInfo.currency_code, occupationRemark, documentTypeRemark);
                    yield this._lqnUserRepository.updateUserIdById(createSender[0].lqn_user_profile_id, userId);
                }
                else {
                    var isUpdate = true;
                    if (senders[0].first_name == firstName &&
                        senders[0].middle_name == middleName &&
                        senders[0].last_name == lastName) {
                        isUpdate = true;
                    }
                    else {
                        isUpdate = false;
                    }
                    yield this._lqnWithdrawService.UpdateSender(senders[0].lqn_user_profile_id, senders[0].lqn_ref_id, firstName, middleName, lastName, gender, tel, nationality, documentType, documentText, countryCode, documentIssueDate === undefined ? "" : documentIssueDate, documentExpiredDate === undefined ? "" : documentExpiredDate, dateOfBirth, occupation, senders[0].email, address.line1 + " " + (address === null || address === void 0 ? void 0 : address.line2), address.city, address.state, address.postcode, address.countryCode, logOnId, isUpdate, userId, occupationRemark === undefined ? "" : occupationRemark, documentTypeRemark === undefined ? "" : documentTypeRemark);
                    if (!isUpdate) {
                        var bankAccount = yield this._bankAccountRepository.getActiveAccountListByUserId(userId);
                        if (bankAccount.length > 0) {
                            for (let i = 0; i < bankAccount.length; i++) {
                                yield this._bankAccountRepository.inactive(bankAccount[i].bank_account_id, logOnId);
                            }
                        }
                    }
                }
                if (users[0].user_id === logOnId &&
                    users[0].status !== status &&
                    status === "I") {
                    throw new customError_1.default(enums.responseCode.ProtectOwnerAccount, enums.responseMessage.ProtectOwnerAccount);
                }
                yield this._userRepository.update(userId, firstName, middleName, lastName, tel, status, logOnId, gender, nationality, occupation, occupationRemark, dateOfBirth, countryCode, users[0].email);
                yield this._userRoleRepository.deleteByUserId(userId);
                yield this._userRoleRepository.create(userId, roleId, logOnId);
                if (address !== undefined) {
                    if (address.line1 !== undefined) {
                        yield this.CreateOrUpdateUserAddress(userId, address.line1, address.line2, address.city, address.state, address.countryCode, address.postcode, address.areaTown, logOnId);
                    }
                }
                if (documentType !== undefined) {
                    if (documentType !== "") {
                        yield this.CreateOrUpdateUserDocument(userId, documentType, documentText, logOnId, documentTypeRemark, documentIssueDate, documentExpiredDate);
                    }
                }
                var result = enums.responseMessage.Success;
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    CreateOrUpdateUserAddress(userId, line1, line2, city, state, countryCode, postcode, areaTown, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._addressRepository.getInfoByRefId(enums.addressType.USER_PROFILE, userId);
                if (isExists.length <= 0) {
                    return yield this._addressRepository.create(enums.addressType.USER_PROFILE, userId, line1, line2, city, state, countryCode, postcode, areaTown, logOnId);
                }
                return yield this._addressRepository.update(isExists[0].address_id, line1, line2, city, state, countryCode, postcode, areaTown, logOnId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    CreateOrUpdateUserDocument(userId, documentType, documentText, logOnId, documentTypeRemark, documentIssueDate, documentExpiredDate) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._userDocumentRepository.getList(userId);
                if (isExists.length <= 0) {
                    return yield this._userDocumentRepository.create(userId, documentType, documentText, logOnId, documentTypeRemark, documentIssueDate, documentExpiredDate);
                }
                return yield this._userDocumentRepository.updateByDocType(isExists[0].user_document_id, documentType, documentText, logOnId, documentTypeRemark, documentIssueDate, documentExpiredDate);
            }
            catch (error) {
                throw error;
            }
        });
    }
    deleteUser(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._userRepository.getInfoById(userId);
                if (isExists.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                yield this._userRoleRepository.deleteByUserId(userId);
                yield this._userTokenRepository.delete(userId);
                yield this._userRepository.delete(userId);
                var result = enums.responseMessage.Success;
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    changePassword(userId, old_password, new_password, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getInfoById(userId);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                const userInfo = users[0];
                if (!(yield utils.comparePassword(old_password, userInfo.password))) {
                    throw new customError_1.default(enums.responseCode.PasswordMismatch, enums.responseMessage.PasswordMismatch);
                }
                yield utils.passwordValidation(new_password);
                const hashPassword = yield utils.hashPassword(new_password);
                yield this._userRepository.changePassword(userId, hashPassword, logOnId);
                var result = yield this.setUserLogin(userInfo);
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    resetPassword(userId, new_password, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getInfoById(userId);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                const userInfo = users[0];
                yield utils.passwordValidation(new_password);
                const hashPassword = yield utils.hashPassword(new_password);
                yield this._userRepository.changePassword(userId, hashPassword, logOnId);
                var result = yield this.setUserLogin(userInfo);
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getRoles() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const roles = yield this._roleRepository.getList();
                var vResult = [];
                for (let i = 0; i < roles.length; i++) {
                    vResult.push(yield this.setRoleInfo(roles[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getActiveRoles() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const roles = yield this._roleRepository.getActiveList();
                var vResult = [];
                for (let i = 0; i < roles.length; i++) {
                    vResult.push(yield this.setRoleInfo(roles[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getRoleById(roleId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const roles = yield this._roleRepository.getInfoById(roleId);
                if (roles.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const roleInfo = roles[0];
                var result_menus = [];
                const group_menus = yield this._groupMenuRepository.getList();
                for (let i = 0; i < group_menus.length; i++) {
                    const groupMenuInfo = group_menus[i];
                    const menus = yield this._menuRepository.getListByGroupMenuCode(groupMenuInfo.group_menu_code);
                    var new_menus = [];
                    for (let j = 0; j < menus.length; j++) {
                        const menuInfo = menus[j];
                        const roleMenus1 = yield this._roleMenuRepository.getInfoByRoleIdGroupMenuCodeMenuCode(roleInfo.role_id, groupMenuInfo.group_menu_code, menuInfo.menu_code);
                        new_menus.push({
                            menu_code: menuInfo.menu_code,
                            name: menuInfo.name,
                            url: menuInfo.url,
                            component_name: menuInfo.component_name,
                            permission: roleMenus1.length > 0 ? true : false,
                        });
                    }
                    if (new_menus.length <= 0) {
                        const roleMenus2 = yield this._roleMenuRepository.getInfoByRoleIdGroupMenuCode(roleInfo.role_id, groupMenuInfo.group_menu_code);
                        result_menus.push({
                            group_menu_code: groupMenuInfo.group_menu_code,
                            name: groupMenuInfo.name,
                            url: groupMenuInfo.url,
                            component_name: groupMenuInfo.component_name,
                            permission: roleMenus2.length > 0 ? true : false,
                            menus: new_menus,
                        });
                    }
                    else {
                        result_menus.push({
                            group_menu_code: groupMenuInfo.group_menu_code,
                            name: groupMenuInfo.name,
                            url: groupMenuInfo.url,
                            component_name: groupMenuInfo.component_name,
                            permission: null,
                            menus: new_menus,
                        });
                    }
                }
                var vResult = {
                    role_id: roleInfo.role_id,
                    name: roleInfo.name,
                    description: roleInfo.description,
                    status: roleInfo.status,
                    status_boolean: roleInfo.status === "A" ? true : false,
                    menus: result_menus,
                };
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setRoleInfo(roleInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                role_id: roleInfo.role_id,
                name: roleInfo.name,
                description: roleInfo.description,
                status: roleInfo.status,
                status_boolean: roleInfo.status === "A" ? true : false,
            };
        });
    }
    createRole(name, description, status, menus, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._roleRepository.isDuplicateByName(enums.mode.INSERT, name, undefined);
                if (isExists.length > 0) {
                    throw new customError_1.default(enums.responseCode.Duplicate, enums.responseMessage.Duplicate);
                }
                const roleCreated = yield this._roleRepository.create(name, description, status, logOnId);
                const roleInfo = roleCreated[0];
                for (let i = 0; i < menus.length; i++) {
                    const menuInfo = menus[i];
                    yield this.updateRoleMenuByRoleId(roleInfo.role_id, menuInfo.group_menu_code, menuInfo.menu_code, menuInfo.permission, logOnId);
                }
                var result = enums.responseMessage.Success;
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateRole(roleId, name, description, status, menus, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._roleRepository.getInfoById(roleId);
                if (isExists.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const isDuplicate = yield this._roleRepository.isDuplicateByName(enums.mode.UPDATE, name, roleId);
                if (isDuplicate.length > 0) {
                    throw new customError_1.default(enums.responseCode.Duplicate, enums.responseMessage.Duplicate);
                }
                yield this._roleRepository.update(roleId, name, description, status, logOnId);
                if (status === "I") {
                    //Nothing
                }
                else {
                    for (let i = 0; i < menus.length; i++) {
                        const menuInfo = menus[i];
                        yield this.updateRoleMenuByRoleId(roleId, menuInfo.group_menu_code, menuInfo.menu_code, menuInfo.permission, logOnId);
                    }
                }
                var result = enums.responseMessage.Success;
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    deleteRole(roleId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._roleRepository.getInfoById(roleId);
                if (isExists.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                yield this._roleMenuRepository.deleteByRoleId(roleId);
                yield this._roleRepository.delete(roleId);
                var result = enums.responseMessage.Success;
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getMasterMenu() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var result_menus = [];
                const group_menus = yield this._groupMenuRepository.getList();
                for (let i = 0; i < group_menus.length; i++) {
                    const groupMenuInfo = group_menus[i];
                    const menus = yield this._menuRepository.getListByGroupMenuCode(groupMenuInfo.group_menu_code);
                    var new_menus = [];
                    for (let j = 0; j < menus.length; j++) {
                        const menuInfo = menus[j];
                        new_menus.push({
                            menu_code: menuInfo.menu_code,
                            name: menuInfo.name,
                            url: menuInfo.url,
                            component_name: menuInfo.component_name,
                            permission: false,
                        });
                    }
                    if (new_menus.length <= 0) {
                        result_menus.push({
                            group_menu_code: groupMenuInfo.group_menu_code,
                            name: groupMenuInfo.name,
                            url: groupMenuInfo.url,
                            component_name: groupMenuInfo.component_name,
                            permission: false,
                            menus: new_menus,
                        });
                    }
                    else {
                        result_menus.push({
                            group_menu_code: groupMenuInfo.group_menu_code,
                            name: groupMenuInfo.name,
                            url: groupMenuInfo.url,
                            component_name: groupMenuInfo.component_name,
                            permission: null,
                            menus: new_menus,
                        });
                    }
                }
                return result_menus;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getMenuByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var result_menus = [];
                var userRoles = yield this._userRoleRepository.getActiveListByUserId(userId);
                if (userRoles.length <= 0) {
                    return [];
                }
                const userRoleInfo = userRoles[0];
                const group_menus = yield this._groupMenuRepository.getListByRoleId(userRoleInfo.role_id);
                for (let i = 0; i < group_menus.length; i++) {
                    const groupMenuInfo = group_menus[i];
                    const menus = yield this._menuRepository.getListByGroupMenuCode(groupMenuInfo.group_menu_code);
                    var new_menus = [];
                    for (let j = 0; j < menus.length; j++) {
                        const menuInfo = menus[j];
                        const roleMenus = yield this._roleMenuRepository.getInfoByRoleIdGroupMenuCodeMenuCode(userRoleInfo.role_id, groupMenuInfo.group_menu_code, menuInfo.menu_code);
                        if (roleMenus.length > 0) {
                            new_menus.push({
                                menu_code: menuInfo.menu_code,
                                title: menuInfo.name,
                                path: menuInfo.url,
                                element: menuInfo.component_name,
                            });
                        }
                    }
                    result_menus.push({
                        group_menu_code: groupMenuInfo.group_menu_code,
                        title: groupMenuInfo.name,
                        path: groupMenuInfo.url,
                        element: groupMenuInfo.component_name,
                        items: new_menus,
                    });
                }
                return result_menus;
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateRoleMenuByRoleId(roleId, groupMenuCode, menuCode, permission, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (permission) {
                    var isExists = [];
                    if (menuCode != undefined) {
                        isExists =
                            yield this._roleMenuRepository.getInfoByRoleIdGroupMenuCodeMenuCode(roleId, groupMenuCode, menuCode);
                    }
                    else {
                        isExists =
                            yield this._roleMenuRepository.getInfoByRoleIdGroupMenuCode(roleId, groupMenuCode);
                    }
                    if (isExists.length <= 0) {
                        yield this._roleMenuRepository.create(roleId, groupMenuCode, menuCode, logOnId);
                    }
                }
                else {
                    if (menuCode != undefined) {
                        yield this._roleMenuRepository.deleteByRoleIdGroupMenuCodeMenuCode(roleId, groupMenuCode, menuCode);
                    }
                    else {
                        yield this._roleMenuRepository.deleteByRoleIdGroupMenuCode(roleId, groupMenuCode);
                    }
                }
                var result = enums.responseMessage.Success;
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCryptoWalletList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const companies = yield this._companyRepository.getList();
                if (companies.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                // get asset list from cactus
                // await this._assetInVaultAccountService.syncFireblocksAssetInVaults(
                //   enums.accountType.COMPANY,
                //   companies[0].company_id,
                //   undefined
                // );
                // const vaultAccountInfo = await this._vaultAccountService.getVaultAccount(
                //   enums.accountType.COMPANY,
                //   companies[0].company_id,
                //   undefined
                // );
                // const cactusWalletInfo = await this._cactusWalletRepository.getInfo(
                //   enums.accountType.COMPANY,
                //   companies[0].company_id,
                //   undefined
                // )
                // var vAssets = await this._assetInVaultAccountService.getCryptoWalletList(
                //   cactusWalletInfo.vault_account_id
                // );
                var result = {
                    company_name: companies[0].name,
                    account_name: "Lightbit",
                    // assets: vAssets,
                };
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    // async getTotalAssetPieChart(assetGroup: string) {
    //   try {
    //     await this.updateSummaryAssetBalance();
    //     const companies = await this._companyRepository.getList();
    //     if (companies.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     const summaryAssetCompany = await this._summaryRepository.getBySummaryKey(
    //       "SUM_" + assetGroup + "_COMPANY"
    //     );
    //     if (summaryAssetCompany.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     const summaryAssetCustomer =
    //       await this._summaryRepository.getBySummaryKey(
    //         "SUM_" + assetGroup + "_CUSTOMER"
    //       );
    //     if (summaryAssetCustomer.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     var vResult = [];
    //     var vCompanyAssetTotal = summaryAssetCompany[0].summary_value;
    //     var vCustomerAssetTotal = summaryAssetCustomer[0].summary_value;
    //     vResult.push({
    //       id: 1,
    //       label: "Company Wallet",
    //       value: vCompanyAssetTotal,
    //     });
    //     vResult.push({
    //       id: 2,
    //       label: "Customer Deposit",
    //       value: vCustomerAssetTotal,
    //     });
    //     var result = {
    //       asset_balance: vResult,
    //       total_balance: vCompanyAssetTotal + vCustomerAssetTotal,
    //       last_updated: summaryAssetCompany[0].updated_date,
    //     };
    //     return result;
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // async getTotalFiatPieChart() {
    //   try {
    //     await this.updateSummaryFiatBalance();
    //     var vResult = [];
    //     var vHoldTotal = 0;
    //     var vPayoutTotal = 0;
    //     const summaryFiatBalance = await this._summaryRepository.getBySummaryKey(
    //       "SUM_FIAT_BALANCE"
    //     );
    //     if (summaryFiatBalance.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     const summaryWithdrawPending =
    //       await this._summaryRepository.getBySummaryKey("SUM_WITHDRAW_PENDING");
    //     if (summaryWithdrawPending.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     vPayoutTotal = summaryWithdrawPending[0].summary_value;
    //     vHoldTotal = summaryFiatBalance[0].summary_value;
    //     vResult.push({
    //       id: 1,
    //       label: "Hold",
    //       value: vHoldTotal,
    //     });
    //     vResult.push({
    //       id: 2,
    //       label: "All Payout",
    //       value: vPayoutTotal,
    //     });
    //     var result = {
    //       fiat_balance: vResult,
    //       last_updated: summaryFiatBalance[0].updated_date,
    //     };
    //     return result;
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // async updateSummaryAssetBalance() {
    //   try {
    //     const assetGroup = "USDC";
    //     const companys = await this._companyRepository.getList();
    //     if (companys.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     var companyId = companys[0].company_id;
    //     const assetInfo = await this._assetRepository.getInfoByAssetGroup(
    //       assetGroup
    //     );
    //     if (assetInfo.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     const totalAssetCompany =
    //       await this._assetInVaultAccountRepository.getSummaryAssetForCompanyByAssetGroup(
    //         companyId,
    //         assetGroup
    //       );
    //     if (totalAssetCompany.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     var totalCom = 0;
    //     var totalCus = 0;
    //     if (totalAssetCompany[0].total != null) {
    //       totalCom = totalAssetCompany[0].total;
    //     }
    //     const updateSummaryAssetCompany =
    //       await this._summaryRepository.updateSummaryValue(
    //         "SUM_" + assetGroup + "_COMPANY",
    //         totalCom
    //       );
    //     if (updateSummaryAssetCompany.length <= 0) {
    //       const insertSummaryAssetCompany = await this._summaryRepository.create(
    //         "SUM_" + assetGroup + "_COMPANY",
    //         totalCom,
    //         "0"
    //       );
    //       if (insertSummaryAssetCompany.length <= 0) {
    //         throw new CustomError(
    //           enums.responseCode.NotFound,
    //           enums.responseMessage.NotFound
    //         );
    //       }
    //     }
    //     const totalAssetCustomer =
    //       await this._assetInVaultAccountRepository.getSummaryAssetForCustomerByAssetGroup(
    //         assetGroup
    //       );
    //     if (totalAssetCustomer.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     if (totalAssetCustomer[0].total != null) {
    //       totalCus = totalAssetCustomer[0].total;
    //     }
    //     const updateSummaryAssetCustomer =
    //       await this._summaryRepository.updateSummaryValue(
    //         "SUM_" + assetGroup + "_CUSTOMER",
    //         totalCus
    //       );
    //     if (updateSummaryAssetCustomer.length <= 0) {
    //       const insertSummaryAssetCompany = await this._summaryRepository.create(
    //         "SUM_" + assetGroup + "_CUSTOMER",
    //         totalCus,
    //         "0"
    //       );
    //       if (insertSummaryAssetCompany.length <= 0) {
    //         throw new CustomError(
    //           enums.responseCode.NotFound,
    //           enums.responseMessage.NotFound
    //         );
    //       }
    //     }
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    // async updateSummaryFiatBalance() {
    //   try {
    //     const assetGroup = "USDC";
    //     //summary withdraw
    //     const totalWithdrawPending =
    //       await this._withdrawRepository.getTotalPendingAmount();
    //     if (totalWithdrawPending.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     const updateSummaryWithdraw =
    //       await this._summaryRepository.updateSummaryValue(
    //         "SUM_WITHDRAW_PENDING",
    //         totalWithdrawPending[0].total
    //       );
    //     if (updateSummaryWithdraw.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     //summary fiat
    //     const totalFiatBalance =
    //       await this._assetInVaultAccountRepository.getSummaryFiatBalanceForCompany(
    //         assetGroup
    //       );
    //     if (totalFiatBalance.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //     const updateSummaryFiat =
    //       await this._summaryRepository.updateSummaryValue(
    //         "SUM_FIAT_BALANCE",
    //         totalFiatBalance[0].total
    //       );
    //     if (updateSummaryFiat.length <= 0) {
    //       throw new CustomError(
    //         enums.responseCode.NotFound,
    //         enums.responseMessage.NotFound
    //       );
    //     }
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    createTransaction(userId, type, assetCode, totalAssets, totalAmount, inputType, isManualMode, walletNetwork, walletAddress, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._transactionService.createTransaction(userId, type, assetCode, totalAssets, totalAmount, inputType, isManualMode, walletNetwork, walletAddress, logOnId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserAssetInVaultAccountByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield this._userRepository.getActiveInfoById(userId);
                if (users.length <= 0) {
                    throw new customError_1.default(enums.responseCode.UserNotFound, enums.responseMessage.UserNotFound);
                }
                return yield this._cactusWalletAddressService.getAssetsInVaultAccountByUserId(
                // return await this._assetInVaultAccountService.getAssetsInVaultAccountByUserId(
                userId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getFiatAccounts() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._fiatAccountRepository.updateBalanceAllUsers();
                const fiatAccounts = yield this._fiatAccountRepository.getListByAdmin();
                var vResult = [];
                for (let i = 0; i < fiatAccounts.length; i++) {
                    vResult.push(yield this.setFiatAccountInfo(fiatAccounts[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    setFiatAccountInfo(fiatAccountInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                fiat_id: fiatAccountInfo.fiat_id,
                user_id: fiatAccountInfo.user_id,
                user_full_name: fiatAccountInfo.user_full_name,
                total: fiatAccountInfo.total,
                available: fiatAccountInfo.available,
                pending: fiatAccountInfo.pending,
                lockedamount: fiatAccountInfo.lockedamount,
                is_favorite: fiatAccountInfo.is_favorite,
                is_favorite_boolean: fiatAccountInfo.is_favorite === "Y" ? true : false,
            };
        });
    }
    updateFavoriteFiatAccount(fiatId, isFavorite, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isExists = yield this._fiatAccountRepository.getInfoById(fiatId);
                if (isExists.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                yield this._fiatAccountRepository.updateFavoriteFiatAccount(fiatId, isFavorite, logOnId);
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getFiatAccountByUser(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getUserFiatAccounts(userId);
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    createTopup(fiatId, amount, fileContent, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var fiatAccounts = yield this._fiatAccountRepository.getInfoById(fiatId);
                if (fiatAccounts.length <= 0) {
                    throw new customError_1.default(enums.responseCode.FiatAccountNotFound, enums.responseMessage.FiatAccountNotFound);
                }
                const fiatAccountInfo = fiatAccounts[0];
                const createdWalletCredit = yield this._walletCreditRepository.create(yield utils.generateKey(fiatId.toString()), fiatAccountInfo.user_id, enums.transactionType.TOPUP, amount, "USD", logOnId);
                const walletCreditInfo = createdWalletCredit[0];
                for (let index = 0; index < fileContent.length; index++) {
                    const element = fileContent[index];
                    const renameFileName = `${(0, uuid_1.v4)()}-${element.file_name}`;
                    const s3Location = yield this._awsService.upload(process.env.S3_PATH, renameFileName, element.file_data);
                    yield this._walletCreditDocumentRepository.create(walletCreditInfo.wallet_credit_id, renameFileName, s3Location, logOnId);
                }
                yield this._fiatAccountRepository.updateBalanceAllUsers();
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTopupInfo(walletCreditId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const walletCredits = yield this._walletCreditRepository.getInfoById(walletCreditId);
                if (walletCredits.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const walletCreditInfo = walletCredits[0];
                var vTypeText = "";
                if (walletCreditInfo.type === enums.transactionType.TOPUP) {
                    vTypeText = "Top-up Credit";
                }
                var vStatusText = "";
                if (walletCreditInfo.status === enums.transactionStatus.SUCCESS) {
                    vStatusText = "Success";
                }
                else if (walletCreditInfo.status === enums.transactionStatus.FAILED) {
                    vStatusText = "Failed";
                }
                else if (walletCreditInfo.status === enums.transactionStatus.PENDING) {
                    vStatusText = "Pending";
                }
                else if (walletCreditInfo.status === enums.transactionStatus.CANCELLED) {
                    vStatusText = "Cancelled";
                }
                else if (walletCreditInfo.status === enums.transactionStatus.ONHOLD) {
                    vStatusText = "On hold";
                }
                const walletCreditDocuments = yield this._walletCreditDocumentRepository.getListByWalletCreditId(walletCreditId);
                var vDocuments = [];
                for (let i = 0; i < walletCreditDocuments.length; i++) {
                    let element = walletCreditDocuments[i];
                    vDocuments.push({
                        wallet_credit_document_id: element.wallet_credit_document_id,
                        file_name: element.file_name,
                        url: element.url,
                    });
                }
                return {
                    wallet_credit_id: walletCreditInfo.wallet_credit_id,
                    wallet_credit_date: walletCreditInfo.wallet_credit_date,
                    wallet_credit_no: walletCreditInfo.wallet_credit_no,
                    operator_id: walletCreditInfo.user_id,
                    operator_by: walletCreditInfo.operator_by,
                    type: walletCreditInfo.type,
                    type_text: vTypeText,
                    amount: walletCreditInfo.amount,
                    currency_code: walletCreditInfo.currency_code,
                    status: walletCreditInfo.status,
                    status_text: vStatusText,
                    documents: vDocuments,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    downloadWalletCreditDocument(walletCreditDocumentId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const walletCreditDocuments = yield this._walletCreditDocumentRepository.getInfoById(walletCreditDocumentId);
                if (walletCreditDocuments.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const walletCreditDocumentInfo = walletCreditDocuments[0];
                return yield this._awsService.download(process.env.S3_PATH, walletCreditDocumentInfo.file_name);
            }
            catch (error) {
                throw error;
            }
        });
    }
    previewWalletCreditDocument(walletCreditDocumentId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const walletCreditDocuments = yield this._walletCreditDocumentRepository.getInfoById(walletCreditDocumentId);
                if (walletCreditDocuments.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const walletCreditDocumentInfo = walletCreditDocuments[0];
                return yield this._awsService.preview(process.env.S3_PATH, walletCreditDocumentInfo.file_name);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getBankAccountsByBankType(userId, bankType) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.getBankAccountsByBankType(userId, bankType);
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    createWithdraw(senderSourceOfFund, senderSourceOfFundRemarks, purposeOfRemittance, purposeOfRemittanceRemarks, senderBeneficiaryRelationship, senderBeneficiaryRelationshipRemarks, transferAmount, //USD
    bankAccountId, userId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.createWithdraw(senderSourceOfFund, senderSourceOfFundRemarks, purposeOfRemittance, purposeOfRemittanceRemarks, senderBeneficiaryRelationship, senderBeneficiaryRelationshipRemarks, transferAmount, bankAccountId, userId, logOnId);
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
    createTransfer(senderSourceOfFund, senderSourceOfFundRemarks, purposeOfRemittance, purposeOfRemittanceRemarks, senderBeneficiaryRelationship, senderBeneficiaryRelationshipRemarks, transferAmount, bankAccountId, userId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._customerService.createTransfer(senderSourceOfFund, senderSourceOfFundRemarks, purposeOfRemittance, purposeOfRemittanceRemarks, senderBeneficiaryRelationship, senderBeneficiaryRelationshipRemarks, transferAmount, bankAccountId, userId, logOnId);
                return result;
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.AdminService = AdminService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [userRepository_1.UserRepository,
        userRoleRepository_1.UserRoleRepository,
        addressRepository_1.AddressRepository,
        userDocumentRepository_1.UserDocumentRepository,
        userTokenRepository_1.UserTokenRepository,
        roleRepository_1.RoleRepository,
        roleMenuRepository_1.RoleMenuRepository,
        menuRepository_1.MenuRepository,
        groupMenuRepository_1.GroupMenuRepository,
        companyRepository_1.CompanyRepository,
        fiatAccountRepository_1.FiatAccountRepository,
        assetRepository_1.AssetRepository,
        withdrawRepository_1.WithdrawRepository,
        summaryRepository_1.SummaryRepository,
        cactusWalletService_1.CactusWalletService,
        cactusWalletAddressService_1.CactusWalletAddressService,
        transactionService_1.TransactionService,
        lqnWithdrawService_1.LqnWithdrawService,
        lqnUserRepository_1.LqnUserRepository,
        walletCreditRepository_1.WalletCreditRepository,
        walletCreditDocumentRepository_1.WalletCreditDocumentRepository,
        awsService_1.AWSService,
        bankAccountRepository_1.BankAccountRepository,
        countryRepository_1.CountryRepository,
        customerService_1.CustomerService,
        assetInVaultAccountService_1.AssetInVaultAccountService])
], AdminService);
//# sourceMappingURL=adminService.js.map