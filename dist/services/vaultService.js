"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VaultService = void 0;
const node_vault_1 = __importDefault(require("node-vault"));
const tsyringe_1 = require("tsyringe");
let VaultService = exports.VaultService = class VaultService {
    constructor() {
        // get new instance of the client
        this._vault = (0, node_vault_1.default)({
            apiVersion: process.env.VAULT_API_VERSION,
            endpoint: process.env.VAULT_ENDPOINT,
            token: process.env.VAULT_TOKEN,
        });
    }
    getFireblocksKeyInVault() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._vault.read(process.env.VAULT_PATH + "/fireblocks");
            }
            catch (error) {
                throw error;
            }
        });
    }
    getChainalysisToken() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._vault.read(process.env.VAULT_PATH + "/chainalysis");
            }
            catch (error) {
                throw error;
            }
        });
    }
    getSMTPServer() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._vault.read(process.env.VAULT_PATH + "/smtpServer");
            }
            catch (error) {
                throw error;
            }
        });
    }
    getLQN() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._vault.read(process.env.VAULT_PATH + "/lqn");
            }
            catch (error) {
                throw error;
            }
        });
    }
    getAWS() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._vault.read(process.env.VAULT_PATH + "/aws");
            }
            catch (error) {
                throw error;
            }
        });
    }
    getCactusCustody() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._vault.read(process.env.VAULT_PATH + "/cactus");
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.VaultService = VaultService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [])
], VaultService);
//# sourceMappingURL=vaultService.js.map