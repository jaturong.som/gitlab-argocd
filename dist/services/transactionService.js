"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionService = void 0;
const companyRepository_1 = require("../repositories/companyRepository");
const assetRepository_1 = require("../repositories/assetRepository");
const assetRateRepository_1 = require("../repositories/assetRateRepository");
const transactionRepository_1 = require("../repositories/transactionRepository");
const withdrawRepository_1 = require("../repositories/withdrawRepository");
const fiatAccountRepository_1 = require("../repositories/fiatAccountRepository");
const assetInVaultAccountRepository_1 = require("../repositories/assetInVaultAccountRepository");
const fireblocksTransactionRepository_1 = require("../repositories/fireblocksTransactionRepository");
const vaultAccountRepository_1 = require("../repositories/vaultAccountRepository");
const gasConfigurationRepository_1 = require("../repositories/gasConfigurationRepository");
const httpLogRepository_1 = require("../repositories/httpLogRepository");
const walletCreditRepository_1 = require("../repositories/walletCreditRepository");
const vaultAccountService_1 = require("./vaultAccountService");
const assetInVaultAccountService_1 = require("./assetInVaultAccountService");
const fireblocksService_1 = require("./fireblocksService");
const enums = __importStar(require("../utilities/enums"));
const utils = __importStar(require("../utilities/utils"));
const customError_1 = __importDefault(require("../middlewares/customError"));
// import moment from "moment";
const fireblocks_sdk_1 = require("fireblocks-sdk");
const tsyringe_1 = require("tsyringe");
const userRepository_1 = require("../repositories/userRepository");
let TransactionService = exports.TransactionService = class TransactionService {
    constructor(companyRepository, assetRepository, assetRateRepository, transactionRepository, withdrawRepository, fiatAccountRepository, assetInVaultAccountRepository, fireblocksTransactionRepository, vaultAccountRepository, gasConfigurationRepository, httpLogRepository, vaultAccountService, assetInVaultAccountService, fireblocksService, walletCreditRepository, userRepository) {
        this._companyRepository = companyRepository;
        this._assetRepository = assetRepository;
        this._assetRateRepository = assetRateRepository;
        this._transactionRepository = transactionRepository;
        this._withdrawRepository = withdrawRepository;
        this._fiatAccountRepository = fiatAccountRepository;
        this._assetInVaultAccountRepository = assetInVaultAccountRepository;
        this._fireblocksTransactionRepository = fireblocksTransactionRepository;
        this._vaultAccountRepository = vaultAccountRepository;
        this._gasConfigurationRepository = gasConfigurationRepository;
        this._httpLogRepository = httpLogRepository;
        this._vaultAccountService = vaultAccountService;
        this._assetInVaultAccountService = assetInVaultAccountService;
        this._fireblocksService = fireblocksService;
        this._walletCreditRepository = walletCreditRepository;
        this._userRepository = userRepository;
    }
    getAdminTransactions() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var vResult = [];
                const transactions = yield this._transactionRepository.getList();
                for (let i = 0; i < transactions.length; i++) {
                    vResult.push(yield this.setTransactionInfo(transactions[i]));
                }
                const withdraws = yield this._withdrawRepository.getList();
                for (let i = 0; i < withdraws.length; i++) {
                    vResult.push(yield this.setWithdrawInfo(withdraws[i]));
                }
                const topups = yield this._walletCreditRepository.getList();
                for (let i = 0; i < topups.length; i++) {
                    vResult.push(yield this.setTopupInfo(topups[i]));
                }
                if (vResult.length > 0) {
                    vResult.sort((a, b) => {
                        return b.transaction_date - a.transaction_date;
                    });
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getUserTransactions(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var vResult = [];
                const transactions = yield this._transactionRepository.getListByUserId(userId);
                for (let i = 0; i < transactions.length; i++) {
                    vResult.push(yield this.setTransactionInfo(transactions[i]));
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionById(transactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const transactions = yield this._transactionRepository.getInfoById(transactionId);
                if (transactions.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                return yield this.setTransactionInfo(transactions[0]);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWithdrawTransactionsByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var vResult = [];
                const withdraws = yield this._withdrawRepository.getListByUserId(userId);
                for (let i = 0; i < withdraws.length; i++) {
                    vResult.push(yield this.setWithdrawInfoByUserId(withdraws[i]));
                }
                const topups = yield this._walletCreditRepository.getListByUserId(userId);
                for (let i = 0; i < topups.length; i++) {
                    vResult.push(yield this.setTopupInfoByUserId(topups[i]));
                }
                if (vResult.length > 0) {
                    vResult.sort((a, b) => {
                        return b.withdraw_date - a.withdraw_date;
                    });
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWithdrawById(withdrawId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const withdraws = yield this._withdrawRepository.getInfoById(withdrawId);
                if (withdraws.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const withdraw = withdraws[0];
                var lqnStatus = withdraw.lqn_transaction_status;
                if (lqnStatus != null) {
                    lqnStatus = lqnStatus.toUpperCase();
                }
                var vLqnStatusText = "";
                if (lqnStatus == enums.lqnTransactionStatus.UN_COMMIT_HOLD) {
                    vLqnStatusText = enums.lqnTransactionStatusText.UN_COMMIT_HOLD;
                }
                else if (lqnStatus == enums.lqnTransactionStatus.UN_COMMIT_COMPLIANCE) {
                    vLqnStatusText = enums.lqnTransactionStatusText.UN_COMMIT_COMPLIANCE;
                }
                else if (lqnStatus == enums.lqnTransactionStatus.COMPLIANCE) {
                    vLqnStatusText = enums.lqnTransactionStatusText.COMPLIANCE;
                }
                else if (lqnStatus == enums.lqnTransactionStatus.SANCTION) {
                    vLqnStatusText = enums.lqnTransactionStatusText.SANCTION;
                }
                else if (lqnStatus == enums.lqnTransactionStatus.HOLD) {
                    vLqnStatusText = enums.lqnTransactionStatusText.HOLD;
                }
                else if (lqnStatus == enums.lqnTransactionStatus.UN_PAID) {
                    vLqnStatusText = enums.lqnTransactionStatusText.UN_PAID;
                }
                else if (lqnStatus == enums.lqnTransactionStatus.POST) {
                    vLqnStatusText = enums.lqnTransactionStatusText.POST;
                }
                else if (lqnStatus == enums.lqnTransactionStatus.API_PROCESSING) {
                    vLqnStatusText = enums.lqnTransactionStatusText.API_PROCESSING;
                }
                else if (lqnStatus == enums.lqnTransactionStatus.PAID) {
                    vLqnStatusText = enums.lqnTransactionStatusText.PAID;
                }
                else if (lqnStatus == enums.lqnTransactionStatus.CANCEL_HOLD) {
                    vLqnStatusText = enums.lqnTransactionStatusText.CANCEL_HOLD;
                }
                else if (lqnStatus == enums.lqnTransactionStatus.BLOCK) {
                    vLqnStatusText = enums.lqnTransactionStatusText.BLOCK;
                }
                else if (lqnStatus == enums.lqnTransactionStatus.CANCEL) {
                    vLqnStatusText = enums.lqnTransactionStatusText.CANCEL;
                }
                else {
                    vLqnStatusText = lqnStatus;
                }
                var vTypeText = "";
                if (withdraw.type === enums.transactionType.WITHDRAW) {
                    vTypeText = "Withdraw";
                }
                else if (withdraw.type === enums.transactionType.TRANSFER) {
                    vTypeText = "Transfer";
                }
                var vStatusText = "";
                if (withdraw.status === enums.transactionStatus.SUCCESS) {
                    vStatusText = "Success";
                }
                else if (withdraw.status === enums.transactionStatus.FAILED) {
                    vStatusText = "Failed";
                }
                else if (withdraw.status === enums.transactionStatus.PENDING) {
                    vStatusText = "Pending";
                }
                else if (withdraw.status === enums.transactionStatus.CANCELLED) {
                    vStatusText = "Cancelled";
                }
                else if (withdraw.status === enums.transactionStatus.ONHOLD) {
                    vStatusText = "On hold";
                }
                return {
                    withdraw_id: withdraw.withdraw_id,
                    withdraw_date: withdraw.withdraw_date,
                    withdraw_no: withdraw.withdraw_no,
                    type: withdraw.type,
                    type_text: vTypeText,
                    completion_date: withdraw.completion_date,
                    status: withdraw.status,
                    status_text: vStatusText,
                    sender_first_name: withdraw.sender_first_name,
                    sender_middle_name: withdraw.sender_middle_name,
                    sender_last_name: withdraw.sender_last_name,
                    sender_contact_number: withdraw.sender_contact_number,
                    sender_email: withdraw.sender_email,
                    sender_address: withdraw.sender_address,
                    sender_state: withdraw.sender_state,
                    sender_area_town: withdraw.sender_area_town,
                    sender_city: withdraw.sender_city,
                    sender_zip_code: withdraw.sender_zip_code,
                    sender_country_code: withdraw.sender_country_code,
                    sender_country_name: withdraw.sender_country_name,
                    receiver_first_name: withdraw.receiver_first_name,
                    receiver_middle_name: withdraw.receiver_middle_name,
                    receiver_last_name: withdraw.receiver_last_name,
                    receiver_date_of_birth: withdraw.receiver_date_of_birth,
                    receiver_gender: withdraw.receiver_gender,
                    receiver_contact_number: withdraw.receiver_contact_number,
                    receiver_email: withdraw.receiver_email,
                    receiver_nationality: withdraw.receiver_nationality,
                    receiver_occupation: withdraw.receiver_occupation,
                    receiver_occupation_remarks: withdraw.receiver_occupation_remarks,
                    receiver_id_type: withdraw.receiver_id_type,
                    receiver_id_type_remarks: withdraw.receiver_id_type_remarks,
                    receiver_id_number: withdraw.receiver_id_number,
                    receiver_id_issue_date: withdraw.receiver_id_issue_date,
                    receiver_id_expire_date: withdraw.receiver_id_expire_date,
                    receiver_native_first_name: withdraw.receiver_native_first_name,
                    receiver_native_middle_name: withdraw.receiver_native_middle_name,
                    receiver_native_last_name: withdraw.receiver_native_last_name,
                    receiver_native_address: withdraw.receiver_native_address,
                    receiver_account_type: withdraw.receiver_account_type,
                    receiver_district: withdraw.receiver_district,
                    beneficiary_type: withdraw.beneficiary_type,
                    currency_code: withdraw.currency_code,
                    receiver_address: withdraw.receiver_address,
                    receiver_state: withdraw.receiver_state,
                    receiver_area_town: withdraw.receiver_area_town,
                    receiver_city: withdraw.receiver_city,
                    receiver_zip_code: withdraw.receiver_zip_code,
                    receiver_country_code: withdraw.receiver_country_code,
                    receiver_country_name: withdraw.receiver_country_name,
                    bank_account_id: withdraw.bank_account_id,
                    bank_type: withdraw.bank_type,
                    location_id: withdraw.location_id,
                    bank_name: withdraw.bank_name,
                    bank_branch_name: withdraw.bank_branch_name,
                    bank_branch_code: withdraw.bank_branch_code,
                    bank_account_number: withdraw.bank_account_number,
                    bank_account_name: withdraw.bank_account_name,
                    swift_code: withdraw.swift_code,
                    iban: withdraw.iban,
                    lqn_transaction_id: withdraw.lqn_transaction_id,
                    lqn_transaction_date: withdraw.lqn_transaction_date,
                    lqn_transaction_type: withdraw.lqn_transaction_type,
                    customer_id: withdraw.customer_id,
                    receiver_id: withdraw.receiver_id,
                    agent_session_id: withdraw.agent_session_id,
                    confirmation_id: withdraw.confirmation_id,
                    agent_txn_id: withdraw.agent_txn_id,
                    collect_amount: withdraw.collect_amount,
                    collect_currency: withdraw.collect_currency,
                    service_charge: withdraw.service_charge,
                    gst_charge: withdraw.gst_charge,
                    transfer_amount: withdraw.transfer_amount,
                    exchange_rate: withdraw.exchange_rate,
                    payout_amount: withdraw.payout_amount,
                    payout_currency: withdraw.payout_currency,
                    fee_discount: withdraw.fee_discount,
                    additional_premium_rate: withdraw.additional_premium_rate,
                    settlement_rate: withdraw.settlement_rate,
                    send_commission: withdraw.send_commission,
                    settlement_amount: withdraw.settlement_amount,
                    pin_number: withdraw.pin_number,
                    lqn_status: lqnStatus,
                    lqn_status_text: vLqnStatusText,
                    message: withdraw.message,
                    additionalmessage: withdraw.additionalmessage,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getConvertById(withdrawId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const withdraws = yield this._withdrawRepository.getConvertInfoById(withdrawId);
                if (withdraws.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const withdraw = withdraws[0];
                var vTypeText = "";
                if (withdraw.type === enums.transactionType.CONVERT) {
                    vTypeText = "Convert to Crypto";
                }
                var vStatusText = "";
                if (withdraw.status === enums.transactionStatus.SUCCESS) {
                    vStatusText = "Success";
                }
                else if (withdraw.status === enums.transactionStatus.FAILED) {
                    vStatusText = "Failed";
                }
                else if (withdraw.status === enums.transactionStatus.PENDING) {
                    vStatusText = "Pending";
                }
                else if (withdraw.status === enums.transactionStatus.CANCELLED) {
                    vStatusText = "Cancelled";
                }
                else if (withdraw.status === enums.transactionStatus.ONHOLD) {
                    vStatusText = "On hold";
                }
                return {
                    wallet_credit_id: withdraw.withdraw_id,
                    wallet_credit_date: withdraw.withdraw_date,
                    wallet_credit_no: withdraw.withdraw_no,
                    operator_id: withdraw.user_id,
                    operator_by: withdraw.operator_by,
                    type: withdraw.type,
                    type_text: vTypeText,
                    amount: withdraw.total_amount,
                    total_assets: withdraw.total_assets,
                    currency_code: withdraw.currency_code,
                    status: withdraw.status,
                    status_text: vStatusText,
                    documents: [],
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    setTransactionInfo(transactionInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            var vTypeText = "";
            if (transactionInfo.type === enums.transactionType.BUYING) {
                vTypeText = "Buying";
            }
            else if (transactionInfo.type === enums.transactionType.SELLING) {
                vTypeText = "Selling";
            }
            var vStatusText = "";
            if (transactionInfo.status === enums.transactionStatus.SUCCESS) {
                vStatusText = "Success";
            }
            else if (transactionInfo.status === enums.transactionStatus.FAILED) {
                vStatusText = "Failed";
            }
            else if (transactionInfo.status === enums.transactionStatus.PENDING) {
                vStatusText = "Pending";
            }
            else if (transactionInfo.status === enums.transactionStatus.CANCELLED) {
                vStatusText = "Cancelled";
            }
            else if (transactionInfo.status === enums.transactionStatus.ONHOLD) {
                vStatusText = "On hold";
            }
            return {
                transaction_id: transactionInfo.transaction_id,
                transaction_date: transactionInfo.transaction_date,
                transaction_no: transactionInfo.transaction_no,
                fb_transaction_hash: transactionInfo.fb_transaction_hash,
                fb_transaction_status: transactionInfo.fb_transaction_status,
                user_id: transactionInfo.user_id,
                type: transactionInfo.type,
                type_text: vTypeText,
                source_vault_account_id: transactionInfo.source_vault_account_id,
                destination_vault_account_id: transactionInfo.destination_vault_account_id,
                buying: transactionInfo.buying,
                selling: transactionInfo.selling,
                customer_name: transactionInfo.customer_name,
                network_id: transactionInfo.network_id,
                network_code: transactionInfo.network_code,
                network_name: transactionInfo.network_name,
                network_image: transactionInfo.network_image,
                asset_id: transactionInfo.asset_id,
                asset_code: transactionInfo.asset_code,
                asset_name: transactionInfo.asset_name,
                asset_image: transactionInfo.asset_image,
                total_amount: transactionInfo.total_amount,
                total_assets: transactionInfo.total_assets,
                currency_code: transactionInfo.currency_code,
                status: transactionInfo.status,
                status_text: vStatusText,
                bank_account_id: 0,
            };
        });
    }
    //admin
    setWithdrawInfo(withdrawInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            var vTypeText = "";
            if (withdrawInfo.type === enums.transactionType.WITHDRAW) {
                vTypeText = "Withdraw";
            }
            else if (withdrawInfo.type === enums.transactionType.TRANSFER) {
                vTypeText = "Transfer";
            }
            else if (withdrawInfo.type === enums.transactionType.CONVERT) {
                vTypeText = "Convert";
            }
            else if (withdrawInfo.type === enums.transactionType.TOPUP) {
                vTypeText = "Topup";
            }
            var lqnStatus = withdrawInfo.lqn_status;
            if (withdrawInfo.lqn_status != null) {
                lqnStatus = withdrawInfo.lqn_status.toUpperCase();
            }
            var vLqnStatusText = "";
            if (lqnStatus == enums.lqnTransactionStatus.UN_COMMIT_HOLD) {
                vLqnStatusText = enums.lqnTransactionStatusText.UN_COMMIT_HOLD;
            }
            else if (lqnStatus == enums.lqnTransactionStatus.UN_COMMIT_COMPLIANCE) {
                vLqnStatusText = enums.lqnTransactionStatusText.UN_COMMIT_COMPLIANCE;
            }
            else if (lqnStatus == enums.lqnTransactionStatus.COMPLIANCE) {
                vLqnStatusText = enums.lqnTransactionStatusText.COMPLIANCE;
            }
            else if (lqnStatus == enums.lqnTransactionStatus.SANCTION) {
                vLqnStatusText = enums.lqnTransactionStatusText.SANCTION;
            }
            else if (lqnStatus == enums.lqnTransactionStatus.HOLD) {
                vLqnStatusText = enums.lqnTransactionStatusText.HOLD;
            }
            else if (lqnStatus == enums.lqnTransactionStatus.UN_PAID) {
                vLqnStatusText = enums.lqnTransactionStatusText.UN_PAID;
            }
            else if (lqnStatus == enums.lqnTransactionStatus.POST) {
                vLqnStatusText = enums.lqnTransactionStatusText.POST;
            }
            else if (lqnStatus == enums.lqnTransactionStatus.API_PROCESSING) {
                vLqnStatusText = enums.lqnTransactionStatusText.API_PROCESSING;
            }
            else if (lqnStatus == enums.lqnTransactionStatus.PAID) {
                vLqnStatusText = enums.lqnTransactionStatusText.PAID;
            }
            else if (lqnStatus == enums.lqnTransactionStatus.CANCEL_HOLD) {
                vLqnStatusText = enums.lqnTransactionStatusText.CANCEL_HOLD;
            }
            else if (lqnStatus == enums.lqnTransactionStatus.BLOCK) {
                vLqnStatusText = enums.lqnTransactionStatusText.BLOCK;
            }
            else if (lqnStatus == enums.lqnTransactionStatus.CANCEL) {
                vLqnStatusText = enums.lqnTransactionStatusText.CANCEL;
            }
            else {
                vLqnStatusText = lqnStatus;
            }
            var vStatusText = "";
            if (withdrawInfo.status === enums.transactionStatus.SUCCESS) {
                vStatusText = "Success";
            }
            else if (withdrawInfo.status === enums.transactionStatus.FAILED) {
                vStatusText = "Failed";
            }
            else if (withdrawInfo.status === enums.transactionStatus.PENDING) {
                vStatusText = "Pending";
            }
            else if (withdrawInfo.status === enums.transactionStatus.CANCELLED) {
                vStatusText = "Cancelled";
            }
            else if (withdrawInfo.status === enums.transactionStatus.ONHOLD) {
                vStatusText = "On hold";
            }
            return {
                transaction_id: withdrawInfo.withdraw_id,
                transaction_date: withdrawInfo.withdraw_date,
                transaction_no: withdrawInfo.withdraw_no,
                fb_transaction_hash: null,
                fb_transaction_status: null,
                user_id: withdrawInfo.user_id,
                type: withdrawInfo.type,
                type_text: vTypeText,
                source_vault_account_id: null,
                destination_vault_account_id: null,
                buying: null,
                selling: null,
                customer_name: withdrawInfo.customer_name,
                network_id: null,
                network_code: null,
                network_name: null,
                network_image: null,
                asset_id: null,
                asset_code: null,
                asset_name: null,
                asset_image: null,
                amount: Number(withdrawInfo.transfer_amount),
                total_amount: Number(withdrawInfo.collect_amount),
                total_assets: null,
                currency_code: withdrawInfo.currency_code,
                purpose_of_transfer: withdrawInfo.purpose_of_transfer,
                purpose_of_transfer_other: withdrawInfo.purpose_of_transfer_other,
                relationship_to_receiver: withdrawInfo.relationship_to_receiver,
                relationship_to_receiver_other: withdrawInfo.relationship_to_receiver_other,
                status: withdrawInfo.status,
                status_text: vStatusText,
                lqn_status: lqnStatus,
                lqn_status_text: vLqnStatusText,
                bank_account_id: withdrawInfo.bank_account_id,
            };
        });
    }
    setWithdrawInfoByUserId(withdrawInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            var vTypeText = "";
            if (withdrawInfo.type === enums.transactionType.WITHDRAW) {
                vTypeText = "Withdraw";
            }
            else if (withdrawInfo.type === enums.transactionType.TRANSFER) {
                vTypeText = "Transfer";
            }
            else if (withdrawInfo.type === enums.transactionType.CONVERT) {
                vTypeText = "Convert";
            }
            else if (withdrawInfo.type === enums.transactionType.TOPUP) {
                vTypeText = "Topup";
            }
            var vStatusText = "";
            if (withdrawInfo.status === enums.transactionStatus.SUCCESS) {
                vStatusText = "Success";
            }
            else if (withdrawInfo.status === enums.transactionStatus.FAILED) {
                vStatusText = "Failed";
            }
            else if (withdrawInfo.status === enums.transactionStatus.PENDING) {
                vStatusText = "Pending";
            }
            else if (withdrawInfo.status === enums.transactionStatus.CANCELLED) {
                vStatusText = "Cancel";
            }
            else if (withdrawInfo.status === enums.transactionStatus.ONHOLD) {
                vStatusText = "Hold";
            }
            return {
                withdraw_id: withdrawInfo.withdraw_id,
                withdraw_date: withdrawInfo.withdraw_date,
                withdraw_no: withdrawInfo.withdraw_no,
                type: withdrawInfo.type,
                type_text: vTypeText,
                amount: withdrawInfo.amount,
                currency_code: withdrawInfo.currency_code,
                purpose_of_transfer: withdrawInfo.purpose_of_transfer,
                purpose_of_transfer_other: withdrawInfo.purpose_of_transfer_other,
                relationship_to_receiver: withdrawInfo.relationship_to_receiver,
                relationship_to_receiver_other: withdrawInfo.relationship_to_receiver_other,
                status: withdrawInfo.status,
                status_text: vStatusText,
                completion_date: withdrawInfo.completion_date,
                bank_id: withdrawInfo.bank_id,
                bank_code: withdrawInfo.bank_code,
                bank_name: withdrawInfo.bank_name,
                account_no: withdrawInfo.account_no,
                account_name: withdrawInfo.account_name,
                swift_code: withdrawInfo.swift_code,
                iban: withdrawInfo.iban,
                transfer_amount: withdrawInfo.transfer_amount,
                lqn_status: withdrawInfo.lqn_status,
                collect_amount: withdrawInfo.collect_amount,
            };
        });
    }
    setTopupInfo(walletCreditInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            var vTypeText = "";
            if (walletCreditInfo.type === enums.transactionType.TOPUP) {
                vTypeText = "Top-Up";
            }
            else if (walletCreditInfo.type === enums.transactionType.CONVERT) {
                vTypeText = "Convert";
            }
            var vStatusText = "";
            if (walletCreditInfo.status === enums.transactionStatus.SUCCESS) {
                vStatusText = "Success";
            }
            else if (walletCreditInfo.status === enums.transactionStatus.FAILED) {
                vStatusText = "Failed";
            }
            else if (walletCreditInfo.status === enums.transactionStatus.PENDING) {
                vStatusText = "Pending";
            }
            else if (walletCreditInfo.status === enums.transactionStatus.CANCELLED) {
                vStatusText = "Cancelled";
            }
            else if (walletCreditInfo.status === enums.transactionStatus.ONHOLD) {
                vStatusText = "On hold";
            }
            return {
                transaction_id: walletCreditInfo.wallet_credit_id,
                transaction_date: walletCreditInfo.wallet_credit_date,
                transaction_no: walletCreditInfo.wallet_credit_no,
                fb_transaction_hash: null,
                fb_transaction_status: null,
                user_id: walletCreditInfo.user_id,
                type: walletCreditInfo.type,
                type_text: vTypeText,
                source_vault_account_id: null,
                destination_vault_account_id: null,
                buying: null,
                selling: null,
                customer_name: walletCreditInfo.customer_name,
                network_id: null,
                network_code: null,
                network_name: null,
                network_image: null,
                asset_id: null,
                asset_code: null,
                asset_name: null,
                asset_image: null,
                total_amount: walletCreditInfo.amount,
                total_assets: null,
                currency_code: walletCreditInfo.currency_code,
                purpose_of_transfer: null,
                purpose_of_transfer_other: null,
                relationship_to_receiver: null,
                relationship_to_receiver_other: null,
                status: walletCreditInfo.status,
                status_text: vStatusText,
                bank_account_id: null,
            };
        });
    }
    setTopupInfoByUserId(walletCreditInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            var vTypeText = "";
            if (walletCreditInfo.type === enums.transactionType.TOPUP) {
                vTypeText = "Top-Up";
            }
            else if (walletCreditInfo.type === enums.transactionType.CONVERT) {
                vTypeText = "Convert";
            }
            var vStatusText = "";
            if (walletCreditInfo.status === enums.transactionStatus.SUCCESS) {
                vStatusText = "Success";
            }
            else if (walletCreditInfo.status === enums.transactionStatus.FAILED) {
                vStatusText = "Failed";
            }
            else if (walletCreditInfo.status === enums.transactionStatus.PENDING) {
                vStatusText = "Pending";
            }
            else if (walletCreditInfo.status === enums.transactionStatus.CANCELLED) {
                vStatusText = "Cancelled";
            }
            else if (walletCreditInfo.status === enums.transactionStatus.ONHOLD) {
                vStatusText = "On hold";
            }
            return {
                withdraw_id: walletCreditInfo.wallet_credit_id,
                withdraw_date: walletCreditInfo.wallet_credit_date,
                withdraw_no: walletCreditInfo.wallet_credit_no,
                type: walletCreditInfo.type,
                type_text: vTypeText,
                amount: walletCreditInfo.amount,
                currency_code: walletCreditInfo.currency_code,
                purpose_of_transfer: null,
                purpose_of_transfer_other: null,
                relationship_to_receiver: null,
                relationship_to_receiver_other: null,
                status: walletCreditInfo.status,
                status_text: vStatusText,
                completion_date: null,
                bank_id: null,
                bank_code: null,
                bank_name: null,
                account_no: null,
                account_name: null,
                swift_code: null,
                iban: null,
                transfer_amount: walletCreditInfo.amount,
                lqn_status: null,
                collect_amount: walletCreditInfo.amount,
            };
        });
    }
    createTransaction(userId, type, assetCode, totalAssets, totalAmount, inputType, isManualMode, walletNetwork, walletAddress, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._assetInVaultAccountService.syncFireblocksAssetInVaults(enums.accountType.USER, undefined, userId);
                const companies = yield this._companyRepository.getList();
                if (companies.length <= 0) {
                    throw new customError_1.default(enums.responseCode.CompanyNotFound, enums.responseMessage.CompanyNotFound);
                }
                const companyInfo = companies[0];
                const assets = yield this._assetRepository.getInfoByCode(assetCode);
                if (assets.length <= 0) {
                    throw new customError_1.default(enums.responseCode.AssetNotFound, enums.responseMessage.AssetNotFound);
                }
                const assetInfo = assets[0];
                let sourceVaultAccountInfo;
                let destinationVaultAccountInfo;
                if (type === enums.transactionType.BUYING) {
                    sourceVaultAccountInfo =
                        yield this._vaultAccountService.getVaultAccount(enums.accountType.COMPANY, companyInfo.company_id, undefined);
                    destinationVaultAccountInfo =
                        yield this._vaultAccountService.getVaultAccount(enums.accountType.USER, undefined, userId);
                }
                else {
                    sourceVaultAccountInfo =
                        yield this._vaultAccountService.getVaultAccount(enums.accountType.USER, undefined, userId);
                    destinationVaultAccountInfo =
                        yield this._vaultAccountService.getVaultAccount(enums.accountType.COMPANY, companyInfo.company_id, undefined);
                }
                const assetInVaults = yield this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(sourceVaultAccountInfo.vault_account_id, assetCode);
                if (assetInVaults.length <= 0) {
                    throw new customError_1.default(enums.responseCode.AssetNotFound, enums.responseMessage.AssetNotFound);
                }
                const assetInVaultInfo = assetInVaults[0];
                var calculateTotalAsset = 0;
                var calculateTotalAmount = 0;
                var vBuyingRate = 0;
                var vSellingRate = 0;
                if (isManualMode) {
                    calculateTotalAsset = totalAssets;
                    calculateTotalAmount = totalAmount;
                    vBuyingRate =
                        totalAssets > 0
                            ? parseFloat((totalAmount / totalAssets).toFixed(10))
                            : 0;
                    vSellingRate =
                        totalAssets > 0
                            ? parseFloat((totalAmount / totalAssets).toFixed(10))
                            : 0;
                }
                else {
                    const assetRates = yield this._assetRateRepository.getInfoByAssetCode(assetCode);
                    if (assetRates.length <= 0) {
                        throw new customError_1.default(enums.responseCode.AssetRateNotFound, enums.responseMessage.AssetRateNotFound);
                    }
                    const assetRateInfo = assetRates[0];
                    vBuyingRate = assetRateInfo.buying;
                    vSellingRate = assetRateInfo.selling;
                    if (inputType === enums.inputType.AMOUNT) {
                        calculateTotalAmount = totalAmount;
                        if (type === enums.transactionType.SELLING) {
                            calculateTotalAsset = parseFloat((totalAmount / assetRateInfo.buying).toFixed(10));
                        }
                        else {
                            calculateTotalAsset = parseFloat((totalAmount / assetRateInfo.selling).toFixed(10));
                        }
                    }
                    else {
                        calculateTotalAsset = totalAssets;
                        if (type === enums.transactionType.SELLING) {
                            calculateTotalAmount = parseFloat((totalAssets * assetRateInfo.buying).toFixed(10));
                        }
                        else {
                            calculateTotalAmount = parseFloat((totalAssets * assetRateInfo.selling).toFixed(10));
                        }
                    }
                }
                var vStatus = enums.transactionStatus.PENDING;
                if (walletNetwork === "WLN99") {
                    vStatus = enums.transactionStatus.SUCCESS;
                }
                else {
                    const assetBalances = yield this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCode(sourceVaultAccountInfo.vault_account_id, assetInVaultInfo.asset_code);
                    if (assetBalances.length <= 0) {
                        throw new customError_1.default(enums.responseCode.InsufficientAssets, enums.responseMessage.InsufficientAssets);
                    }
                    if (assetBalances[0].total < calculateTotalAsset) {
                        throw new customError_1.default(enums.responseCode.InsufficientAssets, enums.responseMessage.InsufficientAssets);
                    }
                }
                if (type === enums.transactionType.BUYING) {
                    yield this._fiatAccountRepository.updateBalanceAllUsers();
                    var fiatAccounts = yield this._fiatAccountRepository.getInfoByUserId(userId);
                    if (fiatAccounts.length <= 0) {
                        throw new customError_1.default(enums.responseCode.FiatAccountNotFound, enums.responseMessage.FiatAccountNotFound);
                    }
                    const fiatAccountInfo = fiatAccounts[0];
                    if (parseFloat(fiatAccountInfo.total) - calculateTotalAmount < 0) {
                        throw new customError_1.default(enums.responseCode.InsufficientFunds, enums.responseMessage.InsufficientFunds);
                    }
                }
                const transactions = yield this._transactionRepository.create(userId, yield utils.generateKey(userId.toString()), type, sourceVaultAccountInfo.vault_account_id, assetInfo.asset_id, assetInVaultInfo.asset_code, vBuyingRate, vSellingRate, calculateTotalAmount, calculateTotalAsset, vStatus, logOnId, enums.source.PORTAL, isManualMode, walletNetwork, walletAddress, destinationVaultAccountInfo.vault_account_id, undefined);
                const transaction = transactions[0];
                if (type === enums.transactionType.BUYING && walletNetwork !== "WLN99") {
                    yield this._withdrawRepository.create(yield utils.generateKey(transaction.transaction_id), userId, enums.transactionType.CONVERT, 0, calculateTotalAmount, 0, enums.transactionStatus.SUCCESS, logOnId, transaction.transaction_id, undefined);
                }
                return enums.responseMessage.Success;
            }
            catch (error) {
                throw error;
            }
        });
    }
    // async getAdminTransactionLogs(
    //   pageSize: number,
    //   pageCursor: number,
    //   fromDate: string,
    //   toDate: string,
    //   transactionType: string,
    //   status: string,
    //   assetName: string
    // ) {
    //   try {
    //     const transactions = await this._transactionRepository.getTransactionLogs(
    //       pageSize,
    //       pageCursor,
    //       fromDate,
    //       toDate,
    //       transactionType,
    //       status,
    //       assetName
    //     );
    //     var vResult = [];
    //     for (let i = 0; i < transactions.length; i++) {
    //       var createdDate = new Date(transactions[i].created_date);
    //       var transaction = {
    //         fb_transaction_id: transactions[i].fb_transaction_id,
    //         source_account_name: transactions[i].source_account_name,
    //         destination_account_name: transactions[i].destination_account_name,
    //         transaction_type: transactions[i].type,
    //         asset: transactions[i].asset_id,
    //         amount: transactions[i].amount,
    //         fee: transactions[i].fee,
    //         status: transactions[i].status,
    //         fb_transaction_hash: transactions[i].fb_transaction_hash,
    //         destination_address: transactions[i].destination_address,
    //         created_date: moment(createdDate).format(),
    //       };
    //       vResult.push(transaction);
    //     }
    //     const totalTransaction =
    //       await this._transactionRepository.getTransactionLogsTotal(
    //         fromDate,
    //         toDate,
    //         transactionType,
    //         status,
    //         assetName
    //       );
    //     let pageTotal = Number(totalTransaction[0].count) / pageSize;
    //     return {
    //       transactions: vResult,
    //       total: totalTransaction[0].count,
    //       page_total: Math.ceil(pageTotal),
    //     };
    //   } catch (error) {
    //     throw error;
    //   }
    // }
    getFireblocksNotificationList(fromDate, toDate, status, searchText) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const notifications = yield this._httpLogRepository.getFireblocksNotificationList(fromDate, toDate, status, searchText);
                var vResult = [];
                for (let i = 0; i < notifications.length; i++) {
                    const notificationInfo = notifications[i];
                    var vTimestamp = notificationInfo.request_date;
                    var vId = "";
                    var vTitle = "";
                    var vAssetId = "";
                    var vSourceName = "";
                    var vDestinationName = "";
                    var vAmount = "";
                    var vNetworkFee = "";
                    var vSourceAddress = "";
                    var vDestinationAddress = "";
                    var vStatus = "";
                    var vTxHash = "";
                    var vSubStatus = "";
                    var vFeeCurrency = "";
                    var vOperation = "";
                    var objectResult;
                    if (notificationInfo.body !== undefined) {
                        objectResult = JSON.parse(notificationInfo.body);
                        if (objectResult.type !== undefined) {
                            vTitle = objectResult.type;
                        }
                        else {
                            vTitle = objectResult.title;
                            vId = objectResult.txId;
                            vAmount = objectResult.netAmount;
                        }
                        if (objectResult.data !== undefined) {
                            const element = objectResult.data;
                            vId = element.id;
                            vAssetId = element.assetId;
                            if (element.source !== undefined) {
                                vSourceName = element.source.name;
                            }
                            if (element.destination !== undefined) {
                                vDestinationName = element.destination.name;
                            }
                            vAmount = element.amount;
                            vNetworkFee = element.networkFee;
                            vSourceAddress = element.sourceAddress;
                            vDestinationAddress = element.destinationAddress;
                            vStatus = element.status;
                            vTxHash = element.txHash;
                            vSubStatus = element.subStatus;
                            vFeeCurrency = element.feeCurrency;
                            vOperation = element.operation;
                        }
                    }
                    var notification = {
                        timestamp: vTimestamp,
                        id: vId,
                        type: vTitle,
                        assetId: vAssetId,
                        sourceName: vSourceName,
                        destinationName: vDestinationName,
                        amount: vAmount,
                        networkFee: vNetworkFee,
                        sourceAddress: vSourceAddress,
                        destinationAddress: vDestinationAddress,
                        status: vStatus,
                        txHash: vTxHash,
                        subStatus: vSubStatus,
                        feeCurrency: vFeeCurrency,
                        operation: vOperation,
                        raw_data: objectResult,
                    };
                    vResult.push(notification);
                }
                return vResult;
            }
            catch (error) {
                throw error;
            }
        });
    }
    submitBuySellTransaction(transactionId, logOnId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const transactions = yield this._transactionRepository.getInfoById(transactionId);
                if (transactions.length <= 0) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                const transaction = transactions[0];
                yield this._assetInVaultAccountService.syncFireblocksAssetInVaults(enums.accountType.USER, undefined, transaction.user_id);
                const companies = yield this._companyRepository.getList();
                if (companies.length <= 0) {
                    throw new customError_1.default(enums.responseCode.CompanyNotFound, enums.responseMessage.CompanyNotFound);
                }
                const companyInfo = companies[0];
                const companyVaultAccounts = yield this._vaultAccountRepository.getInfo(enums.accountType.COMPANY, companyInfo.company_id, undefined);
                if (companyVaultAccounts.length <= 0) {
                    throw new customError_1.default(enums.responseCode.VaultAccountNotFound, enums.responseMessage.VaultAccountNotFound);
                }
                const companyVaultAccountInfo = companyVaultAccounts[0];
                const userVaultAccounts = yield this._vaultAccountRepository.getInfo(enums.accountType.USER, undefined, transaction.user_id);
                if (userVaultAccounts.length <= 0) {
                    throw new customError_1.default(enums.responseCode.VaultAccountNotFound, enums.responseMessage.VaultAccountNotFound);
                }
                const userVaultAccountInfo = userVaultAccounts[0];
                const gasPrices = yield this._gasConfigurationRepository.getInfoByAssetCode(transaction.asset_code);
                if (gasPrices.length <= 0) {
                    throw new customError_1.default(enums.responseCode.GasConfigurationNotFound, enums.responseMessage.GasConfigurationNotFound);
                }
                const gasPrice = gasPrices[0];
                if (transaction.type === enums.transactionType.SELLING &&
                    transaction.status === enums.transactionStatus.PENDING) {
                    const companyBaseAssetInVaults = yield this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(transaction.destination_vault_account_id, gasPrice.native_asset_code);
                    if (companyBaseAssetInVaults.length <= 0) {
                        throw new customError_1.default(enums.responseCode.AssetNotFound, enums.responseMessage.AssetNotFound);
                    }
                    const companyBaseAssetInVaultInfo = companyBaseAssetInVaults[0];
                    if (companyBaseAssetInVaultInfo.total < gasPrice.gas_threshold) {
                        throw new customError_1.default(enums.responseCode.InsufficientFundsForCompany, enums.responseMessage.InsufficientFundsForCompany);
                    }
                    const baseAssetInVaults = yield this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(transaction.source_vault_account_id, gasPrice.native_asset_code);
                    if (baseAssetInVaults.length <= 0) {
                        throw new customError_1.default(enums.responseCode.AssetNotFound, enums.responseMessage.AssetNotFound);
                    }
                    const baseAssetInVaultInfo = baseAssetInVaults[0];
                    if (baseAssetInVaultInfo.total < gasPrice.gas_threshold) {
                        yield this.autoFuelGasFees(transaction.user_id, enums.fireblocksTransactionType.AUTO_FUELING_GAS_FEES, companyVaultAccountInfo.fireblock_vault_account_id, userVaultAccountInfo.fireblock_vault_account_id, gasPrice.asset_code, transaction.transaction_id, companyVaultAccountInfo.vault_account_id);
                        yield this._transactionRepository.updateTransactionStatus(transaction.transaction_id, enums.transactionStatus.ONHOLD);
                    }
                    else {
                        const estimateFee = yield this._fireblocksService.estimateFeeForTransaction(gasPrice.asset_code, userVaultAccountInfo.fireblock_vault_account_id, companyVaultAccountInfo.fireblock_vault_account_id, transaction.total_assets);
                        const transactionFee = estimateFee.medium.networkFee === undefined
                            ? 0
                            : parseFloat(estimateFee.medium.networkFee);
                        const fbTransaction = yield this._fireblocksService.createTransaction(gasPrice.asset_code, userVaultAccountInfo.fireblock_vault_account_id, companyVaultAccountInfo.fireblock_vault_account_id, transaction.total_assets, transactionFee);
                        yield this._fireblocksTransactionRepository.create(fbTransaction.id, enums.fireblocksTransactionType.TRANSFER_SELLING, gasPrice.asset_code, userVaultAccountInfo.fireblock_vault_account_id, "VAULT_ACCOUNT", transaction.total_assets, transactionFee, transaction.transaction_id, fbTransaction.status, companyVaultAccountInfo.fireblock_vault_account_id, undefined, "VAULT_ACCOUNT");
                        yield this._transactionRepository.updateTransactionStatus(transaction.transaction_id, enums.transactionStatus.ONHOLD);
                    }
                }
                else if (transaction.type === enums.transactionType.BUYING &&
                    transaction.status === enums.transactionStatus.PENDING) {
                    const companyBaseAssetInVaults = yield this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(transaction.source_vault_account_id, gasPrice.native_asset_code);
                    if (companyBaseAssetInVaults.length <= 0) {
                        throw new customError_1.default(enums.responseCode.AssetNotFound, enums.responseMessage.AssetNotFound);
                    }
                    const companyBaseAssetInVaultInfo = companyBaseAssetInVaults[0];
                    if (companyBaseAssetInVaultInfo.total < gasPrice.gas_threshold) {
                        throw new customError_1.default(enums.responseCode.InsufficientFundsForCompany, enums.responseMessage.InsufficientFundsForCompany);
                    }
                    const estimateFee = yield this._fireblocksService.estimateFeeForTransaction(gasPrice.asset_code, companyVaultAccountInfo.fireblock_vault_account_id, userVaultAccountInfo.fireblock_vault_account_id, transaction.total_assets);
                    const transactionFee = estimateFee.medium.networkFee === undefined
                        ? 0
                        : parseFloat(estimateFee.medium.networkFee);
                    const fbTransaction = yield this._fireblocksService.createTransaction(gasPrice.asset_code, companyVaultAccountInfo.fireblock_vault_account_id, userVaultAccountInfo.fireblock_vault_account_id, transaction.total_assets, transactionFee);
                    yield this._fireblocksTransactionRepository.create(fbTransaction.id, enums.fireblocksTransactionType.TRANSFER_BUYING, gasPrice.asset_code, companyVaultAccountInfo.fireblock_vault_account_id, "VAULT_ACCOUNT", transaction.total_assets, transactionFee, transaction.transaction_id, fbTransaction.status, userVaultAccountInfo.fireblock_vault_account_id, undefined, "VAULT_ACCOUNT");
                    yield this._transactionRepository.updateTransactionStatus(transaction.transaction_id, enums.transactionStatus.ONHOLD);
                }
            }
            catch (error) {
                throw error;
            }
        });
    }
    autoFuelGasFees(userId, type, fbSourceVaultAccountId, fbDestinationVaultAccountId, assetCode, refId, companyVaultAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const gasPrices = yield this._gasConfigurationRepository.getInfoByAssetCode(assetCode);
                if (gasPrices.length <= 0) {
                    throw new customError_1.default(enums.responseCode.GasConfigurationNotFound, enums.responseMessage.GasConfigurationNotFound);
                }
                const gasPrice = gasPrices[0];
                const companyBaseAssetInVaults = yield this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(companyVaultAccountId, gasPrice.native_asset_code);
                if (companyBaseAssetInVaults.length <= 0) {
                    throw new customError_1.default(enums.responseCode.AssetNotFound, enums.responseMessage.AssetNotFound);
                }
                const companyBaseAssetInVaultInfo = companyBaseAssetInVaults[0];
                if (companyBaseAssetInVaultInfo.total < gasPrice.gas_threshold) {
                    throw new customError_1.default(enums.responseCode.InsufficientFundsForCompany, enums.responseMessage.InsufficientFundsForCompany);
                }
                const estimateFee = yield this._fireblocksService.estimateFeeForTransaction(gasPrice.native_asset_code, fbSourceVaultAccountId, fbDestinationVaultAccountId, parseFloat(gasPrice.gas_cap));
                const transactionFee = estimateFee.medium.networkFee === undefined
                    ? 0
                    : parseFloat(estimateFee.medium.networkFee);
                const fbTransaction = yield this._fireblocksService.createTransaction(gasPrice.native_asset_code, fbSourceVaultAccountId, fbDestinationVaultAccountId, parseFloat(gasPrice.gas_cap), transactionFee);
                yield this._fireblocksTransactionRepository.create(fbTransaction.id, type, gasPrice.native_asset_code, fbSourceVaultAccountId, "VAULT_ACCOUNT", parseFloat(gasPrice.gas_cap), transactionFee, refId, fbTransaction.status, fbDestinationVaultAccountId, undefined, "VAULT_ACCOUNT");
            }
            catch (error) {
                throw error;
            }
        });
    }
    manualUpdateTransactionStatus(fbTransactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbTrans = yield this._fireblocksService.getTransactionById(fbTransactionId);
                if (fbTrans === undefined) {
                    throw new customError_1.default(enums.responseCode.NotFound, enums.responseMessage.NotFound);
                }
                else {
                    yield this.updateTransactionStatus(fbTransactionId, fbTrans.status, fbTrans.subStatus === undefined ? "" : fbTrans.subStatus, fbTrans.txHash);
                }
            }
            catch (error) {
                throw error;
            }
        });
    }
    updateTransactionStatus(fireblockTransactionId, status, remark, fbTransactionHash) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fireblocksTransactions = yield this._fireblocksTransactionRepository.getInfoByfbId(fireblockTransactionId);
                if (fireblocksTransactions.length <= 0) {
                    return;
                }
                const fireblocksTransactionInfo = fireblocksTransactions[0];
                var vStatus = "";
                switch (status) {
                    case fireblocks_sdk_1.TransactionStatus.COMPLETED:
                        vStatus = enums.transactionStatus.SUCCESS;
                        break;
                    case fireblocks_sdk_1.TransactionStatus.FAILED:
                        vStatus = enums.transactionStatus.FAILED;
                        break;
                    case fireblocks_sdk_1.TransactionStatus.CANCELLED:
                        vStatus = enums.transactionStatus.CANCELLED;
                        break;
                    case fireblocks_sdk_1.TransactionStatus.REJECTED:
                        vStatus = enums.transactionStatus.REJECTED;
                        break;
                    default:
                        break;
                }
                if (fireblocksTransactionInfo.type ===
                    enums.fireblocksTransactionType.TRANSFER_SELLING ||
                    fireblocksTransactionInfo.type ===
                        enums.fireblocksTransactionType.TRANSFER_BUYING) {
                    yield this._fireblocksTransactionRepository.updateStatusFromFireblocks(fireblocksTransactionInfo.fireblock_transaction_id, status, remark, fbTransactionHash);
                    yield this._transactionRepository.updateTransactionStatusFromFireblocks(fireblocksTransactionInfo.ref_id, vStatus, fbTransactionHash, status);
                }
                else if (fireblocksTransactionInfo.type ===
                    enums.fireblocksTransactionType.AUTO_FUELING_GAS_FEES) {
                    const transactions = yield this._transactionRepository.getInfoById(fireblocksTransactions[0].ref_id);
                    if (transactions.length <= 0) {
                        return;
                    }
                    const transactionInfo = transactions[0];
                    yield this._fireblocksTransactionRepository.updateStatusFromFireblocks(fireblocksTransactionInfo.fireblock_transaction_id, status, remark, fbTransactionHash);
                    const isExistsFBTransactions = yield this._fireblocksTransactionRepository.getInfoByTypeRefId(enums.fireblocksTransactionType.TRANSFER_SELLING, fireblocksTransactions[0].ref_id);
                    if (isExistsFBTransactions.length > 0) {
                        return;
                    }
                    const estimateFee = yield this._fireblocksService.estimateFeeForTransaction(transactionInfo.asset_code, transactionInfo.user_fireblock_vault_account_id, transactionInfo.company_fireblock_vault_account_id, transactionInfo.total_assets);
                    const transactionFee = estimateFee.medium.networkFee === undefined
                        ? 0
                        : parseFloat(estimateFee.medium.networkFee);
                    const fbTransaction = yield this._fireblocksService.createTransaction(transactionInfo.asset_code, transactionInfo.user_fireblock_vault_account_id, transactionInfo.company_fireblock_vault_account_id, transactionInfo.total_assets, transactionFee);
                    yield this._fireblocksTransactionRepository.create(fbTransaction.id, enums.fireblocksTransactionType.TRANSFER_SELLING, transactionInfo.asset_code, transactionInfo.user_fireblock_vault_account_id, "VAULT_ACCOUNT", transactionInfo.total_assets, transactionFee, transactionInfo.transaction_id, fbTransaction.status, transactionInfo.company_fireblock_vault_account_id, undefined, "VAULT_ACCOUNT");
                }
            }
            catch (error) {
                throw error;
            }
        });
    }
    manualFreezeTransaction(fbTransactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.freezeTransaction(fbTransactionId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    manualUnfreezeTransaction(fbTransactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.unfreezeTransaction(fbTransactionId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    freezeTransaction(fbTransactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._fireblocksService.freezeTransactionById(fbTransactionId);
            }
            catch (error) {
                throw error;
            }
        });
    }
    unfreezeTransaction(fbTransactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._fireblocksService.unfreezeTransactionById(fbTransactionId);
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.TransactionService = TransactionService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [companyRepository_1.CompanyRepository,
        assetRepository_1.AssetRepository,
        assetRateRepository_1.AssetRateRepository,
        transactionRepository_1.TransactionRepository,
        withdrawRepository_1.WithdrawRepository,
        fiatAccountRepository_1.FiatAccountRepository,
        assetInVaultAccountRepository_1.AssetInVaultAccountRepository,
        fireblocksTransactionRepository_1.FireblocksTransactionRepository,
        vaultAccountRepository_1.VaultAccountRepository,
        gasConfigurationRepository_1.GasConfigurationRepository,
        httpLogRepository_1.HttpLogRepository,
        vaultAccountService_1.VaultAccountService,
        assetInVaultAccountService_1.AssetInVaultAccountService,
        fireblocksService_1.FireblocksService,
        walletCreditRepository_1.WalletCreditRepository,
        userRepository_1.UserRepository])
], TransactionService);
//# sourceMappingURL=transactionService.js.map