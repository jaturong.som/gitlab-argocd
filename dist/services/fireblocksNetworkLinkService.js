"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FireblocksNetworkLinkService = void 0;
const fireblocksNetworkLinkRepository_1 = require("../repositories/fireblocksNetworkLinkRepository");
const assetInVaultAccountRepository_1 = require("../repositories/assetInVaultAccountRepository");
const assetRepository_1 = require("../repositories/assetRepository");
const assetRateRepository_1 = require("../repositories/assetRateRepository");
const transactionRepository_1 = require("../repositories/transactionRepository");
const fireblocksTransactionRepository_1 = require("../repositories/fireblocksTransactionRepository");
const vaultAccountRepository_1 = require("../repositories/vaultAccountRepository");
const customerService_1 = require("./customerService");
const fireblocksService_1 = require("./fireblocksService");
const fireblocksNetworkLinkCustomError_1 = __importDefault(require("../middlewares/fireblocksNetworkLinkCustomError"));
const enums = __importStar(require("../utilities/enums"));
const utils = __importStar(require("../utilities/utils"));
const tsyringe_1 = require("tsyringe");
let FireblocksNetworkLinkService = exports.FireblocksNetworkLinkService = class FireblocksNetworkLinkService {
    constructor(fireblocksNetworkLinkRepository, assetInVaultAccountRepository, assetRepository, assetRateRepository, transactionRepository, fireblocksTransactionRepository, vaultAccountRepository, customerService, fireblocksService) {
        this._fireblocksNetworkLinkRepository = fireblocksNetworkLinkRepository;
        this._assetInVaultAccountRepository = assetInVaultAccountRepository;
        this._assetRepository = assetRepository;
        this._assetRateRepository = assetRateRepository;
        this._transactionRepository = transactionRepository;
        this._fireblocksTransactionRepository = fireblocksTransactionRepository;
        this._vaultAccountRepository = vaultAccountRepository;
        this._customerService = customerService;
        this._fireblocksService = fireblocksService;
    }
    getAllAccountTypes(customerId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var wallet = yield this._customerService.getCryptoWalletList(customerId);
                var accountList = [];
                var balances = [];
                for (let j = 0; j < wallet.length; j++) {
                    var total = 0;
                    var pending = 0;
                    var available = 0;
                    if (wallet[j].total != null) {
                        total = wallet[j].total;
                    }
                    if (wallet[j].pending != null) {
                        pending = wallet[j].pending;
                    }
                    if (wallet[j].available != null) {
                        available = wallet[j].available;
                    }
                    balances.push({
                        coinSymbol: wallet[j].asset_code,
                        totalAmount: total,
                        pendingAmount: pending,
                        availableAmount: available,
                        creditAmount: 0,
                    });
                }
                accountList.push({
                    displayName: "Funding",
                    type: "FUNDING",
                    balances: balances,
                });
                return accountList;
            }
            catch (error) {
                throw error;
            }
        });
    }
    getDepositWalletAddress(customerId, accountType, coinSymbol, network) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (accountType != "FUNDING") {
                    throw new fireblocksNetworkLinkCustomError_1.default(404, enums.responseCodeFireblock.NotFound, enums.responseMessageFireblock.NotFound);
                }
                var assetInVault = yield this._assetInVaultAccountRepository.getAddressByUserIdAssetNetwork(customerId, coinSymbol, network);
                if (assetInVault.length <= 0) {
                    throw new fireblocksNetworkLinkCustomError_1.default(400, enums.responseCodeFireblock.ASSET_NOT_SUPPORTED, enums.responseMessageFireblock.ASSET_NOT_SUPPORTED);
                }
                return {
                    depositAddress: assetInVault[0].asset_address,
                    depositAddressTag: null,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createDepositWalletAddress(customerId, accountType, coinSymbol, network) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (accountType != "FUNDING") {
                    throw new fireblocksNetworkLinkCustomError_1.default(404, enums.responseCodeFireblock.NotFound, enums.responseMessageFireblock.NotFound);
                }
                var asset = yield this._assetRepository.getAssetIdByAssetCodeNetworkName(coinSymbol, network);
                if (asset.length <= 0) {
                    throw new fireblocksNetworkLinkCustomError_1.default(400, enums.responseCodeFireblock.ASSET_NOT_SUPPORTED, enums.responseMessageFireblock.ASSET_NOT_SUPPORTED);
                }
                yield this._customerService.createAssetInVaultAccount(customerId, asset[0].asset_code, customerId.toString());
                return yield this.getDepositWalletAddress(customerId, accountType, coinSymbol, network);
            }
            catch (error) {
                throw error;
            }
        });
    }
    getWithDrawalFee(transferAmount, coinSymbol, network) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var asset = yield this._assetRepository.getAssetIdByAssetCodeNetworkName(coinSymbol, network);
                if (asset.length <= 0) {
                    throw new fireblocksNetworkLinkCustomError_1.default(400, enums.responseCodeFireblock.ASSET_NOT_SUPPORTED, enums.responseMessageFireblock.ASSET_NOT_SUPPORTED);
                }
                return { feeAmount: "0" };
            }
            catch (error) {
                throw error;
            }
        });
    }
    createWithDraw(customerId, amount, maxFee, coinSymbol, network, accountType, toAddress, tag, isGross) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (accountType != "FUNDING") {
                    throw new fireblocksNetworkLinkCustomError_1.default(404, enums.responseCodeFireblock.NotFound, enums.responseMessageFireblock.NotFound);
                }
                const type = enums.transactionType.SELLING;
                const userVaultAccounts = yield this._vaultAccountRepository.getInfo(enums.accountType.USER, undefined, customerId);
                if (userVaultAccounts.length <= 0) {
                    throw new fireblocksNetworkLinkCustomError_1.default(404, enums.responseCodeFireblock.NotFound, enums.responseMessageFireblock.NotFound);
                }
                const userVaultAccountInfo = userVaultAccounts[0];
                const assetRates = yield this._assetRateRepository.getInfoByAssetCode(coinSymbol);
                if (assetRates.length <= 0) {
                    throw new fireblocksNetworkLinkCustomError_1.default(400, enums.responseCodeFireblock.ASSET_NOT_SUPPORTED, enums.responseMessageFireblock.ASSET_NOT_SUPPORTED);
                }
                const assetRateInfo = assetRates[0];
                const transactions = yield this._transactionRepository.create(customerId, yield utils.generateKey(customerId.toString()), type, userVaultAccountInfo.vault_account_id, assetRateInfo.asset_id, assetRateInfo.asset_code, assetRateInfo.buying, assetRateInfo.selling, amount * assetRateInfo.buying, amount, enums.transactionStatus.ONHOLD, "0", enums.source.FIREBLOCKS, false, "", "", undefined, toAddress);
                const transaction = transactions[0];
                const estimateFee = yield this._fireblocksService.estimateFeeForTransactionNetworkLink(coinSymbol, userVaultAccountInfo.fireblock_vault_account_id, toAddress, amount);
                const transactionFee = estimateFee.medium.networkFee === undefined
                    ? 0
                    : parseFloat(estimateFee.medium.networkFee);
                const fbTransaction = yield this._fireblocksService.createTransactionNetworkLink(coinSymbol, userVaultAccountInfo.fireblock_vault_account_id, toAddress, amount, transactionFee);
                yield this._fireblocksTransactionRepository.create(fbTransaction.id, enums.fireblocksTransactionType.TRANSFER_SELLING, coinSymbol, userVaultAccountInfo.fireblock_vault_account_id, "VAULT_ACCOUNT", amount, transactionFee, transaction.transaction_id, fbTransaction.status, undefined, toAddress, "ONE_TIME_ADDRESS");
                yield this._transactionRepository.updateTransactionWithFBIdStatus(transaction.transaction_id, enums.transactionStatus.ONHOLD, fbTransaction.id);
                return {
                    transactionID: fbTransaction.id,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionById(transactionID) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var transaction = yield this._transactionRepository.getTransactionByTransactionID(transactionID);
                if (transaction.length <= 0) {
                    return {
                        status: "NOT_FOUND",
                    };
                }
                var transactionType = "";
                if (transaction[0].type == "S") {
                    transactionType = "CRYPTO_WITHDRAWAL";
                }
                else if (transaction[0].type == "B") {
                    transactionType = "CRYPTO_DEPOSIT";
                }
                var timestamp = new Date(transaction[0].transaction_date).valueOf();
                return {
                    transactionID: transaction[0].fb_transaction_id,
                    status: transaction[0].fb_transaction_status,
                    txHash: transaction[0].fb_transaction_hash,
                    amount: transaction[0].total_amount.toString(),
                    serviceFee: "0",
                    coinSymbol: transaction[0].code,
                    network: transaction[0].name,
                    direction: transactionType,
                    timestamp: timestamp,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionByHash(txHash, network) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var transaction = yield this._transactionRepository.getTransactionByTransactionHash(txHash, network);
                if (transaction.length <= 0) {
                    return {
                        status: "NOT_FOUND",
                    };
                }
                var transactionType = "";
                if (transaction[0].type == "S") {
                    transactionType = "CRYPTO_WITHDRAWAL";
                }
                else if (transaction[0].type == "B") {
                    transactionType = "CRYPTO_DEPOSIT";
                }
                var timestamp = new Date(transaction[0].transaction_date).valueOf();
                return {
                    transactionID: transaction[0].fb_transaction_id,
                    status: transaction[0].fb_transaction_status,
                    txHash: transaction[0].fb_transaction_hash,
                    amount: transaction[0].total_amount.toString(),
                    serviceFee: "0",
                    coinSymbol: transaction[0].code,
                    network: transaction[0].name,
                    direction: transactionType,
                    timestamp: timestamp,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getTransactionHistory(fromDate, toDate, pageSize, pageCursor, isSubTransfer, coinSymbol, network, customerId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var tdate = new Date(Number(toDate));
                var fdate = new Date(Number(fromDate));
                var transactionList = [];
                if (pageCursor == "") {
                    pageCursor = "1";
                }
                var transaction = yield this._transactionRepository.getTransactionListPageSize(Number(pageSize), Number(pageCursor), fdate.toISOString(), tdate.toISOString(), coinSymbol, network, customerId);
                if (transaction.length <= 0) {
                    return {
                        status: "NOT_FOUND",
                    };
                }
                for (let j = 0; j < transaction.length; j++) {
                    var transactionType = "";
                    if (transaction[j].type == "S") {
                        transactionType = "CRYPTO_WITHDRAWAL";
                    }
                    else if (transaction[0].type == "B") {
                        transactionType = "CRYPTO_DEPOSIT";
                    }
                    var timestamp = new Date(transaction[j].transaction_date).valueOf();
                    transactionList.push({
                        transactionID: transaction[j].fb_transaction_id,
                        status: transaction[j].fb_transaction_status,
                        txHash: transaction[j].fb_transaction_hash,
                        amount: transaction[j].total_amount.toString(),
                        serviceFee: "0",
                        coinSymbol: transaction[j].code,
                        network: transaction[j].name,
                        direction: transactionType,
                        timestamp: timestamp,
                    });
                }
                var nextPageCursor = Number(pageCursor) + 1;
                return {
                    nextPageCursor: nextPageCursor.toString(),
                    transactions: transactionList,
                };
            }
            catch (error) {
                throw error;
            }
        });
    }
    getSupportedAssets() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var assetList = yield this._assetRepository.getAssetList();
                var assets = [];
                for (let j = 0; j < assetList.length; j++) {
                    var identifiers = [];
                    identifiers.push(assetList[j].contract_address);
                    if (assetList[j].code !== "ALGO_TEST") {
                        assets.push({
                            coinSymbol: assetList[j].asset_name,
                            network: assetList[j].name,
                            coinClass: "TOKEN",
                            identifiers: identifiers,
                        });
                    }
                    else {
                        assets.push({
                            coinSymbol: assetList[j].asset_name,
                            network: assetList[j].name,
                            coinClass: "BASE",
                            identifiers: identifiers,
                        });
                    }
                }
                return assets;
            }
            catch (error) {
                throw error;
            }
        });
    }
    createSubMainTransfer() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkRepository.createSubMainTransfer();
            }
            catch (error) {
                throw error;
            }
        });
    }
    createSubAccountsTransfer() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkRepository.createSubAccountsTransfer();
            }
            catch (error) {
                throw error;
            }
        });
    }
    createInternalTransfer() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this._fireblocksNetworkLinkRepository.createInternalTransfer();
            }
            catch (error) {
                throw error;
            }
        });
    }
};
exports.FireblocksNetworkLinkService = FireblocksNetworkLinkService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [fireblocksNetworkLinkRepository_1.FireblocksNetworkLinkRepository,
        assetInVaultAccountRepository_1.AssetInVaultAccountRepository,
        assetRepository_1.AssetRepository,
        assetRateRepository_1.AssetRateRepository,
        transactionRepository_1.TransactionRepository,
        fireblocksTransactionRepository_1.FireblocksTransactionRepository,
        vaultAccountRepository_1.VaultAccountRepository,
        customerService_1.CustomerService,
        fireblocksService_1.FireblocksService])
], FireblocksNetworkLinkService);
//# sourceMappingURL=fireblocksNetworkLinkService.js.map