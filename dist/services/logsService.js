"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogsService = void 0;
// import * as enums from "../utilities/enums";
// import * as logs from "../utilities/logs";
// import { db } from "../repositories/connection";
// import { LogsRepository } from "../repositories/logsRepository";
const tsyringe_1 = require("tsyringe");
let LogsService = exports.LogsService = class LogsService {
};
exports.LogsService = LogsService = __decorate([
    (0, tsyringe_1.singleton)()
], LogsService);
// export const writeLogsDB = async(
//     logsType: enums.logsType,
//     logsName: string,
//     logsMessage: string
//   ) => {
//     try {
//       const sql = `
//         INSERT INTO logs(logs_type, logs_name, logs_message, created_date)
//         VALUES($1, $2, $3, $4,CURRENT_TIMESTAMP)
//         RETURNING *`;
//       return await db.query(sql, [
//         logsType,
//         logsName,
//         logsMessage,
//       ]);
//     } catch (error) {
//       throw error;
//     }
// };
// export const logInfo = async (
//   logsName : string ,
//   logsMessage : string) => {
//   try {
//     // logs.writeLogsDB(enums.logsType.INFO,logsName,logsMessage)
//     // logs.writeLogsDB(enums.logsType.INFO,logsName,logsMessage)
//     console.log(logsMessage);
//     return ;
//   } catch (error) {
//     throw error;
//   }
// };
// export const logError = async (
//   logsName : string ,
//   logsMessage : string) => {
//   try {
//     // logs.writeLogsDB(enums.logsType.ERROR,logsName,logsMessage)
//     console.log(logsMessage);
//     return ;
//   } catch (error) {
//     throw error;
//   }
// };
// export const logDebug = async (
//   logsName : string ,
//   logsMessage : string) => {
//   try {
//     // logs.writeLogsDB(enums.logsType.DEBUG,logsName,logsMessage)
//     console.log(logsMessage);
//     return ;
//   } catch (error) {
//     throw error;
//   }
// };
// export function writeLogsDB(ERROR: enums.logsType, logsName: string, logsMessage: string) {
//   throw new Error("Function not implemented.");
// }
//# sourceMappingURL=logsService.js.map