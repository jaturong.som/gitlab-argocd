"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FireblocksService = void 0;
const axios_1 = __importDefault(require("axios"));
const fireblocks_sdk_1 = require("fireblocks-sdk");
const tsyringe_1 = require("tsyringe");
const customError_1 = __importDefault(require("../middlewares/customError"));
const vaultService_1 = require("./vaultService");
let FireblocksService = exports.FireblocksService = class FireblocksService {
    constructor(vaultService) {
        this._vaultService = vaultService;
    }
    getVaultAccountsWithPageInfo(pagedVaultAccountsRequestFilters) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKey, fbKey.data.apiKey);
                return yield fireblocks.getVaultAccountsWithPageInfo(pagedVaultAccountsRequestFilters);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
    getVaultAccountById(vaultAccountId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKey, fbKey.data.apiKey);
                return yield fireblocks.getVaultAccountById(vaultAccountId);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
    getVaultAccountAsset(vaultAccountId, assetId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKey, fbKey.data.apiKey);
                return yield fireblocks.getVaultAccountAsset(vaultAccountId, assetId);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
    getDepositAddresses(vaultAccountId, assetId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKey, fbKey.data.apiKey);
                return yield fireblocks.getDepositAddresses(vaultAccountId, assetId);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
    getTransactionById(txId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKey, fbKey.data.apiKey);
                return yield fireblocks.getTransactionById(txId);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
    createVaultAccount(name, hiddenOnUI, customerRefId, autoFuel) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKey, fbKey.data.apiKey);
                return yield fireblocks.createVaultAccount(name, hiddenOnUI, customerRefId, autoFuel);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
    createVaultAsset(vaultAccountId, assetId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKey, fbKey.data.apiKey);
                return yield fireblocks.createVaultAsset(vaultAccountId, assetId);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
    // async getVaultBalanceByAsset(assetId: string) {
    //   try {
    //     const fbKey = await this._vaultService.getFireblocksKeyInVault();
    //     const fireblocks = new FireblocksSDK(
    //       fbKey.data.privateKey,
    //       fbKey.data.apiKey
    //     );
    //     return await fireblocks.getVaultBalanceByAsset(assetId);
    //   } catch (error) {
    //     if (axios.isAxiosError(error)) {
    //       let response = error.response;
    //       if (response !== undefined) {
    //         let respData = response.data;
    //         throw new CustomError(respData.code, respData.message);
    //       }
    //     }
    //     throw error;
    //   }
    // }
    estimateFeeForTransaction(assetId, sourceId, destinationId, amount) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKey, fbKey.data.apiKey);
                const payload = {
                    assetId: assetId,
                    source: {
                        type: fireblocks_sdk_1.PeerType.VAULT_ACCOUNT,
                        id: sourceId || "0",
                    },
                    destination: {
                        type: fireblocks_sdk_1.PeerType.VAULT_ACCOUNT,
                        id: destinationId,
                    },
                    amount: String(amount),
                };
                return yield fireblocks.estimateFeeForTransaction(payload);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
    estimateFeeForTransactionNetworkLink(assetId, sourceId, address, amount) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKey, fbKey.data.apiKey);
                const payload = {
                    assetId: assetId,
                    source: {
                        type: fireblocks_sdk_1.PeerType.VAULT_ACCOUNT,
                        id: sourceId || "0",
                    },
                    destination: {
                        type: fireblocks_sdk_1.PeerType.ONE_TIME_ADDRESS,
                        // id: destinationId,
                        oneTimeAddress: {
                            address: address,
                            tag: "",
                        },
                    },
                    amount: String(amount),
                };
                return yield fireblocks.estimateFeeForTransaction(payload);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
    createTransaction(assetId, sourceId, destinationId, amount, fee) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKey, fbKey.data.apiKey);
                const payload = {
                    assetId: assetId,
                    source: {
                        type: fireblocks_sdk_1.PeerType.VAULT_ACCOUNT,
                        id: sourceId || "0",
                    },
                    destination: {
                        type: fireblocks_sdk_1.PeerType.VAULT_ACCOUNT,
                        id: destinationId,
                    },
                    amount: String(amount),
                    fee: String(fee),
                };
                return yield fireblocks.createTransaction(payload);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
    createTransactionNetworkLink(assetId, sourceId, address, amount, fee) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKey, fbKey.data.apiKey);
                const payload = {
                    assetId: assetId,
                    source: {
                        type: fireblocks_sdk_1.PeerType.VAULT_ACCOUNT,
                        id: sourceId || "0",
                    },
                    destination: {
                        type: fireblocks_sdk_1.PeerType.ONE_TIME_ADDRESS,
                        oneTimeAddress: {
                            address: address,
                            tag: "",
                        },
                    },
                    amount: String(amount),
                    fee: String(fee),
                    note: "Created by fireblocks SDK",
                };
                return yield fireblocks.createTransaction(payload);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
    freezeTransactionById(txId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKeyNonSigningAdmin, fbKey.data.apiKeyNonSigningAdmin);
                return yield fireblocks.freezeTransactionById(txId);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
    unfreezeTransactionById(txId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const fbKey = yield this._vaultService.getFireblocksKeyInVault();
                const fireblocks = new fireblocks_sdk_1.FireblocksSDK(fbKey.data.privateKeyNonSigningAdmin, fbKey.data.apiKeyNonSigningAdmin);
                return yield fireblocks.unfreezeTransactionById(txId);
            }
            catch (error) {
                if (axios_1.default.isAxiosError(error)) {
                    let response = error.response;
                    if (response !== undefined) {
                        let respData = response.data;
                        throw new customError_1.default(respData.code, respData.message);
                    }
                }
                throw error;
            }
        });
    }
};
exports.FireblocksService = FireblocksService = __decorate([
    (0, tsyringe_1.singleton)(),
    __metadata("design:paramtypes", [vaultService_1.VaultService])
], FireblocksService);
//# sourceMappingURL=fireblocksService.js.map