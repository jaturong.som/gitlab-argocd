"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const express_fileupload_1 = __importDefault(require("express-fileupload"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
// import morgan from "morgan";
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
require("dotenv/config");
const errorHandler_1 = __importDefault(require("./middlewares/errorHandler"));
const morgan_1 = require("./middlewares/morgan");
const swagger_json_1 = __importDefault(require("./swagger.json"));
const routes_1 = require("./v1/routes/routes");
const app = (0, express_1.default)();
const PORT = process.env.PORT || 3000;
// override send to store response body for morgan token to retrieve
const originalSend = app.response.send;
app.response.send = function sendOverride(body) {
    this.responseBody = body;
    return originalSend.call(this, body);
};
// enable files upload
app.use((0, express_fileupload_1.default)({
    createParentPath: true,
}));
app.use((0, cors_1.default)());
app.use(body_parser_1.default.json({ limit: "50mb" }));
app.use(body_parser_1.default.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 50000,
}));
app.use(express_1.default.json());
app.use(express_1.default.static("public"));
// app.use(morgan("tiny"));
app.use((0, morgan_1.morgan)(':remote-addr [:date[clf]] ":method :url HTTP/:http-version" Input :input Response :response-body'));
if (process.env.NODE_ENV === "development") {
    app.use("/swagger", swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(swagger_json_1.default));
}
(0, routes_1.RegisterRoutes)(app);
app.use(errorHandler_1.default);
app.listen(PORT, () => {
    console.log(`API is listening on port ${PORT}`);
    if (process.env.NODE_ENV === "development") {
        console.log(`Swagger service are available on http://localhost:${PORT}/swagger`);
    }
});
//# sourceMappingURL=index.js.map