import * as jwt from "jsonwebtoken";

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NODE_ENV: string;
      PORT: string;
      DATABASE_CONNECTION: string;
      JWT_ACCESS_TOKEN_SECRET: jwt.Secret;
      FIREBLOCKS_SECRET_KEY: string;
      FIREBLOCKS_API_KEY: string;
    }
  }
}

export {};
