export enum responseCode {
  Success = 0,
  Duplicate = 4001,
  UserNotFound = 4002,
  CompanyNotFound = 4003,
  VaultAccountNotFound = 4004,
  FiatAccountNotFound = 4005,
  AssetNotFound = 4006,
  AssetRateNotFound = 4007,
  BankAccountNotFound = 4008,
  InsufficientFunds = 4009,
  InsufficientAssets = 4010,
  AccountNameMismatch = 4011,
  ProtectOwnerAccount = 4012,
  GasConfigurationNotFound = 4013,
  InsufficientFundsForCompany = 4014,
  UsernameOrPasswordIncorrectCase = 4020,
  PasswordMismatch = 4021,
  CharactorLongCase = 4022,
  OneUpperCase = 4023,
  OneLowerCase = 4024,
  OneDigitCase = 4025,
  InvalidStatus = 4026,
  SenderNotfound = 4027,
  BankAccountTypeInvalid = 4028,
  InvalidRequest = 4029,
  InvalidUserType = 4030,
  ReceiverNotfound = 4031,
  UploadFileInvalidFileType = 4032,
  UploadFileFileTooLarge = 4033,
  Unauthorized = 403,
  NotFound = 404,
  Error = 500,
}

export enum responseMessage {
  Success = "Success",
  Duplicate = "Data has already exists",
  UserNotFound = "User not found",
  CompanyNotFound = "Company not found",
  VaultAccountNotFound = "Vault account not found",
  FiatAccountNotFound = "Fiat account not found",
  AssetNotFound = "Asset not found",
  AssetRateNotFound = "Asset Rate not found",
  BankAccountNotFound = "Bank account not found",
  InsufficientFunds = "Insufficient funds",
  InsufficientAssets = "Insufficient balance",
  AccountNameMismatch = "Account name mismatch",
  ProtectOwnerAccount = "You are not able to change status of your owner account",
  GasConfigurationNotFound = "Gas configuration not found",
  InsufficientFundsForCompany = "Insufficient funds of company",
  UsernameOrPasswordIncorrectCase = "Username or password is incorrect. Please try again.",
  PasswordMismatch = "Password mismatch",
  CharactorLongCase = "Password must be 8-16 Characters Long.",
  OneUpperCase = "Password must have at least one Uppercase Character.",
  OneLowerCase = "Password must have at least one Lowercase Character.",
  OneDigitCase = "Password must contain at least one Digit.",
  InvalidStatus = "Status is invalid.",
  SenderNotfound = "Sender account not found",
  BankAccountTypeInvalid = "Bank account type is invalid",
  InvalidRequest = "Request is invalid",
  InvalidUserType = "User type is invalid",
  ReceiverNotfound = "Receiver not found",
  UploadFileInvalidFileType = "Invalid file type",
  UploadFileFileTooLarge = "File too large",
  Unauthorized = "Unauthorized",
  NotFound = "Data not found",
  Error = "Internal error",
}

export enum fireblocksTransactionType {
  ADD_BASE_ASSET = "ADD_BASE_ASSET",
  AUTO_FUELING_GAS_FEES = "AUTO_FUELING_GAS_FEES",
  AUTO_FUELING_GAS_FEES_FOR_NEW_ASSET = "AUTO_FUELING_GAS_FEES_FOR_NEW_ASSET",
  TRANSFER_BUYING = "TRANSFER_BUYING",
  TRANSFER_SELLING = "TRANSFER_SELLING",
}

export enum accountType {
  COMPANY = "C",
  USER = "U",
}

export enum mode {
  INSERT = "INSERT",
  UPDATE = "UPDATE",
}

export enum fireblocksStatus {
  SUBMITTED = "SUBMITTED",
}

export enum transactionType {
  BUYING = "B",
  SELLING = "S",
  WITHDRAW = "W",
  TRANSFER = "TF",
  CONVERT = "CV",
  TOPUP = "TU",
}

export enum inputType {
  ASSET = "ASSET",
  AMOUNT = "AMOUNT",
}

export enum transactionStatus {
  ACTIVE = "A",
  SUCCESS = "S",
  FAILED = "F",
  PENDING = "P",
  REJECTED = "R",
  CANCELLED = "C",
  ONHOLD = "H",
}

export enum transactionStatusName {
  ACTIVE = "ACTIVE",
  SUCCESS = "SUCCESS",
  FAILED = "FAILED",
  PENDING = "PENDING",
  REJECTED = "REJECTED",
  CANCELLED = "CANCELLED",
  ONHOLD = "ONHOLD",
}

export enum lqnTransactionStatus {
  UN_COMMIT_HOLD = "UN-COMMIT-HOLD",
  UN_COMMIT_COMPLIANCE = "UN-COMMIT-COMPLIANCE",
  HOLD = "HOLD",
  COMPLIANCE = "COMPLIANCE",
  SANCTION = "SANCTION",
  UN_PAID = "UN-PAID",
  POST = "POST",
  PAID = "PAID",
  CANCEL = "CANCEL",
  CANCEL_HOLD = "CANCELHOLD",
  API_PROCESSING = "API PROCESSING",
  BLOCK = "BLOCK",
}

export enum lqnTransactionStatusText {
  UN_COMMIT_HOLD = "Hold (Un-Commit-Hold",
  UN_COMMIT_COMPLIANCE = "Hold (Un-Commit-Compliance)",
  HOLD = "Hold",
  COMPLIANCE = "Hold (Compliance)",
  SANCTION = "Hold (Sanction)",
  UN_PAID = "Pending (Un-Paid)",
  API_PROCESSING = "Pending (API Processing)",
  POST = "Pending (Post)",
  PAID = "Success (Paid)",
  CANCEL = "Cancel",
  CANCEL_HOLD = "Cancel (Hold)",
  BLOCK = "Cancel (Block)",
}

export enum stepJobProcess {
  SUBMITTED = "SUBMITTED",
  AUTO_FUELING_GAS_FEES = "AUTO_FUELING_GAS_FEES",
  FAILED = "FAILED",
  CANCELLED = "CANCELLED",
  COMPLETED = "COMPLETED",
}

export enum fireblocksTransactionStatus {
  SUBMITTED = "SUBMITTED",
  QUEUED = "QUEUED",
  PENDING_SIGNATURE = "PENDING_SIGNATURE",
  BROADCASTING = "BROADCASTING",
  COMPLETED = "COMPLETED",
  FAILED = "FAILED",
  CANCELLED = "CANCELLED",
  REJECTED = "REJECTED",
}

export enum specialAssetCode {
  ALGO = "ALGO",
  ALGO_USDC_UV4I = "ALGO_USDC_UV4I",
  SOL = "SOL",
  SOL_USDC_PTHX = "SOL_USDC_PTHX",
  XLM = "XLM",
  XLM_USDC_5F3T = "XLM_USDC_5F3T",
}

export enum dropdownlistType {
  PURPOSE_OF_TRANSFER = "PURPOSE_OF_TRANSFER",
  RELATIONSHIP_TO_RECEIVER = "RELATIONSHIP_TO_RECEIVER",
}

export enum bankAccountType {
  OWNER = "M",
  OTHER = "O",
}

export enum addressType {
  USER_PROFILE = "USER_PROFILE",
  BANK_ACCOUNT = "BANK_ACCOUNT",
  LQN_USER_PROFILE = "LQN_USER_PROFILE",
}

export enum source {
  FIREBLOCKS = "FIREBLOCKS",
  PORTAL = "PORTAL",
}

export enum logsType {
  INFO = "INFO",
  ERROR = "ERROR",
  DEBUG = "DEBUG",
}

export enum jobMethod {
  CREATE_SPECIAL_ASSETS = "CREATE_SPECIAL_ASSETS",
  PROCESS_PENDING_TRANSACTIONS = "PROCESS_PENDING_TRANSACTIONS",
  SYNC_FIREBLOCKS_TRANSACTIONS = "SYNC_FIREBLOCKS_TRANSACTIONS",
  UPDATE_CHAINALYSIS_TRANSACTIONS = "UPDATE_CHAINALYSIS_TRANSACTIONS",
}

export enum chainalysisAlert {
  HIGH = "HIGH",
  MEDIUM = "MEDIUM",
  LOW = "LOW",
}

export enum responseCodeFireblock {
  MISSING_REQUEST = 400000,
  NONCE_INVALID = 400001,
  TIMESTAMP_INVALID = 400002,
  SIGNATURE_INVALID = 400003,
  INSUFFICIENT_PERMISSION = 400004,
  INSUFFICIENT_FUND = 400005,
  INSUFFICIENT_FEE = 400006,
  UNSUPPORTED_ACCOUNT = 400007,
  UNSUPPORTED_OPERATION = 400008,
  ASSET_NOT_SUPPORTED = 400009,
  PARAMETER_INVALID = 400010,
  BAD_ADDRESS_FORMAT = 400011,
  BALANCE_AMOUNT_TOO_SMALL = 400012,
  THIRD_PARTY_NEED_MANUAL_DEPOSIT_ADDRESS = 400013,
  THIRD_PARTY_REJECTED_OPERATION = 400014,
  WITHDRAW_CANCELLED = 400015,
  ADDRESS_NOT_WHITELIST = 400016,
  IP_NOT_WHITELIST = 400017,
  ACCOUNT_NOT_FOUND = 400018,
  WITHDRAW_LIMITED = 400019,
  THIRD_PARTY_DENIEDREQUEST = 400020,
  NotFound = 404,
}

export enum responseMessageFireblock {
  MISSING_REQUEST = "Missing request header params",
  NONCE_INVALID = "Nonce sent was invalid",
  TIMESTAMP_INVALID = "Timestamp sent was invalid",
  SIGNATURE_INVALID = "Signature sent was invalid",
  INSUFFICIENT_PERMISSION = "Insufficient permissions for this API key",
  INSUFFICIENT_FUND = "Insufficient funds to carry out this operation",
  INSUFFICIENT_FEE = "Insufficient fee to carry out this operation",
  UNSUPPORTED_ACCOUNT = "Unsupported account type for this 3rd party",
  UNSUPPORTED_OPERATION = "Unsupported operation for this 3rd party",
  ASSET_NOT_SUPPORTED = "Asset not supported on this 3rd party",
  PARAMETER_INVALID = "One of the parameters sent in the body or query is invalid",
  BAD_ADDRESS_FORMAT = "Bad address format sent",
  BALANCE_AMOUNT_TOO_SMALL = "Balance amount is too small",
  THIRD_PARTY_NEED_MANUAL_DEPOSIT_ADDRESS = "This 3rd party needs manual deposit address generation",
  THIRD_PARTY_REJECTED_OPERATION = "The 3rd party rejected this operation",
  WITHDRAW_CANCELLED = "Withdraw was cancelled or failed on the 3rd party",
  ADDRESS_NOT_WHITELIST = "Address wasn't whitelisted",
  IP_NOT_WHITELIST = "IP wasn't whitelisted",
  ACCOUNT_NOT_FOUND = "Account not found",
  WITHDRAW_LIMITED = "Withdrawals are limited by the 3rd party. Please try again in a bit.",
  THIRD_PARTY_DENIEDREQUEST = "3rd party has denied the request - a settlement is required!",
  NotFound = "Account ID or Account Type not found or supported.",
}

export enum lqnUserType {
  SENDER = "SENDER",
  RECEIVER = "RECEIVER",
}

// export enum lqnStatus {
//   ACTIVE = "ACTIVE",
//   INACTIVE = "INACTIVE",
// }

export enum userType {
  INDIVIDUAL = "I",
  BUSINESS = "B",
}

export enum paymentMode {
  BANK_TRANSFER = "B",
}

export enum status {
  ACTIVE = "A",
  INACTIVE = "I",
}

export enum responseCodeLQN {
  INVALID_FIRST_NAME = 4001,
  INVALID_LAST_NAME = 4002,
  INVALID_ADDRESS = 4003,
  INVALID_DATE_OF_BIRTH = 4004,
  INVALID_GENDER = 4005,
  INVALID_CONTACT_NUMBER = 4006,
  INVALID_POSTCODE = 4007,
  INVALID_ID_TYPE = 4008,
  INVALID_ID_NUMBER = 4009,
  INVALID_ID_ISSUE_DATE = 4010,
  INVALID_ID_EXPIRE_DATE = 4011,
  INVALID_OCCUPATION = 4012,
  INVALID_BENEFICIARY = 4013,
}

export enum responseMessageLQN {
  INVALID_FIRST_NAME = "First name is required",
  INVALID_LAST_NAME = "Last name is required",
  INVALID_ADDRESS = "Address is required",
  INVALID_DATE_OF_BIRTH = "Date of birth is required",
  INVALID_GENDER = "Gender is required",
  INVALID_CONTACT_NUMBER = "Contact number is required",
  INVALID_POSTCODE = "Postcode is required",
  INVALID_ID_TYPE = "Id type is required",
  INVALID_ID_NUMBER = "Id number is required",
  INVALID_ID_ISSUE_DATE = "Issue date is required",
  INVALID_ID_EXPIRE_DATE = "Expire date is required",
  INVALID_OCCUPATION = "Occupation is required",
  INVALID_BENEFICIARY = "Beneficiary is required",
}
