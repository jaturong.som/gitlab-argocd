import { Request, Response, NextFunction } from "express";
import { body, validationResult } from "express-validator";

export const loginValidationRules = () => {
  return [
    body("user_name").exists({ checkFalsy: true }),
    body("password").exists({ checkFalsy: true }),
  ];
};

export const registerValidationRules = () => {
  return [
    body("email").isEmail(),
    body("password").exists({ checkFalsy: true }),
  ];
};

export const createUserValidationRules = () => {
  return [
    body("type").exists({ checkFalsy: true }),
    body("email").isEmail(),
    body("password").exists({ checkFalsy: true }),
  ];
};

export const updateUserValidationRules = () => {
  return [body("email").isEmail(), body("status").exists({ checkFalsy: true })];
};

export const createRoleValidationRules = () => {
  return [body("name").exists({ checkFalsy: true })];
};

export const updateRoleValidationRules = () => {
  return [
    body("name").exists({ checkFalsy: true }),
    body("status").exists({ checkFalsy: true }),
  ];
};

export const updateRoleMenuByRoleIdValidationRules = () => {
  return [
    body("group_menu_id").exists({ checkFalsy: true }),
    body("permission").exists({ checkFalsy: true }),
  ];
};

export const validate = (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  return res.status(400).json({
    code: 400,
    message: "Invalid",
    result: errors.array(),
  });
};
