import bcrypt from "bcrypt";
import generateApiKey from "generate-api-key";
import * as jwt from "jsonwebtoken";
import { writeFileSync } from "fs";
import CustomError from "../middlewares/customError";
import * as enums from "../utilities/enums";
import nodemailer from "nodemailer";
import SMTPTransport from "nodemailer/lib/smtp-transport";
import { VaultService } from "../services/vaultService";

export const hashPassword = async (password: string): Promise<string> => {
  return new Promise(function (fulfill, reject) {
    bcrypt.hash(password, 10, async function (err, hash) {
      if (err) {
        return reject(err);
      } else {
        return fulfill(hash);
      }
    });
  });
};

export const comparePassword = async (
  password: string,
  hashPassword: string
) => {
  return new Promise(function (fulfill, reject) {
    bcrypt.compare(password, hashPassword, async function (err, result) {
      if (err) {
        return reject(err);
      } else {
        return fulfill(result);
      }
    });
  });
};

export const jwtGenerateToken = async (user_id: string, code: string) => {
  try {
    let signOptions: jwt.SignOptions = {
      expiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRES_IN!,
      algorithm: "HS256",
    };
    return jwt.sign(
      { user_id: user_id, code: code },
      process.env.JWT_ACCESS_TOKEN_SECRET!,
      signOptions
    );
  } catch (error) {
    throw error;
  }
};

export const jwtRefreshToken = async (user_id: string, code: string) => {
  try {
    let signOptions: jwt.SignOptions = {
      expiresIn: process.env.JWT_REFRESH_TOKEN_EXPIRES_IN!,
      algorithm: "HS256",
    };
    return jwt.sign(
      { user_id: user_id, code: code },
      process.env.JWT_REFRESH_TOKEN_SECRET!,
      signOptions
    );
  } catch (error) {
    throw error;
  }
};

export const jwtVerifyToken = async (token: string) => {
  try {
    let verifyOptions: jwt.VerifyOptions = {
      algorithms: ["HS256"],
    };
    return jwt.verify(
      token,
      process.env.JWT_ACCESS_TOKEN_SECRET!,
      verifyOptions
    );
  } catch (error) {
    throw error;
  }
};

export const jwtVerifyRefreshToken = async (token: string) => {
  try {
    let verifyOptions: jwt.VerifyOptions = {
      algorithms: ["HS256"],
    };
    return jwt.verify(
      token,
      process.env.JWT_REFRESH_TOKEN_SECRET!,
      verifyOptions
    );
  } catch (error) {
    throw error;
  }
};

export const generateKey = async (name: string) => {
  try {
    const code = generateApiKey({
      method: "uuidv5",
      name: name,
      namespace: generateApiKey({ method: "uuidv4" }).toString(),
    });
    return code.toString();
  } catch (error) {
    throw error;
  }
};

export const generateCustomerApiKey = async (user_id: string, name: string) => {
  try {
    const code = generateApiKey({
      method: "uuidv5",
      name: user_id + "-" + name,
      namespace: generateApiKey({ method: "uuidv4" }).toString(),
    });
    return code.toString();
  } catch (error) {
    throw error;
  }
};

export const generateCustomerSignature = async (
  user_id: string,
  user_api_key_name: string
) => {
  try {
    let signOptions: jwt.SignOptions = {
      algorithm: "HS256",
    };
    return jwt.sign(
      { user_id: user_id, user_api_key_name: user_api_key_name },
      process.env.CUSTOMER_SECRET!,
      signOptions
    );
  } catch (error) {
    throw error;
  }
};

export const verifyCustomerSignature = async (token: string) => {
  try {
    let verifyOptions: jwt.VerifyOptions = {
      algorithms: ["HS256"],
    };
    return jwt.verify(token, process.env.CUSTOMER_SECRET!, verifyOptions);
  } catch (error) {
    throw error;
  }
};

export const passwordValidation = async (password: string) => {
  try {
    const charactorLongCase = new RegExp(/^.{8,16}$/);
    const oneUpperCase = new RegExp(/(?=.*[A-Z])/);
    const oneLowerCase = new RegExp(/(?=.*[a-z])/);
    const oneDigitCase = new RegExp(/(?=.*[0-9])/);
    if (!charactorLongCase.test(password)) {
      throw new CustomError(
        enums.responseCode.CharactorLongCase,
        enums.responseMessage.CharactorLongCase
      );
    }
    if (!oneUpperCase.test(password)) {
      throw new CustomError(
        enums.responseCode.OneUpperCase,
        enums.responseMessage.OneUpperCase
      );
    }
    if (!oneLowerCase.test(password)) {
      throw new CustomError(
        enums.responseCode.OneLowerCase,
        enums.responseMessage.OneLowerCase
      );
    }
    if (!oneDigitCase.test(password)) {
      throw new CustomError(
        enums.responseCode.OneDigitCase,
        enums.responseMessage.OneDigitCase
      );
    }
  } catch (error) {
    throw error;
  }
};

export const sendMail = async (
  from: string,
  to: string,
  subject: string,
  html: string
) => {
  try {
    const _vaultService = new VaultService();
    const smtpServer = await _vaultService.getSMTPServer();

    let transporter = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: process.env.MAIL_PORT,
      secure: false, // true for 465, false for other ports
      auth: {
        user: smtpServer.data.smtpUser,
        pass: smtpServer.data.smtpPass,
      },
    } as SMTPTransport.Options);
    let info = await transporter.sendMail({
      from: from,
      to: to,
      subject: subject,
      html: html,
    });
    return info;
  } catch (error) {
    throw error;
  }
};

export const saveToDatabase = (DB: any) => {
  writeFileSync("./src/repositories/db.json", JSON.stringify(DB, null, 2), {
    encoding: "utf-8",
  });
};
