import * as enums from "../utilities/enums";
import * as logs from "../utilities/logs";
import { LogsRepository } from "../repositories/logsRepository";

// @singleton()
// export class LogsUtil {
//   readonly _logsRepository: LogsRepository;
//   constructor(
//     logsRepository: LogsRepository,

//   ) {
//     this._logsRepository = logsRepository;
//   }

//   async writeLogsDB(
//     logsType: enums.logsType,
//     logsName: string,
//     logsMessage: string
//     ) {
//     try {
//       this._logsRepository.create(logsType,logsName,logsMessage);
//     } catch (error) {
//       throw error;
//     }
//   }

// }

export const writeLogsDB = async (
  logsType: enums.logsType,
  logsName: string,
  logsMessage: string
) => {
  try {
    let logRepo = new LogsRepository();
    logRepo.create(logsType, logsName, logsMessage);
  } catch (error) {
    console.log(error);
    // throw error;
  }
};

export const logInfo = async (logsName: string, logsMessage: string) => {
  try {
    logs.writeLogsDB(enums.logsType.INFO, logsName, logsMessage);
    console.log(logsMessage);
    return;
  } catch (error) {
    console.log(error);
    // throw error;
  }
};

export const logError = async (logsName: string, logsMessage: string) => {
  try {
    logs.writeLogsDB(enums.logsType.ERROR, logsName, logsMessage);
    console.log(logsMessage);
    return;
  } catch (error) {
    console.log(error);
    // throw error;
  }
};

export const logDebug = async (logsName: string, logsMessage: string) => {
  try {
    logs.writeLogsDB(enums.logsType.DEBUG, logsName, logsMessage);
    console.log(logsMessage);
    return;
  } catch (error) {
    console.log(error);
    // throw error;
  }
};
