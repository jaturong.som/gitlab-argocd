import {
  Controller,
  Body,
  Post,
  Route,
  Tags,
  Get,
  Put,
  Delete,
  Queries,
  Path,
  Security,
  Request,
} from "tsoa";
import * as Enums from "../utilities/enums";
import * as BaseModels from "../models/baseModels";
import * as ConfigModels from "../models/configModels";
import { ConfigService } from "../services/configService";
import { injectable } from "tsyringe";

@Tags("Configurations")
@Security("api_key")
@Route("/api/v1/config")
@injectable()
export class ConfigController extends Controller {
  readonly _configService: ConfigService;
  constructor(configService: ConfigService) {
    super();
    this._configService = configService;
  }

  @Get("/asset/list")
  public async getAssets(
    @Queries() req: ConfigModels.RequestGetAssetsInVaultAccount
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.getAssetsInVaultAccount();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/asset/create")
  public async createAsset(
    @Body() req: ConfigModels.RequestCreateAssetInVaultAccount,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.createAssetInVaultAccount(
        req.asset_code,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/asset-rate/list")
  public async getAssetRates(
    @Queries() req: ConfigModels.RequestGetAssetRates
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.getAssetRates();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/asset-rate/{id}")
  public async getAssetRateById(
    @Path() id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.getAssetRateById(id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/asset-rate-by-code/{asset_code}")
  public async getAssetRateByAssetCode(
    @Path() asset_code: string
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.getAssetRateByAssetCode(
        asset_code
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/asset-rate/create")
  public async createAssetRate(
    @Body() req: ConfigModels.RequestCreateAssetRate,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.createAssetRate(
        req.asset_code,
        req.buying,
        req.selling,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Put("/asset-rate/{id}")
  public async updateAssetRate(
    @Path() id: number,
    @Body() req: ConfigModels.RequestUpdateAssetRate,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.updateAssetRate(
        id,
        req.buying,
        req.selling,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Delete("/asset-rate/{id}")
  public async deleteAssetRate(
    @Path() id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.deleteAssetRate(id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/network/list")
  public async getNetworks(
    @Queries() req: ConfigModels.RequestGetNetworks
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.getNetworks();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/fb-asset/list/{network_id}")
  public async getFBAssets(
    @Path() network_id: number,
    @Queries() req: ConfigModels.RequestGetAssets
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.getAssets(network_id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/dropdown/purpose-of-transfer")
  public async getPurposeOfTransfer(): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.getPurposeOfTransfer();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/dropdown/relationship-to-receiver")
  public async getRelationshipToReceiver(): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.getRelationshipToReceiver();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/dropdown/{type}")
  public async getDropdownListByType(
    @Path() type: string
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._configService.getDropdownListByType(type);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }
}
