import { Controller, Route, Tags, Get, Security } from "tsoa";
import * as Enums from "../utilities/enums";
import * as BaseModels from "../models/baseModels";
import { MasterService } from "../services/masterService";
import { injectable } from "tsyringe";

@Tags("Master Data")
@Security("api_key")
@Route("/api/v1/master")
@injectable()
export class MasterController extends Controller {
  readonly _masterService: MasterService;
  constructor(masterService: MasterService) {
    super();
    this._masterService = masterService;
  }

  @Get("/country/list")
  public async getCountries(): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._masterService.getCountryList();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/currency/list")
  public async getCurrencies(): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._masterService.getCurrencyList();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/nationality/list")
  public async getNationality(): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._masterService.getNationalityList();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }
}
