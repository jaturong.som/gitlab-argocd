import { Controller, Post, Route, Tags, Request } from "tsoa";
import * as Enums from "../utilities/enums";
import * as BaseModels from "../models/baseModels";
import { FireblocksWebhookService } from "../services/fireblocksWebhookService";
import { injectable } from "tsyringe";

@Tags("Fireblocks Webhook")
@Route("/api/v1/webhook")
@injectable()
export class FireblocksWebhookController extends Controller {
  readonly _fireblocksWebhookService: FireblocksWebhookService;
  constructor(fireblocksWebhookService: FireblocksWebhookService) {
    super();
    this._fireblocksWebhookService = fireblocksWebhookService;
  }

  @Post("/notification")
  public async notification(
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      // const message = JSON.stringify(req.body);
      // const signature = req.headers["fireblocks-signature"];
      // const verifier = crypto.createVerify("RSA-SHA512");
      // verifier.write(message);
      // verifier.end();

      // const isVerified = verifier.verify(publicKey, signature, "base64");
      // console.log("Verified:", isVerified);
      await this._fireblocksWebhookService.monitoringIncomingTransaction(
        reqIncomingMessage.body
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
      };
    } catch (error) {
      console.log(error);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
      };
    }
  }
}
