import {
  Controller,
  Body,
  Post,
  Route,
  Tags,
  Get,
  Queries,
  Security,
  Request,
} from "tsoa";

import { FireblocksNetworkLinkService } from "../services/fireblocksNetworkLinkService";
import * as FireblocksNetworkLinkModels from "../models/fireblocksNetworkLinkModels";
import { injectable } from "tsyringe";

@Tags("Fireblocks Network Link")
@Security("network_link")
@Route("/fireblocks/v1")
@injectable()
export class FireblocksNetworkLinkController extends Controller {
  readonly _fireblocksNetworkLinkService: FireblocksNetworkLinkService;
  constructor(fireblocksNetworkLinkService: FireblocksNetworkLinkService) {
    super();
    this._fireblocksNetworkLinkService = fireblocksNetworkLinkService;
  }

  @Get("/accounts")
  public async getAllAccountTypes(
    @Queries() req: FireblocksNetworkLinkModels.RequestGetAllAccountTypes,
    @Request() reqIncomingMessage: any
  ) {
    try {
      return await this._fireblocksNetworkLinkService.getAllAccountTypes(
        reqIncomingMessage.user.user_id
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/depositAddress")
  public async getDepositWalletAddress(
    @Queries() req: FireblocksNetworkLinkModels.RequestGetDepositWalletAddress,
    @Request() reqIncomingMessage: any
  ) {
    try {
      return await this._fireblocksNetworkLinkService.getDepositWalletAddress(
        reqIncomingMessage.user.user_id,
        req.accountType,
        req.coinSymbol,
        req.network
      );
    } catch (error) {
      throw error;
    }
  }

  @Post("/depositAddress")
  public async createDepositWalletAddress(
    @Body() req: FireblocksNetworkLinkModels.RequestCreateDepositWalletAddress,
    @Request() reqIncomingMessage: any
  ) {
    try {
      return await this._fireblocksNetworkLinkService.createDepositWalletAddress(
        reqIncomingMessage.user.user_id,
        req.accountType,
        req.coinSymbol,
        req.network
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/withdrawalFee")
  public async getWithDrawalFee(
    @Queries() req: FireblocksNetworkLinkModels.RequestGetWithDrawalFee
  ) {
    try {
      return await this._fireblocksNetworkLinkService.getWithDrawalFee(
        req.transferAmount,
        req.coinSymbol,
        req.network
      );
    } catch (error) {
      throw error;
    }
  }

  @Post("/withdraw")
  public async createWithDraw(
    @Body() req: FireblocksNetworkLinkModels.RequestCreateWithDraw,
    @Request() reqIncomingMessage: any
  ) {
    try {
      return await this._fireblocksNetworkLinkService.createWithDraw(
        reqIncomingMessage.user.user_id,
        req.amount,
        req.maxFee!,
        req.coinSymbol,
        req.network,
        req.accountType,
        req.toAddress,
        req.tag!,
        req.isGross
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/transactionByID")
  public async getTransactionById(
    @Queries() req: FireblocksNetworkLinkModels.RequestGetTransactionById
  ) {
    try {
      return await this._fireblocksNetworkLinkService.getTransactionById(
        req.transactionID
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/transactionByHash")
  public async getTransactionByHash(
    @Queries() req: FireblocksNetworkLinkModels.RequestGetTransactionByHash
  ) {
    try {
      return await this._fireblocksNetworkLinkService.getTransactionByHash(
        req.txHash,
        req.network
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/transactionHistory")
  public async getTransactionHistory(
    @Queries() req: FireblocksNetworkLinkModels.RequestGetTransactionHistory,
    @Request() reqIncomingMessage: any
  ) {
    try {
      return await this._fireblocksNetworkLinkService.getTransactionHistory(
        req.fromDate,
        req.toDate,
        req.pageSize,
        req.pageCursor,
        req.isSubTransfer,
        req.coinSymbol,
        req.network,
        reqIncomingMessage.user.user_id,
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/supportedAssets")
  public async getSupportedAssets(
    @Queries() req: FireblocksNetworkLinkModels.RequestGetSupportedAssets
  ) {
    try {
      return await this._fireblocksNetworkLinkService.getSupportedAssets();
    } catch (error) {
      throw error;
    }
  }

  @Post("/subMainTransfer")
  public async createSubMainTransfer(
    @Body() req: FireblocksNetworkLinkModels.RequestCreateSubMainTransfer
  ) {
    try {
      return await this._fireblocksNetworkLinkService.createSubMainTransfer();
    } catch (error) {
      throw error;
    }
  }

  @Post("/subaccountsTransfer")
  public async createSubAccountsTransfer(
    @Body() req: FireblocksNetworkLinkModels.RequestCreateSubAccountsTransfer
  ) {
    try {
      return await this._fireblocksNetworkLinkService.createSubAccountsTransfer();
    } catch (error) {
      throw error;
    }
  }

  @Post("/internalTransfer")
  public async createInternalTransfer(
    @Body() req: FireblocksNetworkLinkModels.RequestCreateInternalTransfer
  ) {
    try {
      return await this._fireblocksNetworkLinkService.createInternalTransfer();
    } catch (error) {
      throw error;
    }
  }
}
