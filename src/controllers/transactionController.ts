import {
  Controller,
  Body,
  Post,
  Route,
  Tags,
  Get,
  Put,
  Queries,
  Path,
  Security,
  Request,
} from "tsoa";
import * as Enums from "../utilities/enums";
import * as BaseModels from "../models/baseModels";
import * as TransactionModels from "../models/transactionModels";
import { TransactionService } from "../services/transactionService";
import { injectable } from "tsyringe";

@Tags("Transactions")
@Security("api_key")
@Route("/api/v1/transaction")
@injectable()
export class TransactionController extends Controller {
  readonly _transactionService: TransactionService;
  constructor(transactionService: TransactionService) {
    super();
    this._transactionService = transactionService;
  }

  @Get("/admin/list")
  public async getAdminTransactions(
    @Queries() req: TransactionModels.RequestGetAdminTransactions
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._transactionService.getAdminTransactions();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  // @Get("/admin/logs")
  // public async getAdminTransactionLogs(
  //   @Queries() req: TransactionModels.RequestGetAdminTransactionLogs
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._transactionService.getAdminTransactionLogs(
  //       req.page_size,
  //       req.page_cursor,
  //       req.from_date!,
  //       req.to_date!,
  //       req.transaction_type!,
  //       req.status!,
  //       req.asset!
  //     );
  //     return {
  //       code: Enums.responseCode.Success,
  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  @Get("/admin/fireblocks/notifications")
  public async getFireblocksNotificationList(
    @Queries() req: TransactionModels.RequestGetFireblocksNotificationList
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result =
        await this._transactionService.getFireblocksNotificationList(
          req.from_date,
          req.to_date,
          req.status!,
          req.search_text!
        );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Put("/admin/transaction/{id}/submit")
  public async submitBuySellTransaction(
    @Path() id: number,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._transactionService.submitBuySellTransaction(
        id,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/manual-update-transaction-status")
  public async manualUpdateTransactionStatus(
    @Body() req: TransactionModels.RequestManualUpdateTransactionStatus
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result =
        await this._transactionService.manualUpdateTransactionStatus(
          req.fireblock_transaction_id
        );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/manual/freeze")
  public async manualFreezeTransaction(
    @Body() req: TransactionModels.RequestManualFreezeTransaction
  ): Promise<BaseModels.ResponseResult> {
    try {
      await this._transactionService.manualFreezeTransaction(
        req.fb_transaction_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/manual/unfreeze")
  public async manualUnfreezeTransaction(
    @Body() req: TransactionModels.RequestManualUnfreezeTransaction
  ): Promise<BaseModels.ResponseResult> {
    try {
      await this._transactionService.manualUnfreezeTransaction(
        req.fb_transaction_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/admin/withdraw/{id}")
  public async getWithdrawById(
    @Path() id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._transactionService.getWithdrawById(id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/admin/convert/{id}")
  public async getConvertById(
    @Path() id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._transactionService.getConvertById(id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }
}
