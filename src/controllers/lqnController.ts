import { Controller, Route, Tags, Get, Queries, Post, Body } from "tsoa";
import * as Enums from "../utilities/enums";
import * as BaseModels from "../models/baseModels";
import * as LqnModels from "../models/lqnModels";
import { injectable } from "tsyringe";
import { LqnService } from "../services/lqnService";
import { LqnWithdrawService } from "../services/lqnWithdrawService";

@Tags("LQN")
@Route("/api/v1/lqn")
@injectable()
export class LQNController extends Controller {
  readonly _lqnService: LqnService;
  readonly _lqnWithdrawService: LqnWithdrawService;
  constructor(lqnService: LqnService, lqnWithdrawService: LqnWithdrawService) {
    super();
    this._lqnService = lqnService;
    this._lqnWithdrawService = lqnWithdrawService;
  }

  @Get("/dropdown/purpose-of-transfer")
  public async getPurposeTransfer(
    @Queries() req: LqnModels.RequestGetPurposeTransfer
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnService.getCatalogue("POR", "", "", "");
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/dropdown/relationship-to-receiver")
  public async getRelationToReceiver(
    @Queries() req: LqnModels.RequestGetRelationToReceiver
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnService.getCatalogue("REL", "", "", "");
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/dropdown/source-of-fund")
  public async getSourceOfFund(
    @Queries() req: LqnModels.RequestGetSourceOfFund
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnService.getCatalogue("SOF", "", "", "");
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/dropdown/occupation")
  public async getOccupation(
    @Queries() req: LqnModels.RequestGetOccupation
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnService.getCatalogue("OCC", "", "", "");
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/dropdown/document-type")
  public async getDocumentType(
    @Queries() req: LqnModels.RequestGetDocumentType
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnService.getCatalogue("DOC", "", "", "");
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/dropdown/nationality")
  public async getNationality(
    @Queries() req: LqnModels.RequestGetNationalityList
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnWithdrawService.getNationalityList();

      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/dropdown/country")
  public async getCountry(
    @Queries() req: LqnModels.RequestGetCountryList
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnWithdrawService.getCoutryList();

      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/dropdown/bank")
  public async getAgentList(
    @Queries() req: LqnModels.RequestGetBankList
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnWithdrawService.getBankList(
        "B",
        req.country_code
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/catalogue")
  public async getCatalogue(
    @Queries() req: LqnModels.RequestGetCatalogue
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnService.getCatalogue(
        req.catalogue_type,
        req.additional_field_1,
        "",
        ""
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/exchangerate")
  public async getExchangeRate(
    @Queries() req: LqnModels.RequestGetExchangeRate
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnService.getExchangeRate(
        req.transfer_amount,
        req.calc_by,
        req.payout_currency,
        req.payment_mode,
        req.bank_id,
        req.payout_country
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/sendTransaction")
  public async sendTransaction(
    @Body() req: LqnModels.SendTransaction
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnService.sendTransaction(
        req.sender_source_of_fund,
        req.sender_source_of_fund_remarks,
        req.purpose_of_remittance,
        req.purpose_of_remittance_remarks,
        req.sender_beneficiary_relationship,
        req.sender_beneficiary_relationship_remarks,
        req.calc_by,
        req.transfer_amount,
        req.remit_currency,
        req.customer_id,
        req.receiver_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/commitTransaction")
  public async commitTransaction(
    @Body() req: LqnModels.CommitTransaction
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnService.commitTransaction(
        req.transaction_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/queryTransaction")
  public async queryTransaction(
    @Queries() req: LqnModels.QueryTransaction
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnService.queryTransaction(req.pin_number);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/callback")
  public async callback(
    @Body() req: LqnModels.RequestCallback
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._lqnWithdrawService.callback(
        req.agentTxnId,
        req.pinNumber,
        req.status,
        req.message,
        req.notification_ts
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }
}
