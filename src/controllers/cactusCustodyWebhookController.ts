import { Controller, Post, Route, Tags, Request } from "tsoa";
import * as Enums from "../utilities/enums";
import * as BaseModels from "../models/baseModels";
import { CactusCustodyService } from "../services/cactusCustodyService";
import { injectable } from "tsyringe";

@Tags("Cactus Custody Webhook")
@Route("/api/v1/cactus/webhook")
@injectable()
export class CactusCustodyWebhookController extends Controller {
  readonly _cactusCustodyService: CactusCustodyService;
  constructor(cactusCustodyService: CactusCustodyService) {
    super();
    this._cactusCustodyService = cactusCustodyService;
  }

  @Post("/notification")
  public async notification(
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      console.log(reqIncomingMessage);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
      };
    } catch (error) {
      console.log(error);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
      };
    }
  }
}
