import { Controller, Body, Post, Route, Tags, Security } from "tsoa";
import * as CronJobModels from "../models/cronJobModels";
import * as BaseModels from "../models/baseModels";
import * as Enums from "../utilities/enums";
import { CronJobService } from "../jobSchedulers/cronJobService";
import { ChainalysisService } from "../services/chainalysisService";
import { injectable } from "tsyringe";

@Tags("Job Scheduler")
@Security("api_key")
@Route("/api/v1/job")
@injectable()
export class CronJobController extends Controller {
  readonly _cronJobService: CronJobService;
  readonly _chainalysisService: ChainalysisService;
  constructor(
    cronJobService: CronJobService,
    chainalysisService: ChainalysisService
  ) {
    super();
    this._cronJobService = cronJobService;
    this._chainalysisService = chainalysisService;
  }

  @Post("/start")
  public async start(): Promise<BaseModels.ResponseResult> {
    try {
      await this._cronJobService.startJob();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/stop")
  public async stop(): Promise<BaseModels.ResponseResult> {
    try {
      await this._cronJobService.stopJob();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/chainalysis/manual")
  public async manualUpdateTransactionStatus(
    @Body() req: CronJobModels.RequestManualUpdateTransactionStatus
  ): Promise<BaseModels.ResponseResult> {
    try {
      await this._chainalysisService.manualUpdateTransactionStatus(
        req.external_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
      };
    } catch (error) {
      throw error;
    }
  }
}
