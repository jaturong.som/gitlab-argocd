import {
  Controller,
  Body,
  Post,
  Route,
  Tags,
  Get,
  Put,
  Delete,
  Queries,
  Path,
  Security,
  Request,
} from "tsoa";
import * as Enums from "../utilities/enums";
import * as BaseModels from "../models/baseModels";
import * as AdminModels from "../models/adminModels";
import { AdminService } from "../services/adminService";
import { injectable } from "tsyringe";
import { CustomerService } from "../services/customerService";

@Tags("Administration")
@Security("api_key")
@Route("/api/v1/admin")
@injectable()
export class AdminController extends Controller {
  readonly _adminService: AdminService;
  readonly _customerService: CustomerService;
  constructor(adminService: AdminService, customerService: CustomerService) {
    super();
    this._adminService = adminService;
    this._customerService = customerService;
  }

  @Get("/user/list")
  public async getUsers(
    @Queries() req: AdminModels.RequestGetUsers
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getUsers();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/user/active/list")
  public async getUserActiveList(
    @Queries() req: AdminModels.RequestGetUsers
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getUserAcriveList();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/user/{user_id}")
  public async getUserById(
    @Path() user_id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getUserById(user_id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/user/create")
  public async createUser(
    @Body() req: AdminModels.RequestCreateUser,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.createUser(
        req.password,
        req.first_name,
        req.middle_name!,
        req.last_name,
        req.email,
        req.tel,
        req.status,
        req.role_id,
        reqIncomingMessage.user.user_id,
        req.gender,
        req.nationality,
        req.occupation,
        req.date_of_birth,
        {
          line1: req.address.line1,
          line2: req.address !== undefined ? req.address.line2 : undefined,
          city: req.address.city,
          state: req.address.state,
          countryCode: req.address.country_code,
          postcode: req.address.postcode,
          areaTown:
            req.address !== undefined ? req.address.area_town : undefined,
        },
        req.document_type,
        req.document_text,
        req.country_code,
        req.occupation_remark,
        req.document_type_remark!,
        req.document_issue_date!,
        req.document_expired_date!
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Put("/user/{user_id}")
  public async updateUser(
    @Path() user_id: number,
    @Body() req: AdminModels.RequestUpdateUser,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.updateUser(
        user_id,
        req.first_name,
        req.middle_name!,
        req.last_name,
        req.tel,
        req.status,
        req.role_id,
        reqIncomingMessage.user.user_id,
        req.gender,
        req.nationality,
        req.occupation,
        req.date_of_birth,
        {
          line1: req.address.line1,
          line2: req.address !== undefined ? req.address.line2 : undefined,
          city: req.address.city,
          state: req.address.state,
          countryCode: req.address.country_code,
          postcode: req.address.postcode,
          areaTown:
            req.address !== undefined ? req.address.area_town : undefined,
        },
        req.document_type,
        req.document_text,
        req.country_code,
        req.occupation_remark,
        req.document_type_remark!,
        req.document_issue_date!,
        req.document_expired_date!
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Delete("/user/{user_id}")
  public async deleteUser(
    @Path() user_id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.deleteUser(user_id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/role/list")
  public async getRoles(
    @Queries() req: AdminModels.RequestGetRoles
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getRoles();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/active-role/list")
  public async getActiveRoles(
    @Queries() req: AdminModels.RequestGetActiveRoles
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getActiveRoles();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/role/{role_id}")
  public async getRoleById(
    @Path() role_id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getRoleById(role_id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/role/create")
  public async createRole(
    @Body() req: AdminModels.RequestCreateRole,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.createRole(
        req.name,
        req.description!,
        req.status,
        req.menus,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Put("/role/{role_id}")
  public async updateRole(
    @Path() role_id: number,
    @Body() req: AdminModels.RequestUpdateRole,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.updateRole(
        role_id,
        req.name,
        req.description!,
        req.status,
        req.menus,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Delete("/role/{role_id}")
  public async deleteRole(
    @Path() role_id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.deleteRole(role_id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/user/menu/list")
  public async getMenuByUserId(
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getMenuByUserId(
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/user/change-password")
  public async changePassword(
    @Body() req: AdminModels.RequestChangePassword,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.changePassword(
        reqIncomingMessage.user.user_id,
        req.old_password,
        req.new_password,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/user/{user_id}/asset/list")
  public async getUserAssetInVaultAccountByUserId(
    @Path() user_id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result =
        await this._adminService.getUserAssetInVaultAccountByUserId(user_id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/user/reset-password")
  public async resetPassword(
    @Body() req: AdminModels.RequestResetPassword,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.resetPassword(
        req.user_id,
        req.new_password,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Put("/role/{role_id}/menu")
  public async updateRoleMenuByRoleId(
    @Path() role_id: number,
    @Body() req: AdminModels.RequestUpdateRoleMenuByRoleId,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.updateRoleMenuByRoleId(
        role_id,
        req.group_menu_code,
        req.menu_code!,
        req.permission,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/master-menu/list")
  public async getMasterMenu(
    @Queries() req: AdminModels.RequestGetMasterMenu
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getMasterMenu();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/company/crypto-wallet/list")
  public async getCryptoWalletList(
    @Queries() req: AdminModels.RequestGetCryptoWalletList
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getCryptoWalletList();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  // @Get("/company/crypto-wallet/total/{asset_group}")
  // public async getTotalAssetPieChart(
  //   @Path() asset_group: string,
  //   @Queries() req: AdminModels.RequestGetTotalAssetPieChart
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._adminService.getTotalAssetPieChart(
  //       asset_group
  //     );

  //     return {
  //       code: Enums.responseCode.Success,

  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // @Get("/company/fiat/total")
  // public async getTotaFiatPieChart(
  //   @Queries() req: AdminModels.RequestGetTotalFiatPieChart
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._adminService.getTotalFiatPieChart();
  //     return {
  //       code: Enums.responseCode.Success,
  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  @Post("/transaction/create")
  public async createTransaction(
    @Body() req: AdminModels.RequestCreateTransaction,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.createTransaction(
        req.user_id,
        req.type,
        req.asset_code,
        req.total_assets,
        req.total_amount,
        req.input_type,
        req.is_manual_mode,
        req.wallet_network,
        req.wallet_address,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/fiat-account/list")
  public async getFiatAccounts(): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getFiatAccounts();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/fiat-account/user/{user_id}")
  public async getFiatAccountByUser(
    @Path() user_id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getFiatAccountByUser(user_id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Put("/fiat-account/{fiat_id}/favorite")
  public async updateFavoriteFiatAccounts(
    @Path() fiat_id: number,
    @Body() req: AdminModels.RequestUpdateFavoriteFiatAccounts,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.updateFavoriteFiatAccount(
        fiat_id,
        req.is_favorite,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/fiat-account/topup")
  public async createTopupTransaction(
    @Body() req: AdminModels.RequestCreateTopup,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      var vDocuments = [];
      const allowedTypes = [
        "image/jpeg",
        "image/jpg",
        "image/png",
        "image/gif",
        "application/pdf",
      ];
      const maxSize = 10 * 1024 * 1024; // 10MB
      if (reqIncomingMessage.files !== null) {
        if (reqIncomingMessage.files.file_path.name === undefined) {
          var totalSize = 0;
          for (let i = 0; i < reqIncomingMessage.files.file_path.length; i++) {
            const element = reqIncomingMessage.files.file_path[i];
            totalSize += element.size;
            if (!allowedTypes.includes(element.mimetype)) {
              return {
                code: Enums.responseCode.UploadFileInvalidFileType,
                message:
                  Enums.responseMessage.UploadFileInvalidFileType +
                  ": " +
                  element.name,
              };
            }
            if (totalSize > maxSize) {
              return {
                code: Enums.responseCode.UploadFileFileTooLarge,
                message: Enums.responseMessage.UploadFileFileTooLarge,
              };
            }

            vDocuments.push({
              file_name: element.name,
              file_data: element.data,
            });
          }
        } else {
          if (
            !allowedTypes.includes(reqIncomingMessage.files.file_path.mimetype)
          ) {
            return {
              code: Enums.responseCode.UploadFileInvalidFileType,
              message:
                Enums.responseMessage.UploadFileInvalidFileType +
                ": " +
                reqIncomingMessage.files.file_path.name,
            };
          }
          if (reqIncomingMessage.files.file_path.size > maxSize) {
            return {
              code: Enums.responseCode.UploadFileFileTooLarge,
              message: Enums.responseMessage.UploadFileFileTooLarge,
            };
          }

          vDocuments.push({
            file_name: reqIncomingMessage.files.file_path.name,
            file_data: reqIncomingMessage.files.file_path.data,
          });
        }
      }

      const result = await this._adminService.createTopup(
        req.fiat_id,
        req.amount,
        vDocuments,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/fiat-account/topup/{wallet_credit_id}")
  public async getTopupInfo(
    @Path() wallet_credit_id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getTopupInfo(wallet_credit_id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get(
    "/fiat-account/wallet-credit-document/{wallet_credit_document_id}/download"
  )
  public async downloadWalletCreditDocument(
    @Path() wallet_credit_document_id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.downloadWalletCreditDocument(
        wallet_credit_document_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get(
    "/fiat-account/wallet-credit-document/{wallet_credit_document_id}/preview"
  )
  public async previewWalletCreditDocument(
    @Path() wallet_credit_document_id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.previewWalletCreditDocument(
        wallet_credit_document_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/bank-account/user/{user_id}/owner/list")
  public async getOwnerBankAccounts(
    @Path() user_id: number,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getBankAccountsByBankType(
        user_id,
        Enums.bankAccountType.OWNER
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/bank-account/user/{user_id}/other/list")
  public async getOtherBankAccounts(
    @Path() user_id: number,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.getBankAccountsByBankType(
        user_id,
        Enums.bankAccountType.OTHER
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/withdraw/create")
  public async createWithdrawTransaction(
    @Body() req: AdminModels.RequestCreateWithdraw,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.createWithdraw(
        req.sender_source_of_fund,
        req.sender_source_of_fund_remarks!,
        req.purpose_of_remittance,
        req.purpose_of_remittance_remarks!,
        req.sender_beneficiary_relationship,
        req.sender_beneficiary_relationship_remarks!,
        req.transfer_amount,
        req.bank_account_id,
        req.user_id,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/transfer/create")
  public async createTransferTransaction(
    @Body() req: AdminModels.RequestCreateTransfer,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.createTransfer(
        req.sender_source_of_fund,
        req.sender_source_of_fund_remarks!,
        req.purpose_of_remittance,
        req.purpose_of_remittance_remarks!,
        req.sender_beneficiary_relationship,
        req.sender_beneficiary_relationship_remarks!,
        req.transfer_amount,
        req.bank_account_id,
        req.user_id,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/bank-account/create")
  public async createBankAccount(
    @Body() req: AdminModels.RequestCreateBankAccount,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const resultCreated = await this._customerService.createBankAccount(
        req.bank_type,
        req.receiver_first_name!,
        req.receiver_middle_name!,
        req.receiver_last_name!,
        req.receiver_address!,
        req.receiver_date_of_birth!,
        req.receiver_gender!,
        req.receiver_contact_number!,
        req.receiver_state!,
        req.receiver_area_town!,
        req.receiver_city!,
        req.receiver_zip_code!,
        req.receiver_country,
        req.receiver_nationality!,
        req.receiver_id_type!,
        req.receiver_id_type_remarks!,
        req.receiver_id_number!,
        req.receiver_id_issue_date!,
        req.receiver_id_expire_date!,
        req.receiver_email!,
        req.receiver_account_type!,
        req.receiver_occupation!,
        req.receiver_occupation_remarks!,
        req.receiver_district!,
        req.beneficiary_type!,
        req.location_id,
        req.bank_name,
        req.bank_branch_name,
        req.bank_branch_code,
        req.bank_account_number,
        req.swift_code!,
        req.iban!,
        req.user_id,
        reqIncomingMessage.user.user_id
      );

      const result = await this._customerService.getBankAccountById(
        resultCreated
      );

      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }
}
