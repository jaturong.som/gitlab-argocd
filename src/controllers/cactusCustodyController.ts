import {
  Controller,
  Route,
  Tags,
  Get,
  Body,
  Security,
  Path,
  Post,
  Put,
} from "tsoa";
import * as BaseModels from "../models/baseModels";
import * as CactusCustodyModels from "../models/cactusCustodyModels";
import { CactusCustodyService } from "../services/cactusCustodyService";
import { injectable } from "tsyringe";

@Tags("Cactus Custody")
@Security("api_key")
@Route("/api/v1/cactus")
@injectable()
export class CactusCustodyController extends Controller {
  readonly _cactusCustodyService: CactusCustodyService;
  constructor(cactusCustodyService: CactusCustodyService) {
    super();
    this._cactusCustodyService = cactusCustodyService;
  }

  @Get("/wallet/list")
  public async getWallets(): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getWalletList();
    } catch (error) {
      throw error;
    }
  }

  @Get("/wallet/{wallet_code}")
  public async getWalletInfo(
    @Path() wallet_code: string
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getWalletInfo(
        process.env.BID_CUSTOMER!,
        wallet_code
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/coin-infos")
  public async getCoinInfo(): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getCoinInfo();
    } catch (error) {
      throw error;
    }
  }

  @Get("/chain-infos")
  public async getChainInfo(): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getChainInfo();
    } catch (error) {
      throw error;
    }
  }

  @Post("/wallet/create")
  public async createWallet(
    @Body() req: CactusCustodyModels.RequestCreateWallet
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.createWallet(
        process.env.BID_CUSTOMER!,
        req.wallet_type,
        req.number
      );
    } catch (error) {
      throw error;
    }
  }

  @Post("/wallet/{wallet_code}/addresses/apply")
  public async applyNewAddress(
    @Path() wallet_code: string,
    @Body() req: CactusCustodyModels.RequestApplyNewAddress
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.applyNewAddress(
        process.env.BID_CUSTOMER!,
        wallet_code,
        req.address_num,
        req.address_type,
        req.coin_name
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/wallet/{wallet_code}/addresses/{address}")
  public async getAddressInfo(
    @Path() wallet_code: string,
    @Path() address: string
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getAddressInfo(
        process.env.BID_CUSTOMER!,
        wallet_code,
        address
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/wallet/{wallet_code}/addresses")
  public async getAddressList(
    @Path() wallet_code: string
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getAddressList(
        process.env.BID_CUSTOMER!,
        wallet_code
      );
    } catch (error) {
      throw error;
    }
  }

  @Put("/wallet/{wallet_code}/addresses/{address}")
  public async editAddress(
    @Path() wallet_code: string,
    @Path() address: string,
    @Body() req: CactusCustodyModels.RequestEditAddress
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.editAddress(
        process.env.BID_CUSTOMER!,
        wallet_code,
        address,
        req.description
      );
    } catch (error) {
      throw error;
    }
  }

  @Post("/addresses/type/check")
  public async verifyAddressFormat(
    @Body() req: CactusCustodyModels.RequestVerifyAddressFormat
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.verifyAddressFormat(
        req.addresses,
        req.coin_name
      );
    } catch (error) {
      throw error;
    }
  }

  @Post("/estimate-miner-fee")
  public async estimateGasFee(
    @Body() req: CactusCustodyModels.RequestEstimateGasFee
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.estimateGasFee(
        process.env.BID_CUSTOMER!,
        req.from_address,
        req.from_wallet_code,
        req.description,
        req.amount,
        req.dest_address,
        req.memo_type,
        req.memo,
        req.remark
      );
    } catch (error) {
      throw error;
    }
  }

  @Post("/order/create")
  public async createWithdrawalOrder(
    @Body() req: CactusCustodyModels.RequestCreateWithdrawalOrder
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.createWithdrawalOrder(
        process.env.BID_CUSTOMER!,
        req.from_address,
        req.from_wallet_code,
        req.coin_name,
        req.description,
        req.amount,
        req.dest_address,
        req.memo_type,
        req.memo,
        req.remark
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/customize-fee-rate/range")
  public async getWithdrawalRateRange(): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getWithdrawalRateRange();
    } catch (error) {
      throw error;
    }
  }

  @Get("/recommend-fee-rate/list")
  public async getWithdrawalRate(): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getWithdrawalRate();
    } catch (error) {
      throw error;
    }
  }

  @Get("/wallets/{wallet_code}/tx-summaries")
  public async getWalletTransactionSummary(
    @Path() wallet_code: string
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getWalletTransactionSummary(
        process.env.BID_CUSTOMER!,
        wallet_code
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/wallets/{wallet_code}/tx-details")
  public async getTransactionDetails(
    @Path() wallet_code: string
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getTransactionDetails(
        process.env.BID_CUSTOMER!,
        wallet_code
      );
    } catch (error) {
      throw error;
    }
  }

  @Post("/wallets/{wallet_code}/details/{id}")
  public async editTransactionDetailRemark(
    @Path() wallet_code: string,
    @Path() id: number,
    @Body() req: CactusCustodyModels.RequestEditTransactionDetailRemark
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.editTransactionDetailRemark(
        process.env.BID_CUSTOMER!,
        wallet_code,
        id,
        req.remark
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/wallets/{wallet_code}/customize-flow")
  public async getTransactionDetailsForCustomCurrency(
    @Path() wallet_code: string
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getTransactionDetailsForCustomCurrency(
        process.env.BID_CUSTOMER!,
        wallet_code
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/history-asset")
  public async getTotalAssetHistoryNotionalValue(): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getTotalAssetHistoryNotionalValue();
    } catch (error) {
      throw error;
    }
  }

  @Get("/asset")
  public async getCurrentAssetNotionalValue(): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getCurrentAssetNotionalValue();
    } catch (error) {
      throw error;
    }
  }

  @Get("/orders")
  public async getOrdersFilterByCriteria(): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getOrdersFilterByCriteria(
        process.env.BID_CUSTOMER!
      );
    } catch (error) {
      throw error;
    }
  }

  @Get("/orders/{order_no}")
  public async getOrderDetails(
    @Path() order_no: string
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.getOrderDetails(
        process.env.BID_CUSTOMER!,
        order_no
      );
    } catch (error) {
      throw error;
    }
  }

  @Post("/orders/{order_no}/accelerate")
  public async rbf(
    @Path() order_no: string,
    @Body() req: CactusCustodyModels.RequestRBF
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.rbf(
        process.env.BID_CUSTOMER!,
        order_no,
        req.level
      );
    } catch (error) {
      throw error;
    }
  }

  @Post("/orders/{order_no}/cancel")
  public async cancelOrder(
    @Path() order_no: string,
    @Body() req: CactusCustodyModels.RequestCancelOrder
  ): Promise<BaseModels.ResponseResult> {
    try {
      return await this._cactusCustodyService.cancelOrder(
        process.env.BID_CUSTOMER!,
        order_no,
        req.level
      );
    } catch (error) {
      throw error;
    }
  }
}
