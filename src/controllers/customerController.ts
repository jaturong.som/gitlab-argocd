import {
  Controller,
  Body,
  Post,
  Route,
  Tags,
  Get,
  Put,
  Delete,
  Queries,
  Path,
  Security,
  Request,
} from "tsoa";
import * as Enums from "../utilities/enums";
import * as BaseModels from "../models/baseModels";
import * as CustomerModels from "../models/customerModels";
import { CustomerService } from "../services/customerService";
import { injectable } from "tsyringe";

@Tags("Customers")
@Security("api_key")
@Route("/api/v1/customer")
@injectable()
export class CustomerController extends Controller {
  readonly _customerService: CustomerService;
  constructor(customerService: CustomerService) {
    super();
    this._customerService = customerService;
  }

  @Get("/api-key/list")
  public async getUserApiKeys(
    @Queries() req: CustomerModels.RequestGetUserApiKeys,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getUserApiKeys(
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/api-key/{id}")
  public async getUserApiKeyById(
    @Path() id: number,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getUserApiKeyById(id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/api-key/create")
  public async createUserApiKey(
    @Body() req: CustomerModels.RequestCreateUserApiKey,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.createUserApiKey(
        reqIncomingMessage.user.user_id,
        req.name,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Put("/api-key/disable/{id}")
  public async disableUserApiKey(
    @Path() id: number,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.enableOrDisableUserApiKey(
        reqIncomingMessage.user.user_id,
        id,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Put("/api-key/all/disable")
  public async disableAllUserApiKeyByUserId(
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.disableAllUserApiKeyByUserId(
        reqIncomingMessage.user.user_id,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/account/asset/request")
  public async requestAccessAsset(
    @Body() req: CustomerModels.RequestRequestAccessAsset,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.requestAccessAsset(
        reqIncomingMessage.user.user_id,
        req.asset_code
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/fiat-account/list")
  public async getUserFiatAccounts(
    @Queries() req: CustomerModels.RequestGetUserFiatAccounts,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getUserFiatAccounts(
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  // @Get("/bank-account/list")
  // public async getBankAccounts(
  //   @Request() reqIncomingMessage: any
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._customerService.getBankAccounts(
  //       reqIncomingMessage.user.user_id
  //     );
  //     return {
  //       code: Enums.responseCode.Success,
  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  //deprecate
  @Get("/bank-account/country/list")
  public async getBankAccountCoutries(
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getBankAccountCoutries();
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/bank-account/owner/list")
  public async getOwnerBankAccounts(
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getBankAccountsByBankType(
        reqIncomingMessage.user.user_id,
        Enums.bankAccountType.OWNER
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/bank-account/other/list")
  public async getOtherBankAccounts(
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getBankAccountsByBankType(
        reqIncomingMessage.user.user_id,
        Enums.bankAccountType.OTHER
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/bank-account/{id}")
  public async getBankAccountById(
    @Path() id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getBankAccountById(id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  // @Get("/bank-account/default/{id}")
  // public async getDefaultBankAccountByUser(
  //   @Path() id: number
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._customerService.getDefaultBankAccountByUser(
  //       id
  //     );
  //     return {
  //       code: Enums.responseCode.Success,
  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  @Post("/bank-account/create")
  public async createBankAccount(
    @Body() req: CustomerModels.RequestCreateBankAccount,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const resultCreated = await this._customerService.createBankAccount(
        req.bank_type,
        req.receiver_first_name!,
        req.receiver_middle_name!,
        req.receiver_last_name!,
        req.receiver_address!,
        req.receiver_date_of_birth!,
        req.receiver_gender!,
        req.receiver_contact_number!,
        req.receiver_state!,
        req.receiver_area_town!,
        req.receiver_city!,
        req.receiver_zip_code!,
        req.receiver_country,
        req.receiver_nationality!,
        req.receiver_id_type!,
        req.receiver_id_type_remarks!,
        req.receiver_id_number!,
        req.receiver_id_issue_date!,
        req.receiver_id_expire_date!,
        req.receiver_email!,
        req.receiver_account_type!,
        req.receiver_occupation!,
        req.receiver_occupation_remarks!,
        req.receiver_district!,
        req.beneficiary_type!,
        req.location_id,
        req.bank_name,
        req.bank_branch_name,
        req.bank_branch_code,
        req.bank_account_number,
        req.swift_code!,
        req.iban!,
        reqIncomingMessage.user.user_id,
        reqIncomingMessage.user.user_id
      );

      const result = await this._customerService.getBankAccountById(
        resultCreated
      );

      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  // @Post("/bank-account/owner/create")
  // public async createOwnerBankAccount(
  //   @Body() req: CustomerModels.RequestCreateOwnerBankAccount,
  //   @Request() reqIncomingMessage: any
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._customerService.createOwnerBankAccount(
  //       reqIncomingMessage.user.user_id,
  //       req.bank_id,
  //       req.bank_code,
  //       req.bank_name,
  //       req.account_no,
  //       req.account_name,
  //       req.swift_code!,
  //       req.iban!,
  //       reqIncomingMessage.user.user_id
  //     );
  //     return {
  //       code: Enums.responseCode.Success,
  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // @Post("/bank-account/other/create")
  // public async createOtherBankAccount(
  //   @Body() req: CustomerModels.RequestCreateOtherBankAccount,
  //   @Request() reqIncomingMessage: any
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._customerService.createOtherBankAccount(
  //       reqIncomingMessage.user.user_id,
  //       req.bank_code,
  //       req.bank_name,
  //       req.account_no,
  //       req.account_name,
  //       req.swift_code!,
  //       req.iban!,
  //       req.form_name,
  //       req.branch_code!,
  //       req.first_name,
  //       req.last_name,
  //       req.nationality!,
  //       req.date_of_birth!,
  //       req.mobile_number!,
  //       reqIncomingMessage.user.user_id,
  //       {
  //         line1: req.address !== undefined ? req.address.line1 : undefined,
  //         line2: req.address !== undefined ? req.address.line2 : undefined,
  //         city: req.address !== undefined ? req.address.city : undefined,
  //         state: req.address !== undefined ? req.address.state : undefined,
  //         countryCode:
  //           req.address !== undefined ? req.address.country_code : undefined,
  //         postcode:
  //           req.address !== undefined ? req.address.postcode : undefined,
  //       }
  //     );
  //     return {
  //       code: Enums.responseCode.Success,
  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // @Put("/bank-account/owner/{id}")
  // public async updateOwnerBankAccount(
  //   @Path() id: number,
  //   @Body() req: CustomerModels.RequestUpdateOwnerBankAccount,
  //   @Request() reqIncomingMessage: any
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._customerService.updateOwnerBankAccount(
  //       id,
  //       reqIncomingMessage.user.user_id,
  //       req.bank_code,
  //       req.bank_name,
  //       req.account_no,
  //       req.account_name,
  //       req.swift_code,
  //       req.iban,
  //       reqIncomingMessage.user.user_id
  //     );
  //     return {
  //       code: Enums.responseCode.Success,
  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // @Put("/bank-account/other/{id}")
  // public async updateOtherBankAccount(
  //   @Path() id: number,
  //   @Body() req: CustomerModels.RequestUpdateOtherBankAccount,
  //   @Request() reqIncomingMessage: any
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._customerService.updateOtherBankAccount(
  //       id,
  //       reqIncomingMessage.user.user_id,
  //       req.bank_code,
  //       req.bank_name,
  //       req.account_no,
  //       req.account_name,
  //       req.swift_code!,
  //       req.iban!,
  //       req.branch_code!,
  //       req.first_name,
  //       req.last_name,
  //       req.nationality!,
  //       req.date_of_birth!,
  //       req.mobile_number!,
  //       reqIncomingMessage.user.user_id,
  //       {
  //         addressId:
  //           req.address !== undefined ? req.address.address_id : undefined,
  //         line1: req.address !== undefined ? req.address.line1 : undefined,
  //         line2: req.address !== undefined ? req.address.line2 : undefined,
  //         city: req.address !== undefined ? req.address.city : undefined,
  //         state: req.address !== undefined ? req.address.state : undefined,
  //         countryCode:
  //           req.address !== undefined ? req.address.country_code : undefined,
  //         postcode:
  //           req.address !== undefined ? req.address.postcode : undefined,
  //       }
  //     );
  //     return {
  //       code: Enums.responseCode.Success,
  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // @Put("/bank-account/{id}/default")
  // public async updateDefaultBankAccount(
  //   @Path() id: number,
  //   @Body() req: CustomerModels.RequestUpdateDefaultBankAccount,
  //   @Request() reqIncomingMessage: any
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._customerService.updateDefaultBankAccount(
  //       id,
  //       reqIncomingMessage.user.user_id,
  //       reqIncomingMessage.user.user_id
  //     );
  //     return {
  //       code: Enums.responseCode.Success,
  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  @Delete("/bank-account/{id}")
  public async deleteBankAccount(
    @Path() id: number,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.deleteBankAccount(
        id,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/crypto-wallet/list")
  public async getCryptoWalletListForUser(
    @Queries() req: CustomerModels.RequestGetCryptoWalletListForUser,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getCryptoWalletList(
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/crypto-wallet/{id}/total/{asset_group}")
  public async getTotalAssetForUser(
    @Path() id: number,
    asset_group: string
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getTotalAssetForUser(
        id,
        asset_group
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/transaction/create")
  public async createTransaction(
    @Body() req: CustomerModels.RequestCreateTransaction,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.createTransaction(
        reqIncomingMessage.user.user_id,
        req.type,
        req.asset_code,
        req.total_assets,
        req.total_amount,
        req.input_type,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/withdraw/list")
  public async getWithdraws(
    @Queries() req: CustomerModels.RequestGetWithdraws,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getWithdraws(
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/withdraw/{id}")
  public async getWithdrawById(
    @Path() id: number
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getWithdrawById(id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/withdraw/create")
  public async createWithdrawTransaction(
    @Body() req: CustomerModels.RequestCreateWithdraw,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.createWithdraw(
        req.sender_source_of_fund,
        req.sender_source_of_fund_remarks!,
        req.purpose_of_remittance,
        req.purpose_of_remittance_remarks!,
        req.sender_beneficiary_relationship,
        req.sender_beneficiary_relationship_remarks!,
        req.transfer_amount,
        req.bank_account_id,
        reqIncomingMessage.user.user_id,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Post("/transfer/create")
  public async createTransferTransaction(
    @Body() req: CustomerModels.RequestCreateTransfer,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.createTransfer(
        req.sender_source_of_fund,
        req.sender_source_of_fund_remarks!,
        req.purpose_of_remittance,
        req.purpose_of_remittance_remarks!,
        req.sender_beneficiary_relationship,
        req.sender_beneficiary_relationship_remarks!,
        req.transfer_amount,
        req.bank_account_id,
        reqIncomingMessage.user.user_id,
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/transaction/list")
  public async getTransactions(
    @Queries() req: CustomerModels.RequestGetTransactions,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getTransactions(
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/profile")
  public async getUserProfile(
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getUserProfile(
        reqIncomingMessage.user.user_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  // @Put("/profile")
  // public async updateUserProfile(
  //   @Body() req: CustomerModels.RequestUpdateCustomerProfile,
  //   @Request() reqIncomingMessage: any
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._customerService.updateUserProfile(
  //       req,
  //       reqIncomingMessage.user.user_id
  //     );
  //     return {
  //       code: Enums.responseCode.Success,
  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // @Put("/address")
  // public async updateUserAddress(
  //   @Body() req: CustomerModels.RequestUpdateCustomerAddress,
  //   @Request() reqIncomingMessage: any
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._customerService.updateUserAddress(
  //       req,
  //       reqIncomingMessage.user.user_id
  //     );
  //     return {
  //       code: Enums.responseCode.Success,
  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  @Get("/{id}/network/list")
  public async getNetworksByUserId(
    @Path() id: number,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getNetworksByUserId(id);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  @Get("/{id}/network/{network_id}/asset/list")
  public async getAssetsByUserIdNetworkId(
    @Path() id: number,
    @Path() network_id: number,
    @Request() reqIncomingMessage: any
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._customerService.getAssetsByUserIdNetworkId(
        id,
        network_id
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }
}
