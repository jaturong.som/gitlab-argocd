import { Controller, Post, Route, Tags, Body } from "tsoa";
import * as Enums from "../utilities/enums";
import * as CustomerModels from "../models/customerModels";
import * as utils from "../utilities/utils";
import { injectable } from "tsyringe";
import { CustomerService } from "../services/customerService";
import { AdminService } from "../services/adminService";

@Tags("ForgotPassword")
@Route("/api/v1/forgotpassword")
@injectable()
export class ForgotPasswordController extends Controller {
	readonly _customerService: CustomerService;
	readonly _adminService: AdminService;
	constructor(
		customerService: CustomerService,
		adminService: AdminService,
	) {
		super();
		this._customerService = customerService;
		this._adminService = adminService;
	}
	
	@Post("")
	public async submitForgotPassword(
		@Body() req: CustomerModels.RequestForgotPassword,
	) {
		try {
			let result = await this._customerService.getUserProfileByEmail(
				req.email
			);
			if (result.user_id > 0) {
				result = await this._adminService.setUserLogin(result);
				return await utils.sendMail(
					process.env.MAIL_FROM_ADD_ASSET!,
					req.email,
					`Forget Password ${result.user_name}`,
					`
					  <a href="${process.env.APP_URL}?token=${result.token}">Reset Passsword</a>
					`
				  );
			}
			return {
				code: Enums.responseCode.Success,
				message: Enums.responseMessage.Success,
				result: {},
			};
		} 	catch (error) {
			throw error;
		}
	}
}