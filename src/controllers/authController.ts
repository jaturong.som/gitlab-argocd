import { Controller, Body, Post, Route, Tags } from "tsoa";
import * as BaseModels from "../models/baseModels";
import * as AuthModels from "../models/authModels";
import * as Enums from "../utilities/enums";
import { AdminService } from "../services/adminService";
import { injectable } from "tsyringe";

@Tags("Authentication")
@Route("/api/v1/auth")
@injectable()
export class AuthController extends Controller {
  readonly _adminService: AdminService;
  constructor(adminService: AdminService) {
    super();
    this._adminService = adminService;
  }

  @Post("/login")
  public async login(
    @Body() req: AuthModels.RequestLogin
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.login(
        req.user_name,
        req.password
      );
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }

  // @Post("/register")
  // public async register(
  //   @Body() req: AuthModels.RequestRegister
  // ): Promise<BaseModels.ResponseResult> {
  //   try {
  //     const result = await this._adminService.createUser(
  //       req.password,
  //       req.first_name,
  //       req.middle_name!,
  //       req.last_name,
  //       req.email,
  //       req.tel!,
  //       "I",
  //       0,
  //       "0",
  //       undefined,
  //       undefined,
  //       undefined,
  //       undefined,
  //       undefined
  //     );
  //     return {
  //       code: Enums.responseCode.Success,
  //       message: Enums.responseMessage.Success,
  //       result: result,
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  @Post("/refresh")
  public async refresh(
    @Body() req: AuthModels.RequestRefreshToken
  ): Promise<BaseModels.ResponseResult> {
    try {
      const result = await this._adminService.refresh(req.refresh_token);
      return {
        code: Enums.responseCode.Success,
        message: Enums.responseMessage.Success,
        result: result,
      };
    } catch (error) {
      throw error;
    }
  }
}
