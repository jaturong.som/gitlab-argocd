import { singleton } from "tsyringe";
import CustomError from "../middlewares/customError";
import { CountryRepository } from "../repositories/countryRepository";
import { LqnTransactionRepository } from "../repositories/lqnTransactionRepository";
import { LqnUserRepository } from "../repositories/lqnUserRepository";
import { NationalityRepository } from "../repositories/nationalityRepository";
import { WithdrawRepository } from "../repositories/withdrawRepository";
import * as enums from "../utilities/enums";
import { LqnService } from "./lqnService";

@singleton()
export class LqnWithdrawService {
  readonly _lqnUserRepository: LqnUserRepository;
  readonly _lqnService: LqnService;
  readonly _countryRepository: CountryRepository;
  readonly _nationlityRepository: NationalityRepository;
  readonly _lqnTransactionRepository: LqnTransactionRepository;
  readonly _withdrawRepository: WithdrawRepository;
  constructor(
    lqnUserRepository: LqnUserRepository,
    lqnService: LqnService,
    nationlityRepository: NationalityRepository,
    countryRepository: CountryRepository,
    lqnTransactionRepository: LqnTransactionRepository,
    withdrawRepository: WithdrawRepository
  ) {
    this._lqnUserRepository = lqnUserRepository;
    this._lqnService = lqnService;
    this._nationlityRepository = nationlityRepository;
    this._countryRepository = countryRepository;
    this._lqnTransactionRepository = lqnTransactionRepository;
    this._withdrawRepository = withdrawRepository;
  }

  async CreateSender(
    senderFirstName: string,
    senderMiddleName: string,
    senderLastName: string,
    senderGender: string,
    senderMobile: string,
    senderNationality: string,
    senderIdType: string,
    senderIdNumber: string,
    senderIdIssueCountry: string,
    senderIdIssueDate: string,
    senderIdExpireDate: string,
    senderDateOfBirth: string,
    senderOccupation: string,
    senderEmail: string,
    senderAddress: string,
    senderCity: string,
    senderState: string,
    senderZipCode: string,
    senderCountry: string,
    logon: string,
    userId: number,
    currencyCode: string,
    senderOccupationRemarks?: string,
    senderIdTypeRemarks?: string
  ) {
    try {
      var senderLqn = await this._lqnService.createSender(
        "I",
        senderFirstName,
        senderMiddleName,
        senderLastName,
        senderGender,
        senderAddress,
        senderCity,
        senderState,
        senderZipCode,
        senderCountry,
        senderMobile,
        senderNationality,
        senderIdType,
        senderIdNumber,
        senderIdIssueCountry,
        senderIdIssueDate,
        senderIdExpireDate,
        senderDateOfBirth,
        senderOccupation,
        "",
        "",
        senderEmail,
        "",
        "",
        "",
        "",
        senderOccupationRemarks!,
        senderIdTypeRemarks!,
        "",
        "",
        "",
        "",
        ""
      );

      if (senderLqn.code != "0") {
        throw new CustomError(senderLqn.code, senderLqn.message);
      }

      return await this._lqnUserRepository.create(
        enums.lqnUserType.SENDER,
        senderFirstName,
        senderMiddleName,
        senderLastName,
        senderGender,
        senderMobile,
        senderNationality,
        senderIdType,
        senderIdTypeRemarks!,
        senderIdNumber,
        senderIdIssueCountry,
        senderIdIssueDate,
        senderIdExpireDate,
        senderDateOfBirth,
        senderOccupation,
        senderOccupationRemarks!,
        senderEmail,
        "",
        "",
        "",
        senderLqn.customerId,
        logon,
        "",
        currencyCode,
        enums.status.ACTIVE,
        userId,
        "I",
        0,
        senderCountry
      );
    } catch (error) {
      throw error;
    }
  }

  async UpdateSender(
    lqnUserId: string,
    senderId: string,
    senderFirstName: string,
    senderMiddleName: string,
    senderLastName: string,
    senderGender: string,
    senderMobile: string,
    senderNationality: string,
    senderIdType: string,
    senderIdNumber: string,
    senderIdIssueCountry: string,
    senderIdIssueDate: string,
    senderIdExpireDate: string,
    senderDateOfBirth: string,
    senderOccupation: string,
    senderEmail: string,
    senderAddress: string,
    senderCity: string,
    senderState: string,
    senderZipCode: string,
    senderCountry: string,
    logon: string,
    isUpdate: boolean,
    userId: number,
    senderOccupationRemarks?: string,
    senderIdTypeRemarks?: string
  ) {
    try {
      var countries = await this._countryRepository.getInfoByCode(
        senderCountry
      );
      if (countries.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const countryInfo = countries[0];

      if (isUpdate) {
        var senderLqn = await this._lqnService.updateSender(
          senderId,
          senderGender,
          senderAddress,
          senderCity,
          senderState,
          senderZipCode,
          senderCountry,
          senderMobile,
          senderNationality,
          senderIdType,
          senderIdNumber,
          senderIdIssueCountry,
          senderIdIssueDate,
          senderIdExpireDate,
          senderDateOfBirth,
          senderOccupation,
          "", //senderSecondaryIdType
          "", //senderSecondaryIdNumber
          senderEmail,
          "", //senderNativeFirstname
          "", //senderNativeMiddlename
          "", //senderNativeLastname
          "", //senderNativeAddress
          senderOccupationRemarks!,
          senderIdTypeRemarks!,
          "", //authorizedPersonIdType
          "", //authorizedPersonIdNumber
          "", //authorizedPersonFirstName
          "", //authorizedPersonLastName
          "" //invoiceOrPayrollNumber
        );

        if (senderLqn.code != "0") {
          throw new CustomError(senderLqn.code, senderLqn.message);
        }

        var sender = await this._lqnUserRepository.update(
          lqnUserId,
          senderGender,
          senderNationality,
          senderMobile,
          senderIdType,
          senderIdTypeRemarks!,
          senderIdNumber,
          senderIdIssueCountry,
          senderIdIssueDate,
          senderIdExpireDate,
          senderDateOfBirth,
          senderOccupation,
          senderOccupationRemarks!,
          senderEmail,
          "", //nativefirstName
          "", //nativeMiddleName
          "", //nativeLastName
          senderLqn.customerId,
          logon,
          countryInfo.currency_code,
          senderCountry
        );

        return sender.lqn_user_profile_id;
      } else {
        var senderLqn = await this._lqnService.createSender(
          "I",
          senderFirstName,
          senderMiddleName,
          senderLastName,
          senderGender,
          senderAddress,
          senderCity,
          senderState,
          senderZipCode,
          senderCountry,
          senderMobile,
          senderNationality,
          senderIdType,
          senderIdNumber,
          senderIdIssueCountry,
          senderIdIssueDate,
          senderIdExpireDate,
          senderDateOfBirth,
          senderOccupation,
          "", //senderSecondaryIdType
          "", //senderSecondaryIdNumber
          senderEmail,
          "", //senderNativeFirstname
          "", //senderNativeMiddlename
          "", //senderNativeLastname
          "", //senderNativeAddress
          senderOccupationRemarks!,
          senderIdTypeRemarks!,
          "", //authorizedPersonIdType
          "", //authorizedPersonIdNumber
          "", //authorizedPersonFirstName
          "", //authorizedPersonLastName
          "" //invoiceOrPayrollNumber
        );

        if (senderLqn.code != "0") {
          throw new CustomError(senderLqn.code, senderLqn.message);
        }

        var sender = await this._lqnUserRepository.create(
          enums.lqnUserType.SENDER,
          senderFirstName,
          senderMiddleName,
          senderLastName,
          senderGender,
          senderMobile,
          senderNationality,
          senderIdType,
          senderIdTypeRemarks!,
          senderIdNumber,
          senderIdIssueCountry,
          senderIdIssueDate,
          senderIdExpireDate,
          senderDateOfBirth,
          senderOccupation,
          senderOccupationRemarks!,
          senderEmail,
          "", //nativefirstName
          "", //nativeMiddleName
          "", //nativeLastName
          senderLqn.customerId,
          logon,
          "", //senderRefId
          countryInfo.currency_code,
          enums.status.ACTIVE,
          userId,
          "I",
          0,
          senderCountry
        );

        await this._lqnUserRepository.updateStatus(lqnUserId, "I");

        return sender.lqn_user_profile_id;
      }
    } catch (error) {
      throw error;
    }
  }

  async Createreceiver(
    receiverFirstName: string,
    receiverMiddleName: string,
    receiverLastName: string,
    receiverAddress: string,
    receiverDateOfBirth: string,
    receiverGender: string,
    receiverContactNumber: string,
    receiverState: string,
    receiverAreaTown: string,
    receiverCity: string,
    receiverZipCode: string,
    receiverCountry: string,
    receiverNationality: string,
    receiverIdType: string,
    receiverIdTypeRemarks: string,
    receiverIdNumber: string,
    receiverIdIssueDate: string,
    receiverIdExpireDate: string,
    receiverEmail: string,
    receiverAccountType: string,
    receiverOccupation: string,
    receiverOccupationRemarks: string,
    receiverDistrict: string,
    beneficiaryType: string,
    locationId: string,
    bankName: string,
    bankBranchName: string,
    bankBranchCode: string,
    bankAccountNumber: string,
    swiftCode: string,
    iban: string,
    paymentMode: string,
    payoutCurrency: string,
    customerId: string
  ) {
    try {
      var result = await this._lqnService.createReceiver(
        receiverFirstName,
        receiverMiddleName,
        receiverLastName,
        receiverAddress,
        receiverDateOfBirth,
        receiverGender,
        receiverContactNumber,
        receiverState,
        receiverAreaTown,
        receiverCity,
        receiverZipCode,
        receiverCountry,
        receiverNationality,
        receiverIdType,
        receiverIdTypeRemarks,
        receiverIdNumber,
        receiverIdIssueDate,
        receiverIdExpireDate,
        receiverEmail,
        receiverAccountType,
        receiverOccupation,
        receiverOccupationRemarks,
        receiverDistrict,
        beneficiaryType,
        locationId,
        bankName,
        bankBranchName,
        bankBranchCode,
        bankAccountNumber,
        swiftCode,
        iban,
        paymentMode,
        payoutCurrency,
        customerId
      );
      if (result.code != "0") {
        throw new CustomError(result.code, result.message);
      }
      return result;
    } catch (error) {
      throw error;
    }
  }

  async getNationalityList() {
    const lqnCountry = await this._lqnService.getCatalogue("CTY", "", "", "");

    const nationalityList = await this._nationlityRepository.getList();
    var vResult = [];
    for (let i = 0; i < lqnCountry.length; i++) {
      for (let j = 0; j < nationalityList.length; j++) {
        var lqn = lqnCountry[i].value;
        var nationality = nationalityList[j].code;

        if (lqn == nationality) {
          vResult.push(
            await this.setDropdownList(
              nationalityList[j].code,
              nationalityList[j].name
            )
          );
        }
      }
    }
    return vResult;
  }

  async setDropdownList(value: any, text: any) {
    return {
      value: value,
      text: text,
    };
  }

  async getCoutryList() {
    try {
      const lqnCountry = await this._lqnService.getCatalogue("CTY", "", "", "");

      const countryList = await this._countryRepository.getList();
      var vResult = [];
      for (let i = 0; i < lqnCountry.length; i++) {
        for (let j = 0; j < countryList.length; j++) {
          var lqn = lqnCountry[i].value;
          var country = countryList[j].code;

          if (lqn == country) {
            vResult.push(
              await this.setDropdownList(
                countryList[j].code,
                countryList[j].name
              )
            );
          }
        }
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getBankList(paymentMode: string, payoutCountry: string) {
    try {
      const bankList = await this._lqnService.getAgentList(
        paymentMode,
        payoutCountry
      );

      var vResult = [];
      for (let i = 0; i < bankList.length; i++) {
        vResult.push(
          await this.setDropdownList(
            bankList[i].location_id,
            bankList[i].location_name
          )
        );
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async callback(
    agentTxnId: string,
    pinNumber: string,
    lqnStatus: string,
    message: string,
    notification_ts: string
  ) {
    var lqnTransaction =
      await this._lqnTransactionRepository.updateCallbackTransaction(
        pinNumber,
        lqnStatus,
        message
      );

    var status = "";
    if (lqnStatus.toUpperCase() == enums.lqnTransactionStatus.PAID) {
      status = enums.transactionStatus.SUCCESS;
    } else if (
      lqnStatus.toUpperCase() == enums.lqnTransactionStatus.CANCEL ||
      lqnStatus.toUpperCase() == enums.lqnTransactionStatus.CANCEL_HOLD ||
      lqnStatus.toUpperCase() == enums.lqnTransactionStatus.BLOCK
    ) {
      status = enums.transactionStatus.CANCELLED;
    }

    await this._withdrawRepository.update(
      lqnTransaction[0].lqn_transaction_id,
      status
    );

    try {
      return "OK";
    } catch (error) {
      throw error;
    }
  }
}
