import { CompanyRepository } from "../repositories/companyRepository";
import { AssetRateRepository } from "../repositories/assetRateRepository";
import { AssetRepository } from "../repositories/assetRepository";
import { NetworkRepository } from "../repositories/networkRepository";
import { DropdownlistRepository } from "../repositories/dropdownlistRepository";
import { VaultAccountService } from "./vaultAccountService";
import { AssetInVaultAccountService } from "./assetInVaultAccountService";
import { MasterService } from "./masterService";
import * as enums from "../utilities/enums";
import CustomError from "../middlewares/customError";
import { singleton } from "tsyringe";

@singleton()
export class ConfigService {
  readonly _companyRepository: CompanyRepository;
  readonly _assetRateRepository: AssetRateRepository;
  readonly _assetRepository: AssetRepository;
  readonly _networkRepository: NetworkRepository;
  readonly _dropdownlistRepository: DropdownlistRepository;
  readonly _vaultAccountService: VaultAccountService;
  readonly _assetInVaultAccountService: AssetInVaultAccountService;
  readonly _masterService: MasterService;
  constructor(
    companyRepository: CompanyRepository,
    assetRateRepository: AssetRateRepository,
    assetRepository: AssetRepository,
    networkRepository: NetworkRepository,
    dropdownlistRepository: DropdownlistRepository,
    vaultAccountService: VaultAccountService,
    assetInVaultAccountService: AssetInVaultAccountService,
    masterService: MasterService
  ) {
    this._companyRepository = companyRepository;
    this._assetRateRepository = assetRateRepository;
    this._assetRepository = assetRepository;
    this._networkRepository = networkRepository;
    this._dropdownlistRepository = dropdownlistRepository;
    this._vaultAccountService = vaultAccountService;
    this._assetInVaultAccountService = assetInVaultAccountService;
    this._masterService = masterService;
  }

  async getAssetsInVaultAccount() {
    try {
      const companies = await this._companyRepository.getList();
      if (companies.length <= 0) {
        throw new CustomError(
          enums.responseCode.CompanyNotFound,
          enums.responseMessage.CompanyNotFound
        );
      }
      const vaultAccountInfo = await this._vaultAccountService.getVaultAccount(
        enums.accountType.COMPANY,
        companies[0].company_id,
        undefined
      );
      return await this._assetInVaultAccountService.getAssetsInVaultAccount(
        vaultAccountInfo.vault_account_id
      );
    } catch (error) {
      throw error;
    }
  }

  async getAssetInVaultAccountById(assetInVaultAccountId: number) {
    try {
      return await this._assetInVaultAccountService.getAssetInVaultAccountById(
        assetInVaultAccountId
      );
    } catch (error) {
      throw error;
    }
  }

  async createAssetInVaultAccount(assetCode: string, logOnId: string) {
    try {
      const companies = await this._companyRepository.getList();
      if (companies.length <= 0) {
        throw new CustomError(
          enums.responseCode.CompanyNotFound,
          enums.responseMessage.CompanyNotFound
        );
      }
      return await this._assetInVaultAccountService.createAssetInVaultAccount(
        enums.accountType.COMPANY,
        assetCode,
        logOnId,
        parseInt(companies[0].company_id),
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getAssetRates() {
    try {
      const networks = await this._networkRepository.getListForAssetRates();
      if (networks.length <= 0) {
        return [];
      }
      var vResult = [];
      for (let i = 0; i < networks.length; i++) {
        const networkInfo = networks[i];
        const assetRates = await this._assetRateRepository.getListByNetworkId(
          networkInfo.network_id
        );
        var vResultAssetRates = [];
        for (let i = 0; i < assetRates.length; i++) {
          const assetRateInfo = assetRates[i];
          vResultAssetRates.push({
            asset_rate_id: assetRateInfo.asset_rate_id,
            asset_id: assetRateInfo.asset_id,
            asset_code: assetRateInfo.asset_code,
            asset_name: assetRateInfo.asset_name,
            asset_image: assetRateInfo.asset_image,
            buying: assetRateInfo.buying,
            selling: assetRateInfo.selling,
            updated_by: assetRateInfo.updated_by,
            updated_date: assetRateInfo.updated_date,
          });
        }
        vResult.push({
          network_id: networkInfo.network_id,
          network_code: networkInfo.code,
          network_name: networkInfo.name,
          network_image: networkInfo.image,
          created_by: networkInfo.fullname_created_by,
          created_date: networkInfo.created_date,
          assets: vResultAssetRates,
        });
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getAssetRateById(assetRateId: number) {
    try {
      const assetRates = await this._assetRateRepository.getInfoById(
        assetRateId
      );
      if (assetRates.length <= 0) {
        throw new CustomError(
          enums.responseCode.AssetNotFound,
          enums.responseMessage.AssetNotFound
        );
      }
      return await this.setAssetRateModel(assetRates[0]);
    } catch (error) {
      throw error;
    }
  }

  async getAssetRateByAssetCode(assetCode: string) {
    try {
      const assetRates = await this._assetRateRepository.getInfoByAssetCode(
        assetCode
      );
      if (assetRates.length <= 0) {
        throw new CustomError(
          enums.responseCode.AssetNotFound,
          enums.responseMessage.AssetNotFound
        );
      }
      return await this.setAssetRateModel(assetRates[0]);
    } catch (error) {
      throw error;
    }
  }

  async setAssetRateModel(assetRateInfo: any) {
    return {
      asset_rate_id: assetRateInfo.asset_rate_id,
      asset_id: assetRateInfo.asset_id,
      asset_code: assetRateInfo.asset_code,
      asset_name: assetRateInfo.asset_name,
      asset_image: assetRateInfo.asset_image,
      network_id: assetRateInfo.network_id,
      network_code: assetRateInfo.network_code,
      network_name: assetRateInfo.network_name,
      network_image: assetRateInfo.network_image,
      buying: assetRateInfo.buying,
      selling: assetRateInfo.selling,
      updated_by: assetRateInfo.updated_by,
      updated_date: assetRateInfo.updated_date,
    };
  }

  async createAssetRate(
    assetCode: string,
    buying: number,
    selling: number,
    logOnId: string
  ) {
    try {
      const assets = await this._assetRepository.getInfoByCode(assetCode);
      if (assets.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }

      const assetRates = await this._assetRateRepository.getInfoByAssetCode(
        assetCode
      );
      if (assetRates.length > 0) {
        throw new CustomError(
          enums.responseCode.Duplicate,
          enums.responseMessage.Duplicate
        );
      }
      await this._assetRateRepository.create(
        assets[0].asset_id,
        assets[0].asset_code,
        buying,
        selling,
        logOnId
      );
      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  async updateAssetRate(
    assetRateId: number,
    buying: number,
    selling: number,
    logOnId: string
  ) {
    try {
      const isExists = await this._assetRateRepository.getInfoById(assetRateId);
      if (isExists.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      await this._assetRateRepository.update(
        assetRateId,
        buying,
        selling,
        logOnId
      );
      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  async deleteAssetRate(assetRateId: number) {
    try {
      const isExists = await this._assetRateRepository.getInfoById(assetRateId);
      if (isExists.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      await this._assetRateRepository.delete(assetRateId);
      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  async getNetworks() {
    try {
      const networks = await this._networkRepository.getList();
      var vResult = [];
      for (let i = 0; i < networks.length; i++) {
        vResult.push(await this.setNetworkInfo(networks[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async setNetworkInfo(networkInfo: any) {
    return {
      network_id: networkInfo.network_id,
      code: networkInfo.code,
      name: networkInfo.name,
      image: networkInfo.image,
    };
  }

  async getAssets(networkId: number) {
    try {
      const assets = await this._assetRepository.getListByNetworkId(networkId);
      var vResult = [];
      for (let i = 0; i < assets.length; i++) {
        vResult.push(await this.setAssetInfo(assets[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async setAssetInfo(assetInfo: any) {
    return {
      asset_id: assetInfo.asset_id,
      asset_code: assetInfo.asset_code,
      asset_name: assetInfo.asset_name,
      asset_image: assetInfo.asset_image,
      contract_address: assetInfo.contract_address,
      network_id: assetInfo.network_id,
      network_code: assetInfo.network_code,
      network_name: assetInfo.network_name,
      network_image: assetInfo.network_image,
    };
  }

  async getPurposeOfTransfer() {
    try {
      return await this._masterService.getDropdownListByType(
        enums.dropdownlistType.PURPOSE_OF_TRANSFER
      );
    } catch (error) {
      throw error;
    }
  }

  async getRelationshipToReceiver() {
    try {
      return await this._masterService.getDropdownListByType(
        enums.dropdownlistType.RELATIONSHIP_TO_RECEIVER
      );
    } catch (error) {
      throw error;
    }
  }

  async getDropdownListByType(type: string) {
    try {
      return await this._masterService.getDropdownListByType(type);
    } catch (error) {
      throw error;
    }
  }
}
