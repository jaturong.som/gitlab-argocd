import { CompanyRepository } from "../repositories/companyRepository";
import { AssetRepository } from "../repositories/assetRepository";
import { AssetRateRepository } from "../repositories/assetRateRepository";
import { TransactionRepository } from "../repositories/transactionRepository";
import { WithdrawRepository } from "../repositories/withdrawRepository";
import { FiatAccountRepository } from "../repositories/fiatAccountRepository";
import { AssetInVaultAccountRepository } from "../repositories/assetInVaultAccountRepository";
import { FireblocksTransactionRepository } from "../repositories/fireblocksTransactionRepository";
import { VaultAccountRepository } from "../repositories/vaultAccountRepository";
import { GasConfigurationRepository } from "../repositories/gasConfigurationRepository";
import { HttpLogRepository } from "../repositories/httpLogRepository";
import { WalletCreditRepository } from "../repositories/walletCreditRepository";
import { VaultAccountService } from "./vaultAccountService";
import { AssetInVaultAccountService } from "./assetInVaultAccountService";
import { FireblocksService } from "./fireblocksService";
import * as enums from "../utilities/enums";
import * as utils from "../utilities/utils";
import CustomError from "../middlewares/customError";
// import moment from "moment";
import { TransactionStatus } from "fireblocks-sdk";
import { singleton } from "tsyringe";
import { UserRepository } from "../repositories/userRepository";

@singleton()
export class TransactionService {
  readonly _companyRepository: CompanyRepository;
  readonly _assetRepository: AssetRepository;
  readonly _assetRateRepository: AssetRateRepository;
  readonly _transactionRepository: TransactionRepository;
  readonly _withdrawRepository: WithdrawRepository;
  readonly _fiatAccountRepository: FiatAccountRepository;
  readonly _assetInVaultAccountRepository: AssetInVaultAccountRepository;
  readonly _fireblocksTransactionRepository: FireblocksTransactionRepository;
  readonly _vaultAccountRepository: VaultAccountRepository;
  readonly _gasConfigurationRepository: GasConfigurationRepository;
  readonly _httpLogRepository: HttpLogRepository;
  readonly _vaultAccountService: VaultAccountService;
  readonly _assetInVaultAccountService: AssetInVaultAccountService;
  readonly _fireblocksService: FireblocksService;
  readonly _walletCreditRepository: WalletCreditRepository;
  readonly _userRepository: UserRepository;
  constructor(
    companyRepository: CompanyRepository,
    assetRepository: AssetRepository,
    assetRateRepository: AssetRateRepository,
    transactionRepository: TransactionRepository,
    withdrawRepository: WithdrawRepository,
    fiatAccountRepository: FiatAccountRepository,
    assetInVaultAccountRepository: AssetInVaultAccountRepository,
    fireblocksTransactionRepository: FireblocksTransactionRepository,
    vaultAccountRepository: VaultAccountRepository,
    gasConfigurationRepository: GasConfigurationRepository,
    httpLogRepository: HttpLogRepository,
    vaultAccountService: VaultAccountService,
    assetInVaultAccountService: AssetInVaultAccountService,
    fireblocksService: FireblocksService,
    walletCreditRepository: WalletCreditRepository,
    userRepository: UserRepository
  ) {
    this._companyRepository = companyRepository;
    this._assetRepository = assetRepository;
    this._assetRateRepository = assetRateRepository;
    this._transactionRepository = transactionRepository;
    this._withdrawRepository = withdrawRepository;
    this._fiatAccountRepository = fiatAccountRepository;
    this._assetInVaultAccountRepository = assetInVaultAccountRepository;
    this._fireblocksTransactionRepository = fireblocksTransactionRepository;
    this._vaultAccountRepository = vaultAccountRepository;
    this._gasConfigurationRepository = gasConfigurationRepository;
    this._httpLogRepository = httpLogRepository;
    this._vaultAccountService = vaultAccountService;
    this._assetInVaultAccountService = assetInVaultAccountService;
    this._fireblocksService = fireblocksService;
    this._walletCreditRepository = walletCreditRepository;
    this._userRepository = userRepository;
  }

  async getAdminTransactions() {
    try {
      var vResult = [];
      const transactions = await this._transactionRepository.getList();
      for (let i = 0; i < transactions.length; i++) {
        vResult.push(await this.setTransactionInfo(transactions[i]));
      }
      const withdraws = await this._withdrawRepository.getList();
      for (let i = 0; i < withdraws.length; i++) {
        vResult.push(await this.setWithdrawInfo(withdraws[i]));
      }
      const topups = await this._walletCreditRepository.getList();
      for (let i = 0; i < topups.length; i++) {
        vResult.push(await this.setTopupInfo(topups[i]));
      }
      if (vResult.length > 0) {
        vResult.sort((a, b) => {
          return b.transaction_date - a.transaction_date;
        });
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getUserTransactions(userId: number) {
    try {
      var vResult = [];
      const transactions = await this._transactionRepository.getListByUserId(
        userId
      );
      for (let i = 0; i < transactions.length; i++) {
        vResult.push(await this.setTransactionInfo(transactions[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getTransactionById(transactionId: number) {
    try {
      const transactions = await this._transactionRepository.getInfoById(
        transactionId
      );
      if (transactions.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      return await this.setTransactionInfo(transactions[0]);
    } catch (error) {
      throw error;
    }
  }

  async getWithdrawTransactionsByUserId(userId: number) {
    try {
      var vResult = [];
      const withdraws = await this._withdrawRepository.getListByUserId(userId);
      for (let i = 0; i < withdraws.length; i++) {
        vResult.push(await this.setWithdrawInfoByUserId(withdraws[i]));
      }
      const topups = await this._walletCreditRepository.getListByUserId(userId);
      for (let i = 0; i < topups.length; i++) {
        vResult.push(await this.setTopupInfoByUserId(topups[i]));
      }
      if (vResult.length > 0) {
        vResult.sort((a, b) => {
          return b.withdraw_date - a.withdraw_date;
        });
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getWithdrawById(withdrawId: number) {
    try {
      const withdraws = await this._withdrawRepository.getInfoById(withdrawId);
      if (withdraws.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const withdraw = withdraws[0];

      var lqnStatus = withdraw.lqn_transaction_status;
      if (lqnStatus != null) {
        lqnStatus = lqnStatus.toUpperCase();
      }
      var vLqnStatusText = "";
      if (lqnStatus == enums.lqnTransactionStatus.UN_COMMIT_HOLD) {
        vLqnStatusText = enums.lqnTransactionStatusText.UN_COMMIT_HOLD;
      } else if (lqnStatus == enums.lqnTransactionStatus.UN_COMMIT_COMPLIANCE) {
        vLqnStatusText = enums.lqnTransactionStatusText.UN_COMMIT_COMPLIANCE;
      } else if (lqnStatus == enums.lqnTransactionStatus.COMPLIANCE) {
        vLqnStatusText = enums.lqnTransactionStatusText.COMPLIANCE;
      } else if (lqnStatus == enums.lqnTransactionStatus.SANCTION) {
        vLqnStatusText = enums.lqnTransactionStatusText.SANCTION;
      } else if (lqnStatus == enums.lqnTransactionStatus.HOLD) {
        vLqnStatusText = enums.lqnTransactionStatusText.HOLD;
      } else if (lqnStatus == enums.lqnTransactionStatus.UN_PAID) {
        vLqnStatusText = enums.lqnTransactionStatusText.UN_PAID;
      } else if (lqnStatus == enums.lqnTransactionStatus.POST) {
        vLqnStatusText = enums.lqnTransactionStatusText.POST;
      } else if (lqnStatus == enums.lqnTransactionStatus.API_PROCESSING) {
        vLqnStatusText = enums.lqnTransactionStatusText.API_PROCESSING;
      } else if (lqnStatus == enums.lqnTransactionStatus.PAID) {
        vLqnStatusText = enums.lqnTransactionStatusText.PAID;
      } else if (lqnStatus == enums.lqnTransactionStatus.CANCEL_HOLD) {
        vLqnStatusText = enums.lqnTransactionStatusText.CANCEL_HOLD;
      } else if (lqnStatus == enums.lqnTransactionStatus.BLOCK) {
        vLqnStatusText = enums.lqnTransactionStatusText.BLOCK;
      } else if (lqnStatus == enums.lqnTransactionStatus.CANCEL) {
        vLqnStatusText = enums.lqnTransactionStatusText.CANCEL;
      } else {
        vLqnStatusText = lqnStatus;
      }

      var vTypeText = "";
      if (withdraw.type === enums.transactionType.WITHDRAW) {
        vTypeText = "Withdraw";
      } else if (withdraw.type === enums.transactionType.TRANSFER) {
        vTypeText = "Transfer";
      }
      var vStatusText = "";
      if (withdraw.status === enums.transactionStatus.SUCCESS) {
        vStatusText = "Success";
      } else if (withdraw.status === enums.transactionStatus.FAILED) {
        vStatusText = "Failed";
      } else if (withdraw.status === enums.transactionStatus.PENDING) {
        vStatusText = "Pending";
      } else if (withdraw.status === enums.transactionStatus.CANCELLED) {
        vStatusText = "Cancelled";
      } else if (withdraw.status === enums.transactionStatus.ONHOLD) {
        vStatusText = "On hold";
      }
      return {
        withdraw_id: withdraw.withdraw_id,
        withdraw_date: withdraw.withdraw_date,
        withdraw_no: withdraw.withdraw_no,
        type: withdraw.type,
        type_text: vTypeText,
        completion_date: withdraw.completion_date,
        status: withdraw.status,
        status_text: vStatusText,
        sender_first_name: withdraw.sender_first_name,
        sender_middle_name: withdraw.sender_middle_name,
        sender_last_name: withdraw.sender_last_name,
        sender_contact_number: withdraw.sender_contact_number,
        sender_email: withdraw.sender_email,
        sender_address: withdraw.sender_address,
        sender_state: withdraw.sender_state,
        sender_area_town: withdraw.sender_area_town,
        sender_city: withdraw.sender_city,
        sender_zip_code: withdraw.sender_zip_code,
        sender_country_code: withdraw.sender_country_code,
        sender_country_name: withdraw.sender_country_name,
        receiver_first_name: withdraw.receiver_first_name,
        receiver_middle_name: withdraw.receiver_middle_name,
        receiver_last_name: withdraw.receiver_last_name,
        receiver_date_of_birth: withdraw.receiver_date_of_birth,
        receiver_gender: withdraw.receiver_gender,
        receiver_contact_number: withdraw.receiver_contact_number,
        receiver_email: withdraw.receiver_email,
        receiver_nationality: withdraw.receiver_nationality,
        receiver_occupation: withdraw.receiver_occupation,
        receiver_occupation_remarks: withdraw.receiver_occupation_remarks,
        receiver_id_type: withdraw.receiver_id_type,
        receiver_id_type_remarks: withdraw.receiver_id_type_remarks,
        receiver_id_number: withdraw.receiver_id_number,
        receiver_id_issue_date: withdraw.receiver_id_issue_date,
        receiver_id_expire_date: withdraw.receiver_id_expire_date,
        receiver_native_first_name: withdraw.receiver_native_first_name,
        receiver_native_middle_name: withdraw.receiver_native_middle_name,
        receiver_native_last_name: withdraw.receiver_native_last_name,
        receiver_native_address: withdraw.receiver_native_address,
        receiver_account_type: withdraw.receiver_account_type,
        receiver_district: withdraw.receiver_district,
        beneficiary_type: withdraw.beneficiary_type,
        currency_code: withdraw.currency_code,
        receiver_address: withdraw.receiver_address,
        receiver_state: withdraw.receiver_state,
        receiver_area_town: withdraw.receiver_area_town,
        receiver_city: withdraw.receiver_city,
        receiver_zip_code: withdraw.receiver_zip_code,
        receiver_country_code: withdraw.receiver_country_code,
        receiver_country_name: withdraw.receiver_country_name,
        bank_account_id: withdraw.bank_account_id,
        bank_type: withdraw.bank_type,
        location_id: withdraw.location_id,
        bank_name: withdraw.bank_name,
        bank_branch_name: withdraw.bank_branch_name,
        bank_branch_code: withdraw.bank_branch_code,
        bank_account_number: withdraw.bank_account_number,
        bank_account_name: withdraw.bank_account_name,
        swift_code: withdraw.swift_code,
        iban: withdraw.iban,
        lqn_transaction_id: withdraw.lqn_transaction_id,
        lqn_transaction_date: withdraw.lqn_transaction_date,
        lqn_transaction_type: withdraw.lqn_transaction_type,
        customer_id: withdraw.customer_id,
        receiver_id: withdraw.receiver_id,
        agent_session_id: withdraw.agent_session_id,
        confirmation_id: withdraw.confirmation_id,
        agent_txn_id: withdraw.agent_txn_id,
        collect_amount: withdraw.collect_amount,
        collect_currency: withdraw.collect_currency,
        service_charge: withdraw.service_charge,
        gst_charge: withdraw.gst_charge,
        transfer_amount: withdraw.transfer_amount,
        exchange_rate: withdraw.exchange_rate,
        payout_amount: withdraw.payout_amount,
        payout_currency: withdraw.payout_currency,
        fee_discount: withdraw.fee_discount,
        additional_premium_rate: withdraw.additional_premium_rate,
        settlement_rate: withdraw.settlement_rate,
        send_commission: withdraw.send_commission,
        settlement_amount: withdraw.settlement_amount,
        pin_number: withdraw.pin_number,
        lqn_status: lqnStatus,
        lqn_status_text: vLqnStatusText,
        message: withdraw.message,
        additionalmessage: withdraw.additionalmessage,
      };
    } catch (error) {
      throw error;
    }
  }

  async getConvertById(withdrawId: number) {
    try {
      const withdraws = await this._withdrawRepository.getConvertInfoById(
        withdrawId
      );
      if (withdraws.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const withdraw = withdraws[0];

      var vTypeText = "";
      if (withdraw.type === enums.transactionType.CONVERT) {
        vTypeText = "Convert to Crypto";
      }
      var vStatusText = "";
      if (withdraw.status === enums.transactionStatus.SUCCESS) {
        vStatusText = "Success";
      } else if (withdraw.status === enums.transactionStatus.FAILED) {
        vStatusText = "Failed";
      } else if (withdraw.status === enums.transactionStatus.PENDING) {
        vStatusText = "Pending";
      } else if (withdraw.status === enums.transactionStatus.CANCELLED) {
        vStatusText = "Cancelled";
      } else if (withdraw.status === enums.transactionStatus.ONHOLD) {
        vStatusText = "On hold";
      }

      return {
        wallet_credit_id: withdraw.withdraw_id,
        wallet_credit_date: withdraw.withdraw_date,
        wallet_credit_no: withdraw.withdraw_no,
        operator_id: withdraw.user_id,
        operator_by: withdraw.operator_by,
        type: withdraw.type,
        type_text: vTypeText,
        amount: withdraw.total_amount,
        total_assets: withdraw.total_assets,
        currency_code: withdraw.currency_code,
        status: withdraw.status,
        status_text: vStatusText,
        documents: [],
      };
    } catch (error) {
      throw error;
    }
  }

  async setTransactionInfo(transactionInfo: any) {
    var vTypeText = "";
    if (transactionInfo.type === enums.transactionType.BUYING) {
      vTypeText = "Buying";
    } else if (transactionInfo.type === enums.transactionType.SELLING) {
      vTypeText = "Selling";
    }
    var vStatusText = "";
    if (transactionInfo.status === enums.transactionStatus.SUCCESS) {
      vStatusText = "Success";
    } else if (transactionInfo.status === enums.transactionStatus.FAILED) {
      vStatusText = "Failed";
    } else if (transactionInfo.status === enums.transactionStatus.PENDING) {
      vStatusText = "Pending";
    } else if (transactionInfo.status === enums.transactionStatus.CANCELLED) {
      vStatusText = "Cancelled";
    } else if (transactionInfo.status === enums.transactionStatus.ONHOLD) {
      vStatusText = "On hold";
    }
    return {
      transaction_id: transactionInfo.transaction_id,
      transaction_date: transactionInfo.transaction_date,
      transaction_no: transactionInfo.transaction_no,
      fb_transaction_hash: transactionInfo.fb_transaction_hash,
      fb_transaction_status: transactionInfo.fb_transaction_status,
      user_id: transactionInfo.user_id,
      type: transactionInfo.type,
      type_text: vTypeText,
      source_vault_account_id: transactionInfo.source_vault_account_id,
      destination_vault_account_id:
        transactionInfo.destination_vault_account_id,
      buying: transactionInfo.buying,
      selling: transactionInfo.selling,
      customer_name: transactionInfo.customer_name,
      network_id: transactionInfo.network_id,
      network_code: transactionInfo.network_code,
      network_name: transactionInfo.network_name,
      network_image: transactionInfo.network_image,
      asset_id: transactionInfo.asset_id,
      asset_code: transactionInfo.asset_code,
      asset_name: transactionInfo.asset_name,
      asset_image: transactionInfo.asset_image,
      total_amount: transactionInfo.total_amount,
      total_assets: transactionInfo.total_assets,
      currency_code: transactionInfo.currency_code,
      status: transactionInfo.status,
      status_text: vStatusText,
      bank_account_id: 0,
    };
  }

  //admin
  async setWithdrawInfo(withdrawInfo: any) {
    var vTypeText = "";
    if (withdrawInfo.type === enums.transactionType.WITHDRAW) {
      vTypeText = "Withdraw";
    } else if (withdrawInfo.type === enums.transactionType.TRANSFER) {
      vTypeText = "Transfer";
    } else if (withdrawInfo.type === enums.transactionType.CONVERT) {
      vTypeText = "Convert";
    } else if (withdrawInfo.type === enums.transactionType.TOPUP) {
      vTypeText = "Topup";
    }

    var lqnStatus = withdrawInfo.lqn_status;
    if (withdrawInfo.lqn_status != null) {
      lqnStatus = withdrawInfo.lqn_status.toUpperCase();
    }
    var vLqnStatusText = "";
    if (lqnStatus == enums.lqnTransactionStatus.UN_COMMIT_HOLD) {
      vLqnStatusText = enums.lqnTransactionStatusText.UN_COMMIT_HOLD;
    } else if (lqnStatus == enums.lqnTransactionStatus.UN_COMMIT_COMPLIANCE) {
      vLqnStatusText = enums.lqnTransactionStatusText.UN_COMMIT_COMPLIANCE;
    } else if (lqnStatus == enums.lqnTransactionStatus.COMPLIANCE) {
      vLqnStatusText = enums.lqnTransactionStatusText.COMPLIANCE;
    } else if (lqnStatus == enums.lqnTransactionStatus.SANCTION) {
      vLqnStatusText = enums.lqnTransactionStatusText.SANCTION;
    } else if (lqnStatus == enums.lqnTransactionStatus.HOLD) {
      vLqnStatusText = enums.lqnTransactionStatusText.HOLD;
    } else if (lqnStatus == enums.lqnTransactionStatus.UN_PAID) {
      vLqnStatusText = enums.lqnTransactionStatusText.UN_PAID;
    } else if (lqnStatus == enums.lqnTransactionStatus.POST) {
      vLqnStatusText = enums.lqnTransactionStatusText.POST;
    } else if (lqnStatus == enums.lqnTransactionStatus.API_PROCESSING) {
      vLqnStatusText = enums.lqnTransactionStatusText.API_PROCESSING;
    } else if (lqnStatus == enums.lqnTransactionStatus.PAID) {
      vLqnStatusText = enums.lqnTransactionStatusText.PAID;
    } else if (lqnStatus == enums.lqnTransactionStatus.CANCEL_HOLD) {
      vLqnStatusText = enums.lqnTransactionStatusText.CANCEL_HOLD;
    } else if (lqnStatus == enums.lqnTransactionStatus.BLOCK) {
      vLqnStatusText = enums.lqnTransactionStatusText.BLOCK;
    } else if (lqnStatus == enums.lqnTransactionStatus.CANCEL) {
      vLqnStatusText = enums.lqnTransactionStatusText.CANCEL;
    } else {
      vLqnStatusText = lqnStatus;
    }

    var vStatusText = "";
    if (withdrawInfo.status === enums.transactionStatus.SUCCESS) {
      vStatusText = "Success";
    } else if (withdrawInfo.status === enums.transactionStatus.FAILED) {
      vStatusText = "Failed";
    } else if (withdrawInfo.status === enums.transactionStatus.PENDING) {
      vStatusText = "Pending";
    } else if (withdrawInfo.status === enums.transactionStatus.CANCELLED) {
      vStatusText = "Cancelled";
    } else if (withdrawInfo.status === enums.transactionStatus.ONHOLD) {
      vStatusText = "On hold";
    }

    return {
      transaction_id: withdrawInfo.withdraw_id,
      transaction_date: withdrawInfo.withdraw_date,
      transaction_no: withdrawInfo.withdraw_no,
      fb_transaction_hash: null,
      fb_transaction_status: null,
      user_id: withdrawInfo.user_id,
      type: withdrawInfo.type,
      type_text: vTypeText,
      source_vault_account_id: null,
      destination_vault_account_id: null,
      buying: null,
      selling: null,
      customer_name: withdrawInfo.customer_name,
      network_id: null,
      network_code: null,
      network_name: null,
      network_image: null,
      asset_id: null,
      asset_code: null,
      asset_name: null,
      asset_image: null,
      amount: Number(withdrawInfo.transfer_amount),
      total_amount: Number(withdrawInfo.collect_amount),
      total_assets: null,
      currency_code: withdrawInfo.currency_code,
      purpose_of_transfer: withdrawInfo.purpose_of_transfer,
      purpose_of_transfer_other: withdrawInfo.purpose_of_transfer_other,
      relationship_to_receiver: withdrawInfo.relationship_to_receiver,
      relationship_to_receiver_other:
        withdrawInfo.relationship_to_receiver_other,
      status: withdrawInfo.status,
      status_text: vStatusText,
      lqn_status: lqnStatus,
      lqn_status_text: vLqnStatusText,
      bank_account_id: withdrawInfo.bank_account_id,
    };
  }

  async setWithdrawInfoByUserId(withdrawInfo: any) {
    var vTypeText = "";
    if (withdrawInfo.type === enums.transactionType.WITHDRAW) {
      vTypeText = "Withdraw";
    } else if (withdrawInfo.type === enums.transactionType.TRANSFER) {
      vTypeText = "Transfer";
    } else if (withdrawInfo.type === enums.transactionType.CONVERT) {
      vTypeText = "Convert";
    } else if (withdrawInfo.type === enums.transactionType.TOPUP) {
      vTypeText = "Topup";
    }

    var vStatusText = "";
    if (withdrawInfo.status === enums.transactionStatus.SUCCESS) {
      vStatusText = "Success";
    } else if (withdrawInfo.status === enums.transactionStatus.FAILED) {
      vStatusText = "Failed";
    } else if (withdrawInfo.status === enums.transactionStatus.PENDING) {
      vStatusText = "Pending";
    } else if (withdrawInfo.status === enums.transactionStatus.CANCELLED) {
      vStatusText = "Cancel";
    } else if (withdrawInfo.status === enums.transactionStatus.ONHOLD) {
      vStatusText = "Hold";
    }
    return {
      withdraw_id: withdrawInfo.withdraw_id,
      withdraw_date: withdrawInfo.withdraw_date,
      withdraw_no: withdrawInfo.withdraw_no,
      type: withdrawInfo.type,
      type_text: vTypeText,
      amount: withdrawInfo.amount,
      currency_code: withdrawInfo.currency_code,
      purpose_of_transfer: withdrawInfo.purpose_of_transfer,
      purpose_of_transfer_other: withdrawInfo.purpose_of_transfer_other,
      relationship_to_receiver: withdrawInfo.relationship_to_receiver,
      relationship_to_receiver_other:
        withdrawInfo.relationship_to_receiver_other,
      status: withdrawInfo.status,
      status_text: vStatusText,
      completion_date: withdrawInfo.completion_date,
      bank_id: withdrawInfo.bank_id,
      bank_code: withdrawInfo.bank_code,
      bank_name: withdrawInfo.bank_name,
      account_no: withdrawInfo.account_no,
      account_name: withdrawInfo.account_name,
      swift_code: withdrawInfo.swift_code,
      iban: withdrawInfo.iban,
      transfer_amount: withdrawInfo.transfer_amount,
      lqn_status: withdrawInfo.lqn_status,
      collect_amount: withdrawInfo.collect_amount,
    };
  }

  async setTopupInfo(walletCreditInfo: any) {
    var vTypeText = "";
    if (walletCreditInfo.type === enums.transactionType.TOPUP) {
      vTypeText = "Top-Up";
    } else if (walletCreditInfo.type === enums.transactionType.CONVERT) {
      vTypeText = "Convert";
    }
    var vStatusText = "";
    if (walletCreditInfo.status === enums.transactionStatus.SUCCESS) {
      vStatusText = "Success";
    } else if (walletCreditInfo.status === enums.transactionStatus.FAILED) {
      vStatusText = "Failed";
    } else if (walletCreditInfo.status === enums.transactionStatus.PENDING) {
      vStatusText = "Pending";
    } else if (walletCreditInfo.status === enums.transactionStatus.CANCELLED) {
      vStatusText = "Cancelled";
    } else if (walletCreditInfo.status === enums.transactionStatus.ONHOLD) {
      vStatusText = "On hold";
    }
    return {
      transaction_id: walletCreditInfo.wallet_credit_id,
      transaction_date: walletCreditInfo.wallet_credit_date,
      transaction_no: walletCreditInfo.wallet_credit_no,
      fb_transaction_hash: null,
      fb_transaction_status: null,
      user_id: walletCreditInfo.user_id,
      type: walletCreditInfo.type,
      type_text: vTypeText,
      source_vault_account_id: null,
      destination_vault_account_id: null,
      buying: null,
      selling: null,
      customer_name: walletCreditInfo.customer_name,
      network_id: null,
      network_code: null,
      network_name: null,
      network_image: null,
      asset_id: null,
      asset_code: null,
      asset_name: null,
      asset_image: null,
      total_amount: walletCreditInfo.amount,
      total_assets: null,
      currency_code: walletCreditInfo.currency_code,
      purpose_of_transfer: null,
      purpose_of_transfer_other: null,
      relationship_to_receiver: null,
      relationship_to_receiver_other: null,
      status: walletCreditInfo.status,
      status_text: vStatusText,
      bank_account_id: null,
    };
  }

  async setTopupInfoByUserId(walletCreditInfo: any) {
    var vTypeText = "";
    if (walletCreditInfo.type === enums.transactionType.TOPUP) {
      vTypeText = "Top-Up";
    } else if (walletCreditInfo.type === enums.transactionType.CONVERT) {
      vTypeText = "Convert";
    }
    var vStatusText = "";
    if (walletCreditInfo.status === enums.transactionStatus.SUCCESS) {
      vStatusText = "Success";
    } else if (walletCreditInfo.status === enums.transactionStatus.FAILED) {
      vStatusText = "Failed";
    } else if (walletCreditInfo.status === enums.transactionStatus.PENDING) {
      vStatusText = "Pending";
    } else if (walletCreditInfo.status === enums.transactionStatus.CANCELLED) {
      vStatusText = "Cancelled";
    } else if (walletCreditInfo.status === enums.transactionStatus.ONHOLD) {
      vStatusText = "On hold";
    }
    return {
      withdraw_id: walletCreditInfo.wallet_credit_id,
      withdraw_date: walletCreditInfo.wallet_credit_date,
      withdraw_no: walletCreditInfo.wallet_credit_no,
      type: walletCreditInfo.type,
      type_text: vTypeText,
      amount: walletCreditInfo.amount,
      currency_code: walletCreditInfo.currency_code,
      purpose_of_transfer: null,
      purpose_of_transfer_other: null,
      relationship_to_receiver: null,
      relationship_to_receiver_other: null,
      status: walletCreditInfo.status,
      status_text: vStatusText,
      completion_date: null,
      bank_id: null,
      bank_code: null,
      bank_name: null,
      account_no: null,
      account_name: null,
      swift_code: null,
      iban: null,
      transfer_amount: walletCreditInfo.amount,
      lqn_status: null,
      collect_amount: walletCreditInfo.amount,
    };
  }

  async createTransaction(
    userId: number,
    type: string,
    assetCode: string,
    totalAssets: number,
    totalAmount: number,
    inputType: string,
    isManualMode: boolean,
    walletNetwork: string,
    walletAddress: string,
    logOnId: string
  ) {
    try {
      await this._assetInVaultAccountService.syncFireblocksAssetInVaults(
        enums.accountType.USER,
        undefined,
        userId
      );

      const companies = await this._companyRepository.getList();
      if (companies.length <= 0) {
        throw new CustomError(
          enums.responseCode.CompanyNotFound,
          enums.responseMessage.CompanyNotFound
        );
      }
      const companyInfo = companies[0];

      const assets = await this._assetRepository.getInfoByCode(assetCode);
      if (assets.length <= 0) {
        throw new CustomError(
          enums.responseCode.AssetNotFound,
          enums.responseMessage.AssetNotFound
        );
      }
      const assetInfo = assets[0];

      let sourceVaultAccountInfo;
      let destinationVaultAccountInfo;

      if (type === enums.transactionType.BUYING) {
        sourceVaultAccountInfo =
          await this._vaultAccountService.getVaultAccount(
            enums.accountType.COMPANY,
            companyInfo.company_id,
            undefined
          );

        destinationVaultAccountInfo =
          await this._vaultAccountService.getVaultAccount(
            enums.accountType.USER,
            undefined,
            userId
          );
      } else {
        sourceVaultAccountInfo =
          await this._vaultAccountService.getVaultAccount(
            enums.accountType.USER,
            undefined,
            userId
          );

        destinationVaultAccountInfo =
          await this._vaultAccountService.getVaultAccount(
            enums.accountType.COMPANY,
            companyInfo.company_id,
            undefined
          );
      }

      const assetInVaults =
        await this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(
          sourceVaultAccountInfo.vault_account_id,
          assetCode
        );
      if (assetInVaults.length <= 0) {
        throw new CustomError(
          enums.responseCode.AssetNotFound,
          enums.responseMessage.AssetNotFound
        );
      }
      const assetInVaultInfo = assetInVaults[0];

      var calculateTotalAsset = 0;
      var calculateTotalAmount = 0;
      var vBuyingRate = 0;
      var vSellingRate = 0;
      if (isManualMode) {
        calculateTotalAsset = totalAssets;
        calculateTotalAmount = totalAmount;
        vBuyingRate =
          totalAssets > 0
            ? parseFloat((totalAmount / totalAssets).toFixed(10))
            : 0;
        vSellingRate =
          totalAssets > 0
            ? parseFloat((totalAmount / totalAssets).toFixed(10))
            : 0;
      } else {
        const assetRates = await this._assetRateRepository.getInfoByAssetCode(
          assetCode
        );
        if (assetRates.length <= 0) {
          throw new CustomError(
            enums.responseCode.AssetRateNotFound,
            enums.responseMessage.AssetRateNotFound
          );
        }
        const assetRateInfo = assetRates[0];

        vBuyingRate = assetRateInfo.buying;
        vSellingRate = assetRateInfo.selling;
        if (inputType === enums.inputType.AMOUNT) {
          calculateTotalAmount = totalAmount;
          if (type === enums.transactionType.SELLING) {
            calculateTotalAsset = parseFloat(
              (totalAmount / assetRateInfo.buying).toFixed(10)
            );
          } else {
            calculateTotalAsset = parseFloat(
              (totalAmount / assetRateInfo.selling).toFixed(10)
            );
          }
        } else {
          calculateTotalAsset = totalAssets;
          if (type === enums.transactionType.SELLING) {
            calculateTotalAmount = parseFloat(
              (totalAssets * assetRateInfo.buying).toFixed(10)
            );
          } else {
            calculateTotalAmount = parseFloat(
              (totalAssets * assetRateInfo.selling).toFixed(10)
            );
          }
        }
      }

      var vStatus = enums.transactionStatus.PENDING;
      if (walletNetwork === "WLN99") {
        vStatus = enums.transactionStatus.SUCCESS;
      } else {
        const assetBalances =
          await this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCode(
            sourceVaultAccountInfo.vault_account_id,
            assetInVaultInfo.asset_code
          );
        if (assetBalances.length <= 0) {
          throw new CustomError(
            enums.responseCode.InsufficientAssets,
            enums.responseMessage.InsufficientAssets
          );
        }
        if (assetBalances[0].total < calculateTotalAsset) {
          throw new CustomError(
            enums.responseCode.InsufficientAssets,
            enums.responseMessage.InsufficientAssets
          );
        }
      }

      if (type === enums.transactionType.BUYING) {
        await this._fiatAccountRepository.updateBalanceAllUsers();

        var fiatAccounts = await this._fiatAccountRepository.getInfoByUserId(
          userId
        );
        if (fiatAccounts.length <= 0) {
          throw new CustomError(
            enums.responseCode.FiatAccountNotFound,
            enums.responseMessage.FiatAccountNotFound
          );
        }
        const fiatAccountInfo = fiatAccounts[0];

        if (parseFloat(fiatAccountInfo.total) - calculateTotalAmount < 0) {
          throw new CustomError(
            enums.responseCode.InsufficientFunds,
            enums.responseMessage.InsufficientFunds
          );
        }
      }

      const transactions = await this._transactionRepository.create(
        userId,
        await utils.generateKey(userId.toString()),
        type,
        sourceVaultAccountInfo.vault_account_id,
        assetInfo.asset_id,
        assetInVaultInfo.asset_code,
        vBuyingRate,
        vSellingRate,
        calculateTotalAmount,
        calculateTotalAsset,
        vStatus,
        logOnId,
        enums.source.PORTAL,
        isManualMode,
        walletNetwork,
        walletAddress,
        destinationVaultAccountInfo.vault_account_id,
        undefined
      );
      const transaction = transactions[0];

      if (type === enums.transactionType.BUYING && walletNetwork !== "WLN99") {
        await this._withdrawRepository.create(
          await utils.generateKey(transaction.transaction_id),
          userId,
          enums.transactionType.CONVERT,
          0,
          calculateTotalAmount,
          0,
          enums.transactionStatus.SUCCESS,
          logOnId,
          transaction.transaction_id,
          undefined
        );
      }

      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  // async getAdminTransactionLogs(
  //   pageSize: number,
  //   pageCursor: number,
  //   fromDate: string,
  //   toDate: string,
  //   transactionType: string,
  //   status: string,
  //   assetName: string
  // ) {
  //   try {
  //     const transactions = await this._transactionRepository.getTransactionLogs(
  //       pageSize,
  //       pageCursor,
  //       fromDate,
  //       toDate,
  //       transactionType,
  //       status,
  //       assetName
  //     );
  //     var vResult = [];
  //     for (let i = 0; i < transactions.length; i++) {
  //       var createdDate = new Date(transactions[i].created_date);
  //       var transaction = {
  //         fb_transaction_id: transactions[i].fb_transaction_id,
  //         source_account_name: transactions[i].source_account_name,
  //         destination_account_name: transactions[i].destination_account_name,
  //         transaction_type: transactions[i].type,
  //         asset: transactions[i].asset_id,
  //         amount: transactions[i].amount,
  //         fee: transactions[i].fee,
  //         status: transactions[i].status,
  //         fb_transaction_hash: transactions[i].fb_transaction_hash,
  //         destination_address: transactions[i].destination_address,
  //         created_date: moment(createdDate).format(),
  //       };
  //       vResult.push(transaction);
  //     }

  //     const totalTransaction =
  //       await this._transactionRepository.getTransactionLogsTotal(
  //         fromDate,
  //         toDate,
  //         transactionType,
  //         status,
  //         assetName
  //       );
  //     let pageTotal = Number(totalTransaction[0].count) / pageSize;
  //     return {
  //       transactions: vResult,
  //       total: totalTransaction[0].count,
  //       page_total: Math.ceil(pageTotal),
  //     };
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async getFireblocksNotificationList(
    fromDate: string,
    toDate: string,
    status: string,
    searchText: string
  ) {
    try {
      const notifications =
        await this._httpLogRepository.getFireblocksNotificationList(
          fromDate,
          toDate,
          status,
          searchText
        );
      var vResult = [];
      for (let i = 0; i < notifications.length; i++) {
        const notificationInfo = notifications[i];
        var vTimestamp = notificationInfo.request_date;
        var vId = "";
        var vTitle = "";
        var vAssetId = "";
        var vSourceName = "";
        var vDestinationName = "";
        var vAmount = "";
        var vNetworkFee = "";
        var vSourceAddress = "";
        var vDestinationAddress = "";
        var vStatus = "";
        var vTxHash = "";
        var vSubStatus = "";
        var vFeeCurrency = "";
        var vOperation = "";
        var objectResult;
        if (notificationInfo.body !== undefined) {
          objectResult = JSON.parse(notificationInfo.body);
          if (objectResult.type !== undefined) {
            vTitle = objectResult.type;
          } else {
            vTitle = objectResult.title;
            vId = objectResult.txId;
            vAmount = objectResult.netAmount;
          }
          if (objectResult.data !== undefined) {
            const element = objectResult.data;
            vId = element.id;
            vAssetId = element.assetId;
            if (element.source !== undefined) {
              vSourceName = element.source.name;
            }
            if (element.destination !== undefined) {
              vDestinationName = element.destination.name;
            }
            vAmount = element.amount;
            vNetworkFee = element.networkFee;
            vSourceAddress = element.sourceAddress;
            vDestinationAddress = element.destinationAddress;
            vStatus = element.status;
            vTxHash = element.txHash;
            vSubStatus = element.subStatus;
            vFeeCurrency = element.feeCurrency;
            vOperation = element.operation;
          }
        }
        var notification = {
          timestamp: vTimestamp,
          id: vId,
          type: vTitle,
          assetId: vAssetId,
          sourceName: vSourceName,
          destinationName: vDestinationName,
          amount: vAmount,
          networkFee: vNetworkFee,
          sourceAddress: vSourceAddress,
          destinationAddress: vDestinationAddress,
          status: vStatus,
          txHash: vTxHash,
          subStatus: vSubStatus,
          feeCurrency: vFeeCurrency,
          operation: vOperation,
          raw_data: objectResult,
        };
        vResult.push(notification);
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async submitBuySellTransaction(transactionId: number, logOnId: string) {
    try {
      const transactions = await this._transactionRepository.getInfoById(
        transactionId
      );
      if (transactions.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const transaction = transactions[0];

      await this._assetInVaultAccountService.syncFireblocksAssetInVaults(
        enums.accountType.USER,
        undefined,
        transaction.user_id
      );

      const companies = await this._companyRepository.getList();
      if (companies.length <= 0) {
        throw new CustomError(
          enums.responseCode.CompanyNotFound,
          enums.responseMessage.CompanyNotFound
        );
      }
      const companyInfo = companies[0];

      const companyVaultAccounts = await this._vaultAccountRepository.getInfo(
        enums.accountType.COMPANY,
        companyInfo.company_id,
        undefined
      );
      if (companyVaultAccounts.length <= 0) {
        throw new CustomError(
          enums.responseCode.VaultAccountNotFound,
          enums.responseMessage.VaultAccountNotFound
        );
      }
      const companyVaultAccountInfo = companyVaultAccounts[0];

      const userVaultAccounts = await this._vaultAccountRepository.getInfo(
        enums.accountType.USER,
        undefined,
        transaction.user_id
      );
      if (userVaultAccounts.length <= 0) {
        throw new CustomError(
          enums.responseCode.VaultAccountNotFound,
          enums.responseMessage.VaultAccountNotFound
        );
      }
      const userVaultAccountInfo = userVaultAccounts[0];

      const gasPrices =
        await this._gasConfigurationRepository.getInfoByAssetCode(
          transaction.asset_code
        );
      if (gasPrices.length <= 0) {
        throw new CustomError(
          enums.responseCode.GasConfigurationNotFound,
          enums.responseMessage.GasConfigurationNotFound
        );
      }
      const gasPrice = gasPrices[0];

      if (
        transaction.type === enums.transactionType.SELLING &&
        transaction.status === enums.transactionStatus.PENDING
      ) {
        const companyBaseAssetInVaults =
          await this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(
            transaction.destination_vault_account_id,
            gasPrice.native_asset_code
          );
        if (companyBaseAssetInVaults.length <= 0) {
          throw new CustomError(
            enums.responseCode.AssetNotFound,
            enums.responseMessage.AssetNotFound
          );
        }
        const companyBaseAssetInVaultInfo = companyBaseAssetInVaults[0];

        if (companyBaseAssetInVaultInfo.total < gasPrice.gas_threshold) {
          throw new CustomError(
            enums.responseCode.InsufficientFundsForCompany,
            enums.responseMessage.InsufficientFundsForCompany
          );
        }

        const baseAssetInVaults =
          await this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(
            transaction.source_vault_account_id,
            gasPrice.native_asset_code
          );
        if (baseAssetInVaults.length <= 0) {
          throw new CustomError(
            enums.responseCode.AssetNotFound,
            enums.responseMessage.AssetNotFound
          );
        }
        const baseAssetInVaultInfo = baseAssetInVaults[0];

        if (baseAssetInVaultInfo.total < gasPrice.gas_threshold) {
          await this.autoFuelGasFees(
            transaction.user_id,
            enums.fireblocksTransactionType.AUTO_FUELING_GAS_FEES,
            companyVaultAccountInfo.fireblock_vault_account_id,
            userVaultAccountInfo.fireblock_vault_account_id,
            gasPrice.asset_code,
            transaction.transaction_id,
            companyVaultAccountInfo.vault_account_id
          );
          await this._transactionRepository.updateTransactionStatus(
            transaction.transaction_id,
            enums.transactionStatus.ONHOLD
          );
        } else {
          const estimateFee =
            await this._fireblocksService.estimateFeeForTransaction(
              gasPrice.asset_code,
              userVaultAccountInfo.fireblock_vault_account_id,
              companyVaultAccountInfo.fireblock_vault_account_id,
              transaction.total_assets
            );
          const transactionFee =
            estimateFee.medium.networkFee === undefined
              ? 0
              : parseFloat(estimateFee.medium.networkFee);
          const fbTransaction = await this._fireblocksService.createTransaction(
            gasPrice.asset_code,
            userVaultAccountInfo.fireblock_vault_account_id,
            companyVaultAccountInfo.fireblock_vault_account_id,
            transaction.total_assets,
            transactionFee
          );
          await this._fireblocksTransactionRepository.create(
            fbTransaction.id,
            enums.fireblocksTransactionType.TRANSFER_SELLING,
            gasPrice.asset_code,
            userVaultAccountInfo.fireblock_vault_account_id,
            "VAULT_ACCOUNT",
            transaction.total_assets,
            transactionFee,
            transaction.transaction_id,
            fbTransaction.status,
            companyVaultAccountInfo.fireblock_vault_account_id,
            undefined,
            "VAULT_ACCOUNT"
          );
          await this._transactionRepository.updateTransactionStatus(
            transaction.transaction_id,
            enums.transactionStatus.ONHOLD
          );
        }
      } else if (
        transaction.type === enums.transactionType.BUYING &&
        transaction.status === enums.transactionStatus.PENDING
      ) {
        const companyBaseAssetInVaults =
          await this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(
            transaction.source_vault_account_id,
            gasPrice.native_asset_code
          );
        if (companyBaseAssetInVaults.length <= 0) {
          throw new CustomError(
            enums.responseCode.AssetNotFound,
            enums.responseMessage.AssetNotFound
          );
        }
        const companyBaseAssetInVaultInfo = companyBaseAssetInVaults[0];

        if (companyBaseAssetInVaultInfo.total < gasPrice.gas_threshold) {
          throw new CustomError(
            enums.responseCode.InsufficientFundsForCompany,
            enums.responseMessage.InsufficientFundsForCompany
          );
        }

        const estimateFee =
          await this._fireblocksService.estimateFeeForTransaction(
            gasPrice.asset_code,
            companyVaultAccountInfo.fireblock_vault_account_id,
            userVaultAccountInfo.fireblock_vault_account_id,
            transaction.total_assets
          );
        const transactionFee =
          estimateFee.medium.networkFee === undefined
            ? 0
            : parseFloat(estimateFee.medium.networkFee);
        const fbTransaction = await this._fireblocksService.createTransaction(
          gasPrice.asset_code,
          companyVaultAccountInfo.fireblock_vault_account_id,
          userVaultAccountInfo.fireblock_vault_account_id,
          transaction.total_assets,
          transactionFee
        );

        await this._fireblocksTransactionRepository.create(
          fbTransaction.id,
          enums.fireblocksTransactionType.TRANSFER_BUYING,
          gasPrice.asset_code,
          companyVaultAccountInfo.fireblock_vault_account_id,
          "VAULT_ACCOUNT",
          transaction.total_assets,
          transactionFee,
          transaction.transaction_id,
          fbTransaction.status,
          userVaultAccountInfo.fireblock_vault_account_id,
          undefined,
          "VAULT_ACCOUNT"
        );

        await this._transactionRepository.updateTransactionStatus(
          transaction.transaction_id,
          enums.transactionStatus.ONHOLD
        );
      }
    } catch (error) {
      throw error;
    }
  }

  async autoFuelGasFees(
    userId: number,
    type: string,
    fbSourceVaultAccountId: string,
    fbDestinationVaultAccountId: string,
    assetCode: string,
    refId: number,
    companyVaultAccountId: number
  ) {
    try {
      const gasPrices =
        await this._gasConfigurationRepository.getInfoByAssetCode(assetCode);
      if (gasPrices.length <= 0) {
        throw new CustomError(
          enums.responseCode.GasConfigurationNotFound,
          enums.responseMessage.GasConfigurationNotFound
        );
      }
      const gasPrice = gasPrices[0];

      const companyBaseAssetInVaults =
        await this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(
          companyVaultAccountId,
          gasPrice.native_asset_code
        );
      if (companyBaseAssetInVaults.length <= 0) {
        throw new CustomError(
          enums.responseCode.AssetNotFound,
          enums.responseMessage.AssetNotFound
        );
      }
      const companyBaseAssetInVaultInfo = companyBaseAssetInVaults[0];

      if (companyBaseAssetInVaultInfo.total < gasPrice.gas_threshold) {
        throw new CustomError(
          enums.responseCode.InsufficientFundsForCompany,
          enums.responseMessage.InsufficientFundsForCompany
        );
      }

      const estimateFee =
        await this._fireblocksService.estimateFeeForTransaction(
          gasPrice.native_asset_code,
          fbSourceVaultAccountId,
          fbDestinationVaultAccountId,
          parseFloat(gasPrice.gas_cap)
        );

      const transactionFee =
        estimateFee.medium.networkFee === undefined
          ? 0
          : parseFloat(estimateFee.medium.networkFee);

      const fbTransaction = await this._fireblocksService.createTransaction(
        gasPrice.native_asset_code,
        fbSourceVaultAccountId,
        fbDestinationVaultAccountId,
        parseFloat(gasPrice.gas_cap),
        transactionFee
      );

      await this._fireblocksTransactionRepository.create(
        fbTransaction.id,
        type,
        gasPrice.native_asset_code,
        fbSourceVaultAccountId,
        "VAULT_ACCOUNT",
        parseFloat(gasPrice.gas_cap),
        transactionFee,
        refId,
        fbTransaction.status,
        fbDestinationVaultAccountId,
        undefined,
        "VAULT_ACCOUNT"
      );
    } catch (error) {
      throw error;
    }
  }

  async manualUpdateTransactionStatus(fbTransactionId: string) {
    try {
      const fbTrans = await this._fireblocksService.getTransactionById(
        fbTransactionId
      );
      if (fbTrans === undefined) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      } else {
        await this.updateTransactionStatus(
          fbTransactionId,
          fbTrans.status,
          fbTrans.subStatus === undefined ? "" : fbTrans.subStatus,
          fbTrans.txHash
        );
      }
    } catch (error) {
      throw error;
    }
  }

  async updateTransactionStatus(
    fireblockTransactionId: string,
    status: string,
    remark: string,
    fbTransactionHash: string
  ) {
    try {
      const fireblocksTransactions =
        await this._fireblocksTransactionRepository.getInfoByfbId(
          fireblockTransactionId
        );
      if (fireblocksTransactions.length <= 0) {
        return;
      }
      const fireblocksTransactionInfo = fireblocksTransactions[0];

      var vStatus = "";
      switch (status) {
        case TransactionStatus.COMPLETED:
          vStatus = enums.transactionStatus.SUCCESS;
          break;
        case TransactionStatus.FAILED:
          vStatus = enums.transactionStatus.FAILED;
          break;
        case TransactionStatus.CANCELLED:
          vStatus = enums.transactionStatus.CANCELLED;
          break;
        case TransactionStatus.REJECTED:
          vStatus = enums.transactionStatus.REJECTED;
          break;
        default:
          break;
      }

      if (
        fireblocksTransactionInfo.type ===
          enums.fireblocksTransactionType.TRANSFER_SELLING ||
        fireblocksTransactionInfo.type ===
          enums.fireblocksTransactionType.TRANSFER_BUYING
      ) {
        await this._fireblocksTransactionRepository.updateStatusFromFireblocks(
          fireblocksTransactionInfo.fireblock_transaction_id,
          status,
          remark,
          fbTransactionHash
        );
        await this._transactionRepository.updateTransactionStatusFromFireblocks(
          fireblocksTransactionInfo.ref_id,
          vStatus,
          fbTransactionHash,
          status
        );
      } else if (
        fireblocksTransactionInfo.type ===
        enums.fireblocksTransactionType.AUTO_FUELING_GAS_FEES
      ) {
        const transactions = await this._transactionRepository.getInfoById(
          fireblocksTransactions[0].ref_id
        );
        if (transactions.length <= 0) {
          return;
        }
        const transactionInfo = transactions[0];

        await this._fireblocksTransactionRepository.updateStatusFromFireblocks(
          fireblocksTransactionInfo.fireblock_transaction_id,
          status,
          remark,
          fbTransactionHash
        );

        const isExistsFBTransactions =
          await this._fireblocksTransactionRepository.getInfoByTypeRefId(
            enums.fireblocksTransactionType.TRANSFER_SELLING,
            fireblocksTransactions[0].ref_id
          );
        if (isExistsFBTransactions.length > 0) {
          return;
        }

        const estimateFee =
          await this._fireblocksService.estimateFeeForTransaction(
            transactionInfo.asset_code,
            transactionInfo.user_fireblock_vault_account_id,
            transactionInfo.company_fireblock_vault_account_id,
            transactionInfo.total_assets
          );
        const transactionFee =
          estimateFee.medium.networkFee === undefined
            ? 0
            : parseFloat(estimateFee.medium.networkFee);
        const fbTransaction = await this._fireblocksService.createTransaction(
          transactionInfo.asset_code,
          transactionInfo.user_fireblock_vault_account_id,
          transactionInfo.company_fireblock_vault_account_id,
          transactionInfo.total_assets,
          transactionFee
        );
        await this._fireblocksTransactionRepository.create(
          fbTransaction.id,
          enums.fireblocksTransactionType.TRANSFER_SELLING,
          transactionInfo.asset_code,
          transactionInfo.user_fireblock_vault_account_id,
          "VAULT_ACCOUNT",
          transactionInfo.total_assets,
          transactionFee,
          transactionInfo.transaction_id,
          fbTransaction.status,
          transactionInfo.company_fireblock_vault_account_id,
          undefined,
          "VAULT_ACCOUNT"
        );
      }
    } catch (error) {
      throw error;
    }
  }

  async manualFreezeTransaction(fbTransactionId: string) {
    try {
      await this.freezeTransaction(fbTransactionId);
    } catch (error) {
      throw error;
    }
  }

  async manualUnfreezeTransaction(fbTransactionId: string) {
    try {
      await this.unfreezeTransaction(fbTransactionId);
    } catch (error) {
      throw error;
    }
  }

  async freezeTransaction(fbTransactionId: string) {
    try {
      await this._fireblocksService.freezeTransactionById(fbTransactionId);
    } catch (error) {
      throw error;
    }
  }

  async unfreezeTransaction(fbTransactionId: string) {
    try {
      await this._fireblocksService.unfreezeTransactionById(fbTransactionId);
    } catch (error) {
      throw error;
    }
  }
}
