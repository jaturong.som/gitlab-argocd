import { CountryRepository } from "../repositories/countryRepository";
import { CurrencyRepository } from "../repositories/currencyRepository";
import { DropdownlistRepository } from "../repositories/dropdownlistRepository";
import { singleton } from "tsyringe";
import { NationalityRepository } from "../repositories/nationalityRepository";

@singleton()
export class MasterService {
  readonly _countryRepository: CountryRepository;
  readonly _currencyRepository: CurrencyRepository;
  readonly _dropdownlistRepository: DropdownlistRepository;
  readonly _nationalityRepository: NationalityRepository;
  constructor(
    countryRepository: CountryRepository,
    currencyRepository: CurrencyRepository,
    dropdownlistRepository: DropdownlistRepository,
    nationalityRepository: NationalityRepository,
  ) {
    this._countryRepository = countryRepository;
    this._currencyRepository = currencyRepository;
    this._dropdownlistRepository = dropdownlistRepository;
    this._nationalityRepository = nationalityRepository;
  }

  async getCountryList() {
    try {
      const result = await this._countryRepository.getList();
      var vResult = [];
      for (let i = 0; i < result.length; i++) {
        vResult.push(await this.setCountrylistInfo(result[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async setCountrylistInfo(data: any) {
    return {
      code: data.code,
      alpha_2_code: data.alpha_2_code,
      name: data.name,
      image: data.image,
      calling_code: data.calling_code,
    };
  }

  async getCurrencyList() {
    try {
      const result = await this._currencyRepository.getList();
      var vResult = [];
      for (let i = 0; i < result.length; i++) {
        vResult.push(await this.setCurrencylistInfo(result[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async setCurrencylistInfo(data: any) {
    return {
      code: data.code,
    };
  }

  async getDropdownListByType(type: string) {
    try {
      const result = await this._dropdownlistRepository.getListByType(type);
      var vResult = [];
      for (let i = 0; i < result.length; i++) {
        vResult.push(await this.setDropdownlistInfo(result[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async setDropdownlistInfo(data: any) {
    return {
      value: data.value,
      text: data.text,
    };
  }

  async getNationalityList() {
    try {
      const result = await this._nationalityRepository.getList();
      var vResult = [];
      for (let i = 0; i < result.length; i++) {
        vResult.push(await this.setNationaliotylistInfo(result[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async setNationaliotylistInfo(data: any) {
    return {
      value: data.code,
      text: data.name,
    };
  }
}
