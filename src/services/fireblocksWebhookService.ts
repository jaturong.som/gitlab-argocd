import { TransactionService } from "../services/transactionService";
import { ChainalysisService } from "../services/chainalysisService";
import { AssetInVaultAccountService } from "../services/assetInVaultAccountService";
import * as enums from "../utilities/enums";
import { singleton } from "tsyringe";

@singleton()
export class FireblocksWebhookService {
  readonly _transactionService: TransactionService;
  readonly _chainalysisService: ChainalysisService;
  readonly _assetInVaultAccountService: AssetInVaultAccountService;
  constructor(
    transactionService: TransactionService,
    chainalysisService: ChainalysisService,
    assetInVaultAccountService: AssetInVaultAccountService
  ) {
    this._transactionService = transactionService;
    this._chainalysisService = chainalysisService;
    this._assetInVaultAccountService = assetInVaultAccountService;
  }

  async monitoringIncomingTransaction(body: any) {
    try {
      if (body.type === "TRANSACTION_STATUS_UPDATED") {
        if (body.data != undefined) {
          const vData = body.data;
          if (
            vData.status === enums.fireblocksTransactionStatus.COMPLETED ||
            vData.status === enums.fireblocksTransactionStatus.FAILED ||
            vData.status === enums.fireblocksTransactionStatus.CANCELLED ||
            vData.status === enums.fireblocksTransactionStatus.REJECTED
          ) {
            await this._transactionService.updateTransactionStatus(
              vData.id,
              vData.status,
              vData.subStatus,
              vData.txHash
            );
            if (process.env.NODE_ENV! !== "development") {
              // Freezes transaction so that it will not be available for spending
              if (
                vData.status === enums.fireblocksTransactionStatus.COMPLETED
              ) {
                await this._chainalysisService.monitoringTransaction(body);
              }
            }
          }
        }
      } else if (body.type === "VAULT_ACCOUNT_ASSET_ADDED") {
        if (body.data != undefined) {
          const vData = body.data;
          await this._assetInVaultAccountService.syncFireblocksAssetAdded(
            vData.accountId
          );
        }
      }
    } catch (error) {
      throw error;
    }
  }

  async updateTransactionStatus(
    fireblockTransactionId: string,
    status: string,
    remark: string,
    fbTransactionHash: string
  ) {
    try {
      return await this._transactionService.updateTransactionStatus(
        fireblockTransactionId,
        status,
        remark,
        fbTransactionHash
      );
    } catch (error) {
      throw error;
    }
  }
}
