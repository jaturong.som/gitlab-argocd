import { singleton } from "tsyringe";
import fetch from "cross-fetch";
import { FetchLogRepository } from "../repositories/fetchLogRepository";

@singleton()
export class HttpService {
  readonly _fetchLogRepository: FetchLogRepository;
  constructor(fetchLogRepository: FetchLogRepository) {
    this._fetchLogRepository = fetchLogRepository;
  }

  async callFetch(method: string, header: any, url: string, body: any) {
    try {
      const result = await fetch(url, {
        method: method,
        headers: header,
        body: body,
      });
      var response = await result.json();
      await this._fetchLogRepository.create(
        method,
        url,
        header,
        body,
        response
      );
      return response;
    } catch (error) {
      throw error;
    }
  }
}
