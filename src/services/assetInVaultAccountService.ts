import { CompanyRepository } from "../repositories/companyRepository";
import { AssetRepository } from "../repositories/assetRepository";
import { VaultAccountRepository } from "../repositories/vaultAccountRepository";
import { AssetInVaultAccountRepository } from "../repositories/assetInVaultAccountRepository";
import { FireblocksService } from "./fireblocksService";
import * as enums from "../utilities/enums";
import CustomError from "../middlewares/customError";
import { singleton } from "tsyringe";

@singleton()
export class AssetInVaultAccountService {
  readonly _companyRepository: CompanyRepository;
  readonly _assetRepository: AssetRepository;
  readonly _vaultAccountRepository: VaultAccountRepository;
  readonly _assetInVaultAccountRepository: AssetInVaultAccountRepository;
  readonly _fireblocksService: FireblocksService;
  constructor(
    companyRepository: CompanyRepository,
    assetRepository: AssetRepository,
    vaultAccountRepository: VaultAccountRepository,
    assetInVaultAccountRepository: AssetInVaultAccountRepository,
    fireblocksService: FireblocksService
  ) {
    this._companyRepository = companyRepository;
    this._assetRepository = assetRepository;
    this._vaultAccountRepository = vaultAccountRepository;
    this._assetInVaultAccountRepository = assetInVaultAccountRepository;
    this._fireblocksService = fireblocksService;
  }

  async getAssetsInVaultAccount(vaultAccountId: number) {
    try {
      const assetInVaults = await this._assetInVaultAccountRepository.getList(
        vaultAccountId
      );
      var vResult = [];
      for (let i = 0; i < assetInVaults.length; i++) {
        vResult.push(await this.setAssetInVaultAccountInfo(assetInVaults[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getAssetInVaultAccountById(assetInVaultAccountId: number) {
    try {
      const assetInVault =
        await this._assetInVaultAccountRepository.getInfoById(
          assetInVaultAccountId
        );
      if (assetInVault.length <= 0) {
        throw new CustomError(
          enums.responseCode.AssetNotFound,
          enums.responseMessage.AssetNotFound
        );
      }
      return await this.setAssetInVaultAccountInfo(assetInVault[0]);
    } catch (error) {
      throw error;
    }
  }

  async getCryptoWalletList(vaultAccountId: number) {
    try {
      const assetInVaults =
        await this._assetInVaultAccountRepository.getCryptoWalletList(
          vaultAccountId
        );
      var vResult = [];
      for (let i = 0; i < assetInVaults.length; i++) {
        vResult.push(await this.setAssetInVaultAccountInfo(assetInVaults[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getAssetsInVaultAccountByUserId(userId: number) {
    try {
      const assetInVaults =
        await this._assetInVaultAccountRepository.getListByUserId(userId);
      var vResult = [];
      for (let i = 0; i < assetInVaults.length; i++) {
        vResult.push(await this.setAssetInVaultAccountInfo(assetInVaults[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async setAssetInVaultAccountInfo(assetInVaultInfo: any) {
    return {
      asset_in_vault_account_id: assetInVaultInfo.asset_in_vault_account_id,
      network_id: assetInVaultInfo.network_id,
      network_code: assetInVaultInfo.network_code,
      network_name: assetInVaultInfo.network_name,
      network_image: assetInVaultInfo.network_image,
      asset_id: assetInVaultInfo.asset_id,
      asset_code: assetInVaultInfo.asset_code,
      asset_name: assetInVaultInfo.asset_name,
      asset_image: assetInVaultInfo.asset_image,
      asset_address: assetInVaultInfo.asset_address,
      total: assetInVaultInfo.total,
      lockedamount: assetInVaultInfo.lockedamount,
      available: assetInVaultInfo.available,
      pending: assetInVaultInfo.pending,
      total_selling_amount: assetInVaultInfo.total_selling_amount,
      buying: assetInVaultInfo.buying,
      selling: assetInVaultInfo.selling,
      status: assetInVaultInfo.status,
      updated_by: assetInVaultInfo.updated_by,
      updated_date: assetInVaultInfo.updated_date,
    };
  }

  async createAssetInVaultAccount(
    type: string,
    assetCode: string,
    logOnId: string,
    companyId?: number,
    userId?: number
  ) {
    try {
      const assets = await this._assetRepository.getInfoByCode(assetCode);
      if (assets.length <= 0) {
        throw new CustomError(
          enums.responseCode.AssetNotFound,
          enums.responseMessage.AssetNotFound
        );
      }
      const assetInfo = assets[0];

      const vaultAccounts = await this._vaultAccountRepository.getInfo(
        type,
        companyId,
        userId
      );
      if (vaultAccounts.length <= 0) {
        throw new CustomError(
          enums.responseCode.VaultAccountNotFound,
          enums.responseMessage.VaultAccountNotFound
        );
      }
      const vaultAccountInfo = vaultAccounts[0];

      const assetInVaultInfo =
        await this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(
          vaultAccountInfo.vault_account_id,
          assetCode
        );
      if (assetInVaultInfo.length > 0) {
        if (
          assetInVaultInfo[0].status === enums.transactionStatus.ACTIVE ||
          assetInVaultInfo[0].status === enums.transactionStatus.PENDING ||
          assetInVaultInfo[0].status === enums.transactionStatus.ONHOLD
        ) {
          throw new CustomError(
            enums.responseCode.Duplicate,
            enums.responseMessage.Duplicate
          );
        }
      }

      var vStatus = enums.transactionStatus.ACTIVE;
      var vStepProcess = enums.stepJobProcess.COMPLETED;
      if (
        assetInfo.asset_code === enums.specialAssetCode.ALGO_USDC_UV4I ||
        assetInfo.asset_code === enums.specialAssetCode.SOL_USDC_PTHX ||
        assetInfo.asset_code === enums.specialAssetCode.XLM_USDC_5F3T
      ) {
        vStatus = enums.transactionStatus.PENDING;
        vStepProcess = enums.stepJobProcess.SUBMITTED;
      }
      var vAssetAddress = "";
      if (vStatus === enums.transactionStatus.ACTIVE) {
        const fbResult = await this._fireblocksService.createVaultAsset(
          vaultAccountInfo.fireblock_vault_account_id,
          assetInfo.asset_code
        );
        vAssetAddress = fbResult.address;
      }
      if (assetInVaultInfo.length > 0) {
        await this._assetInVaultAccountRepository.updateStatus(
          assetInVaultInfo[0].asset_in_vault_account_id,
          vStatus
        );
      } else {
        await this._assetInVaultAccountRepository.create(
          vaultAccountInfo.vault_account_id,
          assetInfo.asset_id,
          assetInfo.asset_code,
          vAssetAddress,
          0,
          0,
          0,
          0,
          vStatus,
          vStepProcess,
          logOnId
        );
      }
      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  async syncFireblocksAssetAdded(FireblocksVaultAccountId: string) {
    try {
      const vaultAccounts =
        await this._vaultAccountRepository.getInfoByFireblocksVaultAccountId(
          FireblocksVaultAccountId
        );
      if (vaultAccounts.length <= 0) {
        return;
      }
      const vaultAccountInfo = vaultAccounts[0];

      await this.syncFireblocksAssetInVaults(
        enums.accountType.USER,
        undefined,
        vaultAccountInfo.user_id
      );
    } catch (error) {
      throw error;
    }
  }
  async syncFireblocksAssetInVaults(
    type: string,
    companyId?: number,
    userId?: number
  ) {
    try {
      const vaultAccounts = await this._vaultAccountRepository.getInfo(
        type,
        companyId,
        userId
      );
      if (vaultAccounts.length <= 0) {
        return;
      }
      const vaultAccountInfo = vaultAccounts[0];

      const fbVaultAccount = await this._fireblocksService.getVaultAccountById(
        vaultAccountInfo.fireblock_vault_account_id
      );
      if (fbVaultAccount === undefined) {
        return;
      } else {
        if (fbVaultAccount.assets !== undefined) {
          for (let index = 0; index < fbVaultAccount.assets.length; index++) {
            const fbAsset = fbVaultAccount.assets[index];
            const assetInVaults =
              await this._assetInVaultAccountRepository.getInfoByVaultAccountIdAssetCodeIgnoreStatus(
                vaultAccountInfo.vault_account_id,
                fbAsset.id
              );
            if (assetInVaults.length <= 0) {
              const assets =
                await this._assetRepository.getInfoByCodeIgnoreStatus(
                  fbAsset.id
                );
              if (assets.length <= 0) {
                continue;
              }
              const assetInfo = assets[0];
              const fbAssetAddress = await this.getFBAssetAddress(
                vaultAccountInfo.fireblock_vault_account_id,
                fbAsset.id
              );
              await this._assetInVaultAccountRepository.create(
                vaultAccountInfo.vault_account_id,
                assetInfo.asset_id,
                assetInfo.asset_code,
                fbAssetAddress,
                parseFloat(fbAsset.total),
                fbAsset.available === undefined
                  ? 0
                  : parseFloat(fbAsset.available),
                fbAsset.pending === undefined ? 0 : parseFloat(fbAsset.pending),
                fbAsset.lockedAmount === undefined
                  ? 0
                  : parseFloat(fbAsset.lockedAmount),
                assetInfo.asset_status,
                enums.stepJobProcess.COMPLETED,
                "0"
              );
            } else {
              const assetInVaultInfo = assetInVaults[0];
              if (assetInVaultInfo.asset_address === "") {
                const fbAssetAddress = await this.getFBAssetAddress(
                  vaultAccountInfo.fireblock_vault_account_id,
                  fbAsset.id
                );
                if (fbAssetAddress !== "") {
                  await this.updateAssetAddress(
                    assetInVaultInfo.asset_in_vault_account_id,
                    fbAssetAddress
                  );
                }
              }
              await this._assetInVaultAccountRepository.syncBalanceFromFireblocks(
                assetInVaultInfo.asset_in_vault_account_id,
                parseFloat(fbAsset.total),
                fbAsset.available === undefined
                  ? 0
                  : parseFloat(fbAsset.available),
                fbAsset.pending === undefined ? 0 : parseFloat(fbAsset.pending),
                fbAsset.lockedAmount === undefined
                  ? 0
                  : parseFloat(fbAsset.lockedAmount)
              );
            }
          }
        }
      }
    } catch (error) {
      throw error;
    }
  }

  async getFBAssetAddress(fbVaultAccountId: string, assetId: string) {
    try {
      const fbAddress = await this._fireblocksService.getDepositAddresses(
        fbVaultAccountId,
        assetId
      );
      if (fbAddress !== undefined) {
        if (fbAddress.length > 0) {
          if (fbAddress[0].assetId === assetId) {
            return fbAddress[0].address;
          }
        }
      }
      return "";
    } catch (error) {
      throw error;
    }
  }

  async updateAssetAddress(
    assetInVaultAccountId: number,
    fbAssetAddress: string
  ) {
    try {
      await this._assetInVaultAccountRepository.updateAddress(
        assetInVaultAccountId,
        fbAssetAddress
      );
    } catch (error) {
      throw error;
    }
  }

  async updateStepProcess(assetInVaultAccountId: number, stepProcess: string) {
    try {
      await this._assetInVaultAccountRepository.updateStepProcess(
        assetInVaultAccountId,
        stepProcess
      );
    } catch (error) {
      throw error;
    }
  }
}
