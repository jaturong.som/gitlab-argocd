import { FireblocksNetworkLinkRepository } from "../repositories/fireblocksNetworkLinkRepository";
import { AssetInVaultAccountRepository } from "../repositories/assetInVaultAccountRepository";
import { AssetRepository } from "../repositories/assetRepository";
import { AssetRateRepository } from "../repositories/assetRateRepository";
import { TransactionRepository } from "../repositories/transactionRepository";
import { FireblocksTransactionRepository } from "../repositories/fireblocksTransactionRepository";
import { VaultAccountRepository } from "../repositories/vaultAccountRepository";
import { CustomerService } from "./customerService";
import { FireblocksService } from "./fireblocksService";
import FireblockError from "../middlewares/fireblocksNetworkLinkCustomError";
import * as enums from "../utilities/enums";
import * as utils from "../utilities/utils";
import { singleton } from "tsyringe";

@singleton()
export class FireblocksNetworkLinkService {
  readonly _fireblocksNetworkLinkRepository: FireblocksNetworkLinkRepository;
  readonly _assetInVaultAccountRepository: AssetInVaultAccountRepository;
  readonly _assetRepository: AssetRepository;
  readonly _assetRateRepository: AssetRateRepository;
  readonly _transactionRepository: TransactionRepository;
  readonly _fireblocksTransactionRepository: FireblocksTransactionRepository;
  readonly _vaultAccountRepository: VaultAccountRepository;
  readonly _customerService: CustomerService;
  readonly _fireblocksService: FireblocksService;
  constructor(
    fireblocksNetworkLinkRepository: FireblocksNetworkLinkRepository,
    assetInVaultAccountRepository: AssetInVaultAccountRepository,
    assetRepository: AssetRepository,
    assetRateRepository: AssetRateRepository,
    transactionRepository: TransactionRepository,
    fireblocksTransactionRepository: FireblocksTransactionRepository,
    vaultAccountRepository: VaultAccountRepository,
    customerService: CustomerService,
    fireblocksService: FireblocksService
  ) {
    this._fireblocksNetworkLinkRepository = fireblocksNetworkLinkRepository;
    this._assetInVaultAccountRepository = assetInVaultAccountRepository;
    this._assetRepository = assetRepository;
    this._assetRateRepository = assetRateRepository;
    this._transactionRepository = transactionRepository;
    this._fireblocksTransactionRepository = fireblocksTransactionRepository;
    this._vaultAccountRepository = vaultAccountRepository;
    this._customerService = customerService;
    this._fireblocksService = fireblocksService;
  }

  async getAllAccountTypes(customerId: number) {
    try {
      var wallet = await this._customerService.getCryptoWalletList(customerId);
      var accountList = [];
      var balances = [];
      for (let j = 0; j < wallet.length; j++) {
        var total = 0;
        var pending = 0;
        var available = 0;
        if (wallet[j].total != null) {
          total = wallet[j].total;
        }
        if (wallet[j].pending != null) {
          pending = wallet[j].pending;
        }
        if (wallet[j].available != null) {
          available = wallet[j].available;
        }

        balances.push({
          coinSymbol: wallet[j].asset_code,
          totalAmount: total,
          pendingAmount: pending,
          availableAmount: available,
          creditAmount: 0,
        });
      }

      accountList.push({
        displayName: "Funding",
        type: "FUNDING",
        balances: balances,
      });
      return accountList;
    } catch (error) {
      throw error;
    }
  }

  async getDepositWalletAddress(
    customerId: number,
    accountType: string,
    coinSymbol: string,
    network: string
  ) {
    try {
      if (accountType != "FUNDING") {
        throw new FireblockError(
          404,
          enums.responseCodeFireblock.NotFound,
          enums.responseMessageFireblock.NotFound
        );
      }
      var assetInVault =
        await this._assetInVaultAccountRepository.getAddressByUserIdAssetNetwork(
          customerId,
          coinSymbol,
          network
        );
      if (assetInVault.length <= 0) {
        throw new FireblockError(
          400,
          enums.responseCodeFireblock.ASSET_NOT_SUPPORTED,
          enums.responseMessageFireblock.ASSET_NOT_SUPPORTED
        );
      }
      return {
        depositAddress: assetInVault[0].asset_address,
        depositAddressTag: null,
      };
    } catch (error) {
      throw error;
    }
  }

  async createDepositWalletAddress(
    customerId: number,
    accountType: string,
    coinSymbol: string,
    network: string
  ) {
    try {
      if (accountType != "FUNDING") {
        throw new FireblockError(
          404,
          enums.responseCodeFireblock.NotFound,
          enums.responseMessageFireblock.NotFound
        );
      }

      var asset = await this._assetRepository.getAssetIdByAssetCodeNetworkName(
        coinSymbol,
        network
      );
      if (asset.length <= 0) {
        throw new FireblockError(
          400,
          enums.responseCodeFireblock.ASSET_NOT_SUPPORTED,
          enums.responseMessageFireblock.ASSET_NOT_SUPPORTED
        );
      }

      await this._customerService.createAssetInVaultAccount(
        customerId,
        asset[0].asset_code,
        customerId.toString()
      );

      return await this.getDepositWalletAddress(
        customerId,
        accountType,
        coinSymbol,
        network
      );
    } catch (error) {
      throw error;
    }
  }

  async getWithDrawalFee(
    transferAmount: number,
    coinSymbol: string,
    network: string
  ) {
    try {
      var asset = await this._assetRepository.getAssetIdByAssetCodeNetworkName(
        coinSymbol,
        network
      );
      if (asset.length <= 0) {
        throw new FireblockError(
          400,
          enums.responseCodeFireblock.ASSET_NOT_SUPPORTED,
          enums.responseMessageFireblock.ASSET_NOT_SUPPORTED
        );
      }

      return { feeAmount: "0" };
    } catch (error) {
      throw error;
    }
  }

  async createWithDraw(
    customerId: number,
    amount: number,
    maxFee: string,
    coinSymbol: string,
    network: string,
    accountType: string,
    toAddress: string,
    tag: string,
    isGross: boolean
  ) {
    try {
      if (accountType != "FUNDING") {
        throw new FireblockError(
          404,
          enums.responseCodeFireblock.NotFound,
          enums.responseMessageFireblock.NotFound
        );
      }

      const type = enums.transactionType.SELLING;

      const userVaultAccounts = await this._vaultAccountRepository.getInfo(
        enums.accountType.USER,
        undefined,
        customerId
      );
      if (userVaultAccounts.length <= 0) {
        throw new FireblockError(
          404,
          enums.responseCodeFireblock.NotFound,
          enums.responseMessageFireblock.NotFound
        );
      }
      const userVaultAccountInfo = userVaultAccounts[0];

      const assetRates = await this._assetRateRepository.getInfoByAssetCode(
        coinSymbol
      );
      if (assetRates.length <= 0) {
        throw new FireblockError(
          400,
          enums.responseCodeFireblock.ASSET_NOT_SUPPORTED,
          enums.responseMessageFireblock.ASSET_NOT_SUPPORTED
        );
      }
      const assetRateInfo = assetRates[0];

      const transactions = await this._transactionRepository.create(
        customerId,
        await utils.generateKey(customerId.toString()),
        type,
        userVaultAccountInfo.vault_account_id,
        assetRateInfo.asset_id,
        assetRateInfo.asset_code,
        assetRateInfo.buying,
        assetRateInfo.selling,
        amount * assetRateInfo.buying,
        amount,
        enums.transactionStatus.ONHOLD,
        "0",
        enums.source.FIREBLOCKS,
        false,
        "",
        "",
        undefined,
        toAddress
      );
      const transaction = transactions[0];

      const estimateFee =
        await this._fireblocksService.estimateFeeForTransactionNetworkLink(
          coinSymbol,
          userVaultAccountInfo.fireblock_vault_account_id,
          toAddress,
          amount
        );
      const transactionFee =
        estimateFee.medium.networkFee === undefined
          ? 0
          : parseFloat(estimateFee.medium.networkFee);
      const fbTransaction =
        await this._fireblocksService.createTransactionNetworkLink(
          coinSymbol,
          userVaultAccountInfo.fireblock_vault_account_id,
          toAddress,
          amount,
          transactionFee
        );
      await this._fireblocksTransactionRepository.create(
        fbTransaction.id,
        enums.fireblocksTransactionType.TRANSFER_SELLING,
        coinSymbol,
        userVaultAccountInfo.fireblock_vault_account_id,
        "VAULT_ACCOUNT",
        amount,
        transactionFee,
        transaction.transaction_id,
        fbTransaction.status,
        undefined,
        toAddress,
        "ONE_TIME_ADDRESS"
      );
      await this._transactionRepository.updateTransactionWithFBIdStatus(
        transaction.transaction_id,
        enums.transactionStatus.ONHOLD,
        fbTransaction.id
      );
      return {
        transactionID: fbTransaction.id,
      };
    } catch (error) {
      throw error;
    }
  }

  async getTransactionById(transactionID: string) {
    try {
      var transaction =
        await this._transactionRepository.getTransactionByTransactionID(
          transactionID
        );

      if (transaction.length <= 0) {
        return {
          status: "NOT_FOUND",
        };
      }
      var transactionType = "";
      if (transaction[0].type == "S") {
        transactionType = "CRYPTO_WITHDRAWAL";
      } else if (transaction[0].type == "B") {
        transactionType = "CRYPTO_DEPOSIT";
      }

      var timestamp = new Date(transaction[0].transaction_date).valueOf();

      return {
        transactionID: transaction[0].fb_transaction_id,
        status: transaction[0].fb_transaction_status,
        txHash: transaction[0].fb_transaction_hash,
        amount: transaction[0].total_amount.toString(),
        serviceFee: "0",
        coinSymbol: transaction[0].code,
        network: transaction[0].name,
        direction: transactionType,
        timestamp: timestamp,
      };
    } catch (error) {
      throw error;
    }
  }

  async getTransactionByHash(txHash: string, network: string) {
    try {
      var transaction =
        await this._transactionRepository.getTransactionByTransactionHash(
          txHash,
          network
        );
      if (transaction.length <= 0) {
        return {
          status: "NOT_FOUND",
        };
      }
      var transactionType = "";
      if (transaction[0].type == "S") {
        transactionType = "CRYPTO_WITHDRAWAL";
      } else if (transaction[0].type == "B") {
        transactionType = "CRYPTO_DEPOSIT";
      }

      var timestamp = new Date(transaction[0].transaction_date).valueOf();

      return {
        transactionID: transaction[0].fb_transaction_id,
        status: transaction[0].fb_transaction_status,
        txHash: transaction[0].fb_transaction_hash,
        amount: transaction[0].total_amount.toString(),
        serviceFee: "0",
        coinSymbol: transaction[0].code,
        network: transaction[0].name,
        direction: transactionType,
        timestamp: timestamp,
      };
    } catch (error) {
      throw error;
    }
  }

  async getTransactionHistory(
    fromDate: string,
    toDate: string,
    pageSize: string,
    pageCursor: string,
    isSubTransfer: string,
    coinSymbol: string,
    network: string,
    customerId: number
  ) {
    try {
      var tdate = new Date(Number(toDate));
      var fdate = new Date(Number(fromDate));
      var transactionList = [];

      if (pageCursor == "") {
        pageCursor = "1";
      }

      var transaction =
        await this._transactionRepository.getTransactionListPageSize(
          Number(pageSize),
          Number(pageCursor),
          fdate.toISOString(),
          tdate.toISOString(),
          coinSymbol,
          network,
          customerId
        );

      if (transaction.length <= 0) {
        return {
          status: "NOT_FOUND",
        };
      }

      for (let j = 0; j < transaction.length; j++) {
        var transactionType = "";
        if (transaction[j].type == "S") {
          transactionType = "CRYPTO_WITHDRAWAL";
        } else if (transaction[0].type == "B") {
          transactionType = "CRYPTO_DEPOSIT";
        }

        var timestamp = new Date(transaction[j].transaction_date).valueOf();

        transactionList.push({
          transactionID: transaction[j].fb_transaction_id,
          status: transaction[j].fb_transaction_status,
          txHash: transaction[j].fb_transaction_hash,
          amount: transaction[j].total_amount.toString(),
          serviceFee: "0",
          coinSymbol: transaction[j].code,
          network: transaction[j].name,
          direction: transactionType,
          timestamp: timestamp,
        });
      }

      var nextPageCursor = Number(pageCursor) + 1;
      return {
        nextPageCursor: nextPageCursor.toString(),
        transactions: transactionList,
      };
    } catch (error) {
      throw error;
    }
  }

  async getSupportedAssets() {
    try {
      var assetList = await this._assetRepository.getAssetList();
      var assets = [];
      for (let j = 0; j < assetList.length; j++) {
        var identifiers = [];
        identifiers.push(assetList[j].contract_address);
        if (assetList[j].code !== "ALGO_TEST") {
          assets.push({
            coinSymbol: assetList[j].asset_name,
            network: assetList[j].name,
            coinClass: "TOKEN",
            identifiers: identifiers,
          });
        } else {
          assets.push({
            coinSymbol: assetList[j].asset_name,
            network: assetList[j].name,
            coinClass: "BASE",
            identifiers: identifiers,
          });
        }
      }

      return assets;
    } catch (error) {
      throw error;
    }
  }

  async createSubMainTransfer() {
    try {
      return await this._fireblocksNetworkLinkRepository.createSubMainTransfer();
    } catch (error) {
      throw error;
    }
  }

  async createSubAccountsTransfer() {
    try {
      return await this._fireblocksNetworkLinkRepository.createSubAccountsTransfer();
    } catch (error) {
      throw error;
    }
  }

  async createInternalTransfer() {
    try {
      return await this._fireblocksNetworkLinkRepository.createInternalTransfer();
    } catch (error) {
      throw error;
    }
  }
}
