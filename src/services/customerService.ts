import { CompanyRepository } from "../repositories/companyRepository";
import { UserRepository } from "../repositories/userRepository";
import { UserApiKeyRepository } from "../repositories/userApiKeyRepository";
import { BankAccountRepository } from "../repositories/bankAccountRepository";
import { VaultAccountRepository } from "../repositories/vaultAccountRepository";
import { FiatAccountRepository } from "../repositories/fiatAccountRepository";
import { AssetInVaultAccountRepository } from "../repositories/assetInVaultAccountRepository";
import { WithdrawRepository } from "../repositories/withdrawRepository";
import { AssetRepository } from "../repositories/assetRepository";
import { NetworkRepository } from "../repositories/networkRepository";
import { AssetRateRepository } from "../repositories/assetRateRepository";
import { TransactionRepository } from "../repositories/transactionRepository";
import { FireblocksTransactionRepository } from "../repositories/fireblocksTransactionRepository";
import { VaultAccountService } from "./vaultAccountService";
import { AssetInVaultAccountService } from "./assetInVaultAccountService";
import { TransactionService } from "./transactionService";
import { FireblocksService } from "./fireblocksService";
import * as enums from "../utilities/enums";
import CustomError from "../middlewares/customError";
import * as utils from "../utilities/utils";
import { singleton } from "tsyringe";
import { AddressRepository } from "../repositories/addressRepository";
import { UserDocumentRepository } from "../repositories/userDocumentRepository";
// import {
//   RequestUpdateCustomerAddress,
//   RequestUpdateCustomerProfile,
// } from "../models/customerModels";
import { LqnService } from "./lqnService";
import { CountryRepository } from "../repositories/countryRepository";
import { LqnWithdrawService } from "./lqnWithdrawService";
import { LqnUserRepository } from "../repositories/lqnUserRepository";
import { LqnTransactionRepository } from "../repositories/lqnTransactionRepository";
// import moment from "moment";

@singleton()
export class CustomerService {
  readonly _companyRepository: CompanyRepository;
  readonly _userRepository: UserRepository;
  readonly _userApiKeyRepository: UserApiKeyRepository;
  readonly _bankAccountRepository: BankAccountRepository;
  readonly _vaultAccountRepository: VaultAccountRepository;
  readonly _fiatAccountRepository: FiatAccountRepository;
  readonly _assetInVaultAccountRepository: AssetInVaultAccountRepository;
  readonly _withdrawRepository: WithdrawRepository;
  readonly _assetRepository: AssetRepository;
  readonly _networkRepository: NetworkRepository;
  readonly _assetRateRepository: AssetRateRepository;
  readonly _transactionRepository: TransactionRepository;
  readonly _fireblocksTransactionRepository: FireblocksTransactionRepository;
  readonly _addressRepository: AddressRepository;
  readonly _userDocumentRepository: UserDocumentRepository;
  readonly _vaultAccountService: VaultAccountService;
  readonly _assetInVaultAccountService: AssetInVaultAccountService;
  readonly _transactionService: TransactionService;
  readonly _fireblocksService: FireblocksService;
  readonly _lqnService: LqnService;
  readonly _countryRepository: CountryRepository;
  readonly _lqnWithdrawService: LqnWithdrawService;
  readonly _lqnUserRepository: LqnUserRepository;
  readonly _lqnTransactionRepository: LqnTransactionRepository;
  constructor(
    companyRepository: CompanyRepository,
    userRepository: UserRepository,
    userApiKeyRepository: UserApiKeyRepository,
    bankAccountRepository: BankAccountRepository,
    vaultAccountRepository: VaultAccountRepository,
    fiatAccountRepository: FiatAccountRepository,
    assetInVaultAccountRepository: AssetInVaultAccountRepository,
    withdrawRepository: WithdrawRepository,
    assetRepository: AssetRepository,
    networkRepository: NetworkRepository,
    assetRateRepository: AssetRateRepository,
    transactionRepository: TransactionRepository,
    fireblocksTransactionRepository: FireblocksTransactionRepository,
    addressRepository: AddressRepository,
    userDocumentRepository: UserDocumentRepository,
    vaultAccountService: VaultAccountService,
    assetInVaultAccountService: AssetInVaultAccountService,
    transactionService: TransactionService,
    fireblocksService: FireblocksService,
    lqnService: LqnService,
    countryRepository: CountryRepository,
    lqnWithdrawService: LqnWithdrawService,
    lqnUserRepository: LqnUserRepository,
    lqnTransactionRepository: LqnTransactionRepository
  ) {
    this._companyRepository = companyRepository;
    this._userRepository = userRepository;
    this._userApiKeyRepository = userApiKeyRepository;
    this._bankAccountRepository = bankAccountRepository;
    this._vaultAccountRepository = vaultAccountRepository;
    this._fiatAccountRepository = fiatAccountRepository;
    this._assetInVaultAccountRepository = assetInVaultAccountRepository;
    this._withdrawRepository = withdrawRepository;
    this._assetRepository = assetRepository;
    this._networkRepository = networkRepository;
    this._assetRateRepository = assetRateRepository;
    this._transactionRepository = transactionRepository;
    this._fireblocksTransactionRepository = fireblocksTransactionRepository;
    this._addressRepository = addressRepository;
    this._userDocumentRepository = userDocumentRepository;
    this._vaultAccountService = vaultAccountService;
    this._assetInVaultAccountService = assetInVaultAccountService;
    this._transactionService = transactionService;
    this._fireblocksService = fireblocksService;
    this._lqnService = lqnService;
    this._countryRepository = countryRepository;
    this._lqnWithdrawService = lqnWithdrawService;
    this._lqnUserRepository = lqnUserRepository;
    this._lqnTransactionRepository = lqnTransactionRepository;
  }

  async getUserApiKeys(userId: number) {
    try {
      const userApiKeys = await this._userApiKeyRepository.getList(userId);
      var vResult = [];
      for (let i = 0; i < userApiKeys.length; i++) {
        vResult.push(await this.setUserApiKeyInfo(userApiKeys[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getUserApiKeyById(userApiKeyId: number) {
    try {
      const userApiKeys = await this._userApiKeyRepository.getInfoById(
        userApiKeyId
      );
      if (userApiKeys.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      return await this.setUserApiKeyInfo(userApiKeys[0]);
    } catch (error) {
      throw error;
    }
  }

  async setUserApiKeyInfo(userApiKeyInfo: any) {
    return {
      user_api_key_id: userApiKeyInfo.user_api_key_id,
      user_id: userApiKeyInfo.user_id,
      name: userApiKeyInfo.name,
      user_key: userApiKeyInfo.user_key,
      user_secret: userApiKeyInfo.user_secret,
      status: userApiKeyInfo.status,
      created_by: userApiKeyInfo.created_by,
      created_date: userApiKeyInfo.created_date,
    };
  }

  async createUserApiKey(userId: number, name: string, logOnId: string) {
    try {
      const isExists = await this._userApiKeyRepository.isDuplicateByName(
        userId,
        name
      );
      if (isExists.length > 0) {
        throw new CustomError(
          enums.responseCode.Duplicate,
          enums.responseMessage.Duplicate
        );
      }
      await this._userApiKeyRepository.create(
        userId,
        name,
        await utils.generateCustomerApiKey(userId.toString(), name),
        await utils.generateCustomerSignature(userId.toString(), name),
        logOnId
      );
      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  async enableOrDisableUserApiKey(
    userId: number,
    userApiKeyId: number,
    logOnId: string
  ) {
    try {
      const apiKeys = await this._userApiKeyRepository.getInfoById(
        userApiKeyId
      );
      if (apiKeys.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const apiKeyInfo = apiKeys[0];
      await this._userApiKeyRepository.enableOrDisable(
        userId,
        userApiKeyId,
        apiKeyInfo.status === "A" ? "I" : "A",
        logOnId
      );
      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  async disableAllUserApiKeyByUserId(userId: number, logOnId: string) {
    try {
      const isExists = await this._userApiKeyRepository.getList(userId);
      if (isExists.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      await this._userApiKeyRepository.disableAllByUserId(userId, logOnId);
      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  async getUserFiatAccounts(userId: number) {
    try {
      await this._fiatAccountRepository.updateBalanceAllUsers();
      const userFiatAccounts =
        await this._fiatAccountRepository.getInfoByUserId(userId);
      if (userFiatAccounts.length <= 0) {
        throw new CustomError(
          enums.responseCode.FiatAccountNotFound,
          enums.responseMessage.FiatAccountNotFound
        );
      }
      return await this.setFiatAccountInfo(userFiatAccounts[0]);
    } catch (error) {
      throw error;
    }
  }

  async setFiatAccountInfo(fiatAccountInfo: any) {
    return {
      total: fiatAccountInfo.total,
      available: fiatAccountInfo.available,
      pending: fiatAccountInfo.pending,
      lockedamount: fiatAccountInfo.lockedamount,
    };
  }

  // async getBankAccounts(userId: number) {
  //   try {
  //     const bankAccounts = await this._bankAccountRepository.getList(userId);
  //     var vResult = [];
  //     for (let i = 0; i < bankAccounts.length; i++) {
  //       var bankAddress = await this._addressRepository.getInfoByRefId(
  //         enums.addressType.BANK_ACCOUNT,
  //         bankAccounts[i].bank_account_id
  //       );
  //       vResult.push(
  //         await this.setBankAccountInfo(bankAccounts[i], bankAddress)
  //       );
  //     }
  //     return vResult;
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  //deprecate
  async getBankAccountCoutries() {
    try {
      const lqnCountry = await this._lqnService.getCatalogue("CTY", "", "", "");

      const countryList = await this._countryRepository.getList();
      var vResult = [];
      for (let i = 0; i < lqnCountry.length; i++) {
        for (let j = 0; j < countryList.length; j++) {
          var lqn = lqnCountry[i].value;
          var country = countryList[j].code;

          if (lqn == country) {
            vResult.push(
              await this.setBankAccountCountry(i + 1, countryList[j])
            );
          }
        }
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async setBankAccountCountry(id: any, bankAccountCountry: any) {
    return {
      value: bankAccountCountry.name,
      text: bankAccountCountry.code,
    };
  }

  async getBankAccountsByBankType(userId: number, bankType: string) {
    try {
      const bankAccounts = await this._bankAccountRepository.getListByBankType(
        userId,
        bankType
      );
      var vResult = [];
      for (let i = 0; i < bankAccounts.length; i++) {
        vResult.push(await this.setBankAccountInfo(bankAccounts[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getBankAccountById(bankAccountId: number) {
    try {
      const bankAccounts = await this._bankAccountRepository.getInfoById(
        bankAccountId
      );
      if (bankAccounts.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      return await this.setBankAccountInfo(bankAccounts[0]);
    } catch (error) {
      throw error;
    }
  }

  // async getDefaultBankAccountByUser(userId: number) {
  //   try {
  //     const bankAccounts =
  //       await this._bankAccountRepository.getDefaultBankAccount(userId);
  //     if (bankAccounts.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }
  //     var bankAddress = await this._addressRepository.getInfoByRefId(
  //       enums.addressType.BANK_ACCOUNT,
  //       bankAccounts[0].bank_account_id
  //     );
  //     return await this.setBankAccountInfo(bankAccounts[0], bankAddress);
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async setBankAccountInfo(bankAccountInfo: any) {
    var vTypeText = "";
    if (bankAccountInfo.bank_type === enums.bankAccountType.OWNER) {
      vTypeText = "My Book Bank";
    } else if (bankAccountInfo.bank_type === enums.bankAccountType.OTHER) {
      vTypeText = "Beneficiary Book Bank";
    }
    return {
      bank_account_id: bankAccountInfo.bank_account_id,
      bank_type: bankAccountInfo.bank_type,
      bank_type_text: vTypeText,
      location_id: bankAccountInfo.location_id,
      bank_name: bankAccountInfo.bank_name,
      bank_branch_name: bankAccountInfo.bank_branch_name,
      bank_branch_code: bankAccountInfo.bank_branch_code,
      bank_account_number: bankAccountInfo.bank_account_number,
      bank_account_name: bankAccountInfo.bank_account_name,
      swift_code: bankAccountInfo.swift_code,
      iban: bankAccountInfo.iban,
      sender_first_name: bankAccountInfo.sender_first_name,
      sender_middle_name: bankAccountInfo.sender_middle_name,
      sender_last_name: bankAccountInfo.sender_last_name,
      sender_contact_number: bankAccountInfo.sender_contact_number,
      sender_email: bankAccountInfo.sender_email,
      sender_address: bankAccountInfo.sender_address,
      sender_state: bankAccountInfo.sender_state,
      sender_area_town: bankAccountInfo.sender_area_town,
      sender_city: bankAccountInfo.sender_city,
      sender_zip_code: bankAccountInfo.sender_zip_code,
      sender_country_code: bankAccountInfo.sender_country_code,
      sender_country_name: bankAccountInfo.sender_country_name,
      receiver_first_name: bankAccountInfo.receiver_first_name,
      receiver_middle_name: bankAccountInfo.receiver_middle_name,
      receiver_last_name: bankAccountInfo.receiver_last_name,
      receiver_date_of_birth: bankAccountInfo.receiver_date_of_birth,
      receiver_gender: bankAccountInfo.receiver_gender,
      receiver_contact_number: bankAccountInfo.receiver_contact_number,
      receiver_email: bankAccountInfo.receiver_email,
      receiver_nationality: bankAccountInfo.receiver_nationality,
      receiver_nationality_name: bankAccountInfo.receiver_nationality_name,
      receiver_occupation: bankAccountInfo.receiver_occupation,
      receiver_occupation_remarks: bankAccountInfo.receiver_occupation_remarks,
      receiver_id_type: bankAccountInfo.receiver_id_type,
      receiver_id_type_remarks: bankAccountInfo.receiver_id_type_remarks,
      receiver_id_number: bankAccountInfo.receiver_id_number,
      receiver_id_issue_date: bankAccountInfo.receiver_id_issue_date,
      receiver_id_expire_date: bankAccountInfo.receiver_id_expire_date,
      receiver_native_first_name: bankAccountInfo.receiver_native_first_name,
      receiver_native_middle_name: bankAccountInfo.receiver_native_middle_name,
      receiver_native_last_name: bankAccountInfo.receiver_native_last_name,
      receiver_native_address: bankAccountInfo.receiver_native_address,
      receiver_account_type: bankAccountInfo.receiver_account_type,
      receiver_district: bankAccountInfo.receiver_district,
      beneficiary_type: bankAccountInfo.beneficiary_type,
      beneficiary_type_text: bankAccountInfo.beneficiary_type_text,
      currency_code: bankAccountInfo.currency_code,
      receiver_address: bankAccountInfo.receiver_address,
      receiver_state: bankAccountInfo.receiver_state,
      receiver_area_town: bankAccountInfo.receiver_area_town,
      receiver_city: bankAccountInfo.receiver_city,
      receiver_zip_code: bankAccountInfo.receiver_zip_code,
      receiver_country: bankAccountInfo.receiver_country_code,
      receiver_country_name: bankAccountInfo.receiver_country_name,
    };
  }

  async createBankAccount(
    bankType: string,
    receiverFirstName: string,
    receiverMiddleName: string,
    receiverLastName: string,
    receiverAddress: string,
    receiverDateOfBirth: string,
    receiverGender: string,
    receiverContactNumber: string,
    receiverState: string,
    receiverAreaTown: string,
    receiverCity: string,
    receiverZipCode: string,
    receiverCountry: string,
    receiverNationality: string,
    receiverIdType: string,
    receiverIdTypeRemarks: string,
    receiverIdNumber: string,
    receiverIdIssueDate: string,
    receiverIdExpireDate: string,
    receiverEmail: string,
    receiverAccountType: string,
    receiverOccupation: string,
    receiverOccupationRemarks: string,
    receiverDistrict: string,
    beneficiaryType: string,
    locationId: string,
    bankName: string,
    bankBranchName: string,
    bankBranchCode: string,
    bankAccountNumber: string,
    swiftCode: string,
    iban: string,
    userId: number,
    logOnId: string
  ) {
    try {
      const users = await this._userRepository.getActiveInfoById(userId);
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }
      // const userInfo = users[0];

      var countries = await this._countryRepository.getInfoByCode(
        receiverCountry
      );
      if (countries.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const countryInfo = countries[0];

      var senders = await this._lqnUserRepository.getLqnUserByType(
        userId,
        enums.lqnUserType.SENDER
      );
      if (senders.length <= 0) {
        throw new CustomError(
          enums.responseCode.SenderNotfound,
          enums.responseMessage.SenderNotfound
        );
      }
      const senderInfo = senders[0];

      const paymentMode = enums.paymentMode.BANK_TRANSFER;

      if (bankType == enums.bankAccountType.OWNER) {
        if (senderInfo.country_code == receiverCountry) {
          var addresses = await this._addressRepository.getInfoByRefId(
            enums.addressType.USER_PROFILE,
            userId
          );
          if (addresses.length <= 0) {
            receiverAddress = "";
            receiverState = "";
            receiverAreaTown = "";
            receiverCity = "";
            receiverZipCode = "";
            receiverCountry = "";
          } else {
            const addressInfo = addresses[0];
            receiverAddress = addressInfo.line1;
            receiverState = addressInfo.state;
            receiverAreaTown = addressInfo.area_town;
            receiverCity = addressInfo.city;
            receiverZipCode = addressInfo.postcode;
            receiverCountry = addressInfo.country_code;
          }
        }

        receiverFirstName = senderInfo.first_name;
        receiverMiddleName = senderInfo.middle_name;
        receiverLastName = senderInfo.last_name;
        receiverDateOfBirth = senderInfo.date_of_birth;
        receiverGender = senderInfo.gender;
        receiverContactNumber = senderInfo.mobile_number;
        receiverNationality = senderInfo.nationality;
        receiverIdType = senderInfo.id_type;
        receiverIdTypeRemarks = senderInfo.id_type_remark;
        receiverIdNumber = senderInfo.id_number;
        receiverIdIssueDate = senderInfo.id_issue_date;
        receiverIdExpireDate = senderInfo.id_expired_date;
        receiverEmail = senderInfo.email;
        receiverAccountType = "";
        receiverOccupation = senderInfo.occupation;
        receiverOccupationRemarks = senderInfo.occupation_remark;
        receiverDistrict = "";
        beneficiaryType = senderInfo.user_type;
      } else {
        await this.validateBankAccount(
          bankType,
          receiverFirstName,
          receiverLastName,
          receiverAddress,
          receiverDateOfBirth,
          receiverGender,
          receiverContactNumber,
          receiverZipCode,
          receiverCountry,
          receiverIdType,
          receiverIdNumber,
          receiverIdIssueDate,
          receiverIdExpireDate,
          receiverOccupation,
          beneficiaryType
        );
      }

      const createReceiver = await this._lqnService.createReceiver(
        receiverFirstName,
        receiverMiddleName,
        receiverLastName,
        receiverAddress,
        receiverDateOfBirth,
        receiverGender,
        receiverContactNumber,
        receiverState,
        receiverAreaTown,
        receiverCity,
        receiverZipCode,
        receiverCountry,
        receiverNationality,
        receiverIdType,
        receiverIdTypeRemarks,
        receiverIdNumber,
        receiverIdIssueDate,
        receiverIdExpireDate,
        receiverEmail,
        receiverAccountType,
        receiverOccupation,
        receiverOccupationRemarks,
        receiverDistrict,
        beneficiaryType,
        locationId,
        bankName,
        bankBranchName,
        bankBranchCode,
        bankAccountNumber,
        swiftCode,
        iban,
        paymentMode,
        countryInfo.currency_code,
        senderInfo.lqn_ref_id
      );

      if (createReceiver.code !== "0") {
        throw new CustomError(createReceiver.code, createReceiver.message);
      }

      const accountName = receiverFirstName + " " + receiverLastName;
      const bankAccountCreated = await this._bankAccountRepository.create(
        userId,
        bankType,
        locationId,
        bankName,
        bankAccountNumber,
        accountName,
        swiftCode,
        iban,
        bankBranchName,
        bankBranchCode,
        logOnId
      );

      const lqnUserProfiles = await this._lqnUserRepository.create(
        enums.lqnUserType.RECEIVER,
        receiverFirstName,
        receiverMiddleName,
        receiverLastName,
        receiverGender,
        receiverContactNumber,
        receiverNationality,
        receiverIdType,
        receiverIdTypeRemarks,
        receiverIdNumber,
        receiverCountry,
        receiverIdIssueDate,
        receiverIdExpireDate,
        receiverDateOfBirth,
        receiverOccupation,
        receiverOccupationRemarks,
        receiverEmail,
        receiverFirstName,
        receiverMiddleName,
        receiverLastName,
        createReceiver.receiverId,
        logOnId,
        senderInfo.lqn_ref_id,
        countryInfo.currency_code,
        enums.status.ACTIVE,
        userId,
        beneficiaryType,
        bankAccountCreated[0].bank_account_id,
        receiverCountry
      );

      await this._addressRepository.create(
        enums.addressType.LQN_USER_PROFILE,
        lqnUserProfiles[0].lqn_user_profile_id,
        receiverAddress,
        "",
        receiverCity,
        receiverState,
        receiverCountry,
        receiverZipCode,
        receiverAreaTown,
        logOnId
      );
      return bankAccountCreated[0].bank_account_id;
    } catch (error) {
      throw error;
    }
  }

  async validateBankAccount(
    bankType: string,
    receiverFirstName: string,
    // receiverMiddleName: string,
    receiverLastName: string,
    receiverAddress: string,
    receiverDateOfBirth: string,
    receiverGender: string,
    receiverContactNumber: string,
    // receiverState: string,
    // receiverAreaTown: string,
    // receiverCity: string,
    receiverZipCode: string,
    receiverCountry: string,
    // receiverNationality: string,
    receiverIdType: string,
    // receiverIdTypeRemarks: string,
    receiverIdNumber: string,
    receiverIdIssueDate: string,
    receiverIdExpireDate: string,
    // receiverEmail: string,
    // receiverAccountType: string,
    receiverOccupation: string,
    // receiverOccupationRemarks: string,
    // receiverDistrict: string,
    beneficiaryType: string
    // locationId: string,
    // bankName: string,
    // bankBranchName: string,
    // bankBranchCode: string,
    // bankAccountNumber: string,
    // swiftCode: string,
    // iban: string,
    // userId: number
  ) {
    try {
      if (receiverFirstName === undefined || receiverFirstName === undefined) {
        throw new CustomError(
          enums.responseCodeLQN.INVALID_FIRST_NAME,
          enums.responseMessageLQN.INVALID_FIRST_NAME
        );
      } else if (
        receiverLastName === undefined ||
        receiverLastName === undefined
      ) {
        throw new CustomError(
          enums.responseCodeLQN.INVALID_LAST_NAME,
          enums.responseMessageLQN.INVALID_LAST_NAME
        );
      } else if (receiverAddress === undefined || receiverAddress === "") {
        throw new CustomError(
          enums.responseCodeLQN.INVALID_ADDRESS,
          enums.responseMessageLQN.INVALID_ADDRESS
        );
      } else if (receiverZipCode === undefined || receiverZipCode === "") {
        throw new CustomError(
          enums.responseCodeLQN.INVALID_POSTCODE,
          enums.responseMessageLQN.INVALID_POSTCODE
        );
      } else if (beneficiaryType === undefined || beneficiaryType === "") {
        throw new CustomError(
          enums.responseCodeLQN.INVALID_BENEFICIARY,
          enums.responseMessageLQN.INVALID_BENEFICIARY
        );
      }

      if (receiverCountry === "SGP") {
        if (
          receiverContactNumber === undefined ||
          receiverContactNumber === ""
        ) {
          throw new CustomError(
            enums.responseCodeLQN.INVALID_CONTACT_NUMBER,
            enums.responseMessageLQN.INVALID_CONTACT_NUMBER
          );
        }
      } else if (receiverCountry === "CHN") {
        if (receiverDateOfBirth === undefined || receiverDateOfBirth === "") {
          throw new CustomError(
            enums.responseCodeLQN.INVALID_DATE_OF_BIRTH,
            enums.responseMessageLQN.INVALID_DATE_OF_BIRTH
          );
        } else if (receiverGender === undefined || receiverGender === "") {
          throw new CustomError(
            enums.responseCodeLQN.INVALID_GENDER,
            enums.responseMessageLQN.INVALID_GENDER
          );
        } else if (
          receiverContactNumber === undefined ||
          receiverContactNumber === ""
        ) {
          throw new CustomError(
            enums.responseCodeLQN.INVALID_CONTACT_NUMBER,
            enums.responseMessageLQN.INVALID_CONTACT_NUMBER
          );
        } else if (receiverIdType === undefined || receiverIdType === "") {
          throw new CustomError(
            enums.responseCodeLQN.INVALID_ID_TYPE,
            enums.responseMessageLQN.INVALID_ID_TYPE
          );
        } else if (receiverIdNumber === undefined || receiverIdNumber === "") {
          throw new CustomError(
            enums.responseCodeLQN.INVALID_ID_NUMBER,
            enums.responseMessageLQN.INVALID_ID_NUMBER
          );
        } else if (
          receiverIdIssueDate === undefined ||
          receiverIdIssueDate === ""
        ) {
          throw new CustomError(
            enums.responseCodeLQN.INVALID_ID_ISSUE_DATE,
            enums.responseMessageLQN.INVALID_ID_ISSUE_DATE
          );
        } else if (
          receiverIdExpireDate === undefined ||
          receiverIdExpireDate === ""
        ) {
          throw new CustomError(
            enums.responseCodeLQN.INVALID_ID_EXPIRE_DATE,
            enums.responseMessageLQN.INVALID_ID_EXPIRE_DATE
          );
        } else if (
          receiverOccupation === undefined ||
          receiverOccupation === ""
        ) {
          throw new CustomError(
            enums.responseCodeLQN.INVALID_OCCUPATION,
            enums.responseMessageLQN.INVALID_OCCUPATION
          );
        }
      }
    } catch (error) {
      throw error;
    }
  }

  // async createOwnerBankAccount(
  //   userId: number,
  //   bankId: string,
  //   bankCode: string,
  //   bankName: string,
  //   accountNo: string,
  //   accountName: string,
  //   swiftCode: string,
  //   iban: string,
  //   logOnId: string
  // ) {
  //   try {
  //     const users = await this._userRepository.getActiveInfoById(userId);
  //     if (users.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.UserNotFound,
  //         enums.responseMessage.UserNotFound
  //       );
  //     }
  //     const userInfo = users[0];

  //     const isExists =
  //       await this._bankAccountRepository.isDuplicateByBankIdAccountNo(
  //         enums.mode.INSERT,
  //         userId,
  //         enums.bankAccountType.OWNER,
  //         bankCode,
  //         accountNo,
  //         undefined
  //       );
  //     if (isExists.length > 0) {
  //       throw new CustomError(
  //         enums.responseCode.Duplicate,
  //         enums.responseMessage.Duplicate
  //       );
  //     }

  //     const fullName = userInfo.first_name + " " + userInfo.last_name;
  //     if (accountName.toLowerCase() !== fullName.toLowerCase()) {
  //       throw new CustomError(
  //         enums.responseCode.AccountNameMismatch,
  //         enums.responseMessage.AccountNameMismatch
  //       );
  //     }
  //     var sender = await this._lqnUserRepository.getLqnUser(userId);
  //     if (sender.length == 0) {
  //       throw new CustomError(
  //         enums.responseCode.SenderNotfound,
  //         enums.responseMessage.SenderNotfound
  //       );
  //     }

  //     var currency = await this._countryCurrencyRepository.getByCountryCode(
  //       userInfo.country_code
  //     );
  //     var address = await this._addressRepository.getInfoByRefId(
  //       enums.addressType.USER_PROFILE,
  //       userId
  //     );
  //     var document = await this._userDocumentRepository.getList(userId);
  //     const dob = moment(userInfo.date_of_birth).format("yyyy-MM-DD");
  //     await this._lqnWithdrawService.Createreceiver(
  //       userInfo.first_name,
  //       userInfo.middle_name,
  //       userInfo.last_name,
  //       address[0].line1 + " " + address.line2,
  //       address[0].state,
  //       "",
  //       address[0].city,
  //       address[0].postcode,
  //       dob,
  //       userInfo.country_code,
  //       userInfo.nationality_code,
  //       document[0].document_type,
  //       document[0].document_type_remark,
  //       document[0].document_text,
  //       userInfo.email,
  //       userInfo.tel,
  //       "B",
  //       bankName,
  //       "",
  //       "",
  //       accountNo,
  //       userInfo.occupation,
  //       userInfo.occupation_remark,
  //       "",
  //       "",
  //       bankId,
  //       currency[0].currency_code,
  //       sender[0].lqn_ref_id,
  //       logOnId,
  //       userId
  //     );

  //     await this._bankAccountRepository.createOwner(
  //       userId,
  //       enums.bankAccountType.OWNER,
  //       bankCode,
  //       bankName,
  //       accountNo,
  //       accountName,
  //       swiftCode,
  //       iban,
  //       logOnId
  //     );
  //     return enums.responseMessage.Success;
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // async createOtherBankAccount(
  //   userId: number,
  //   bankCode: string,
  //   bankName: string,
  //   accountNo: string,
  //   accountName: string,
  //   swiftCode: string,
  //   iban: string,
  //   formName: string,
  //   branchCode: string,
  //   firstName: string,
  //   lastName: string,
  //   nationality: string,
  //   dateOfBirth: string,
  //   mobileNumber: string,
  //   logOnId: string,
  //   address?: {
  //     line1?: string;
  //     line2?: string;
  //     city?: string;
  //     state?: string;
  //     countryCode?: string;
  //     postcode?: string;
  //   }
  // ) {
  //   try {
  //     const users = await this._userRepository.getActiveInfoById(userId);
  //     if (users.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.UserNotFound,
  //         enums.responseMessage.UserNotFound
  //       );
  //     }

  //     const isExists =
  //       await this._bankAccountRepository.isDuplicateByBankIdAccountNo(
  //         enums.mode.INSERT,
  //         userId,
  //         enums.bankAccountType.OTHER,
  //         bankCode,
  //         accountNo,
  //         undefined
  //       );
  //     if (isExists.length > 0) {
  //       throw new CustomError(
  //         enums.responseCode.Duplicate,
  //         enums.responseMessage.Duplicate
  //       );
  //     }

  //     var vDateOfBirth = undefined;
  //     if (dateOfBirth !== undefined) {
  //       if (dateOfBirth !== null) {
  //         if (dateOfBirth !== "") {
  //           vDateOfBirth = dateOfBirth;
  //         }
  //       }
  //     }

  //     var bankAccount = await this._bankAccountRepository.createOther(
  //       userId,
  //       enums.bankAccountType.OTHER,
  //       bankCode,
  //       bankName,
  //       accountNo,
  //       accountName,
  //       swiftCode,
  //       iban,
  //       logOnId,
  //       firstName,
  //       lastName,
  //       nationality,
  //       mobileNumber,
  //       branchCode,
  //       formName,
  //       vDateOfBirth
  //     );

  //     await this._addressRepository.create(
  //       enums.addressType.BANK_ACCOUNT,
  //       bankAccount[0].bank_account_id,
  //       address!.line1!,
  //       address!.line2!,
  //       address!.city!,
  //       address!.state!,
  //       address!.countryCode!,
  //       address!.postcode!,
  //       logOnId
  //     );

  //     return enums.responseMessage.Success;
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // async updateOwnerBankAccount(
  //   bankAccountId: number,
  //   userId: number,
  //   bankCode: string,
  //   bankName: string,
  //   accountNo: string,
  //   accountName: string,
  //   swiftCode: string,
  //   iban: string,
  //   logOnId: string
  // ) {
  //   try {
  //     const users = await this._userRepository.getActiveInfoById(userId);
  //     if (users.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.UserNotFound,
  //         enums.responseMessage.UserNotFound
  //       );
  //     }
  //     const userInfo = users[0];

  //     const isExists = await this._bankAccountRepository.getInfoById(
  //       bankAccountId
  //     );
  //     if (isExists.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     const isDuplicate =
  //       await this._bankAccountRepository.isDuplicateByBankIdAccountNo(
  //         enums.mode.UPDATE,
  //         userId,
  //         enums.bankAccountType.OWNER,
  //         bankCode,
  //         accountNo,
  //         bankAccountId
  //       );
  //     if (isDuplicate.length > 0) {
  //       throw new CustomError(
  //         enums.responseCode.Duplicate,
  //         enums.responseMessage.Duplicate
  //       );
  //     }

  //     const fullName = userInfo.first_name + " " + userInfo.last_name;
  //     if (accountName.toLowerCase() !== fullName.toLowerCase()) {
  //       throw new CustomError(
  //         enums.responseCode.AccountNameMismatch,
  //         enums.responseMessage.AccountNameMismatch
  //       );
  //     }

  //     await this._bankAccountRepository.updateOwner(
  //       bankAccountId,
  //       bankCode,
  //       bankName,
  //       accountNo,
  //       accountName,
  //       swiftCode,
  //       iban,
  //       logOnId
  //     );
  //     return enums.responseMessage.Success;
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // async updateOtherBankAccount(
  //   bankAccountId: number,
  //   userId: number,
  //   bankCode: string,
  //   bankName: string,
  //   accountNo: string,
  //   accountName: string,
  //   swiftCode: string,
  //   iban: string,
  //   branchCode: string,
  //   firstName: string,
  //   lastName: string,
  //   nationality: string,
  //   dateOfBirth: string,
  //   mobileNumber: string,
  //   logOnId: string,
  //   address?: {
  //     addressId?: number;
  //     line1?: string;
  //     line2?: string;
  //     city?: string;
  //     state?: string;
  //     countryCode?: string;
  //     postcode?: string;
  //   }
  // ) {
  //   try {
  //     const users = await this._userRepository.getActiveInfoById(userId);
  //     if (users.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.UserNotFound,
  //         enums.responseMessage.UserNotFound
  //       );
  //     }

  //     const isExists = await this._bankAccountRepository.getInfoById(
  //       bankAccountId
  //     );
  //     if (isExists.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     const isDuplicate =
  //       await this._bankAccountRepository.isDuplicateByBankIdAccountNo(
  //         enums.mode.UPDATE,
  //         userId,
  //         enums.bankAccountType.OTHER,
  //         bankCode,
  //         accountNo,
  //         bankAccountId
  //       );
  //     if (isDuplicate.length > 0) {
  //       throw new CustomError(
  //         enums.responseCode.Duplicate,
  //         enums.responseMessage.Duplicate
  //       );
  //     }

  //     var vDateOfBirth = undefined;
  //     if (dateOfBirth !== undefined) {
  //       if (dateOfBirth !== null) {
  //         if (dateOfBirth !== "") {
  //           vDateOfBirth = dateOfBirth;
  //         }
  //       }
  //     }

  //     await this._bankAccountRepository.updateOther(
  //       bankAccountId,
  //       bankCode,
  //       bankName,
  //       accountNo,
  //       accountName,
  //       swiftCode,
  //       iban,
  //       logOnId,
  //       firstName,
  //       lastName,
  //       nationality,
  //       mobileNumber,
  //       branchCode,
  //       vDateOfBirth
  //     );

  //     await this._addressRepository.update(
  //       address!.addressId!,
  //       address!.line1!,
  //       address!.line2!,
  //       address!.city!,
  //       address!.state!,
  //       address!.countryCode!,
  //       address!.postcode!,
  //       logOnId
  //     );

  //     return enums.responseMessage.Success;
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async deleteBankAccount(bankAccountId: number, logOnId: string) {
    try {
      const isExists = await this._bankAccountRepository.getInfoById(
        bankAccountId
      );
      if (isExists.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      await this._bankAccountRepository.delete(bankAccountId, logOnId);
      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  // async updateDefaultBankAccount(
  //   bankAccountId: number,
  //   userId: number,
  //   logOnId: string
  // ) {
  //   try {
  //     const isExists = await this._bankAccountRepository.getInfoById(
  //       bankAccountId
  //     );
  //     if (isExists.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }
  //     await this._bankAccountRepository.disableAllBankAccount(userId, logOnId);
  //     await this._bankAccountRepository.setDefaultBankAccount(
  //       bankAccountId,
  //       logOnId
  //     );
  //     return enums.responseMessage.Success;
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async getWithdraws(userId: number) {
    try {
      await this._fiatAccountRepository.updateBalanceAllUsers();
      return await this._transactionService.getWithdrawTransactionsByUserId(
        userId
      );
    } catch (error) {
      throw error;
    }
  }

  async getWithdrawById(withdrawId: number) {
    try {
      return await this._transactionService.getWithdrawById(withdrawId);
    } catch (error) {
      throw error;
    }
  }

  // async createWithdraw(
  //   userId: number,
  //   bankAccountId: number,
  //   amount: number,
  //   logOnId: string
  // ) {
  //   try {
  //     await this._transactionService.updateBalance(
  //       enums.accountType.USER,
  //       undefined,
  //       userId
  //     );
  //     const userInfo = await this._userRepository.getActiveInfoById(userId);
  //     if (userInfo.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.UserNotFound,
  //         enums.responseMessage.UserNotFound
  //       );
  //     }
  //     const bankAccountInfo = await this._bankAccountRepository.getInfoById(
  //       bankAccountId
  //     );
  //     if (bankAccountInfo.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.BankAccountNotFound,
  //         enums.responseMessage.BankAccountNotFound
  //       );
  //     }
  //     var fiatAccounts = await this._fiatAccountRepository.getList(
  //       enums.accountType.USER,
  //       undefined,
  //       userId
  //     );
  //     if (fiatAccounts.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.FiatAccountNotFound,
  //         enums.responseMessage.FiatAccountNotFound
  //       );
  //     }
  //     const fiatAccountInfo = fiatAccounts[0];
  //     if (
  //       parseFloat(fiatAccountInfo.total_income) -
  //         parseFloat(fiatAccountInfo.total_expense) -
  //         parseFloat(fiatAccountInfo.total_pending) -
  //         amount <
  //       0
  //     ) {
  //       throw new CustomError(
  //         enums.responseCode.InsufficientFunds,
  //         enums.responseMessage.InsufficientFunds
  //       );
  //     }
  //     await this._withdrawRepository.create(
  //       userId,
  //       await utils.generateKey(userId.toString()),
  //       enums.transactionType.WITHDRAW,
  //       bankAccountId,
  //       amount,
  //       logOnId,
  //       undefined,
  //       undefined,
  //       undefined,
  //       undefined
  //     );
  //     await this._transactionService.updateBalance(
  //       enums.accountType.USER,
  //       undefined,
  //       userId
  //     );
  //     return enums.responseMessage.Success;
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async createWithdraw(
    senderSourceOfFund: string,
    senderSourceOfFundRemarks: string,
    purposeOfRemittance: string,
    purposeOfRemittanceRemarks: string,
    senderBeneficiaryRelationship: string,
    senderBeneficiaryRelationshipRemarks: string,
    transferAmount: number, //USD
    bankAccountId: number,
    userId: number,
    logOnId: string
  ) {
    try {
      const users = await this._userRepository.getActiveInfoById(userId);
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }

      await this._fiatAccountRepository.updateBalanceAllUsers();

      var fiatAccounts = await this._fiatAccountRepository.getInfoByUserId(
        userId
      );
      if (fiatAccounts.length <= 0) {
        throw new CustomError(
          enums.responseCode.FiatAccountNotFound,
          enums.responseMessage.FiatAccountNotFound
        );
      }
      const fiatAccountInfo = fiatAccounts[0];

      var bankAccounts = await this._bankAccountRepository.getInfoById(
        bankAccountId
      );
      if (bankAccounts.length <= 0) {
        throw new CustomError(
          enums.responseCode.ReceiverNotfound,
          enums.responseMessage.ReceiverNotfound
        );
      }
      const bankAccountsInfo = bankAccounts[0];

      var exchangeRate = await this._lqnService.getExchangeRate(
        transferAmount,
        "C",
        bankAccountsInfo.currency_code,
        "B",
        bankAccountsInfo.location_id,
        bankAccountsInfo.receiver_country_code
      );

      var collectAmount = exchangeRate.collect_amount; // transferAmount + service_charge
      var payoutAmount = exchangeRate.payout_amount; //
      var serviceCharge = exchangeRate.service_charge;

      if (parseFloat(fiatAccountInfo.total) - collectAmount < 0) {
        throw new CustomError(
          enums.responseCode.InsufficientFunds,
          enums.responseMessage.InsufficientFunds
        );
      }

      var receivers = await this._lqnUserRepository.getLqnUserByBankAccountId(
        bankAccountId
      );
      if (receivers.length <= 0) {
        throw new CustomError(
          enums.responseCode.ReceiverNotfound,
          enums.responseMessage.ReceiverNotfound
        );
      }
      const receiverInfo = receivers[0];

      const sendTransaction = await this._lqnService.sendTransaction(
        senderSourceOfFund,
        senderSourceOfFundRemarks,
        purposeOfRemittance,
        purposeOfRemittanceRemarks,
        senderBeneficiaryRelationship,
        senderBeneficiaryRelationshipRemarks,
        "P", // calcBy option ‘C’ removed in sendTransaction API. Use only calcBy as ‘P’
        payoutAmount,
        "USD", // Currency (FIXED!?)
        receiverInfo.sender_ref_id,
        receiverInfo.lqn_ref_id
      );
      if (sendTransaction.code !== "0") {
        throw new CustomError(sendTransaction.code, sendTransaction.message);
      }

      const lqnTransactionCreated = await this._lqnTransactionRepository.create(
        enums.transactionType.WITHDRAW,
        receiverInfo.sender_ref_id,
        receiverInfo.lqn_ref_id,
        sendTransaction.agentSessionId,
        sendTransaction.confirmationId,
        sendTransaction.agentTxnId,
        sendTransaction.collectAmount,
        sendTransaction.collectCurrency,
        sendTransaction.serviceCharge,
        sendTransaction.gstCharge,
        sendTransaction.transferAmount,
        sendTransaction.exchangeRate,
        sendTransaction.payoutAmount,
        sendTransaction.payoutCurrency,
        sendTransaction.feeDiscount,
        sendTransaction.additionalPremiumRate,
        sendTransaction.settlementRate,
        sendTransaction.sendCommission,
        sendTransaction.settlementAmount,
        "",
        "",
        sendTransaction.message,
        ""
      );

      const commitTransaction = await this._lqnService.commitTransaction(
        sendTransaction.confirmationId
      );
      if (commitTransaction.code !== "0") {
        throw new CustomError(
          commitTransaction.code,
          commitTransaction.message
        );
      }

      await this._lqnTransactionRepository.updateConfirmTransaction(
        sendTransaction.confirmationId,
        commitTransaction.pinNumber,
        commitTransaction.status,
        ""
      );

      var status = await this.setLqnStatus(commitTransaction.status);

      await this._withdrawRepository.create(
        await utils.generateKey(userId.toString()),
        userId,
        enums.transactionType.WITHDRAW,
        bankAccountId,
        transferAmount,
        serviceCharge,
        status,
        logOnId,
        undefined,
        lqnTransactionCreated[0].lqn_transaction_id
      );

      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  async setLqnStatus(status: string) {
    status = status.toUpperCase();

    if (
      status == enums.lqnTransactionStatus.UN_COMMIT_HOLD ||
      status == enums.lqnTransactionStatus.UN_COMMIT_COMPLIANCE ||
      status == enums.lqnTransactionStatus.HOLD ||
      status == enums.lqnTransactionStatus.COMPLIANCE ||
      status == enums.lqnTransactionStatus.SANCTION
    ) {
      return enums.transactionStatus.ONHOLD;
    } else if (
      status == enums.lqnTransactionStatus.UN_PAID ||
      status == enums.lqnTransactionStatus.POST ||
      status == enums.lqnTransactionStatus.API_PROCESSING
    ) {
      return enums.transactionStatus.PENDING;
    } else if (status == enums.lqnTransactionStatus.PAID) {
      return enums.transactionStatus.SUCCESS;
    } else if (
      status == enums.lqnTransactionStatus.CANCEL ||
      status == enums.lqnTransactionStatus.CANCEL_HOLD ||
      status == enums.lqnTransactionStatus.BLOCK
    ) {
      return enums.transactionStatus.CANCELLED;
    } else {
      return status;
    }
  }

  async createTransfer(
    senderSourceOfFund: string,
    senderSourceOfFundRemarks: string,
    purposeOfRemittance: string,
    purposeOfRemittanceRemarks: string,
    senderBeneficiaryRelationship: string,
    senderBeneficiaryRelationshipRemarks: string,
    transferAmount: number,
    bankAccountId: number,
    userId: number,
    logOnId: string
  ) {
    try {
      const users = await this._userRepository.getActiveInfoById(userId);
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }

      await this._fiatAccountRepository.updateBalanceAllUsers();

      var fiatAccounts = await this._fiatAccountRepository.getInfoByUserId(
        userId
      );
      if (fiatAccounts.length <= 0) {
        throw new CustomError(
          enums.responseCode.FiatAccountNotFound,
          enums.responseMessage.FiatAccountNotFound
        );
      }
      const fiatAccountInfo = fiatAccounts[0];

      var bankAccounts = await this._bankAccountRepository.getInfoById(
        bankAccountId
      );
      if (bankAccounts.length <= 0) {
        throw new CustomError(
          enums.responseCode.ReceiverNotfound,
          enums.responseMessage.ReceiverNotfound
        );
      }
      const bankAccountsInfo = bankAccounts[0];

      var exchangeRate = await this._lqnService.getExchangeRate(
        transferAmount,
        "C",
        bankAccountsInfo.currency_code,
        "B",
        bankAccountsInfo.location_id,
        bankAccountsInfo.receiver_country_code
      );

      var collectAmount = exchangeRate.collect_amount; // transferAmount + service_charge
      var payoutAmount = exchangeRate.payout_amount; //
      var serviceCharge = exchangeRate.service_charge;

      if (parseFloat(fiatAccountInfo.total) - collectAmount < 0) {
        throw new CustomError(
          enums.responseCode.InsufficientFunds,
          enums.responseMessage.InsufficientFunds
        );
      }

      var receivers = await this._lqnUserRepository.getLqnUserByBankAccountId(
        bankAccountId
      );
      if (receivers.length <= 0) {
        throw new CustomError(
          enums.responseCode.ReceiverNotfound,
          enums.responseMessage.ReceiverNotfound
        );
      }
      const receiverInfo = receivers[0];

      const sendTransaction = await this._lqnService.sendTransaction(
        senderSourceOfFund,
        senderSourceOfFundRemarks,
        purposeOfRemittance,
        purposeOfRemittanceRemarks,
        senderBeneficiaryRelationship,
        senderBeneficiaryRelationshipRemarks,
        "P", // calcBy option ‘C’ removed in sendTransaction API. Use only calcBy as ‘P’
        payoutAmount,
        "USD", // Currency (FIXED!?)
        receiverInfo.sender_ref_id,
        receiverInfo.lqn_ref_id
      );
      if (sendTransaction.code !== "0") {
        throw new CustomError(sendTransaction.code, sendTransaction.message);
      }

      const lqnTransactionCreated = await this._lqnTransactionRepository.create(
        enums.transactionType.TRANSFER,
        receiverInfo.sender_ref_id,
        receiverInfo.lqn_ref_id,
        sendTransaction.agentSessionId,
        sendTransaction.confirmationId,
        sendTransaction.agentTxnId,
        sendTransaction.collectAmount,
        sendTransaction.collectCurrency,
        sendTransaction.serviceCharge,
        sendTransaction.gstCharge,
        sendTransaction.transferAmount,
        sendTransaction.exchangeRate,
        sendTransaction.payoutAmount,
        sendTransaction.payoutCurrency,
        sendTransaction.feeDiscount,
        sendTransaction.additionalPremiumRate,
        sendTransaction.settlementRate,
        sendTransaction.sendCommission,
        sendTransaction.settlementAmount,
        "",
        "",
        sendTransaction.message,
        ""
      );

      const commitTransaction = await this._lqnService.commitTransaction(
        sendTransaction.confirmationId
      );
      if (commitTransaction.code !== "0") {
        throw new CustomError(
          commitTransaction.code,
          commitTransaction.message
        );
      }

      await this._lqnTransactionRepository.updateConfirmTransaction(
        sendTransaction.confirmationId,
        commitTransaction.pinNumber,
        commitTransaction.status,
        ""
      );

      var status = await this.setLqnStatus(commitTransaction.status);

      await this._withdrawRepository.create(
        await utils.generateKey(userId.toString()),
        userId,
        enums.transactionType.TRANSFER,
        bankAccountId,
        transferAmount,
        serviceCharge,
        status,
        logOnId,
        undefined,
        lqnTransactionCreated[0].lqn_transaction_id
      );

      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  async getCryptoWalletList(userId: number) {
    try {
      await this._assetInVaultAccountService.syncFireblocksAssetInVaults(
        enums.accountType.USER,
        undefined,
        userId
      );
      const vaultAccountInfo = await this._vaultAccountService.getVaultAccount(
        enums.accountType.USER,
        undefined,
        userId
      );
      return await this._assetInVaultAccountService.getCryptoWalletList(
        vaultAccountInfo.vault_account_id
      );
    } catch (error) {
      throw error;
    }
  }

  async getAssetInVaultAccountById(assetInVaultAccountId: number) {
    try {
      return await this._assetInVaultAccountService.getAssetInVaultAccountById(
        assetInVaultAccountId
      );
    } catch (error) {
      throw error;
    }
  }

  async createAssetInVaultAccount(
    userId: number,
    assetCode: string,
    logOnId: string
  ) {
    try {
      return await this._assetInVaultAccountService.createAssetInVaultAccount(
        enums.accountType.USER,
        assetCode,
        logOnId,
        undefined,
        userId
      );
    } catch (error) {
      throw error;
    }
  }

  async getTransactionById(transactionId: number) {
    try {
      return await this._transactionService.getTransactionById(transactionId);
    } catch (error) {
      throw error;
    }
  }

  async createTransaction(
    userId: number,
    type: string,
    assetCode: string,
    totalAssets: number,
    totalAmount: number,
    inputType: string,
    logOnId: string
  ) {
    try {
      return await this._transactionService.createTransaction(
        userId,
        type,
        assetCode,
        totalAssets,
        totalAmount,
        inputType,
        false,
        "",
        "",
        logOnId
      );
    } catch (error) {
      throw error;
    }
  }

  async getTransactions(userId: number) {
    try {
      return await this._transactionService.getUserTransactions(userId);
    } catch (error) {
      throw error;
    }
  }

  async getTotalAssetForUser(userId: number, assetGroup: string) {
    try {
      var balance = 0;

      const totalAssetUser =
        await this._assetInVaultAccountRepository.getSummaryAssetForUserByAssetGroup(
          userId,
          assetGroup
        );
      if (totalAssetUser.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }

      if (totalAssetUser[0].total != null) {
        balance = totalAssetUser[0].total;
      }

      return {
        asset_grou: assetGroup,
        balance: balance,
      };
    } catch (error) {
      throw error;
    }
  }

  async getUserProfile(userId: number) {
    try {
      const users = await this._userRepository.getInfoById(userId);
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }
      var userAddress = await this._addressRepository.getInfoByRefId(
        enums.addressType.USER_PROFILE,
        users[0].user_id
      );
      var userDocuments = await this._userDocumentRepository.getList(
        users[0].user_id
      );
      var result = await this.setUserInfo(users[0], userAddress, userDocuments);
      return result;
    } catch (error) {
      throw error;
    }
  }

  // async updateUserProfile(req: RequestUpdateCustomerProfile, userId: number) {
  //   try {
  //     var users = await this._userRepository.getInfoById(userId);
  //     if (users.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     users = await this._userRepository.update(
  //       userId,
  //       req.first_name ? req.first_name : users[0].first_name,
  //       req.middle_name ? req.middle_name : users[0].middle_name,
  //       req.last_name ? req.last_name : users[0].last_name,
  //       req.tel ? req.tel : users[0].tel,
  //       users[0].status,
  //       userId.toString(),
  //       req.gender ? req.gender : users[0].gender,
  //       req.nationality ? req.nationality : users[0].nationality,
  //       req.occupation ? req.occupation : users[0].occupation,
  //       req.occupation_remark
  //         ? req.occupation_remark
  //         : users[0].occupation_remark,
  //       req.date_of_birth ? req.date_of_birth : users[0].date_of_birth,
  //       req.nationality ? req.nationality : users[0].country_code,
  //       users[0].email
  //     );

  //     if (req.document_type !== undefined) {
  //       if (req.document_type !== "") {
  //         const isExists = await this._userDocumentRepository.getList(userId);
  //         if (isExists.length <= 0) {
  //           throw new CustomError(
  //             enums.responseCode.NotFound,
  //             enums.responseMessage.NotFound
  //           );
  //         }

  //         return await this._userDocumentRepository.updateByDocType(
  //           isExists[0].user_document_id,
  //           req.document_type,
  //           req.document_text,
  //           userId.toString(),
  //           req.document_type_remark!,
  //           req.document_issue_date!,
  //           req.document_expired_date!
  //         );
  //       }
  //     }
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // async updateUserAddress(req: RequestUpdateCustomerAddress, userId: number) {
  //   try {
  //     const userAddress = await this._addressRepository.getInfoByRefId(
  //       enums.addressType.USER_PROFILE,
  //       userId
  //     );
  //     return await this._addressRepository.update(
  //       userAddress[0].address_id,
  //       req.line1 ? req.line1 : userAddress[0].line1,
  //       req.line2 ? req.line2 : userAddress[0].line2,
  //       req.city ? req.city : userAddress[0].city,
  //       req.state ? req.state : userAddress[0].state,
  //       req.country_code ? req.country_code : userAddress[0].country_code,
  //       req.postcode ? req.postcode : userAddress[0].postcode,
  //       userId.toString()
  //     );
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async setUserInfo(userInfo: any, userAddress: any, userDocuments: any) {
    var vAddress = {};
    if (userAddress.length > 0) {
      vAddress = {
        address_id: userAddress[0].address_id,
        line1: userAddress[0].line1,
        line2: userAddress[0].line2,
        city: userAddress[0].city,
        state: userAddress[0].state,
        country_code: userAddress[0].country_code,
        country_name: userAddress[0].country_name,
        postcode: userAddress[0].postcode,
        area_town: userAddress[0].area_town,
      };
    }
    // var vUserDocuments = [];
    var vDocumentType = "";
    var vDocumentTypeName = "";
    var vDocumentText = "";
    var vDocumentTypeRemark = "";
    var vDocumentIssueDate = "";
    var vDocumentExpiredDate = "";
    if (userDocuments.length > 0) {
      // for (let index = 0; index < userDocuments.length; index++) {
      //   const element = userDocuments[index];
      //   vUserDocuments.push({
      //     document_type: element.document_type,
      //     document_text: element.document_text,
      //   });
      // }
      const lqnDocuments = await this._lqnService.getCatalogue(
        "DOC",
        userInfo.country_code,
        "",
        ""
      );
      for (let index = 0; index < lqnDocuments.length; index++) {
        const element = lqnDocuments[index];
        if (element.value === userDocuments[0].document_type) {
          vDocumentTypeName = element.text;
          break;
        }
      }
      vDocumentType = userDocuments[0].document_type;
      vDocumentText = userDocuments[0].document_text;
      vDocumentTypeRemark = userDocuments[0].document_type_remark;
      vDocumentIssueDate = userDocuments[0].document_issue_date;
      vDocumentExpiredDate = userDocuments[0].document_expired_date;
    }
    return {
      user_id: userInfo.user_id,
      first_name: userInfo.first_name,
      middle_name: userInfo.middle_name,
      last_name: userInfo.last_name,
      date_of_birth: userInfo.date_of_birth,
      email: userInfo.email,
      tel: userInfo.tel,
      country_code: userInfo.country_code,
      country_name: userInfo.country_name,
      status: userInfo.status,
      gender: userInfo.gender,
      nationality: userInfo.nationality_code,
      occupation: userInfo.occupation,
      occupation_remark: userInfo.occupation_remark,
      status_boolean: userInfo.status === "A" ? true : false,
      address: vAddress,
      // documents: vUserDocuments,
      document_type: vDocumentType,
      document_type_name: vDocumentTypeName,
      document_text: vDocumentText,
      document_type_remark: vDocumentTypeRemark,
      document_issue_date: vDocumentIssueDate,
      document_expired_date: vDocumentExpiredDate,
    };
  }

  async requestAccessAsset(userId: number, assetCode: string) {
    try {
      const users = await this._userRepository.getInfoById(userId);
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }
      const userInfo = users[0];

      const assets = await this._assetRepository.getInfoByCode(assetCode);
      if (assets.length <= 0) {
        throw new CustomError(
          enums.responseCode.AssetNotFound,
          enums.responseMessage.AssetNotFound
        );
      }
      const assetInfo = assets[0];

      return await utils.sendMail(
        process.env.MAIL_FROM_ADD_ASSET!,
        process.env.MAIL_TO_ADD_ASSET!,
        `Request access ${assetInfo.asset_name} from ${userInfo.email}`,
        `
          To Customer support
          <br/><br/>
          <b>Customer</b><br/>
          Email: <b>${userInfo.email}</b><br/>
          First name: <b>${userInfo.first_name}</b><br/>
          Last name: <b>${userInfo.last_name}</b>
          <br/><br/>
          <b>Request to access</b><br/>
          Network Code: <b>${assetInfo.network_code}</b><br/>
          Network Name: <b>${assetInfo.network_name}</b><br/>
          Asset Code: <b>${assetInfo.asset_code}</b><br/>
          Asset Name: <b>${assetInfo.asset_name}</b><br/>
        `
      );
    } catch (error) {
      throw error;
    }
  }

  async getUserProfileByEmail(email: string) {
    try {
      const users = await this._userRepository.getInfoByEmail(email);
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }
      return users[0];
    } catch (error) {
      throw error;
    }
  }

  async getNetworksByUserId(userId: number) {
    try {
      const networks = await this._networkRepository.getListByUserId(userId);
      var vResult = [];
      for (let i = 0; i < networks.length; i++) {
        vResult.push({
          network_id: networks[i].network_id,
          code: networks[i].code,
          name: networks[i].name,
          image: networks[i].image,
        });
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getAssetsByUserIdNetworkId(userId: number, networkId: number) {
    try {
      const assetsInVaultAccount =
        await this._assetInVaultAccountRepository.getListByUserIdNetworkId(
          userId,
          networkId
        );
      var vResult = [];
      for (let i = 0; i < assetsInVaultAccount.length; i++) {
        vResult.push({
          asset_in_vault_account_id:
            assetsInVaultAccount[i].asset_in_vault_account_id,
          network_id: assetsInVaultAccount[i].network_id,
          network_code: assetsInVaultAccount[i].network_code,
          network_name: assetsInVaultAccount[i].network_name,
          network_image: assetsInVaultAccount[i].network_image,
          asset_id: assetsInVaultAccount[i].asset_id,
          asset_code: assetsInVaultAccount[i].asset_code,
          asset_name: assetsInVaultAccount[i].asset_name,
          asset_image: assetsInVaultAccount[i].asset_image,
          asset_address: assetsInVaultAccount[i].asset_address,
          total: assetsInVaultAccount[i].total,
          lockedamount: assetsInVaultAccount[i].lockedamount,
          available: assetsInVaultAccount[i].available,
          pending: assetsInVaultAccount[i].pending,
          total_selling_amount: assetsInVaultAccount[i].total_selling_amount,
          buying: assetsInVaultAccount[i].buying,
          selling: assetsInVaultAccount[i].selling,
          status: assetsInVaultAccount[i].status,
          updated_by: assetsInVaultAccount[i].updated_by,
          updated_date: assetsInVaultAccount[i].updated_date,
        });
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }
}
