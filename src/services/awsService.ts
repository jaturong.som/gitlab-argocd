import {
  S3Client,
  PutObjectCommand,
  GetObjectCommand,
} from "@aws-sdk/client-s3";
import { getSignedUrl } from "@aws-sdk/s3-request-presigner";
import mime from "mime-types";
import { VaultService } from "./vaultService";
import { singleton } from "tsyringe";

@singleton()
export class AWSService {
  readonly _vaultService: VaultService;
  constructor(vaultService: VaultService) {
    this._vaultService = vaultService;
  }

  async upload(bucketName: string, fileName: string, fileContent: string) {
    try {
      const awsKey = await this._vaultService.getAWS();
      const s3Client = new S3Client({
        region: process.env.S3_REGION!,
        credentials: {
          accessKeyId: awsKey.data.awsS3AccessKeyId,
          secretAccessKey: awsKey.data.awsS3SecretAccessKey,
        },
      });
      await s3Client.send(
        new PutObjectCommand({
          Bucket: bucketName,
          Key: fileName,
          Body: fileContent,
        })
      );
      const encodeFileName = encodeURIComponent(fileName);
      return `https://${bucketName}.s3.amazonaws.com/${encodeFileName}`;
    } catch (error) {
      throw error;
    }
  }

  async download(bucketName: string, fileName: string) {
    try {
      const awsKey = await this._vaultService.getAWS();
      const s3Client = new S3Client({
        region: process.env.S3_REGION!,
        credentials: {
          accessKeyId: awsKey.data.awsS3AccessKeyId,
          secretAccessKey: awsKey.data.awsS3SecretAccessKey,
        },
      });

      return await getSignedUrl(
        s3Client,
        new GetObjectCommand({
          Bucket: bucketName,
          Key: fileName,
        }),
        { expiresIn: 60 }
      );
    } catch (error) {
      throw error;
    }
  }

  async preview(bucketName: string, fileName: string) {
    try {
      const awsKey = await this._vaultService.getAWS();
      const s3Client = new S3Client({
        region: process.env.S3_REGION!,
        credentials: {
          accessKeyId: awsKey.data.awsS3AccessKeyId,
          secretAccessKey: awsKey.data.awsS3SecretAccessKey,
        },
      });

      return await getSignedUrl(
        s3Client,
        new GetObjectCommand({
          Bucket: bucketName,
          Key: fileName,
          ResponseContentDisposition: "inline",
          ResponseContentType: mime.lookup(fileName).toString(),
        }),
        { expiresIn: 60 }
      );
    } catch (error) {
      throw error;
    }
  }
}
