import axios from "axios";
import {
  FireblocksSDK,
  PeerType,
  TransactionArguments,
  PagedVaultAccountsRequestFilters,
} from "fireblocks-sdk";
import { singleton } from "tsyringe";
import CustomError from "../middlewares/customError";
import { VaultService } from "./vaultService";

@singleton()
export class FireblocksService {
  readonly _vaultService: VaultService;
  constructor(vaultService: VaultService) {
    this._vaultService = vaultService;
  }

  async getVaultAccountsWithPageInfo(
    pagedVaultAccountsRequestFilters: PagedVaultAccountsRequestFilters
  ) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKey,
        fbKey.data.apiKey
      );
      return await fireblocks.getVaultAccountsWithPageInfo(
        pagedVaultAccountsRequestFilters
      );
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }

  async getVaultAccountById(vaultAccountId: string) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKey,
        fbKey.data.apiKey
      );
      return await fireblocks.getVaultAccountById(vaultAccountId);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }

  async getVaultAccountAsset(vaultAccountId: string, assetId: string) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKey,
        fbKey.data.apiKey
      );
      return await fireblocks.getVaultAccountAsset(vaultAccountId, assetId);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }

  async getDepositAddresses(vaultAccountId: string, assetId: string) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKey,
        fbKey.data.apiKey
      );
      return await fireblocks.getDepositAddresses(vaultAccountId, assetId);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }

  async getTransactionById(txId: string) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKey,
        fbKey.data.apiKey
      );
      return await fireblocks.getTransactionById(txId);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }

  async createVaultAccount(
    name: string,
    hiddenOnUI?: boolean,
    customerRefId?: string,
    autoFuel?: boolean
  ) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKey,
        fbKey.data.apiKey
      );
      return await fireblocks.createVaultAccount(
        name,
        hiddenOnUI,
        customerRefId,
        autoFuel
      );
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }

  async createVaultAsset(vaultAccountId: string, assetId: string) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKey,
        fbKey.data.apiKey
      );
      return await fireblocks.createVaultAsset(vaultAccountId, assetId);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }

  // async getVaultBalanceByAsset(assetId: string) {
  //   try {
  //     const fbKey = await this._vaultService.getFireblocksKeyInVault();
  //     const fireblocks = new FireblocksSDK(
  //       fbKey.data.privateKey,
  //       fbKey.data.apiKey
  //     );
  //     return await fireblocks.getVaultBalanceByAsset(assetId);
  //   } catch (error) {
  //     if (axios.isAxiosError(error)) {
  //       let response = error.response;
  //       if (response !== undefined) {
  //         let respData = response.data;
  //         throw new CustomError(respData.code, respData.message);
  //       }
  //     }
  //     throw error;
  //   }
  // }

  async estimateFeeForTransaction(
    assetId: string,
    sourceId: string,
    destinationId: string,
    amount: number
  ) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKey,
        fbKey.data.apiKey
      );
      const payload: TransactionArguments = {
        assetId: assetId,
        source: {
          type: PeerType.VAULT_ACCOUNT,
          id: sourceId || "0",
        },
        destination: {
          type: PeerType.VAULT_ACCOUNT,
          id: destinationId,
        },
        amount: String(amount),
      };
      return await fireblocks.estimateFeeForTransaction(payload);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }

  async estimateFeeForTransactionNetworkLink(
    assetId: string,
    sourceId: string,
    address: string,
    amount: number
  ) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKey,
        fbKey.data.apiKey
      );
      const payload: TransactionArguments = {
        assetId: assetId,
        source: {
          type: PeerType.VAULT_ACCOUNT,
          id: sourceId || "0",
        },
        destination: {
          type: PeerType.ONE_TIME_ADDRESS,
          // id: destinationId,
          oneTimeAddress: {
            address: address,
            tag: "",
          },
        },
        amount: String(amount),
      };
      return await fireblocks.estimateFeeForTransaction(payload);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }

  async createTransaction(
    assetId: string,
    sourceId: string,
    destinationId: string,
    amount: number,
    fee: number
  ) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKey,
        fbKey.data.apiKey
      );
      const payload: TransactionArguments = {
        assetId: assetId,
        source: {
          type: PeerType.VAULT_ACCOUNT,
          id: sourceId || "0",
        },
        destination: {
          type: PeerType.VAULT_ACCOUNT,
          id: destinationId,
        },
        amount: String(amount),
        fee: String(fee),
      };
      return await fireblocks.createTransaction(payload);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }

  async createTransactionNetworkLink(
    assetId: string,
    sourceId: string,
    address: string,
    amount: number,
    fee: number
  ) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKey,
        fbKey.data.apiKey
      );
      const payload: TransactionArguments = {
        assetId: assetId,
        source: {
          type: PeerType.VAULT_ACCOUNT,
          id: sourceId || "0",
        },
        destination: {
          type: PeerType.ONE_TIME_ADDRESS,
          oneTimeAddress: {
            address: address,
            tag: "",
          },
        },
        amount: String(amount),
        fee: String(fee),
        note: "Created by fireblocks SDK",
      };
      return await fireblocks.createTransaction(payload);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }

  async freezeTransactionById(txId: string) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKeyNonSigningAdmin,
        fbKey.data.apiKeyNonSigningAdmin
      );
      return await fireblocks.freezeTransactionById(txId);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }

  async unfreezeTransactionById(txId: string) {
    try {
      const fbKey = await this._vaultService.getFireblocksKeyInVault();
      const fireblocks = new FireblocksSDK(
        fbKey.data.privateKeyNonSigningAdmin,
        fbKey.data.apiKeyNonSigningAdmin
      );
      return await fireblocks.unfreezeTransactionById(txId);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        let response = error.response;
        if (response !== undefined) {
          let respData = response.data;
          throw new CustomError(respData.code, respData.message);
        }
      }
      throw error;
    }
  }
}
