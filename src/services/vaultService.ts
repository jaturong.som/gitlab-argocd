import NodeVault from "node-vault";
import { singleton } from "tsyringe";

@singleton()
export class VaultService {
  readonly _vault: NodeVault.client;
  constructor() {
    // get new instance of the client
    this._vault = NodeVault({
      apiVersion: process.env.VAULT_API_VERSION!,
      endpoint: process.env.VAULT_ENDPOINT!,
      token: process.env.VAULT_TOKEN!,
    });
  }

  async getFireblocksKeyInVault() {
    try {
      return await this._vault.read(process.env.VAULT_PATH! + "/fireblocks");
    } catch (error) {
      throw error;
    }
  }

  async getChainalysisToken() {
    try {
      return await this._vault.read(process.env.VAULT_PATH! + "/chainalysis");
    } catch (error) {
      throw error;
    }
  }

  async getSMTPServer() {
    try {
      return await this._vault.read(process.env.VAULT_PATH! + "/smtpServer");
    } catch (error) {
      throw error;
    }
  }

  async getLQN() {
    try {
      return await this._vault.read(process.env.VAULT_PATH! + "/lqn");
    } catch (error) {
      throw error;
    }
  }

  async getAWS() {
    try {
      return await this._vault.read(process.env.VAULT_PATH! + "/aws");
    } catch (error) {
      throw error;
    }
  }

  async getCactusCustody() {
    try {
      return await this._vault.read(process.env.VAULT_PATH! + "/cactus");
    } catch (error) {
      throw error;
    }
  }
}
