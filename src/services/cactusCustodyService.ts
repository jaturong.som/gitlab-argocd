// import CryptoJS from "crypto-js";
import * as crypto from "crypto";
import { VaultService } from "./vaultService";
import { HttpService } from "./httpService";
import { singleton } from "tsyringe";

@singleton()
export class CactusCustodyService {
  readonly _vaultService: VaultService;
  readonly _httpService: HttpService;
  constructor(vaultService: VaultService, httpService: HttpService) {
    this._vaultService = vaultService;
    this._httpService = httpService;
  }

  async getWalletList(walletCode?: string) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlWalletList = "/custody/v1/api/wallets";

      var vQueryParam = [];
      if (walletCode !== undefined) {
        vQueryParam.push({
          main_wallet_code: walletCode,
        });
      }

      var queryParam = vQueryParam;

      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlWalletList,
        queryParam,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getWalletInfo(businessLineId: string, walletCode: string) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlWalletInfo =
        "/custody/v1/api/projects/" + businessLineId + "/wallets/" + walletCode;
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlWalletInfo,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getCoinInfo() {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlCoinInfo = "/custody/v1/api/coin-infos";
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlCoinInfo,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getChainInfo() {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlChainInfo = "/custody/v1/api/chain-infos";
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlChainInfo,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async createWallet(
    businessLineId: string,
    walletType: string,
    number: number
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlCreateWallet =
        "/custody/v1/api/projects/" + businessLineId + "/wallets/create";
      const body = JSON.stringify({
        wallet_type: walletType,
        number: number,
      });
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "POST",
        urlCreateWallet,
        undefined,
        body
      );
    } catch (error) {
      throw error;
    }
  }

  async applyNewAddress(
    businessLineId: string,
    walletCode: string,
    addressNum: number,
    addressType: string,
    coinName?: string
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlApplyNewAddress =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/wallets/" +
        walletCode +
        "/addresses/apply";
      const body = JSON.stringify({
        address_num: addressNum,
        address_type: addressType,
        coin_name: coinName,
      });

      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "POST",
        urlApplyNewAddress,
        undefined,
        body
      );
    } catch (error) {
      throw error;
    }
  }

  async getAddressInfo(
    businessLineId: string,
    walletCode: string,
    address: string
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlAddressInfo =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/wallets/" +
        walletCode +
        "/addresses/" +
        address;
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlAddressInfo,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getAddressList(businessLineId: string, walletCode: string) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlAddressList =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/wallets/" +
        walletCode +
        "/addresses";
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlAddressList,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async editAddress(
    businessLineId: string,
    walletCode: string,
    address: string,
    description: string
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlEditAddress =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/wallets/" +
        walletCode +
        "/addresses/" +
        address;
      const body = JSON.stringify({
        description: description,
      });

      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "PUT",
        urlEditAddress,
        undefined,
        body
      );
    } catch (error) {
      throw error;
    }
  }

  async verifyAddressFormat(addresses: string[], coinName: string) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlVerifyAddressFormat = "/custody/v1/api/addresses/type/check";
      const body = JSON.stringify({
        addresses: addresses,
        coin_name: coinName,
      });
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "POST",
        urlVerifyAddressFormat,
        undefined,
        body
      );
    } catch (error) {
      throw error;
    }
  }

  async estimateGasFee(
    businessLineId: string,
    fromAddress: string,
    fromWalletCode: string,
    description: string,
    amount: number,
    destAddress: string,
    memoType: string,
    memo: string,
    remark: string
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlEstimateGasFee =
        "/custody/v1/api/projects/" + businessLineId + "/estimate-miner-fee";
      const body = JSON.stringify({
        from_address: fromAddress,
        from_wallet_code: fromWalletCode,
        description: description,
        dest_address_item_list: [
          {
            amount: amount,
            dest_address: destAddress,
            memo_type: memoType,
            memo: memo,
            is_all_withdrawal: false,
            remark: remark,
          },
        ],
      });
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "POST",
        urlEstimateGasFee,
        undefined,
        body
      );
    } catch (error) {
      throw error;
    }
  }

  async createWithdrawalOrder(
    businessLineId: string,
    fromAddress: string,
    fromWalletCode: string,
    coinName: string,
    description: string,
    amount: number,
    destAddress: string,
    memoType: string,
    memo: string,
    remark: string
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlCreateWithdrawalOrder =
        "/custody/v1/api/projects/" + businessLineId + "/order/create";
      const body = JSON.stringify({
        from_address: fromAddress,
        from_wallet_code: fromWalletCode,
        coin_name: coinName,
        description: description,
        dest_address_item_list: [
          {
            amount: amount,
            dest_address: destAddress,
            memo_type: memoType,
            memo: memo,
            is_all_withdrawal: false,
            remark: remark,
          },
        ],
      });
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "POST",
        urlCreateWithdrawalOrder,
        undefined,
        body
      );
    } catch (error) {
      throw error;
    }
  }

  async getWithdrawalRateRange() {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlWithdrawalRateRange = "/custody/v1/api/customize-fee-rate/range";
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlWithdrawalRateRange,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getWithdrawalRate() {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlWithdrawalRate = "/custody/v1/api/recommend-fee-rate/list";
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlWithdrawalRate,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getWalletTransactionSummary(
    businessLineId: string,
    walletCode: string
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlWalletTransactionSummary =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/wallets/" +
        walletCode +
        "/tx-summaries";
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlWalletTransactionSummary,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getTransactionDetailsDeFi(
    businessLineId: string,
    walletCode: string,
    orderNo: string
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlTransactionDetails =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/wallets/" +
        walletCode +
        "/contract/orders/" +
        orderNo;
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlTransactionDetails,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async editTransactionDetailRemark(
    businessLineId: string,
    walletCode: string,
    id: number,
    remark: string
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlEditTransactionDetailRemark =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/wallets/" +
        walletCode +
        "/details/" +
        id;
      const body = JSON.stringify({
        remark: remark,
      });
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "POST",
        urlEditTransactionDetailRemark,
        undefined,
        body
      );
    } catch (error) {
      throw error;
    }
  }

  async getTransactionDetailsForCustomCurrency(
    businessLineId: string,
    walletCode: string
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlTransactionDetailsForCustomCurrency =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/wallets/" +
        walletCode +
        "/customize-flow";
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlTransactionDetailsForCustomCurrency,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getTotalAssetHistoryNotionalValue() {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlTotalAssetHistoryNotionalValue = "/custody/v1/api/history-asset";
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlTotalAssetHistoryNotionalValue,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getCurrentAssetNotionalValue() {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlCurrentAssetNotionalValue = "/custody/v1/api/asset";
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlCurrentAssetNotionalValue,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getOrdersFilterByCriteria(businessLineId: string) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlOrdersFilterByCriteria =
        "/custody/v1/api/projects/" + businessLineId + "/orders";
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlOrdersFilterByCriteria,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getOrderDetails(businessLineId: string, orderNo: string) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlOrderDetails =
        "/custody/v1/api/projects/" + businessLineId + "/orders/" + orderNo;
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlOrderDetails,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async rbf(businessLineId: string, orderNo: string, level: string) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlRBF =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/orders/" +
        orderNo +
        "/accelerate";
      const body = JSON.stringify({
        level: level,
      });
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "POST",
        urlRBF,
        undefined,
        body
      );
    } catch (error) {
      throw error;
    }
  }

  async cancelOrder(businessLineId: string, orderNo: string, level: string) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlCancelOrder =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/orders/" +
        orderNo +
        "/cancel";
      const body = JSON.stringify({
        level: level,
      });
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "POST",
        urlCancelOrder,
        undefined,
        body
      );
    } catch (error) {
      throw error;
    }
  }

  async getOrderTXForCallback(orderNo: string, level: string) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlOrderTXForCallback = "/[Client Url]";
      const body = JSON.stringify({
        // domain_id: domain_id,
        // b_id: b_id,
        // wallet_code: wallet_code,
        // order_id: order_id,
        // confirmations: confirmations,
        // event_status: event_status,
        // event_type: event_type,
        // detail_id: detail_id,
      });
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "POST",
        urlOrderTXForCallback,
        undefined,
        body
      );
    } catch (error) {
      throw error;
    }
  }

  async createContractOrder(
    businessLineId: string,
    orderNo: string,
    fromWalletCode: string,
    fromAddress: string,
    toAddress: string,
    amount: number,
    contractData: string,
    gasPriceLevel: string,
    gasPrice: number,
    gasLimit: string,
    description: string
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlContractOrder =
        "/custody/v1/api/projects/" + businessLineId + "/contract/call";
      const body = JSON.stringify({
        order_no: orderNo,
        from_wallet_code: fromWalletCode,
        from_address: fromAddress,
        to_address: toAddress,
        amount: amount,
        contract_data: contractData,
        "gas_price_level ": gasPriceLevel,
        gas_price: gasPrice,
        gas_limit: gasLimit,
        description: description,
      });
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "POST",
        urlContractOrder,
        undefined,
        body
      );
    } catch (error) {
      throw error;
    }
  }

  async signContractOrder(
    businessLineId: string,
    walletCode: string,
    address: string,
    signatureVersion: string,
    payloadMessage: string,
    chain: string,
    orderNo: string,
    description: string
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlSignContractOrder =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/wallets/" +
        walletCode +
        "/signatures";
      const body = JSON.stringify({
        address: address,
        signature_version: signatureVersion,
        payload: { message: payloadMessage },
        chain: chain,
        order_no: orderNo,
        description: description,
      });

      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "POST",
        urlSignContractOrder,
        undefined,
        body
      );
    } catch (error) {
      throw error;
    }
  }

  async getConnectedWalletTransactionHistory(
    businessLineId: string,
    walletCode: string
  ) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlConnectedWalletTransactionHistory =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/wallets/" +
        walletCode +
        "/contract/orders";
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlConnectedWalletTransactionHistory,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async getTransactionDetails(businessLineId: string, walletCode: string) {
    try {
      const cactusKey = await this._vaultService.getCactusCustody();

      const urlTransactionDetails =
        "/custody/v1/api/projects/" +
        businessLineId +
        "/wallets/" +
        walletCode +
        "/contract/orders";
      return this.callExternalService(
        cactusKey.data.akid,
        cactusKey.data.apiKey,
        cactusKey.data.privateKey,
        "GET",
        urlTransactionDetails,
        undefined,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async callExternalService(
    akid: string,
    apiKey: string,
    privateKey: string,
    mothod: string,
    endpoint: string,
    queryParam?: any[],
    body?: string
  ) {
    const date = new Date().toUTCString();
    const apiNonce = Math.round(new Date().getTime() / 1000);

    var queryString = "";
    var queryStringToSign = "";
    if (queryParam !== undefined) {
      if (queryParam?.length != 0) {
        queryString = "?" + (await this.getUrlParams(queryParam));
        queryStringToSign = "?" + (await this.getUrlSignParams(queryParam));
      }
    }

    var bodyHash = undefined;
    if (body !== undefined) {
      bodyHash = await this.getBodyHash(body);
    }

    var contentToSign = "";
    if (mothod !== "GET") {
      contentToSign =
        mothod +
        "\n" +
        "application/json\n" +
        bodyHash +
        "\n" +
        "application/json\n" +
        date +
        "\n" +
        "x-api-key:" +
        apiKey +
        "\n" +
        "x-api-nonce:" +
        apiNonce +
        "\n" +
        endpoint
    } else {
      contentToSign =
        mothod +
        "\n" +
        "application/json\n" +
        "\n" +
        "application/json\n" +
        date +
        "\n" +
        "x-api-key:" +
        apiKey +
        "\n" +
        "x-api-nonce:" +
        apiNonce +
        "\n" +
        endpoint +
        queryStringToSign;
    }

    const signature = await this.generateSignature(contentToSign, privateKey);

    const header = await this.getHeaderMethod(
      akid,
      apiKey,
      apiNonce.toString(),
      date,
      signature,
      bodyHash
    );

    return await this._httpService.callFetch(
      mothod,
      header,
      process.env.HOST_CACTUS_CUSTODY! + endpoint + queryString,
      body
    );
  }

  async getUrlParams(queryParam: string[]) {
    if (queryParam.length == 0) {
      return "";
    }
    var param = "";
    var sortedKey = queryParam.sort();
    sortedKey.forEach((element) => {
      Object.entries(element).forEach(([key, value]) => {
        param += key + "=" + value + "&";
      });
    });
    param = param.slice(0, -1);
    return param;
  }

  async getUrlSignParams(queryParam: string[]) {
    if (queryParam.length == 0) {
      return "";
    }
    var param = "{";
    var sortedKey = queryParam.sort();
    sortedKey.forEach((element) => {
      Object.entries(element).forEach(([key, value]) => {
        param += key + "=[" + value + "], ";
      });
    });
    param = param.slice(0, -2);
    return param + "}";
  }

  async getHeaderMethod(
    akid: string,
    apiKey: string,
    apiNonce: string,
    date: string,
    signature: string,
    bodyHash?: string
  ) {
    if (bodyHash !== undefined) {
      return {
        "x-api-key": apiKey,
        "x-api-nonce": apiNonce,
        Accept: "application/json",
        "Content-SHA256": bodyHash,
        Date: date,
        "Content-type": "application/json",
        Authorization: "api " + akid + ":" + signature,
      };
    } else {
      return {
        "x-api-key": apiKey,
        "x-api-nonce": apiNonce,
        Accept: "application/json",
        Date: date,
        "Content-type": "application/json",
        Authorization: "api " + akid + ":" + signature,
      };
    }
  }

  async generateSignature(contentToSign: string, privateKey: string) {
    const sign = crypto.createSign("sha256");
    sign.write(contentToSign);
    sign.end();
    var signature = sign.sign(privateKey, "base64");
    return signature;
  }

  async getBodyHash(contentToSign: string) {
    const hash = crypto.createHash("sha256");
    hash.update(contentToSign);
    hash.end();
    return hash.digest("base64");
  }
}
