import { AssetRepository } from "../repositories/assetRepository";
import { ChainalysisTransactionRepository } from "../repositories/chainalysisTransactionRepository";
import { VaultService } from "./vaultService";
import { FireblocksService } from "./fireblocksService";
import { singleton } from "tsyringe";
import * as enums from "../utilities/enums";
import CustomError from "../middlewares/customError";
import { HttpService } from "./httpService";

@singleton()
export class ChainalysisService {
  readonly _assetRepository: AssetRepository;
  readonly _chainalysisTransactionRepository: ChainalysisTransactionRepository;
  readonly _vaultService: VaultService;
  readonly _fireblocksService: FireblocksService;
  readonly _httpService: HttpService;
  constructor(
    chainalysisTransactionRepository: ChainalysisTransactionRepository,
    assetRepository: AssetRepository,
    vaultService: VaultService,
    fireblocksService: FireblocksService,
    httpService: HttpService
  ) {
    this._chainalysisTransactionRepository = chainalysisTransactionRepository;
    this._assetRepository = assetRepository;
    this._vaultService = vaultService;
    this._fireblocksService = fireblocksService;
    this._httpService = httpService;
  }

  async manualUpdateTransactionStatus(externalId: string) {
    try {
      await this.updateTransactionStatus(externalId);
    } catch (error) {
      throw error;
    }
  }

  async updateTransactionStatus(externalId: string) {
    try {
      const chainalysisTransactions =
        await this._chainalysisTransactionRepository.getInfoByExternalId(
          externalId
        );
      if (chainalysisTransactions.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const chainalysisTransactionInfo = chainalysisTransactions[0];

      const summary = await this.transfers_get_a_summary(externalId);
      if (summary !== undefined) {
        if (summary.updatedAt !== undefined) {
          if (summary.updatedAt !== null) {
            await this._chainalysisTransactionRepository.update(
              externalId,
              summary.updatedAt,
              summary.asset,
              summary.network,
              summary.transferReference,
              summary.tx,
              summary.idx,
              summary.usdAmount,
              summary.assetAmount,
              summary.timestamp,
              summary.outputAddress
            );
            const alertList = await this.transfers_get_alerts(externalId);
            if (alertList === undefined) {
              await this._fireblocksService.unfreezeTransactionById(
                chainalysisTransactionInfo.fb_transaction_id
              );
            } else {
              if (alertList.alerts.length <= 0) {
                await this._fireblocksService.unfreezeTransactionById(
                  chainalysisTransactionInfo.fb_transaction_id
                );
              } else {
                var isPass = true;
                for (let index = 0; index < alertList.alerts.length; index++) {
                  const alert = alertList.alerts[index];
                  if (
                    alert.alertLevel === enums.chainalysisAlert.HIGH ||
                    alert.alertLevel === enums.chainalysisAlert.MEDIUM
                  ) {
                    isPass = false;
                    break;
                  }
                }
                if (isPass) {
                  await this._fireblocksService.unfreezeTransactionById(
                    chainalysisTransactionInfo.fb_transaction_id
                  );
                }
              }
            }
          }
        }
      }
    } catch (error) {
      throw error;
    }
  }

  async monitoringTransaction(body: any) {
    try {
      let data = body.data;

      const chainalysisTransactions =
        await this._chainalysisTransactionRepository.getInfoByFBTransactionId(
          data.id
        );
      if (chainalysisTransactions.length > 0) {
        return;
      }

      if (data.source === undefined) {
        return;
      }

      if (
        data.source.type === "EXTERNAL_WALLET" ||
        data.source.type === "UNKNOWN"
      ) {
        let vUserId = "";
        if (data.destination != undefined) {
          if (data.destination.id != "") {
            vUserId = data.destination.id;
          } else {
            vUserId = "Anonymous";
          }
        }

        const assets = await this._assetRepository.getInfoByCodeIgnoreStatus(
          data.assetId
        );
        if (assets.length <= 0) {
          return;
        }
        const assetInfo = assets[0];

        const registerTrasaction = await this.register_a_transfer(
          vUserId,
          assetInfo.network_name,
          assetInfo.native_asset_symbol,
          data.txHash,
          data.destinationAddress,
          "sent",
          data.amount,
          1,
          "USD"
        );

        await this._chainalysisTransactionRepository.create(
          data.id,
          registerTrasaction.updatedAt,
          registerTrasaction.asset,
          registerTrasaction.network,
          registerTrasaction.transferReference,
          registerTrasaction.tx,
          registerTrasaction.idx,
          registerTrasaction.usdAmount,
          registerTrasaction.assetAmount,
          registerTrasaction.timestamp,
          registerTrasaction.outputAddress,
          registerTrasaction.externalId
        );

        await this._fireblocksService.freezeTransactionById(data.id);
      }
    } catch (error) {
      throw error;
    }
  }

  async register_a_transfer(
    userId: string,
    pNetwork: string,
    pAsset: string,
    pTransactionHash: string,
    pOutputAddress: string,
    pDirection: string,
    pAssetAmount: number,
    pAssetPrice: number,
    pAssetDenomination: string
  ) {
    try {
      const chainalysisToken = await this._vaultService.getChainalysisToken();
      const vBody = JSON.stringify({
        network: pNetwork,
        asset: pAsset,
        transferReference: pTransactionHash + ":" + pOutputAddress,
        direction: pDirection,
        transferTimestamp: new Date().toISOString(),
        assetAmount: pAssetAmount,
        outputAddress: pOutputAddress,
        assetPrice: pAssetPrice,
        assetDenomination: pAssetDenomination,
      });

      return await this._httpService.callFetch(
        "POST",
        {
          Token: chainalysisToken.data.apiKey,
          "Content-type": "application/json",
        },
        process.env.CHAINALYSIS_ENDPOINT! +
          "/v2/users/" +
          userId +
          "/transfers",
        vBody
      );
    } catch (error) {
      throw error;
    }
  }

  async register_a_withdrawal_attempt(
    userId: string,
    pNetwork: string,
    pAsset: string,
    pAddress: string,
    pAttemptIdentifier: string,
    pAssetAmount: number,
    pAssetPrice: number,
    pAssetDenomination: string
  ) {
    try {
      const chainalysisToken = await this._vaultService.getChainalysisToken();
      const vBody = JSON.stringify({
        network: pNetwork,
        asset: pAsset,
        address: pAddress,
        attemptIdentifier: pAttemptIdentifier,
        assetAmount: pAssetAmount,
        assetPrice: pAssetPrice,
        assetDenomination: pAssetDenomination,
        attemptTimestamp: new Date().toISOString(),
      });

      return await this._httpService.callFetch(
        "POST",
        {
          Token: chainalysisToken.data.apiKey,
          "Content-type": "application/json",
        },
        process.env.CHAINALYSIS_ENDPOINT! +
          "/v2/users/" +
          userId +
          "/withdrawal-attempts",
        vBody
      );
    } catch (error) {
      throw error;
    }
  }

  async transfers_get_a_summary(externalId: string) {
    try {
      const chainalysisToken = await this._vaultService.getChainalysisToken();
      return await this._httpService.callFetch(
        "GET",
        {
          Token: chainalysisToken.data.apiKey,
          "Content-type": "application/json",
        },
        process.env.CHAINALYSIS_ENDPOINT! + "/v2/transfers/" + externalId,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async transfers_get_direct_exposure(externalId: string) {
    try {
      const chainalysisToken = await this._vaultService.getChainalysisToken();
      return await this._httpService.callFetch(
        "GET",
        {
          Token: chainalysisToken.data.apiKey,
          "Content-type": "application/json",
        },
        process.env.CHAINALYSIS_ENDPOINT! +
          "/v2/transfers/" +
          externalId +
          "/exposures",
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async transfers_get_alerts(externalId: string) {
    try {
      const chainalysisToken = await this._vaultService.getChainalysisToken();
      return await this._httpService.callFetch(
        "GET",
        {
          Token: chainalysisToken.data.apiKey,
          "Content-type": "application/json",
        },
        process.env.CHAINALYSIS_ENDPOINT! +
          "/v2/transfers/" +
          externalId +
          "/alerts",
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async transfers_get_network_identifications(externalId: string) {
    try {
      const chainalysisToken = await this._vaultService.getChainalysisToken();
      return await this._httpService.callFetch(
        "GET",
        {
          Token: chainalysisToken.data.apiKey,
          "Content-type": "application/json",
        },
        process.env.CHAINALYSIS_ENDPOINT! +
          "/v2/transfers/" +
          externalId +
          "/network-identifications",
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async withdrawal_attempts_get_a_summary(externalId: string) {
    try {
      const chainalysisToken = await this._vaultService.getChainalysisToken();
      return await this._httpService.callFetch(
        "GET",
        {
          Token: chainalysisToken.data.apiKey,
          "Content-type": "application/json",
        },
        process.env.CHAINALYSIS_ENDPOINT! +
          "/v2/withdrawal-attempts/" +
          externalId,
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async withdrawal_attempts_get_direct_exposure(externalId: string) {
    try {
      const chainalysisToken = await this._vaultService.getChainalysisToken();
      return await this._httpService.callFetch(
        "GET",
        {
          Token: chainalysisToken.data.apiKey,
          "Content-type": "application/json",
        },
        process.env.CHAINALYSIS_ENDPOINT! +
          "/v2/withdrawal-attempts/" +
          externalId +
          "/exposures",
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async withdrawal_attempts_get_alerts(externalId: string) {
    try {
      const chainalysisToken = await this._vaultService.getChainalysisToken();
      return await this._httpService.callFetch(
        "GET",
        {
          Token: chainalysisToken.data.apiKey,
          "Content-type": "application/json",
        },
        process.env.CHAINALYSIS_ENDPOINT! +
          "/v2/withdrawal-attempts/" +
          externalId +
          "/alerts",
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async withdrawal_attempts_get_address_identifications(externalId: string) {
    try {
      const chainalysisToken = await this._vaultService.getChainalysisToken();
      return await this._httpService.callFetch(
        "GET",
        {
          Token: chainalysisToken.data.apiKey,
          "Content-type": "application/json",
        },
        process.env.CHAINALYSIS_ENDPOINT! +
          "/v2/withdrawal-attempts/" +
          externalId +
          "/high-risk-addresses",
        undefined
      );
    } catch (error) {
      throw error;
    }
  }

  async withdrawal_attempts_get_network_identifications(externalId: string) {
    try {
      const chainalysisToken = await this._vaultService.getChainalysisToken();
      return await this._httpService.callFetch(
        "GET",
        {
          Token: chainalysisToken.data.apiKey,
          "Content-type": "application/json",
        },
        process.env.CHAINALYSIS_ENDPOINT! +
          "/v2/withdrawal-attempts/" +
          externalId +
          "/network-identifications",
        undefined
      );
    } catch (error) {
      throw error;
    }
  }
}
