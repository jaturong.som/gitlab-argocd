import { VaultAccountRepository } from "../repositories/vaultAccountRepository";
import { FireblocksService } from "./fireblocksService";
import * as enums from "../utilities/enums";
import CustomError from "../middlewares/customError";
import { singleton } from "tsyringe";

@singleton()
export class VaultAccountService {
  readonly _vaultAccountRepository: VaultAccountRepository;
  readonly _fireblocksService: FireblocksService;
  constructor(
    vaultAccountRepository: VaultAccountRepository,
    fireblocksService: FireblocksService
  ) {
    this._vaultAccountRepository = vaultAccountRepository;
    this._fireblocksService = fireblocksService;
  }

  async getVaultAccount(type: string, companyId?: number, userId?: number) {
    try {
      const vaultAccounts = await this._vaultAccountRepository.getInfo(
        type,
        companyId,
        userId
      );
      if (vaultAccounts.length <= 0) {
        throw new CustomError(
          enums.responseCode.VaultAccountNotFound,
          enums.responseMessage.VaultAccountNotFound
        );
      }
      return vaultAccounts[0];
    } catch (error) {
      throw error;
    }
  }

  async createVaultAccount(
    type: string,
    name: string,
    logOnId: string,
    companyId?: number,
    userId?: number
  ) {
    try {
      const isExists = await this._vaultAccountRepository.getInfo(
        type,
        companyId,
        userId
      );
      if (isExists.length > 0) {
        throw new CustomError(
          enums.responseCode.Duplicate,
          enums.responseMessage.Duplicate
        );
      }
      let fireblockResponse = await this._fireblocksService.createVaultAccount(
        name,
        undefined,
        userId === undefined ? companyId!.toString() : userId.toString(),
        true
      );
      await this._vaultAccountRepository.create(
        type,
        name,
        fireblockResponse.id,
        logOnId,
        companyId,
        userId
      );
      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }
}
