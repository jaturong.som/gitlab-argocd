import { UserRepository } from "../repositories/userRepository";
import { UserRoleRepository } from "../repositories/userRoleRepository";
import { AddressRepository } from "../repositories/addressRepository";
import { UserDocumentRepository } from "../repositories/userDocumentRepository";
import { UserTokenRepository } from "../repositories/userTokenRepository";
import { RoleRepository } from "../repositories/roleRepository";
import { RoleMenuRepository } from "../repositories/roleMenuRepository";
import { MenuRepository } from "../repositories/menuRepository";
import { GroupMenuRepository } from "../repositories/groupMenuRepository";
import { CompanyRepository } from "../repositories/companyRepository";
import { FiatAccountRepository } from "../repositories/fiatAccountRepository";
import { AssetRepository } from "../repositories/assetRepository";
import { WithdrawRepository } from "../repositories/withdrawRepository";
import { SummaryRepository } from "../repositories/summaryRepository";
import * as enums from "../utilities/enums";
import CustomError from "../middlewares/customError";
import * as utils from "../utilities/utils";
import { singleton } from "tsyringe";
import { TransactionService } from "./transactionService";
import { LqnWithdrawService } from "./lqnWithdrawService";
import { LqnUserRepository } from "../repositories/lqnUserRepository";
import { WalletCreditRepository } from "../repositories/walletCreditRepository";
import { WalletCreditDocumentRepository } from "../repositories/walletCreditDocumentRepository";
import { AWSService } from "./awsService";
import { v4 as uuidv4 } from "uuid";
import { BankAccountRepository } from "../repositories/bankAccountRepository";
import { CountryRepository } from "../repositories/countryRepository";
import { CustomerService } from "./customerService";
import { CactusWalletService } from "./cactusWalletService";
import { CactusWalletAddressService } from "./cactusWalletAddressService";
import { AssetInVaultAccountService } from "./assetInVaultAccountService";


@singleton()
export class AdminService {
  readonly _userRepository: UserRepository;
  readonly _userRoleRepository: UserRoleRepository;
  readonly _addressRepository: AddressRepository;
  readonly _userDocumentRepository: UserDocumentRepository;
  readonly _userTokenRepository: UserTokenRepository;
  readonly _roleRepository: RoleRepository;
  readonly _roleMenuRepository: RoleMenuRepository;
  readonly _menuRepository: MenuRepository;
  readonly _groupMenuRepository: GroupMenuRepository;
  readonly _companyRepository: CompanyRepository;
  readonly _fiatAccountRepository: FiatAccountRepository;
  readonly _assetRepository: AssetRepository;
  readonly _withdrawRepository: WithdrawRepository;
  readonly _summaryRepository: SummaryRepository;
  readonly _cactusWalletService: CactusWalletService;
  readonly _cactusWalletAddressService: CactusWalletAddressService;
  readonly _transactionService: TransactionService;
  readonly _lqnWithdrawService: LqnWithdrawService;
  readonly _lqnUserRepository: LqnUserRepository;
  readonly _walletCreditRepository: WalletCreditRepository;
  readonly _walletCreditDocumentRepository: WalletCreditDocumentRepository;
  readonly _awsService: AWSService;
  readonly _bankAccountRepository: BankAccountRepository;
  readonly _countryRepository: CountryRepository;
  readonly _customerService: CustomerService;
  readonly _assetInVaultAccountService: AssetInVaultAccountService;
  constructor(
    userRepository: UserRepository,
    userRoleRepository: UserRoleRepository,
    addressRepository: AddressRepository,
    userDocumentRepository: UserDocumentRepository,
    userTokenRepository: UserTokenRepository,
    roleRepository: RoleRepository,
    roleMenuRepository: RoleMenuRepository,
    menuRepository: MenuRepository,
    groupMenuRepository: GroupMenuRepository,
    companyRepository: CompanyRepository,
    fiatAccountRepository: FiatAccountRepository,
    assetRepository: AssetRepository,
    withdrawRepository: WithdrawRepository,
    summaryRepository: SummaryRepository,
    cactusWalletService: CactusWalletService,
    cactusWalletAddressService: CactusWalletAddressService,
    transactionService: TransactionService,
    lqnWithdrawService: LqnWithdrawService,
    lqnUserRepository: LqnUserRepository,
    walletCreditRepository: WalletCreditRepository,
    walletCreditDocumentRepository: WalletCreditDocumentRepository,
    awsService: AWSService,
    bankAccountRepository: BankAccountRepository,
    countryRepository: CountryRepository,
    customerService: CustomerService,
    assetInVaultAccountService: AssetInVaultAccountService,
  ) {
    this._userRepository = userRepository;
    this._userRoleRepository = userRoleRepository;
    this._addressRepository = addressRepository;
    this._userDocumentRepository = userDocumentRepository;
    this._userTokenRepository = userTokenRepository;
    this._roleRepository = roleRepository;
    this._roleMenuRepository = roleMenuRepository;
    this._menuRepository = menuRepository;
    this._groupMenuRepository = groupMenuRepository;
    this._companyRepository = companyRepository;
    this._fiatAccountRepository = fiatAccountRepository;
    this._assetRepository = assetRepository;
    this._withdrawRepository = withdrawRepository;
    this._summaryRepository = summaryRepository;
    this._cactusWalletService = cactusWalletService;
    this._cactusWalletAddressService = cactusWalletAddressService;
    this._transactionService = transactionService;
    this._lqnWithdrawService = lqnWithdrawService;
    this._lqnUserRepository = lqnUserRepository;
    this._walletCreditRepository = walletCreditRepository;
    this._walletCreditDocumentRepository = walletCreditDocumentRepository;
    this._awsService = awsService;
    this._bankAccountRepository = bankAccountRepository;
    this._countryRepository = countryRepository;
    this._customerService = customerService;
    this._assetInVaultAccountService = assetInVaultAccountService;

  }

  async login(userName: string, password: string) {
    try {
      const users = await this._userRepository.getActiveLoginInfoByUserName(
        userName
      );
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UsernameOrPasswordIncorrectCase,
          enums.responseMessage.UsernameOrPasswordIncorrectCase
        );
      }
      const userInfo = users[0];
      if (!(await utils.comparePassword(password, userInfo.password))) {
        throw new CustomError(
          enums.responseCode.UsernameOrPasswordIncorrectCase,
          enums.responseMessage.UsernameOrPasswordIncorrectCase
        );
      } else {
        var result = await this.setUserLogin(userInfo);
        return result;
      }
    } catch (error) {
      throw error;
    }
  }

  async refresh(refreshToken: string) {
    try {
      interface ResponseRefresh {
        user_id: string;
        code: string;
        iat: number;
        exp: number;
      }
      const userAccess = await utils.jwtVerifyRefreshToken(refreshToken);
      const obj = JSON.stringify(userAccess);
      const resRefresh: ResponseRefresh = JSON.parse(obj);
      const users = await this._userRepository.getActiveInfoById(
        parseInt(resRefresh.user_id)
      );
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.Unauthorized,
          enums.responseMessage.Unauthorized
        );
      }
      const userInfo = users[0];
      var result = await this.setUserLogin(userInfo);
      return result;
    } catch (error) {
      throw error;
    }
  }

  async setUserLogin(userInfo: any) {
    const genToken = await utils.jwtGenerateToken(
      userInfo.user_id,
      userInfo.code
    );
    const genRefreshToken = await utils.jwtRefreshToken(
      userInfo.user_id,
      userInfo.code
    );
    const isExists = await this._userTokenRepository.getInfo(userInfo.user_id);
    if (isExists.length <= 0) {
      await this._userTokenRepository.create(
        userInfo.user_id,
        genToken,
        genRefreshToken
      );
    } else {
      await this._userTokenRepository.update(
        userInfo.user_id,
        genToken,
        genRefreshToken
      );
    }
    return {
      user_id: userInfo.user_id,
      code: userInfo.code,
      user_name: userInfo.user_name,
      full_name: userInfo.first_name + " " + userInfo.last_name,
      token: genToken,
      refresh_token: genRefreshToken,
    };
  }

  async getUsers() {
    try {
      const users = await this._userRepository.getList();
      var vResult = [];
      for (let i = 0; i < users.length; i++) {
        var userAddress = await this._addressRepository.getInfoByRefId(
          enums.addressType.USER_PROFILE,
          users[i].user_id
        );
        var userDocuments = await this._userDocumentRepository.getList(
          users[i].user_id
        );
        vResult.push(
          await this.setUserInfo(users[i], userAddress, userDocuments)
        );
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getUserAcriveList() {
    try {
      const users = await this._userRepository.getActiveList();
      return users;
    } catch (error) {
      throw error;
    }
  }

  async getUserById(userId: number) {
    try {
      const users = await this._userRepository.getInfoById(userId);
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }
      var userAddress = await this._addressRepository.getInfoByRefId(
        enums.addressType.USER_PROFILE,
        users[0].user_id
      );
      var userDocuments = await this._userDocumentRepository.getList(
        users[0].user_id
      );
      var result = await this.setUserInfo(users[0], userAddress, userDocuments);
      return result;
    } catch (error) {
      throw error;
    }
  }

  async getActiveUserById(userId: number) {
    try {
      const users = await this._userRepository.getActiveInfoById(userId);
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }
      var userAddress = await this._addressRepository.getInfoByRefId(
        enums.addressType.USER_PROFILE,
        users[0].user_id
      );
      var userDocuments = await this._userDocumentRepository.getList(
        users[0].user_id
      );
      var result = await this.setUserInfo(users[0], userAddress, userDocuments);
      return result;
    } catch (error) {
      throw error;
    }
  }

  async setUserInfo(userInfo: any, userAddress: any, userDocuments: any) {
    var vAddress = {};
    if (userAddress.length > 0) {
      vAddress = {
        address_id: userAddress[0].address_id,
        line1: userAddress[0].line1,
        line2: userAddress[0].line2,
        city: userAddress[0].city,
        state: userAddress[0].state,
        country_code: userAddress[0].country_code,
        country_name: userAddress[0].country_name,
        postcode: userAddress[0].postcode,
        area_town: userAddress[0].area_town,
      };
    }
    // var vUserDocuments = [];
    var vDocumentType = "";
    var vDocumentText = "";
    var vDocumentTypeRemark = "";
    var vDocumentIssueDate = "";
    var vDocumentExpiredDate = "";
    if (userDocuments.length > 0) {
      // for (let index = 0; index < userDocuments.length; index++) {
      //   const element = userDocuments[index];
      //   vUserDocuments.push({
      //     document_type: element.document_type,
      //     document_text: element.document_text,
      //   });
      // }
      vDocumentType = userDocuments[0].document_type;
      vDocumentText = userDocuments[0].document_text;
      vDocumentTypeRemark = userDocuments[0].document_type_remark;
      vDocumentIssueDate = userDocuments[0].document_issue_date;
      vDocumentExpiredDate = userDocuments[0].document_expired_date;
    }
    return {
      user_id: userInfo.user_id,
      code: userInfo.code,
      type: userInfo.type,
      user_name: userInfo.user_name,
      password: userInfo.password,
      first_name: userInfo.first_name,
      middle_name: userInfo.middle_name,
      last_name: userInfo.last_name,
      date_of_birth: userInfo.date_of_birth,
      email: userInfo.email,
      tel: userInfo.tel,
      country_code: userInfo.country_code,
      user_id_ref: userInfo.user_id_ref,
      status: userInfo.status,
      status_boolean: userInfo.status === "A" ? true : false,
      role_id: userInfo.role_id,
      gender: userInfo.gender,
      nationality: userInfo.nationality_code,
      occupation: userInfo.occupation,
      occupation_remark: userInfo.occupation_remark,
      address: vAddress,
      // documents: vUserDocuments,
      document_type: vDocumentType,
      document_text: vDocumentText,
      document_type_remark: vDocumentTypeRemark,
      document_issue_date: vDocumentIssueDate,
      document_expired_date: vDocumentExpiredDate,
    };
  }

  async createUser(
    password: string,
    firstName: string,
    middleName: string,
    lastName: string,
    email: string,
    tel: string,
    status: string,
    roleId: number,
    logOnId: string,
    gender: string,
    nationality: string,
    occupation: string,
    dateOfBirth: string,
    address: {
      line1: string;
      line2?: string;
      city: string;
      state: string;
      countryCode: string;
      postcode: string;
      areaTown?: string;
    },
    documentType: string,
    documentText: string,
    countryCode: string,
    occupationRemark?: string,
    documentTypeRemark?: string,
    documentIssueDate?: string,
    documentExpiredDate?: string
  ) {
    try {
      const isExists = await this._userRepository.getLoginInfoByUserName(email);
      if (isExists.length > 0) {
        throw new CustomError(
          enums.responseCode.Duplicate,
          enums.responseMessage.Duplicate
        );
      }

      var countries = await this._countryRepository.getInfoByCode(
        address.countryCode
      );
      if (countries.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const countryInfo = countries[0];

      const createSender = await this._lqnWithdrawService.CreateSender(
        firstName,
        middleName,
        lastName,
        gender,
        tel,
        nationality,
        documentType,
        documentText,
        "",
        documentIssueDate === undefined ? "" : documentIssueDate,
        documentExpiredDate === undefined ? "" : documentExpiredDate,
        dateOfBirth,
        occupation,
        email,
        address.line1 + " " + address?.line2,
        address.city,
        address.state,
        address.postcode,
        address.countryCode,
        logOnId,
        0,
        countryInfo.currency_code,
        occupationRemark!,
        documentTypeRemark!
      );

      await utils.passwordValidation(password);
      const hashPassword = await utils.hashPassword(password);
      const userCreated = await this._userRepository.create(
        await utils.generateKey(email),
        "G",
        hashPassword,
        firstName,
        middleName,
        lastName,
        email,
        tel,
        status,
        logOnId,
        gender,
        nationality,
        occupation,
        occupationRemark!,
        dateOfBirth,
        countryCode
      );
      const userInfo = userCreated[0];

      await this._lqnUserRepository.updateUserIdById(
        createSender[0].lqn_user_profile_id,
        userInfo.user_id
      );

      if (roleId !== 0) {
        await this._userRoleRepository.create(
          userInfo.user_id,
          roleId,
          logOnId
        );
      }
      if (address !== undefined) {
        if (address.line1 !== undefined) {
          await this.CreateOrUpdateUserAddress(
            userInfo.user_id,
            address.line1!,
            address.line2!,
            address.city!,
            address.state!,
            address.countryCode!,
            address.postcode!,
            address.areaTown!,
            logOnId
          );
        }
      }
      if (documentType !== undefined) {
        if (documentType !== "") {
          await this.CreateOrUpdateUserDocument(
            userInfo.user_id,
            documentType!,
            documentText!,
            logOnId,
            documentTypeRemark!,
            documentIssueDate!,
            documentExpiredDate!
          );
        }
      }

      await this._cactusWalletService.createWalletAccount(
        enums.accountType.USER,
        logOnId,
        undefined,
        userInfo.user_id
      )

      await this._fiatAccountRepository.create(
        enums.accountType.USER,
        "0",
        undefined,
        userInfo.user_id
      );

      var result = enums.responseMessage.Success;
      return result;
    } catch (error) {
      throw error;
    }
  }

  async updateUser(
    userId: number,
    firstName: string,
    middleName: string,
    lastName: string,
    tel: string,
    status: string,
    roleId: number,
    logOnId: string,
    gender: string,
    nationality: string,
    occupation: string,
    dateOfBirth: string,
    address: {
      line1: string;
      line2?: string;
      city: string;
      state: string;
      countryCode: string;
      postcode: string;
      areaTown?: string;
    },
    documentType: string,
    documentText: string,
    countryCode: string,
    occupationRemark?: string,
    documentTypeRemark?: string,
    documentIssueDate?: string,
    documentExpiredDate?: string
  ) {
    try {
      const users = await this._userRepository.getInfoById(userId);
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }

      var senders = await this._lqnUserRepository.getLqnUserByType(
        userId,
        enums.lqnUserType.SENDER
      );
      if (senders.length <= 0) {
        var countries = await this._countryRepository.getInfoByCode(
          address.countryCode
        );
        if (countries.length <= 0) {
          throw new CustomError(
            enums.responseCode.NotFound,
            enums.responseMessage.NotFound
          );
        }
        const countryInfo = countries[0];

        const createSender = await this._lqnWithdrawService.CreateSender(
          firstName,
          middleName,
          lastName,
          gender,
          tel,
          nationality,
          documentType,
          documentText,
          "",
          documentIssueDate === undefined ? "" : documentIssueDate,
          documentExpiredDate === undefined ? "" : documentExpiredDate,
          dateOfBirth,
          occupation,
          users[0].email,
          address.line1 + " " + address?.line2,
          address.city,
          address.state,
          address.postcode,
          address.countryCode,
          logOnId,
          0,
          countryInfo.currency_code,
          occupationRemark!,
          documentTypeRemark!
        );

        await this._lqnUserRepository.updateUserIdById(
          createSender[0].lqn_user_profile_id,
          userId
        );
      } else {
        var isUpdate = true;
        if (
          senders[0].first_name == firstName &&
          senders[0].middle_name == middleName &&
          senders[0].last_name == lastName
        ) {
          isUpdate = true;
        } else {
          isUpdate = false;
        }

        await this._lqnWithdrawService.UpdateSender(
          senders[0].lqn_user_profile_id,
          senders[0].lqn_ref_id,
          firstName,
          middleName,
          lastName,
          gender,
          tel,
          nationality,
          documentType,
          documentText,
          countryCode,
          documentIssueDate === undefined ? "" : documentIssueDate,
          documentExpiredDate === undefined ? "" : documentExpiredDate,
          dateOfBirth,
          occupation,
          senders[0].email,
          address.line1 + " " + address?.line2,
          address.city,
          address.state,
          address.postcode,
          address.countryCode,
          logOnId,
          isUpdate,
          userId,
          occupationRemark === undefined ? "" : occupationRemark,
          documentTypeRemark === undefined ? "" : documentTypeRemark
        );

        if (!isUpdate) {
          var bankAccount =
            await this._bankAccountRepository.getActiveAccountListByUserId(
              userId
            );
          if (bankAccount.length > 0) {
            for (let i = 0; i < bankAccount.length; i++) {
              await this._bankAccountRepository.inactive(
                bankAccount[i].bank_account_id,
                logOnId
              );
            }
          }
        }
      }

      if (
        users[0].user_id === logOnId &&
        users[0].status !== status &&
        status === "I"
      ) {
        throw new CustomError(
          enums.responseCode.ProtectOwnerAccount,
          enums.responseMessage.ProtectOwnerAccount
        );
      }

      await this._userRepository.update(
        userId,
        firstName,
        middleName,
        lastName,
        tel,
        status,
        logOnId,
        gender,
        nationality,
        occupation,
        occupationRemark!,
        dateOfBirth,
        countryCode,
        users[0].email
      );
      await this._userRoleRepository.deleteByUserId(userId);
      await this._userRoleRepository.create(userId, roleId, logOnId);
      if (address !== undefined) {
        if (address.line1 !== undefined) {
          await this.CreateOrUpdateUserAddress(
            userId,
            address.line1!,
            address.line2!,
            address.city!,
            address.state!,
            address.countryCode!,
            address.postcode!,
            address.areaTown!,
            logOnId
          );
        }
      }
      if (documentType !== undefined) {
        if (documentType !== "") {
          await this.CreateOrUpdateUserDocument(
            userId,
            documentType!,
            documentText!,
            logOnId,
            documentTypeRemark!,
            documentIssueDate!,
            documentExpiredDate!
          );
        }
      }
      var result = enums.responseMessage.Success;
      return result;
    } catch (error) {
      throw error;
    }
  }

  async CreateOrUpdateUserAddress(
    userId: number,
    line1: string,
    line2: string,
    city: string,
    state: string,
    countryCode: string,
    postcode: string,
    areaTown: string,
    logOnId: string
  ) {
    try {
      const isExists = await this._addressRepository.getInfoByRefId(
        enums.addressType.USER_PROFILE,
        userId
      );
      if (isExists.length <= 0) {
        return await this._addressRepository.create(
          enums.addressType.USER_PROFILE,
          userId,
          line1,
          line2,
          city,
          state,
          countryCode,
          postcode,
          areaTown,
          logOnId
        );
      }
      return await this._addressRepository.update(
        isExists[0].address_id,
        line1,
        line2,
        city,
        state,
        countryCode,
        postcode,
        areaTown,
        logOnId
      );
    } catch (error) {
      throw error;
    }
  }

  async CreateOrUpdateUserDocument(
    userId: number,
    documentType: string,
    documentText: string,
    logOnId: string,
    documentTypeRemark: string,
    documentIssueDate: string,
    documentExpiredDate: string
  ) {
    try {
      const isExists = await this._userDocumentRepository.getList(userId);
      if (isExists.length <= 0) {
        return await this._userDocumentRepository.create(
          userId,
          documentType,
          documentText,
          logOnId,
          documentTypeRemark,
          documentIssueDate,
          documentExpiredDate
        );
      }
      return await this._userDocumentRepository.updateByDocType(
        isExists[0].user_document_id,
        documentType,
        documentText,
        logOnId,
        documentTypeRemark,
        documentIssueDate,
        documentExpiredDate
      );
    } catch (error) {
      throw error;
    }
  }

  async deleteUser(userId: number) {
    try {
      const isExists = await this._userRepository.getInfoById(userId);
      if (isExists.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }
      await this._userRoleRepository.deleteByUserId(userId);
      await this._userTokenRepository.delete(userId);
      await this._userRepository.delete(userId);
      var result = enums.responseMessage.Success;
      return result;
    } catch (error) {
      throw error;
    }
  }

  async changePassword(
    userId: number,
    old_password: string,
    new_password: string,
    logOnId: string
  ) {
    try {
      const users = await this._userRepository.getInfoById(userId);
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }
      const userInfo = users[0];
      if (!(await utils.comparePassword(old_password, userInfo.password))) {
        throw new CustomError(
          enums.responseCode.PasswordMismatch,
          enums.responseMessage.PasswordMismatch
        );
      }
      await utils.passwordValidation(new_password);
      const hashPassword = await utils.hashPassword(new_password);
      await this._userRepository.changePassword(userId, hashPassword, logOnId);
      var result = await this.setUserLogin(userInfo);
      return result;
    } catch (error) {
      throw error;
    }
  }

  async resetPassword(userId: number, new_password: string, logOnId: string) {
    try {
      const users = await this._userRepository.getInfoById(userId);
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }
      const userInfo = users[0];
      await utils.passwordValidation(new_password);
      const hashPassword = await utils.hashPassword(new_password);
      await this._userRepository.changePassword(userId, hashPassword, logOnId);

      var result = await this.setUserLogin(userInfo);
      return result;
    } catch (error) {
      throw error;
    }
  }

  async getRoles() {
    try {
      const roles = await this._roleRepository.getList();
      var vResult = [];
      for (let i = 0; i < roles.length; i++) {
        vResult.push(await this.setRoleInfo(roles[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getActiveRoles() {
    try {
      const roles = await this._roleRepository.getActiveList();
      var vResult = [];
      for (let i = 0; i < roles.length; i++) {
        vResult.push(await this.setRoleInfo(roles[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async getRoleById(roleId: number) {
    try {
      const roles = await this._roleRepository.getInfoById(roleId);
      if (roles.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const roleInfo = roles[0];
      var result_menus = [];
      const group_menus = await this._groupMenuRepository.getList();
      for (let i = 0; i < group_menus.length; i++) {
        const groupMenuInfo = group_menus[i];
        const menus = await this._menuRepository.getListByGroupMenuCode(
          groupMenuInfo.group_menu_code
        );
        var new_menus = [];
        for (let j = 0; j < menus.length; j++) {
          const menuInfo = menus[j];
          const roleMenus1 =
            await this._roleMenuRepository.getInfoByRoleIdGroupMenuCodeMenuCode(
              roleInfo.role_id,
              groupMenuInfo.group_menu_code,
              menuInfo.menu_code
            );
          new_menus.push({
            menu_code: menuInfo.menu_code,
            name: menuInfo.name,
            url: menuInfo.url,
            component_name: menuInfo.component_name,
            permission: roleMenus1.length > 0 ? true : false,
          });
        }
        if (new_menus.length <= 0) {
          const roleMenus2 =
            await this._roleMenuRepository.getInfoByRoleIdGroupMenuCode(
              roleInfo.role_id,
              groupMenuInfo.group_menu_code
            );
          result_menus.push({
            group_menu_code: groupMenuInfo.group_menu_code,
            name: groupMenuInfo.name,
            url: groupMenuInfo.url,
            component_name: groupMenuInfo.component_name,
            permission: roleMenus2.length > 0 ? true : false,
            menus: new_menus,
          });
        } else {
          result_menus.push({
            group_menu_code: groupMenuInfo.group_menu_code,
            name: groupMenuInfo.name,
            url: groupMenuInfo.url,
            component_name: groupMenuInfo.component_name,
            permission: null,
            menus: new_menus,
          });
        }
      }
      var vResult = {
        role_id: roleInfo.role_id,
        name: roleInfo.name,
        description: roleInfo.description,
        status: roleInfo.status,
        status_boolean: roleInfo.status === "A" ? true : false,
        menus: result_menus,
      };
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async setRoleInfo(roleInfo: any) {
    return {
      role_id: roleInfo.role_id,
      name: roleInfo.name,
      description: roleInfo.description,
      status: roleInfo.status,
      status_boolean: roleInfo.status === "A" ? true : false,
    };
  }

  async createRole(
    name: string,
    description: string,
    status: string,
    menus: any,
    logOnId: string
  ) {
    try {
      const isExists = await this._roleRepository.isDuplicateByName(
        enums.mode.INSERT,
        name,
        undefined
      );
      if (isExists.length > 0) {
        throw new CustomError(
          enums.responseCode.Duplicate,
          enums.responseMessage.Duplicate
        );
      }
      const roleCreated = await this._roleRepository.create(
        name,
        description,
        status,
        logOnId
      );
      const roleInfo = roleCreated[0];
      for (let i = 0; i < menus.length; i++) {
        const menuInfo = menus[i];
        await this.updateRoleMenuByRoleId(
          roleInfo.role_id,
          menuInfo.group_menu_code,
          menuInfo.menu_code,
          menuInfo.permission,
          logOnId
        );
      }
      var result = enums.responseMessage.Success;
      return result;
    } catch (error) {
      throw error;
    }
  }

  async updateRole(
    roleId: number,
    name: string,
    description: string,
    status: string,
    menus: any,
    logOnId: string
  ) {
    try {
      const isExists = await this._roleRepository.getInfoById(roleId);
      if (isExists.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const isDuplicate = await this._roleRepository.isDuplicateByName(
        enums.mode.UPDATE,
        name,
        roleId
      );
      if (isDuplicate.length > 0) {
        throw new CustomError(
          enums.responseCode.Duplicate,
          enums.responseMessage.Duplicate
        );
      }
      await this._roleRepository.update(
        roleId,
        name,
        description,
        status,
        logOnId
      );
      if (status === "I") {
        //Nothing
      } else {
        for (let i = 0; i < menus.length; i++) {
          const menuInfo = menus[i];
          await this.updateRoleMenuByRoleId(
            roleId,
            menuInfo.group_menu_code,
            menuInfo.menu_code,
            menuInfo.permission,
            logOnId
          );
        }
      }
      var result = enums.responseMessage.Success;
      return result;
    } catch (error) {
      throw error;
    }
  }

  async deleteRole(roleId: number) {
    try {
      const isExists = await this._roleRepository.getInfoById(roleId);
      if (isExists.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      await this._roleMenuRepository.deleteByRoleId(roleId);
      await this._roleRepository.delete(roleId);
      var result = enums.responseMessage.Success;
      return result;
    } catch (error) {
      throw error;
    }
  }

  async getMasterMenu() {
    try {
      var result_menus = [];
      const group_menus = await this._groupMenuRepository.getList();
      for (let i = 0; i < group_menus.length; i++) {
        const groupMenuInfo = group_menus[i];
        const menus = await this._menuRepository.getListByGroupMenuCode(
          groupMenuInfo.group_menu_code
        );
        var new_menus = [];
        for (let j = 0; j < menus.length; j++) {
          const menuInfo = menus[j];
          new_menus.push({
            menu_code: menuInfo.menu_code,
            name: menuInfo.name,
            url: menuInfo.url,
            component_name: menuInfo.component_name,
            permission: false,
          });
        }
        if (new_menus.length <= 0) {
          result_menus.push({
            group_menu_code: groupMenuInfo.group_menu_code,
            name: groupMenuInfo.name,
            url: groupMenuInfo.url,
            component_name: groupMenuInfo.component_name,
            permission: false,
            menus: new_menus,
          });
        } else {
          result_menus.push({
            group_menu_code: groupMenuInfo.group_menu_code,
            name: groupMenuInfo.name,
            url: groupMenuInfo.url,
            component_name: groupMenuInfo.component_name,
            permission: null,
            menus: new_menus,
          });
        }
      }
      return result_menus;
    } catch (error) {
      throw error;
    }
  }

  async getMenuByUserId(userId: number) {
    try {
      var result_menus = [];
      var userRoles = await this._userRoleRepository.getActiveListByUserId(
        userId
      );
      if (userRoles.length <= 0) {
        return [];
      }
      const userRoleInfo = userRoles[0];
      const group_menus = await this._groupMenuRepository.getListByRoleId(
        userRoleInfo.role_id
      );
      for (let i = 0; i < group_menus.length; i++) {
        const groupMenuInfo = group_menus[i];
        const menus = await this._menuRepository.getListByGroupMenuCode(
          groupMenuInfo.group_menu_code
        );
        var new_menus = [];
        for (let j = 0; j < menus.length; j++) {
          const menuInfo = menus[j];
          const roleMenus =
            await this._roleMenuRepository.getInfoByRoleIdGroupMenuCodeMenuCode(
              userRoleInfo.role_id,
              groupMenuInfo.group_menu_code,
              menuInfo.menu_code
            );
          if (roleMenus.length > 0) {
            new_menus.push({
              menu_code: menuInfo.menu_code,
              title: menuInfo.name,
              path: menuInfo.url,
              element: menuInfo.component_name,
            });
          }
        }
        result_menus.push({
          group_menu_code: groupMenuInfo.group_menu_code,
          title: groupMenuInfo.name,
          path: groupMenuInfo.url,
          element: groupMenuInfo.component_name,
          items: new_menus,
        });
      }
      return result_menus;
    } catch (error) {
      throw error;
    }
  }

  async updateRoleMenuByRoleId(
    roleId: number,
    groupMenuCode: string,
    menuCode: string,
    permission: boolean,
    logOnId: string
  ) {
    try {
      if (permission) {
        var isExists = [];
        if (menuCode != undefined) {
          isExists =
            await this._roleMenuRepository.getInfoByRoleIdGroupMenuCodeMenuCode(
              roleId,
              groupMenuCode,
              menuCode
            );
        } else {
          isExists =
            await this._roleMenuRepository.getInfoByRoleIdGroupMenuCode(
              roleId,
              groupMenuCode
            );
        }
        if (isExists.length <= 0) {
          await this._roleMenuRepository.create(
            roleId,
            groupMenuCode,
            menuCode,
            logOnId
          );
        }
      } else {
        if (menuCode != undefined) {
          await this._roleMenuRepository.deleteByRoleIdGroupMenuCodeMenuCode(
            roleId,
            groupMenuCode,
            menuCode
          );
        } else {
          await this._roleMenuRepository.deleteByRoleIdGroupMenuCode(
            roleId,
            groupMenuCode
          );
        }
      }
      var result = enums.responseMessage.Success;
      return result;
    } catch (error) {
      throw error;
    }
  }

  async getCryptoWalletList() {
    try {
      const companies = await this._companyRepository.getList();
      if (companies.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }

      // get asset list from cactus

      // await this._assetInVaultAccountService.syncFireblocksAssetInVaults(
      //   enums.accountType.COMPANY,
      //   companies[0].company_id,
      //   undefined
      // );


      // const vaultAccountInfo = await this._vaultAccountService.getVaultAccount(
      //   enums.accountType.COMPANY,
      //   companies[0].company_id,
      //   undefined
      // );

      // const cactusWalletInfo = await this._cactusWalletRepository.getInfo(
      //   enums.accountType.COMPANY,
      //   companies[0].company_id,
      //   undefined
      // )

      // var vAssets = await this._assetInVaultAccountService.getCryptoWalletList(
      //   cactusWalletInfo.vault_account_id
      // );
      var result = {
        company_name: companies[0].name,
        account_name: "Lightbit",
        // assets: vAssets,
      };

      return result;
    } catch (error) {
      throw error;
    }
  }

  // async getTotalAssetPieChart(assetGroup: string) {
  //   try {
  //     await this.updateSummaryAssetBalance();

  //     const companies = await this._companyRepository.getList();
  //     if (companies.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     const summaryAssetCompany = await this._summaryRepository.getBySummaryKey(
  //       "SUM_" + assetGroup + "_COMPANY"
  //     );
  //     if (summaryAssetCompany.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     const summaryAssetCustomer =
  //       await this._summaryRepository.getBySummaryKey(
  //         "SUM_" + assetGroup + "_CUSTOMER"
  //       );
  //     if (summaryAssetCustomer.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     var vResult = [];
  //     var vCompanyAssetTotal = summaryAssetCompany[0].summary_value;
  //     var vCustomerAssetTotal = summaryAssetCustomer[0].summary_value;

  //     vResult.push({
  //       id: 1,
  //       label: "Company Wallet",
  //       value: vCompanyAssetTotal,
  //     });
  //     vResult.push({
  //       id: 2,
  //       label: "Customer Deposit",
  //       value: vCustomerAssetTotal,
  //     });
  //     var result = {
  //       asset_balance: vResult,
  //       total_balance: vCompanyAssetTotal + vCustomerAssetTotal,
  //       last_updated: summaryAssetCompany[0].updated_date,
  //     };
  //     return result;
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // async getTotalFiatPieChart() {
  //   try {
  //     await this.updateSummaryFiatBalance();

  //     var vResult = [];
  //     var vHoldTotal = 0;
  //     var vPayoutTotal = 0;

  //     const summaryFiatBalance = await this._summaryRepository.getBySummaryKey(
  //       "SUM_FIAT_BALANCE"
  //     );
  //     if (summaryFiatBalance.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     const summaryWithdrawPending =
  //       await this._summaryRepository.getBySummaryKey("SUM_WITHDRAW_PENDING");
  //     if (summaryWithdrawPending.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     vPayoutTotal = summaryWithdrawPending[0].summary_value;
  //     vHoldTotal = summaryFiatBalance[0].summary_value;

  //     vResult.push({
  //       id: 1,
  //       label: "Hold",
  //       value: vHoldTotal,
  //     });
  //     vResult.push({
  //       id: 2,
  //       label: "All Payout",
  //       value: vPayoutTotal,
  //     });
  //     var result = {
  //       fiat_balance: vResult,
  //       last_updated: summaryFiatBalance[0].updated_date,
  //     };
  //     return result;
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // async updateSummaryAssetBalance() {
  //   try {
  //     const assetGroup = "USDC";
  //     const companys = await this._companyRepository.getList();
  //     if (companys.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }
  //     var companyId = companys[0].company_id;

  //     const assetInfo = await this._assetRepository.getInfoByAssetGroup(
  //       assetGroup
  //     );
  //     if (assetInfo.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     const totalAssetCompany =
  //       await this._assetInVaultAccountRepository.getSummaryAssetForCompanyByAssetGroup(
  //         companyId,
  //         assetGroup
  //       );
  //     if (totalAssetCompany.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     var totalCom = 0;
  //     var totalCus = 0;

  //     if (totalAssetCompany[0].total != null) {
  //       totalCom = totalAssetCompany[0].total;
  //     }

  //     const updateSummaryAssetCompany =
  //       await this._summaryRepository.updateSummaryValue(
  //         "SUM_" + assetGroup + "_COMPANY",
  //         totalCom
  //       );
  //     if (updateSummaryAssetCompany.length <= 0) {
  //       const insertSummaryAssetCompany = await this._summaryRepository.create(
  //         "SUM_" + assetGroup + "_COMPANY",
  //         totalCom,
  //         "0"
  //       );
  //       if (insertSummaryAssetCompany.length <= 0) {
  //         throw new CustomError(
  //           enums.responseCode.NotFound,
  //           enums.responseMessage.NotFound
  //         );
  //       }
  //     }

  //     const totalAssetCustomer =
  //       await this._assetInVaultAccountRepository.getSummaryAssetForCustomerByAssetGroup(
  //         assetGroup
  //       );
  //     if (totalAssetCustomer.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     if (totalAssetCustomer[0].total != null) {
  //       totalCus = totalAssetCustomer[0].total;
  //     }

  //     const updateSummaryAssetCustomer =
  //       await this._summaryRepository.updateSummaryValue(
  //         "SUM_" + assetGroup + "_CUSTOMER",
  //         totalCus
  //       );
  //     if (updateSummaryAssetCustomer.length <= 0) {
  //       const insertSummaryAssetCompany = await this._summaryRepository.create(
  //         "SUM_" + assetGroup + "_CUSTOMER",
  //         totalCus,
  //         "0"
  //       );
  //       if (insertSummaryAssetCompany.length <= 0) {
  //         throw new CustomError(
  //           enums.responseCode.NotFound,
  //           enums.responseMessage.NotFound
  //         );
  //       }
  //     }
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // async updateSummaryFiatBalance() {
  //   try {
  //     const assetGroup = "USDC";
  //     //summary withdraw
  //     const totalWithdrawPending =
  //       await this._withdrawRepository.getTotalPendingAmount();
  //     if (totalWithdrawPending.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     const updateSummaryWithdraw =
  //       await this._summaryRepository.updateSummaryValue(
  //         "SUM_WITHDRAW_PENDING",
  //         totalWithdrawPending[0].total
  //       );
  //     if (updateSummaryWithdraw.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     //summary fiat
  //     const totalFiatBalance =
  //       await this._assetInVaultAccountRepository.getSummaryFiatBalanceForCompany(
  //         assetGroup
  //       );
  //     if (totalFiatBalance.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }

  //     const updateSummaryFiat =
  //       await this._summaryRepository.updateSummaryValue(
  //         "SUM_FIAT_BALANCE",
  //         totalFiatBalance[0].total
  //       );
  //     if (updateSummaryFiat.length <= 0) {
  //       throw new CustomError(
  //         enums.responseCode.NotFound,
  //         enums.responseMessage.NotFound
  //       );
  //     }
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async createTransaction(
    userId: number,
    type: string,
    assetCode: string,
    totalAssets: number,
    totalAmount: number,
    inputType: string,
    isManualMode: boolean,
    walletNetwork: string,
    walletAddress: string,
    logOnId: string
  ) {
    try {
      return await this._transactionService.createTransaction(
        userId,
        type,
        assetCode,
        totalAssets,
        totalAmount,
        inputType,
        isManualMode,
        walletNetwork,
        walletAddress,
        logOnId
      );
    } catch (error) {
      throw error;
    }
  }

  async getUserAssetInVaultAccountByUserId(userId: number) {
    try {
      const users = await this._userRepository.getActiveInfoById(userId);
      if (users.length <= 0) {
        throw new CustomError(
          enums.responseCode.UserNotFound,
          enums.responseMessage.UserNotFound
        );
      }
      return await this._cactusWalletAddressService.getAssetsInVaultAccountByUserId(
        // return await this._assetInVaultAccountService.getAssetsInVaultAccountByUserId(
        userId
      );
    } catch (error) {
      throw error;
    }
  }

  async getFiatAccounts() {
    try {
      await this._fiatAccountRepository.updateBalanceAllUsers();
      const fiatAccounts = await this._fiatAccountRepository.getListByAdmin();
      var vResult = [];
      for (let i = 0; i < fiatAccounts.length; i++) {
        vResult.push(await this.setFiatAccountInfo(fiatAccounts[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async setFiatAccountInfo(fiatAccountInfo: any) {
    return {
      fiat_id: fiatAccountInfo.fiat_id,
      user_id: fiatAccountInfo.user_id,
      user_full_name: fiatAccountInfo.user_full_name,
      total: fiatAccountInfo.total,
      available: fiatAccountInfo.available,
      pending: fiatAccountInfo.pending,
      lockedamount: fiatAccountInfo.lockedamount,
      is_favorite: fiatAccountInfo.is_favorite,
      is_favorite_boolean: fiatAccountInfo.is_favorite === "Y" ? true : false,
    };
  }

  async updateFavoriteFiatAccount(
    fiatId: number,
    isFavorite: string,
    logOnId: string
  ) {
    try {
      const isExists = await this._fiatAccountRepository.getInfoById(fiatId);
      if (isExists.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      await this._fiatAccountRepository.updateFavoriteFiatAccount(
        fiatId,
        isFavorite,
        logOnId
      );
      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  async getFiatAccountByUser(
    userId: number
  ) {
    try {
      const result = await this._customerService.getUserFiatAccounts(
        userId
      );
      return result
    } catch (error) {
      throw error;
    }
  }

  async createTopup(
    fiatId: number,
    amount: number,
    fileContent: any,
    logOnId: string
  ) {
    try {
      var fiatAccounts = await this._fiatAccountRepository.getInfoById(fiatId);
      if (fiatAccounts.length <= 0) {
        throw new CustomError(
          enums.responseCode.FiatAccountNotFound,
          enums.responseMessage.FiatAccountNotFound
        );
      }
      const fiatAccountInfo = fiatAccounts[0];

      const createdWalletCredit = await this._walletCreditRepository.create(
        await utils.generateKey(fiatId.toString()),
        fiatAccountInfo.user_id,
        enums.transactionType.TOPUP,
        amount,
        "USD",
        logOnId
      );
      const walletCreditInfo = createdWalletCredit[0];

      for (let index = 0; index < fileContent.length; index++) {
        const element = fileContent[index];
        const renameFileName = `${uuidv4()}-${element.file_name}`;
        const s3Location = await this._awsService.upload(
          process.env.S3_PATH!,
          renameFileName,
          element.file_data
        );
        await this._walletCreditDocumentRepository.create(
          walletCreditInfo.wallet_credit_id,
          renameFileName,
          s3Location,
          logOnId
        );
      }

      await this._fiatAccountRepository.updateBalanceAllUsers();

      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }

  async getTopupInfo(walletCreditId: number) {
    try {
      const walletCredits = await this._walletCreditRepository.getInfoById(
        walletCreditId
      );
      if (walletCredits.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const walletCreditInfo = walletCredits[0];

      var vTypeText = "";
      if (walletCreditInfo.type === enums.transactionType.TOPUP) {
        vTypeText = "Top-up Credit";
      }

      var vStatusText = "";
      if (walletCreditInfo.status === enums.transactionStatus.SUCCESS) {
        vStatusText = "Success";
      } else if (walletCreditInfo.status === enums.transactionStatus.FAILED) {
        vStatusText = "Failed";
      } else if (walletCreditInfo.status === enums.transactionStatus.PENDING) {
        vStatusText = "Pending";
      } else if (
        walletCreditInfo.status === enums.transactionStatus.CANCELLED
      ) {
        vStatusText = "Cancelled";
      } else if (walletCreditInfo.status === enums.transactionStatus.ONHOLD) {
        vStatusText = "On hold";
      }
      const walletCreditDocuments =
        await this._walletCreditDocumentRepository.getListByWalletCreditId(
          walletCreditId
        );

      var vDocuments = [];
      for (let i = 0; i < walletCreditDocuments.length; i++) {
        let element = walletCreditDocuments[i];
        vDocuments.push({
          wallet_credit_document_id: element.wallet_credit_document_id,
          file_name: element.file_name,
          url: element.url,
        });
      }
      return {
        wallet_credit_id: walletCreditInfo.wallet_credit_id,
        wallet_credit_date: walletCreditInfo.wallet_credit_date,
        wallet_credit_no: walletCreditInfo.wallet_credit_no,
        operator_id: walletCreditInfo.user_id,
        operator_by: walletCreditInfo.operator_by,
        type: walletCreditInfo.type,
        type_text: vTypeText,
        amount: walletCreditInfo.amount,
        currency_code: walletCreditInfo.currency_code,
        status: walletCreditInfo.status,
        status_text: vStatusText,
        documents: vDocuments,
      };
    } catch (error) {
      throw error;
    }
  }

  async downloadWalletCreditDocument(walletCreditDocumentId: number) {
    try {
      const walletCreditDocuments =
        await this._walletCreditDocumentRepository.getInfoById(
          walletCreditDocumentId
        );
      if (walletCreditDocuments.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const walletCreditDocumentInfo = walletCreditDocuments[0];

      return await this._awsService.download(
        process.env.S3_PATH!,
        walletCreditDocumentInfo.file_name
      );
    } catch (error) {
      throw error;
    }
  }

  async previewWalletCreditDocument(walletCreditDocumentId: number) {
    try {
      const walletCreditDocuments =
        await this._walletCreditDocumentRepository.getInfoById(
          walletCreditDocumentId
        );
      if (walletCreditDocuments.length <= 0) {
        throw new CustomError(
          enums.responseCode.NotFound,
          enums.responseMessage.NotFound
        );
      }
      const walletCreditDocumentInfo = walletCreditDocuments[0];

      return await this._awsService.preview(
        process.env.S3_PATH!,
        walletCreditDocumentInfo.file_name
      );
    } catch (error) {
      throw error;
    }
  }


  async getBankAccountsByBankType(userId: number, bankType: string) {
    try {

      const result = await this._customerService.getBankAccountsByBankType(
        userId,
        bankType
      );
      return result
    } catch (error) {
      throw error;
    }
  }


  async createWithdraw(
    senderSourceOfFund: string,
    senderSourceOfFundRemarks: string,
    purposeOfRemittance: string,
    purposeOfRemittanceRemarks: string,
    senderBeneficiaryRelationship: string,
    senderBeneficiaryRelationshipRemarks: string,
    transferAmount: number, //USD
    bankAccountId: number,
    userId: number,
    logOnId: string
  ) {
    try {
      const result = await this._customerService.createWithdraw(
        senderSourceOfFund,
        senderSourceOfFundRemarks,
        purposeOfRemittance,
        purposeOfRemittanceRemarks,
        senderBeneficiaryRelationship,
        senderBeneficiaryRelationshipRemarks,
        transferAmount,
        bankAccountId,
        userId,
        logOnId
      );

      return result
    } catch (error) {
      throw error;
    }
  }

  async createTransfer(
    senderSourceOfFund: string,
    senderSourceOfFundRemarks: string,
    purposeOfRemittance: string,
    purposeOfRemittanceRemarks: string,
    senderBeneficiaryRelationship: string,
    senderBeneficiaryRelationshipRemarks: string,
    transferAmount: number,
    bankAccountId: number,
    userId: number,
    logOnId: string
  ) {
    try {
      const result = await this._customerService.createTransfer(
        senderSourceOfFund,
        senderSourceOfFundRemarks,
        purposeOfRemittance,
        purposeOfRemittanceRemarks,
        senderBeneficiaryRelationship,
        senderBeneficiaryRelationshipRemarks,
        transferAmount,
        bankAccountId,
        userId,
        logOnId
      );
      return result
    } catch (error) {
      throw error;
    }
  }
}
