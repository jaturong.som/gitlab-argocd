import { singleton } from "tsyringe";
import { CactusWalletAddressRepository } from "../repositories/cactusWalletAddressRepository";

@singleton()
export class CactusWalletAddressService {
  readonly _cactusWalletAddressRepository: CactusWalletAddressRepository

  constructor(
    cactusWalletAddressRepository: CactusWalletAddressRepository,
  ) {
    this._cactusWalletAddressRepository = cactusWalletAddressRepository;
  }


  async getAssetsInVaultAccountByUserId(userId: number) {
    try {
      const assetInVaults =
        await this._cactusWalletAddressRepository.getListByUserId(userId);
      var vResult = [];
      for (let i = 0; i < assetInVaults.length; i++) {
        vResult.push(await this.setAssetInVaultAccountInfo(assetInVaults[i]));
      }
      return vResult;
    } catch (error) {
      throw error;
    }
  }

  async setAssetInVaultAccountInfo(assetInVaultInfo: any) {
    return {
      asset_in_vault_account_id: assetInVaultInfo.cactus_wallet_address_id,
      network_id: assetInVaultInfo.network_id,
      network_code: assetInVaultInfo.network_code,
      network_name: assetInVaultInfo.network_name,
      network_image: assetInVaultInfo.network_image,
      asset_id: assetInVaultInfo.asset_id,
      asset_code: assetInVaultInfo.asset_code,
      asset_name: assetInVaultInfo.asset_name,
      asset_image: assetInVaultInfo.asset_image,
      asset_address: assetInVaultInfo.asset_address,
      total: assetInVaultInfo.total,
      lockedamount: assetInVaultInfo.lockedamount,
      available: assetInVaultInfo.available,
      pending: assetInVaultInfo.pending,
      total_selling_amount: assetInVaultInfo.total_selling_amount,
      buying: assetInVaultInfo.buying,
      selling: assetInVaultInfo.selling,
      status: assetInVaultInfo.status,
      updated_by: assetInVaultInfo.updated_by,
      updated_date: assetInVaultInfo.updated_date,
    };
  }
}
