import { HttpLogRepository } from "../repositories/httpLogRepository";
import { singleton } from "tsyringe";

@singleton()
export class HttpLogService {
  readonly _httpLogRepository: HttpLogRepository;
  constructor(httpLogRepository: HttpLogRepository) {
    this._httpLogRepository = httpLogRepository;
  }

  async createHttpLog(
    requester: string,
    remoteAddress: string,
    method: string,
    url: string,
    header: string,
    body: string,
    response: string
  ) {
    try {
      await this._httpLogRepository.create(
        requester,
        remoteAddress,
        method,
        url,
        header,
        body,
        response
      );
      return "Success";
    } catch (error) {
      throw error;
    }
  }
}
