import * as enums from "../utilities/enums";
import CustomError from "../middlewares/customError";
import { singleton } from "tsyringe";
import { CactusWalletRepository } from "../repositories/cactusWalletRepository";
import { CactusWalletAddressRepository } from "../repositories/cactusWalletAddressRepository";
import { CactusCustodyService } from "./cactusCustodyService";
import { AssetRepository } from "../repositories/assetRepository";

@singleton()
export class CactusWalletService {

  readonly _cactusWalletRepository: CactusWalletRepository
  readonly _cactusWalletAddressRepository: CactusWalletAddressRepository
  readonly _cactusCustodyService: CactusCustodyService
  readonly _assetRepository: AssetRepository

  constructor(
    cactusWalletAddressRepository: CactusWalletAddressRepository,
    cactusWalletRepository: CactusWalletRepository,
    cactusCustodyService: CactusCustodyService,
    assetRepository: AssetRepository,
  ) {
    this._cactusWalletAddressRepository = cactusWalletAddressRepository;
    this._cactusWalletRepository = cactusWalletRepository
    this._cactusCustodyService = cactusCustodyService;
    this._assetRepository = assetRepository;
  }

  async getWallets() {
    try {
      throw new CustomError(
        enums.responseCode.NotFound,
        enums.responseMessage.NotFound
      );
    } catch (error) {
      throw error;
    }
  }

  async createWalletAccount(
    type: string,
    logOnId: string,
    companyId?: number,
    userId?: number
  ) {
    try {
      const isExists = await this._cactusWalletRepository.getInfo(
        type,
        companyId,
        userId
      );
      if (isExists.length > 0) {
        throw new CustomError(
          enums.responseCode.Duplicate,
          enums.responseMessage.Duplicate
        );
      }

      // create new address
      var newAddressResponse = await this._cactusCustodyService.applyNewAddress(
        process.env.BID_CUSTOMER!,
        process.env.WALLET_CODE_CUSTOMER!,
        1,
        "NORMAL_ADDRESS");
      var walletAddress = newAddressResponse.data[0]
      // var walletAddress = "0x4e62098a78E7E367712987ff8A9f7413e43fd88f"

      //get address info
      var addressInfoResponse = await this._cactusCustodyService.getAddressInfo(
        process.env.BID_CUSTOMER!,
        process.env.WALLET_CODE_CUSTOMER!,
        walletAddress
      )
      var addressInfo = addressInfoResponse.data

      //get wallet info
      var walletInfoResponse = await this._cactusCustodyService.getWalletInfo(
        process.env.BID_CUSTOMER!,
        process.env.WALLET_CODE_CUSTOMER!
      )
      var walletInfo = walletInfoResponse.data

      //save 
      var cactusWallet = await this._cactusWalletRepository.create(
        type,
        process.env.BID_COMPANY!,
        walletInfo.business_name,
        process.env.WALLET_CODE_CUSTOMER!,
        walletInfo.wallet_type,
        walletInfo.storage_type,
        logOnId,
        companyId,
        userId
      );

      //get wallet list
      var walletListResponse = await this._cactusCustodyService.getWalletList(process.env.WALLET_CODE_CUSTOMER!)
      var walletList = walletListResponse.data.list

      //get asset company
      var assetCompanyList = await this._assetRepository.getAssetList()


      for (let i = 0; i < assetCompanyList.length; i++) {
        for (let j = 0; j < walletList.length; j++) {

          if (assetCompanyList[i].code == walletList[j].coin_name) {
            await this._cactusWalletAddressRepository.create(
              cactusWallet[0].cactus_wallet_id,
              assetCompanyList[i].network_code,
              assetCompanyList[i].code,
              walletAddress,
              addressInfo.address_type,
              addressInfo.address_storage,
              0,
              0,
              0,
              0,
              'A',
              logOnId
            )
          }
        }
      }

      return enums.responseMessage.Success;
    } catch (error) {
      throw error;
    }
  }
}
