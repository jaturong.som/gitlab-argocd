export interface RequestRegister {
  email: string;
  password: string;
  first_name: string;
  middle_name?: string;
  last_name: string;
  tel?: string;
}

export interface RequestLogin {
  user_name: string;
  password: string;
}

export interface ResponseLogin {
  user_id: string;
  code: string;
  iat: number;
  exp: number;
}

export interface RequestRefreshToken {
  refresh_token: string;
}
