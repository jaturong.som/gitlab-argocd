export interface RequestGetWalletInfo {
  wallet_code: string;
}

export interface RequestCreateWallet {
  wallet_type: string;
  number: number;
}

export interface RequestApplyNewAddress {
  address_num: number;
  address_type: string;
  coin_name?: string;
}

export interface RequestEditAddress {
  description: string;
}

export interface RequestVerifyAddressFormat {
  addresses: string[];
  coin_name: string;
}

export interface RequestEstimateGasFee {
  from_address: string;
  from_wallet_code: string;
  description: string;
  amount: number;
  dest_address: string;
  memo_type: string;
  memo: string;
  remark: string;
}

export interface RequestCreateWithdrawalOrder {
  from_address: string;
  from_wallet_code: string;
  coin_name: string;
  description: string;
  amount: number;
  dest_address: string;
  memo_type: string;
  memo: string;
  remark: string;
}

export interface RequestEditTransactionDetailRemark {
  remark: string;
}

export interface RequestRBF {
  level: string;
}

export interface RequestCancelOrder {
  level: string;
}
