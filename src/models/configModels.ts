export interface RequestGetAssetsInVaultAccount {}

export interface RequestCreateAssetInVaultAccount {
  asset_code: string;
}

export interface RequestGetAssetRates {}

export interface RequestCreateAssetRate {
  asset_code: string;
  buying: number;
  selling: number;
}

export interface RequestUpdateAssetRate {
  buying: number;
  selling: number;
}

export interface RequestGetCompanyVaultAccounts {}

export interface RequestCreateCompanyVaultAccount {
  name: string;
}

export interface RequestGetCoins {}

export interface RequestGetNetworks {}

export interface RequestGetAssets {}
