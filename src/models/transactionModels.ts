export interface RequestGetAdminTransactions {}

export interface RequestSubmitTransaction {}

// export interface RequestGetAdminTransactionLogs {
//   page_size: number;
//   page_cursor: number;
//   transaction_type?: string;
//   status?: string;
//   from_date?: string;
//   to_date?: string;
//   asset?: string;
// }

export interface RequestGetFireblocksNotificationList {
  from_date: string;
  to_date: string;
  status?: string;
  search_text?: string;
}

export interface RequestManualUpdateTransactionStatus {
  fireblock_transaction_id: string;
}

export interface RequestManualFreezeTransaction {
  fb_transaction_id: string;
}

export interface RequestManualUnfreezeTransaction {
  fb_transaction_id: string;
}
