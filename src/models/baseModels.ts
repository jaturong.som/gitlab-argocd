export interface LogonInfo {
  user_id: string;
}

export interface ResponseResult {
  code: number;
  message: string;
  result?: any;
}
