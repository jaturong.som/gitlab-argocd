export interface RequestGetCatalogue {
  catalogue_type: string;
  additional_field_1: string;
}

export interface RequestGetBankList {
  country_code: string;
}

export interface RequestGetExchangeRate {
  transfer_amount: number;
  calc_by: string;
  payout_currency: string;
  payment_mode: string;
  bank_id: string;
  payout_country: string;
}

export interface SendTransaction {
  sender_source_of_fund: string;
  sender_source_of_fund_remarks: string;
  sender_beneficiary_relationship: string;
  sender_beneficiary_relationship_remarks: string;
  purpose_of_remittance: string;
  purpose_of_remittance_remarks: string;
  calc_by: string;
  transfer_amount: number;
  remit_currency: string;
  customer_id: string;
  receiver_id: string;
}

export interface CommitTransaction {
  transaction_id: string;
}

export interface QueryTransaction {
  pin_number: string;
}

export interface RequestGetPurposeTransfer { }

export interface RequestGetRelationToReceiver { }

export interface RequestGetSourceOfFund { }

export interface RequestGetOccupation { }

export interface RequestGetDocumentType { }

export interface RequestGetNationalityList { }

export interface RequestGetCountryList { }

export interface RequestCallback {
  agentTxnId: string;
  pinNumber: string;
  status: string;
  message: string;
  notification_ts: string;
}

export interface LqnRequestCreateReceiver {
  receiverFirstName: string;
  receiverMiddleName: string;
  receiverLastName: string;
  receiverAddress: string;
  receiverState: string;
  receiverAreaTown: string;
  receiverCity: string;
  receiverZipCode: string;
  receiverDateOfBirth: string;
  receiverCountry: string;
  receiverNationality: string;
  receiverIdType: string;
  receiverIdNumber: string;
  receiverEmail: string;
  receiverAccountType: string;
  receiverContactNumber: string;
  paymentMode: string;
  bankName: string;
  bankBranchName: string;
  bankBranchCode: string;
  bankAccountNumber: string;
  beneficiaryType: string;
  receiverOccupation: string;
  receiverIdIssueDate: string;
  receiverIdExpireDate: string;
  receiverDistrict: string;
  locationId: string;
  payoutCurrency: string;
  customerId: string;
}

export interface LqnRequestCreateSender {
  userId: string;
  senderType: string;
  senderFirstName: string;
  senderMiddleName: string;
  senderLastName: string;
  senderGender: string;
  senderAddress: string;
  senderCity: string;
  senderState: string;
  senderZipCode: string;
  senderCountry: string;
  senderMobile: string;
  senderNationality: string;
  senderIdType: string;
  senderIdNumber: string;
  senderIdIssueCountry: string;
  senderIdIssueDate: string;
  senderIdExpireDate: string;
  senderDateOfBirth: string;
  senderOccupation: string;
  senderSecondaryIdType: string;
  senderSecondaryIdNumber: string;
  senderEmail: string;
  senderNativeFirstname: string;
  senderNativeMiddlename: string;
  senderNativeLastname: string;
  senderNativeAddress: string;
  senderOccupationRemarks: string;
  senderIdTypeRemarks: string;
  authorizedPersonIdType: string;
  authorizedPersonIdNumber: string;
  authorizedPersonFirstName: string;
  authorizedPersonLastName: string;
  invoiceOrPayrollNumber: string;
}
