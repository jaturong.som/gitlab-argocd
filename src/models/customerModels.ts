export interface RequestGetAssets {}
export interface RequestGetUserApiKeys {}

export interface RequestCreateUserApiKey {
  name: string;
}

export interface RequestGetUserFiatAccounts {}

export interface RequestCreateUserVaultAccount {
  name: string;
}

// export interface RequestCreateUserAssetInVaultAccount {
//   asset_code: string;
// }

// export interface RequestGetBankAccounts {}

export interface RequestCreateBankAccount {
  bank_type: string;
  receiver_first_name?: string;
  receiver_middle_name?: string;
  receiver_last_name?: string;
  receiver_date_of_birth?: string; // Required: China
  receiver_gender?: string; // Required: China
  receiver_contact_number?: string; // Required: China, Singapore
  receiver_email?: string;
  receiver_nationality?: string;
  receiver_occupation?: string; // Required: China
  receiver_occupation_remarks?: string;
  receiver_id_type?: string; // Required: China
  receiver_id_type_remarks?: string;
  receiver_id_number?: string; // Required: China
  receiver_id_issue_date?: string; // Required: China
  receiver_id_expire_date?: string; // Required: China
  receiver_address?: string;
  receiver_state?: string;
  receiver_area_town?: string;
  receiver_city?: string;
  receiver_zip_code?: string;
  receiver_country: string;
  receiver_native_first_name?: string;
  receiver_native_middle_name?: string;
  receiver_native_last_name?: string;
  receiver_native_address?: string;
  receiver_account_type?: string;
  receiver_district?: string;
  beneficiary_type?: string;
  location_id: string;
  bank_name: string;
  bank_branch_name: string;
  bank_branch_code: string;
  bank_account_number: string;
  swift_code?: string;
  iban?: string;
}

// export interface RequestCreateOtherBankAccount {
//   bank_code: string;
//   bank_name: string;
//   branch_code?: string;
//   account_no: string;
//   account_name: string;
//   swift_code?: string;
//   iban?: string;
//   form_name: string;
//   first_name: string;
//   last_name: string;
//   nationality?: string;
//   date_of_birth?: string;
//   mobile_number?: string;
//   address?: {
//     line1?: string;
//     line2?: string;
//     city?: string;
//     state?: string;
//     country_code?: string;
//     postcode?: string;
//   };
// }

// export interface RequestUpdateOwnerBankAccount {
//   bank_code: string;
//   bank_name: string;
//   account_no: string;
//   account_name: string;
//   swift_code: string;
//   iban: string;
// }

// export interface RequestUpdateOtherBankAccount {
//   bank_code: string;
//   bank_name: string;
//   branch_code?: string;
//   account_no: string;
//   account_name: string;
//   swift_code?: string;
//   iban?: string;
//   first_name: string;
//   last_name: string;
//   nationality?: string;
//   date_of_birth?: string;
//   mobile_number?: string;
//   address?: {
//     address_id?: number;
//     line1?: string;
//     line2?: string;
//     city?: string;
//     state?: string;
//     country_code?: string;
//     postcode?: string;
//   };
// }

// export interface RequestUpdateDefaultBankAccount { }

export interface RequestGetWithdraws {}

export interface RequestCreateWithdraw {
  sender_source_of_fund: string;
  sender_source_of_fund_remarks?: string;
  purpose_of_remittance: string;
  purpose_of_remittance_remarks?: string;
  sender_beneficiary_relationship: string;
  sender_beneficiary_relationship_remarks?: string;
  transfer_amount: number;
  bank_account_id: number;
}

export interface RequestCreateTransfer {
  sender_source_of_fund: string;
  sender_source_of_fund_remarks?: string;
  purpose_of_remittance: string;
  purpose_of_remittance_remarks?: string;
  sender_beneficiary_relationship: string;
  sender_beneficiary_relationship_remarks?: string;
  transfer_amount: number;
  bank_account_id: number;
}

export interface RequestGetCryptoWalletListForUser {}

export interface RequestCreateAssetInVaultAccount {
  asset_code: string;
}

export interface RequestCreateTransaction {
  type: string;
  asset_code: string;
  total_assets: number;
  total_amount: number;
  input_type: string;
}

export interface RequestGetTransactions {}

export interface RequestRequestAccessAsset {
  asset_code: string;
}

export interface RequestUpdateCustomerProfile {
  first_name?: string;
  middle_name?: string;
  last_name?: string;
  date_of_birth?: string;
  nationality?: string;
  tel?: string;
  gender: string;
  occupation: string;
  occupation_remark?: string;
  country_code: string;
  document_type: string;
  document_type_remark?: string;
  document_text: string;
  document_issue_date?: string;
  document_expired_date?: string;
}

export interface RequestUpdateCustomerAddress {
  line1?: string;
  line2?: string;
  city?: string;
  state?: string;
  country_code?: string;
  postcode?: string;
  area_town?: string;
}

export interface RequestForgotPassword {
  email: string;
}

export interface RequestUpdatePassword {
  password: string;
}
