export interface RequestGetUsers { }
export interface ResponseGetUsers { }

export interface RequestGetUserById {
  user_id: number;
}

export interface RequestGetActiveUserById {
  user_id: number;
}

export interface RequestGetUserByUserName {
  user_name: string;
}

export interface RequestCreateUser {
  password: string;
  first_name: string;
  middle_name?: string;
  last_name: string;
  date_of_birth: string;
  email: string;
  tel: string;
  country_code: string;
  status: string;
  role_id: number;
  document_type: string;
  document_type_remark?: string;
  document_text: string;
  address: {
    line1: string;
    line2?: string;
    city: string;
    state: string;
    country_code: string;
    postcode: string;
    area_town?: string;
  };
  gender: string;
  nationality: string;
  occupation: string;
  occupation_remark?: string;
  document_issue_date?: string;
  document_expired_date?: string;
}

export interface RequestUpdateUser {
  first_name: string;
  middle_name?: string;
  last_name: string;
  date_of_birth: string;
  tel: string;
  country_code: string;
  status: string;
  role_id: number;
  document_type: string;
  document_type_remark?: string;
  document_text: string;
  address: {
    line1: string;
    line2?: string;
    city: string;
    state: string;
    country_code: string;
    postcode: string;
    area_town?: string;
  };
  gender: string;
  nationality: string;
  occupation: string;
  occupation_remark?: string;
  document_issue_date?: string;
  document_expired_date?: string;
}

export interface RequestDeleteUser {
  user_id: number;
}

export interface RequestUpdateUserToken {
  user_id: number;
  token: string;
}

export interface RequestGetRoles { }

export interface RequestGetActiveRoles { }

export interface RequestGetRoleById {
  role_id: number;
}

export interface RequestCreateRole {
  name: string;
  description?: string;
  status: string;
  menus: RoleMenuPermission[];
}

export interface RequestUpdateRole {
  name: string;
  description?: string;
  status: string;
  menus: RoleMenuPermission[];
}

export interface RoleMenuPermission {
  group_menu_code: string;
  menu_code?: string;
  permission: boolean;
}

export interface RequestDeleteRole {
  role_id: number;
}

export interface RequestGetMenuByUserId {
  user_id: number;
}

export interface RequestUpdateRoleMenuByRoleId {
  group_menu_code: string;
  menu_code?: string;
  permission: boolean;
}

export interface RequestChangePassword {
  old_password: string;
  new_password: string;
}

export interface RequestResetPassword {
  user_id: number;
  new_password: string;
}

export interface RequestGetMasterMenu { }

export interface RequestGetMasterMenuByRoleId { }

export interface RequestGetCryptoWalletList { }

export interface RequestGetTotalAssetPieChart { }

export interface RequestGetTotalFiatPieChart { }

export interface RequestUpdateSummary { }

export interface RequestCreateTransaction {
  user_id: number;
  type: string;
  asset_code: string;
  total_assets: number;
  total_amount: number;
  input_type: string;
  is_manual_mode: boolean;
  wallet_network: string;
  wallet_address: string;
}

export interface RequestUpdateFavoriteFiatAccounts {
  is_favorite: string;
}

export interface RequestCreateTopup {
  fiat_id: number;
  amount: number;
}

export interface RequestCreateWithdraw {
  user_id: number;
  sender_source_of_fund: string;
  sender_source_of_fund_remarks?: string;
  purpose_of_remittance: string;
  purpose_of_remittance_remarks?: string;
  sender_beneficiary_relationship: string;
  sender_beneficiary_relationship_remarks?: string;
  transfer_amount: number;
  bank_account_id: number;
}

export interface RequestCreateTransfer {
  user_id: number;
  sender_source_of_fund: string;
  sender_source_of_fund_remarks?: string;
  purpose_of_remittance: string;
  purpose_of_remittance_remarks?: string;
  sender_beneficiary_relationship: string;
  sender_beneficiary_relationship_remarks?: string;
  transfer_amount: number;
  bank_account_id: number;
}

export interface RequestCreateBankAccount {
  user_id: number;
  bank_type: string;
  receiver_first_name?: string;
  receiver_middle_name?: string;
  receiver_last_name?: string;
  receiver_date_of_birth?: string; // Required: China
  receiver_gender?: string; // Required: China
  receiver_contact_number?: string; // Required: China, Singapore
  receiver_email?: string;
  receiver_nationality?: string;
  receiver_occupation?: string; // Required: China
  receiver_occupation_remarks?: string;
  receiver_id_type?: string; // Required: China
  receiver_id_type_remarks?: string;
  receiver_id_number?: string; // Required: China
  receiver_id_issue_date?: string; // Required: China
  receiver_id_expire_date?: string; // Required: China
  receiver_address?: string;
  receiver_state?: string;
  receiver_area_town?: string;
  receiver_city?: string;
  receiver_zip_code?: string;
  receiver_country: string;
  receiver_native_first_name?: string;
  receiver_native_middle_name?: string;
  receiver_native_last_name?: string;
  receiver_native_address?: string;
  receiver_account_type?: string;
  receiver_district?: string;
  beneficiary_type?: string;
  location_id: string;
  bank_name: string;
  bank_branch_name: string;
  bank_branch_code: string;
  bank_account_number: string;
  swift_code?: string;
  iban?: string;
}