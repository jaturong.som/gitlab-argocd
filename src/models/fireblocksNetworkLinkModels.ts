export interface RequestGetAllAccountTypes {}

export interface RequestGetDepositWalletAddress {
  accountType: string;
  coinSymbol: string;
  network: string;
}

export interface RequestCreateDepositWalletAddress {
  accountType: string;
  coinSymbol: string;
  network: string;
}

export interface RequestGetWithDrawalFee {
  transferAmount: number;
  coinSymbol: string;
  network: string;
}

export interface RequestCreateWithDraw {
  amount: number;
  maxFee?:  string;
  coinSymbol: string;
  network: string;
  accountType: string;
  toAddress: string;
  tag?: string;
  isGross: boolean;
}

export interface RequestGetTransactionById {
  transactionID: string;
}

export interface RequestGetTransactionByHash {
  txHash: string;
  network: string;
}

export interface RequestGetTransactionHistory {
  fromDate: string;
  toDate: string;
  pageSize: string;
  pageCursor: string;
  isSubTransfer: string;
  coinSymbol: string;
  network: string;
}

export interface RequestGetSupportedAssets {}

export interface RequestCreateSubMainTransfer {
  subAccountID: string;
  direction: string;
  coinSymbol: string;
  amount: string;
}

export interface RequestCreateSubAccountsTransfer {
  srcSubAccountID: string;
  dstSubAccountID: string;
  coinSymbol: string;
  amount: string;
}

export interface RequestCreateInternalTransfer {
  fromAccountType: string;
  toAccountType: string;
  coinSymbol: string;
  amount: string;
}
