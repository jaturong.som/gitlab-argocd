import { CronJob } from "cron";
import { AssetRepository } from "../repositories/assetRepository";
import { JobSchedulerLogRepository } from "../repositories/jobSchedulerLogRepository";
import { ChainalysisTransactionRepository } from "../repositories/chainalysisTransactionRepository";
import { FetchLogRepository } from "../repositories/fetchLogRepository";
import { VaultService } from "../services/vaultService";
import { FireblocksService } from "../services/fireblocksService";
import { HttpService } from "../services/httpService";
import { ChainalysisService } from "../services/chainalysisService";
import * as enums from "../utilities/enums";
import { singleton } from "tsyringe";

@singleton()
export class CronJobService {
  readonly _assetRepository: AssetRepository;
  readonly _jobSchedulerLogRepository: JobSchedulerLogRepository;
  readonly _chainalysisTransactionRepository: ChainalysisTransactionRepository;
  readonly _fetchLogRepository: FetchLogRepository;
  readonly _vaultService: VaultService;
  readonly _fireblocksService: FireblocksService;
  readonly _httpService: HttpService;
  readonly _chainalysisService: ChainalysisService;
  readonly _cronJob1: CronJob;
  constructor() {
    this._assetRepository = new AssetRepository();
    this._jobSchedulerLogRepository = new JobSchedulerLogRepository();
    this._chainalysisTransactionRepository =
      new ChainalysisTransactionRepository();
    this._fetchLogRepository = new FetchLogRepository();
    this._vaultService = new VaultService();
    this._fireblocksService = new FireblocksService(this._vaultService);
    this._httpService = new HttpService(this._fetchLogRepository);
    this._chainalysisService = new ChainalysisService(
      this._chainalysisTransactionRepository,
      this._assetRepository,
      this._vaultService,
      this._fireblocksService,
      this._httpService
    );

    this._cronJob1 = new CronJob(
      process.env.CRON_JOB_UPDATE_CHAINALYSIS_TRANSACTION!,
      async () => {
        const jobSchedulerLogs =
          await this._jobSchedulerLogRepository.logStartJob(
            enums.jobMethod.UPDATE_CHAINALYSIS_TRANSACTIONS
          );
        try {
          const timeElapsed = Date.now();
          const today = new Date(timeElapsed);
          console.log(
            today.toISOString(),
            ": Running update chainalysis transactions"
          );

          await this.updateChainalysisTransactions();

          const endTimeElapsed = Date.now();
          const endToday = new Date(endTimeElapsed);
          console.log(
            endToday.toISOString(),
            ": Finish running update chainalysis transactions"
          );
          await this._jobSchedulerLogRepository.logEndJob(
            jobSchedulerLogs[0].job_scheduler_log_id,
            "Finish running " + enums.jobMethod.UPDATE_CHAINALYSIS_TRANSACTIONS
          );
        } catch (e) {
          console.error(e);
          if (e instanceof Error) {
            await this._jobSchedulerLogRepository.logEndJob(
              jobSchedulerLogs[0].job_scheduler_log_id,
              e?.message
            );
          }
        }
      },
      null,
      false
    );
  }
  async startJob() {
    if (!this._cronJob1.running) {
      this._cronJob1.start();
    }
  }
  async stopJob() {
    if (this._cronJob1.running) {
      this._cronJob1.stop();
    }
  }

  async updateChainalysisTransactions() {
    try {
      const chainalysisTransactions =
        await this._chainalysisTransactionRepository.getPendingList();
      for (let index = 0; index < chainalysisTransactions.length; index++) {
        const element = chainalysisTransactions[index];
        await this._chainalysisService.updateTransactionStatus(
          element.external_id
        );
      }
    } catch (error) {
      console.log(error);
    }
  }
}
