import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class AssetRateRepository {
  constructor() {}
  async getList() {
    try {
      const sql = `
        select asset_rates.asset_rate_id
          , networks.network_id
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , asset_rates.buying
          , asset_rates.selling
          , CONCAT(users.first_name,' ', users.last_name) as updated_by
          , asset_rates.updated_date
        from asset_rates
          inner join assets on assets.code = asset_rates.asset_code
          inner join networks on networks.network_id = assets.network_id
          left join users on users.user_id = NULLIF(asset_rates.updated_by, '')::int
        order by networks.code, assets.code`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getListByNetworkId(networkId: number) {
    try {
      const sql = `
        select asset_rates.asset_rate_id
          , networks.network_id
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , asset_rates.buying
          , asset_rates.selling
          , CONCAT(users.first_name,' ', users.last_name) as updated_by
          , asset_rates.updated_date
        from asset_rates
          inner join assets on assets.code = asset_rates.asset_code
          inner join networks on networks.network_id = assets.network_id
          left join users on users.user_id = NULLIF(asset_rates.updated_by, '')::int
        where assets.network_id = $1
        order by networks.code, assets.code`;
      return await db.query(sql, [networkId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoById(assetRateId: number) {
    try {
      const sql = `
        select asset_rates.asset_rate_id
          , networks.network_id
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , asset_rates.buying
          , asset_rates.selling
        from asset_rates
          inner join assets on assets.code = asset_rates.asset_code
          inner join networks on networks.network_id = assets.network_id
        where asset_rates.asset_rate_id = $1`;
      return await db.query(sql, [assetRateId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByAssetCode(assetCode: string) {
    try {
      const sql = `
        select asset_rates.asset_rate_id
          , networks.network_id
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
          , assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , asset_rates.buying
          , asset_rates.selling
        from asset_rates
          inner join assets on assets.code = asset_rates.asset_code
          inner join networks on networks.network_id = assets.network_id
        where assets.code = $1`;
      return await db.query(sql, [assetCode]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    assetId: number,
    assetCode: string,
    buying: number,
    selling: number,
    logOnId: string
  ) {
    try {
      const sql = `
        INSERT INTO asset_rates(asset_id, asset_code, buying, selling, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, CURRENT_TIMESTAMP, $5, CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [
        assetId,
        assetCode,
        buying,
        selling,
        logOnId,
      ]);
    } catch (error) {
      throw error;
    }
  }

  async update(
    assetRateId: number,
    buying: number,
    selling: number,
    logOnId: string
  ) {
    try {
      const sql = `
        UPDATE asset_rates
        SET buying = $2
            , selling = $3
            , updated_by = $4
            , updated_date = CURRENT_TIMESTAMP
        WHERE asset_rate_id = $1
        RETURNING *`;
      return await db.query(sql, [assetRateId, buying, selling, logOnId]);
    } catch (error) {
      throw error;
    }
  }

  async delete(assetRateId: number) {
    try {
      const sql = `
        DELETE FROM asset_rates
        WHERE asset_rate_id = $1`;
      return await db.query(sql, [assetRateId]);
    } catch (error) {
      throw error;
    }
  }
}
