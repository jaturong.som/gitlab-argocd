import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class GasConfigurationRepository {
  constructor() {}
  async getList() {
    try {
      const sql = `
      SELECT *
      FROM gas_configurations
      WHERE status = 'A'`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByAssetCode(assetCode: string) {
    try {
      const sql = `
      SELECT *
      FROM gas_configurations
      WHERE status = 'A'
      and asset_code = $1`;
      return await db.query(sql, [assetCode]);
    } catch (error) {
      throw error;
    }
  }
}
