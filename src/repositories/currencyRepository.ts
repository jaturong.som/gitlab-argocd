import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class CurrencyRepository {
  constructor() {}
  async getList() {
    try {
      const sql = `
      SELECT *
      FROM currencies
      WHERE status = 'A'
      ORDER BY code`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }
}
