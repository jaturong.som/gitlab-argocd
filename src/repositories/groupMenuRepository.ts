import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class GroupMenuRepository {
  constructor() {}
  async getList() {
    try {
      const sql = `
        SELECT *
        FROM group_menus
        WHERE status = 'A'
        ORDER BY "order"`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getListByRoleId(roleId: number) {
    try {
      const sql = `
        SELECT *
        FROM group_menus
        WHERE status = 'A'
        AND group_menu_code in (
          SELECT group_menu_code
          FROM role_menus
          WHERE role_id = $1
          GROUP BY group_menu_code)
        ORDER BY "order"`;
      return await db.query(sql, [roleId]);
    } catch (error) {
      throw error;
    }
  }
}
