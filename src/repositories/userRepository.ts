import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class UserRepository {
  constructor() { }
  async getList() {
    try {
      const sql = `
        SELECT users.*, (select role_id from user_roles where user_roles.user_id = users.user_id limit 1) as role_id
        FROM users
        ORDER BY users.user_id`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getActiveList() {
    try {
      const sql = `
      select user_id ,  CONCAT(first_name , ' ', last_name ) as "full_name"
      from users
      where status = 'A'
      order by first_name`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getInfoById(userId: number) {
    try {
      const sql = `
        SELECT users.*
          , (select role_id from user_roles where user_roles.user_id = users.user_id limit 1) as role_id
          , countries."name" as country_name
        FROM users
        left join countries on countries.code = users.country_code
                  and countries.status = 'A'
        WHERE user_id = $1`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByUserName(userName: string) {
    try {
      const sql = `
        SELECT *
        FROM users
        WHERE user_name = $1`;
      return await db.query(sql, [userName]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByEmail(email: string) {
    try {
      const sql = `
        SELECT *
        FROM users
        WHERE email = $1`;
      return await db.query(sql, [email]);
    } catch (error) {
      throw error;
    }
  }

  async getLoginInfoByUserName(userName: string) {
    try {
      const sql = `
        SELECT *
        FROM users
        WHERE lower(user_name) = lower($1)`;
      return await db.query(sql, [userName]);
    } catch (error) {
      throw error;
    }
  }

  async getActiveInfoById(userId: number) {
    try {
      const sql = `
        SELECT users.*, (select role_id from user_roles where user_roles.user_id = users.user_id limit 1) as role_id
        FROM users
        WHERE status = 'A'
        AND user_id = $1`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }

  async getActiveInfoByUserName(userName: string) {
    try {
      const sql = `
        SELECT *
        FROM users
        WHERE status = 'A'
        AND user_name = $1`;
      return await db.query(sql, [userName]);
    } catch (error) {
      throw error;
    }
  }

  async getActiveLoginInfoByUserName(userName: string) {
    try {
      const sql = `
        SELECT *
        FROM users
        WHERE status = 'A'
        AND lower(user_name) = lower($1)`;
      return await db.query(sql, [userName]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    code: string,
    type: string,
    password: string,
    firstName: string,
    middleName: string,
    lastName: string,
    email: string,
    tel: string,
    status: string,
    logOnId: string,
    gender: string,
    nationality: string,
    occupation: string,
    occupationRemark?: string,
    dateOfBirth?: string,
    countryCode?: string,
  ) {
    try {
      const sql = `
        INSERT INTO users(code, "type", user_name, "password", first_name, middle_name, last_name, email, tel, status, created_by, created_date, updated_by, updated_date, date_of_birth, country_code,
          gender, nationality_code, occupation, occupation_remark)
        VALUES($1, $2, $7, $3, $4, $5, $6, $7, $8, $9, $10, CURRENT_TIMESTAMP, $10, CURRENT_TIMESTAMP, $11, $12, $13, $14, $15 ,$16)
        RETURNING *`;
      return await db.query(sql, [
        code,
        type,
        password,
        firstName,
        middleName,
        lastName,
        email,
        tel,
        status,
        logOnId,
        dateOfBirth,
        countryCode,
        gender,
        nationality,
        occupation,
        occupationRemark!
      ]);
    } catch (error) {
      throw error;
    }
  }

  async update(
    userId: number,
    firstName: string,
    middleName: string,
    lastName: string,
    tel: string,
    status: string,
    logOnId: string,
    gender: string,
    nationality: string,
    occupation: string,
    occupationRemark?: string,
    dateOfBirth?: string,
    countryCode?: string,
    email?: string,
  ) {
    try {
      const sql = `
        UPDATE users
        SET first_name = $2
          , middle_name = $3
          , last_name = $4
          , tel = $5
          , status = $6
          , updated_by = $7
          , updated_date = CURRENT_TIMESTAMP
          , date_of_birth = $8
          , country_code = $9
          , email = $10
          , gender = $11
          , nationality_code = $12
          , occupation = $13
          , occupation_remark = $14
        WHERE user_id = $1
        RETURNING *`;
      return await db.query(sql, [
        userId,
        firstName,
        middleName,
        lastName,
        tel,
        status,
        logOnId,
        dateOfBirth,
        countryCode,
        email,
        gender,
        nationality,
        occupation,
        occupationRemark!,
      ]);
    } catch (error) {
      throw error;
    }
  }

  async delete(userId: number) {
    try {
      const sql = `
        DELETE FROM users
        WHERE user_id = $1`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }

  async changePassword(userId: number, password: string, logOnId: string) {
    try {
      const sql = `
        UPDATE users
        SET password = $2
          , updated_by = $3
          , updated_date = CURRENT_TIMESTAMP
        WHERE user_id = $1
        RETURNING *`;
      return await db.query(sql, [userId, password, logOnId]);
    } catch (error) {
      throw error;
    }
  }
}
