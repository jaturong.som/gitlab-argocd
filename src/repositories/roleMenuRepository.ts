import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class RoleMenuRepository {
  constructor() {}
  async getInfoByRoleIdGroupMenuCode(roleId: number, groupMenuCode: string) {
    try {
      const sql = `
        SELECT *
        FROM role_menus
        WHERE role_id = $1
        AND group_menu_code = $2
        AND menu_code IS NULL`;
      return await db.query(sql, [roleId, groupMenuCode]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByRoleIdGroupMenuCodeMenuCode(
    roleId: number,
    groupMenuCode: string,
    menuCode: string
  ) {
    try {
      const sql = `
        SELECT *
        FROM role_menus
        WHERE role_id = $1
        AND group_menu_code = $2
        AND menu_code = $3`;
      return await db.query(sql, [roleId, groupMenuCode, menuCode]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    roleId: number,
    groupMenuCode: string,
    menuCode: string,
    logOnId: string
  ) {
    try {
      const sql = `
        INSERT INTO role_menus(role_id, group_menu_code, menu_code, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, CURRENT_TIMESTAMP, $4, CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [roleId, groupMenuCode, menuCode, logOnId]);
    } catch (error) {
      throw error;
    }
  }

  async deleteByRoleId(roleId: number) {
    try {
      const sql = `
        DELETE FROM role_menus
        WHERE role_id = $1`;
      return await db.query(sql, [roleId]);
    } catch (error) {
      throw error;
    }
  }

  async deleteByRoleIdGroupMenuCode(roleId: number, groupMenuCode: string) {
    try {
      const sql = `
        DELETE FROM role_menus
        WHERE role_id = $1
        AND group_menu_code = $2
        AND menu_code IS NULL`;
      return await db.query(sql, [roleId, groupMenuCode]);
    } catch (error) {
      throw error;
    }
  }

  async deleteByRoleIdGroupMenuCodeMenuCode(
    roleId: number,
    groupMenuCode: string,
    menuCode: string
  ) {
    try {
      const sql = `
        DELETE FROM role_menus
        WHERE role_id = $1
        AND group_menu_code = $2
        AND menu_code = $3`;
      return await db.query(sql, [roleId, groupMenuCode, menuCode]);
    } catch (error) {
      throw error;
    }
  }
}
