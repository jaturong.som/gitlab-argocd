import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class UserApiKeyRepository {
  constructor() {}
  async getList(userId: number) {
    try {
      const sql = `
        SELECT *
        FROM user_api_keys
        WHERE user_id = $1
        ORDER BY user_api_key_id`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoById(userApiKeyId: number) {
    try {
      const sql = `
        SELECT *
        FROM user_api_keys
        WHERE user_api_key_id = $1`;
      return await db.query(sql, [userApiKeyId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByName(userId: number, name: string) {
    try {
      const sql = `
        SELECT *
        FROM user_api_keys
        WHERE user_id = $1
        AND name = $2`;
      return await db.query(sql, [userId, name]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByKey(userKey: string) {
    try {
      const sql = `
        SELECT *
        FROM user_api_keys
        where status = 'A'
        and user_key = $1`;
      return await db.query(sql, [userKey]);
    } catch (error) {
      throw error;
    }
  }

  async isDuplicateByName(userId: number, name: string) {
    try {
      const sql = `
        SELECT *
        FROM user_api_keys
        WHERE user_id = $1
        AND lower("name") = lower($2)`;
      return await db.query(sql, [userId, name]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    userId: number,
    name: string,
    userKey: string,
    userSecret: string,
    logOnId: string
  ) {
    try {
      const sql = `
        INSERT INTO user_api_keys(user_id, "name", user_key, user_secret, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, 'A', $5, CURRENT_TIMESTAMP, $5, CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [userId, name, userKey, userSecret, logOnId]);
    } catch (error) {
      throw error;
    }
  }

  async enableOrDisable(
    userId: number,
    userApiKeyId: number,
    status: string,
    logOnId: string
  ) {
    try {
      const sql = `
        UPDATE user_api_keys
        SET status = $3
            , updated_by = $4
            , updated_date = CURRENT_TIMESTAMP
        WHERE user_id = $1
        AND user_api_key_id = $2
        RETURNING *`;
      return await db.query(sql, [userId, userApiKeyId, status, logOnId]);
    } catch (error) {
      throw error;
    }
  }

  async disableAllByUserId(userId: number, logOnId: string) {
    try {
      const sql = `
        UPDATE user_api_keys
        SET status = 'I'
            , updated_by = $2
            , updated_date = CURRENT_TIMESTAMP
        WHERE user_id = $1
        RETURNING *`;
      return await db.query(sql, [userId, logOnId]);
    } catch (error) {
      throw error;
    }
  }
}
