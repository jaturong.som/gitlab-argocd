import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class FiatAccountRepository {
  constructor() {}

  async getListByAdmin() {
    try {
      const sql = `
        select fiat_accounts.fiat_id
          , fiat_accounts.user_id
          , CONCAT(coalesce(users.first_name, ''), ' ', coalesce(users.last_name, '')) as user_full_name
          , fiat_accounts.total
          , fiat_accounts.available
          , fiat_accounts.pending
          , fiat_accounts.lockedamount
          , coalesce(fiat_accounts.is_favorite, 'N') as is_favorite
        from fiat_accounts
          inner join users on users.user_id = fiat_accounts.user_id
                          and users.status = 'A'
        where fiat_accounts."type" = 'U'
        and fiat_accounts.status = 'A'
        order by fiat_accounts.is_favorite desc, users.first_name, users.last_name;`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  // async getList(type: string, companyId?: number, userId?: number) {
  //   try {
  //     let sql = "";
  //     if (type === "C") {
  //       sql = `
  //         select 0 as user_id, 0 as total_income, 0 as total_expense, 0 as total_pending`;
  //     } else {
  //       sql = `
  //       select users.user_id
  //         , COALESCE(trans.total_income,0) as total_income
  //         , COALESCE(withdraw.total_expense,0) as total_expense
  //         , COALESCE(withdraw_pending.total_pending,0) as total_pending
  //       from users
  //         left join (
  //           select user_id, COALESCE(sum(total_amount),0) as total_income
  //           from transactions
  //           where user_id = $2
  //           and "type" = 'S'
  //           and status = 'S'
  //           and wallet_network != 'WLN99'
  //           group by user_id
  //         ) trans on trans.user_id = users.user_id
  //         left join (
  //           select user_id, COALESCE(sum(amount),0) + COALESCE(sum(service_charge),0) as total_expense
  //           from withdraws
  //           where user_id = $2
  //           and status = 'S'
  //           group by user_id
  //         ) withdraw on withdraw.user_id = trans.user_id
  //         left join (
  //           select user_id, COALESCE(sum(amount),0) + COALESCE(sum(service_charge),0) as total_pending
  //           from withdraws
  //           where user_id = $2
  //           and status in ('P','H')
  //           group by user_id
  //         ) withdraw_pending on withdraw_pending.user_id = trans.user_id
  //       where users.status = 'A'
  //       and users.user_id = $2;`;
  //     }
  //     return await db.query(sql, [companyId, userId]);
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async getInfoById(fiatId: number) {
    try {
      const sql = `
        select *
        from fiat_accounts
        where status = 'A'
        and fiat_id = $1`;
      return await db.query(sql, [fiatId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByUserId(userId: number) {
    try {
      const sql = `
        select *
        from fiat_accounts
        where status = 'A'
        and user_id = $1`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    type: string,
    logOnId: string,
    companyId?: number,
    userId?: number
  ) {
    try {
      const sql = `
        INSERT INTO fiat_accounts("type", company_id, user_id, total, available, pending, lockedamount, is_favorite, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, 0, 0, 0, 0, 'N', 'A', $4, CURRENT_TIMESTAMP, $4, CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [type, companyId, userId, logOnId]);
    } catch (error) {
      throw error;
    }
  }

  // async updateBalance(
  //   type: string,
  //   total: number,
  //   pending: number,
  //   companyId?: number,
  //   userId?: number
  // ) {
  //   try {
  //     let sql = "";
  //     if (type === "C") {
  //       sql = `
  //         UPDATE fiat_accounts
  //         SET total = $3
  //           , pending = $4
  //           , updated_date = CURRENT_TIMESTAMP
  //         WHERE company_id = $1
  //         RETURNING *`;
  //     } else {
  //       sql = `
  //         UPDATE fiat_accounts
  //         SET total = $3
  //           , pending = $4
  //           , updated_date = CURRENT_TIMESTAMP
  //         WHERE user_id = $2
  //         RETURNING *`;
  //     }
  //     return await db.query(sql, [companyId, userId, total, pending]);
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async updateBalanceAllUsers() {
    try {
      const sql = `
        UPDATE fiat_accounts
        SET total = (COALESCE(trans.total_income,0) + COALESCE(wallet_credits_topup.total_topup,0) - COALESCE(withdraw.total_expense,0) - COALESCE(withdraw_pending.total_pending,0))::numeric
            , available = (COALESCE(trans.total_income,0) + COALESCE(wallet_credits_topup.total_topup,0) - COALESCE(withdraw.total_expense,0) - COALESCE(withdraw_pending.total_pending,0))::numeric
            , pending = (COALESCE(withdraw_pending.total_pending,0))::numeric
            , updated_date = CURRENT_TIMESTAMP
        from users
          left join (
            select user_id, COALESCE(sum(total_amount),0) as total_income
            from transactions
            where "type" = 'S'
            and status = 'S'
            and wallet_network != 'WLN99'
            group by user_id
          ) trans on trans.user_id = users.user_id
          left join (
            select user_id, COALESCE(sum(amount),0) + COALESCE(sum(service_charge),0) as total_expense 
            from withdraws
            where status = 'S'
            group by user_id
          ) withdraw on withdraw.user_id = users.user_id
          left join (
            select user_id, COALESCE(sum(amount),0) + COALESCE(sum(service_charge),0) as total_pending
            from withdraws
            where status in ('P','H')
            group by user_id
          ) withdraw_pending on withdraw_pending.user_id = users.user_id
          left join (
            select user_id, COALESCE(sum(amount),0) as total_topup
            from wallet_credits
            where status = 'S'
            group by user_id
          ) wallet_credits_topup on wallet_credits_topup.user_id = users.user_id
        where fiat_accounts.status = 'A'
        and users.status = 'A'
        and fiat_accounts.user_id = users.user_id
        RETURNING *`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async updateFavoriteFiatAccount(
    fiatId: number,
    isFavorite: string,
    logOnId: string
  ) {
    try {
      const sql = `
      UPDATE fiat_accounts
      SET is_favorite = $2
        , updated_by = $3
        , updated_date = CURRENT_TIMESTAMP
      WHERE fiat_id = $1
      RETURNING *`;
      return await db.query(sql, [fiatId, isFavorite, logOnId]);
    } catch (error) {
      throw error;
    }
  }
}
