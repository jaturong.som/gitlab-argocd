import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class FireblocksTransactionRepository {
  constructor() {}
  async getList() {
    try {
      const sql = `
        select *
        from fireblock_transactions`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getListByStatus(status: string) {
    try {
      const sql = `
        select *
        from fireblock_transactions
        where status = $1`;
      return await db.query(sql, [status]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByStatusRefId(status: string, refId: number) {
    try {
      const sql = `
        select *
        from fireblock_transactions
        where status = $1
        and ref_id = $2`;
      return await db.query(sql, [status, refId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoById(fireblockTransactionId: number) {
    try {
      const sql = `
        select *
        from fireblock_transactions
        where fireblock_transaction_id = $1`;
      return await db.query(sql, [fireblockTransactionId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByfbId(fbTransactionId: string) {
    try {
      const sql = `
        select *
        from fireblock_transactions
        where fb_transaction_id = $1`;
      return await db.query(sql, [fbTransactionId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByTypeRefId(type: string, ref_id: number) {
    try {
      const sql = `
        select *
        from fireblock_transactions
        where "type" = $1
        and ref_id = $2`;
      return await db.query(sql, [type, ref_id]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    fbTransactionId: string,
    type: string,
    assetId: string,
    sourceId: string,
    sourceType: string,
    amount: number,
    fee: number,
    refId: number,
    status: string,
    destinationId?: string,
    destinationAddress?: string,
    destinationType?: string
  ) {
    try {
      const sql = `
        INSERT INTO fireblock_transactions(fb_transaction_id, "type", asset_id, source_id, source_type, destination_id, destination_address, destination_type, amount, fee, remark, ref_id, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, '', $11, $12, '0', CURRENT_TIMESTAMP, '0', CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [
        fbTransactionId,
        type,
        assetId,
        sourceId,
        sourceType,
        destinationId,
        destinationAddress,
        destinationType,
        amount,
        fee,
        refId,
        status,
      ]);
    } catch (error) {
      throw error;
    }
  }

  async updateStatusFromFireblocks(
    fireblockTransactionId: string,
    status: string,
    remark: string,
    fbTransactionHash: string
  ) {
    try {
      const sql = `
        UPDATE fireblock_transactions
        SET status = $2
        , remark = $3
        , fb_transaction_hash = $4
        , updated_date = CURRENT_TIMESTAMP
        WHERE fireblock_transaction_id = $1
        RETURNING *`;
      return await db.query(sql, [
        fireblockTransactionId,
        status,
        remark,
        fbTransactionHash,
      ]);
    } catch (error) {
      throw error;
    }
  }
}
