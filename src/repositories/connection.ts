import pgPromise from "pg-promise";

const connection: any = {
  host: process.env.DATABASE_HOST!,
  port: process.env.DATABASE_PORT!,
  database: process.env.DATABASE_NAME!,
  user: process.env.DATABASE_USER!,
  password: process.env.DATABASE_PASSWORD!,
};

const pg = pgPromise({});
export const db = pg(connection);
