import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class ChainalysisTransactionRepository {
  constructor() {}
  async getPendingList() {
    try {
      const sql = `
      SELECT *
      FROM chainalysis_transactions
      WHERE updated_at IS NULL`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByFBTransactionId(fbTransactionId: string) {
    try {
      const sql = `
      SELECT *
      FROM chainalysis_transactions
      WHERE fb_transaction_id = $1`;
      return await db.query(sql, [fbTransactionId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByExternalId(externalId: string) {
    try {
      const sql = `
      SELECT *
      FROM chainalysis_transactions
      WHERE external_id = $1`;
      return await db.query(sql, [externalId]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    fbTransactionId: string,
    updatedAt: string,
    asset: string,
    network: string,
    transferReference: string,
    tx: string,
    idx: string,
    usdAmount: number,
    assetAmount: number,
    timestamp: string,
    outputAddress: string,
    externalId: string
  ) {
    try {
      const sql = `
        INSERT INTO chainalysis_transactions(fb_transaction_id, updated_at, asset, network, transfer_reference, tx, idx, usd_amount, asset_amount, "timestamp", output_address, external_id, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, 'A','0',CURRENT_TIMESTAMP,'0',CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [
        fbTransactionId,
        updatedAt,
        asset,
        network,
        transferReference,
        tx,
        idx,
        usdAmount,
        assetAmount,
        timestamp,
        outputAddress,
        externalId,
      ]);
    } catch (error) {
      throw error;
    }
  }

  async update(
    externalId: string,
    updatedAt: string,
    asset: string,
    network: string,
    transferReference: string,
    tx: string,
    idx: string,
    usdAmount: number,
    assetAmount: number,
    timestamp: string,
    outputAddress: string
  ) {
    try {
      const sql = `
        UPDATE chainalysis_transactions
        SET updated_at = $2
          , asset = $3
          , network = $4
          , transfer_reference = $5
          , tx = $6
          , idx = $7
          , usd_amount = $8
          , asset_amount = $9
          , "timestamp" = $10
          , output_address = $11
          , updated_date = CURRENT_TIMESTAMP
        WHERE external_id = $1
        RETURNING *`;
      return await db.query(sql, [
        externalId,
        updatedAt,
        asset,
        network,
        transferReference,
        tx,
        idx,
        usdAmount,
        assetAmount,
        timestamp,
        outputAddress,
      ]);
    } catch (error) {
      throw error;
    }
  }
}
