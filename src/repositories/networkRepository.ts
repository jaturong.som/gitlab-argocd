import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class NetworkRepository {
  constructor() {}
  async getList() {
    try {
      const sql = `
      SELECT networks.*
          , CONCAT(users.first_name,' ', users.last_name) as fullname_created_by
      FROM networks
        left join users on users.user_id = NULLIF(networks.created_by, '')::int
      WHERE networks.status = 'A'
      ORDER BY networks.network_id`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getListForAssetRates() {
    try {
      const sql = `
      SELECT networks.*
          , CONCAT(users.first_name,' ', users.last_name) as fullname_created_by
      FROM networks
        left join users on users.user_id = NULLIF(networks.created_by, '')::int
      WHERE networks.status = 'A'
      and networks.network_id in (
            select network_id
            from assets
              inner join asset_rates on asset_rates.asset_code = assets.code
            where assets.status = 'A')
      ORDER BY networks.network_id`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getListByUserId(userId: number) {
    try {
      const sql = `
        select networks.*
        from vault_accounts
          inner join asset_in_vault_accounts on asset_in_vault_accounts.vault_account_id = vault_accounts.vault_account_id
                            and asset_in_vault_accounts.status = 'A'
          inner join assets on assets.code = asset_in_vault_accounts.asset_code
                    and assets.status = 'A'
          inner join networks on networks.network_id = assets.network_id
                    and networks.status = 'A'
        where vault_accounts.status = 'A'
        and vault_accounts.user_id = $1`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }
}
