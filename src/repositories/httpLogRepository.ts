import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class HttpLogRepository {
  constructor() {}

  async getFireblocksNotificationList(
    fromDate: string,
    toDate: string,
    status: string,
    searchText: string
  ) {
    try {
      let sql = `
        SELECT *
        FROM http_logs
        WHERE url = '/api/v1/webhook/notification'
        and (request_date::date between $1 and $2)`;
      let filter = ``;
      if (status != "" && status != undefined) {
        filter = filter + ` and body like '%"status":"$3:value"%'`;
      }
      if (searchText != "" && searchText != undefined) {
        searchText = searchText.toLowerCase();
        filter = filter + ` and lower(body) like '%$4:value%'`;
      }
      if (filter != ``) {
        filter = filter + ` order by request_date desc`;
        sql = sql + filter;
      } else {
        sql = sql + ` order by request_date desc`;
      }
      return await db.query(sql, [fromDate, toDate, status, searchText]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    requester: string,
    remoteAddress: string,
    method: string,
    url: string,
    header: string,
    body: string,
    response: string
  ) {
    try {
      const sql = `
        INSERT INTO http_logs(request_date, requester, remote_address, "method", url, header, body, response)
        VALUES(CURRENT_TIMESTAMP, $1, $2, $3, $4, $5, $6, $7)
        RETURNING *`;
      return await db.query(sql, [
        requester,
        remoteAddress,
        method,
        url,
        header,
        body,
        response,
      ]);
    } catch (error) {
      throw error;
    }
  }
}
