import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class SummaryRepository {
  constructor() {}
  async getBySummaryKey(summary_key: string) {
    try {
      const sql = `
      select * from summary where summary_key = $1`;
      return await db.query(sql, [summary_key]);
    } catch (error) {
      throw error;
    }
  }

  async updateSummaryValue(summary_key: string, summary_value: number) {
    try {
      const sql = `
      update summary 
      set summary_value = $2 ,
          updated_date=CURRENT_TIMESTAMP
      where summary_key = $1
      RETURNING *`;
      return await db.query(sql, [summary_key, summary_value]);
    } catch (error) {
      throw error;
    }
  }

  async create(summary_key: string, summary_value: number, updated_by: string) {
    try {
      const sql = `
        INSERT INTO summary(summary_key, summary_value,updated_by,updated_date)
        VALUES( $1, $2, $3, CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [summary_key, summary_value, updated_by]);
    } catch (error) {
      throw error;
    }
  }
}
