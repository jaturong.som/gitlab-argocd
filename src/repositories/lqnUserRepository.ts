import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class LqnUserRepository {
  constructor() { }

  async create(
    lqnType: string,
    firstName: string,
    middleName: string,
    lastname: string,
    gender: string,
    mobileNumber: string,
    nationality: string,
    idType: string,
    idTpyeRemark: string,
    idNumber: string,
    idIssueCountry: string,
    idIssueDate: string,
    idExpiredDate: string,
    dateOfBirth: string,
    occupation: string,
    occupation_remark: string,
    email: string,
    nativefirstName: string,
    nativeMiddleName: string,
    nativeLastName: string,
    lqnRefId: string,
    logon: string,
    senderRefId: string,
    currency: string,
    status: string,
    userId: number,
    userType: string,
    bankAccountId: number,
    countryCode: string,
  ) {
    try {
      const sql = `
        INSERT INTO lqn_user_profile(lqn_type,first_name,middle_name,last_name,gender,mobile_number,nationality,id_type,id_type_remark,id_number,id_issue_country,id_issue_date,id_expired_date,date_of_birth,
          occupation,occupation_remark,email,native_first_name,native_middle_name,native_last_name,lqn_ref_id,created_by,created_date,updated_by,updated_date,sender_ref_id,currency,status,user_id,user_type,bank_account_id, country_code)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, CURRENT_TIMESTAMP, $22, CURRENT_TIMESTAMP, $23, $24, $25, $26, $27, $28, $29)
        RETURNING *`;
      return await db.query(sql, [
        lqnType,
        firstName,
        middleName,
        lastname,
        gender,
        mobileNumber,
        nationality,
        idType,
        idTpyeRemark,
        idNumber,
        idIssueCountry,
        idIssueDate,
        idExpiredDate,
        dateOfBirth,
        occupation,
        occupation_remark,
        email,
        nativefirstName,
        nativeMiddleName,
        nativeLastName,
        lqnRefId,
        logon,
        senderRefId,
        currency,
        status,
        userId,
        userType,
        bankAccountId,
        countryCode
      ]);
    } catch (error) {
      throw error;
    }
  }

  async update(
    lqnUserId: string,
    gender: string,
    nationality: string,
    mobileNumber: string,
    idType: string,
    idTpyeRemark: string,
    idNumber: string,
    idIssueCountry: string,
    idIssueDate: string,
    idExpiredDate: string,
    dateOfBirth: string,
    occupation: string,
    occupation_remark: string,
    email: string,
    nativefirstName: string,
    nativeMiddleName: string,
    nativeLastName: string,
    lqnRefId: string,
    logon: string,
    currency: string,
    countryCode: string,
  ) {
    try {
      const sql = `
      UPDATE lqn_user_profile
      SET gender = $2,
          nationality =$3,
          mobile_number = $4,
          id_type =  $5,
          id_type_remark = $6,
          id_number =$7,
          id_issue_country = $8,
          id_issue_date = $9,
          id_expired_date = $10,
          date_of_birth = $11,
          occupation = $12,
          occupation_remark = $13,
          email = $14,
          native_first_name = $15,
          native_middle_name = $16,
          native_last_name = $17,
          lqn_ref_id = $18,
          updated_by = $19 ,
          updated_date = CURRENT_TIMESTAMP,
          currency = $20,
          country_code = $21
      WHERE lqn_user_profile_id = $1
        RETURNING *`;
      return await db.query(sql, [
        lqnUserId,
        gender,
        nationality,
        mobileNumber,
        idType,
        idTpyeRemark,
        idNumber,
        idIssueCountry,
        idIssueDate,
        idExpiredDate,
        dateOfBirth,
        occupation,
        occupation_remark,
        email,
        nativefirstName,
        nativeMiddleName,
        nativeLastName,
        lqnRefId,
        logon,
        currency,
        countryCode,
      ]);
    } catch (error) {
      throw error;
    }
  }

  async updateStatus(lqnUserId: string, status: string) {
    try {
      const sql = `
        UPDATE lqn_user_profile
        SET status =$2
        WHERE lqn_user_profile_id = $1
        RETURNING *`;
      return await db.query(sql, [lqnUserId, status]);
    } catch (error) {
      throw error;
    }
  }

  async getLqnUser(userId: number) {
    try {
      const sql = `
      select *
      from lqn_user_profile
      where user_id = $1;`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }

  async getLqnUserByType(userId: number, lqnType: string) {
    try {
      const sql = `
      select *
      from lqn_user_profile
      where user_id = $1
        and status = 'A'
        and lqn_type = $2;`;
      return await db.query(sql, [userId, lqnType]);
    } catch (error) {
      throw error;
    }
  }

  async getLqnUserByLqnRefIdLqnType(lqnRefId: string, lqnType: string) {
    try {
      const sql = `
      select *
      from lqn_user_profile
      where lqn_ref_id = $1
        and lqn_type = $2;`;
      return await db.query(sql, [lqnRefId, lqnType]);
    } catch (error) {
      throw error;
    }
  }

  async getLqnUserByBankAccountId(bankAccountId: number) {
    try {
      const sql = `
        select *
        from lqn_user_profile
	      where bank_account_id = $1`;
      return await db.query(sql, [bankAccountId]);
    } catch (error) {
      throw error;
    }
  }

  async updateUserIdById(lqnUserProfileId: number, userId: number) {
    try {
      const sql = `
        UPDATE lqn_user_profile
        SET user_id = $2
        WHERE lqn_user_profile_id = $1
        RETURNING *`;
      return await db.query(sql, [lqnUserProfileId, userId]);
    } catch (error) {
      throw error;
    }
  }

  async getRecieverBySenderRefId(senderRefId: number) {
    try {
      const sql = `
      select *
      from lqn_user_profile
      where  sender_ref_id = $1
      and lqn_type = 'RECEIVER'`;
      return await db.query(sql, [senderRefId]);
    } catch (error) {
      throw error;
    }
  }


}
