import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class CactusWalletRepository {
  constructor() { }
  async getInfo(type: string, companyId?: number, userId?: number) {
    try {
      let sql = "";
      if (type === "C") {
        sql = `
            select *
            from cactus_wallets
            where company_id = $1`;
      } else {
        sql = `
            select *
            from cactus_wallets
            where user_id = $2`;
      }
      return await db.query(sql, [companyId, userId]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    type: string,
    bid: string,
    businessName: string,
    walletCode: string,
    walletType: string,
    storageType: string,
    logOnId: string,
    companyId?: number,
    userId?: number
  ) {
    try {
      const sql = `
        INSERT INTO cactus_wallets("type", company_id, user_id, bid, business_name, wallet_code, wallet_type, storage_type, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, $6, $7 ,$8 ,'A', $9, CURRENT_TIMESTAMP, $9, CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [
        type,
        companyId,
        userId,
        bid,
        businessName,
        walletCode,
        walletType,
        storageType,
        logOnId,
      ]);
    } catch (error) {
      throw error;
    }
  }
}
