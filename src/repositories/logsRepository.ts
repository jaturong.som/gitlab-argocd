import { db } from "./connection";
import { singleton } from "tsyringe";
import * as enums from "../utilities/enums";

@singleton()
export class LogsRepository {
  constructor() {}
  async create(
    // requestId: string,
    logsType: enums.logsType,
    logsName: string,
    logsMessage: string
  ) {
    try {
      const sql = `
        INSERT INTO logs(logs_type, logs_name, logs_message, created_date)
        VALUES($1, $2, $3,CURRENT_TIMESTAMP)
        RETURNING *`;
        // const sql = `
        // INSERT INTO users(request_id, logs_type, logs_name, logs_message, created_date)
        // VALUES($1, $2, $3, $4,CURRENT_TIMESTAMP)
        // RETURNING *`;
      return await db.query(sql, [
        // requestId,
        logsType,
        logsName,
        logsMessage,
      ]);
    } catch (error) {
      throw error;
    }
  }

}

