import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class WalletCreditRepository {
  constructor() {}
  async getList() {
    try {
      const sql = `
      select wallet_credits.wallet_credit_id
        , wallet_credits.wallet_credit_date
        , wallet_credits.wallet_credit_no
        , wallet_credits.user_id
        , wallet_credits."type"
        , wallet_credits.amount
        , wallet_credits.currency_code
        , wallet_credits.status
        , concat(users.first_name, ' ', users.last_name) as customer_name
      from wallet_credits
        inner join users on users.user_id = wallet_credits.user_id
      order by wallet_credits.wallet_credit_date desc`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getListByUserId(userId: number) {
    try {
      const sql = `
      select wallet_credits.wallet_credit_id
        , wallet_credits.wallet_credit_date
        , wallet_credits.wallet_credit_no
        , wallet_credits.user_id
        , wallet_credits."type"
        , wallet_credits.amount
        , wallet_credits.currency_code
        , wallet_credits.status
        , concat(users.first_name, ' ', users.last_name) as customer_name
      from wallet_credits
        inner join users on users.user_id = wallet_credits.user_id
      where wallet_credits.user_id = $1`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoById(walletCreditId: number) {
    try {
      const sql = `
        select wallet_credits.*
          , CONCAT(users.first_name,' ', users.last_name) as operator_by
        from wallet_credits
          left join users on users.user_id = NULLIF(wallet_credits.created_by, '')::int
        where wallet_credits.status = 'S'
        and wallet_credits.wallet_credit_id = $1`;
      return await db.query(sql, [walletCreditId]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    walletCreditNo: string,
    userId: number,
    type: string,
    amount: number,
    currencyCode: string,
    logOnId: string
  ) {
    try {
      const sql = `
        INSERT INTO wallet_credits(wallet_credit_date, wallet_credit_no, user_id, "type", amount, currency_code, status, created_by, created_date, updated_by, updated_date)
        VALUES(CURRENT_TIMESTAMP, $1, $2, $3, $4, $5, 'S', $6, CURRENT_TIMESTAMP, $6, CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [
        walletCreditNo,
        userId,
        type,
        amount,
        currencyCode,
        logOnId,
      ]);
    } catch (error) {
      throw error;
    }
  }
}
