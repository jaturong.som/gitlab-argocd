import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class DropdownlistRepository {
  constructor() {}
  async getList() {
    try {
      const sql = `
      SELECT *
      FROM dropdownlists
      WHERE status = 'A'
      ORDER BY "order"`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getListByType(type: string) {
    try {
      const sql = `
      SELECT *
      FROM dropdownlists
      WHERE status = 'A'
      AND "type" = $1
      ORDER BY "order"`;
      return await db.query(sql, [type]);
    } catch (error) {
      throw error;
    }
  }
}
