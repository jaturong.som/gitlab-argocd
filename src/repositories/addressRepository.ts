import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class AddressRepository {
  constructor() { }
  async getInfoById(addressType: string, addressId: number) {
    try {
      const sql = `
        select addresses.*, countries."name" as country_name
        from addresses
          left join countries on countries.code = addresses.country_code
                and countries.status = 'A'
        where addresses.status = 'A'
        and addresses.address_type = $1
        and addresses.address_id = $2`;
      return await db.query(sql, [addressType, addressId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByRefId(addressType: string, refId: number) {
    try {
      const sql = `
        select addresses.*, countries."name" as country_name
        from addresses
          left join countries on countries.code = addresses.country_code
                  and countries.status = 'A'
        where addresses.status = 'A'
        and addresses.address_type = $1
        and addresses.ref_id = $2`;
      return await db.query(sql, [addressType, refId]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    addressType: string,
    refId: number,
    line1: string,
    line2: string,
    city: string,
    state: string,
    countryCode: string,
    postcode: string,
    areaTown: string,
    logOnId: string
  ) {
    try {
      const sql = `
        INSERT INTO addresses(address_type, ref_id, line1, line2, city, state, country_code, postcode, area_town, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, 'A', $10, CURRENT_TIMESTAMP, $10, CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [
        addressType,
        refId,
        line1,
        line2,
        city,
        state,
        countryCode,
        postcode,
        areaTown,
        logOnId,
      ]);
    } catch (error) {
      throw error;
    }
  }

  async update(
    addressId: number,
    line1: string,
    line2: string,
    city: string,
    state: string,
    countryCode: string,
    postcode: string,
    areaTown: string,
    logOnId: string
  ) {
    try {
      const sql = `
        UPDATE addresses
        SET line1 = $2
            , line2 = $3
            , city = $4
            , state = $5
            , country_code = $6
            , postcode = $7
            , area_town = $8
            , updated_by = $9
            , updated_date = CURRENT_TIMESTAMP
        WHERE address_id = $1
        RETURNING *`;
      return await db.query(sql, [
        addressId,
        line1,
        line2,
        city,
        state,
        countryCode,
        postcode,
        areaTown,
        logOnId,
      ]);
    } catch (error) {
      throw error;
    }
  }

  async delete(addressId: number, logOnId: string) {
    try {
      const sql = `
        UPDATE addresses
        SET status = 'D'
            , updated_by = $2
            , updated_date = CURRENT_TIMESTAMP
        WHERE address_id = $1
        RETURNING *`;
      return await db.query(sql, [addressId, logOnId]);
    } catch (error) {
      throw error;
    }
  }
}
