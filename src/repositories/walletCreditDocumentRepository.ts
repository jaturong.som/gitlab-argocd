import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class WalletCreditDocumentRepository {
  constructor() {}
  async getList(userId: number) {
    try {
      const sql = `
        select wallet_credit_documents.*
        from wallet_credits
          inner join wallet_credit_documents on wallet_credit_documents.wallet_credit_id = wallet_credits.wallet_credit_id
                                            and wallet_credit_documents.status = 'A'
        where wallet_credits.status = 'A'
        and wallet_credits.user_id = $1
        ORDER BY wallet_credit_documents.wallet_credit_document_id`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }

  async getListByWalletCreditId(walletCreditId: number) {
    try {
      const sql = `
        select *
        from wallet_credit_documents
        where status = 'A'
        and wallet_credit_id = $1
        ORDER BY wallet_credit_documents.wallet_credit_document_id`;
      return await db.query(sql, [walletCreditId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoById(walletCreditDocumentId: number) {
    try {
      const sql = `
        select *
        from wallet_credit_documents
        where status = 'A'
        and wallet_credit_document_id = $1`;
      return await db.query(sql, [walletCreditDocumentId]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    walletCreditId: number,
    fileName: string,
    url: string,
    logOnId: string
  ) {
    try {
      const sql = `
        INSERT INTO wallet_credit_documents(wallet_credit_id, file_name, url, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, 'A', $4, CURRENT_TIMESTAMP, $4, CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [walletCreditId, fileName, url, logOnId]);
    } catch (error) {
      throw error;
    }
  }
}
