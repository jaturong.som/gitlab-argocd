import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class RoleRepository {
  constructor() {}
  async getList() {
    try {
      const sql = `
        SELECT *
        FROM roles
        ORDER BY role_id`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getActiveList() {
    try {
      const sql = `
        SELECT *
        FROM roles
        WHERE status = 'A'
        ORDER BY role_id`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getInfoById(roleId: number) {
    try {
      const sql = `
        SELECT *
        FROM roles
        WHERE role_id = $1`;
      return await db.query(sql, [roleId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByName(name: string) {
    try {
      const sql = `
        SELECT *
        FROM roles
        WHERE "name" = $1`;
      return await db.query(sql, [name]);
    } catch (error) {
      throw error;
    }
  }

  async isDuplicateByName(mode: string, name: string, roleId?: number) {
    try {
      let sql = "";
      if (mode === "INSERT") {
        sql = `
          SELECT *
          FROM roles
          WHERE lower("name") = lower($1)`;
      } else {
        sql = `
          SELECT *
          FROM roles
          WHERE role_id != $2
          AND lower("name") = lower($1)`;
      }
      return await db.query(sql, [name, roleId]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    name: string,
    description: string,
    status: string,
    logOnId: string
  ) {
    try {
      const sql = `
        INSERT INTO roles("name", description, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, CURRENT_TIMESTAMP, $4, CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [name, description, status, logOnId]);
    } catch (error) {
      throw error;
    }
  }

  async update(
    roleId: number,
    name: string,
    description: string,
    status: string,
    logOnId: string
  ) {
    try {
      const sql = `
        UPDATE roles
        SET "name" = $2
          , description = $3
          , status = $4
          , updated_by = $5
          , updated_date = CURRENT_TIMESTAMP
        WHERE role_id = $1
        RETURNING *`;
      return await db.query(sql, [roleId, name, description, status, logOnId]);
    } catch (error) {
      throw error;
    }
  }

  async delete(roleId: number) {
    try {
      const sql = `
        DELETE FROM roles
        WHERE role_id = $1`;
      return await db.query(sql, [roleId]);
    } catch (error) {
      throw error;
    }
  }
}
