import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class UserRoleRepository {
  constructor() {}
  async getListByUserId(userId: number) {
    try {
      const sql = `
        SELECT *
        FROM user_roles
        WHERE user_id = $1`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }

  async getActiveListByUserId(userId: number) {
    try {
      const sql = `
        SELECT user_roles.*
        FROM user_roles
          inner join roles on roles.role_id = user_roles.role_id
                  and roles.status = 'A'
        WHERE user_roles.user_id = $1`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByUserIdRoleId(userId: number, roleId: number) {
    try {
      const sql = `
        SELECT *
        FROM user_roles
        WHERE user_id = $1
        AND role_id = $2`;
      return await db.query(sql, [userId, roleId]);
    } catch (error) {
      throw error;
    }
  }

  async create(userId: number, roleId: number, logOnId: string) {
    try {
      const sql = `
        INSERT INTO user_roles(user_id, role_id, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, CURRENT_TIMESTAMP, $3, CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [userId, roleId, logOnId]);
    } catch (error) {
      throw error;
    }
  }

  async deleteByUserId(userId: number) {
    try {
      const sql = `
        DELETE FROM user_roles
        WHERE user_id = $1`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }
}
