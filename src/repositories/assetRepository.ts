import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class AssetRepository {
  constructor() { }
  async getListByNetworkId(networkId: number) {
    try {
      const sql = `
        SELECT assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , assets.network_id
          , assets.contract_address
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
        FROM assets
            inner join networks on networks.network_id = assets.network_id
                    and networks.status = 'A'
        WHERE assets.status = 'A'
        AND assets.network_id = $1
        ORDER BY assets.asset_id`;
      return await db.query(sql, [networkId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByCode(code: string) {
    try {
      const sql = `
        SELECT assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , assets.network_id
          , assets.contract_address
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
        FROM assets
            inner join networks on networks.network_id = assets.network_id
                    and networks.status = 'A'
        WHERE assets.status = 'A'
        AND assets.code = $1`;
      return await db.query(sql, [code]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByCodeIgnoreStatus(code: string) {
    try {
      const sql = `
        SELECT assets.asset_id
          , assets.code as asset_code
          , assets."name" as asset_name
          , assets.image as asset_image
          , assets.network_id
          , assets.status as asset_status
          , assets.contract_address
          , assets.native_asset_symbol
          , networks.code as network_code
          , networks."name" as network_name
          , networks.image as network_image
        FROM assets
            inner join networks on networks.network_id = assets.network_id
        WHERE assets.code = $1`;
      return await db.query(sql, [code]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByAssetGroup(assetGroup: string) {
    try {
      const sql = `
        SELECT *
        FROM assets
        WHERE assets.status = 'A'
        AND assets.asset_group = $1`;
      return await db.query(sql, [assetGroup]);
    } catch (error) {
      throw error;
    }
  }

  async getAssetIdByAssetCodeNetworkName(
    assetCode: string,
    networkName: string
  ) {
    try {
      const sql = `
          select a.asset_id
          from assets a, networks n
          where a.network_id = n.network_id
          and a.code = $1
          and n.name = $2
          and a.status = 'A'`;
      return await db.query(sql, [assetCode, networkName]);
    } catch (error) {
      throw error;
    }
  }

  async getAssetList() {
    try {
      const sql = `
        select a.code, a.name as asset_name, n.name, a.contract_address, n.code as network_code
        from assets a, networks n
        where a.network_id = n.network_id
        and a.status ='A'`;
      return await db.query(sql, []);
    } catch (error) {
      throw error;
    }
  }
}
