import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class UserTokenRepository {
  constructor() {}
  async getInfo(userId: number) {
    try {
      const sql = `
        SELECT *
        FROM user_tokens
        WHERE user_id = $1`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }

  async create(userId: number, token: string, refreshToken: string) {
    try {
      const sql = `
        INSERT INTO user_tokens(user_id, "token", refresh_token, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, '0', CURRENT_TIMESTAMP, '0', CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [userId, token, refreshToken]);
    } catch (error) {
      throw error;
    }
  }

  async update(userId: number, token: string, refreshToken: string) {
    try {
      const sql = `
        UPDATE user_tokens
        SET "token" = $2
          , refresh_token = $3
          , updated_by = '0'
          , updated_date = CURRENT_TIMESTAMP
        WHERE user_id = $1
        RETURNING *`;
      return await db.query(sql, [userId, token, refreshToken]);
    } catch (error) {
      throw error;
    }
  }

  async delete(userId: number) {
    try {
      const sql = `
        DELETE FROM user_tokens
        WHERE user_id = $1`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }
}
