import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class MenuRepository {
  constructor() {}
  async getListByGroupMenuCode(groupMenuCode: string) {
    try {
      const sql = `
        SELECT *
        FROM menus
        WHERE group_menu_code = $1
        AND status = 'A'
        ORDER BY "order"`;
      return await db.query(sql, [groupMenuCode]);
    } catch (error) {
      throw error;
    }
  }
}
