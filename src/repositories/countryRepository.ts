import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class CountryRepository {
  constructor() {}
  async getList() {
    try {
      const sql = `
      SELECT *
      FROM countries
      WHERE status = 'A'
      ORDER BY code`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByCode(code: string) {
    try {
      const sql = `
      SELECT *
      FROM countries
      WHERE status = 'A'
      AND code = $1`;
      return await db.query(sql, [code]);
    } catch (error) {
      throw error;
    }
  }
}
