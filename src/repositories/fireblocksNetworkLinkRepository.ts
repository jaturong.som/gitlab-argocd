import ethers from "ethers";
import crypto from "crypto";
import DB from "./db.json";
import { saveToDatabase } from "../utilities/utils";
import { singleton } from "tsyringe";

@singleton()
export class FireblocksNetworkLinkRepository {
  constructor() {}
  async getAllAccountTypes(customerId: string) {
    const customer = DB.customers.find((c) => c.id === customerId);
    if (!customer) {
      return;
    }
    const accounts = [];
    const objAccounts = customer.accounts;
    for (let i = 0; i < objAccounts.length; i++) {
      const assets = [];
      var objAssets = objAccounts[i].assets;
      for (let j = 0; j < objAssets.length; j++) {
        assets.push({
          coinSymbol: objAssets[j].coinSymbol,
          totalAmount: objAssets[j].totalAmount,
          pendingAmount: objAssets[j].pendingAmount,
          availableAmount: objAssets[j].availableAmount,
          creditAmount: objAssets[j].creditAmount,
        });
      }
      accounts.push({
        displayName: objAccounts[i].accountName,
        type: objAccounts[i].accountType,
        balances: assets,
      });
    }
    return accounts;
  }

  async getDepositWalletAddress(
    customerId: string,
    accountType: string,
    coinSymbol: string,
    network: string
  ) {
    const customer = DB.customers.find((c) => c.id === customerId);
    if (!customer) {
      return;
    }
    const account = customer.accounts.find(
      (a) => a.accountType === accountType
    );
    if (!account) {
      return;
    }
    const asset = account.assets.find(
      (a) => a.coinSymbol === coinSymbol && a.network === network
    );
    if (!asset) {
      return;
    }
    const depositAddress = {
      depositAddress: asset.depositAddress,
      depositAddressTag: asset.depositAddressTag,
    };
    return depositAddress;
  }

  async createDepositWalletAddress(
    customerId: string,
    accountType: string,
    coinSymbol: string,
    network: string
  ) {
    const indexCustomer = DB.customers.findIndex((c) => c.id === customerId);
    if (indexCustomer === -1) {
      return;
    }
    const indexAccount = DB.customers[indexCustomer].accounts.findIndex(
      (a) => a.accountType === accountType
    );
    if (indexAccount === -1) {
      return;
    }
    const indexAsset = DB.customers[indexCustomer].accounts[
      indexAccount
    ].assets.findIndex(
      (a) => a.coinSymbol === coinSymbol && a.network === network
    );
    if (indexAsset === -1) {
      var id = crypto.randomBytes(32).toString("hex");
      var privateKey = "0x" + id;
      var wallet = new ethers.Wallet(privateKey);

      const newAsset = {
        coinSymbol: coinSymbol,
        totalAmount: 0,
        pendingAmount: 0,
        availableAmount: 0,
        creditAmount: 0,
        network: network,
        depositAddress: wallet.address,
        depositAddressTag: Date.now(),
      };
      DB.customers[indexCustomer].accounts[indexAccount].assets.push(newAsset);
      saveToDatabase(DB);
      return {
        depositAddress: wallet.address,
        depositAddressTag: Date.now(),
      };
    }
    return;
  }

  async getWithDrawalFee(
    transferAmount: number,
    coinSymbol: string,
    network: string
  ) {
    return { feeAmount: transferAmount / 65 };
  }

  async createWithDraw(
    amount: string,
    maxFee: string,
    coinSymbol: string,
    network: string,
    accountType: string,
    toAddress: string,
    tag: string,
    isGross: boolean
  ) {
    var newTransactionID = crypto.randomBytes(16).toString("hex");
    var newTxHash = crypto.randomBytes(32).toString("hex");
    const newTransaction = {
      transactionID: newTransactionID,
      status: "COMPLETED",
      txHash: newTxHash,
      amount: amount,
      serviceFee: maxFee,
      coinSymbol: coinSymbol,
      network: network,
      direction: "CRYPTO_WITHDRAWAL",
      timestamp: Date.now(),
      accountType: accountType,
      toAddress: toAddress,
      tag: tag,
      isGross: isGross,
      maxFee: maxFee,
    };
    DB.transactions.push(newTransaction);
    saveToDatabase(DB);
    return { transactionID: newTransactionID };
  }

  async getTransactionById(transactionID: string) {
    const transaction = DB.transactions.find(
      (c) => c.transactionID === transactionID
    );
    if (!transaction) {
      return;
    }
    return {
      transactionID: transaction.transactionID,
      status: transaction.status,
      txHash: transaction.txHash,
      amount: transaction.amount,
      serviceFee: transaction.serviceFee,
      coinSymbol: transaction.coinSymbol,
      network: transaction.network,
      direction: transaction.direction,
      timestamp: transaction.timestamp,
    };
  }

  async getTransactionByHash(txHash: string, network: string) {
    const transaction = DB.transactions.find(
      (c) => c.txHash === txHash && c.network === network
    );
    if (!transaction) {
      return;
    }
    return {
      transactionID: transaction.transactionID,
      status: transaction.status,
      txHash: transaction.txHash,
      amount: transaction.amount,
      serviceFee: transaction.serviceFee,
      coinSymbol: transaction.coinSymbol,
      network: transaction.network,
      direction: transaction.direction,
      timestamp: transaction.timestamp,
    };
  }

  async getTransactionHistory(
    fromDate: string,
    toDate: string,
    pageSize: string,
    pageCursor: string,
    isSubTransfer: string,
    coinSymbol: string,
    network: string
  ) {
    const transactions = DB.transactions.filter(
      (c) => c.coinSymbol === coinSymbol && c.network === network
    );
    if (!transactions) {
      return;
    }
    const results = [];
    for (let i = 0; i < transactions.length; i++) {
      results.push({
        transactionID: transactions[i].transactionID,
        status: transactions[i].status,
        txHash: transactions[i].txHash,
        amount: transactions[i].amount,
        serviceFee: transactions[i].serviceFee,
        coinSymbol: transactions[i].coinSymbol,
        network: transactions[i].network,
        direction: transactions[i].direction,
        timestamp: transactions[i].timestamp,
      });
    }
    return {
      nextPageCursor: null,
      transactions: results,
    };
  }

  async getSupportedAssets() {
    return DB.assets;
  }

  async createSubMainTransfer() {
    return {};
  }

  async createSubAccountsTransfer() {
    return {};
  }

  async createInternalTransfer() {
    return {};
  }
}
