import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class CompanyRepository {
  constructor() {}
  async getList() {
    try {
      const sql = `
        SELECT *
        FROM companies
        WHERE status = 'A'`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }
  async getInfoById(companyId: number) {
    try {
      const sql = `
        SELECT *
        FROM companies
        WHERE status = 'A'
        AND company_id = $1`;
      return await db.query(sql, [companyId]);
    } catch (error) {
      throw error;
    }
  }
}
