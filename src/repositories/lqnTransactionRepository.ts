import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class LqnTransactionRepository {
  constructor() { }
  async create(
    lqnTransactionType: string,
    customerId: string,
    receiverId: string,
    agentSessionId: string,
    confirmationId: string,
    agentTxnId: string,
    collectAmount: string,
    collectCurrency: string,
    serviceCharge: string,
    gstCharge: string,
    transferAmount: string,
    exchangeRate: string,
    payoutAmount: string,
    payoutCurrency: string,
    feeDiscount: string,
    additionalPremiumRate: string,
    settlementRate: string,
    sendCommission: string,
    settlementAmount: string,
    pinNumber: string,
    status: string,
    message: string,
    additionalMessage: string
  ) {
    try {
      const sql = `
        INSERT INTO lqn_transactions(lqn_transaction_date, lqn_transaction_type, customer_id, receiver_id, agent_session_id, confirmation_id, agent_txn_id, collect_amount, collect_currency, service_charge, gst_charge, transfer_amount, exchange_rate, payout_amount, payout_currency, fee_discount, additional_premium_rate, settlement_rate, send_commission, settlement_amount, pin_number, status, message, additionalmessage)
        VALUES(CURRENT_TIMESTAMP, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23)
        RETURNING *`;
      return await db.query(sql, [
        lqnTransactionType,
        customerId,
        receiverId,
        agentSessionId,
        confirmationId,
        agentTxnId,
        collectAmount,
        collectCurrency,
        serviceCharge,
        gstCharge,
        transferAmount,
        exchangeRate,
        payoutAmount,
        payoutCurrency,
        feeDiscount,
        additionalPremiumRate,
        settlementRate,
        sendCommission,
        settlementAmount,
        pinNumber,
        status,
        message,
        additionalMessage,
      ]);
    } catch (error) {
      throw error;
    }
  }

  async updateConfirmTransaction(
    confirmationId: string,
    pinNumber: string,
    status: string,
    message: string
  ) {
    try {
      const sql = `
        UPDATE lqn_transactions
        SET pin_number = $2
          , status = $3
          , message = $4
        WHERE confirmation_id = $1
        RETURNING *`;
      return await db.query(sql, [confirmationId, pinNumber, status, message]);
    } catch (error) {
      throw error;
    }
  }

  async updateCallbackTransaction(
    pinNumber: string,
    status: string,
    message: string
  ) {
    try {
      const sql = `
        UPDATE lqn_transactions
        SET status = $2
          , message = $3
        WHERE pin_number = $1
        RETURNING *`;
      return await db.query(sql, [pinNumber, status, message]);
    } catch (error) {
      throw error;
    }
  }
}
