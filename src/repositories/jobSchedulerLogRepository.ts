import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class JobSchedulerLogRepository {
  constructor() {}
  async logStartJob(method: string) {
    try {
      const sql = `
        INSERT INTO job_scheduler_logs(start_time, end_time, "method", message)
        VALUES(CURRENT_TIMESTAMP, NULL, $1, NULL)
        RETURNING *`;
      return await db.query(sql, [method]);
    } catch (error) {
      throw error;
    }
  }

  async logEndJob(jobSchedulerLogId: number, message: string) {
    try {
      const sql = `
        UPDATE job_scheduler_logs
        SET end_time = CURRENT_TIMESTAMP
            , message = $2
        WHERE job_scheduler_log_id = $1
        RETURNING *`;
      return await db.query(sql, [jobSchedulerLogId, message]);
    } catch (error) {
      throw error;
    }
  }
}
