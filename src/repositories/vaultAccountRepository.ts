import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class VaultAccountRepository {
  constructor() {}
  async getInfo(type: string, companyId?: number, userId?: number) {
    try {
      let sql = "";
      if (type === "C") {
        sql = `
          select companies.company_id as id
            , companies."name" as "name"
            , vault_accounts.vault_account_id
            , vault_accounts.vault_account_name
            , vault_accounts.fireblock_vault_account_id
            , vault_accounts.status
          from vault_accounts
            inner join companies on companies.company_id = vault_accounts.company_id
                                and companies.status = 'A'
          where vault_accounts.status = 'A'
          and companies.company_id = $1`;
      } else {
        sql = `
          select users.user_id as id
            , CONCAT(users.first_name, ' ', users.last_name) as "name"
            , vault_accounts.vault_account_id
            , vault_accounts.vault_account_name
            , vault_accounts.fireblock_vault_account_id
            , vault_accounts.status
          from vault_accounts
            inner join users on users.user_id = vault_accounts.user_id
                            and users.status = 'A'
          where vault_accounts.status = 'A'
          and users.user_id = $2`;
      }
      return await db.query(sql, [companyId, userId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByFireblocksVaultAccountId(fireblocksVaultAccountId: string) {
    try {
      const sql = `
        select users.user_id
          , CONCAT(users.first_name, ' ', users.last_name) as "user_full_name"
          , vault_accounts.vault_account_id
          , vault_accounts.vault_account_name
          , vault_accounts.fireblock_vault_account_id
          , vault_accounts.status
        from vault_accounts
          inner join users on users.user_id = vault_accounts.user_id
        and users.status = 'A'
        where vault_accounts.status = 'A'
        and vault_accounts.fireblock_vault_account_id = $1`;
      return await db.query(sql, [fireblocksVaultAccountId]);
    } catch (error) {
      throw error;
    }
  }

  // async getInfoById(type: string, vaultAccountId: number) {
  //   try {
  //     let sql = "";
  //     if (type === "C") {
  //       sql = `
  //         select companies.company_id as id
  //           , companies."name" as "name"
  //           , vault_accounts.vault_account_id
  //           , vault_accounts.vault_account_name
  //           , vault_accounts.fireblock_vault_account_id
  //           , vault_accounts.status
  //         from vault_accounts
  //           inner join companies on companies.company_id = vault_accounts.company_id
  //                               and companies.status = 'A'
  //         where vault_accounts.status = 'A'
  //         and vault_accounts.vault_account_id = $1`;
  //     } else {
  //       sql = `
  //         select users.user_id as id
  //           , CONCAT(users.first_name, ' ', users.last_name) as "name"
  //           , vault_accounts.vault_account_id
  //           , vault_accounts.vault_account_name
  //           , vault_accounts.fireblock_vault_account_id
  //           , vault_accounts.status
  //         from vault_accounts
  //           inner join users on users.user_id = vault_accounts.user_id
  //                           and users.status = 'A'
  //         where vault_accounts.status = 'A'
  //         and vault_accounts.vault_account_id = $1`;
  //     }
  //     return await db.query(sql, [vaultAccountId]);
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async create(
    type: string,
    vaultAccountName: string,
    fireblockVaultAccountId: string,
    logOnId: string,
    companyId?: number,
    userId?: number
  ) {
    try {
      const sql = `
        INSERT INTO vault_accounts("type", company_id, user_id, vault_account_name, fireblock_vault_account_id, status, created_by, created_date, updated_by, updated_date)
        VALUES($1, $2, $3, $4, $5, 'A', $6, CURRENT_TIMESTAMP, $6, CURRENT_TIMESTAMP)
        RETURNING *`;
      return await db.query(sql, [
        type,
        companyId,
        userId,
        vaultAccountName,
        fireblockVaultAccountId,
        logOnId,
      ]);
    } catch (error) {
      throw error;
    }
  }
}
