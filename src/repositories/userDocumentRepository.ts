import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class UserDocumentRepository {
  constructor() {}
  async getList(userId: number) {
    try {
      const sql = `
        SELECT *
        FROM user_documents
        WHERE user_id = $1
        ORDER BY user_document_id`;
      return await db.query(sql, [userId]);
    } catch (error) {
      throw error;
    }
  }

  async getInfoByType(userId: number, documentType: string) {
    try {
      const sql = `
        SELECT *
        FROM user_documents
        WHERE user_id = $1
        and document_type = $2`;
      return await db.query(sql, [userId, documentType]);
    } catch (error) {
      throw error;
    }
  }

  async create(
    userId: number,
    documentType: string,
    documentText: string,
    logOnId: string,
    documentTypeRemark: string,
    documentIssueDate: string,
    documentExpiredDate: string
  ) {
    try {
      const sql = `
        INSERT INTO user_documents(user_id, document_type, document_text, status, created_by, created_date, updated_by, updated_date,document_type_remark, document_issue_date, document_expired_date)
        VALUES($1, $2, $3, 'A', $4, CURRENT_TIMESTAMP, $4, CURRENT_TIMESTAMP, $5, $6, $7)
        RETURNING *`;
      return await db.query(sql, [
        userId,
        documentType,
        documentText,
        logOnId,
        documentTypeRemark,
        documentIssueDate,
        documentExpiredDate,
      ]);
    } catch (error) {
      throw error;
    }
  }

  // async update(userDocumentId: number, documentText: string, logOnId: string) {
  //   try {
  //     const sql = `
  //       UPDATE user_documents
  //       SET document_text = $2
  //           , updated_by = $3
  //           , updated_date = CURRENT_TIMESTAMP
  //       WHERE user_document_id = $1
  //       RETURNING *`;
  //     return await db.query(sql, [userDocumentId, documentText, logOnId]);
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async updateByDocType(
    userDocumentId: number,
    documentType: string,
    documentText: string,
    logOnId: string,
    documentTypeRemark: string,
    documentIssueDate: string,
    documentExpiredDate: string
  ) {
    try {
      const sql = `
        UPDATE user_documents
        SET document_type = $2
            , document_text = $3
            , updated_by = $4
            , updated_date = CURRENT_TIMESTAMP
            , document_type_remark = $5
            , document_issue_date = $6
            , document_expired_date = $7
        WHERE user_document_id = $1
        RETURNING *`;
      return await db.query(sql, [
        userDocumentId,
        documentType,
        documentText,
        logOnId,
        documentTypeRemark,
        documentIssueDate,
        documentExpiredDate,
      ]);
    } catch (error) {
      throw error;
    }
  }

  async delete(userDocumentId: number) {
    try {
      const sql = `
        DELETE FROM user_documents WHERE user_document_id = $1
        RETURNING *`;
      return await db.query(sql, [userDocumentId]);
    } catch (error) {
      throw error;
    }
  }
}
