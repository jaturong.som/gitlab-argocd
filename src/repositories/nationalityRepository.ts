import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class NationalityRepository {
  constructor() { }
  async getList() {
    try {
      const sql = `
      SELECT *
      FROM nationalities
      WHERE status = 'A'
      ORDER BY code`;
      return await db.query(sql);
    } catch (error) {
      throw error;
    }
  }
}
