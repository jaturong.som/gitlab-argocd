import { db } from "./connection";
import { singleton } from "tsyringe";

@singleton()
export class FetchLogRepository {
  constructor() {}

  async create(
    method: string,
    url: string,
    header: string,
    body: string,
    response: string
  ) {
    try {
      const sql = `
        INSERT INTO fetch_logs(request_date, "method", url, header, body, response)
        VALUES(CURRENT_TIMESTAMP, $1, $2, $3, $4, $5)
        RETURNING *`;
      return await db.query(sql, [method, url, header, body, response]);
    } catch (error) {
      throw error;
    }
  }
}
