import express from "express";
import fileUpload from "express-fileupload";
import cors from "cors";
import bodyParser from "body-parser";
// import morgan from "morgan";
import swaggerUi from "swagger-ui-express";

import "dotenv/config";

import errorHandler from "./middlewares/errorHandler";
import { morgan } from "./middlewares/morgan";

import swaggerDocument from "./swagger.json";

import { RegisterRoutes } from "./v1/routes/routes";

const app = express();
const PORT = process.env.PORT! || 3000;

// override send to store response body for morgan token to retrieve
const originalSend = app.response.send;
app.response.send = function sendOverride(body: any) {
  this.responseBody = body;
  return originalSend.call(this, body);
};

// enable files upload
app.use(
  fileUpload({
    createParentPath: true,
  })
);

app.use(cors());
app.use(bodyParser.json({ limit: "50mb" }));
app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 50000,
  })
);
app.use(express.json());
app.use(express.static("public"));
// app.use(morgan("tiny"));
app.use(
  morgan(
    ':remote-addr [:date[clf]] ":method :url HTTP/:http-version" Input :input Response :response-body'
  )
);

if (process.env.NODE_ENV! === "development") {
  app.use("/swagger", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
}

RegisterRoutes(app);

app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`API is listening on port ${PORT}`);
  if (process.env.NODE_ENV! === "development") {
    console.log(
      `Swagger service are available on http://localhost:${PORT}/swagger`
    );
  }
});
