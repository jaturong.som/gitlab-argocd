CREATE TABLE addresses (
	address_id bigserial NOT NULL,
	address_type varchar(50) NOT NULL,
	ref_id int4 NOT NULL,
	line1 varchar(255) NOT NULL,
	line2 varchar(255) NULL,
	city varchar(50) NULL,
	state varchar(50) NULL,
	country_code varchar(3) NOT NULL,
	postcode varchar(50) NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT addresses_pkey PRIMARY KEY (address_id)
);

CREATE TABLE asset_in_vault_accounts (
	asset_in_vault_account_id bigserial NOT NULL,
	vault_account_id int4 NOT NULL,
	asset_id int4 NULL,
	asset_code varchar(50) NULL,
	asset_address varchar(255) NOT NULL,
	total float8 NOT NULL,
	available float8 NOT NULL,
	pending float8 NOT NULL,
	lockedamount float8 NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	step_process varchar(50) NULL,
	CONSTRAINT asset_in_vault_accounts_pkey PRIMARY KEY (asset_in_vault_account_id)
);

CREATE TABLE asset_rates (
	asset_rate_id bigserial NOT NULL,
	asset_id int4 NULL,
	asset_code varchar(50) NULL,
	buying float8 NOT NULL,
	selling float8 NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT asset_rates_pkey PRIMARY KEY (asset_rate_id)
);

CREATE TABLE assets (
	asset_id bigserial NOT NULL,
	network_id int4 NOT NULL,
	native_asset_symbol varchar(50) NULL,
	code varchar(50) NOT NULL,
	"name" varchar(255) NOT NULL,
	image varchar(1000) NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	asset_group varchar(50) NOT NULL,
	contract_address varchar(100) NULL,
	CONSTRAINT assets_pkey PRIMARY KEY (asset_id)
);

CREATE TABLE bank_account_country_forms (
	bank_account_forms_id bigserial NOT NULL,
	country varchar NOT NULL,
	form_name varchar NOT NULL,
	CONSTRAINT bank_account_forms_pkey PRIMARY KEY (bank_account_forms_id)
);

CREATE TABLE bank_accounts (
	bank_account_id bigserial NOT NULL,
	user_id int4 NOT NULL,
	bank_code varchar(50) NOT NULL,
	bank_name varchar(255) NOT NULL,
	account_no varchar(50) NOT NULL,
	account_name varchar(255) NOT NULL,
	swift_code varchar(50) NULL,
	iban varchar(50) NULL,
	is_default varchar(2) NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	bank_type varchar(2) NULL,
	first_name varchar(255) NULL,
	last_name varchar(255) NULL,
	nationality varchar(255) NULL,
	date_of_birth timestamp NULL,
	mobile_number varchar NULL,
	branch_code varchar NULL,
	form_name varchar NULL,
	CONSTRAINT bank_accounts_pkey PRIMARY KEY (bank_account_id)
);

CREATE TABLE companies (
	company_id bigserial NOT NULL,
	"name" varchar(100) NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT companies_pkey PRIMARY KEY (company_id)
);

CREATE TABLE countries (
	code varchar(3) NOT NULL,
	alpha_2_code varchar(2) NOT NULL,
	"name" varchar(100) NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	image varchar(1000) NULL,
	calling_code varchar(10) NULL,
	CONSTRAINT countries_pkey PRIMARY KEY (code)
);

CREATE TABLE currencies (
	code varchar(3) NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT currencies_pkey PRIMARY KEY (code)
);

CREATE TABLE dropdownlists (
	id bigserial NOT NULL,
	"type" varchar(100) NOT NULL,
	value varchar(50) NOT NULL,
	"text" varchar(255) NOT NULL,
	"order" int2 NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT dropdownlists_pkey PRIMARY KEY (id)
);

CREATE TABLE fiat_accounts (
	fiat_id bigserial NOT NULL,
	"type" varchar(2) NOT NULL,
	company_id int4 NULL,
	user_id int4 NULL,
	total float8 NOT NULL,
	available float8 NOT NULL,
	pending float8 NOT NULL,
	lockedamount float8 NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT fiat_accounts_pkey PRIMARY KEY (fiat_id)
);

CREATE TABLE fireblock_transactions (
	fireblock_transaction_id bigserial NOT NULL,
	fb_transaction_id varchar(100) NOT NULL,
	"type" varchar(100) NOT NULL,
	asset_id varchar(50) NOT NULL,
	source_id varchar(50) NOT NULL,
	source_type varchar(100) NOT NULL,
	destination_id varchar(50) NULL,
	destination_type varchar(100) NOT NULL,
	amount float8 NOT NULL,
	fee float8 NOT NULL,
	remark varchar(1000) NULL,
	status varchar(50) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	ref_id int4 NULL,
	fb_transaction_hash varchar(100) NULL,
	destination_address varchar(255) NULL,
	CONSTRAINT fireblock_transactions_pkey PRIMARY KEY (fireblock_transaction_id)
);

CREATE TABLE gas_configurations (
	gas_configuration_id bigserial NOT NULL,
	asset_code varchar(50) NOT NULL,
	native_asset_code varchar(50) NOT NULL,
	gas_threshold float8 NOT NULL,
	gas_cap float8 NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT gas_configurations_pkey PRIMARY KEY (gas_configuration_id)
);

CREATE TABLE group_menus (
	group_menu_code varchar(5) NOT NULL,
	"name" varchar(100) NOT NULL,
	url varchar(255) NULL,
	component_name varchar(100) NULL,
	description varchar(255) NULL,
	"order" int2 NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT group_menus_pkey PRIMARY KEY (group_menu_code)
);

CREATE TABLE http_logs (
	http_log_id bigserial NOT NULL,
	request_date timestamp NOT NULL,
	requester text NULL,
	remote_address varchar(100) NULL,
	"method" varchar(10) NULL,
	url varchar(1000) NULL,
	body text NULL,
	response text NULL,
	"header" text NULL,
	CONSTRAINT http_logs_pkey PRIMARY KEY (http_log_id)
);

CREATE TABLE job_scheduler_logs (
	job_scheduler_log_id bigserial NOT NULL,
	start_time timestamp NOT NULL,
	end_time timestamp NULL,
	"method" varchar(100) NOT NULL,
	message text NULL,
	CONSTRAINT job_scheduler_logs_pkey PRIMARY KEY (job_scheduler_log_id)
);

CREATE TABLE logs (
	logs_id bigserial NOT NULL,
	logs_type varchar(50) NOT NULL,
	logs_name varchar NOT NULL,
	logs_message varchar NOT NULL,
	created_date timestamp NOT NULL,
	CONSTRAINT logs_pkey PRIMARY KEY (logs_id)
);

CREATE TABLE menus (
	group_menu_code varchar(5) NOT NULL,
	menu_code varchar(5) NOT NULL,
	"name" varchar(100) NOT NULL,
	url varchar(255) NOT NULL,
	component_name varchar(100) NULL,
	description varchar(255) NULL,
	"order" int2 NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT menus_pkey PRIMARY KEY (group_menu_code, menu_code)
);

CREATE TABLE networks (
	network_id bigserial NOT NULL,
	code varchar(50) NOT NULL,
	"name" varchar(255) NOT NULL,
	image varchar(1000) NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT networks_pkey PRIMARY KEY (network_id)
);

CREATE TABLE password_logs (
	password_log_id bigserial NOT NULL,
	user_id int4 NOT NULL,
	"password" varchar(255) NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT password_logs_pkey PRIMARY KEY (password_log_id)
);

CREATE TABLE permissions (
	permission_id bigserial NOT NULL,
	code varchar(20) NOT NULL,
	description varchar(255) NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT permissions_pkey PRIMARY KEY (permission_id)
);

CREATE TABLE role_menus (
	role_menu_id bigserial NOT NULL,
	role_id int4 NOT NULL,
	group_menu_code varchar(5) NOT NULL,
	menu_code varchar(5) NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT role_menus_pkey PRIMARY KEY (role_menu_id)
);

CREATE TABLE role_permissions (
	role_permission_id bigserial NOT NULL,
	role_id int4 NOT NULL,
	permission_id int4 NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT role_permissions_pkey PRIMARY KEY (role_permission_id)
);

CREATE TABLE roles (
	role_id bigserial NOT NULL,
	"name" varchar(100) NOT NULL,
	description varchar(255) NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT roles_pkey PRIMARY KEY (role_id)
);

CREATE TABLE summary (
	summary_key varchar(50) NOT NULL,
	summary_value float8 NOT NULL,
	updated_date timestamp NULL,
	updated_by varchar(50) NULL,
	CONSTRAINT summary_pkey PRIMARY KEY (summary_key)
);

CREATE TABLE transactions (
	transaction_id bigserial NOT NULL,
	transaction_date timestamp NOT NULL,
	transaction_no varchar(100) NOT NULL,
	user_id int4 NOT NULL,
	"type" varchar(2) NOT NULL,
	asset_id int4 NULL,
	asset_code varchar(50) NULL,
	buying float8 NULL,
	selling float8 NULL,
	total_amount float8 NOT NULL,
	total_assets float8 NOT NULL,
	completion_date timestamp NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	source_id int4 NULL,
	destination_id int4 NULL,
	source_vault_account_id int4 NULL,
	destination_vault_account_id int4 NULL,
	currency_code varchar(3) NULL,
	fb_transaction_hash varchar(100) NULL,
	fb_transaction_status varchar(50) NULL,
	fb_transaction_id varchar(100) NULL,
	"source" varchar(50) NULL,
	destination_address varchar(255) NULL,
	CONSTRAINT transactions_pkey PRIMARY KEY (transaction_id)
);

CREATE TABLE user_api_keys (
	user_api_key_id bigserial NOT NULL,
	user_id int4 NOT NULL,
	"name" varchar(100) NOT NULL,
	user_key varchar(255) NOT NULL,
	user_secret varchar NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT user_api_keys_pkey PRIMARY KEY (user_api_key_id)
);

CREATE TABLE user_documents (
	user_document_id bigserial NOT NULL,
	user_id int4 NOT NULL,
	document_type varchar(50) NOT NULL,
	document_text varchar(200) NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT user_documents_pkey PRIMARY KEY (user_document_id)
);

CREATE TABLE user_locks (
	user_lock_id bigserial NOT NULL,
	user_id int4 NOT NULL,
	locked_date timestamp NOT NULL,
	reason varchar(255) NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT user_locks_pkey PRIMARY KEY (user_lock_id)
);

CREATE TABLE user_roles (
	user_role_id bigserial NOT NULL,
	user_id int4 NOT NULL,
	role_id int4 NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT user_roles_pkey PRIMARY KEY (user_role_id)
);

CREATE TABLE user_tokens (
	user_token_id bigserial NOT NULL,
	user_id int4 NOT NULL,
	"token" varchar(255) NOT NULL,
	refresh_token varchar(255) NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT user_tokens_pkey PRIMARY KEY (user_token_id)
);

CREATE TABLE users (
	user_id bigserial NOT NULL,
	code varchar(255) NOT NULL,
	"type" bpchar(1) NOT NULL,
	user_name varchar(50) NOT NULL,
	"password" varchar(255) NOT NULL,
	first_name varchar(100) NULL,
	middle_name varchar(100) NULL,
	last_name varchar(100) NULL,
	email varchar(100) NOT NULL,
	tel varchar(100) NULL,
	user_id_ref int4 NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	date_of_birth timestamp NULL,
	country_code varchar(3) NULL,
	CONSTRAINT users_pkey PRIMARY KEY (user_id)
);

CREATE TABLE vault_accounts (
	vault_account_id bigserial NOT NULL,
	"type" varchar(2) NOT NULL,
	company_id int4 NULL,
	user_id int4 NULL,
	vault_account_name varchar(100) NOT NULL,
	fireblock_vault_account_id varchar(255) NOT NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	CONSTRAINT vault_accounts_pkey PRIMARY KEY (vault_account_id)
);

CREATE TABLE withdraws (
	withdraw_id bigserial NOT NULL,
	withdraw_date timestamp NOT NULL,
	withdraw_no varchar(100) NOT NULL,
	user_id int4 NOT NULL,
	"type" varchar(2) NOT NULL,
	bank_account_id int4 NOT NULL,
	amount float8 NOT NULL,
	completion_date timestamp NULL,
	status varchar(2) NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	currency_code varchar(3) NULL,
	purpose_of_transfer varchar(100) NULL,
	purpose_of_transfer_other varchar(1000) NULL,
	relationship_to_receiver varchar(100) NULL,
	relationship_to_receiver_other varchar(1000) NULL,
	reason varchar(1000) NULL,
	CONSTRAINT withdraws_pkey PRIMARY KEY (withdraw_id)
);

CREATE TABLE chainalysis_transactions (
	chainalysis_transaction_id bigserial NOT NULL,
	updated_at varchar NULL,
	asset varchar not NULL,
	"network" varchar not NULL,
	transfer_reference varchar not NULL,
	tx varchar NULL,
	idx int4 NULL,
	usd_amount float8 NULL,
	asset_amount float8 NULL,
	"timestamp" varchar NULL,
	output_address varchar NULL,
	external_id varchar not null,
	status varchar(2) NULL,
	created_by varchar(50) NULL,
	created_date timestamp NULL,
	updated_by varchar(50) NULL,
	updated_date timestamp NULL,
	fb_transaction_id varchar(100) NULL,
	CONSTRAINT chainalysis_transactions_pkey PRIMARY KEY (chainalysis_transaction_id)
);

CREATE TABLE fetch_logs (
	fetch_log_id bigserial NOT NULL,
	request_date timestamp NOT NULL,
	"method" varchar(10) NULL,
	url varchar(1000) NULL,
	body text NULL,
	response text NULL,
	"header" text NULL,
	CONSTRAINT fetch_logs_pkey PRIMARY KEY (fetch_log_id)
);

CREATE TABLE lqn_transactions (
	lqn_transaction_id bigserial NOT NULL,
	lqn_transaction_date timestamp NOT NULL,
	lqn_transaction_type varchar(2) not null,
	customer_id varchar NOT null,
	receiver_id varchar NOT null,
	agent_session_id varchar null,
	confirmation_id varchar null,
	agent_txn_id varchar null,
	collect_amount varchar null,
	collect_currency varchar(3) null,
	service_charge varchar null,
	gst_charge varchar null,
	transfer_amount varchar null,
	exchange_rate varchar null,
	payout_amount varchar null,
	payout_currency varchar(3) null,
	fee_discount varchar null,
	additional_premium_rate varchar null,
	settlement_rate varchar null,
	send_commission varchar null,
	settlement_amount varchar null,
	pin_number varchar null,
	status varchar null,
	message varchar null,
	additionalMessage varchar null,
	CONSTRAINT lqn_transactions_pkey PRIMARY KEY (lqn_transaction_id)
);