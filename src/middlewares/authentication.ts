import * as express from "express";
import * as crypto from "crypto";
import { AdminService } from "../services/adminService";
import * as authModels from "../models/authModels";
import * as utils from "../utilities/utils";
import * as Enums from "../utilities/enums";
import CustomError from "./customError";
import { UserRepository } from "../repositories/userRepository";
import { UserRoleRepository } from "../repositories/userRoleRepository";
import { AddressRepository } from "../repositories/addressRepository";
import { UserDocumentRepository } from "../repositories/userDocumentRepository";
import { UserApiKeyRepository } from "../repositories/userApiKeyRepository";
import FireblocksNetworkLinkCustomError from "./fireblocksNetworkLinkCustomError";

export async function expressAuthentication(
  request: express.Request,
  securityName: string,
  scopes?: string[]
): Promise<any> {
  if (securityName === "api_key") {
    if (!request.headers["authorization"])
      return new Promise((resolve, reject) => {
        reject(
          new CustomError(
            Enums.responseCode.Unauthorized,
            Enums.responseMessage.Unauthorized
          )
        );
      });
    /*
    if (!request.headers["access-code"])
      return new Promise((resolve, reject) => {
        reject(new CustomError(Enums.responseCode.Unauthorized, Enums.responseMessage.Unauthorized));
      });
      */
    const accessToken = request.headers["authorization"].replace("Bearer ", "");
    if (!accessToken) {
      return new Promise((resolve, reject) => {
        reject(
          new CustomError(
            Enums.responseCode.Unauthorized,
            Enums.responseMessage.Unauthorized
          )
        );
      });
    }
    try {
      const userAccess = await utils.jwtVerifyToken(accessToken);
      const obj = JSON.stringify(userAccess);
      const resLogin: authModels.ResponseLogin = JSON.parse(obj);
      let tempObject: any = null;
      const userRepository = new UserRepository();
      const userRoleRepository = new UserRoleRepository();
      const userAddressRepository = new AddressRepository();
      const userDocumentRepository = new UserDocumentRepository();
      const _adminService = new AdminService(
        userRepository,
        userRoleRepository,
        userAddressRepository,
        userDocumentRepository,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject,
        tempObject
      );
      const userInfo = await _adminService.getActiveUserById(
        parseInt(resLogin.user_id)
      );
      return Promise.resolve({
        user_id: userInfo.user_id,
        code: userInfo.code,
        user_name: userInfo.user_name,
        first_name: userInfo.first_name,
        middle_name: userInfo.middle_name,
        last_name: userInfo.last_name,
        email: userInfo.email,
        tel: userInfo.tel,
      });
    } catch (error) {
      let errorMessage: string;
      if (error instanceof Error) {
        errorMessage = error.message;
      }
      return new Promise((resolve, reject) => {
        reject(new Error(errorMessage));
      });
    }
  }
  if (securityName === "network_link") {
    const userApiKeyRepository = new UserApiKeyRepository();
    const userRepository = new UserRepository();
    if (
      !request.headers["x-fbapi-key"] ||
      !request.headers["x-fbapi-signature"] ||
      !request.headers["x-fbapi-timestamp"] ||
      !request.headers["x-fbapi-nonce"]
    ) {
      return new Promise((resolve, reject) => {
        reject(
          new FireblocksNetworkLinkCustomError(
            400,
            Enums.responseCodeFireblock.MISSING_REQUEST,
            Enums.responseMessageFireblock.MISSING_REQUEST
          )
        );
      });
    }
    const fbKey = request.headers["x-fbapi-key"];
    const fbSignature = request.headers["x-fbapi-signature"];
    const fbTimestamp = request.headers["x-fbapi-timestamp"];
    const fbNonce = request.headers["x-fbapi-nonce"];
    const url = request.url.replace("/fireblocks", "");
    const reqBody =
      JSON.stringify(request.body) !== "{}" ? JSON.stringify(request.body) : "";
    const prehashString =
      fbTimestamp.toString() +
      fbNonce.toString() +
      request.method +
      url +
      reqBody;
    const preHashStringUTF8 = Buffer.from(prehashString, "utf-8").toString(
      "hex"
    );

    try {
      const userApiKey = await userApiKeyRepository.getInfoByKey(
        fbKey.toString()
      );
      if (userApiKey.length <= 0) {
        throw new FireblocksNetworkLinkCustomError(
          400,
          Enums.responseCodeFireblock.ACCOUNT_NOT_FOUND,
          Enums.responseMessageFireblock.ACCOUNT_NOT_FOUND
        );
      }
      const hmac = crypto.createHmac("sha256", userApiKey[0].user_secret);
      hmac.update(preHashStringUTF8, "utf-8");
      const signature = hmac.digest("hex");
      if (signature === fbSignature) {
        const users = await userRepository.getActiveInfoById(
          userApiKey[0].user_id
        );
        if (users.length <= 0) {
          throw new FireblocksNetworkLinkCustomError(
            400,
            Enums.responseCodeFireblock.ACCOUNT_NOT_FOUND,
            Enums.responseMessageFireblock.ACCOUNT_NOT_FOUND
          );
        }
        const userInfo = users[0];

        return Promise.resolve({
          user_id: userInfo.user_id,
          first_name: userInfo.first_name,
          middle_name: userInfo.middle_name,
          last_name: userInfo.last_name,
          date_of_birth: userInfo.date_of_birth,
          email: userInfo.email,
          tel: userInfo.tel,
        });
      } else {
        throw new FireblocksNetworkLinkCustomError(
          400,
          Enums.responseCodeFireblock.SIGNATURE_INVALID,
          Enums.responseMessageFireblock.SIGNATURE_INVALID
        );
      }
    } catch (error) {
      throw error;
    }
  }
  return new Promise((resolve, reject) => {
    reject(
      new CustomError(
        Enums.responseCode.Unauthorized,
        Enums.responseMessage.Unauthorized
      )
    );
  });
}
