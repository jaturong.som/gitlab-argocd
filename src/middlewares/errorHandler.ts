import { Request, Response, NextFunction } from "express";
import { ValidateError } from "tsoa";
import CustomError from "./customError";
import FireblocksNetworkLinkCustomError from "./fireblocksNetworkLinkCustomError";

const errorHandler = async (
  err: unknown,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (err instanceof ValidateError) {
    console.warn(`Caught Validation Error for ${req.path}:`, err.fields);
    return res.status(422).json({
      message: "Validation Failed",
      details: err?.fields,
    });
  }
  if (err instanceof CustomError) {
    console.warn(`Custom Error:`, err?.message);
    return res.status(400).json({
      code: err?.code,
      message: err?.message,
    });
  }
  if (err instanceof FireblocksNetworkLinkCustomError) {
    console.warn(`FireblocksNetworkLinkCustomError Error:`, err?.message);
    return res.status(err?.httpStatas).json({
      errorCode: err?.errorCode,
      error: err?.message,
    });
  }
  if (err instanceof Error) {
    console.error(`Error:`, err?.message);
    return res.status(500).json({
      code: 500,
      message: err?.message,
      stack: process.env.NODE_ENV! === "development" ? err.stack : {},
    });
  }
  return next();
};

export = errorHandler;
