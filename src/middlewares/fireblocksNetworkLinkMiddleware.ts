import { Request, Response, NextFunction } from "express";

export const fireblocksNetworkLinkAuthorization = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.headers["x-fbapi-key"]) return res.sendStatus(401);
  if (!req.headers["x-fbapi-signature"]) return res.sendStatus(401);
  if (!req.headers["x-fbapi-timestamp"]) return res.sendStatus(401);
  if (!req.headers["x-fbapi-nonce"]) return res.sendStatus(401);

  const fbKey = req.headers["x-fbapi-key"];
  const fbSignature = req.headers["x-fbapi-signature"];
  const fbTimestamp = req.headers["x-fbapi-timestamp"];
  const fbNonce = req.headers["x-fbapi-nonce"];
  console.log(fbKey);
  console.log(fbSignature);
  console.log(fbTimestamp);
  console.log(fbNonce);
  try {
    return next();
  } catch (err) {
    return res.status(500).send({
      code: 500,
      message: {
        error: (err as Error)?.message || err,
        stack:
          process.env.NODE_ENV! === "development" ? (err as Error)?.stack : {},
      },
    });
  }
};
