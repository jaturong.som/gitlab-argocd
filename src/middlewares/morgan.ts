import { Request, Response } from "express";
import morgan from "morgan";
import { HttpLogService } from "../services/httpLogService";
// import { FireblocksWebhookService } from "../services/fireblocksWebhookService";
import { HttpLogRepository } from "../repositories/httpLogRepository";
// import { TransactionRepository } from "../repositories/transactionRepository";
// import { FireblocksTransactionRepository } from "../repositories/fireblocksTransactionRepository";
// import * as enums from "../utilities/enums";
// import { TransactionService } from "../services/transactionService";
// import { ChainalysisService } from "../services/chainalysisService";
// import { FireblocksService } from "../services/fireblocksService";

morgan.token("user", function getRequester(req: Request): string {
  return JSON.stringify(req.user);
});

morgan.token("input", function getInput(req: Request): string {
  let input: Record<string, any> = {};
  if (req.method === "GET") {
    input = req.query;
  } else {
    input = req.body;
  }
  // mask any input that should be secret
  input = { ...input };
  if (input.password) {
    input.password = "***";
  }
  if (input.old_password) {
    input.old_password = "***";
  }
  if (input.new_password) {
    input.new_password = "***";
  }
  if (input.refresh_token) {
    input.refresh_token = "***";
  }
  return JSON.stringify(input);
});

morgan.token("response-body", (req: Request, res: Response): string => {
  // console.log("Request:", req);
  // console.log("Response:", res);
  // console.log("requester:", req.user);
  // console.log("_remoteAddress:", req.socket.remoteAddress);
  // console.log("method:", req.method);
  // console.log("url:", req.url);
  // console.log("httpVersion:", req.httpVersion);
  // console.log("Input:", req.body);
  // console.log("response-body:", res.responseBody);

  var jsonString = "";
  try {
    jsonString = JSON.stringify(req.headers);
  } catch (error) {
    console.log(error);
  }

  var vJSONValid = true;
  var body;
  try {
    body = { ...JSON.parse(res.responseBody) };
    vJSONValid = true;
  } catch (error) {
    vJSONValid = false;
  }
  if (vJSONValid) {
    const reqBody = req.body;
    // mask any input that should be secret
    if (reqBody?.password) {
      reqBody.password = "***";
    }
    if (reqBody?.old_password) {
      reqBody.old_password = "***";
    }
    if (reqBody?.new_password) {
      reqBody.new_password = "***";
    }
    if (reqBody?.refresh_token) {
      reqBody.refresh_token = "***";
    }
    if (body?.result?.token) {
      body.result.token = "***";
    }
    if (body?.result?.refresh_token) {
      body.result.refresh_token = "***";
    }

    try {
      var httpLogService = new HttpLogService(new HttpLogRepository());
      httpLogService.createHttpLog(
        req.user,
        req.socket.remoteAddress === undefined ? "" : req.socket.remoteAddress,
        req.method,
        req.url,
        jsonString,
        req.body,
        body
      );
    } catch (error) {
      console.log(error);
    }
  }
  return JSON.stringify(body);
});

export { morgan };
