class FireblocksNetworkLinkCustomError extends Error {
  httpStatas : number;
  errorCode: number;
  error: string;
  constructor(httpStatas:number,code: number, message: string) {
    super(message);
    this.errorCode = code;
    this.error = message;
    this.httpStatas = httpStatas;
  }
}

export default FireblocksNetworkLinkCustomError;
